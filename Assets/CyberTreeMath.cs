﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CyberTreeMath : MonoBehaviour {

    public static Vector3 s_vecTempPos = new Vector3();

    public static float c_fAngleToRadian = 0.017453f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public static void  RotateGameObjByAngle( GameObject obj, float fAngle )
    {
        obj.transform.localRotation = Quaternion.identity;
        obj.transform.Rotate(0.0f, 0.0f, fAngle);
    }

    public static float Dir2Angle( float fDirX, float fDirY )
    {
        float fAngle = 0f;

        if (fDirX == 0)
        {
            if (fDirY > 0)
            {
                fAngle = 90f;
            }
            else
            {
                fAngle = 270;
            }
        }
        else if (fDirY == 0 )
        {
            if (fDirX > 0)
            {
                fAngle = 0f;
            }
            else
            {
                fAngle = 180;
            }
        }
        else
        {
            fAngle = Mathf.Abs( Mathf.Atan(fDirY / fDirX) );
            fAngle = Radian2Angel(fAngle);

            if ( fDirX < 0 && fDirY > 0)
            {
                fAngle = 180 - fAngle;
            }
            else if (fDirX < 0 && fDirY < 0)
            {
                fAngle = 180 + fAngle;
            }
            else if (fDirX > 0 && fDirY < 0)
            {
                fAngle = 360 - fAngle;
            }
        }

        
        return fAngle;
    }

    public static float Angle2Radian( float fAngle)
    {
        return c_fAngleToRadian * fAngle;
    }

    public static float Radian2Angel(float fRadian)
    {
        return fRadian / c_fAngleToRadian;
    }

    public static float CosByAngle( float fAngle )
    {
        return Mathf.Cos(Angle2Radian(fAngle));
    }

    // 计算位移
    public static float CalculateDistance( float v0, float a, float t )
	{
		return ( v0 * t + 0.5f * a * t * t );
	}

    // 已知位移和时间求初速度（末速度为零）
    public static float GetV0(float s, float t)
    {
        return (2.0f * s / t);
    }

    // 已知位移和时间求加速度（末速度为零）
    public static float GetA(float s, float t)
    {
        return -2.0f * s / (t * t);
    }

    public static float GetAByV0( float s, float v0 )
    {
        return 0.5f * (-v0 * v0) / s;
    }

    public static float Get_A_By_V0_And_t( float s, float v0, float t )
    {
        return 2 * ( s - v0 * t ) / ( t * t );
    }

    // 计算新吐出的球初始位置和冲刺方向
    public static void CalculateSpittedObjBornPos( float fMotherPosX, float fMotherPosY, float fMotherRadius, float fMotherDirX, float fMotherDirY, float fChildRadius, ref float fChildPosX, ref float fChildPosY )
	{
		float fDis = fMotherRadius +  fChildRadius;
		fChildPosX = fMotherPosX + fDis * fMotherDirX;
		fChildPosY = fMotherPosY + fDis * fMotherDirY;
	}

    public static bool ClampBallPosWithinSCreen( Vector3 srcPos, ref Vector3 destPos )
    {
        bool bRet = false;
		destPos = srcPos;
		return false;

        s_vecTempPos = Camera.main.WorldToScreenPoint(srcPos);
        if (s_vecTempPos.x > Screen.width)
        {
            s_vecTempPos.x = Screen.width;
            bRet = true;
        }
        if (s_vecTempPos.x < 0)
        {
            s_vecTempPos.x = 0;
            bRet = true;
        }
        if (s_vecTempPos.y > Screen.height)
        {
            s_vecTempPos.y = Screen.height;
            bRet = true;
        }
        if (s_vecTempPos.y < 0)
        {
            s_vecTempPos.y = 0;
            bRet = true;
        }
        destPos = Camera.main.ScreenToWorldPoint(s_vecTempPos);
        return bRet;
    }


	public static void CalculateSpittedObjRunParams( float fRunDistance, float fRunTime, ref float fInitSpeed, ref float fAccelerate )
	{
		fInitSpeed = GetV0 (fRunDistance, fRunTime  );
		fAccelerate = GetA ( fRunDistance, fRunTime);
	}

	public const float c_shit = 1.0f / 3.0f;
    public const float c_shi_1 = 3f / (4f * Mathf.PI);
    public const float c_shi_2 = (4f * Mathf.PI) / 3f;

	public static float SizeToArea( float fSize, int nShit )
	{
		float fArea = 0.0f;
		if (nShit == 2) {
			fArea = fSize * fSize;
		} else if( nShit == 3 )  {
			fArea = fSize * fSize * fSize;
		}
		return fArea;
	}

    public static float TiJi2BanJing( float fTiJi )
    {
        if (Main.s_Instance.m_nShit == 2)
        {
           return  Mathf.Sqrt(fTiJi);
        }
        else
        {
            return Mathf.Pow(fTiJi, c_shit);
        }
    }   

    public static float Kai3CiFangGenHao( float src )
    {
        return Mathf.Pow(src, c_shit);
    }

    public static float KaiGenHao( float src, float mi )
    {
        return Mathf.Pow(src, mi);
    }

    public static float AreaToSize( float fArea, int nShit )
	{
		float fSize = 0.0f;
		if (nShit == 2) {
			fSize = Mathf.Sqrt( fArea );
		} else if( nShit == 3 )  {
			fSize = Mathf.Pow ( fArea, c_shit );
		}
        return fSize;
	}

    public static string ReOrganizeFilePath( string szFileName )
    {
        string[] ary = szFileName.Split( '/' );
        string ret = "";
        for ( int i = 0; i < ary.Length; i++ )
        {
            ret += ary[i];
            ret += '\\';
        }
        return ret;
    }

	public static bool CheckIfPartiallyCover_CircleCollider2D( CircleCollider2D c1, CircleCollider2D c2, float fMianJi2 )
	{
		float  d;
		float  s,s1,s2,s3,angle1,angle2;
		float r1 = c1.bounds.size.x / 2f ;
		float r2 = c2.bounds.size.x / 2f ;

		d = Mathf.Sqrt((c1.bounds.center.x-c2.bounds.center.x)*(c1.bounds.center.x-c2.bounds.center.x)+(c1.bounds.center.y-c2.bounds.center.y)*(c1.bounds.center.y-c2.bounds.center.y));
		if (d >= (r1 + r2)) {//两圆相离
			return false;
		}
		if ((r1 - r2) >= d) {//两圆内含,c1大
			return true;
		}

		angle1 = Mathf.Acos((r1*r1+d*d-r2*r2)/(2*r1*d));
		angle2 = Mathf.Acos((r2*r2+d*d-r1*r1)/(2*r2*d));

		s1=angle1*r1*r1;
		s2=angle2*r2*r2;
		s3=r1*d*Mathf.Sin(angle1);
		s=s1+s2-s3;

		float fCurPercent = s / fMianJi2;
		if (fCurPercent >= MapEditor.s_Instance.m_fYaQiuBaiFenBi ) {
			return true;
		}

		return false;
	}

	public static bool CheckIfArriveDest( Vector2 pos, Vector2 dest, float fDis )
	{
		float fShit = Vector2.Distance (pos, dest);
		if ( fShit <= fDis) {
			return true;
		}
		return false;
	}

	public static float FloatTrim( float raw, int n = 2 )
	{
		return (((int)( raw * 100)) / 100 );
	}

    public static float Scale2Radius( float fScale )
    {
        return fScale * 0.5f;// fScale * Main.s_Instance.m_fScale2Radius;
    }

    public static float Radius2Scale( float fRadius )
    {
        return fRadius * 2f;//  fRadius * Main.s_Instance.m_fRadius2Scale;
    }

    // [to youhua]这里要抽空看下，开方运算的使用要严格控制
    public static float Volume2Scale(float fVolume)
    {
        float fRadius = Kai3CiFangGenHao(c_shi_1 * fVolume);
        return Radius2Scale(fRadius);
    }

    public static float Volume2Scale(float fVolume, ref float fRadius)
    {
        fRadius = Kai3CiFangGenHao(c_shi_1 * fVolume);
        return Radius2Scale(fRadius);
    }

    public static float Scale2Volume( float fScale, ref float fRadius )
    {
        float fVolume = 0f;
        fRadius = Scale2Radius(fScale);
        fVolume = Radius2Volume(fRadius);

        return fVolume;
    }

    public static float Radius2Volume( float fRadius)
    {
        return c_shi_2 * (fRadius * fRadius * fRadius);
    }
    
    public static float LineY( float x, float x2, float y2 )
    {
        float y = y2 + y2 * (x - x2) / x2;
        return y;
    }

    public static float LineX(float y, float x2, float y2)
    {
        float x = x2 + x2 * (y - y2) / y2;
        return x;
    }

    public static bool IsPointInCircle( float fPointX, float fPointY, float fCirclrCenterX, float fCirclrCenterY, float fCirclrRadius)
    {
        return (fPointX - fCirclrCenterX) * (fPointX - fCirclrCenterX) + (fPointY - fCirclrCenterY) * (fPointY - fCirclrCenterY) < fCirclrRadius * fCirclrRadius;
    }

    public static string K_Style( float val )
    {
        string szVal = "";
        if ( val < 1000f )
        {
            szVal = val.ToString( "f2" );
        }
        else
        {
            szVal = (val / 1000f).ToString( "f2" ) + "K";
        }
        return szVal;
    }
}


/*
设有向线段AB，两端点A（xa,ya）,B(xb,yb)
另一点C(xc,yc)

float f = (xb - xa) * (yc - ya) - (xc - xa) * (yb - ya);
if(f > 0)
  点C位于有向线段AB的左侧
else if(f == 0)
  点C位于有向线段AB上（也有可能在延长线上）
else
  点C位于有向线段AB的右侧

    */
