﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
public class UI_MainSkill_Select : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public CShuangYaoGan _ShuangYaoGan;

    Main m_Main;
	public static bool s_bUsingUi = false;

	bool m_bSpitting = false;
	Touch _touch;

	// Use this for initialization
	void Start ()
    {
        GameObject go;
        go = GameObject.Find("Main Camera");
        m_Main = go.GetComponent<Main>();
	}
	
	// Update is called once per frame
	void Update () 
    {

	}



    public void OnPointerDown(PointerEventData evt)
	{
       
        if ( Main.s_Instance.GetPlatformType() == Main.ePlatformType.mobile )
        {
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(Input.touchCount - 1);
                Main.s_Instance.BeginSpitTouch(touch.fingerId, touch.position);

                _ShuangYaoGan.SetVisible( true );
            }
        }

        Main.s_Instance.Cast_R_Skill();
    }

    public void OnPointerUp(PointerEventData evt)
    {
       
        if (Main.s_Instance.GetPlatformType() == Main.ePlatformType.mobile)
        {
            Main.s_Instance.EndSpitTouch();

            _ShuangYaoGan.SetVisible(false);
        }


    }

	public void OnPointerEnter(PointerEventData evt)
	{
		if (this.gameObject.name == "btnSpit") {	

		} else if (this.gameObject.name == "btnCancelSkill") {

		} 
	}


	public void OnMove(PointerEventData evt)
	{
		Debug.Log ( "移动木有！！！移动木有！！！！" );


		if (this.gameObject.name == "btnSpit") {	
			
		} else if (this.gameObject.name == "btnCancelSkill") {
			
		} 
		//MapEditor.s_Instance._txtDebugInfo.text = "手指move " + _touch.position;
	}
}
