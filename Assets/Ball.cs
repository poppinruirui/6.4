/*
 * 一个PhotonView就是一个跟服务器端的长连接。如果每个球球身上都带一个photonView，这势必无谓地占用大量带宽资源，造成网络拥堵。
 * 
*/
// [to youhua] Ball身上_ba不要挂那么多东西，否则球多的时候，性能会呈几何级下降。能共享的必须共享。比如特效，用到的时候才实例化出来，不要一直挂在球球身上。
// 特效不要挂在球球身上。全客户端所有的球球共享特效资源，由一个特效管理器统一调度。
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering;
using Spine.Unity;

public class Ball : MonoBehaviour
{
    public CCosmosGunSight _gunsight = null;

    public SortingGroup _sortingGroup;

    public const int MAX_BALL_NUM = 10000;

    public MeshShape m_msMain;
    public MeshShape m_msStaticShell;
    public MeshShape m_msDynamicShell;

    /// <summary>
    /// /特效相关
    /// </summary>
    public CSkillModuleForBall _skillModule; // 技能模块

    public SpriteRenderer _srDustBling;

    public CFrameAnimationEffect _effectPlayerLevelUp;
    public CFrameAnimationEffect _effectSpitSmoke;

    // 狂暴
    public GameObject _goEffectContainer_KuangBao;
    public CFrameAnimationEffect _effectKuangBao_QianYao;
    public CFrameAnimationEffect _effectKuangBao_Up;
    public CFrameAnimationEffect _effectKuangBao_Down;

    public CFrameAnimationEffect[] m_aryEffectQianZou;
    public CFrameAnimationEffect[] m_aryEffectChiXu;
    public CFrameAnimationEffect[] m_aryEffectChiXu1;
    public SkeletonAnimation _skeaniEffectBianCi;

    public CFrameAnimationEffect _effectKill; // 击杀特效（这个特效是放在击杀者身上，而不是被杀者身上）
    public CEffect _effectMergeAll_QianYao;

    /// <summary>
    /// / 扩张技能
    /// </summary>
    public GameObject _goEffectContainer_QianYao; // 前摇特效的容器
    public MeshShape _effectUnfoldQianYao; // 扩张技能的前摇

    // end 特效相关

    public GameObject _container;

    public BallTrigger_New _scriptBallTrigger;


    public SkeletonAnimation _skeletonAnimatnion;
    public MeshRenderer _mrSkeleton;

    public MeshRenderer _mrShellCircle;

    public GameObject _shell;
    public GameObject _thorn;

    public CircleCollider2D _Trigger;
    public CircleCollider2D _TriggerMergeSelf;
    public CircleCollider2D _Collider;
    public CircleCollider2D _ColliderTemp;
    public CircleCollider2D _ColliderDust;

    public GameObject _Mouth;
    public SpriteRenderer _srMouth;
    public SpriteRenderer _srOutterRing;
    public GameObject _main; // 暂不废弃ƒ
    public Rigidbody2D _rigid;


    bool m_bIsEjecting = false;

    public static Vector3 vecTempSize = new Vector3();
    public static Vector3 vecTempPos = new Vector3();
    public static Vector3 vecTempPos2 = new Vector3();
    public static Vector3 vecTempScale = new Vector3();
    public static Vector2 vec2Temp = new Vector2();
    public static Vector2 vec2Temp1 = new Vector2();
    public static Color cTempColor = new Color();
    public static Color colorTemp = new Color();
    public static Vector3 vecTempDir = new Vector3();
    public static Vector3 vecTempDir2 = new Vector3();



    Vector2 m_vDir = new Vector2();
    Vector2 m_vSpeed = new Vector2();
    Vector2 m_vInitSpeed = new Vector2();
    Vector2 m_vAcclerate = new Vector2();


    public GameObject _goPlayerName;
    public Text _txtPlayerName;
    public Text _txtPlayerNameMiaoBian;


    public static Vector3 s_vecShellInitScale = new Vector3();


    public CEffect _effectPreUnfold;


    /*
	{
		set {  
			if (_ba == null) {
				_direction.x = value;
			} else {
				_ba._direction.x = value; 
			}

		}
		get { 
			if (_ba == null) {
				return _direction.x;
			} else {
				return _ba._direction.x; 
			}
		}
	}
	*/

    public float _diry = 0.0f;
    /*
	{
		set {  
			if (_ba == null) {
				_direction.y = value;
			} else {
				_ba._direction.y = value; 
			}

		}
		get { 
			if (_ba == null) {
				return _direction.y;
			} else {
				return _ba._direction.y; 
			}
		}
	}
	*/

    public GameObject _obsceneCircle;
    float m_fSpeed = 0.0f;




    public Player _Player = null; // 本Ball所属的Player（网络对象）
    public CPlayerIns _PlayerIns; // 本Ball所属的Player（实例）
    public GameObject _player = null; // 这个 ball所属的player

    eBallType m_eBallType = eBallType.ball_type_ball;
    public eBallType _balltype
    {
        set { m_eBallType = value; }
        get { return m_eBallType; }
    }

    public enum eBallType
    {
        ball_type_ball,          // 常规的球
        ball_type_thorn,         // 刺
        ball_type_bean,          // 豆子
        ball_type_spore,         // 孢子 
    };

    public BallAction _ba; // 小熊那边做的BallAction模块


    /*
	[PunRPC]
	public void RPC_DestroyBall()
	{
		if (photonView.isMine) {
			Main.s_Instance.PhotonDestroy (this.gameObject);
		} 
	}
	*/

    protected bool m_bDead = false;
    public void Die()
    {

    }

    public bool IsDead()
    {
        return m_bDead;
    }

    // [poppin youhua]
    public static void DestroyTarget(SpitBallTarget target)
    {
        //GameObject.Destroy ( target.gameObject );
        if (target)
        {
            target.SetVisible(false);
        }
    }


    SpriteRenderer m_sr;
    void Awake()
    {
        Init();

        m_sr = this.gameObject.GetComponent<SpriteRenderer>();


    }

    void Start()
    {

    }

    public bool IsMine()
    {
        return _Player.photonView.isMine;
    }

    public void GenerateMeshShape(int nSkinId, Color color)
    {
        if (m_msMain.meshRenderer == null)
        {
            return;
        }

        if (nSkinId == AccountData.INVALID_SKIN_ID)
        {
            m_msMain.meshRenderer.material = ResourceManager.s_Instance.GetShellMaterial();
            m_msMain.SetColor(color);
        }
        else
        {
            m_msMain.meshRenderer.material = ResourceManager.s_Instance.GetSkinMaterial(nSkinId);
        }
        m_msMain.drawPolygon();

        if (m_msStaticShell.meshRenderer != null && m_msDynamicShell.meshRenderer != null)
        {
            m_msStaticShell.meshRenderer.material = ResourceManager.s_Instance.GetShellMaterial();
            m_msStaticShell.SetColor(color);
            m_msDynamicShell.meshRenderer.material = ResourceManager.s_Instance.GetShellMaterial();
            m_msStaticShell.drawPolygon();
            m_msDynamicShell.SetColor(Color.white);
            m_msDynamicShell.gameObject.SetActive(false);
            m_msDynamicShell.drawPolygon();
            m_msStaticShell.meshRenderer.sortingOrder = 1;
            m_msDynamicShell.meshRenderer.sortingOrder = 2;
            m_msStaticShell.meshRenderer.sortingLayerName = "BallSkin";
            m_msDynamicShell.meshRenderer.sortingLayerName = "BallSkin";
        }

        _effectUnfoldQianYao.meshRenderer.material = ResourceManager.s_Instance.GetShellMaterial();
        _effectUnfoldQianYao.gameObject.SetActive(false);


        m_msMain.meshRenderer.sortingOrder = 0;

        m_msMain.meshRenderer.sortingLayerName = "BallSkin";

        _sortingGroup.sortingLayerName = "BallSkinGroup";
    }

    public void SetPlayerIns(CPlayerIns playerIns)
    {
        _PlayerIns = playerIns;
    }

    public void SetPlayer(Player player)
    {
        _Player = player;

        if (_Player != null)
        {
            _effectPlayerLevelUp._sprMain.color = _Player.GetColor();
        }



        if (_Player != null && !_Player.IsMainPlayer())
        {
            _Mouth.layer = (int)CLayerManager.eLayerId.other_player;
            this.gameObject.layer = (int)CLayerManager.eLayerId.other_player;

            _ColliderDust.enabled = false;
        }
        else
        {
            _ColliderDust.enabled = true;
        }

    }


    public void Init()
    {
        if (_shell)
        {
            s_vecShellInitScale = _shell.transform.localScale;
        }
        SetShellEnable(false);

        /*
        if (_player)
        {
            _Player = _player.GetComponent<Player>();
			if (!photonView.isMine) {
				_Player.AddOneBall (this);

			}

            if (photonView.isMine)
            {
                DarkForest.s_Instance.AddBall(this);
            }

            MiniMap.s_Instance.AddBall(this, photonView.isMine);

        }
	*/
        /*
                Transform transThorn = this.gameObject.transform.Find ("Thorn"); 
                if (transThorn) {
                    _thorn = transThorn.gameObject;
                    SpriteRenderer sr = _thorn.GetComponent<SpriteRenderer> ();
                    if (_Player)
                    {
                        sr.color = _Player._color_poison;
                    }
                    transThorn.localScale = Vector3.zero;
                } else {
                    Debug.LogError ( "transThorn empty" );
                    return;
                }
        */

        /*
                Transform transMouth = this.gameObject.transform.Find ("Mouth"); 
                if (transMouth) {
                    _Mouth = transMouth.gameObject;
                    _srMouth = _Mouth.GetComponent<SpriteRenderer> ();
                    if (photonView) {
                        _srMouth.sprite = Main.s_Instance.GetSpriteByPhotonOwnerId (photonView.ownerId);
                    } else {
                        _srMouth.sprite = Main.s_Instance.GetSpriteByPhotonOwnerId (3);
                    }
                    if (_Player)
                    {
                       // _srMouth.color = _Player._color_inner;  // 用贴图了，颜色机制暂时废弃
                    }
                } else {
                    Debug.LogError ( "transMouth empty" );
                    return;
                }
        */

        /*
		GameObject goObsceneMask = this.gameObject.transform.Find ( "ObsceneCircle" ).gameObject;
		_obsceneCircle = goObsceneMask.GetComponent<ObsceneMaskShader> ();
		_obsceneCircle.gameObject.SetActive ( false );
		m_vecObsCircleInitScale = _obsceneCircle.gameObject.transform.localScale;
        if (AccountManager.m_eSceneMode == AccountManager.eSceneMode.Game)
        {
       
        }
        else
        {
            goObsceneMask.SetActive( false );
        }
		_ba = this.gameObject.GetComponent<BallAction> ();

		go = this.gameObject.transform.Find ( "Text" ).gameObject;
		_text = go.GetComponent<TextMesh> ();

		*/

        InitMeshShape();



    }

    void InitMeshShape()
    {
        if (m_msMain) {
            m_msMain.SetCoreParams(ResourceManager.s_Instance.m_Perlin,
                false,
                null,
                ResourceManager.NUM_OF_SEGMENTS,
                3.0f,
                0.0f,
                360,
                0,
                0,
                0.3f
            );

        }

        if (m_msStaticShell) {
            m_msStaticShell.SetCoreParams(ResourceManager.s_Instance.m_Perlin,
                true,
                Color.black,
                ResourceManager.NUM_OF_SEGMENTS,
                3.0f,
                2.8f,
                360,
                360,
                0,
                0.3f
            );
        }

        if (m_msDynamicShell) {
            m_msDynamicShell.SetCoreParams(ResourceManager.s_Instance.m_Perlin,
                true,
                Color.white,
                ResourceManager.NUM_OF_SEGMENTS,
                3.0f,
                2.8f,
                360,
                360,
                0,
                0.3f
            );

        }
        if (_effectUnfoldQianYao) {
            _effectUnfoldQianYao.SetCoreParams(ResourceManager.s_Instance.m_Perlin,
                true,
                Color.white,
                ResourceManager.NUM_OF_SEGMENTS,
                3.0f,
                2.8f,
                360,
                0,
                0,
                0.3f
            );
        }

    }

    bool m_bMatInit = false;
    public void SetSprite(int nSpriteId)
    {
        int nSpriteIndex = nSpriteId % Main.s_Instance.m_aryBallSprite.Length;
        _srMouth.sprite = Main.s_Instance.m_aryBallSprite[nSpriteIndex];
    }

    public void SetSprite(Sprite sprite)
    {
        _srMouth.sprite = sprite;
    }

    public void SetMainSprVisible(bool bVisible)
    {
        //_srMouth.enabled = bVisible;
    }

    public void SetSkeletonVisible(bool bVisible)
    {
        // _skeletonAnimatnion.gameObject.SetActive(bVisible);
    }


    public void SetColor(Color color)
    {
        _srMouth.color = color;
    }

    public void SetShellEnable(bool val)
    {
        if (_shell)
        {
            _shell.gameObject.SetActive(val);
        }
        _Collider.enabled = val;
    }


    List<Ball> m_lstIgnoredCollisionBalls = new List<Ball>();
    public void RecoverCollision()
    {
        return;

        for (int i = 0; i < m_lstIgnoredCollisionBalls.Count; i++)
        {
            Ball ball = m_lstIgnoredCollisionBalls[i];
            Physics2D.IgnoreCollision(this._Trigger, ball._Trigger, false);
        }
        m_lstIgnoredCollisionBalls.Clear();
    }

    public void IgnoreCollision(Ball ball)
    {
        Physics2D.IgnoreCollision(this._Trigger, ball._Trigger);
        m_lstIgnoredCollisionBalls.Add(ball);
    }

    // 上面的Init()是初生Init，由于我们是内存池机制，球球删除时并不真正销毁，待会儿会拿出来复用，所以再专门设置一个重生的Init()
    public void RebornInit()
    {
        m_bExploding = false;
        m_bIsMovingToDest = false;
        m_fShellShrinkCurTime = 01f;
        SetShellEnable(false);
        Local_EndShell();
        SetSelfTriggerEnable(false);
        m_nMovingToCenterStatus = 0;
        vecTempScale.x = 1f;
        vecTempScale.y = 1f;
        vecTempScale.z = 1f;
        _Mouth.transform.localScale = vecTempScale;
        m_nUnfoldStatus = 0;

        SetDir(Vector2.zero);
        m_vDir = Vector2.zero;
        m_vInitSpeed = Vector2.zero;
        m_eEjectingStatus = eEjectMode.none;
        m_bFinishedX = false;
        m_bFinishedY = false;

        m_fMoveSpeedX = 0.0f;
        m_fMoveSpeedY = 0.0f;
        m_vecdirection = Vector2.zero;
        ClearSpitTarget();
        SetDead(false);
        SetEaten(false);


        if (IsMainPlayer())
        {
            _ColliderDust.enabled = true;
        }
        else
        {
            _ColliderDust.enabled = false;
        }


        GetCurPlayerSkillStatus();

        SetRingVisible(true);

        m_nMouthUnfoldStatus = 0;
        SetMouthScale(1f);

       
        //// 最近新加的
        _skillModule.ResetAllSkillStatus();
        SetPicked( false );
        m_bFirstCalculateMoveInfo = true;
        _rigid.bodyType = RigidbodyType2D.Dynamic;
        _skillModule.enabled = true;
        EndMoveToDestination();
        ////
    }

   

    public void GetCurPlayerSkillStatus()
    {
		/*

        SetMergeAll(_Player.IsMergeAll());
      //  SetSneak(_Player.IsSneaking());
        SetAnnihilate(_Player.IsAnnihilaing());
        SetMagicShielding(_Player.IsMagicShielding());
        SetHenzy(_Player.IsHenzy());
        SetGold(_Player.IsGold());
        SetBecomeThorn(_Player.IsBecomeThorn(), _Player.IsBecomeThorn_QianYao());
        CSpineManager.s_Instance.StopBallAnimation(this, 0);
		*/
    }

    int[] m_aryEffectStatus = new int[10];
    public void SetEffectStatus(int nSkillId, int nStatus)
    {
        m_aryEffectStatus[nSkillId] = nStatus;
    }

    public int GetEffectStatus(int nSkillId)
    {
        return m_aryEffectStatus[nSkillId];
    }

    byte m_byChiXuEffectIndex = 0;
    public void EffectLoop(CSkillSystem.eSkillId eSkillId)
    {
        switch (eSkillId)
        {

            case CSkillSystem.eSkillId.o_fenzy: // 狂暴
                {
                    int nStatus = m_aryEffectStatus[(int)CSkillSystem.eSkillId.o_fenzy];
                    if (nStatus == 1)
                    {
                        if (_effectKuangBao_QianYao.isEnd())
                        {
                            _effectKuangBao_Up.gameObject.SetActive(true);
                            _effectKuangBao_Up.BeginPlay(true);
                            // _effectKuangBao_Down.gameObject.SetActive(true);
                            // _effectKuangBao_Down.BeginPlay(true);
                            SetEffectStatus((int)CSkillSystem.eSkillId.o_fenzy, 2);
                        }
                    }

                }
                break;
            /*
        case CSkillSystem.eSkillId.p_gold: // 金壳
            {
                int nSkillId = (int)CSkillSystem.eSkillId.p_gold;
                int nStatus = m_aryEffectStatus[nSkillId];
                if (nStatus == 1)
                {
                    CFrameAnimationEffect effectGoldQianZou = m_aryEffectQianZou[nSkillId];
                    if (effectGoldQianZou.isEnd())
                    {
                        CFrameAnimationEffect effectGoldChiXu = m_aryEffectChiXu[nSkillId];
                        effectGoldChiXu.gameObject.SetActive(true);
                        effectGoldChiXu.BeginPlay(true);
                        SetEffectStatus((int)CSkillSystem.eSkillId.p_gold, 2);
                    }
                }
            }
            break;
            */

            case CSkillSystem.eSkillId.p_gold: // 金壳
            case CSkillSystem.eSkillId.y_annihilate: // 湮灭
            case CSkillSystem.eSkillId.u_magicshield: // 魔盾
            case CSkillSystem.eSkillId.r_sneak: // 潜行
                {
                    int nSkillId = (int)eSkillId;
                    int nStatus = m_aryEffectStatus[nSkillId];
                    if (nStatus == 1) // 前奏期
                    {
                        CFrameAnimationEffect effecQianZou = m_aryEffectQianZou[nSkillId];
                        if (effecQianZou.isEnd())
                        {
                            CFrameAnimationEffect effecChiXu = m_aryEffectChiXu[nSkillId];
                            effecChiXu.gameObject.SetActive(true);
                            effecChiXu.BeginPlay(true);
                            SetEffectStatus((int)eSkillId, 2);
                            m_byChiXuEffectIndex = 0;
                            effecChiXu.SetLoopTimes(0);
                        }
                    }
                    else if (nStatus == 2) // 循环期
                    {
                        if (eSkillId == CSkillSystem.eSkillId.u_magicshield) // 时不时来点高光
                        {
                            if (m_byChiXuEffectIndex == 3)
                            {
                                CFrameAnimationEffect effecChiXu0 = m_aryEffectChiXu[nSkillId];
                                effecChiXu0.gameObject.SetActive(false);
                                CFrameAnimationEffect effecChiXu1 = m_aryEffectChiXu1[nSkillId];
                                effecChiXu1.gameObject.SetActive(true);
                                effecChiXu1.SetLoopTimes(0);
                                effecChiXu1.BeginPlay(true);
                                m_byChiXuEffectIndex = 4;
                            }

                            if (m_byChiXuEffectIndex == 4)
                            {
                                CFrameAnimationEffect effecChiXu1 = m_aryEffectChiXu1[nSkillId];
                                if (effecChiXu1.GetLoopTimes() >= 1)
                                {
                                    CFrameAnimationEffect effecChiXu0 = m_aryEffectChiXu[nSkillId];
                                    effecChiXu0.gameObject.SetActive(true);
                                    effecChiXu0.BeginPlay(true);
                                    effecChiXu0.SetLoopTimes(0);
                                    effecChiXu1.gameObject.SetActive(false);
                                    m_byChiXuEffectIndex = 0;
                                }
                            }

                            if (m_byChiXuEffectIndex < 3)
                            {
                                CFrameAnimationEffect effecChiXu = m_aryEffectChiXu[nSkillId];
                                if (effecChiXu.GetLoopTimes() >= 1)
                                {
                                    effecChiXu.SetLoopTimes(0);
                                    m_byChiXuEffectIndex++;
                                }
                            }
                        }
                    }
                }
                break;


        }
    }

    // [to youhua] 很多操作都不需要每帧执行，优化一下，看看能不能提一些内容出来在其它不那么频繁的场合执行
    // Update is called once per frame
    public virtual void Update()
    {
        if (!GetActive())
        {
            return;
        }

        if (Main.s_Instance.IsGameOver())
        {
            return;
        }

         Shell();
        
        ChangeSizeLoop();        

              

        UpdateDickDir();


        TempShellLOop();




       

        if (IsMainPlayer())
        {
            BlingDust();
            Staying();
        }
        else
        {

        }
  
    }
    MeshRenderer _shitRender = null;
    bool m_bBallSkeletonMaterialSet = false;

    public void UpdateDickDir()
    {
        /*
        if (_goDick == null)
        {
            return;
        }

        float fDirX = 0f;
        float fDirY = 0f;
        if (_Player.IsMainPlayer())
        {

            fDirX = _dirx;
            fDirY = _diry;


        }
        else
        {
            fDirX = m_vecdirection.x;
            fDirY = m_vecdirection.y;
        }

        float fAngle = 0f;
        if (fDirX == 0)
        {
            if (fDirY > 0)
            {
                fAngle = 90f;
            }
            else if (fDirY < 0)
            {
                fAngle = -90f;
            }
        }
        else
        {
            float fTan = fDirY / fDirX;
            fAngle = Mathf.Atan(fTan) / CyberTreeMath.c_fAngleToRadian;
            if (fDirX < 0)
            {

                fAngle = 180f + fAngle;

            }
        }

        _goDick.transform.localRotation = Quaternion.identity;
        _goDick.transform.Rotate(0.0f, 0.0f, fAngle);
        */
    }

    byte m_byPlayerLevel = 1;
    public void SetPlayerName(string szPlayerName)
    {
        _txtPlayerName.text = szPlayerName + " lv." + m_byPlayerLevel;
      //  _txtPlayerNameMiaoBian.text = _txtPlayerName.text;
    }

    public void SetPlayerNameVisible(bool bVisible)
    {
        _goPlayerName.SetActive(bVisible);
        bPlayerNameVisible = bVisible;
    }

    public bool GetPlayerNameVisible()
    {
        return bPlayerNameVisible;
    }

    public void SetPlayerLevel(int nPlayerLevel)
    {
        m_byPlayerLevel = (byte)nPlayerLevel;
    }

    void FixedUpdate()
    {
        if (!GetActive())
        {
            return;
        }

         Move();
        MovingToDest();

        MouthUnfoldLoop_Up();
        MouthUnfoldLoop_Down();
        Ejecting_BySpeed();

        // poppin test
        InterpolateLoop();
    }

    bool m_bNeedSyncSize = false;
    float m_fLastSyncSizeTime = 0f;
    public void SetSize(float val)
    {
        Local_SetSize(val);
    }

    [PunRPC]
    public void RPC_SetSize(float val)
    {
        Local_SetSize(val);
    }

    float m_fDestSize = 0f;
    float m_fSizeUpdateSpeed = 0f;

    static int s_nShitSortOrderIndex = 0;
    static float[] s_aryShitSortOrder =
    {
        0.001f,
        0.002f,
        0.003f,
        0.004f,
        0.005f,
        0.006f,
        0.007f,
        0.008f,
        0.009f,
    };

    /*
    public void Local_SetSize(float val)
    {
        if ( GetSize() == val ) // 不要重复设置size，会有冗余运算量
        {
            return;
        }

        vecTempSize.x = val;
        vecTempSize.y = val;
        vecTempSize.z = 1.0f;
        this.transform.localScale = vecTempSize;

        // 遮挡是由z坐标值决定。
        vecTempPos = GetPos();
        if (s_nShitSortOrderIndex >= s_aryShitSortOrder.Length)
        {
            s_nShitSortOrderIndex = 0;
        }
        vecTempPos.z = -val + s_aryShitSortOrder[s_nShitSortOrderIndex++];
        SetPos(vecTempPos);

        BallSizeChange();

       
    }
    */
    public void Local_SetSize(float val)
    {
        if (GetSize() == val) // 不要重复设置size，会有冗余运算量
        {
            return;
        }

        /*
        vecTempSize.x = val;
        vecTempSize.y = val;
        vecTempSize.z = 1.0f;
        this.transform.localScale = vecTempSize;
        */
        m_fSize = val;
        m_fRadius = val * 0.5f;

        m_bySizeChangeOp = m_fSize > GetVisionSize() ? 1 : -1;

        // 遮挡是由z坐标值决定。
        vecTempPos = GetPos();
        if (s_nShitSortOrderIndex >= s_aryShitSortOrder.Length)
        {
            s_nShitSortOrderIndex = 0;
        }
        vecTempPos.z = -m_fSize + s_aryShitSortOrder[s_nShitSortOrderIndex++];
        SetPos(vecTempPos);
    }

    void ChangeSizeLoop()
    {
        if (!GetActive()) // 没显示（死亡或者被视野裁剪）时不作这项运算
        {
            return;
        }

        if (GetVisionSize() == m_fSize)
        {
            return;
        }

        vecTempSize.x = GetVisionSize();
        vecTempSize.y = GetVisionSize();
        float fDelta = m_fSize - vecTempSize.x;
        float fSpeed = fDelta * Main.s_Instance.m_fSizeChangeSpeed * Time.deltaTime;
        vecTempSize.x += fSpeed;
        vecTempSize.y += fSpeed;
        if (fDelta > 0)
        {
            if (vecTempSize.x >= m_fSize)
            {
                vecTempSize.x = m_fSize;
                vecTempSize.y = m_fSize;
            }
        }
        else if (fDelta < 0)
        {
            if (vecTempSize.x <= m_fSize)
            {
                vecTempSize.x = m_fSize;
                vecTempSize.y = m_fSize;
            }
        }


        this.transform.localScale = vecTempSize;
       
        BallSizeChange();
    }

    public void SetSizeDirectly()
    {
        vecTempSize.x = m_fSize;
        vecTempSize.y = m_fSize;
        vecTempSize.z = 1;
        this.transform.localScale = vecTempSize;
    }

    void UpdateSize()
    {
        return;

        if (m_fSizeUpdateSpeed == 0f)
        {
            return;
        }

        float fCurSize = GetRealTimeScale();
        fCurSize += m_fSizeUpdateSpeed * Time.deltaTime;
        if (m_fSizeUpdateSpeed > 0 && fCurSize >= m_fDestSize)
        {
            fCurSize = m_fDestSize;
            m_fSizeUpdateSpeed = 0;
        }
        if (m_fSizeUpdateSpeed < 0 && fCurSize <= m_fDestSize)
        {
            fCurSize = m_fDestSize;
            m_fSizeUpdateSpeed = 0;
        }

        Local_Directly_SetSize(fCurSize);
    }

    public void Local_Directly_SetSize(float val)
    {
        vecTempSize.x = val;
        vecTempSize.y = val;
        vecTempSize.z = 1.0f;
        this.transform.localScale = vecTempSize;

        // 遮挡是由z坐标值决定。
        vecTempPos = GetPos();
        vecTempPos.z = -val;
        SetPos(vecTempPos);

    }


    float m_fSizeKaiGenHao = 0.0f;
    float m_fVolume = 0f;
    float m_fRadius = 0f;

    // [to youhua]这里要作一个性能优化.感觉运算有点多
    public void BallSizeChange()
    {
        float fSize = this.transform.localScale.x;

        CalculateMassBySize(fSize);

        int nSortOrderId = (int)(m_fRadius * 100f);
        SetSortingOrder(nSortOrderId);
    }

    public void SetSortingOrder(int nSortingOrder)
    {
        _sortingGroup.sortingOrder = nSortingOrder;

        m_msMain.SetSortingOrder(0);
        m_msStaticShell.SetSortingOrder(1);
        m_msDynamicShell.SetSortingOrder(2);
    }

    public void SetSortingLayer(string szLayer)
    {
        m_msMain.SetSortingLayer(szLayer);
        m_msStaticShell.SetSortingLayer(szLayer);
        m_msDynamicShell.SetSortingLayer(szLayer);
    }

    void UpdateClassStatus()
    {
        if (_Player)
        {
            CheckClassStatus();
            ChangeSizeClassId();
        }
    }

    public float GetVolume()
    {
        if ( IsMainPlayer() )
        {
            return m_fVolume;
        }
        else
        {
            return CyberTreeMath.Scale2Volume(m_fSize, ref m_fRadius); // 好诡异的一段运算, 稍后优化一下[to youhua]
        }
    }

    public float GetVolumeWithoutRealTimeCalculate()
    {
        return m_fVolume;
    }


    public float GetVolumeWithRealtimeCalculate()
    {
        m_fVolume = CyberTreeMath.Scale2Volume(GetSize(), ref m_fRadius);
        return m_fVolume;
    }

    // without base-volume
    public float GetEatVolume()
    {
        //float fEatVolume = m_fVolume - 
        return 0;
    }

    public float GetRadius()
    {
        return m_fRadius;
    }

    public void SetRadius(float fRadius)
    {
        m_fRadius = fRadius;
    }

    public float GetKaiGenHao()
    {
        return m_fSizeKaiGenHao;
    }

    void CalculateMassBySize(float fSize)
    {
        if (_rigid == null) {
            return;
        }
        _rigid.mass = 1;
    }


    public float GetTriggerSize()
    {
        return _Trigger.bounds.size.x;
    }

    float m_fSize = 0;
    int m_bySizeChangeOp = 0;
    public float GetSize()
    {
        //return this.transform.localScale.x;
        return m_fSize;
    }

    public float GetVisionSize()
    {
        return this.transform.localScale.x;
    }

    public float GetRealTimeScale()
    {
        return this.transform.localScale.x;
    }

    public bool IsEjecting()
    {
        return m_bIsEjecting;
    }

    public void SlowDownEject()
    {
        if (IsEjecting())
        {
            EndEject();
        }
    }

    Vector2 m_vecEjectSpeed = new Vector2();
    Vector2 m_vecEjectStartPos = new Vector2();
    float m_fEjectTotalDis = 0f;
    
    bool m_bUseColliderTemp = false;
    float m_fStayTimeWhenEjectEnd = 0f;
    bool m_bIgnoreDust = false;
    bool m_bAccelerating = false;
    Vector2 m_vecA = new Vector2();
    Vector2 m_vecAccelerateSpeed = new Vector2();
    Vector2 m_vecV0 = new Vector2();
    Vector2 m_vecMotherMoveDir = new Vector2();
    float m_fMotherRealTimeSpeed = 0f;
    bool m_bAffectByMotherMove = false;
    public void SetMotherStatus( Vector2 dir, float fMotherRealTimeSpeed)
    {
        m_vecMotherMoveDir = dir;
        m_fMotherRealTimeSpeed = fMotherRealTimeSpeed;
    }

    public void BeginEject_BySpeed(float fDistance, float fSpeed, float fStayTime = 0f, bool bIgnoreDust = false, bool bAffectByMotherMove = false )
    {
        m_bAffectByMotherMove = bAffectByMotherMove;

        // 预测母球在射球期间将会在射球方向作多少位移（注意，由于是双摇杆机制，母球的运行方向和射球方向没有必然联系）
        float fDisOnEject = 0f;
        float fAngle = 0;
        if (m_bAffectByMotherMove)
        {
            Vector2.Angle(m_vecMotherMoveDir, GetDir());
            if (fAngle >= 0 && fAngle < 90)
            {
                float fSpeedOnEjectDir = CyberTreeMath.CosByAngle(fAngle) * m_fMotherRealTimeSpeed;
                float fMainEjectTime = fDistance / fSpeed;
                fDisOnEject = fSpeedOnEjectDir * (Main.s_Instance.m_fEjectHuanTing + fMainEjectTime);
            }
            else
            {

                fDisOnEject = 0f;
            }
        }
        else
        {
            fDisOnEject = 0f;
        }
        

        m_vecEjectSpeed.x = GetDir().x * fSpeed;
        m_vecEjectSpeed.y = GetDir().y * fSpeed;
        m_vecEjectStartPos = GetPos();
        m_fEjectTotalDis = fDistance;
        m_bIsEjecting = true;
        m_fStayTimeWhenEjectEnd = fStayTime;
        m_bAccelerating = false;
        float s = GetRadius() + fDisOnEject;
        float fA = CyberTreeMath.GetA( s, Main.s_Instance.m_fEjectHuanTing ); // CyberTreeMath.Get_A_By_V0_And_t(s, fSpeed, 0.5f);
        float v0 = CyberTreeMath.GetV0(s, Main.s_Instance.m_fEjectHuanTing);
         

        m_vecV0.x = v0 * GetDir().x;
        m_vecV0.y = v0 * GetDir().y;
        m_vecA.x = fA * GetDir().x;
        m_vecA.y = fA * GetDir().y;
        /*
        float fV0 = CyberTreeMath.GetV0(s, 0.5f);
        m_vecAccelerateSpeed.x = fV0 * GetDir().x;
        m_vecAccelerateSpeed.y = fV0 * GetDir().y;
        */
        /*
        m_bIgnoreDust = bIgnoreDust;
        if (bIgnoreDust)
        {
            _ColliderDust.enabled = false;
        }
        else
        {
            
        }
        */


    }

    public enum eEjectMode
    {
        none,
        uniform,            // 匀速运动
        accelerated,          // 加速运动
    };

    bool m_bFinishedX = false;
    bool m_bFinishedY = false;
    eEjectMode m_eEjectingStatus = eEjectMode.none;
    public void BeginEject(float s, float t, Vector2 dir, eEjectMode eStatus, float fStayTime = 0f, bool bIgnoreDust = false)
    {
        m_vDir = dir;
        m_eEjectingStatus = eStatus;
        float v0 = CyberTreeMath.GetV0(s, t);
        float a = CyberTreeMath.GetA(s, t);
        m_vSpeed.x = dir.x * v0;
        m_vSpeed.y = dir.y * v0;
        m_vInitSpeed = m_vSpeed;
        m_vAcclerate.x = dir.x * a;
        m_vAcclerate.y = dir.y * a;
        m_bFinishedX = false;
        m_bFinishedY = false;

        m_fStayTimeWhenEjectEnd = fStayTime;

        m_bIgnoreDust = bIgnoreDust;
        if (bIgnoreDust)
        {
            _ColliderDust.enabled = false;
        }
        else
        {

        }
    }

    void Ejecting_Accelerate()
    {
        if (m_eEjectingStatus != eEjectMode.accelerated)
        {
            return;
        }

        vecTempPos = GetPos();

        m_vSpeed.x += m_vAcclerate.x * Time.deltaTime;
        m_vSpeed.y += m_vAcclerate.y * Time.deltaTime;


        if (m_vDir.x > 0)
        {
            if (m_vSpeed.x > 0)
            {
                vecTempPos.x += m_vSpeed.x * Time.deltaTime;
            }
            else
            {
                m_bFinishedX = true;
            }
        }
        else if (m_vDir.x < 0)
        {
            if (m_vSpeed.x < 0)
            {
                vecTempPos.x += m_vSpeed.x * Time.deltaTime;
            }
            else
            {
                m_bFinishedX = true;
            }
        }
        else
        {
            m_bFinishedX = true;
        }

        if (m_vDir.y > 0)
        {
            if (m_vSpeed.y > 0)
            {
                vecTempPos.y += m_vSpeed.y * Time.deltaTime;
            }
            else
            {
                m_bFinishedY = true;
            }
        }
        else if (m_vDir.y < 0)
        {
            if (m_vSpeed.y < 0)
            {
                vecTempPos.y += m_vSpeed.y * Time.deltaTime;
            }
            else
            {
                m_bFinishedY = true;
            }
        }
        else
        {
            m_bFinishedY = true;
        }

        if (m_bFinishedX && m_bFinishedY)
        {
            EndEject();
            return;
        }

        SetPos(vecTempPos);

    }


    void Ejecting_BySpeed()
    {
        if ( !IsMainPlayer() )
        {
            return;
        }

        if ( !m_bIsEjecting)
        {
            return;
        }

        float fDeltaX = 0f;// Time.deltaTime * m_vecEjectSpeed.x;
        float fDeltaY = 0f;// Time.deltaTime * m_vecEjectSpeed.y;


        vecTempPos = GetPos();

        float fCurDis = 0;
        if (!m_bAccelerating)
        {
            fCurDis = Vector2.Distance(vecTempPos, m_vecEjectStartPos);
            if ((fCurDis >= m_fEjectTotalDis))
            {
                //EndEject_BySpeed();   // 达到目标点后先不结束，再减速滑行自身一个半径的距离再停下
                m_bAccelerating = true;
            }
        }

        float fHuanTingEndSpeed = 0;
        if ( m_bAffectByMotherMove )
        {
            fHuanTingEndSpeed = m_fMotherRealTimeSpeed;
        }

        if (m_bAccelerating)
        {
            m_vecV0 += Time.fixedDeltaTime * m_vecA;
            m_vecEjectSpeed = m_vecV0;
           // m_vecEjectSpeed.x += Time.fixedDeltaTime * m_vecA.x;
           // m_vecEjectSpeed.y += Time.fixedDeltaTime * m_vecA.y;
            bool bCompletedX = false;
            bool bCompletedY = false;
            if (m_vecA.x > 0)
            {
                if (m_vecEjectSpeed.x >= 0)
                {
                    bCompletedX = true;
                }
            }
            else if (m_vecA.x < 0)
            {
                if (m_vecEjectSpeed.x <= 0)
                {
                    bCompletedX = true;
                }
            }
            else
            {
                bCompletedX = true;
            }

            if (m_vecA.y > 0)
            {
                if (m_vecEjectSpeed.y >= 0)
                {
                    bCompletedY = true;
                }
            }
            else if (m_vecA.y < 0)
            {
                if (m_vecEjectSpeed.y <= 0)
                {
                    bCompletedY = true;
                }
            }
            else
            {
                bCompletedY = true;
            }

            if (bCompletedX && bCompletedY)
            {
                EndEject_BySpeed();
                return;
            }
        }
        
        fDeltaX = Time.fixedDeltaTime * m_vecEjectSpeed.x;
        fDeltaY = Time.fixedDeltaTime * m_vecEjectSpeed.y;
        

        bool bCanMoveX = true;
        bool bCanMoveY = true;
        MapEditor.s_Instance.CheckIfWillExceedWorldBorder(this, fDeltaX, fDeltaY, ref bCanMoveX, ref bCanMoveY);

        if (bCanMoveX)
        {
            vecTempPos.x += fDeltaX;
        }

        if (bCanMoveY)
        {
            vecTempPos.y += fDeltaY;
        }

        if (!bCanMoveX || !bCanMoveY)
        {
            EndEject_BySpeed();
            return;
        }

        SetPos(vecTempPos);
    }

    public bool IsIgnoreDust()
    {
        return m_bIgnoreDust;
    }

    public void EndEject_BySpeed()
    {
        if ( !IsMainPlayer() )
        {
            return;
        }

        m_bIsEjecting = false;

        SetRingVisible( true );

        if (_Player.IsMainPlayer())
        {
            SetSelfTriggerEnable(true);


        }
        else
        {
            
        }

        if (_Player.IsMainPlayer())
        {
            if (_Player.IsSneaking())
            {

            }
            else
            {
                _ColliderDust.enabled = true;
            }
        }

        m_bIgnoreDust = false;
        Local_BeginShell();
        if (m_fStayTimeWhenEjectEnd > 0f)
        {
            BeginStay();
        }
        else
        {
            
          
        }

        CSpineManager.s_Instance.SetBallAnimationRotation(this, 0);
    }

    float m_fInitSpeed = 0f;
    float m_fAccelerate = 0f;

    public float GetCurEjectSpeed()
    {
        return m_fInitSpeed;
    }

    public void SetEjectSpeed(float val)
    {
        m_fInitSpeed = val;
    }

    public bool CheckNeedCollider()
    {
        if (!m_bShell)
        {
            return false;
        }

        if (Main.s_Instance.m_fShellShrinkTotalTime == 0f)
        {
            return false;
        }

        return true;
    }

    public void EndEject()
    {
        m_eEjectingStatus = eEjectMode.none;
        m_bIsEjecting = false;

        if (IsMainPlayer())
        {
            SetSelfTriggerEnable(true);


        }
        else
        {

        }

        if (IsMainPlayer())
        {
            if (_Player.IsSneaking()) 
            {

            }
            else
            {
                _ColliderDust.enabled = true;
            }
        }

        m_bIgnoreDust = false;

        Local_BeginShell();
        if (m_fStayTimeWhenEjectEnd > 0f)
        {
            BeginStay();
        }
        else
        {

           
        }


        /*
        m_bIsEjecting = false;


        if (m_bUseColliderTemp)
        {
            //_ColliderTemp.enabled = true;
            m_fColliderTempTime = 0.5f;
        }

        if (m_bShell)
        {

        }

        if (m_fStayTimeWhenEjectEnd > 0f)
        {
            BeginStay();
        }
        else
        {
            Local_BeginShell();
        }

        CSpineManager.s_Instance.SetBallAnimationRotation(this, 0);
        */
    }

    bool m_bStaying = false;
    public void SetStayTime(float val)
    {
        m_fStayTimeWhenEjectEnd = val;
    }

    public void BeginStay()
    {
        m_bStaying = true;
    }

    void Staying()
    {
        if (IsDead())
        {
            return;
        }

        if (m_bStaying == false)
        {
            return;
        }

        m_fStayTimeWhenEjectEnd -= Time.deltaTime;
        if (m_fStayTimeWhenEjectEnd <= 0f)
        {
            EndStay();
        }
    }

    public bool IsStaying()
    {
        return m_bStaying;
    }

    public void EndStay()
    {
        m_bStaying = false;
        if (m_fShellTotalTime > 0)
        {
            
        }
    }




    public void SetDirLineVisible(bool bVisible)
    {
        if (_ba == null)
        {
            return;
        }

        _ba._dir_line.gameObject.SetActive(bVisible);
    }

    int m_nUnfoldStatus = 0;

    /// <summary>
    /// /  技能：变刺
    /// </summary>



    bool m_bPreunfolding = false;
    public void BeginPreUnfold(float fScale)
    {
        if (!IsMainPlayer() && IsDynamicShellVisible())
        {
            return;
        }

        m_bPreunfolding = true;

        _goEffectContainer_QianYao.SetActive(true);
        _effectUnfoldQianYao.gameObject.SetActive(true);
        vecTempScale.x = fScale;
        vecTempScale.y = fScale;
        vecTempScale.z = 1f;
        _goEffectContainer_QianYao.transform.localScale = vecTempScale;
        _effectUnfoldQianYao.SetAngle1(0);
        _effectUnfoldQianYao.drawPolygon();

    }

    void PreUnfolding()
    {
        /*
        if (m_nUnfoldStatus != 1 && m_nUnfoldStatus != 2 )
        {
            return;
        }

        if (IsDead())
        {
            return;
        }

        m_fPreUnfoldTimeCount += Time.deltaTime;

        if (m_fPreUnfoldTimeCount >= _Player.GetUnfoldSkillParam_PreUnfoldTime() - 0.2f) // 播放骨骼动画
        {
            if (m_nUnfoldStatus == 1)
            {
                CSpineManager.s_Instance.PlayBallAnimation(this, CSpineManager.eBallSpineType.unfold, false, 2f);
                m_nUnfoldStatus = 2;
            }
            
        }
    
        if (m_fPreUnfoldTimeCount >= _Player.GetUnfoldSkillParam_PreUnfoldTime())
        {
            _obsceneCircle.gameObject.SetActive(false);
            vecTempScale = _obsceneCircle.transform.localScale;
            vecTempScale.x = 1f;
            vecTempScale.y = 1f;
            vecTempScale.z = 1f;
            _obsceneCircle.transform.localScale = vecTempScale;
            BeginUnfold();
        }
        */
    }

    float m_fUnfoldLeftTime = 0.0f;

    public bool IsUnfolding()
    {
        return (m_nUnfoldStatus == 3);
    }



    float m_fSizeBeforeUnfold = 0;
    public void BeginUnfold(float fScale)
    {
        //        m_nUnfoldStatus = 3;
        //        m_fUnfoldLeftTime = _Player.GetUnfoldSkillParam_UnfoldKeepTim();



        vecTempSize.x = fScale;
        vecTempSize.y = fScale;
        vecTempSize.z = 1.0f;

        // poppin test for unfold
        //_Mouth.transform.localScale = vecTempSize;
        BeginMouthUnfold(fScale);

        // CSpineManager.s_Instance.PlayBallAnimation(this, CSpineManager.eBallSpineType.unfold, false, 2f);
    }

    float m_fMouthUnfoldScale = 0;
    int m_nMouthUnfoldStatus = 0;
    public void BeginMouthUnfold(float fScale)
    {
        m_fMouthUnfoldScale = fScale;
        m_nMouthUnfoldStatus = 1;
    }

    void MouthUnfoldLoop_Up()
    {
        if (m_nMouthUnfoldStatus != 1)
        {
            return;
        }

        float fCurMouthScale = GetMouthScale();
        if (fCurMouthScale >= m_fMouthUnfoldScale)
        {
            fCurMouthScale = m_fMouthUnfoldScale;
            SetMouthScale(fCurMouthScale);
            return;
        }

        fCurMouthScale += Time.fixedDeltaTime * 5f;
        SetMouthScale(fCurMouthScale);
    }

    public void SetMouthScale(float fScale)
    {
        vecTempScale.x = fScale;
        vecTempScale.y = fScale;
        vecTempScale.z = 1f;
        _Mouth.transform.localScale = vecTempScale;
        _Trigger.transform.localScale = vecTempScale;
    }

    public float GetMouthScale()
    {
        return _Mouth.transform.localScale.x;
    }

    public void EndMouthUnfold()
    {
        m_nMouthUnfoldStatus = 2;
    }

    void MouthUnfoldLoop_Down()
    {
        if (m_nMouthUnfoldStatus != 2)
        {
            return;
        }

        float fCurMouthScale = GetMouthScale();
        if (fCurMouthScale <= 1f)
        {
            fCurMouthScale = 1f;
            SetMouthScale(fCurMouthScale);
            return;
        }

        fCurMouthScale -= Time.fixedDeltaTime * 5f;
        SetMouthScale(fCurMouthScale);
    }


    void Unfolding()
    {
        /*
        if (m_nUnfoldStatus != 3 )
        {
            return;
        }

        if (IsDead())
        {
            return;
        }

        m_fUnfoldLeftTime -= Time.deltaTime;
        if (m_fUnfoldLeftTime <= 0.0f)
        {
            EndUnfold();
        }
        */
    }

    public void EndUnfold()
    {
        SetMouthScale(1);
    }



    public void ProcessPk_Monster(CMonster monster)
    {
        if (this._Player.IsProtect())
        {
            return;
        }

        //[orphan]
        if (!IsMainPlayer() && !isOrphan()) // 只运算自己控制的球球的吃怪情况，不管别的客户端的球的吃怪情况[orphan]
        {
            return;
        }

        // [to youhua] 不要用CheckIfPartiallyCover这个接口，太耗了
        if (GetTriggerSize() > monster.GetTriggerSize())
        {
            if (CheckIfCover(this.GetRadius(), monster.GetRadius(), this.GetPos(), monster.GetPos()))
            {
                this.EatThorn(monster);
            }

        }
 

    }


    public void Ball_Eat_SceneBall(CMonster scene_ball)
    {
        if (scene_ball.m_bIsJueSheng)
        {
            this._Player.photonView.RPC("RPC_EatShengFuPanDingMonster", PhotonTargets.All);
        }

        scene_ball.SetActive(false); // 避免重复吃球

        /*
        float fSceneBallArea = scene_ball.GetConfig().fFoodSize;
        float fCurArea = CyberTreeMath.SizeToArea( GetSize(), Main.s_Instance.m_nShit );
        float fNewArea = fCurArea + fSceneBallArea;
        float fNewSize = CyberTreeMath.AreaToSize(fNewArea, Main.s_Instance.m_nShit);
        _Player.SetBallSize( this.GetIndex(), fNewSize);
        _Player.DestroyMonster(scene_ball);
        */
        float fSceneBallVolume = scene_ball.GetConfig().fFoodSize;
        float fCurVolume = GetVolume();
        float fNewVolume = fSceneBallVolume + fCurVolume;
        float fNewSize = CyberTreeMath.Volume2Scale(fNewVolume);
        Local_SetSize(fNewSize);
        _Player.DestroyMonster(scene_ball);
    }

    List<CMonster> m_lstPkMonsterList = new List<CMonster>();
    public void AddToEatThornList(CMonster monster)
    {
        m_lstPkMonsterList.Add(monster);
        //Debug.Log ("add monster:" + monster.GetGuid ());
    }

    public void RemoveFromEatThornList(CMonster monster)
    {
        for (int i = 0; i < m_lstPkMonsterList.Count; i++) {
            CMonster node_monster = m_lstPkMonsterList[i];
            if (node_monster.GetGuid() == monster.GetGuid()) {
                m_lstPkMonsterList.RemoveAt(i);
                //Debug.Log ("remove monster:" + monster.GetGuid ());
                return;
            }
        }
    }

    List<Ball> m_lstPkListSelf = new List<Ball>(); // 仅针对自己队伍的球
    List<Ball> m_lstPkListOther = new List<Ball>(); // 仅针对别人的球

    public static bool IsTeammate(Ball ball1, Ball ball2)
    {
        if (ball1._Player == null || ball2._Player == null)
        {
            return false;
        }
        return ball1._Player.GetOwnerId() == ball2._Player.GetOwnerId();
    }

    public void AddToPKList(Ball ballOpponent)
    {
        if (IsTeammate(this, ballOpponent))
        {
            // [to youhua]这儿要优化一下，不要重复添加，只要一方PK另一方就好了。
            m_lstPkListSelf.Add(ballOpponent);
        }
        else
        {
            m_lstPkListOther.Add(ballOpponent); // 非本客户端主角的球体没有碰撞检测脚本，不会主动发起PK流程，因此无需检测
        }


    }

    public void RemoveFromPKList(Ball ballOpponent)
    {
        if (IsTeammate(this, ballOpponent)) // 自己队伍的球
        {

            for (int i = 0; i < m_lstPkListSelf.Count; i++)
            {
                Ball opponent = m_lstPkListSelf[i];
                if (opponent == ballOpponent)
                {
                    m_lstPkListSelf.RemoveAt(i);
                    //	Debug.Log( ballOpponent.GetIndex() + "离开self:" + opponent.GetIndex() );
                    return;
                }
            }
        }
        else // 别的玩家的球
        {
            for (int i = 0; i < m_lstPkListOther.Count; i++)
            {
                Ball opponent = m_lstPkListOther[i];
                if (opponent == ballOpponent)
                {
                    m_lstPkListOther.RemoveAt(i);
                    //Debug.Log(opponent + "离开other: " + ballOpponent);
                    return;
                }
            }
        }
    }

    public void PKLoop_Self()
    {
        for (int i = 0; i < m_lstPkListSelf.Count; i++)
        {
            Ball opponent = m_lstPkListSelf[i];
            this.ProcessPK_Self(opponent);
        }
    }

    public void PKLoop_Other()
    {
        for (int i = m_lstPkListOther.Count - 1; i >= 0; i--)
        {
            Ball opponent = m_lstPkListOther[i];
           this.ProcessPK_Other(opponent);
        }
    }

    public void PKLoop_Monster()
    {
        for (int i = 0; i < m_lstPkMonsterList.Count; i++)
        {
            CMonster monster = m_lstPkMonsterList[i];
            this.ProcessPk_Monster(monster);
        }
    }

    public void PKLoop(bool bPkSelfBall)
    {
        /// 自己人
        /*
		if (bPkSelfBall) {
            for (int i = 0; i < m_lstPkListSelf.Count; i++) {
                Ball opponent = m_lstPkListSelf[i];
                this.ProcessPk(opponent);
            }
        }
        */

        //// ！---- 其它玩家

        for (int i = m_lstPkListOther.Count - 1; i >= 0; i--)
        {
            Ball opponent = m_lstPkListOther[i];
            if (opponent._Player == null)
            {
                this.ProcessPK_OrphanBall(opponent); // PK孤儿球
                continue;
            }


            //// 不PK的情况
            if (_Player.IsSneaking() || opponent._Player.IsSneaking())
            {
                continue;
            }

            if (_Player.IsGold() || opponent._Player.IsGold())
            {
                opponent.Local_Begin_Shell_Temp();
                this.Local_Begin_Shell_Temp();
                continue;
            }

            //// end 不PK的情况

            this.ProcessPk(opponent);
        } // end for (int i = m_lstPkListOther.Count - 1; i >= 0; i--)

        //// ！---- end 其它玩家


        //// 吃刺
        bool bPKMonster = true;
        if (_Player.IsSneaking())
        {
            bPKMonster = false;
        }

        if (bPKMonster)
        {
            for (int i = 0; i < m_lstPkMonsterList.Count; i++)
            {
                CMonster monster = m_lstPkMonsterList[i];
                this.ProcessPk_Monster(monster);
            }
        }
    }

    public bool IsInPKList(int nIndex)
    {
        for (int i = 0; i < m_lstPkListSelf.Count; i++)
        {
            if (nIndex == m_lstPkListSelf[i].GetIndex())
            {
                return true;
            }
        }
        return false;
    }

    // PK孤儿球（也就是对方的Player已经掉线了）
    public void ProcessPK_OrphanBall(Ball ballOpponent)
    {
        // to do
        // 各种情况（比如孤儿球身上可能有技能状态）先不处理，直接吃

        int nRet = Ball.WhoIsBig(this, ballOpponent);
        if (nRet == 1)// 我比对方大
        {
            this.PreEat_OrphanBall(ballOpponent);
        }
        else if (nRet == -1) // 我比对方小
        {
           
        }
    }


    public void ProcessPK_Self(Ball ballOpponent)
    {
        if (!IsMainPlayer())
        {
            return;
        }

        if (this.HaveShell() && ballOpponent.HaveShell())
        {
            return;
        }

        if (this.IsEjecting() || ballOpponent.IsEjecting())
        {
            return;
        }

        int nRet = Ball.WhoIsBig(this, ballOpponent);
        if (nRet >= 0)
        {
            this.EatTeammate(ballOpponent);
        }
        else
        {

        }
    }


    // 
    public void ProcessPK_Other(Ball ballOpponent)
    {
		if (this._Player == null || ballOpponent._Player == null) {
			return;
		}

        if (!IsMainPlayer()) // 执行PK者必须是本客户端主角
        {
            return;
        }

  
            if (this._Player.IsProtect() || ballOpponent._Player.IsProtect())
            {
                return;
            }

            if ((this._Player.IsSneaking() || ballOpponent._Player.IsSneaking()) && (this._Player.GetOwnerId() != ballOpponent._Player.GetOwnerId()))
            {
                return;
            }


            if (this._Player.photonView.ownerId == ballOpponent._Player.photonView.ownerId)// 队友
            {
                if (this.IsEjecting() && ballOpponent.IsEjecting())
                {
                    return;
                }
                if (this.HaveShell() && ballOpponent.HaveShell())
                {
                    return;
                }
            }

            if ((this._Player.IsGold() || ballOpponent._Player.IsGold()) && (!Ball.IsTeammate(this, ballOpponent)))
            {
                ballOpponent.Local_Begin_Shell_Temp();
                this.Local_Begin_Shell_Temp();
            }
        



        int nRet = Ball.WhoIsBig(this, ballOpponent);
        if (nRet == 1)
        { 
            if (ballOpponent.isOrphan())
            {
               this.PreEat_Other(ballOpponent);
            }
        }
        else if (nRet == -1)
        {
            ballOpponent.PreEat_Other(this);
           
        }
    }

    public bool isOrphan()
    {
        return ( _Player == null );
    }

    // [to youhua]一定要优化到这种程度：OtherClient的Trigger在本机几乎没有触发的机会（除了实在必要的场合）
    public void ProcessPk(Ball ballOpponent)
    {
        return;

        if (this._balltype != eBallType.ball_type_ball)
        {
            return;
        }

        if (!IsMainPlayer())
        {
            return;
        }

        if (ballOpponent._Player == null)
        {
            ProcessPK_OrphanBall(ballOpponent);
            return;
        }

        if (this._Player.IsProtect() || ballOpponent._Player.IsProtect())
        {
            return;
        }

        if ( ( this._Player.IsSneaking() || ballOpponent._Player.IsSneaking() ) && ( this._Player.GetOwnerId() != ballOpponent._Player.GetOwnerId() ))
        {
            return;
        }


        if (this._Player.photonView.ownerId == ballOpponent._Player.photonView.ownerId)// 队友
        { 
            if (this.IsEjecting() && ballOpponent.IsEjecting())
            {
                return;
            }
            if (this.HaveShell() && ballOpponent.HaveShell())
            {
                return;
            }
        }

        if ( ( this._Player.IsGold() || ballOpponent._Player.IsGold() ) && ( !Ball.IsTeammate(this, ballOpponent) ) )
        {
             ballOpponent.Local_Begin_Shell_Temp();
             this.Local_Begin_Shell_Temp();
            /*
            if ( Ball.IsTeammate(this, ballOpponent) )
            {
                Physics2D.IgnoreCollision( this._ColliderTemp, ballOpponent._ColliderTemp );
                Physics2D.IgnoreCollision( this._ColliderTemp, ballOpponent._Collider);
                Physics2D.IgnoreCollision(this._Collider, ballOpponent._ColliderTemp);
            }
            */
        }


        if (IsDead() || ballOpponent.IsDead())
        {
            return;
        }

        if ((this._Player != null && this._Player.IsProtect()) || (ballOpponent._Player != null && ballOpponent._Player.IsProtect()))
        {
            return;
        }

        int nRet = Ball.WhoIsBig(this, ballOpponent);
        if (this._Player.photonView.ownerId == ballOpponent._Player.photonView.ownerId)
        {
            if (nRet >= 0) 
            {
                this.EatTeammate(ballOpponent);
            }
            else
            {
                
            }
        }
        else
        {
            if (nRet == 1)
            {
                
            }
            else if(nRet == -1 )
            {
                ballOpponent.PreEat( this );
            }
        }

    } 

    public bool CheckIfCover_ByBox( float r1, float r2, Vector3 pos1, Vector3 pos2, float fPercent )
    {
        // 1 - big  2 - small 

        float fOverlayArea = 0;
        float fSmallArea = 0;
        if ( pos2.x < pos1.x )
        {
            if ( pos2.y < pos1.y)
            {
                vecTempPos.x = pos1.x - r1;
                vecTempPos.y = pos1.y - r1;
                vecTempPos2.x = pos2.x + r2;
                vecTempPos2.y = pos2.y + r2;
                fOverlayArea = (vecTempPos2.x - vecTempPos.x) * (vecTempPos2.y - vecTempPos.y);
                fSmallArea = 4 * r2 * r2;
            }
            else
            {
                vecTempPos.x = pos1.x - r1;
                vecTempPos.y = pos1.y + r1;
                vecTempPos2.x = pos2.x + r2;
                vecTempPos2.y = pos2.y - r2;
                fOverlayArea = (vecTempPos2.x - vecTempPos.x) * (vecTempPos.y - vecTempPos2.y);
                fSmallArea = 4 * r2 * r2;
            }
        }
        else
        {
            if (pos2.y < pos1.y)
            {
                vecTempPos.x = pos1.x + r1;
                vecTempPos.y = pos1.y - r1;
                vecTempPos2.x = pos2.x - r2;
                vecTempPos2.y = pos2.y + r2;
                fOverlayArea = (vecTempPos.x - vecTempPos2.x) * (vecTempPos2.y - vecTempPos.y);
                fSmallArea = 4 * r2 * r2;
            }
            else
            {
   
                    vecTempPos.x = pos1.x + r1;
                    vecTempPos.y = pos1.y + r1;
                    vecTempPos2.x = pos2.x - r2;
                    vecTempPos2.y = pos2.y - r2;
                    fOverlayArea = (vecTempPos.x - vecTempPos2.x) * (vecTempPos.y - vecTempPos2.y);
                    fSmallArea = 4 * r2 * r2;
   
            }
        }

        if (fOverlayArea / fSmallArea >= fPercent)
        {
            return true;
        }


        return false;
    }

    public void EatTeammate(Ball ballOpponent)
    {
        if (!CheckIfCover_ByInterpolate(this.GetRadius(), ballOpponent.GetRadius(), this.GetPos(), ballOpponent.GetPos(), MapEditor.s_Instance.m_fYaQiuBaiFenBi_Self))
        {
            return;

        }

        this.EatBall_Self(ballOpponent);
    }

    public void PreEat_Other(Ball ballOpponent )
    {
        if ( !CheckIfCover_ByInterpolate(this.GetRadius(), ballOpponent.GetRadius(), this.GetPos(), ballOpponent.GetPos(), MapEditor.s_Instance.m_fYaQiuBaiFenBi) )
        {
            return;

        }

        this.EatBall_Other(ballOpponent);
    }

    void PreEat_OrphanBall(Ball ballOpponent)
    {
        if (!CheckIfCover(ballOpponent))
        {
            return;

        }

        this.EatOrphanBall(ballOpponent);
    }


    void PreEat(Ball ballOpponent )
    {
        if ( !CheckIfCover( ballOpponent ) )
        {
            return;

        }

        this.EatBall(ballOpponent);
    }

    public void DoDraw()
    {
        /*
        float fCurArea = GetArea();
        float fAreaDelta = m_fPreDrawDeltaTime * MapEditor.s_Instance.m_fXiQiuSuDu;
        m_fPreDrawDeltaTime = 0.0f;
        float fNewArea = fCurArea + fAreaDelta;
        if (fNewArea <= 0.0f) {
            SetDead(true);
            DestroyBall();
            return;
        }
        Local_SetSize(CyberTreeMath.AreaToSize(fNewArea, Main.s_Instance.m_nShit));
        SetNeedSyncSize();
        */
    }

    void DrawBall(Ball ballOpponent)
    {
        /*
		if (this.photonView.ownerId == ballOpponent.photonView.ownerId) {
			if (this.m_bShell && ballOpponent.m_bShell) {
				return;
			}
		}
		*/
        if (ballOpponent.IsDead())
        {
            return;
        }

        // 因为被吸的是绝对数量而不是自身体积的百分比，因此非常好计算。
        this.PreDraw(Time.deltaTime);
        ballOpponent.PreDraw(-Time.deltaTime);
    }

    float m_fPreDrawDeltaTime = 0.0f;
    public void PreDraw(float fDeltaTime)
    {
        m_fPreDrawDeltaTime += fDeltaTime;

    }

    public void AddArea(float fAreaToAdd)
    {

        float fCurArea = CyberTreeMath.SizeToArea(GetSize(), Main.s_Instance.m_nShit);
        float fNewArea = fCurArea + fAreaToAdd;
        float fNewSize = CyberTreeMath.AreaToSize(fNewArea, Main.s_Instance.m_nShit);
        SetSize(fNewSize);

    }



    void Eat(Ball ballOpponent)
    {
         EatBall(ballOpponent);
    }

    public void EatBean(CMonster bean)
    {
        if (bean.IsDead())
        { // 避免重复吃豆子(Unity的很多操作都要下一贞才真正执行，所以完全有可能被吃的东西没有立刻消失，又被吃一次)
            return;
        }

        if (_Player.photonView.isMine)
        { // 吃球者只在自己那个客户端（MainPlayer端）涨体积，在别人的客户端吃了豆子就不涨了，只涨一边
            /*
            float fCurSize = GetSize();
			float fBeanSize = bean.GetFloatParam(0);//Main.s_Instance.m_fBeanSize;
            float fAreaToAdd = CyberTreeMath.SizeToArea(fBeanSize, Main.s_Instance.m_nShit);
            float fCurArea = CyberTreeMath.SizeToArea(fCurSize, Main.s_Instance.m_nShit);
            float fNewArea = fCurArea + fAreaToAdd;
            float fNewSize = CyberTreeMath.AreaToSize(fNewArea, Main.s_Instance.m_nShit);
            _Player.SetBallSize(GetIndex(), fNewSize);
            */
            float fBeanVolume = bean.GetConfig().fFoodSize;
            float fCurVolume = GetVolume();
            float fNewVolume = fBeanVolume + fCurVolume;
            SetVolume(fNewVolume);
            //float fNewSize = CyberTreeMath.Volume2Scale(fNewVolume);
            //Local_SetSize( fNewSize );

            // 经验值
            float fExp = bean.GetConfig().fExp;
            if (fExp > 0f)
            {
                _Player.AddExp((int)fExp);
            }

            // 金钱
            float fMoney = bean.GetConfig().fMoney;
            if (fMoney > 0)
            {
                _Player.AddMoney((int)fMoney);
            }
        }

        ResourceManager.RecycleMonster(bean);
    }

    public void EatBean( int nCollectionGuid, int nBeanIdxInColle,  ref CMonsterEditor.sThornConfig thorn_buildin_config )
    {
        CMonsterEditor.sThornConfig config = CMonsterEditor.s_Instance.GetMonsterConfigById(thorn_buildin_config.szId);
        if (IsMainPlayer())
        {
            float fCurTiJi = GetVolume();
            float fNewTiJi = fCurTiJi + config.fFoodSize; // 这里的fFoodSize是指体积值
            SetVolume(fNewTiJi);
            //float fNewSize = CyberTreeMath.Volume2Scale(fNewTiJi);
            //Local_SetSize(fNewSize);

            // 经验值
            float fExp = config.fExp;
            if (fExp > 0f)
            {
                _Player.AddExp((int)fExp);
            }

            // 金钱
            float fMoney = config.fMoney;
            if (fMoney > 0)
            {
                _Player.AddMoney((int)fMoney);
            }
            _Player.AddEatenBeanToSyncQueue(nCollectionGuid, nBeanIdxInColle);
        }
    }



    public void EatBean_New( CBean bean )
    {
		if ( !IsMainPlayer() )
		{
			return;
		}


		if (!_PlayerIns.CheckIfCanEatBean ()) {
			return;
		}

        bean.SetActive( false ); // 避免重复吃豆

        CMonsterEditor.sThornConfig config = bean.GetConfig();
        float fCurTiJi = GetVolume();
        float fNewTiJi = fCurTiJi + config.fFoodSize; // 这里的fFoodSize是指体积值
        SetVolume(fNewTiJi);
       
        // 经验值
        float fExp = config.fExp;
        if (fExp > 0f)
        {
            _Player.AddExp((int)fExp);
        }

        // 金钱
        float fMoney = config.fMoney;
        if (fMoney > 0)
        {
            _Player.AddMoney((int)fMoney);
        }

        CBeanManager.s_Instance.AddToEatBeanList( bean );
    }
    
    public void EatThorn(CMonster thorn)
    {
        if ( !IsMainPlayer() )
        {
            return;
        }

		if (!_PlayerIns.CheckIfCanEatThorn ()) {
			return;
		}

        if (thorn.IsDead()) // 避免重复吃刺
        {
            return;
        }

        if (!IsMainPlayer()) // 非本客户端MainPlayer的球球执行吃刺动作
        {
            return;
        }

        CMonsterEditor.sThornConfig config = CMonsterEditor.s_Instance.GetMonsterConfigById(thorn.GetConfigId());

 
        thorn.SetDead(true);

        // 吃刺也会增加球本身的总体积。但如果当前队伍已经不能爆刺了，则吃刺不增加球的体积
        // 先执行“增加体积”，再炸。以免不小心炸死了就没有增加体积
        // poppin to youhua 吃一个刺居然发了这么多条网络协议，看看有没有可以优化
        if (IsMainPlayer())
        {
            CGrowSystem.s_Instance.SetEatThornNum(CGrowSystem.s_Instance.GetEatThornNum() + 1);
           

            float fCurTiJi = GetVolume();
            float fNewTiJi = fCurTiJi + config.fFoodSize; // 这里的fFoodSize是指体积值
            SetVolume(fNewTiJi);
           

            if (config.fBuffId > 0)
            { // 这个刺带Buff
                _Player.AddBuff((int)config.fBuffId);
            }

            // 经验值
            if (config.fExp > 0f)
            {
                _Player.AddExp((int)config.fExp);
            }

            // 金钱
            if (config.fMoney > 0)
            {
                _Player.AddMoney((int)config.fMoney);
            }

            // 吃刺直接炸
            if ((int)config.fExplodeChildNum > 0 && ( !_Player.IsMagicShielding() ) ) 
            { // 如果配置的炸球个数等于0，则不炸球，这种刺可以当豆子用
                Explode(config);
            }
        }

        _Player.DestroyMonster(thorn);
    }

    public void SetThornSize(float fThornSize)
    {
        Local_SetThornSize(fThornSize);
    }

    [PunRPC]
    public void RPC_SetThornSize(float fThornSize)
    {
        Local_SetThornSize(fThornSize);
    }

    float m_fThornSize = 0.0f;
    public void Local_SetThornSize(float fThornSize)
    {   // “刺环”概念废弃
        /*
        if (_thorn == null) {
            return;
        }

        m_fThornSize = fThornSize;
        float fBallSize = GetSize();
        if (m_fThornSize > fBallSize) {
            m_fThornSize = fBallSize;
        }
        if (fBallSize == 0) {
            return;
        }
        float fThornRealScaleSize = m_fThornSize / fBallSize;
        vecTempScale.x = fThornRealScaleSize;
        vecTempScale.y = fThornRealScaleSize;
        vecTempScale.z = 1.0f;
        _thorn.transform.localScale = vecTempScale;
        //_thornForAlpha.transform.localScale = vecTempScale;
        if (_Player.photonView.isMine && CheckIfExplode()) {
            Explode();
        }
		*/
    }

    public void SetVolume( float fVolume )
    {
        m_fVolume = fVolume;
        if (m_fVolume < Main.m_fBallMinVolume )
        {
            m_fVolume = Main.m_fBallMinVolume;
        }
        float fNewSize = CyberTreeMath.Volume2Scale(m_fVolume);
        Local_SetSize( fNewSize );
    }

    public float GetThornSize()
    {
        return m_fThornSize;
    }

    bool CheckIfExplode()
    {
        float fBallSize = GetSize();
        if (m_fThornSize < fBallSize)
        {
            return false;
        }

        if (fBallSize < 2.0f)
        {
            return false;
        }

        return true;
    }

    public static Vector2[] s_aryBallExplodeDirection = {
        new Vector2( 0.0f, 1.0f ),
        new Vector2( 0.707f, 0.707f ),
        new Vector2( 1.0f, 0.0f ),
        new Vector2( 0.707f, -0.707f ),
        new Vector2( 0, -1.0f ),
        new Vector2( -0.707f, -0.707f ),
        new Vector2( -1.0f, 0.0f ),
        new Vector2( -0.707f, 0.707f ),

    };

    /*
	 * 爆裂的规则：
	 * 1、准备爆裂的母球，半径最小为2。不然没法爆，因为爆出的小球最小半径得是1（一个孢子的尺寸），并且爆了之后母球剩下的半径至少也得是1.
	 * 2、母球分一半的半径出来爆。
	 * 3、能爆成8等分尽量8等分（确保爆出来的每个小球的半径大于等于1）；不够8等分则能爆几个算几个。
	 * */
    public float GetMinExplodeSize()
    {
        return Main.BALL_MIN_SIZE;
    }

    float m_fShitExplodeTime = 0.0f;

    public void RushDueToExplode(CMonsterEditor.sThornConfig config)
    {
        if ( _ba.GetRealTimeVelocity() <= 0f )
        {
            return;
        }

        _Player.RushDueToExplode(GetIndex(), config.fExplodeRunDistance, config.fExplodeRunTime );
    }

    public void Explode(CMonsterEditor.sThornConfig config)
    {
        if (!_Player.photonView.isMine)
        {
            return;
        }

        _Player.UpdateBuffEffect_Explode();

        int nChildNum = 0;
        float fMotherLeftVolume = 0f;
        float fChildVolume = 0f;

        //if (!CheckIfReallyCanExplode(config, _Player.GetExplodeChildNumBuffEffect(), _Player.GetExplodeMotherLeftBuffEffect(), ref nChildNum, ref fChildSize, ref fMotherLeftSize))
        if( !CheckIfReallyCanExplode_New(config, _Player.GetExplodeChildNumBuffEffect(), _Player.GetExplodeMotherLeftBuffEffect(), ref nChildNum, ref fChildVolume, ref fMotherLeftVolume))
        {
            this.RushDueToExplode(config);

            return;
        }

        Main.s_Instance.BeginCameraSizeProtect(config.fExplodeRunTime);

        bool bMotherMoving = _ba.GetRealTimeVelocity() > 0f;

        _Player.ExplodeBall( GetIndex(), config.szId, (int)config.fExplodeChildNum, nChildNum, fChildVolume, fMotherLeftVolume, bMotherMoving);
    }

    public void Explode_CausedBySkill(CMonsterEditor.sThornConfig config, float fChildVolume)
    {
        _Player.UpdateBuffEffect_Explode();

        int nChildNum = 0;
        float fMotherLeftVolume = 0f;
        //if (!CheckIfReallyCanExplode_CausedBySkill(config, _Player.GetExplodeChildNumBuffEffect(), fChildVolume, ref nChildNum, ref fChildSize, ref fMotherLeftSize))
        if ( !CheckIfReallyCanExplode_CausedBySkill_New(config, _Player.GetExplodeChildNumBuffEffect(), fChildVolume, ref nChildNum, ref fMotherLeftVolume))
        {
            this.RushDueToExplode(config);

            return;
        }
        //Main.s_Instance.BeginCameraSizeProtect(config.fExplodeRunTime);

        bool bMotherMoving = _ba.GetRealTimeVelocity() > 0f;
        _Player.ExplodeBall(GetIndex(), config.szId, (int)config.fExplodeChildNum, nChildNum, fChildVolume, fMotherLeftVolume, bMotherMoving);
    }

    public bool CheckIfReallyCanExplode_CausedBySkill_New(CMonsterEditor.sThornConfig config, float fChildNumBuffEffect, float fChildVolume, ref int nChildNum, ref float fMotherLeftVolume)
    {
        int nAvailableNum = _Player.GetCurAvailableBallCount();
        if (nAvailableNum < Main.s_Instance.m_nExplodeMinNum)// 至少得有1个空位的余额才行，不然炸个屁的球啊
        {
            return false;
        }

        float fMotherCurVolume = GetVolume();
        int nExpectChildNum = (int)config.fExplodeChildNum;
        nExpectChildNum = (int)(nExpectChildNum * fChildNumBuffEffect);

        if (nExpectChildNum < 1)
        {
            return false;
        }

        bool bFound = false;
        for (int i = nExpectChildNum; i >= 1; i--)
        {
            fMotherCurVolume = GetVolume() - i * fChildVolume;
            if (fMotherCurVolume >= 0)
            {
                nChildNum = i;
                bFound = true;
                break;
            }
        } // end for i
        if (!bFound)
        {
            return false;
        }

        fMotherLeftVolume = fMotherCurVolume;
        if (fMotherLeftVolume < Main.m_fBallMinVolume)
        {
            fMotherLeftVolume = 0; 
        }

        return true;
    }


    public bool CheckIfReallyCanExplode_CausedBySkill(CMonsterEditor.sThornConfig config, float fChildNumBuffEffect, float fChildVolume, ref int nChildNum, ref float fChildSize, ref float fMotherLeftSize)
    {
        int nAvailableNum = _Player.GetCurAvailableBallCount();
        if (nAvailableNum < Main.s_Instance.m_nExplodeMinNum)// 至少得有1个空位的余额才行，不然炸个屁的球啊
        {
            return false;
        }

        float fMotherCurVolume = GetVolume();
        int nExpectChildNum = (int)config.fExplodeChildNum;
        nExpectChildNum = (int)(nExpectChildNum * fChildNumBuffEffect);

        if (nExpectChildNum < Main.s_Instance.m_nExplodeMinNum)
        {
            return false;
        }

        bool bFound = false;
        for (int i = nExpectChildNum; i >= Main.s_Instance.m_nExplodeMinNum; i--)
        {
            fMotherCurVolume = GetVolume() - i * fChildVolume;
            if (fMotherCurVolume >= 0)
            {
                nChildNum = i;
                bFound = true;
                break;
            }
        } // end for i
        if (!bFound)
        {
            return false;
        }

        fChildSize = CyberTreeMath.Volume2Scale(fChildVolume);
        fMotherLeftSize = CyberTreeMath.Volume2Scale(fMotherCurVolume);

        return true;
    }

    public bool CheckIfReallyCanExplode(CMonsterEditor.sThornConfig config, float fChildNumBuffEffect, float fMotherLeftBuffEffect, ref int nChildNum, ref float fChildSize, ref float fMotherLeftSize)
    {
        int nAvailableNum = _Player.GetCurAvailableBallCount();
        if (nAvailableNum < Main.s_Instance.m_nExplodeMinNum)
        { // 至少得有1个空位的余额才行，不然炸个屁的球啊
            return false;
        }

        float fMotherCurArea = GetVolume();

        // buff可能会影母体存留百分比
        float fRealMotherLeftPercent = config.fExplodeMotherLeftPercent + fMotherLeftBuffEffect;
        //Debug.Log( "母体存留百分比：" + config.fExplodeMotherLeftPercent + " , " +  fRealMotherLeftPercent);

        float fExplodePercent = 1f - fRealMotherLeftPercent;
        if (fExplodePercent <= 0f)
        {
            return false;
        }
        float fChildsArea = fMotherCurArea * fExplodePercent; // (预期母球分出来的体积，即子球的总体积)

        // buff可能会影预期分球数
        int nExpectNumConfig = (int)config.fExplodeChildNum; // 预期分球个数，如果不够预期，则能分几个算几个（这是原始配置中的个数）
        int nExpectNum = (int)(nExpectNumConfig * fChildNumBuffEffect); // （配置和个数乘以Buff的加成数，就是实际打算分的个数）
                                                                        //Debug.Log( "分球数：" + nExpectNumConfig + "," + nExpectNum );

        int nRealNum = 0;
        if (nExpectNum > nAvailableNum)
        {
            nExpectNum = nAvailableNum;
        }
        float fRealChildArea = 0f;// (实际子球的总面积)
        for (nRealNum = nExpectNum; nRealNum >= Main.s_Instance.m_nExplodeMinNum; nRealNum--)
        {
            fChildSize = CyberTreeMath.Volume2Scale(fChildsArea / nRealNum);
            if (fChildSize >= Main.BALL_MIN_SIZE)
            {
                fRealChildArea = fChildsArea;
                break;
            }
        }

        if (nRealNum < Main.s_Instance.m_nExplodeMinNum)
        {
            return false;
        }

        // 这里再结合Buff



        nChildNum = nRealNum;

        float fMotherLeftArea = fMotherCurArea - fRealChildArea;
        fMotherLeftSize = CyberTreeMath.Volume2Scale(fMotherLeftArea);
        if (fMotherLeftSize < Main.BALL_MIN_SIZE)
        { // 如果母球剩余半径太小（小于本游戏规定的半径最小值），就直接让它死掉
            fMotherLeftSize = 0f;
        }


        return true;
    }

    public bool CheckIfReallyCanExplode_New(CMonsterEditor.sThornConfig config, float fChildNumBuffEffect, float fMotherLeftBuffEffect, ref int nChildNum, ref float fChildVolume, ref float fMotherLeftVolume)
    {
        int nAvailableNum = _Player.GetCurAvailableBallCount();
        if (nAvailableNum < 1)
        { 
            return false;
        }

        float fMotherCurArea = GetVolume();

        // buff可能会影母体存留百分比
        float fRealMotherLeftPercent = config.fExplodeMotherLeftPercent + fMotherLeftBuffEffect;
        //Debug.Log( "母体存留百分比：" + config.fExplodeMotherLeftPercent + " , " +  fRealMotherLeftPercent);

        float fExplodePercent = 1f - fRealMotherLeftPercent;
        if (fExplodePercent <= 0f)
        {
            return false;
        }
        float fChildsArea = fMotherCurArea * fExplodePercent; // (预期母球分出来的体积，即子球的总体积)

        // buff可能会影预期分球数
        int nExpectNumConfig = (int)config.fExplodeChildNum; // 预期分球个数，如果不够预期，则能分几个算几个（这是原始配置中的个数）
        int nExpectNum = (int)(nExpectNumConfig * fChildNumBuffEffect); // （配置和个数乘以Buff的加成数，就是实际打算分的个数）
                                                                        //Debug.Log( "分球数：" + nExpectNumConfig + "," + nExpectNum );

        int nRealNum = 0;
        if (nExpectNum > nAvailableNum)
        {
            nExpectNum = nAvailableNum;
        }
        float fRealChildArea = 0f;// (实际子球的总面积)
        for (nRealNum = nExpectNum; nRealNum >= Main.s_Instance.m_nExplodeMinNum; nRealNum--)
        {
            fChildVolume = fChildsArea / nRealNum;
            if (fChildVolume >= Main.m_fBallMinVolume)
            {
                fRealChildArea = fChildsArea;
                break;
            }
        }

        if (nRealNum < Main.s_Instance.m_nExplodeMinNum)
        {
            return false;
        }

        // 这里再结合Buff



        nChildNum = nRealNum;

        fMotherLeftVolume = fMotherCurArea - fRealChildArea;
        if (fMotherLeftVolume < Main.m_fBallMinVolume)
        { // 如果母球剩余半径太小（小于本游戏规定的半径最小值），就直接让它死掉
            fMotherLeftVolume = 0f;
        }


        return true;
    }

    public void DoExplode(string szThornConfigId, int nChildNum, float fChildSize, float fMotherLeftSize, List<int> lstChildBallsIndex )
    {
        if (IsExploding())
        {
            return;
        }

        if (IsDead())
        {
            Debug.LogError("DoExplode   IsDead ()");
            return;
        }
        if (IsEjecting())
        {
            EndEject();
        }
        m_ExplodeConfig = CMonsterEditor.s_Instance.GetMonsterConfigById(szThornConfigId);
        if (IsMainPlayer())
        {
            CAudioManager.s_Instance.PlayAudio(CAudioManager.eAudioId.e_audio_explode);
        }
        m_vecMotherPos = GetPos();
        float fRunDistance = m_ExplodeConfig.fExplodeRunDistance * GetRadius();
        m_lstDir = MapEditor.s_Instance.GetExplodeDirList(nChildNum);
        float fSpeed = m_ExplodeConfig.fExplodeRunTime > 10f ? m_ExplodeConfig.fExplodeRunTime : 10f;
        BeginDoExplode(nChildNum, fChildSize, fMotherLeftSize, fRunDistance, fSpeed, lstChildBallsIndex);
    }
    /*
    public void DoExplode(string szThornConfigId, int nChildNum, float fChildSize, float fMotherLeft, double dOccurTime)
    {
		if (IsExploding ()) {
			return;
		}

        if (IsDead())
        {
            Debug.LogError("DoExplode   IsDead ()");
            return;
        }

        int nAvailableNum = _Player.GetCurAvailableBallCount();
        if (nAvailableNum <= Main.s_Instance.m_nExplodeMinNum)
        {
            return;
        }

        if (IsEjecting())
        {
            EndEject();
        }

        // CAudioManager.s_Instance.audio_explode.Play();

        float fDelayTime = (float)(PhotonNetwork.time - dOccurTime);
		m_ExplodeConfig = CMonsterEditor.s_Instance.GetMonsterConfigById(szThornConfigId);
		float fRealRunTime = m_ExplodeConfig.fExplodeRunTime - fDelayTime;
        if (fRealRunTime < Main.s_Instance.m_fMinRunTime)
        {
            fRealRunTime = Main.s_Instance.m_fMinRunTime;
        }
        // 生成小球
        float fRadiusMother = GetRadius();
        if (IsUnfolding())
        {
            fRadiusMother /= Main.s_Instance.m_fUnfoldSale;
        }

        if (_Player.IsMainPlayer())
        {
            CAudioManager.s_Instance.PlayAudio(CAudioManager.eAudioId.e_audio_explode);
        }

		m_vecMotherPos = GetPos();
		float fRunDistance = m_ExplodeConfig.fExplodeRunDistance * fRadiusMother;
		m_lstDir = MapEditor.s_Instance.GetExplodeDirList(nChildNum);
		BeginDoExplode ( nChildNum, fChildSize, fMotherLeft, fRunDistance, fRealRunTime);
    }
    */
	float m_fMotherLeft = 0;
	float m_fExplodeShellTime = 0;
	int m_nTotalChildNum = 0;
	int m_nCurChildNum = 0;
	const int BeginDoExplode_CHILD_NUM_PER_FRAME = 8;
	bool m_bExploding = false;
	CMonsterEditor.sThornConfig m_ExplodeConfig;
	Vector3 m_vecMotherPos = new Vector3();
	float m_fRunDistance = 0f;
	float m_fRealRunTime = 0f;
	List<Vector2> m_lstDir;
	float m_fChildSize = 0;
	float m_fSegDis = 0f;
    int[] m_lstChildBallsIndex = new int[Main.MAX_BALL_NUM];
	void BeginDoExplode( int nTotalChildNum, float fChildSize,float fMotherLeft, float fRunDistance, float fSpeed, List<int> lstChildBallsIndex )
	{
		if (IsExploding ()) {
			return;
		}

		if (nTotalChildNum == 0) {
			return;
		}
        
        lstChildBallsIndex.CopyTo(m_lstChildBallsIndex);
        m_nTotalChildNum = lstChildBallsIndex.Count;
		m_bExploding = true;
		m_fChildSize = fChildSize;
		m_fRunDistance = fRunDistance;
		m_nCurChildNum = 0;
		m_fSegDis = fRunDistance / ( m_ExplodeConfig.fExplodeChildNum / 4 );
        //m_fEjectSpeed = fSpeed;
        m_nExplodeRound = 0;
        m_fMotherLeft = fMotherLeft;
    }

	public bool IsExploding()
	{
		return m_bExploding;
	}

    int m_nExplodeRound = 0;

    
	void EndExplode()
	{
        m_bExploding = false;

		// 母球剩余
		if (m_fMotherLeft <= 0f)
		{
			_Player.DestroyBall(GetIndex());
		}
		else
		{
			Local_SetSize(m_fMotherLeft);
			Local_SetShellInfo(m_fExplodeShellTime, 0f, eShellType.explode_ball);
            Local_BeginShell();
        }
    }


    public struct EatNode
    {
        public Ball eatenBall;
        public float eatenSize;
    };

    List<EatNode> m_lstEaten = new List<EatNode>();

    void AddEatenNode(EatNode node)
    {
        m_lstEaten.Add(node);
    }

   

    bool CheckIfInEatenList(Ball the_eater, Ball ballOpponent)
    {
        for (int i = m_lstEaten.Count - 1; i >= 0; i--)
        {
            EatNode node = m_lstEaten[i];
            if (node.eatenBall == the_eater || node.eatenBall == ballOpponent)
            {
                return true;
            }
        }

        return false;
    }

    public void SetSelfTriggerEnable( bool bEnabled )
    {
        _TriggerMergeSelf.enabled = bEnabled;
    }

    public virtual void SetDead(bool val, int nEaterOwnerId = 0)
    {
        m_bDead = val;

        if (m_bDead == true) // 死亡
        {
            SetActive(false );

            SetSelfTriggerEnable(false);
        }
        else // 活着
        {
            if (_Player != null && _Player.IsMainPlayer()) // MainPlayer的球球只要活着就一定显示。OtherClient的球球活着不一定显示，还要看视野裁剪结果
            {
                SetActive(true);
            }
            else
            {

            }
        }


    }

 

    CGrass m_Grass = null;
    float m_fEnterGrassTime = 0f;

    public CGrass GetGrass()
    {
        return m_Grass;
    }

    void ProcessGrassWhenDead()
    {
        LeaveGrass();
    }

    List<CGrass> m_lstGrass = new List<CGrass>();
    public void LeaveGrass()
    {
        if (m_Grass)
        {
            m_Grass.RemoveBall(this);
        }
        m_Grass = null;
    }

    public void EnterGrass(CGrass grass)
    {
        m_Grass = grass;
        grass.AddBall(this);
        if (m_Grass)
        {
            m_fEnterGrassTime = (float)PhotonNetwork.time; // 进入草丛的时间
        }
    }


    public void Hide()
    {
        vecTempPos = GetPos();
        vecTempPos.x = -10000;
        vecTempPos.y = -10000;
        Local_SetPos(vecTempPos);
    }

    public static bool IsSelf( Ball ball1, Ball ball2 )
    {
        return ball1._Player.GetOwnerId() == ball2._Player.GetOwnerId();
    }

    public void EatOrphanBall(Ball ballOpponent)
    {
        float fEaterVolume = GetVolume();

        float fPoorthingVolume = ballOpponent.GetVolume();

        // to do 各种技能状态都暂不处理

        // 湮灭状态，暂不处理
        /*
        if (ballOpponent._Player.IsAnnihilaing() && (!IsSelf(this, ballOpponent)) && (!IsTeammate(this, ballOpponent)) && (!_Player.IsMagicShielding()))
        {
            fPoorthingVolume *= ballOpponent._Player.GetAnnihilatePercent();
        }
        */

        float fNewVolume = fEaterVolume + fPoorthingVolume;

        if (fNewVolume < Main.m_fBallMinVolume)
        {
            fNewVolume = Main.m_fBallMinVolume;
        }

        this.SetVolume(fNewVolume);

        // 变刺状态，暂不处理
        /*
        float fExplodeChildVolume = 0f;
        if (ballOpponent._Player.IsBecomeThorn() && (!Ball.IsTeammate(this, ballOpponent)))
        {
            fExplodeChildVolume = ballOpponent.GetVolume();
        }
        */

        this._Player.photonView.RPC("RPC_EatOrphanBall", PhotonTargets.All, ballOpponent._PlayerIns.GetAccount(), ballOpponent.GetIndex());
    }

    public void EatBall_Self(Ball ballOpponent)
    {
        if ( (!this.IsMainPlayer()) || (!ballOpponent.IsMainPlayer() ) ) // 吃球者和被吃者都是MainPlayer的球
        {
            return;
        }

        if (ballOpponent.IsDead()) // 避免重复吃球
        {
            return;
        }

        ballOpponent.SetDead( true );

        float fEaterVolume = GetVolume();
        float fPoorthingVolume = ballOpponent.GetVolume();
        float fNewVolume = fEaterVolume + fPoorthingVolume;
        this.SetVolume(fNewVolume);

       ballOpponent.SetDead( true );
      //  _Player.photonView.RPC("DestroyBall_New", PhotonTargets.All, _Player.GetAccount(), this.GetIndex(), _Player.GetAccount(), ballOpponent.GetIndex() );
    }

    public void EatBall_Other(Ball ballOpponent)
    {
        if ((!this.IsMainPlayer()) && (!ballOpponent.IsMainPlayer())) // 吃球者或被吃者至少有一方是MainPlayer的球
        {
            return;
        }

        if (ballOpponent.IsDead()) // 避免重复吃球
        {
            return;
        }

        ballOpponent.SetDead(true);

        float fEaterVolume = GetVolume();
        float fPoorthingVolume = ballOpponent.GetVolume();



        ////// 湮灭状态
        if (ballOpponent._Player != null && _Player != null) // [to do]这里还没改成最新的机制
        {
            if (ballOpponent._Player.IsAnnihilaing() && (!IsTeammate(this, ballOpponent)) && (!_Player.IsMagicShielding()))
            {
                fPoorthingVolume *= ballOpponent._Player.GetAnnihilatePercent();
            }
        }
        // end 湮灭状态
        

        float fNewVolume = fEaterVolume + fPoorthingVolume;
        if (fNewVolume < Main.m_fBallMinVolume)
        {
            fNewVolume = Main.m_fBallMinVolume;
        }

        //// 变刺状态
        float fExplodeChildVolume = 0f;
        string szThornId = "";  
        if (ballOpponent._Player != null && _Player != null) // [to do]这里还没改成最新的机制
        {
            if (ballOpponent._Player.IsBecomeThorn() && (!Ball.IsTeammate(this, ballOpponent)))
            {
                fExplodeChildVolume = ballOpponent.GetVolume();
                szThornId = ballOpponent._Player.GetBecomeThornExplodeConfigId();
            }
        }

        //// end 变刺状态



        // 我们的规则是
        Player player = ballOpponent._Player;
        if (player == null ) // 被吃者是“孤儿球”（其对应的玩家此时处于离线状态）
        {
            player = this._Player;
        }
        if ( player == null )
        {
            Debug.LogError("player == null");
            return;
        }

        player.photonView.RPC("EatBall_Process", PhotonTargets.All, this.GetAccount(), this.GetIndex(), ballOpponent.GetAccount(), ballOpponent.GetIndex(), fNewVolume, fExplodeChildVolume, szThornId );
        player.photonView.RPC("DestroyBall_New", PhotonTargets.All, this.GetAccount(), this.GetIndex(), ballOpponent.GetAccount(), ballOpponent.GetIndex());
    }

    string m_szAccount = "";

    public void SetAccount( string szAccount )
    {
        m_szAccount = szAccount;
    }

    public string GetAccount()
    {
        /*
        if ( _PlayerIns == null )
        {
            Debug.LogError(" _PlayerIns == null");
            return "";
        }
        return _PlayerIns.GetAccount();
        */
        return m_szAccount;
    }

    // 发起“吃球”事件。发起该事件的客户端可能是主吃方，也可能是被吃方，以那边先触发为准
    public void EatBall(Ball ballOpponent)
    {
        if ( !ballOpponent.IsMainPlayer()) // 被吃者是MainPlayer
        {
            return;
        }

        if (ballOpponent.IsDead())
        {
            return;
        }

        ballOpponent._Player.DestroyBall(ballOpponent.GetIndex(), this._Player.GetOwnerId(), this.GetIndex() );

        float fEaterVolume = GetVolume();

        float fPoorthingVolume = ballOpponent.GetVolume();// CyberTreeMath.SizeToArea(fPoorthingSize, Main.s_Instance.m_nShit);

        // 湮灭状态
        if (ballOpponent._Player.IsAnnihilaing() && ( !IsSelf( this, ballOpponent) ) && ( !IsTeammate(this, ballOpponent) ) && ( !_Player.IsMagicShielding() ) )  
        {
            fPoorthingVolume *= ballOpponent._Player.GetAnnihilatePercent();
        }

        float fNewVolume = fEaterVolume + fPoorthingVolume;

        if (fNewVolume < Main.m_fBallMinVolume)
        {
            fNewVolume = Main.m_fBallMinVolume;
        }


        float fTotalEatArea = this._Player.GetTotalEatArea();
        if (ballOpponent._Player.GetOwnerId() != this._Player.GetOwnerId()) // 吃自己的球不算哦，必须吃别人的球。另外，场景球、豆子、孢子都是走的Monster流程而不是Ball流程，所以不用担心和这里混淆起来
        {
            fTotalEatArea += fPoorthingVolume;
        }

        float fExplodeChildVolume = 0f;
        if (ballOpponent._Player.IsBecomeThorn() && ( !Ball.IsTeammate( this, ballOpponent ) ) )
        {
            fExplodeChildVolume = ballOpponent.GetVolume();
        }

        this._Player.photonView.RPC("RPC_EatSucceed", PhotonTargets.All, ballOpponent._Player.GetOwnerId() , ballOpponent.GetIndex(), this.GetIndex(), fNewVolume, fTotalEatArea, fExplodeChildVolume, ballOpponent._Player.GetBecomeThornExplodeConfigId());
       // ballOpponent._Player.photonView.RPC("RPC_BeEaten", PhotonTargets.All, ballOpponent.GetIndex(), this._Player.GetOwnerId());
    }

    public void DestroyBall()
    {

    }




    void FrameInterpolation(Vector3 dest)
    {
        Local_SetPos(dest.x, dest.y, dest.z);
    }


    public void Terminate()
    {
        //if (this.photonView.isMine) {
        //	PhotonNetwork.Destroy ( this.gameObject );
        //}
    }

    public static int WhoIsBig(Ball ball1, Ball ball2)
    {
        float fSize1 = ball1.GetRadius();// CyberTreeMath.FloatTrim(ball1._Trigger.bounds.size.x);
        float fSize2 = ball2.GetRadius();// CyberTreeMath.FloatTrim(ball2._Trigger.bounds.size.x);
        if (fSize1 == fSize2)
        {
            return 0;
        }
        else if (fSize1 > fSize2)
        {
            return 1;
        }
        else
        {
            return -1;
        }
    }

    // [to youhua] 稍后优化这个覆盖算法
    bool CheckIfTotallyCover(float r1, float r2, Vector2 pos1, Vector2 pos2)
    {
        //float r1 = circle1.bounds.size.x / 2.0f;
        //float r2 = circle2.bounds.size.x / 2.0f;

        float deltaX = pos1.x - pos2.x;
        float deltaY = pos1.y - pos2.y;
        float dis = Mathf.Sqrt(deltaX * deltaX + deltaY * deltaY);
        if (dis <= Mathf.Abs(r1 - r2) * 0.9f)
        {
            return true;
        }

        return false;
    }

    public bool CheckIfCover( float r1, float r2, Vector2 pos1, Vector2 pos2, float fPercent = 0.9f )
    {
        float fDis = Vector2.Distance(pos1, pos2);
        if ( ( r1 + r2 ) >= fDis )
        {
            return true;
        }

        return false;
    }

    // 插值法
    public bool CheckIfCover_ByInterpolate(float r1, float r2, Vector2 pos1, Vector2 pos2, float fPercent = 0.9f)
    {
        float fTotal = 2 * r2;
        float fMiddle = Vector2.Distance( pos1, pos2 ) - r1 + r2;
        float t = 1 - (fMiddle / fTotal);
        if ( t >= fPercent)
        {
            return true;
        }

        return false;
    }

    public bool CheckIfCover(Ball opponent)
    {
        return CheckIfCover( this.GetRadius(), opponent.GetRadius(), this.GetPos(), opponent.GetPos(), MapEditor.s_Instance.m_fYaQiuBaiFenBi);
    }

    bool CheckIfPartiallyCover(CircleCollider2D c1, CircleCollider2D c2, float fYaQiuBaiFenBi)
    {
        // CircleCollider2D c1 = this._Trigger;
        //  CircleCollider2D c2 = ballOpponent._Trigger;

        float d;
        float s, s1, s2, s3, angle1, angle2;
        float r1 = c1.bounds.size.x / 2f; // this ball radius
        float r2 = c2.bounds.size.x / 2f;        // opponent radius
        float r1_2cifang = r1 * r1;
        float r2_2cifang = r2 * r2;

        d = Mathf.Sqrt((c1.bounds.center.x - c2.bounds.center.x) * (c1.bounds.center.x - c2.bounds.center.x) + (c1.bounds.center.y - c2.bounds.center.y) * (c1.bounds.center.y - c2.bounds.center.y));
        float d_2cifang = d * d;
        if (d >= (r1 + r2))
        {//两圆相离
            return false;
        }
        if ((r1 - r2) >= d)
        {//两圆内含,c1大
            return true;
        }

        angle1 = Mathf.Acos((r1_2cifang + d_2cifang - r2_2cifang) / (2 * r1 * d));
        angle2 = Mathf.Acos((r2_2cifang + d_2cifang - r1_2cifang) / (2 * r2 * d));

        s1 = angle1 * r1_2cifang;
        s2 = angle2 * r2_2cifang;
        s3 = r1 * d * Mathf.Sin(angle1);
        s = s1 + s2 - s3;

        float fOpponentMianJi = Mathf.PI * r2_2cifang;
        float fCurPercent = s / fOpponentMianJi;

        if (fCurPercent >= fYaQiuBaiFenBi/*MapEditor.s_Instance.m_fYaQiuBaiFenBi*/)
        {
            return true;
        }

        return false;
    }

    public bool CheckIfCanForceSpit()
    {
        if (IsEjecting())
        {
            return false;
        }

        return true;
    }

    public void EatSpore( CSpore spore )
    {
        /*
        if ( spore.GetBall() == this && spore.IsEjecting() )
        {
            return;
        }
        */
        if (spore.GetVolume() >= this.GetVolume()) // 体积大于孢子才能吃孢子
        {
            return;
        }

        this.SetVolume( this.GetVolume() + spore.GetVolume() );

        _Player.EatSpore(spore);
    }

    public CSpore SpitSpore()
    {
        CSpore spore = null;

        float fMotherVolume = GetVolume();
        float fChildVolume = fMotherVolume * CSkillSystem.s_Instance.m_SpitSporeParam.fCostMotherVolumePercent;

        if (fChildVolume < CSkillSystem.s_Instance.m_SpitSporeParam.fSporeMinVolume)
        {
            fChildVolume = CSkillSystem.s_Instance.m_SpitSporeParam.fSporeMinVolume;
        }

        if (fChildVolume > CSkillSystem.s_Instance.m_SpitSporeParam.fSporeMaxVolume)
        {
            fChildVolume = CSkillSystem.s_Instance.m_SpitSporeParam.fSporeMaxVolume;
        }

        float fMotherLeftVolume = fMotherVolume - fChildVolume;

        if (fMotherLeftVolume < Main.m_fBallMinVolume )
        {
            return null;
        }

        spore = CSporeManager.s_Instance.NewSpore();

        spore.SetSporeId( _Player.GenerateSporeGuid() );
        spore.SetPlayerId(_Player.GetOwnerId());
        spore.SetColor( _Player.GetColor() );
        spore.SetBall( this );

        CSporeManager.s_Instance.AddSpore(spore);

        spore.SetVolume(fChildVolume);

        vecTempPos = GetPos();
        if (GetDir().x == 0 && GetDir().y == 0)
        {
            vecTempDir = m_vecLastDirection;
        }
        else
        {
            vecTempDir = GetDir();
        }

        if (vecTempDir.x == 0 && vecTempDir.y == 0)
        {
                vecTempDir.x = 1f;
                vecTempDir.y = 1f;
          
        }

        vecTempPos.x += vecTempDir.x * ( this.GetRadius() + spore.GetSize());
        vecTempPos.y += vecTempDir.y * ( this.GetRadius() + spore.GetSize());

        spore.SetPos(vecTempPos);

        float fDis = CSkillSystem.s_Instance.m_SpitSporeParam.fRadiusMultiple * this.GetRadius();
        //vecTempPos.x += fDis * GetDir().x;
        //vecTempPos.y += fDis * GetDir().y;
        //spore.BeginEject(vecTempPos, CSpore.eEjectMode.accelerated);
        spore.BeginEject(fDis, CSkillSystem.s_Instance.m_SpitSporeParam.fEjectSpeed, vecTempDir, CSpore.eEjectMode.accelerated);
        
        this.SetVolume(fMotherLeftVolume);

        return spore;
    }

    public bool SpitBean(float fRealTimeSpitSporeDistance, string szMonsterId, uint uPlayerSporeGuid)
    {
        CMonsterEditor.sThornConfig config = CMonsterEditor.s_Instance.GetMonsterConfigById(szMonsterId/*Main.s_Instance.m_szSpitSporeBeanType*/ );

        float fMotherSize = GetSize();
        float fMotherCurVolume = GetVolume();
        float fMotherLeftVolume = fMotherCurVolume - config.fFoodSize; // foodsize是体积値
        if (fMotherLeftVolume <= 0)
        {
            return false;
        }
        float fMotherLeftSize = CyberTreeMath.Volume2Scale(fMotherLeftVolume);
        if (fMotherLeftSize < Main.BALL_MIN_SIZE)
        {
            return false;
        }

        CMonster spore = ResourceManager.s_Instance.ReuseMonster(); // 孢子没必要具有网络属性，就是每个客户端的单机版
        if (spore == null)
        {
            return false;
        }

        spore.SetGuid((long)uPlayerSporeGuid);
        spore.SetPlayer(_Player);
        spore.SetMonsterBuildingType(CMonsterEditor.eMonsterBuildingType.spore);
        spore.SetConfigId(szMonsterId);
        spore.SetAutoReborn(false);
        spore.SetMonsterType(CMonster.eMonsterType.spit_spore);
        spore.transform.parent = Main.s_Instance.m_goSpores.transform;
        Local_SetSize(fMotherLeftSize);
        spore.SetDir(GetDir());
        vecTempPos = GetPos(); // 孢子从球球的正中心出来
                               //vecTempPos.x += (GetRadius() + spore.GetDiameter()) * GetDir().x;
                               //vecTempPos.y += (GetRadius() + spore.GetDiameter()) * GetDir().y;
        vecTempPos.z = 0f;
        spore.SetPos(vecTempPos);
        fRealTimeSpitSporeDistance += GetRadius();
        spore.BeginEject(fRealTimeSpitSporeDistance/*Main.s_Instance.m_fSpitSporeRunDistance*/, 0.5f/*Main.s_Instance.m_fSpitRunTime*/ );
        spore.SetBall(this);
        CClassEditor.s_Instance.AddSpore(uPlayerSporeGuid, (uint)_Player.GetOwnerId(), spore);

        spore.SetColor( _Player.GetColor() );

        return true;
    }

    // 废弃。没有“孢子”的概念了。就吐豆子SpitBean
    /*
	public Spore SpitSpore()
	{
		float fMotherSize = GetSize();
		float fMotherLeftSize = 0.0f;
		if (!CheckIfCanSpitBall (fMotherSize, Main.BALL_MIN_SIZE, ref fMotherLeftSize)) {
			return null;
		}
		Main.s_Instance.s_Params [0] = Main.eInstantiateType.Spore;
		Main.s_Instance.s_Params [1] = 444;
		GameObject goSpore = Main.s_Instance.PhotonInstantiate (Main.s_Instance.m_preSpore, Vector3.zero, Main.s_Instance.s_Params );//GameObject.Instantiate( Main.s_Instance.m_preSpore );
		Spore spore = goSpore.GetComponent<Spore>();
		goSpore.transform.parent = Main.s_Instance.m_goSpores.transform;
		spore._srMain.color = this._srMain.color;
		SetSize ( fMotherLeftSize );
        spore.SetDir( _dirx, _diry );
        vecTempPos = GetPos();
        vecTempPos.x += ( GetRadius() + spore.GetRadius() ) * _dirx;
        vecTempPos.y += ( GetRadius() + spore.GetRadius() ) * _diry;
        vecTempPos.z = -spore.GetSize();
        spore.SetPos(vecTempPos);
        float fSpitBallInitSpeed = CyberTreeMath.GetV0 ( Main.s_Instance.m_fSpitSporeRunDistance, Main.s_Instance.m_fSpitRunTime  );
        float fSpitBallAccelerate = CyberTreeMath.GetA (  Main.s_Instance.m_fSpitSporeRunDistance, Main.s_Instance.m_fSpitRunTime);
        spore.BeginEject ( fSpitBallInitSpeed, fSpitBallAccelerate, Main.s_Instance.m_fSpitRunTime );
		return spore;

	}
	*/



    public float GetBoundsSize()
    {
        return _srMouth.bounds.size.x; // ;_Trigger.bounds.size.x;
    }

    public Vector2 GetDir()
    {
        return m_vecdirection;
    }


    public void SetDir(Vector2 dir)
    {
        m_vecdirection = dir;
    }

    public Vector3 GetPos()
    {
        return this.gameObject.transform.position;
    }

    public void GetPos(ref float fX, ref float fY)
    {
        fX = this.gameObject.transform.position.x;
        fY = this.gameObject.transform.position.y;
    }

    public float GetPosX()
    {
        return GetPos().x;
    }

    public float GetPosY()
    {
        return GetPos().y;
    }

    public void SetLocalPosition(Vector3 pos)
    {
        this.transform.localPosition = pos;
    }

    public Vector3 GetLocalPosition()
    {
        return this.transform.localPosition;
    }

    public void SetPos(Vector3 pos)
    {
        Local_SetPos(pos);
    }


    public void Local_SetPos(float fX, float fY, float fZ)
    {
        vecTempPos.x = fX;
        vecTempPos.y = fY;
        vecTempPos.z = fZ;
        this.gameObject.transform.position = vecTempPos;
    }

    public void Local_SetPos(Vector3 pos)
    {
        this.gameObject.transform.position = pos;
    }
    

    float m_fMotherLeftSize = 0.0f;
    float m_fChildThornSize = 0.0f;
    float m_fMotherLeftThornSize = 0.0f;
    public void CalculateChildSize(float fForceSpitPercent)
    {
        float fMotherVolume = GetVolume();
        float fChildVolume = fForceSpitPercent * fMotherVolume;
        float fChildSize = CyberTreeMath.Volume2Scale(fChildVolume);
        float fMotherLeftVolume = fMotherVolume - fChildVolume;
        if (fMotherLeftVolume < 0)
        {
            fMotherLeftVolume = 0;
        }
        float fMotherLeftSize = CyberTreeMath.Volume2Scale(fMotherLeftVolume);
        m_fChildSize = fChildSize;
        m_fMotherLeftSize = fMotherLeftSize;
        if (m_fChildSize < Main.BALL_MIN_SIZE || m_fMotherLeftSize < Main.BALL_MIN_SIZE || m_fMotherLeftSize < m_fChildSize) // 如果按百分比分球失败了，就按最小尺寸分
        {
            //CalculateChildSize_ByVolume(Main.m_fBallMinVolume);
            CalculateChildSize_BySize(Main.BALL_MIN_SIZE);
        }
    }

    public bool CalculateForceSpitBallVolumeInfo(float fForceSpitPercent, ref float fMotherLeftVolume, ref float fChildVolume)
    {
        float fMotherVolume = GetVolume();
        fChildVolume = fForceSpitPercent * fMotherVolume;
        if (fChildVolume < Main.m_fBallMinVolume)
        {
            fChildVolume = Main.m_fBallMinVolume;
        }
        fMotherLeftVolume = fMotherVolume - fChildVolume;
        if (fMotherLeftVolume < Main.m_fBallMinVolume)
        {
            fChildVolume = Main.m_fBallMinVolume;
            fMotherLeftVolume = fMotherVolume - fChildVolume;
            if (fMotherLeftVolume < Main.m_fBallMinVolume)
            {
                return false;
            }
        }
        return true;
    }

    public void CalculateChildSize_ByVolume(float fMinVolume)
    {
        float fMotherVolume = GetVolume();
        float fChildVolume = fMinVolume;
        float fChildSize = CyberTreeMath.Volume2Scale(fChildVolume);
        float fMotherLeftVolume = fMotherVolume - fChildVolume;
        if (fMotherLeftVolume < 0)
        {
            fMotherLeftVolume = 0;
        }
        float fMotherLeftSize = CyberTreeMath.Volume2Scale(fMotherLeftVolume);
        m_fChildSize = fChildSize;
        m_fMotherLeftSize = fMotherLeftSize;
    }

    public void CalculateChildSize_BySize(float fMinSize)
    {
        float fMotherVolume = GetVolume();
        float fRadius = 0;
        float fChildVolume = CyberTreeMath.Scale2Volume(fMinSize, ref fRadius);
        float fMotherLeftVolume = fMotherVolume - fChildVolume;
        if (fMotherLeftVolume < 0)
        {
            fMotherLeftVolume = 0;
        }
        float fMotherLeftSize = CyberTreeMath.Volume2Scale(fMotherLeftVolume);
        m_fChildSize = fMinSize;
        m_fMotherLeftSize = fMotherLeftSize;
    }

    public static float CalculateChildSize( float fMotherSize, float fPercent, ref float fRadius)
    {
        float fMotherVolume = CyberTreeMath.Scale2Volume(fMotherSize, ref fRadius);
        float fChildVolume = fMotherVolume * fPercent;
        float fChildSize = CyberTreeMath.Volume2Scale(fChildVolume);
        return fChildSize;
    }

    public static float CalculateChildSize_BySize(float fMotherSize, float fChildSize, ref float fRadius)
    {
        float fMotherVolume = CyberTreeMath.Scale2Volume(fMotherSize, ref fRadius);
        float fChildRadius = 0;
        float fChildVolume = CyberTreeMath.Scale2Volume(fChildSize, ref fChildRadius);
        float fMotherLeftSize = CyberTreeMath.Volume2Scale(fMotherVolume - fChildVolume);
        return fMotherLeftSize;
    }

    public void ClearAvailableChildSize()
    {
        m_fChildSize = 0.0f;
    }

    public float GetAvailableChildSize(ref float fMotherLeftSize)
    {
        fMotherLeftSize = m_fMotherLeftSize;
        return m_fChildSize;
    }

    bool m_bShell = false;
    float m_fShellShrinkCurTime = -1f;
    float m_fShellTotalTime = 0f;


    public void Local_SetShellInfo(float fTotalTime, float fStarTime, eShellType type = (eShellType)0)
    {
        m_fShellTotalTime = fTotalTime;
        m_fShellShrinkCurTime = fStarTime;
        SetCurShellType(type);
    }

    public enum eShellType
    {
        none,
        w_spilt_ball, // w键吐球
        r_split_ball, // r键分球
        explode_ball, // 炸球
    };
    eShellType m_eShellType = eShellType.none;

    public eShellType GetCurShellType()
    {
        return m_eShellType;
    }

    public void SetCurShellType(eShellType type)
    {
        m_eShellType = type;
    }

    public float GetCurShellTime()
    {
        return m_fShellShrinkCurTime;
    }

    public float GetTotalShellTime()
    {
        return m_fShellTotalTime;
    }

    public void UpdateShell()
    {
        if ( !m_bShell )
        {
            return;
        }

        m_msDynamicShell.SetAngle1( m_fEndAngle);
        m_msDynamicShell.drawPolygon();
    }


    public void BeginUnfoldQianYao( float fScale )
    {
        _goEffectContainer_QianYao.gameObject.SetActive( true );
        _effectUnfoldQianYao.gameObject.SetActive(true);
        vecTempScale.x = fScale;
        vecTempScale.y = fScale;
        vecTempScale.z = 1f;
        _goEffectContainer_QianYao.transform.localScale = vecTempScale;
        _effectUnfoldQianYao.SetAngle1(0);
        _effectUnfoldQianYao.drawPolygon();
    }

    public void UpdateUnfoldQianYao(float fEndAngle)
    {
        _effectUnfoldQianYao.SetAngle1(fEndAngle);
        _effectUnfoldQianYao.drawPolygon();
    }

    public void EndPreUnfold()
    {
        _goEffectContainer_QianYao.gameObject.SetActive(false);
        _effectUnfoldQianYao.gameObject.SetActive(false);
    }

    float m_fBallTestValue = 0f;
    public void UpdateValue( float fTestValue )
    {
        m_fBallTestValue = fTestValue;
    }

    public void SetStaticShellVisible( bool bVisible )
    {
        m_msStaticShell.gameObject.SetActive(bVisible);
    }

    bool m_bDynamicShellVisible = true;
    public void SetDynamicShellVisible( bool bVisible )
    {
        m_bDynamicShellVisible = bVisible;
    }

    public bool IsDynamicShellVisible()
    {
        return m_bDynamicShellVisible;
    }

    float m_fShellUnfoldScale = 0.1f;
    public void Local_BeginShell()
    {
        /*
        if ( IsEjecting() )
        {
            return;
        }
        */
        if (m_fShellTotalTime <= 0f)
        {
            return;
        }

        if (IsDynamicShellVisible())
        {
            m_msDynamicShell.gameObject.SetActive(true);
        }

        m_bShell = true;

            _Collider.enabled = true;


        
        //_TriggerMergeSelf.enabled = false;
    }

    // 临时启用壳
    float m_fTempShellCount = 0f;
    public void Local_Begin_Shell_Temp()
    {
        _ColliderTemp.enabled = true;
        m_fTempShellCount = 0.5f;
    }

    void TempShellLOop()
    {
        if (m_fTempShellCount == 0f)
        {
            return;
        }

        m_fTempShellCount -= Time.deltaTime;
        if (m_fTempShellCount <= 0f)
        {
            _ColliderTemp.enabled = false;
            m_fTempShellCount = 0f;
            return;
        }

    }

    public bool HaveShell()
    {
        return m_bShell;
    }

    float m_fEndAngle = 0f;
    void Shell()
    {
        if (m_fShellTotalTime <= 0)
        {
            return;
        }

        if (!m_bShell)
        {
            return;
        }

        if (IsDead())
        {
            return;
        }


        m_fShellShrinkCurTime += Time.deltaTime;

        float fPercent = m_fShellShrinkCurTime / m_fShellTotalTime;


        // 最新的壳机制
        m_fEndAngle = 360f * (1 - fPercent);
        if (m_fEndAngle <= 0f)
        {
            m_fEndAngle = 0.01f;
        }

        //m_msDynamicShell.SetAngle1(fEndAngle);
        //m_msDynamicShell.drawPolygon();
      
        if (m_fShellShrinkCurTime >= m_fShellTotalTime)
        {
            Local_EndShell();
            return;
        }

    }

    public float GetCurLeftShellTime()
    {
        return m_fShellTotalTime - m_fShellShrinkCurTime;
    }

    [PunRPC]
    public void RPC_EndShell()
    {
        Local_EndShell();
    }

    void EndShell()
    {
        //photonView.RPC ( "RPC_EndShell", PhotonTargets.All );
    }

    public void Local_EndShell()
    {
        if (_Player && _Player.IsMainPlayer())
        {
            _TriggerMergeSelf.enabled = true;
        }
        _Collider.enabled = false;
        m_bShell = false;
        m_fShellShrinkCurTime = 0f;
        m_fShellTotalTime = 0f;

        vecTempScale.x = s_vecShellInitScale.x;
        vecTempScale.y = s_vecShellInitScale.x;
        vecTempScale.z = 1f;
        //_shell.transform.localScale = vecTempScale;
        SetShellSize(vecTempScale);

        /*
		cTempColor = _srShellForAlpha.color;
		cTempColor.a = 0.0f;
		_srShellForAlpha.color = cTempColor;
		*/
        if (_shell)
        {
            _shell.gameObject.SetActive(false);
        }

        //_mrShellCircle.gameObject.SetActive(false);
        //_arcShellZouQuan.gameObject.SetActive( false );
        m_msDynamicShell.gameObject.SetActive( false );


        m_eShellType = eShellType.none;
    }

    void SetShellSize(Vector3 vecScale)
    {
        if (!_shell)
        {
            return;
        }
        _shell.transform.localScale = vecScale;
        //_shellForAlpha.transform.localScale = vecScale;
    }

    public float m_fSpitBallInitSpeed = 5.0f;           // 分球的初速度
    public float m_fSpitBallAccelerate = 0.0f;         // 吐孢子的加速度(这个值通过距离、初速度、时间 算出来，不用配置) 

    public float m_fExplodeInitSpeed = 0.0f;
    public float m_fExplodeInitAccelerate = 0.0f;

    SpitBallTarget m_SpitBallTarget = null;
    float m_fSpitBallTargetParam = 0f;
    bool IfCanShowTarget()
    {
        if (IsEjecting())
        {
            return false;
        }


        return true;
    }

    public static void RongCuo_NotPointToMyself(ref float dirx, ref float diry)
    {
        // 容错，不准往自己身上吐
        if (dirx == 0 && diry == 0f)
        {
            dirx = 0.5f;
            diry = 0.5f;
        }
    }

    bool m_bTargeting = false;
    public void ClearSpitTarget()
    {
        /*
        m_bTargeting = false;

        if (m_SpitBallTarget)
        {
            m_SpitBallTarget.SetBall(null);
            m_SpitBallTarget.SetVisible(false);
        }

        _Dick.gameObject.SetActive(false);
        */
    }

    public void ShowTarget(float fSpitBallDistance)
    {
        /*
        if (!IfCanShowTarget())
        {
            return;
        }

        if (m_SpitBallTarget == null)
        {
            m_SpitBallTarget = ResourceManager.ReuseTarget();
            m_SpitBallTarget.gameObject.transform.parent = Main.s_Instance.m_goSpitBallTargets.transform;
        }
        m_SpitBallTarget.gameObject.SetActive(true);
        m_SpitBallTarget.SetBall(this);

        float fMotherSize = GetSize();
        float fRadiusMother = GetBoundsSize() / 2.0f;

        float fMotherLeftSize = 0.0f;
        float fChildSize = GetAvailableChildSize(ref fMotherLeftSize);
        if (fChildSize < Main.BALL_MIN_SIZE || fMotherLeftSize < Main.BALL_MIN_SIZE || fMotherLeftSize < fChildSize)
        {
            m_SpitBallTarget.gameObject.SetActive(false);
            return;
        }
        else
        {
            m_SpitBallTarget.gameObject.SetActive(true);
        }

        m_SpitBallTarget.SetSize(fChildSize);
        float dirx = 0f;
        float diry = 0f;
        if (Main.s_Instance.m_nPlatform == 0)
        { // PC
            if (_Player.IsMainPlayer())
            {
                dirx = _dirx;
                diry = _diry;
            }
            else
            {
                dirx = m_vecdirection.x;
                diry = m_vecdirection.y;
            }
        }
        else if (Main.s_Instance.m_nPlatform == 1)
        { // Mobile Phone
            if (_Player.IsMainPlayer())
            {
                dirx = Main.s_Instance.GetSpitDirection().x;
                diry = Main.s_Instance.GetSpitDirection().y;
            }
            else
            {
                dirx = _Player.m_vSpecialDirecton.x;
                diry = _Player.m_vSpecialDirecton.y;
            }
        }
        if (dirx == 0f && diry == 0f)
        {
            dirx = _dirx;
            diry = _diry;
        }

        RongCuo_NotPointToMyself(ref dirx, ref diry);

        vecTempPos = GetPos();
        float fDis = fSpitBallDistance * fRadiusMother;
        vecTempPos.x += fDis * dirx;
        vecTempPos.y += fDis * diry;
        m_SpitBallTarget.SetPos(vecTempPos);

        _goDick.SetActive(true);
        vecTempScale.x = fDis / Main.s_Instance.m_fPixel2Scale / GetSize();
        vecTempScale.y = fChildSize / GetSize();
        vecTempScale.z = 1f;
        _Dick._goCyliner.transform.localScale = vecTempScale;

        vecTempScale.x = fChildSize / GetSize();
        vecTempScale.y = fChildSize / GetSize();
        vecTempScale.z = 1f;
        //_Dick._goCircle.transform.position = vecTempPos;
        vecTempPos2.x = fDis / GetSize();
        vecTempPos2.y = 0;// fDis * diry /   GetSize();
        vecTempPos2.z = 0f;
        _Dick._goCircle.transform.localScale = vecTempScale;
        _Dick._goCircle.transform.localPosition = vecTempPos2;

        m_bTargeting = true;
        */
    }

    public bool IsTargeting()
    {
        return m_bTargeting;
    }

    SpitBallTarget m_SplitBallTarget = null;
    public SpitBallTarget GetSplitTarget()
    {
        return m_SplitBallTarget;
    }

    public void UpdateSplitTarget()
    {

    }

    public void ShowSplitTarget(float fCurOneBtnSplitDis)
    {
        if (m_SpitBallTarget == null)
        {
            m_SpitBallTarget = ResourceManager.ReuseTarget();
            m_SpitBallTarget.gameObject.transform.parent = Main.s_Instance.m_goSpitBallTargets.transform;
        }
        m_SpitBallTarget.gameObject.SetActive(true);

        float fSize = GetSize();
        vecTempScale.x = fSize;
        vecTempScale.y = fSize;
        vecTempScale.z = 1.0f;
        m_SpitBallTarget.gameObject.transform.localScale = vecTempScale;

        float dirx = 0f;
        float diry = 0f;
        dirx = GetDir().x;
        diry = GetDir().y;
        RongCuo_NotPointToMyself(ref dirx, ref diry);


        vecTempPos = GetPos();
        vecTempPos.x += fCurOneBtnSplitDis * dirx;
        vecTempPos.y += fCurOneBtnSplitDis * diry;
        //	Debug.Log (  fCurOneBtnSplitDis);

        m_SpitBallTarget.gameObject.transform.localPosition = vecTempPos;
        //	MapEditor.s_Instance._txtDebugInfo.text = m_SplitBallTarget.gameObject.transform.localPosition.ToString() + " _ " + vecTempPos.ToString ();

    }

    public void CalculateSpitBallRunParams(float fRadiusMother, float fRadiusChild, float fMultiple, ref float fInitSpeed, ref float fAccelerate)
    {
        float fRunDistance = fMultiple * fRadiusMother;
        fInitSpeed = CyberTreeMath.GetV0(fRunDistance, Main.s_Instance.m_fSpitRunTime);
        fAccelerate = CyberTreeMath.GetA(fRunDistance, Main.s_Instance.m_fSpitRunTime);
    }

    public SpitBallTarget _relateTarget = null;
    public void RelateTarget(Ball ball)
    {
        return; // 废弃

        ball._relateTarget = m_SpitBallTarget;
    }

    public SpitBallTarget _relateSplitTarget = null;
    public void RelateSplitTarget(SpitBallTarget target)
    {
        _relateSplitTarget = target;
    }

    public void RefeshPosDueToOutOfScreen()
    {
        /*		
                if (_balltype == eBallType.ball_type_thorn && _isSpited) {
                    return;
                }
        */
        if (!CheckIfOutOfScreen())
        {
            return;
        }

        this.gameObject.transform.position = Main.s_Instance.RandomPosWithinScreen();
    }

    public bool CheckIfOutOfScreen()
    {
        Vector3 vecScreenPos = Camera.main.WorldToScreenPoint(this.gameObject.transform.position);

        if (vecScreenPos.x < 0 || vecScreenPos.x > Screen.width || vecScreenPos.y < 0 || vecScreenPos.y > Screen.height)
        {
            return true;
        }

        return false;
    }



    public  void DoAttenuate( float fEatVolumeTotal)
    {
        float fCurVolume = GetVolume();
        float fEatVolumeMe = fCurVolume / _Player.GetTotalVolume() * fEatVolumeTotal;

        float fAttenuate = CClassEditor.s_Instance.m_fAttenuateRate * fEatVolumeMe; // 每个球的衰减率可能不一样。因为就算是同一个玩家的球，也可能出在不同的衰减区
        if (fAttenuate == 0f)
        {
            return;
        }
        fCurVolume += fAttenuate;
        SetVolume( fCurVolume );
        /*
        float fCurSize = CyberTreeMath.Volume2Scale(fCurVolume);
        if (fCurSize < Main.BALL_MIN_SIZE)
        {
            fCurSize = Main.BALL_MIN_SIZE;
        }
        Local_SetSize(fCurSize);*/
    }

    void ShowSize()
    {
        /*
        if (_text)
        {
            string szInfo = "半径" + GetRadius().ToString("f2") + "米\n";
            szInfo += "速度:" + _ba.GetRealTimeVelocity().ToString("f2");

            _text.text = szInfo;

            // _text.text = m_nIndex + " : " + GetPos().x.ToString("f1") + " , " + GetPos().y.ToString("f1");


        }
        */

    }


    public void EatGrassSeed(int nGrassGUID)
    {
        //  photonView.RPC("RPC_EatGrassSeed", PhotonTargets.AllBuffered, this.photonView.ownerId, nGrassGUID, PhotonNetwork.time );
    }

    [PunRPC]
    public void RPC_EatGrassSeed(int nOwnerId, int nGrassGUID, double dCurTime)
    {
        Local_EatGrassSeed(nOwnerId, nGrassGUID, dCurTime);
    }

    public void Local_EatGrassSeed(int nOwnerId, int nGrassGUID, double dCurTime)
    {
        MapEditor.s_Instance.ProcessGrassSeed(nOwnerId, nGrassGUID, dCurTime);
    }

   

    public bool DoNotSyncMoveInfo()
    {
        if (IsEjecting())
        {
            return true;
        }

        return false;
    }

    bool m_bDoNotMove = false;
    public void SetDoNotMove( bool bDoNotMove )
    {
        m_bDoNotMove = bDoNotMove;
    }

    public bool DoNotMove()
    {
        if ( !IsMainPlayer() )
        {
            return m_bDoNotMove; // Other client不需要关注过于具体的逻辑，只需要被动接受MainPlayer同步过来的指示即可
        }

        if (IsStaying())
        {
            return true;
        }

        if (IsEjecting())
        {
            return true;
        }

        return false;
    }

    public void SetAdjustInfo_InGroup(float fPosX, float fPosY)
    {
        vecTempPos.x = fPosX;
        vecTempPos.y = fPosY;
        vecTempPos.z = -GetSize();
        SetLocalPosition(vecTempPos);
    }

    float m_fAdjustSpeedX = 0f;
    float m_fAdjustSpeedY = 0f;

    public void SetAdjustInfo(float fPosX, float fPosY, int nDirect = 0)
    {
        vecTempPos = GetPos();
        vecTempPos.x = fPosX;
        vecTempPos.y = fPosY;
        SetPos(vecTempPos);
    }


    float m_fExtraSpeedX = 0f;
    float m_fExtraSpeedY = 0f;

    void SetExtraSpeedX(float fDeltaX)
    {
        m_fAdjustSpeedX = fDeltaX;
    }

    void ExtraMoveX()
    {
        if (m_fExtraSpeedX == 0f)
        {
            return;
        }


    }

    void SetExtraSpeedY(float fDeltaY)
    {
        m_fAdjustSpeedY = fDeltaY;
    }


    void UpdateSpitDir()
    {
        if (!Main.s_Instance.IsForceSpitting())
        {
            return;
        }

        if (Main.s_Instance.m_nPlatform != 1) // 这种“多点触控分球”只针对手机版，不针对PC版
        {
            return;
        }

        //MapEditor.s_Instance._txtDebugInfo.text = Main.s_Instance.GetSpitTouchTargetPos ().ToString ();
        //_vecSpitDir = Main.s_Instance.GetSpitTouchTargetPos () - this.GetPos ();
        //_vecSpitDir.Normalize ();
    }

    public void CanceSplit()
    {
    }

    public void CancelSpit()
    {
        /*
		if (_relateTarget) {
			DestroyTarget ( _relateTarget );
		}

		if (_relateSplitTarget) {
			DestroyTarget ( _relateSplitTarget );
		}

		if (m_SpitBallTarget) {
			DestroyTarget ( m_SpitBallTarget );
		}

		if (m_SplitBallTarget) {
			DestroyTarget ( m_SplitBallTarget );
		}
		*/
        if (m_SpitBallTarget)
        {
            m_SpitBallTarget.SetVisible(false);
        }
    }

    public bool CheckIfCanSpitHalf()
    {
        if (IsEjecting())
        {
            return false;
        }

        if (IsDead())
        {
            return false;
        }

        return true;
    }

    public void SpitHalf(float fDelayTime, float fChildSize, float fDistance, float fRunTime, float fShellTime)
    {
        /*
        if (!CheckIfCanSpitHalf())
        {
            return;
        }

        float fRadiusMother = GetRadius();
        Ball new_ball = _Player.ReuseOneBall();
        if (new_ball == null)
        {
            return;
        }

        float fRealRunTime = fRunTime - fDelayTime;
        if (fRealRunTime < Main.s_Instance.m_fMinRunTime)
        {
            fRealRunTime = Main.s_Instance.m_fMinRunTime;
        }

        float fRunDistance = fDistance * fRadiusMother;

        vecTempPos = GetPos();

        if (_Player.IsMainPlayer())
        {
            //	Main.s_Instance.g_SystemMsg.SetContent ("二分分球，配置壳时间：" + Main.s_Instance.m_fShellShrinkTotalTime + ", Buff加成后时间:" + fShellTime);
        }
        new_ball.Local_SetShellInfo(fShellTime, 0f, eShellType.w_spilt_ball);
        new_ball.Local_SetPos(vecTempPos);
        new_ball.Local_SetSize(fChildSize);
        new_ball.SetDir(GetDir());
        float fSpeed = fRunDistance / fRealRunTime;
        new_ball.Local_BeginEject_New(fSpeed, fRunDistance);

        Local_SetShellInfo(fShellTime, 0f, eShellType.w_spilt_ball);
        Local_BeginShell();
        Local_SetSize(fChildSize);
        */
    }

    public bool DoNotSpit()
    {
        if ( IsEjecting() )
        {
            return true;
        }

        return false;
    }

    public bool IsInCamView()
    {
        vecTempPos = GetPos();
        float fLeft = vecTempPos.x - m_fRadius;
        return Main.s_Instance.IsBallInCamView(fLeft, vecTempPos.x, vecTempPos.y);
    }

    public const float EJECT_SPEED_XISHU = 15f;

    public static float GetRunDistance( float fMultiple, float fRadius )
    {
        float fDis = fMultiple * CyberTreeMath.KaiGenHao(MapEditor.s_Instance.m_fEjectParamA * fRadius, MapEditor.s_Instance.m_fEjectParamX) + MapEditor.s_Instance.m_fEjectParamB;
        return fDis;
        //return ( fMultiple * Mathf.Sqrt(2 * fRadius) );
    }

    public Ball SpitBall(Ball ballChild, float fMotherLeftVolume, float fChildVolume, int nPlatform, float fWorldCursorPosX, float fWorldCursorPosY, float fSpeicalDirX, float fSpeicalDirY, float fSpitBallRunDistance, float fSpitBallRunTime, float fShellTime)
    {
        /*
        // 如果不是MainPlayer，且不在可视范围内，则不执行分球操作。分出来的新球自然会在“主同步流程”中生成
        if ( !_Player.IsMainPlayer() )
        {
            if ( !IsInCamView() )
            {
                return;
            }
        }*/

        vecTempPos = GetPos();
        vec2Temp.x = fWorldCursorPosX;
        vec2Temp.y = fWorldCursorPosY;
        vec2Temp.x -= vecTempPos.x;
        vec2Temp.y -= vecTempPos.y;
        vec2Temp.Normalize();

        if (vec2Temp.x == 0 && vec2Temp.y == 0 )
        {
            int shit = 123;
        }

        if ( nPlatform == (int)Main.ePlatformType.mobile)
        {
            vec2Temp1.x = fSpeicalDirX;
            vec2Temp1.y = fSpeicalDirY;
            if (vec2Temp1.x == 0 && vec2Temp1.y == 0)
            {

            }
            else
            {
                vec2Temp = vec2Temp1;
            }
        }

        // poppin test
        //float fRunDistance = fSpitBallRunDistance * GetRadius();
        float fRunDistance = GetRunDistance(fSpitBallRunDistance, GetRadius());// fSpitBallRunDistance * Mathf.Sqrt( 2 * GetRadius() );

      

        ballChild.Local_SetShellInfo(fShellTime, 0f, eShellType.w_spilt_ball);
        ballChild.Local_SetPos(vecTempPos);

        //ballChild.Local_SetSize(fChildSize);
        ballChild.SetVolume( fChildVolume );


        ballChild.SetSizeDirectly();
        ballChild.SetDir(vec2Temp);
        float fSpeed = fSpitBallRunTime;// > 10f ? fSpitBallRunTime :10f;

        ballChild.SetMotherStatus(GetDir(), GetRealTimeSpeed() );
        ballChild.BeginEject_BySpeed( fRunDistance, fSpeed, 0, false, true);
        //ballChild.BeginEject(fRunDistance, fSpitBallRunTime, vec2Temp, eEjectMode.accelerated);


        Local_SetShellInfo(fShellTime, 0f, eShellType.w_spilt_ball);
        Local_BeginShell();

        //Local_SetSize(fMotherSize);
        SetVolume( fMotherLeftVolume );

        ballChild.SetRingVisible( false );

        // 播放球体的喷射动画
        //CSpineManager.s_Instance.SetBallAnimationRotation(ballChild, CyberTreeMath.Dir2Angle(ballChild.GetDir().x, ballChild.GetDir().y));
        //CSpineManager.s_Instance.PlayBallAnimation(ballChild, CSpineManager.eBallSpineType.split, false, 3f);

        //_effectSpitSmoke.BeginPlay(false);
        // 喷射血迹
        /*
        vecTempPos = this.GetPos();
        vecTempPos.x += this.GetRadius() * this.GetDir().x ;
        vecTempPos.y += this.GetRadius() * this.GetDir().y ;
        vecTempDir = this.GetDir();
        float k = 8.61f;
        float t = fRunDistance / fSpeed * 1.1f;
        float fScaleX = fRunDistance / k * 0.9f;
        float fScaleY = ballChild.GetSize() * 0.5f;
        CBloodStainManager.s_Instance.AddBloodStain(vecTempPos, vecTempDir, fScaleX, fScaleY, CBloodStainManager.eBloodStainType.spit, this._Player.GetColor(), CBloodStainManager.eDynamicEffectType.fill, t, CBloodStainManager.eFadeMode.fill);
        */



        return ballChild;
    }

    public void SetRingVisible(bool bVisible)
    {
        /*
        _arcShellChangTai.gameObject.SetActive(bVisible);
        */
    }
    public float GetMoveSpeedX()
    {
        return m_fMoveSpeedX;
    }

    public float GetMoveSpeedY()
    {
        return m_fMoveSpeedY;
    }


    float m_fMoveSpeedX = 0.0f;
    float m_fMoveSpeedY = 0.0f;
    Vector2 m_vecdirection = new Vector2();
    public Vector2 m_vecLastDirection = new Vector2();
    public void BeginMove(Vector3 vecWorldCursorPos, float fRealTimeSpeedWithoutSize)
    {
        if ( DoNotMove() )
        {
            return;
        }

        m_vecWorldCursorPos = vecWorldCursorPos;


        m_vecdirection = vecWorldCursorPos - this.transform.position;
        m_vecdirection.Normalize();

        bool bVAlid = false;
        float fSpeed = _ba.GetRealSpeedBySize(fRealTimeSpeedWithoutSize, ref bVAlid);
        if ( !bVAlid)
        {
            return;
        }

      
		float fShitDis = Vector2.Distance (vecWorldCursorPos, GetPos ());
		if ( fShitDis < Main.s_Instance.m_fDistanceToSlowDown) {

			fSpeed = fSpeed * fShitDis / Main.s_Instance.m_fDistanceToSlowDown;
		}
        

        fSpeed = BallAction.ProcessSomethingAffectVelocity(this, fSpeed);

        //// 限速：
        if (fSpeed > Main.s_Instance.m_fMaxPlayerSpeed)
        {
            fSpeed = Main.s_Instance.m_fMaxPlayerSpeed;
        }

        //// end 限速

        m_fSpeed = fSpeed;
        m_fMoveSpeedX = fSpeed * m_vecdirection.x + m_fAdjustSpeedX;
        m_fMoveSpeedY = fSpeed * m_vecdirection.y + m_fAdjustSpeedY;
    }

    public void SetMoveParams( float fDirX, float fDirY, float fRealTimeSpeed )
    {
        m_fMoveSpeedX = fRealTimeSpeed * fDirX;
        m_fMoveSpeedY = fRealTimeSpeed * fDirY;
    }

    public float GetRealTimeSpeed()
    {
        return _ba.GetRealTimeVelocity();
    }

    public float GetSpeed()
    {
        return m_fSpeed;
    }

    public void SetSpeed( float val )
    {
        m_fSpeed = val;
    }

    Vector3 m_vecWorldCursorPos = new Vector3();

    bool m_bFirstCalculateMoveInfo = true;
    Vector3 m_vecLastPos = new Vector3();
    public void CalculateMoveInfo()
    {
        if (m_bFirstCalculateMoveInfo)
        {
            m_bFirstCalculateMoveInfo = false;
            return;
        }


    }

    Vector3 m_vecInterpolateDest = new Vector3();
    Vector3 m_vecInterpolateSpeed = new Vector3();
    public void BeginInterpolate( Vector3 vecDest )
    {
        m_vecInterpolateDest = vecDest;
        m_vecInterpolateSpeed = ( m_vecInterpolateDest - GetPos() ) / 0.15f;

    }

    void InterpolateLoop()
    {
        vecTempPos = GetPos();
        vecTempPos.x += m_vecInterpolateSpeed.x * Time.fixedDeltaTime;
        vecTempPos.y += m_vecInterpolateSpeed.y * Time.fixedDeltaTime;
        SetPos(vecTempPos);
    }

    public void Move() // none-mainplayer
    {
        return; // poppin test 6.4

        if (_Player == null)
        {
            return;
        }

        if (_Player.photonView.isMine)
        {
            return;
        }

        if (!_Player.IsMoving())
        {
            return;
        }

        if (_Player.DoNotMove())
        {
            return;
        }

        if (IsDead())
        {
            return;
        }

        if (DoNotMove())
        {
            return;
        }

        
        float fRealTimeSpeedWithSize = _Player.GetRealTimeSpeedWithSize();
        _ba.UpdatePosition(_Player.m_vWorldCursorPos, fRealTimeSpeedWithSize);
        

        return;


        vecTempPos = GetPos();

        // poppin test
        float fShitDis = Vector2.Distance(m_vecWorldCursorPos, GetPos());
        if (fShitDis < Main.s_Instance.m_fDistanceToSlowDown)
        {
            float the_shit = fShitDis / Main.s_Instance.m_fDistanceToSlowDown;
            m_fMoveSpeedX = m_fMoveSpeedX * the_shit;
            m_fMoveSpeedY = m_fMoveSpeedY * the_shit;
        }
        

        float fDeltaX = m_fMoveSpeedX * Time.fixedDeltaTime;
        float fDeltaY = m_fMoveSpeedY * Time.fixedDeltaTime;

        bool bCanMoveX = true;
        bool bCanMoveY = true;

        MapEditor.s_Instance.CheckIfWillExceedWorldBorder(this, fDeltaX, fDeltaY, ref bCanMoveX, ref bCanMoveY);
        /*
        if (IsStayOnClassCircle() && IsSizeReachClassCircleThreshold())   
        {
            MapEditor.s_Instance.CheckIfCanCrossClassCircle(this, fDeltaX, fDeltaY, ref bCanMoveX, ref bCanMoveY);
        }
        */

        if (bCanMoveX)
        {
            vecTempPos.x += fDeltaX;
        }
        if (bCanMoveY)
        {
            vecTempPos.y += fDeltaY;
        }
        vecTempPos.z = -GetSize();
        
        if ( float.IsNaN(vecTempPos.x) || float.IsNaN(vecTempPos.y))
        {
            Debug.LogError("float.IsNaN(vecTempPos.x) || float.IsNaN(vecTempPos.y)");
            return;
        }

        //_rigid.MovePosition(vecTempPos);
        //_rigid.MoveRotation(0);
        SetPos(vecTempPos);

      
    }

    void MoveInGroup() // none-mainplayer
    {
        float fDeltaX = m_fMoveSpeedX * Time.fixedDeltaTime;
        float fDeltaY = m_fMoveSpeedY * Time.fixedDeltaTime;

        //// 检测是否超越了场景边界
        bool bCanMoveX = true;
        bool bCanMoveY = true;

        MapEditor.s_Instance.CheckIfWillExceedWorldBorder(this, fDeltaX, fDeltaY, ref bCanMoveX, ref bCanMoveY);
        //  end 检测是否超越了场景边界

        vecTempPos = GetLocalPosition();


        if (bCanMoveX)
        {
            vecTempPos.x += fDeltaX;
        }
        if (bCanMoveY)
        {
            vecTempPos.y += fDeltaY;
        }
        vecTempPos.z = -GetSize();
        SetLocalPosition(vecTempPos);
    }

    /*
	public void SyncFullInfo()
	{
		vecTempPos = GetPos ();
		//photonView.RPC ( "RPC_SyncFullInfo", PhotonTargets.Others, m_bDead, vecTempPos.x, vecTempPos.y, GetSize(), m_fShellShrinkCurTime, m_fShellTotalTime, GetThornSize() );
	}

	[PunRPC]
	public void RPC_SyncFullInfo( bool bDead, float fPosX, float fPosY, float fSize, float fShellCurTime, float m_fShellTotalTime, float fThornSize )
	{
		float fPosZ = -fSize;
		Local_SetPos ( fPosX,  fPosY, fPosZ);   
		Local_SetSize ( fSize );
		Local_SetThornSize ( fThornSize );
	
		if (fShellCurTime > 0f) {
			Local_SetShellInfo(m_fShellTotalTime, fShellCurTime);
			Local_BeginShell (); 
		}
	
		//Debug.Log ( "全量同步：" + photonView.viewID + " .... " + bDead );
	
		if (bDead) {
			SetDead (true);
			_Player.m_lstRecycledBalls.Remove ( this );
			_Player.m_lstBalls.Remove ( this );
			_Player.m_lstRecycledBalls.Add ( this );
		}
		else
		{
			SetDead (false);
			bool bNew = false;
			_Player.m_lstBalls.Remove ( this );
			_Player.m_lstRecycledBalls.Remove ( this );
			_Player.m_lstBalls.Add ( this );
		}
	}
	*/
    int m_nRealSplitNum = 0;
    float m_fSplitSize = 0f;
    float m_fSplitArea = 0f;
    float m_fLeftArea = 0f;
    float m_fTotalDis = 0f;
    bool m_bMotherDeadAfterSplit = false;

    public void CalculateSplitChildNumAndSize()
    {
        m_nRealSplitNum = 0;
        float fCurSize = GetSize();
        float fCurArea = CyberTreeMath.SizeToArea(fCurSize, Main.s_Instance.m_nShit);
        m_fLeftArea = fCurArea;
        for (int i = Main.s_Instance.m_nSplitNum; i >= 2; i--)
        {
            float fSmallArea = fCurArea / i;
            float fSmallSize = CyberTreeMath.AreaToSize(fSmallArea, Main.s_Instance.m_nShit);
            if (fSmallSize >= Main.BALL_MIN_SIZE)
            {
                m_nRealSplitNum = i;
                m_fSplitArea = fSmallArea;
                m_fSplitSize = fSmallSize;
                break;
            }
        } // end for i
    }

    public void CalculateRealSplitInfo(float fTotalDistance, ref int nRealSplitNum, ref float fRealSplitChildSize, ref float fSegDis)
    {
        float m_fTotalDis = fTotalDistance;
        if (m_fTotalDis > Main.s_Instance.m_fSplitMaxDistance)
        {
            m_fTotalDis = Main.s_Instance.m_fSplitMaxDistance;
        }
        m_fSegDis = m_fTotalDis / Main.s_Instance.m_nSplitNum; // 废弃
        fSegDis = m_fTotalDis / Main.s_Instance.m_nSplitNum;


        m_nRealSplitNum = 0; // 废弃
        nRealSplitNum = 0;
        float fCurSize = GetSize();
        float fCurArea = CyberTreeMath.SizeToArea(fCurSize, Main.s_Instance.m_nShit);
        m_fLeftArea = fCurArea;
        for (int i = Main.s_Instance.m_nSplitNum; i >= 2; i--)
        {
            float fSmallArea = fCurArea / i;
            float fSmallSize = CyberTreeMath.AreaToSize(fSmallArea, Main.s_Instance.m_nShit);
            if (fSmallSize >= Main.BALL_MIN_SIZE)
            {
                m_nRealSplitNum = i; // 废弃
                nRealSplitNum = i;
                fRealSplitChildSize = fSmallSize;
                m_fSplitArea = fSmallArea;
                m_fSplitSize = fSmallSize;
                break;
            }
        } // end for i


    }

    public void CalculateRealSplitInfo(float fWorldCurPosX, float fWorldCurPosY, ref int nRealSplitNum, ref float fRealSplitChildSize, ref float fSegDis)
    {
        vecTempPos.x = fWorldCurPosX;
        vecTempPos.y = fWorldCurPosY;
        float fTotalDistance = Vector2.Distance(GetPos(), vecTempPos);
        CalculateRealSplitInfo(fTotalDistance, ref nRealSplitNum, ref fRealSplitChildSize, ref fSegDis);
    }

    public void SplitOne(int nTimes)
    {
        /*
		if (m_nRealSplitNum <= 0) { // 废弃
			return;
		}
		bool bNew = false;
		Ball new_ball = _Player.ReuseOneBall ( photonView.isMine, ref bNew ); 
		if (new_ball == null) {
			return;
		}
		Debug.Log ( "spit_" + nTimes );
		if (!photonView.isMine) {
			_dirx = m_vecdirection.x;
			_diry = m_vecdirection.y;
		}

		float fDis = m_fSegDis * ( nTimes + 1 );
		float fInitSpeed = CyberTreeMath.GetV0 (fDis, Main.s_Instance.m_fSplitRunTime  );
		float fAccelerate = CyberTreeMath.GetA ( fDis, Main.s_Instance.m_fSplitRunTime);

		if (bNew) {
			new_ball.SetPos (GetPos ());
			new_ball.SetSize (m_fSplitSize);
			new_ball.SetDir (_dirx, _diry);
			new_ball.BeginEject (fInitSpeed, fAccelerate, Main.s_Instance.m_fSplitRunTime, true, true);
		} else {
			new_ball.Local_SetPos (GetPos ());
			new_ball.Local_SetSize (m_fSplitSize);
			new_ball.Local_SetDir (_dirx, _diry);
			new_ball.Local_BeginEject (fInitSpeed, fAccelerate, Main.s_Instance.m_fSplitRunTime, true, true);
		}

		m_fLeftArea -= m_fSplitArea;
		*/
    }

    public void CheckIfDeadAfterSplit()
    {
        //		if (!photonView.isMine) {
        //			return;
        //		}

        if (m_fLeftArea < Main.BALL_MIN_SIZE)
        {
            DestroyBall();
            return;
        }

        float fLeftSize = CyberTreeMath.AreaToSize(m_fLeftArea, Main.s_Instance.m_nShit);
        SetSize(fLeftSize);
    }

  

    /// 回收利用相关
    int m_nIndex = 0;
    public void SetIndex(int nIndex)
    {
        m_nIndex = nIndex;
		this.gameObject.name = "ball_" + nIndex;
    }

	public int GetPlayerId()
	{
		return _Player.GetOwnerId ();
	}

    public int GetIndex()
    {
        return m_nIndex;
    }

    int m_nMovingToCenterStatus = 0; // 0 - 还没开始移动  1 - 开始移动  2 - 移动完成
    public int GetMovingToCenterStatus()
    {
        return m_nMovingToCenterStatus;
    }

 

    public void SetMovingToCenterStatus(int nMovingToCenterStatus)
    {
        m_nMovingToCenterStatus = nMovingToCenterStatus;
    }



    public void CalculateMoveToCenterRealSpeed()
    {
        /*
        float fSpeed = Main.s_Instance.m_fBallMoveToCenterBaseSpeed;
        if (Main.s_Instance.m_nShit == 2) {
            fSpeed = fSpeed / GetSize2KaiGenHao();
        } else if (Main.s_Instance.m_nShit == 3) {
            fSpeed = fSpeed / GetSize3KaiGenHao();
        }
        _speed.x = fSpeed * _direction.x + m_fAdjustSpeedX;
        _speed.y = fSpeed * _direction.y + m_fAdjustSpeedY;
        */
    }

    public bool IfBallPosInvalid()
    {
        if (GetPos().x < -5000f || GetPos().y < -5000f)
        {
            return true;
        }

        return false;
    }

   

    

    bool m_bIsAdjustMoveProtect = false;
    float m_fAdjustMoveProtectTime = 0f;
    public void BeginAdjustMoveProtectTime()
    {
        m_fAdjustMoveProtectTime = 0f;
        m_bIsAdjustMoveProtect = true;
    }

    public void EndAdjustMoveProtectTime()
    {
        m_fAdjustMoveProtectTime = 0f;
        m_bIsAdjustMoveProtect = false;
    }

    public bool IsAdjustMoveProtect()
    {
        return m_bIsAdjustMoveProtect;
    }

    void AdjustMoveProtectLoop()
    {
        if (!m_bIsAdjustMoveProtect)
        {
            return;
        }

        m_fAdjustMoveProtectTime += Time.deltaTime;
        if (m_fAdjustMoveProtectTime >= Main.s_Instance.m_fAdjustMoveProtectInterval)
        {
            EndAdjustMoveProtectTime();
        }
    }

    Dictionary<int, int> m_dicGroupRoundId = new Dictionary<int, int>();
    public void GroupCount(int nGroupId)
    {
        int nCount = 0;
        if (m_dicGroupRoundId.TryGetValue(nGroupId, out nCount))
        {
            nCount++;
            m_dicGroupRoundId[nGroupId] = nCount;
        }
        else
        {
            m_dicGroupRoundId[nGroupId] = 0;
        }
    }

    public int GetGroupCount(int nGroupId)
    {
        int nCount = 0;
        if (m_dicGroupRoundId.TryGetValue(nGroupId, out nCount))
        {
        }
        return nCount;
    }

    float m_fLocalOffsetX = 0f;
    float m_fLocalOffsetY = 0f;
    public void RecordLocalPosOffset(float fStartLocalPosOfThisRoundX, float fStartLocalPosOfThisRoundY)
    {
        m_fLocalOffsetX = fStartLocalPosOfThisRoundX - GetLocalPosition().x;
        m_fLocalOffsetY = fStartLocalPosOfThisRoundY - GetLocalPosition().y;
    }

    bool m_bLockMove = false;
    public void SetLockMove(bool val)
    {
        m_bLockMove = val;
    }

    public bool GetLockMove()
    {
        return m_bLockMove;
    }

    public void LockMoveLoop()
    {

    }

    public void SetRebornProtectEfffect(bool val)
    {
        if (val)
        {
            cTempColor = _srMouth.color;
            cTempColor.a = 0.3f;
            _srMouth.color = cTempColor;

            cTempColor = _srOutterRing.color;
            cTempColor.a = 0.3f;
            _srOutterRing.color = cTempColor;


        }
        else
        {
            cTempColor = _srMouth.color;
            cTempColor.a = 1f;
            _srMouth.color = cTempColor;

            cTempColor = _srOutterRing.color;
            cTempColor.a = 1f;
            _srOutterRing.color = cTempColor;
        }

    }

    int m_nSizeClassId = -1; // 此球自身的区级
    public void ChangeSizeClassId()
    {
        float fRadisu = GetRadius();
        if (fRadisu >= CClassEditor.s_Instance.GetLowThreshold())
        {
            SetSizeClassId(1);
        }
        else
        {
            SetSizeClassId(0);
        }


    }

    float m_fAttenuateSpeed = -0.02f;
    float m_fClassInvasionAffectSpeed = 0f;

    public float GetClassInvasionAffectSpeed()
    {
        return m_fClassInvasionAffectSpeed;
    }

    public void ChangeAttenuateSpeed()
    {

        return;

        CClassEditor.sClassConfig low_class_config = CClassEditor.s_Instance.GetClassConfigById(0);
        CClassEditor.sClassConfig mid_class_config = CClassEditor.s_Instance.GetClassConfigById(1);
        CClassEditor.sClassConfig high_class_config = CClassEditor.s_Instance.GetClassConfigById(2);
        if (m_nClassId == 1)
        { // 处在中级区
            m_fAttenuateSpeed = mid_class_config.fNormalAttenuate;
            m_fClassInvasionAffectSpeed = 0;
            //	Debug.Log( "执行中级区正常衰减：" + m_fAttenuateSpeed );
        }
        else if (m_nClassId == 0)
        { // 处在低级区
            if (m_nSizeClassId == 1)
            {// 中级球闯低级区则执行低级区的侵略衰减
                m_fAttenuateSpeed = low_class_config.fInvasionAttenuate;
                m_fClassInvasionAffectSpeed = low_class_config.fInvasionSpeedSlowdown;
                //      Debug.Log( "中级球闯低级区则执行低级区的侵略衰减：" + m_fAttenuateSpeed );
            }
            else
            {// 其余情况执行低级区的常规衰减
                m_fAttenuateSpeed = low_class_config.fNormalAttenuate;
                m_fClassInvasionAffectSpeed = 0;
                //	Debug.Log( "执行低级区的常规衰减：" + m_fAttenuateSpeed );
            }
        }

    }

    public void SetSizeClassId(int val)
    {
        bool bChanged = (m_nSizeClassId != val);
        m_nSizeClassId = val;

        if (m_nSizeClassId == 0)
        {

        }
        else if (m_nSizeClassId == 1)
        {

        }
        else if (m_nSizeClassId == 2)
        {

        }

        if (bChanged)
        {
            ChangeAttenuateSpeed();
        }
    }

    int m_nClassId = 0; // 此球当前所呆在的区
    public void SetClassId(int val, bool bYaXian = false)
    {
        bool bChanged = (m_nClassId != val);
        m_nClassId = val;
        string msg = "";
        switch (m_nClassId)
        {
            case 0:
                {
                    msg = "低级区";
                }
                break;
            case 1:
                {
                    msg = "高级区";
                }
                break;
        }

        if (bYaXian)
        {
            if (m_nClassId == 0)
            {
                if (_Player.IsMainPlayer() && bChanged)
                {
                    //	Main.s_Instance.g_SystemMsg.SetContent ("门槛线(压线算低级区)。");
                }
            }
            else if (m_nClassId == 1)
            {
                if (_Player.IsMainPlayer() && bChanged)
                {
                    //Main.s_Instance.g_SystemMsg.SetContent ("高级区门槛线(压线算中级区)。");
                }
            }
        }
        else
        {
            if (_Player.IsMainPlayer() && bChanged)
            {
                //Main.s_Instance.g_SystemMsg.SetContent ("你娃进入了【" + msg + "】");
            }
        }

        if (bChanged)
        {
            ChangeAttenuateSpeed();


        }
    }

    public int GetClassId()
    {
        return m_nClassId;
    }
    /*
    public void ProcessEnterOrLeaveClassCircle()
    {
       if ( Vector2.Distance( GetPos(),  Main.s_Instance.m_vecClassCircleCenterPos)  < Main.s_Instance.m_fClassCircleLength ) // 想出
        {
            ExitClassCircle();
        }
        else // 想入
        {
            EnterClassCircle();
        }
    }
    */

    bool m_bStayOnClassCircle = false;
    int m_nStayClassCircleId = 0;
    public void StayOnClassCircle(string szName)
    {
        if (szName == "ClassCircle0")
        {
            m_nStayClassCircleId = 0;
        }
        if (szName == "ClassCircle1")
        {
            m_nStayClassCircleId = 1;
        }
        m_bStayOnClassCircle = true;
    }

    public bool IsStayOnClassCircle()
    {
        return m_bStayOnClassCircle;
    }

    // 没有压线
    public void NotStayOnClassCircleLoop()
    {

    }

    // 正在压线
    public void StayOnClassCircleLoop()
    {
        if (!m_bStayOnClassCircle)
        {
            return;
        }
        CheckClassStatus();
    }

    public void CheckClassStatus()
    {
        float fBallRadius = GetRadius();
        float fDis = Vector2.Distance(GetPos(), Main.s_Instance.m_vecClassCircleCenterPos);

        if ((CClassEditor.s_Instance.GetClassCircleRadius() + fBallRadius) < fDis)// 在圈外
        {
            SetClassId(0);
            m_bStayOnClassCircle = false; // 没压线了
            return;
        }
        if (CClassEditor.s_Instance.GetClassCircleRadius() > (fBallRadius + fDis)) // 在圈内
        {
            SetClassId(1);
            m_bStayOnClassCircle = false; // 没压线了
            return;
        }

        //// 在压线
        SetClassId(0, true);
    }

    public void EnterClassCircle()
    {
        m_nClassId = 1;
    }

    public void ExitClassCircle()
    {
        m_nClassId = 0;
    }

    public bool IsInClassCircle()
    {
        return m_nClassId == 1;
    }

    /*
    public bool IsSizeReachClassCircleThreshold()
    {
        return GetSize() >= Main.s_Instance.m_fClassCircleThreshold;
    }
    */


    public void SetEffectVisibleCommon( CSkillSystem.eSkillId eSkillId, bool bVisible )
    {
		return;

        int nSkillId = (int)eSkillId;
        CFrameAnimationEffect effectChiXu = m_aryEffectChiXu[nSkillId];
        CFrameAnimationEffect effectChiXu1 = m_aryEffectChiXu1[nSkillId];
        CFrameAnimationEffect effectQianZou = m_aryEffectQianZou[nSkillId];
        if (bVisible)
        {
            effectChiXu.gameObject.SetActive(true);
            effectChiXu.BeginPlay(true);
            effectChiXu.SetLoopTimes(0);
            m_byChiXuEffectIndex = 0;
            SetEffectStatus(nSkillId, 2);

            if (eSkillId == CSkillSystem.eSkillId.u_magicshield)
            {

            }
        }
        else
        {
            effectChiXu.gameObject.SetActive(false);
            if (effectChiXu1)
            {
                effectChiXu1.gameObject.SetActive(false);
            }
            effectQianZou.gameObject.SetActive(false);
        }
    }
   
    //// 湮灭状态
    public void SetAnnihilate(bool val)
    {
        SetEffectVisibleCommon( CSkillSystem.eSkillId.y_annihilate, val );

        if (val)
        {
           
        }
    }

    //// 魔盾状态
    public void SetMagicShielding(bool val)
    {
        /*
        _effectMagicShield.gameObject.SetActive(val);
        if (val)
        {
            _effectMagicShield.Play();
        }
        */

        SetEffectVisibleCommon( CSkillSystem.eSkillId.u_magicshield, val);
    }





    public void BeginSkillEffect( CSkillSystem.eSkillId eSkillId )
    {

        CFrameAnimationEffect _effectQianZou = m_aryEffectQianZou[(int)eSkillId];
        _effectQianZou.gameObject.SetActive( true );
        _effectQianZou.BeginPlay( false );
        SetEffectStatus( (int)eSkillId, 1);
    }

    public void EndSkillEffect(CSkillSystem.eSkillId eSkillId)
    {
        int nSkillId = (int)eSkillId;
        CFrameAnimationEffect effectQianZou = m_aryEffectQianZou[nSkillId];
        CFrameAnimationEffect effectChiXu = m_aryEffectChiXu[nSkillId];
        CFrameAnimationEffect effectChiXu1 = m_aryEffectChiXu1[nSkillId];
        if (effectQianZou)
        {
            effectQianZou.gameObject.SetActive(false);
        }
        if (effectChiXu)
        {
            effectChiXu.gameObject.SetActive(false);
        }
        if (effectChiXu1)
        {
            effectChiXu.gameObject.SetActive(false);
        }
        SetEffectStatus( (int)eSkillId, 0);
    }

    public CFrameAnimationEffect GetSkillEffect( CSkillSystem.eSkillId eSkillId, int nSkillType )
    {
        int nSkillid = (int)eSkillId;

        if (nSkillType == 1) // 前奏
        {
            return m_aryEffectQianZou[nSkillid];
        }
        else if (nSkillType == 2) // 持续
        {
            return m_aryEffectChiXu[nSkillid];
        }

        return null;
    }

    public bool IsMainPlayer()
    {
        if (_Player != null )
        {
            return _Player.IsMainPlayer();
        }

        return false;
    }

    bool m_bActive = false;
    public void SetActive( bool bActive )
    {
        m_bActive = bActive;

        if (bActive)
        {
            _PlayerIns.SetLiveBall(GetIndex(), this);

            _container.SetActive(true);

            if (IsMainPlayer())
            {
                _ColliderDust.enabled = true;
            }

            if (_Player)
            {
                _Player.AddActiveBall(this); // [to youhua] 这个流程有点鸡肋，稍后优化一下
            }
            
        }
        else
        {
            //// 最新的球球复用机制
            _PlayerIns.SetLiveBall(GetIndex(), null);
            CBallManager.s_Instance.DeleteBall(this, IsMainPlayer());
           
            if ( IsMainPlayer() )
            {
                _PlayerIns.AddBallToReuseList(this);
                _PlayerIns.CheckIfPlayerDead();
            }
            //// 

            _container.SetActive(false);
            _Collider.enabled = false;
            _ColliderTemp.enabled = false;
            _ColliderDust.enabled = false;
            Local_EndShell();
            SetSelfTriggerEnable(false);

            if (_Player)
            {
                _Player.RemoveActiveBall(this);// [to youhua] 这个流程有点鸡肋，稍后优化一下
            }
        }

        _Trigger.enabled = bActive;
    }
    public void ClearRelatedThings()
    {
        // clear effects 
        // poppin to to


        // clear gunsight
        if ( _gunsight != null )
        {
            CGunSightManager.s_Instance.DeleteGunSight(_gunsight);
            _gunsight = null;
        }
    }

    int m_nGuid = 0;
    public void SetGuid( int nGuid )
    {
        m_nGuid = nGuid;
    }

    public int GetGuid()
    {
        return m_nGuid;
    }

    public bool GetActive()
    {
        return m_bActive;
    }

    bool m_bEaten = false;
    public void SetEaten( bool bEaten )
    {
        m_bEaten = bEaten;
    }

    public bool GetEaten()
    {
        return m_bEaten;
    }


    //// move to some destination
    Vector3 m_vecDest = new Vector3();
    bool m_bIsMovingToDest = false;
    public void BeginMoveToDestination(Vector3 dest)
    {
        m_vecDest = dest;
        m_bIsMovingToDest = true;
    }

    public void SetMovingToDest( bool bVal )
    {
        m_bIsMovingToDest = bVal;
    }

    public bool IsMovingToDest()
    {
        return m_bIsMovingToDest;
    }

    void MovingToDest()
    {
        if ( !IsMainPlayer() )
        {
            return;
        }

        
        if (!m_bIsMovingToDest)
        {
            return;
        }

        if ( _Player == null )
        {
            return;
        }

        float fPlayerbaseSpeed = _Player.GetBaseSpeed();
        _ba.UpdatePosition(m_vecDest, fPlayerbaseSpeed, true);

        //EndMoveToDestination();

        /*
        bool bCompletedX = false;
        bool bCompletedY = false;
        if (fMovingToDestDirX > 0)
        {
            if ( GetPos().x > m_vecDest.x )
            {
                bCompletedX = true;
            }
        }
        else if (fMovingToDestDirX < 0)
        {
            if (GetPos().x < m_vecDest.x)
            {
                bCompletedX = true;
            }

        }
        else
        {
            bCompletedX = true;
        }

        if (fMovingToDestDirY > 0)
        {
            if (GetPos().y > m_vecDest.y)
            {
                bCompletedY = true;
            }
        }
        else if (fMovingToDestDirY < 0)
        {
            if (GetPos().y < m_vecDest.y)
            {
                bCompletedY = true;
            }

        }
        else
        {
            bCompletedY = true;
        }
        if (bCompletedX && bCompletedY)
        {
            EndMoveToDestination();
        }
        */
    }

    public void EndMoveToDestination()
    {
        m_bIsMovingToDest = false;
        _rigid.bodyType = RigidbodyType2D.Dynamic;
    }

    public void SetColliderDustEnable( bool bEnable )
    {
        _ColliderDust.enabled = bEnable;
    }

    //// end move to some destination





    //// 技能相关

    public void SetMergeAll(  bool bIsMergingAll )
    {
        _effectMergeAll_QianYao.gameObject.SetActive(bIsMergingAll);
    }


    public void SetSneak( bool bSneaking )
    {
        if ( IsMainPlayer() )
        {
            if (bSneaking)
            {
                SetColliderDustEnable(false);

            }
            else
            {
                SetColliderDustEnable(true);
            }
        }
    }

    public void SetHenzy(bool bHenzy)
    {
        _goEffectContainer_KuangBao.gameObject.SetActive(bHenzy);
        _effectKuangBao_Up.gameObject.SetActive(bHenzy);
        //_effectKuangBao_Down.gameObject.SetActive(bHenzy);

        if (!bHenzy)
        {
            _goEffectContainer_KuangBao.SetActive(false);
        }

        if (bHenzy)
        {
            _goEffectContainer_KuangBao.SetActive( true );
            _effectKuangBao_Up.BeginPlay(true);
            //_effectKuangBao_Down.BeginPlay(true);
            SetEffectStatus((int)CSkillSystem.eSkillId.o_fenzy, 2);
        }
    }

    public void SetGold( bool bGold )
    {
        int nSkillId = (int)CSkillSystem.eSkillId.p_gold;
        CFrameAnimationEffect effectChiXu =  m_aryEffectChiXu[nSkillId];
        CFrameAnimationEffect effectQianZou = m_aryEffectQianZou[nSkillId];
        if ( bGold )
        {
            effectChiXu.gameObject.SetActive( true );
            effectChiXu.BeginPlay( true );
            SetEffectStatus((int)CSkillSystem.eSkillId.p_gold, 2);
        }
        else
        {
            effectChiXu.gameObject.SetActive(false);
            effectQianZou.gameObject.SetActive(false);
        }

    }

    public void SetBecomeThorn( bool bIsBecomeThorn, bool bIsBecomeThornQianYao)
    {

        if (bIsBecomeThorn)
        {
            _skeaniEffectBianCi.gameObject.SetActive(true);
            _skeaniEffectBianCi.state.SetAnimation(0, "ani04_1", true);
            _skeaniEffectBianCi.state.TimeScale = 0.2f;
        }
        else
        {
            _skeaniEffectBianCi.gameObject.SetActive( false );
            
        }
      
    }

    Dictionary<int, CDust> m_dicDust = new Dictionary<int, CDust>();
    public void AddDust( CDust dust )
    {
        m_dicDust[dust.GetId()] = dust;
        BeginBlingMe();
    }

    public void RemoveDust(CDust dust)
    {
        m_dicDust.Remove(dust.GetId());
    }

    void BlingDust()
    {
        if ( !IsMainPlayer())
        {
            return;
        }

        foreach( KeyValuePair<int, CDust> pair in m_dicDust )
        {
            pair.Value.BeginBling();
            BeginBlingMe();
        }

        BlingMeForDustLoop();
    }


    int m_nOp = 1;
    int m_nOpScale = 1;
    const float COLOR_SPEED = 0.025f;
    const float SCALE_SPEED = 0.005f;
    void BlingMeForDustLoop()
    {
        if (m_fBlingTime <= 0f)
        {
            return;
        }
        m_fBlingTime -= Time.deltaTime;
        if (m_fBlingTime <= 0f)
        {
            EndBlingMe();
        }

        if (m_nOp == 1)
        {
            colorTemp = _srDustBling.color;
            colorTemp.a += COLOR_SPEED;
            _srDustBling.color = colorTemp;
            if (_srDustBling.color.a >= 0.9)
            {
                m_nOp = -1;
            }
        }
        else if (m_nOp == -1)
        {
            colorTemp = _srDustBling.color;
            colorTemp.a -= COLOR_SPEED;
            _srDustBling.color = colorTemp;
            if (_srDustBling.color.a <= 0)
            {
                m_nOp = 1;
            }

        }


        if (m_nOpScale == 1)
        {
            vecTempScale = _srDustBling.transform.localScale;
            vecTempScale.x += SCALE_SPEED;
            vecTempScale.y += SCALE_SPEED;
            _srDustBling.transform.localScale = vecTempScale;
            if (vecTempScale.x >= 1.1f)
            {
                m_nOpScale = -1;
            }
        }
        else if (m_nOpScale == -1)
        {
            vecTempScale = _srDustBling.transform.localScale;
            vecTempScale.x -= SCALE_SPEED;
            vecTempScale.y -= SCALE_SPEED;
            _srDustBling.transform.localScale = vecTempScale;
            if (vecTempScale.x <= 1f)
            {
                m_nOpScale = 1;
            }
        }
    }
    float m_fBlingTime = 0f;
    void BeginBlingMe()
    {
        m_fBlingTime = 0.5f;
        _srDustBling.gameObject.SetActive( true );
    }

    void EndBlingMe()
    {
        _srDustBling.gameObject.SetActive(false);
    }
    //// end 技能相关

    


    //// =============== 测试功能
    int m_nAutoMove = 0;
    public void SetAutoMoving( int nAutoMove )
    {
        m_nAutoMove = nAutoMove;
    }

    public void AutoMoveLoop( float fSpeed )
    {
        if (m_nAutoMove == 0)
        {
            return;
        }



        vecTempPos = GetPos();
        if (m_nAutoMove == 1)
        {
            vecTempPos.x += fSpeed;
            if (CCheat.s_Instance.isRigidMove())
            {
                _rigid.MovePosition(vecTempPos);   
            }
            else
            {
                SetPos(vecTempPos);
            }
            if (vecTempPos.x >= 400 )
            {
                m_nAutoMove = 2;
            }
        }
        else
        {
            vecTempPos.x -= fSpeed;
            if (CCheat.s_Instance.isRigidMove())
            {
                _rigid.MovePosition(vecTempPos);
            }
            else
            {
                SetPos(vecTempPos);
            }
            if (vecTempPos.x <= -400 )
            {
                m_nAutoMove = 1;
            }

        }
    }

    public void RedrawPolygons( int nTriangleNum )
    {
        m_msMain.SetCoreParams(ResourceManager.s_Instance.m_Perlin,
                                false,
                                null,
                                nTriangleNum,
                                3.0f,
                                0.0f,
                                360,
                                0,
                                0,
                                0.3f
            );

        m_msMain.drawPolygon();
    }

    public CZhangYu m_goZhangYu;
    public void BecomeZhangYu()
    {
        m_msStaticShell.gameObject.SetActive( false );
        m_msMain.gameObject.SetActive(false);
        m_goZhangYu.gameObject.SetActive(true);
        m_goZhangYu.SetColor( _Player.GetColor() );
    }

    bool bPlayerNameVisible = true;

    public void ProcessPlayerNameVisible(float fCamSize )
    {

        float fXiShu = fCamSize / GetSize();
        if (bPlayerNameVisible)
        {
            if (!_PlayerIns.IsMostBigBallsToShowName(GetIndex()))
            {
                SetPlayerNameVisible(false);
            }

            if (fXiShu > CCameraManager.s_Instance.m_fPlayerNameVisibleXiShu)
            {
                SetPlayerNameVisible(false);
            }
        }
        else
        {
            if (fXiShu < CCameraManager.s_Instance.m_fPlayerNameVisibleXiShu)
            {
                if (_PlayerIns.IsMostBigBallsToShowName(GetIndex()))
                {
                    SetPlayerNameVisible(true);
                }
            }
        }

    }

    bool bPlayerNameSizeControl = false;
    public void ProcessPlayerName( float fCamSize ) // [to youhua]
    {
        if (!GetPlayerNameVisible()) // 没显示的名字根本谈不上其它处理
        {
            return;
        }

          float fCurBiLi = GetSize() / fCamSize;
          if (fCurBiLi > Main.s_Instance.m_fPlayerNameMaxThreshold)
          {
                float fScale = Main.s_Instance.m_fPlayerNameBaseSize * Main.s_Instance.m_fPlayerNameMaxThreshold / fCurBiLi;
                vecTempScale.x = fScale;
                vecTempScale.y = fScale;
                vecTempScale.z = 1;
                _goPlayerName.transform.localScale = vecTempScale;
            }
       
        else
        {
            float fScale = Main.s_Instance.m_fPlayerNameBaseSize;
            vecTempScale.x = fScale;
            vecTempScale.y = fScale;
            vecTempScale.z = 1;
            _goPlayerName.transform.localScale = vecTempScale;
        }
        
    }

    
    public void UpdateSkillStatus_KuoZhang()
    {

    }

    public void UpdateSkillStatus( CSkillSystem.eSkillId eSkillId, short nSkillStatus )
    {
        _skillModule.UpdateSkillStatus( eSkillId, nSkillStatus);
    }

    public void GetCurEffects( ref CCosmosEffect[] aryQianYao, ref CCosmosEffect[] aryChiXu )
    {
        _skillModule.GetCurEffects( ref aryQianYao, ref aryChiXu );
    }

    bool m_bPicked = false;
    public void SetPicked( bool bPicked )
    {
        m_bPicked = bPicked;
    }

    public bool IsPicked()
    {
        return m_bPicked;
    }

    public void ClearSkillModule()
    {
        _skillModule.Clear();
        _skillModule.enabled = false;
    }
}