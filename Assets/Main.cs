﻿/*
 *  只要有流水，奖金每月发，所谓年终奖不是真汉子。


*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

/*
 * 关于豆子的基本规则
 * 1、一个竞技场的总面积是有限的（有边界）。面积可按策划需求配置。
 * 2、豆子单位面积的分布数量有一个系数，该系数可按策划需求配置。
 * 3、豆子是要网络同步的，同竞技场每个客户端面对的豆子都完全一致（位置、产出、消耗 等）
 * 4、玩家视角不限，只要在游戏逻辑允许的范围内，视角可以大到同时看到全竞技场所有的豆子，故必须有优化策略（后期再考虑）
 * 5、每个豆子被消耗掉后，并非立即又刷出来，而是间隔一定时间后再刷出来，这个时间间隔可按策划需求配置。
 */
public class Main : Photon.PunBehaviour
{
    /// <summary>
    ///  射球公式相关参数
    /// </summary>
    public float m_fEjectHuanTing = 0.5f;
    public float m_fA = 1f;
    public float m_fB = 2f;
    public float m_fX = 0.5f;

    public const int INVALID_BALL_INDEX = 64;
    public const int MAX_BALL_NUM_TO_SHOW_NAME = 4;
    public float m_fPlayerNameBaseSize = 0.2f;
    public float m_fPlayerNameMaxThreshold = 0.005f;
    public float m_fSporeMaxSize = 5f;

    

    public float m_fGameOverPanelTime = 60f;
    public int m_nPlatform = 0; // 0 - PC  1 - Mobile Phone
    public float m_fDistanceToSlowDown = 10f;
    public float m_fBiggestBallAsCenterThreshold = 1.5f;
    public float m_fSizeChangeSpeed = 100f;

    public int[] m_aryUnfoldArcStartAngle;

    public GameObject _goGameOver;

    public GameObject _goBallsCenter;

    public float m_fCamRaiseAccelerate = 150f;
    public float m_fCamDownAccelerate = 150f;
    public float m_fCamChangeDelaySpit = 0f;
    public float m_fCamChangeDelayExplode = 0f;
    public float m_fRaiseSpeed = 400f;
    public float m_fDownSpeed = 300f;
    public float m_fCamRespondSpeed = 5f;
    public float m_fTimeBeforeCamDown = 0.1f;
    public float m_fCamMinSize = 30f;

    /// <summary>
    /// / 暂时废弃了的变量
    /// </summary>
    public float m_fCamChangeSpeed = 50f;
    public float m_fCamGuiWeiSpeed = 0.5f;
    public float m_fShangFuXiShu = 20f;
    public float m_fYiJianHeQiuSpeed = 10f;
    public float m_fCamSizeXiShu = 1f;
    /// 

    public CCheat _goCheat;

    public GameObject m_goJueShengQiu;

    public Camera m_camWarFog;
    public Camera m_camForCalc;

    public SystemMsg g_SystemMsg;

    public Sprite[] m_aryBallSprite;

    // Self
    public static Main s_Instance;

    // temp
    public GameObject m_goHongBao;

    public PlayerAction _playeraction;

    // 胜负判定怪
    public CMonsterBall m_ShengFuPanDingMonster;

    // prefab “预设件”。  游戏中的各种对象都是用预设件来实例化出来的
    public GameObject m_preGridLine;
    public GameObject m_preBallLine;
    public GameObject m_preBall;
    public GameObject m_preBallTest;
    public GameObject m_preElectronicBall;
    public GameObject m_prePlayer;
    public GameObject m_preThorn; //  刺
    public GameObject m_preRhythm; // 节奏环
    public GameObject m_preBean;   // 豆子
    public GameObject m_preEffectPreExplode; // 预爆炸
    public GameObject m_preEffectExplode;    //  爆炸 
    public GameObject m_preBlackHole; // 黑洞
    public GameObject m_preSpitBallTarget; // 吐球的目标指示
    public GameObject m_preSpore; // 孢子
    public GameObject m_preNail;  // 钉子
    public GameObject m_preVoice; // 语音聊天控件
    public GameObject m_preGroup; // 分组
    public GameObject m_preOneWayObstacle; // 单向障碍
    public GameObject m_preExplodeNode; // 爆炸信息节点


    // UI
    public GameObject _panelRank;
    public GameObject _panelSkillPoint;

    public CMsgList _msglistKillPeople;
    public Text _txtTimeLeft;
    public Text _txtTimeLeft_MOBILE;
    public Text _txtTimeLeft_PC;
    public GameObject _uiDead;
    public GameObject _uiDeadNew;
    public GameObject _uiGameOver;
    Text m_txtDebugInfo;
    Slider m_sliderStickSize;
    Slider m_sliderAccelerate;
    Button m_btnSpit;
    Scrollbar m_sbSpitSpore;
    Scrollbar m_sbSpitBall;
    Scrollbar m_sbUnfold;
    public GameObject m_uiGamePanel;
    public GameObject m_uiMapEditorPanel;

    public Image m_imgSpitBall_Fill;
    public Text m_txtColdDown_W;
    public Text m_txtColdDown_E;
    public Text m_txtColdDown_R;
    public Text m_txtColdDown_W_Mobile;
    public Text m_txtColdDown_E_Mobile;
    public Text m_txtColdDown_R_Mobile;
    public Image m_imgSpitSpore_Fill;
    public Image m_imgUnfold_Fill;
    public Image m_imgOneBtnSplit_Fill;
    public Image m_imgSelectedSkill_Fill; // 自选技能的Colddown条
    public float m_SelectedSkillColdDownTimeCount = 0f;
    public float m_SelectedSkillColdDownInterval = 0f;
    public Image m_imgLababa_Fill;


    // public data


    public float m_fBallTriggerBasrRadius = 1.3f;
    public float m_fPixel2Scale = 2.59f;
    public float m_fScale2Radius = 1.295f; // Scale转换为绝对半径的系数
    public float m_fRadius2Scale = 0.7722007722f; // 绝对半径转换为Scale的系数
    public int m_nShit = 3;

    public static float BALL_MIN_SIZE = 1.0f; // spore size
    public const int MAX_BALL_NUM = 64;
    public static float m_fBallMinVolume = 1f; // 球的最小体积

    public float m_fBallBaseSpeed;
    public float m_fBallSpeedRadiusKaiFangFenMu = 0f;
    public float m_fBallMoveToCenterBaseSpeed = 40f;

    /// <summary>
    /// new new new 
    /// </summary>
    public int m_nRebornPosIdx = -1;

    public float m_fBaseVolumeyByItemDecreasedPercentWhenDead = 0.1f;

    public float m_fMaxAudioVolume = 1.5f;
    public float m_fMinAudioVolume = 0.7f;

    public float m_fMaxPlayerSpeed = 200f;

    public float m_fShengFuPanDingSize = 50f;
    
    public float m_fClassCircleThreshold0 = 8f;
    public float m_fClassCircleThreshold1 = 16f;
    public float m_fClassCircleLength0 = 200f;
    public float m_fClassCircleLength1 = 400f;
    public Vector3 m_vecClassCircleCenterPos = Vector3.zero;

    public float m_fLaBabaRunTime = 1f;   // 拉粑粑的运行时间
    public float m_fLaBabaRunDistance = 20f;   // 拉粑粑的距离
    public float m_fLababaColdDown = 10f; // “拉粑粑”技能的Colddown
    public float m_fLababaLiveTime = 3f;
    public float m_fLababaSizeCostPercent = 0.03f; // 拉粑粑损失的半径百分比
    public float m_fLababaAreaThreshold = 0f;        // 队伍总体积达到多少值才能拉粑粑

    public float m_fTimeOfOneGame = 5f;
    public float m_fAdjustMoveProtectInterval = 0.5f;
    public float m_fMaxMoveDistancePerFrame = 5f;
    public int m_nObservor = 1;
    public float m_fShellAdjustShit = 3f;
    public float m_fBaseSpeedInGroupLocal = 2f;
    public float m_fSyncMoveCountInterval = 0.3f;
    public float m_fAdjustPosDelayThreshold = 0.1f;
    public float m_nCheatMode = 0; // 是否可以作弊
    public float m_fBornProtectTime = 5f; // 重生保护时间
    public float m_fMinRunTime = 0.02f;
    public float m_fGridLineGap = 5;
    public float m_fTimeBeforeUiDead = 3f;
    public float m_fBeanNumPerRange = 0.01f;  // 单位面积豆子产生系数
    public float m_fThornNumPerRange = 0.005f; // 单位面积刺产生系数
    public float m_fBeanSize = 0.5f;
    public float m_fThornSize = 0.5f;

    public float m_fShellShrinkTotalTime;
    public float m_fExplodeShellShrinkTotalTime;
    public float m_fSplitShellShrinkTotalTime;
    public float m_fThornContributeToBallSize = 0.1f; // 刺对球总体积的贡献值

    public float m_fAutoAttenuate = 0.001f; // 球体的自动衰减系数（如果体内含刺，那么刺的体积也跟着衰减）。注意这个衰减是按比例，而不是绝对数值
    public float m_fAutoAttenuateTimeInterval = 1.0f;
    public float m_fThornAttenuate = 0.001f;

    public float m_fArenaWidth = 100.0f;
    public float m_fArenaHeight = 100.0f;
    public float m_fGenerateNewBeanTimeInterval = 10.0f; // 生成新豆子的时间间隔（单位：秒）
    public float m_fGenerateNewThornTimeInterval = 10.0f; // 生成新刺的时间间隔（单位：秒）

    public float m_fSpitSporeTimeInterval = 0.3f;        // 吐孢子时间间隔
    public float m_fSpitBallTimeInterval = 0.5f;         // 吐球时间间隔

    public float m_fUnfoldCostMP = 1f;
    public float m_fMainTriggerUnfoldTime;
    public float m_fMainTriggerPreUnfoldTime;
    public float m_fUnfoldTimeInterval = 2.0f;           // 膨胀的时间间隔

    public float m_fUnfoldSale = 2.0f;                   // 猥琐膨胀的体积是原始体积的多少倍
    public float m_fUnfoldCircleSale = 0.8f;                   // 猥琐膨胀的体积是原始体积的多少倍

    public float m_fForceSpitTotalTime = 1.0f;
    public float m_fSpitRunTime = 1.0f;                  // 吐孢子的运行时间(秒)
    public float m_fSpitBallRunTime = 1.0f;                  // 吐球的运行时间(秒)
    public string m_szSpitSporeBeanType = "";              // 吐孢子选用的豆子型号
    public float m_fSpitSporeCostMP = 0.1f;              // 吐一颗孢子消耗的蓝
    public float m_fSpitSporeRunDistance = 30.0f;         // 吐孢子的运行距离(描述绝对距离)
    public float m_fSpitSporeInitSpeed = 5.0f;           // 吐孢子的初速度
    public float m_fSpitSporeAccelerate = 0.0f;                 // 吐孢子的加速度(这个值通过距离、初速度、时间 算出来，不用配置) 
    public float m_fSpittedObjrunRunTime = 1.0f;
    public float m_fSpittedObjrunRunDistance = 5.0f;

    /// <summary>
    // “二分分球”技能
    /// </summary>
    public float m_fSpitBallHalfRunTime = 0.5f;
    public float m_fSpitBallHalfRunDistance = 3f;
    public float m_fSpitBallHalfCostMP = 1f;
    public float m_fSpitBallHalfColddown = 1f;
    public float m_fSpitBallHalfQianYao = 1f;

    public float m_fSpitBallRunSpeed = 150f;
    public float m_fSpitBallRunDistanceMultiple = 3.0f;         // 分球的运行距离（描述母球半径的倍数, 而非直接的距离数值）
    public float m_fSpitBallCostMP = 1f;
    public float m_fSpitBallStayTime = 1f;

    public float m_fBornMinTiJi = 0.0f;              // 初生最小体积
    public float m_fMinDiameterToEatThorn = 0.0f;              // 吃刺最小直径

    public float m_fExplodeRunDistanceMultiple = 0.0f;   // 爆球时子球运行的距离（母球半径的倍数）
    public float m_fExplodeStayTime = 1f;   // 爆球逗留时间
    public float m_fExplodePercent = 0.8f;   // 母球分出百分之多少的体积来爆
    public float m_fExpectExplodeNum = 8.0f;             // 每次爆刺期望爆出的球球数
    public float m_fExplodeRunTime = 0.5f;             // 爆球运行时间
    public float m_nExplodeMinNum = 2;

    public float m_fMaxBallNumPerPlayer = 0.0f;          // 一个玩家最多的球球数


    public float m_fSprayBeanMaxInitSpeed = 4.0f;
    public float m_fSprayBeanMinInitSpeed = 1.5f;
    public float m_fSprayBeanAccelerate = 8.0f;
    public float m_fSprayBeanLiveTime = 40.0f;

    public float m_fCrucifix = 0.0f;
    public int m_nSplitNum = 32;
    public float m_fSplitCostMP = 1f;
    public float m_fSplitStayTime = 1f;
    public float m_fSplitRunTime = 2f;
    public float m_fSplitMaxDistance = 40f;
    public float m_fSplitTotalTime = 1f;
    public float m_fSplitTimeInterval = 2f;

    public float m_fTriggerRadius = 1.33f;
    // poppin temp
    public GameObject the_spray;
    public GameObject m_goSprays;

    /// <summary>
    /// 钉子
    public float m_fSpitNailTimeInterval = 0.5f;        // 吐钉子时间间隔
    public float m_fNailLiveTime = 20.0f;               // 钉子存活时间

    public LineRenderer m_lineWorldBorderTop;
    public LineRenderer m_lineWorldBorderBottom;
    public LineRenderer m_lineWorldBorderLeft;
    public LineRenderer m_lineWorldBorderRight;

    public GameObject m_goBorderLeft;
    public GameObject m_goBorderRight;
    public GameObject m_goBorderTop;
    public GameObject m_goBorderBottom;


    Vector3 vecTempPos = new Vector3();
    Vector3 vecTempPos1 = new Vector3();

    /// <summary>
    /// // public GameObjects
    /// </summary>
    /// 
    // container
    public GameObject m_goThornsSpit;
    public GameObject m_goThorns;
    public GameObject m_goSpores;
    public GameObject m_goBeans;
    public GameObject m_goSpitBallTargets;
    public GameObject m_goNails;
    public GameObject m_goBallsBeNailed;
    public GameObject m_goDestroyedBalls;

    public GameObject m_goBalls;

    public GameObject m_goJoyStick;

    public int s_nBallTotalNum = 0;
    public const int c_nStartUserLayerId = 9;

    public Player m_MainPlayer;
    public GameObject m_goMainPlayer;
    public GameObject _mainplayer
    {
        get { return m_goMainPlayer; }
        set { m_goMainPlayer = value; }
    }
    PhotonView m_photonViewMainPlayer;

    /// <summary>
    /// / UI
    /// </summary>
    public Text m_textDebugInfo;

    /// <summary>
    /// / Instantiate Shit
    /// </summary>
    public enum eInstantiateType
    {
        Player,
        Ball,
        Bean,
        Spore,
    };
    public object[] s_Params = new object[256];

    public bool m_bInitingAllBalls = true;
    public bool IsInitingAllBalls()
    {
        return m_bInitingAllBalls;
    }

    private void Awake()
    {
        Application.targetFrameRate = -1;
    }

    bool IsGameStarted()
    {
        return true; // poppin test

        return CQueueManager.s_Instance.IsQueueFinished();
    }

    public static List<PhotonView> m_lstDelayInit = new List<PhotonView>();
    // Use this for initialization
    void Start()
    {
        // poppin test
        /*
        if (PhotonNetwork.isMasterClient ) {
			CQueueManager.s_Instance.StartQueue ();
		}
        */

   
        _inputCheatPanelExplodeThornId.text = "0_1";

        m_fScreenWidth = Screen.width + 100;
        m_fScreenHeight = Screen.height + 100;

        for (int i = 0; i < m_aryUnfoldArcStartAngle.Length; i++)
        {
            m_aryUnfoldArcStartAngle[i] = i * 15;
        }


        // 操作模式
        if (GetPlatformType() == Main.ePlatformType.pc)
        {
            GUIAction.s_Instance.SetCtrlMode(CtrlMode.CTRL_MODE_HAND); // 拖线模式
            //_txtTimeLeft = _txtTimeLeft_PC;
            _txtTimeLeft = _txtTimeLeft_MOBILE;
        }
        else if (GetPlatformType() == Main.ePlatformType.mobile)
        {
            GUIAction.s_Instance.SetCtrlMode(CtrlMode.CTRL_MODE_JOYSTICK); // 摇杆模式
            _txtTimeLeft = _txtTimeLeft_MOBILE;
        }



        StringManager.InitBlobs();

        if (AccountManager.m_eSceneMode == AccountManager.eSceneMode.None) { // 这儿有个BUg，暂时没找到源头，所以只有做一个容错
            return;
        }

        s_Instance = this;

        ReadGamePlayDataConfig();

        if (AccountManager.m_eSceneMode == AccountManager.eSceneMode.Game && !AccountManager.s_bObserve)
        {
            //InitArena();
            AccountManager.m_bInGame = true;
            InitMainPlayer();
            //m_uiGamePanel.SetActive( true );
            UIManager.s_Instance.SetUiGameVisible(true);
            /*
			for (int i = 0; i < m_lstDelayInit.Count; i++) {
				PhotonView pv = m_lstDelayInit [i];
				GameObject go = GameObject.Instantiate ( m_prePlayer );
				Player player = go.GetComponent<Player>();
				PhotonView pv_new = go.GetComponent<PhotonView>();
				int nViewId = pv.viewID;
				int nOwnerId = pv.ownerId;
				pv_new.ownerId = nOwnerId;
				pv_new.viewID = nViewId;
				player.Init ();

			}
			*/
        }
        else
        {
            m_goJoyStick.SetActive(false);
            //m_uiGamePanel.SetActive(false);
            UIManager.s_Instance.SetUiGameVisible(false);
            // Camera.main.orthographicSize = 100f;
        }

        if (AccountManager.m_eSceneMode == AccountManager.eSceneMode.MapEditor)
        {
            MapEditor.s_Instance.Init();
            m_uiMapEditorPanel.SetActive(true);

        }
        else
        {
            m_uiMapEditorPanel.SetActive(false);
        }

        //MapEditor.s_Instance.LoadMap();
    }

    Dictionary<string, float> m_dicGamePlayDataConfig = new Dictionary<string, float>();
    string[][] Array;
    void ReadGamePlayDataConfig()
    {
        return; // 这块功能转移到统一的地图编辑器了

        //按行读取为字符串数组
        string filename = Application.streamingAssetsPath + "/GamePlayData.txt";

        string[] lines = System.IO.File.ReadAllLines(filename);
        List<string> lst = new List<string>();
        foreach (string line in lines)
        {
            lst.Add(line);
        }
        Array = new string[lst.Count][];  //创建二维数组  
        for (int i = 0; i < lst.Count; i++) {
            Array[i] = lst[i].Split('\t');
            float val = 0.0f;
            float.TryParse(Array[i][2], out val);
            m_dicGamePlayDataConfig[Array[i][1]] = val;
        }

        m_fBeanNumPerRange = m_dicGamePlayDataConfig["fBeanNumPerRange"];
        m_fThornNumPerRange = m_dicGamePlayDataConfig["fThornNumPerRange"];
        m_fArenaWidth = m_dicGamePlayDataConfig["fArenaWidth"];
        m_fArenaHeight = m_dicGamePlayDataConfig["fArenaHeight"];
        m_fGenerateNewBeanTimeInterval = m_dicGamePlayDataConfig["fGenerateNewBeanTimeInterval"];
        m_fGenerateNewThornTimeInterval = m_dicGamePlayDataConfig["fGenerateNewThornTimeInterval"];
        m_fBeanSize = m_dicGamePlayDataConfig["fBeanSize"];
        m_fThornSize = m_dicGamePlayDataConfig["fThornSize"];
        m_fThornContributeToBallSize = m_dicGamePlayDataConfig["fThornContributeToBallSize"];
        m_fBallBaseSpeed = m_dicGamePlayDataConfig["fBallBaseSpeed"];
        m_fSpitSporeRunDistance = m_dicGamePlayDataConfig["fSpitSporeRunDistance"];
        m_fSpitRunTime = m_dicGamePlayDataConfig["fSpitRunTime"];
        m_fSpitBallRunDistanceMultiple = m_dicGamePlayDataConfig["fSpitBallRunDistanceMultiple"];
        m_fMainTriggerPreUnfoldTime = m_dicGamePlayDataConfig["fMainTriggerPreUnfoldTime"];
        m_fMainTriggerUnfoldTime = m_dicGamePlayDataConfig["fMainTriggerUnfoldTime"];
        m_fUnfoldTimeInterval = m_dicGamePlayDataConfig["fUnfoldTimeInterval"];
        m_fUnfoldSale = m_dicGamePlayDataConfig["fUnfoldSale"];
        m_fUnfoldCircleSale = m_fUnfoldSale * 0.8f / 2.0f;
        m_fAutoAttenuate = m_dicGamePlayDataConfig["fAutoAttenuate"];
        m_fAutoAttenuateTimeInterval = m_dicGamePlayDataConfig["fAutoAttenuateTimeInterval"];
        m_fForceSpitTotalTime = m_dicGamePlayDataConfig["fForceSpitTotalTime"];
        m_fShellShrinkTotalTime = m_dicGamePlayDataConfig["fShellShrinkTotalTime"];
        if (m_fShellShrinkTotalTime < 0.1f) {
            m_fShellShrinkTotalTime = 0.1f;
        }
        m_fSpitBallTimeInterval = m_dicGamePlayDataConfig["fSpitBallTimeInterval"];
        m_fSpitSporeTimeInterval = m_dicGamePlayDataConfig["fSpitSporeTimeInterval"];
        //m_fBornMinDiameter = m_dicGamePlayDataConfig ["fBornMinDiameter"];
        m_fMinDiameterToEatThorn = m_dicGamePlayDataConfig["fMinDiameterToEatThorn"];
        m_fExplodeRunDistanceMultiple = m_dicGamePlayDataConfig["fExplodeRunDistanceMultiple"];
        m_fMaxBallNumPerPlayer = m_dicGamePlayDataConfig["fMaxBallNumPerPlayer"];
        m_fSpitNailTimeInterval = m_dicGamePlayDataConfig["fSpitNailTimeInterval"];
        m_fNailLiveTime = m_dicGamePlayDataConfig["fNailLiveTime"];
        m_fSpittedObjrunRunDistance = m_dicGamePlayDataConfig["fSpittedObjrunRunDistance"];
        m_fCrucifix = m_dicGamePlayDataConfig["fCrucifix"];
        m_nSplitNum = (int)m_dicGamePlayDataConfig["nSplitNum"];
        m_fSplitRunTime = m_dicGamePlayDataConfig["fSplitRunTime"];
        m_fSplitMaxDistance = m_dicGamePlayDataConfig["fSplitMaxDistance"];

        m_fSpitSporeAccelerate = CyberTreeMath.GetA(m_fSpitSporeRunDistance, m_fSpitRunTime);
        m_fSpitSporeInitSpeed = CyberTreeMath.GetV0(m_fSpitSporeRunDistance, m_fSpitRunTime);
    }

    // MainPlayer的Born
    public void Born()
    {
        if (AccountManager.m_eSceneMode != AccountManager.eSceneMode.Game) {
            return;
        }

        if (AccountManager.s_bObserve) {
            return;
        }

        string szPlayerName = PhotonNetwork.playerName;
        if (PhotonNetwork.playerName.Length == 0)
        {
//            g_SystemMsg.SetContent("进来不取名，菊花万人捅！！！！！！！！！！！！");
        }


        // 直接初始化本玩家的全部球出来（配置的上限是多少个就是多少个）
        /*
        Ball ball = null;
        for (int i = 0; i < m_fMaxBallNumPerPlayer; i++) {
            ball = GameObject.Instantiate(m_preBall).GetComponent<Ball>();
            m_MainPlayer.AddOneBall(ball); // main player
        }
        

        m_MainPlayer.DoSomeCollisionIgnore();
        m_MainPlayer.SetPlayerInitCompleted(true);
        m_MainPlayer.BeginGenerateMeshShape();
        */

        
        MapEditor.s_Instance.GenerateRebornSporPos();


        /*
		if (PhotonNetwork.isMasterClient) {
			m_MainPlayer.DoReborn ();
		} else {
			m_MainPlayer.PleaseSyncMyDataIfExist ();
		}
           */

       // CPlayerManager.s_Instance.TryInitBallsForThisPlayer( m_MainPlayer.GetAccount(), m_MainPlayer.GetSkinId(), m_MainPlayer.GetOwnerId(), m_MainPlayer.GetPlayerName());

    }

    void InitMainPlayer()
    {
        if (Main.s_Instance == null) {
            return;
        }

        s_Params[0] = eInstantiateType.Player;
        s_Params[1] = 111;
        m_goMainPlayer = PhotonInstantiate(m_prePlayer, Vector3.zero, s_Params);// GameObject.Instantiate( m_prePlayer );
        m_MainPlayer = m_goMainPlayer.GetComponent<Player>();

        m_MainPlayer.SetAccount(m_MainPlayer.GetOwnerId().ToString()/*AccountManager.GetAccount()*/); // 账号
        m_MainPlayer.SetPlayerName(AccountManager.GetPlayerName());    // 昵称
        m_MainPlayer.SetSkinId(ShoppingMallManager.GetCurEquipedSkinId()); // 所装备的皮肤

        m_goMainPlayer = m_MainPlayer.gameObject;
        m_photonViewMainPlayer = m_goMainPlayer.GetComponent<PhotonView>();

        m_goMainPlayer.name = "player_" + m_photonViewMainPlayer.ownerId;
        m_goMainPlayer.transform.parent = m_goBalls.transform;

        PlayerAction.s_Instance.StopAndMoveToCenter();
    }


    public int GetMainPlayerPhotonId()
    {
        return m_MainPlayer.photonView.ownerId;
    }

    public GameObject PhotonInstantiate(GameObject prefab, Vector3 vecPos, object[] aryParams = null)
    {
        return PhotonNetwork.Instantiate(prefab.name, vecPos, Quaternion.identity, 0, aryParams);
    }

    public void PhotonDestroy(GameObject obj)
    {
        PhotonNetwork.Destroy(obj);
    }

    int woca = 1;

    public bool IsSkillColdDowning(CSkillSystem.eSkillId id)
    {
        if (id == CSkillSystem.eSkillId.w_spit)
        {
            return m_fSpitBallCurColdDownTime > 0.0f;
        }
        else if (id == CSkillSystem.eSkillId.e_unfold)
        {
            return m_fUnfoldCurColdDownTime > 0.0f;
        }
        else if ((int)id >= 3)
        {
            return m_SelectedSkillColdDownTimeCount > 0.0f;
        }

        return false;
    }

    public void SetSkillColdownTxtActive(CSkillSystem.eSkillId eskillId, bool bActive)
    {
        if (eskillId == CSkillSystem.eSkillId.w_spit)
        {
            if (GetPlatformType() == ePlatformType.pc)
            {
                m_txtColdDown_W.gameObject.SetActive(bActive);
            }
            else if (GetPlatformType() == ePlatformType.mobile)
            {
                m_txtColdDown_W_Mobile.gameObject.SetActive(bActive);
            }
        }
    }

    public void SetSkillColdownTxtValue(CSkillSystem.eSkillId eskillId, string szValue)
    {
        if (eskillId == CSkillSystem.eSkillId.w_spit)
        {
            if (GetPlatformType() == ePlatformType.pc)
            {
                m_txtColdDown_W.text = szValue;
            }
            else if (GetPlatformType() == ePlatformType.mobile)
            {
                m_txtColdDown_W_Mobile.text = szValue;
            }
        }
    }

    void ColdDownLoop()
    {
        // SpitBall ColdDown
        if (!m_bForceSpit)
        {
            if (m_fSpitBallCurColdDownTime > 0.0f)
            {
                m_imgSpitBall_Fill.gameObject.SetActive(true);
                //m_txtColdDown_W.gameObject.SetActive(true);
                //SetSkillColdownTxtActive( CSkillSystem.eSkillId.w_spit, true );
                CSkillSystem.s_Instance.SetSkillColdownActive(CSkillSystem.eSkillId.w_spit, true);
                m_fSpitBallCurColdDownTime -= Time.deltaTime;
                if (m_fSpitBallTimeInterval > 0)
                {
                    //m_imgSpitBall_Fill.fillAmount = ( m_fSpitBallTimeInterval - m_fSpitBallCurColdDownTime ) / m_fSpitBallTimeInterval;
                    float fFillAmount = m_fSpitBallCurColdDownTime / m_fSpitBallTimeInterval;
                    if (m_fSpitBallCurColdDownTime >= 1f)
                    {
                        // m_txtColdDown_W.text = m_fSpitBallCurColdDownTime.ToString("f0");
                        // SetSkillColdownTxtValue(CSkillSystem.eSkillId.w_spit, m_fSpitBallCurColdDownTime.ToString("f0")); 
                        string szLeftTime = m_fSpitBallCurColdDownTime.ToString("f0");
                        CSkillSystem.s_Instance.SetSkillColdownInfo(CSkillSystem.eSkillId.w_spit, fFillAmount, szLeftTime);
                    }
                    else
                    {
                        //m_txtColdDown_W.text = m_fSpitBallCurColdDownTime.ToString("f1");
                        //SetSkillColdownTxtValue(CSkillSystem.eSkillId.w_spit, m_fSpitBallCurColdDownTime.ToString("f1"));
                        string szLeftTime = m_fSpitBallCurColdDownTime.ToString("f1");
                        CSkillSystem.s_Instance.SetSkillColdownInfo(CSkillSystem.eSkillId.w_spit, fFillAmount, szLeftTime);

                    }

                }
                else
                {
                    // m_imgSpitBall_Fill.fillAmount = 1f;
                }
            }
            else
            {
                //m_imgSpitBall_Fill.gameObject.SetActive( false );
                //m_txtColdDown_W.gameObject.SetActive(false);
                CSkillSystem.s_Instance.SetSkillColdownActive(CSkillSystem.eSkillId.w_spit, false);
            }
        }

        // SpitSpore ColdDown
        if (m_fSpitSporeCurColdDownTime > 0.0f)
        {
            m_fSpitSporeCurColdDownTime -= Time.deltaTime;
            if (m_fSpitSporeTimeInterval > 0)
            {
                m_imgSpitSpore_Fill.fillAmount = (m_fSpitSporeTimeInterval - m_fSpitSporeCurColdDownTime) / m_fSpitSporeTimeInterval;
            }
            else
            {
                m_imgSpitSpore_Fill.fillAmount = 1f;
            }
        }

        // unfold colddown
        if (m_fUnfoldCurColdDownTime > 0.0f)
        {
            //m_imgUnfold_Fill.gameObject.SetActive( true );
            //m_txtColdDown_E.gameObject.SetActive(true);
            m_fUnfoldCurColdDownTime -= Time.deltaTime;
            CSkillSystem.s_Instance.SetSkillColdownActive(CSkillSystem.eSkillId.e_unfold, true);
            if (m_fUnfoldTimeInterval > 0)
            {
                float fFillAmount = m_fUnfoldCurColdDownTime / m_fUnfoldTimeInterval;
                if (m_fUnfoldCurColdDownTime > 1f)
                {
                    // m_txtColdDown_E.text = m_fUnfoldCurColdDownTime.ToString("f0");
                    string szLeftTime = m_fUnfoldCurColdDownTime.ToString("f0");
                    CSkillSystem.s_Instance.SetSkillColdownInfo(CSkillSystem.eSkillId.e_unfold, fFillAmount, szLeftTime);
                }
                else
                {
                    // m_txtColdDown_E.text = m_fUnfoldCurColdDownTime.ToString("f1");
                    string szLeftTime = m_fUnfoldCurColdDownTime.ToString("f1");
                    CSkillSystem.s_Instance.SetSkillColdownInfo(CSkillSystem.eSkillId.e_unfold, fFillAmount, szLeftTime);
                }
            }
            else
            {
                //m_imgUnfold_Fill.fillAmount = 1f;
            }
        }
        else
        {
            //m_txtColdDown_E.gameObject.SetActive(false);
            //m_imgUnfold_Fill.gameObject.SetActive(false);
            CSkillSystem.s_Instance.SetSkillColdownActive(CSkillSystem.eSkillId.e_unfold, false);
        }

        // become-thorn
        if (m_fCurColdDownTime_BecomeThor_Count > 0.0f)
        {
            m_fCurColdDownTime_BecomeThor_Count -= Time.deltaTime;
            m_SelectedSkillColdDownInterval = m_fCurColdDownTime_BecomeThorn;
            m_SelectedSkillColdDownTimeCount = m_fCurColdDownTime_BecomeThor_Count;
            if (m_fCurColdDownTime_BecomeThorn > 0)
            {
                m_imgSelectedSkill_Fill.fillAmount = (m_fCurColdDownTime_BecomeThorn - m_fCurColdDownTime_BecomeThor_Count) / m_fCurColdDownTime_BecomeThorn;
            }
            else
            {
                m_imgSelectedSkill_Fill.fillAmount = 1f;
            }
        }


        // Sneak ColdDown
        if (m_fSkillColdDownCount_Sneak > 0)
        {
            m_SelectedSkillColdDownInterval = m_fSkillColdDownCount;
            m_fSkillColdDownCount_Sneak -= Time.deltaTime;
            m_SelectedSkillColdDownTimeCount = m_fSkillColdDownCount_Sneak;
        }


        // Annihilate ColdDown
        if (m_fSkillColdDownCount_Annihilate > 0)
        {
            m_SelectedSkillColdDownInterval = m_fSkillColdDown_Annihilate;
            m_fSkillColdDownCount_Annihilate -= Time.deltaTime;
            m_SelectedSkillColdDownTimeCount = m_fSkillColdDownCount_Annihilate;
            if (m_fSkillColdDown_Annihilate > 0)
            {
                m_imgSelectedSkill_Fill.fillAmount = (m_fSkillColdDown_Annihilate - m_fSkillColdDownCount_Annihilate) / m_fSkillColdDown_Annihilate;
            }
            else
            {
                m_imgSelectedSkill_Fill.fillAmount = 1f;
            }
        }

        // MagicShield ColdDown
        if (m_fSkillColdDownCount_MagicShield > 0)
        {
            m_SelectedSkillColdDownInterval = m_fSkillColdDown_MagicShield;
            m_fSkillColdDownCount_MagicShield -= Time.deltaTime;
            m_SelectedSkillColdDownTimeCount = m_fSkillColdDownCount_MagicShield;
            if (m_fSkillColdDown_MagicShield > 0)
            {
                m_imgSelectedSkill_Fill.fillAmount = (m_fSkillColdDown_MagicShield - m_fSkillColdDownCount_MagicShield) / m_fSkillColdDown_MagicShield;
            }
            else
            {
                m_imgSelectedSkill_Fill.fillAmount = 1f;
            }
        }

        // MergeAll ColdDown
        if (m_fSkillColdDownCount_MergeAll > 0)
        {
            m_SelectedSkillColdDownInterval = m_fSkillColdDown_MergeAll;
            m_fSkillColdDownCount_MergeAll -= Time.deltaTime;
            m_SelectedSkillColdDownTimeCount = m_fSkillColdDownCount_MergeAll;
            if (m_fSkillColdDown_MergeAll > 0)
            {
                m_imgSelectedSkill_Fill.fillAmount = (m_fSkillColdDown_MergeAll - m_fSkillColdDownCount_MergeAll) / m_fSkillColdDown_MergeAll;
            }
            else
            {
                m_imgSelectedSkill_Fill.fillAmount = 1f;
            }
        }

        // Henzy ColdDown
        if (m_fSkillColdDownCount_Henzy > 0)
        {
            m_SelectedSkillColdDownInterval = m_fSkillColdDown_Henzy;
            m_fSkillColdDownCount_Henzy -= Time.deltaTime;
            m_SelectedSkillColdDownTimeCount = m_fSkillColdDownCount_Henzy;
            if (m_fSkillColdDown_Henzy > 0)
            {
                m_imgSelectedSkill_Fill.fillAmount = (m_fSkillColdDown_Henzy - m_fSkillColdDownCount_Henzy) / m_fSkillColdDown_Henzy;
            }
            else
            {
                m_imgSelectedSkill_Fill.fillAmount = 1f;
            }
        }

        // Henzy ColdDown
        if (m_fSkillColdDownCount_Gold > 0)
        {
            m_SelectedSkillColdDownInterval = m_fSkillColdDown_Gold;
            m_fSkillColdDownCount_Gold -= Time.deltaTime;
            m_SelectedSkillColdDownTimeCount = m_fSkillColdDownCount_Gold;
            if (m_fSkillColdDown_Gold > 0)
            {
                m_imgSelectedSkill_Fill.fillAmount = (m_fSkillColdDown_Gold - m_fSkillColdDownCount_Gold) / m_fSkillColdDown_Gold;
            }
            else
            {
                m_imgSelectedSkill_Fill.fillAmount = 1f;
            }
        }

        ////
        if (m_SelectedSkillColdDownTimeCount > 0)
        {
            //m_imgSelectedSkill_Fill.gameObject.SetActive( true );
            //m_txtColdDown_R.gameObject.gameObject.SetActive(true);
            CSkillSystem.s_Instance.SetSkillColdownActive(CSkillSystem.eSkillId.r_sneak, true);
            float fFillAmount = m_SelectedSkillColdDownTimeCount / m_SelectedSkillColdDownInterval;
            if (m_SelectedSkillColdDownTimeCount > 1f)
            {
                //m_txtColdDown_R.text = m_SelectedSkillColdDownTimeCount.ToString("f0");
                string szLeftTime = m_SelectedSkillColdDownTimeCount.ToString("f0");
                CSkillSystem.s_Instance.SetSkillColdownInfo(CSkillSystem.eSkillId.r_sneak, fFillAmount, szLeftTime);
            }
            else
            {
                string szLeftTime = m_SelectedSkillColdDownTimeCount.ToString("f1");
                CSkillSystem.s_Instance.SetSkillColdownInfo(CSkillSystem.eSkillId.r_sneak, fFillAmount, szLeftTime);

            }
        }
        else
        {
            //m_txtColdDown_R.gameObject.gameObject.SetActive(false);
            //m_imgSelectedSkill_Fill.gameObject.SetActive(false);
            CSkillSystem.s_Instance.SetSkillColdownActive(CSkillSystem.eSkillId.r_sneak, false);
        }
        ////

    }

    int m_nDeadStatus = 0; // 0 - 没有展示Uidead的状态  1 - 正在展示UiDead的状态
    float m_fDeadProgressTimeCount = 0f;
    List<Sprite> lstAssistAvatar = new List<Sprite>();
    public void ShowWhoKillWhom(Player playerKiller, Player playerDeadMeat)
    {
        //_msglistKillPeople.AddOneItem(szKiller, szDeadMeat);
        lstAssistAvatar.Clear();
        List<int> lstAssist = playerDeadMeat.GetAssistPlayerIdList();
        for (int i = 0; i < lstAssist.Count; i++)
        {
            Player playerAssist = CPlayerManager.s_Instance.GetPlayer(lstAssist[i]);
            if (playerKiller.GetOwnerId() == playerAssist.GetOwnerId())
            {
                continue;
            }
            lstAssistAvatar.Add(playerAssist.GetAvatar());
        }
        _msglistKillPeople.AddOneItem(playerKiller.GetPlayerName(), playerKiller.GetAvatar(), lstAssistAvatar, playerDeadMeat.GetPlayerName(), playerDeadMeat.GetAvatar());

    }

    float m_fDeadUICount = 0;
    void ProcessDead()
    {
        if ((m_MainPlayer == null) || (!m_MainPlayer.IsRebornCompleted()) || (!m_MainPlayer.IsDead() )) {
            return;
        }

        if (m_nDeadStatus == 0) { // UI_Dead界面出现阶段
            MapEditor.s_Instance.GenerateRebornSporPos();
            Main.s_Instance.m_MainPlayer.SetMoving(false);
            PlayerAction.s_Instance._release_joystick_fast_stop = true;
            m_MainPlayer.ProcessPlayerDead();

            // “击杀”公告触发不能在这里了，因为死者有可能是“孤儿球”，它们的Player已下线，没有Player来触发
            // m_MainPlayer.AnnouceWhoKillWhom();


            //CBlackAndWhiteShader.s_Instance.BeginFade(m_fTimeBeforeUiDead * 0.5f);
            CShakeCamera.s_Instance.shake();
            CAudioManager.s_Instance.PlayAudio(CAudioManager.eAudioId.e_audio_dead);
            m_nDeadStatus = 1;
            //_uiDeadNew.SetActive( true );
            m_fDeadUICount = CCameraManager.s_Instance.GetTimeBeforeUp();
            CDeadManager.s_Instance.BeginRebornWaitingTimeLoop(m_fDeadUICount);
        }
        else if (m_nDeadStatus == 1)
        {
            OnClickBtn_Reborn();

            /*
            m_fDeadUICount -= Time.deltaTime;
            if (m_fDeadUICount <= 0f)
            {
                int nDeadWaitingTime = CGrowSystem.s_Instance.GetDeadWaitingTime(m_MainPlayer.GetLevel());
                CCameraManager.s_Instance.BeginRebornCameraUp(nDeadWaitingTime);
                m_nDeadStatus = 2;
            }
            */
        }
        else if (m_nDeadStatus == 2)
        {
            /*
            m_fDeadProgressTimeCount += Time.deltaTime;
            if (m_fDeadProgressTimeCount >= CCameraManager.s_Instance.GetDeadWatchTime()) {
                m_nDeadStatus = 3;
                //_uiDead.SetActive (true);
                m_fDeadProgressTimeCount = 0f;
                _uiDeadNew.SetActive(false);
                OnClickBtn_Reborn();
                CCameraManager.s_Instance.BeginRebornCameraDown();
                
            
            */
        }
        else if (m_nDeadStatus == 3) {

        }



    }

    public static List<Player> m_lstDelay = new List<Player>();
   
    void GameUpdate()
    {
        if (!IsGameStarted())
        {
            return;
        }

       
        /*
        if (m_lstDelay.Count > 0)
        {
            Debug.Log(" ____________________________");
            for (int i = 0; i < m_lstDelay.Count; i++)
            {
                Debug.Log("delayed player: " + m_lstDelay[i].photonView.ownerId);
            }
            Debug.Log(" *********************************");
        }
        */

        if (AccountManager.m_eSceneMode != AccountManager.eSceneMode.Game)
        {
            return;
        }

        if (!MapEditor.s_Instance.IsMapInitCompleted()) { // 地图初始化完成之前，不要开始执行游戏逻辑
            return;
        }

        Test_Fuck_Loop();

        ProcessTimeLeft();


        if (AccountManager.s_bObserve)
        {
            return;
        }

        SpitTouching();

        //PreOneBtnSplitting ();
        //InitingBalls ();
        InitArenaBeans();
        InitArenaThorns();

        Thorn.GenerateNewThornLoop();
        Thorn.ColorThornReborn();

        ColdDownLoop();
        CameraSizeProtectLoop();

        ForceSpitting();

        ProcessDead();

        ProcessKeyboard();
        ProcessMouse();
        ProcessTouch();

        //AutoAttenuate();
        if (m_goMainPlayer)
        {
            //m_textDebugInfo.text = "主角球球数: " + m_goMainPlayer.transform.childCount.ToString () + "\n场景豆子和场景刺总数:" + (m_nBeanTotalNumDueToRange + m_nThornTotalNumDueToRange) .ToString();
            //m_textDebugInfo.text = PhotonNetwork.time.ToString();
            //m_textDebugInfo.text = PhotonNetwork.isMasterClient.ToString();
            //m_textDebugInfo.text = the_spray.transform.childCount.ToString();
        }

    }


    void Update()
    {
        if ( !IsGameStarted())
        {
            return;
        }

        if (AccountManager.m_eSceneMode == AccountManager.eSceneMode.None) {
            return;
        }

        GameUpdate();

    }

    // 判断当前是否点击在了UI上
    public bool IsPointerOverUI()
    {
        PointerEventData eventData = new PointerEventData(UnityEngine.EventSystems.EventSystem.current);
        eventData.pressPosition = Input.mousePosition;
        eventData.position = Input.mousePosition;

        List<RaycastResult> list = new List<RaycastResult>();
        UnityEngine.EventSystems.EventSystem.current.RaycastAll(eventData, list);

        return list.Count > 0;

    }

    void ProcessTouch()
    {
        if (m_nPlatform != 1) {
            return;
        }

        if (IsPointerOverUI()) {
            return;
        }
        /*
		if (Input.touchCount > 0) {
			if (!m_MainPlayer.IsMoving ()) {
				m_MainPlayer.SetMoving ( true );
			}
		}
        */
    }

    void ProcessMouse()
    {
        //poppin test
        /*
		if (m_nPlatform != 0) {
			return;
		}
        */

        if (IsPointerOverUI()) {
            return;
        }

        if (Input.GetMouseButtonDown(1)) {
            if (m_MainPlayer) {
                m_MainPlayer.SetMoving(false);
            }
        }

        if (Input.GetMouseButtonDown(0)) {
            if (m_MainPlayer && GetPlatformType() == ePlatformType.pc) {
                m_MainPlayer.SetMoving(true);
            }
        }

    }

    bool m_bCtrlDown = false;
    bool m_bShowRank = false;
    bool m_bKeyDown_R = false;
    bool m_bKeyDown_E = false;
    void ProcessKeyboard()
    {
        /*
        if (CChatSystem.s_Instance && CChatSystem.s_Instance.IsShowing()) { // 当前聊天输入框获得了焦点，正在输入，不要执行其它键盘操作
            return;
        }
*/
        /*
        if (Input.GetKeyDown(KeyCode.T))
        {
            if (m_fLababTimeCount >= m_fLababaColdDown)
            {
                UseSkill_LaBaba();
                m_fLababTimeCount = 0f;
            }
        }
		*/

        if (Input.GetKeyDown(KeyCode.V))
        {
            // OnButtonClick_ChangeSkin();
            Test_AddMP();
        }

        if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
        {
            m_bCtrlDown = true;
        }
        else
        {
            m_bCtrlDown = false;
        }


        if (Input.GetKey(KeyCode.Tab))
        {
            if (!m_bShowRank)
            {
                //_panelRank.SetActive(true);
                CPaiHangBang_Mobile.s_Instance.ShowPaiHangBang();
                m_bShowRank = true;
            }
        }
        else
        {
            if (m_bShowRank)
            {
                //_panelRank.SetActive(false);
                CPaiHangBang_Mobile.s_Instance.HidePaiHangBang();
                m_bShowRank = false;
            }
        }

        if (Input.GetKeyDown(KeyCode.Z))
        {
            Test_AddBallsSize();
        }

        if (Input.GetKeyDown(KeyCode.X))
        {
            MergeAll();
        }

        if (Input.GetKeyDown(KeyCode.K))
        {
            Suicide();
        }





        if (Input.GetKeyDown(KeyCode.B))
        {
            CSkillSystem.s_Instance.SetTotalPoint(CSkillSystem.s_Instance.GetTotalPoint() + 1);
        }

        if (Input.GetKeyDown(KeyCode.N))
        {
            CMoneySystem.s_Instance.SetMoney(CMoneySystem.s_Instance.GetMoney() + 100);
        }

        if (Input.GetKeyDown(KeyCode.M))
        {
            CGrowSystem.s_Instance.AddExp(100);
        }


        if (Input.GetKeyDown(KeyCode.C))
        {
            Test_KingExplode();
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            Test_Fuck();
        }

        if (Input.GetKeyDown(KeyCode.O))
        {
            CJieSuanManager.s_Instance.Show();
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            if (m_bCtrlDown) // 此时Ctrl键是按下的，则进行该技能的技能加点
            {
                CSkillSystem.s_Instance.AddPoint((int)CSkillSystem.eSkillId.q_spore);
            }
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            if (m_bCtrlDown) // 此时Ctrl键是按下的，则进行该技能的技能加点
            {
                CSkillSystem.s_Instance.AddPoint((int)CSkillSystem.eSkillId.w_spit);
            }
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            if (m_bCtrlDown) // 此时Ctrl键是按下的，则进行该技能的技能加点
            {
                CSkillSystem.s_Instance.AddPoint((int)CSkillSystem.eSkillId.e_unfold);
            }
        }

        if (GetPlatformType() == ePlatformType.pc)
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                if (m_bCtrlDown) // 此时Ctrl键是按下的，则进行该技能的技能加点
                {
                    CSkillSystem.s_Instance.AddPoint((int)CSkillSystem.eSkillId.t_become_thorn);
                }
            }

            if (Input.GetKey(KeyCode.Q))
            {
                if (m_bCtrlDown) // 此时Ctrl键是按下的，则进行该技能的技能加点
                {

                }
                else
                {
                    if (!m_MainPlayer.IsSpittingSpore())
                    {
                        m_MainPlayer.BeginSpitSpore();
                    }
                }
            }
            else
            {
                if (m_MainPlayer.IsSpittingSpore())
                {
                    m_MainPlayer.EndSpitSpore();
                }
            }
        }


        if (Input.GetKeyUp(KeyCode.Q))
        {
            if (m_MainPlayer.IsSpittingSpore())
            {
                m_MainPlayer.EndSpitSpore();
            }
        }



        if (Input.GetKey(KeyCode.Escape))
        {
            ExitGame();
        }

        if (GetPlatformType() == ePlatformType.pc)
        {
            if (!UI_Spit.s_bUsingUi)
            {
                if (Input.GetKey(KeyCode.W))
                {
                    if (m_bCtrlDown) // 此时Ctrl键是按下的，则进行该技能的技能加点
                    {

                    }
                    else
                    {
                        if (!Input.GetKeyDown(KeyCode.W))
                        {
                            BeginSpit();
                        }
                    }
                }
                else
                {
                    if (m_bForceSpit)
                    {
                        EndSpit();
                    }
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            if (m_bCtrlDown) // 此时Ctrl键是按下的，则进行该技能的技能加点
            {

            }
            else
            {
                if (m_fUnfoldCurColdDownTime <= 0.0f)
                {
                    Unfold();
                }
                else if (!m_bKeyDown_E)
                {
                    g_SystemMsg.SetContent(CMsgSystem.s_Instance.GetMsgContentByType(CMsgSystem.eSysMsgType.cold_down_not_completed));
                    return;
                }
                m_bKeyDown_E = true;
            }
        }
        else
        {
            m_bKeyDown_E = false;
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            if (m_bCtrlDown) // 此时Ctrl键是按下的，则进行该技能的技能加点
            {
                CSkillSystem.s_Instance.AddPickSkillPoint();
            }
        }


        // R键是英雄技能，不是基础技能。依据玩家进游戏之前的自主选择而定
        if (Input.GetKeyDown(KeyCode.R))
        {


            /*
            if (m_bCtrlDown) // 此时Ctrl键是按下, 则不能释放技能
            {

            }
            else
            {
                if ( m_fSkillColdDownCount_Sneak <= 0)
                {
                    CastSkill_Sneak();
                }
            }
            */
            if (m_bCtrlDown) // 此时Ctrl键是按下, 则不能释放技能
            {

            }
            else
            {
                Cast_R_Skill();
            }// if (m_bCtrlDown)

            m_bKeyDown_R = true;
        }
        else // if ( Input.GetKey( Keycode.R ) )
        {
            m_bKeyDown_R = false;
        }




        if (Input.GetKey(KeyCode.S)) // 湮灭
        {
            if (m_bCtrlDown) // 此时Ctrl键是按下, 则不能释放技能
            {

            }
            else
            {
                /*
                if (m_fSkillColdDownCount_Annihilate <= 0)
                {
                    CastSkill_Annihilate();
                }
                */
            }

        }

        if (Input.GetKey(KeyCode.D)) // 魔盾
        {
            if (m_bCtrlDown) // 此时Ctrl键是按下, 则不能释放技能
            {

            }
            else
            {
                /*
                if (m_fSkillColdDownCount_MagicShield <= 0)
                {
                    CastSkill_MagicShield();
                }
                */
            }

        }

        if (Input.GetKey(KeyCode.F)) // 秒合
        {
            if (m_bCtrlDown) // 此时Ctrl键是按下, 则不能释放技能
            {

            }
            else
            {
                /*
                if (m_fSkillColdDownCount_MergeAll <= 0)
                {
                    CastSkill_MergeAll();
                }
                */
            }

        }

        if (Input.GetKey(KeyCode.G)) // 狂暴
        {
            if (m_bCtrlDown) // 此时Ctrl键是按下, 则不能释放技能
            {

            }
            else
            {
                /*
                if (m_fSkillColdDownCount_Henzy <= 0)
                {
                    CastSkill_Henzy();
                }
                */
            }

        }

        if (Input.GetKey(KeyCode.A)) // PC版二分分球
        {
            if (m_bCtrlDown) // 此时Ctrl键是按下的，则进行该技能的技能加点
            {

            }
            else
            {
                /*
                if (m_fCurColdDownTime_BecomeThor_Count <= 0f)
                {
                    m_imgLababa_Fill.fillAmount = 1;
                    BecomeThorn();
                }	
                */
            }
        }

    }

    public void OnClick_OneBtnSplitBall()
    {
        /*
		if (m_fSplitTimeCount > m_fSplitTimeInterval ) {
			OneBtnSplitBall ();
			m_fSplitTimeCount = 0f;
		}  
		*/
    }


    public void Cast_R_Skill()
    {
        int nPickSkillId = CSkillSystem.s_Instance.GetPickSkillId();
        if (nPickSkillId == CSkillSystem.INVALID_SKILL_ID)
        {
            //g_SystemMsg.SetContent("可选技能槽为空耶");
            return;
        }
        switch (nPickSkillId/*AccountManager.s_nSelectedSkillId*/ )
        {
            case 3:
                {
                    if (m_SelectedSkillColdDownTimeCount <= 0)
                    {
                        CastSkill_Sneak();
                    }
                    else if (!m_bKeyDown_R)
                    {
                        g_SystemMsg.SetContent(CMsgSystem.s_Instance.GetMsgContentByType(CMsgSystem.eSysMsgType.cold_down_not_completed));
                    }
                }
                break;
            case 4:
                {
                    if (m_SelectedSkillColdDownTimeCount <= 0f)
                    {
                        m_imgLababa_Fill.fillAmount = 1;
                        BecomeThorn();
                    }
                    else if (!m_bKeyDown_R)
                    {
                        g_SystemMsg.SetContent(CMsgSystem.s_Instance.GetMsgContentByType(CMsgSystem.eSysMsgType.cold_down_not_completed));
                    }

                }
                break;
            case 5:
                {
                    if (m_SelectedSkillColdDownTimeCount <= 0)
                    {
                        CastSkill_Annihilate();
                    }
                    else if (!m_bKeyDown_R)
                    {
                        g_SystemMsg.SetContent(CMsgSystem.s_Instance.GetMsgContentByType(CMsgSystem.eSysMsgType.cold_down_not_completed));
                    }

                }
                break;
            case 6:
                {
                    if (m_SelectedSkillColdDownTimeCount <= 0)
                    {
                        CastSkill_MagicShield();
                    }
                    else if (!m_bKeyDown_R)
                    {
                        g_SystemMsg.SetContent(CMsgSystem.s_Instance.GetMsgContentByType(CMsgSystem.eSysMsgType.cold_down_not_completed));
                    }

                }
                break;
            case 7:
                {
                    if (m_SelectedSkillColdDownTimeCount <= 0)
                    {
                        CastSkill_MergeAll();
                    }
                    else if (!m_bKeyDown_R)
                    {
                        g_SystemMsg.SetContent(CMsgSystem.s_Instance.GetMsgContentByType(CMsgSystem.eSysMsgType.cold_down_not_completed));
                    }

                }
                break;
            case 8:
                {
                    if (m_SelectedSkillColdDownTimeCount <= 0)
                    {
                        CastSkill_Henzy();
                    }
                    else if (!m_bKeyDown_R)
                    {
                        g_SystemMsg.SetContent(CMsgSystem.s_Instance.GetMsgContentByType(CMsgSystem.eSysMsgType.cold_down_not_completed));
                    }

                }
                break;
            case 9: // 金壳
                {
                    if (m_SelectedSkillColdDownTimeCount <= 0)
                    {
                        CastSkill_Gold();
                    }
                    else if (!m_bKeyDown_R)
                    {
                        g_SystemMsg.SetContent(CMsgSystem.s_Instance.GetMsgContentByType(CMsgSystem.eSysMsgType.cold_down_not_completed));
                    }

                }
                break;
        } // switch (nPickSkillId )
    }

    float m_fSplitTimeCount = 0;
    float m_fLababTimeCount = 1024f;

    float m_fArenaRange = 0.0f;
    float m_nBeanTotalNumDueToRange = 0;
    float m_nCurBeanTotalNum = 0;
    float m_nCurGeneraedByMeBeanTotalNum = 0;
    int m_nCurGeneraedByMeThornTotalNum = 0;
    float m_nThornTotalNumDueToRange = 0;
    float m_nCurThornTotalNum = 0;
    public void UpdateWorldSize(float fWorldSizeX, float fWorldSizeY)
    {
        /*
		m_fArenaRange = m_fArenaWidth * m_fArenaHeight;
		m_nBeanTotalNumDueToRange = (int)( m_fBeanNumPerRange * m_fArenaRange );
		m_nThornTotalNumDueToRange =  (int)( m_fThornNumPerRange * m_fArenaRange );
		m_fWorldLeft = -m_fArenaWidth / 2.0f;
		m_fWorldRight = m_fArenaWidth / 2.0f; 
		m_fWorldBottom = -m_fArenaHeight / 2.0f;
		m_fWorldTop = m_fArenaHeight / 2.0f;
        */
        m_fArenaRange = fWorldSizeX * fWorldSizeY;
        m_nBeanTotalNumDueToRange = (int)(m_fBeanNumPerRange * m_fArenaRange);
        m_nThornTotalNumDueToRange = (int)(m_fThornNumPerRange * m_fArenaRange);
        m_fWorldLeft = -fWorldSizeX / 2.0f;
        m_fWorldRight = fWorldSizeX / 2.0f;
        m_fWorldBottom = -fWorldSizeY / 2.0f;
        m_fWorldTop = fWorldSizeY / 2.0f;
    }

    // 为竞技场生成豆子。注意一定要分帧处理，不然会卡死。


    // 平衡场景豆子的总量
    float m_fGenerateBeanTime = 0.0f;
    float m_fGenerateThornTime = 0.0f;
    const float c_GenerateTotalTime = 10.0f;
    public void InitArenaBeans()
    {
        /*
       if (m_nCurGeneraedByMeBeanTotalNum >= m_nBeanTotalNumDueToRange   ) {
			return;
		}

	    GenerateBean ();
        m_nCurGeneraedByMeBeanTotalNum++;
		*/
    }

    // 平衡刺的个数
    public void InitArenaThorns()
    {
        /*
        if (m_nCurGeneraedByMeThornTotalNum >= m_nThornTotalNumDueToRange)
        {
            return;
        }

		GenerateThorn ();
        m_nCurGeneraedByMeThornTotalNum++;
		*/
    }




    /*
	public void CalculateCurTotalBeanNum()
	{
		vecWorldMin = Camera.main.ScreenToWorldPoint ( vecScreenMin );
		vecWorldMax = Camera.main.ScreenToWorldPoint ( vecScreenMax );
		float fWorldWidth = vecWorldMax.x - vecWorldMin.x;
		float fWorldHeight = vecWorldMax.y - vecWorldMin.y;
		float fRange = fWorldWidth * fWorldHeight;
		m_nCurBeanTotalNum = (int)(fRange * m_fBeanNumPerRange);
	}



	public void CalculateCurTotalThornNum()
	{
		vecWorldMin = Camera.main.ScreenToWorldPoint ( vecScreenMin );
		vecWorldMax = Camera.main.ScreenToWorldPoint ( vecScreenMax );
		float fWorldWidth = vecWorldMax.x - vecWorldMin.x;
		float fWorldHeight = vecWorldMax.y - vecWorldMin.y;
		float fRange = fWorldWidth * fWorldHeight;
		//Debug.Log ( "fRange=" + fRange );
		m_nCurThornTotalNum = (int)(fRange * m_fThornNumPerRange);
	}
*/
    float m_fWorldLeft = 0.0f, m_fWorldRight = 0.0f, m_fWorldBottom = 0.0f, m_fWorldTop = 0.0f;
    public Vector3 RandomPosWithinWorld()
    {
        vecTempPos.x = (float)UnityEngine.Random.Range(m_fWorldLeft, m_fWorldRight);
        vecTempPos.y = (float)UnityEngine.Random.Range(m_fWorldBottom, m_fWorldTop);
        vecTempPos.z = 0.0f;
        return vecTempPos;
    }

    Vector3 vecScreenMin = new Vector3(0.0f, 0.0f, 0.0f);
    Vector3 vecScreenMax = new Vector3(Screen.width, Screen.height, 0.0f);
    Vector3 vecWorldMin = new Vector3();
    Vector3 vecWorldMax = new Vector3();
    public Vector3 RandomPosWithinScreen()
    {
        vecTempPos.x = (float)UnityEngine.Random.Range(vecWorldMin.x, vecWorldMax.x);
        vecTempPos.y = (float)UnityEngine.Random.Range(vecWorldMin.y, vecWorldMax.y);
        vecTempPos.z = 0.0f;
        return vecTempPos;
    }

    public void GenerateBean()
    {
        /*
		//GameObject goBean = PhotonInstantiate( m_preBean, new Vector3 ( (float)UnityEngine.Random.Range(-200, 200), (float)UnityEngine.Random.Range(-200, 200) , 10.0f ) ) ;
		Vector3 pos = RandomPosWithinWorld();
		CMonster bean = ResourceManager.ReuseBean();
		if (bean == null) {
			return;
		}
		bean.SetIntParam( 0, (int)CMonster.eBeanType.scene ); // 豆子类型：场景静态豆子
		bean.SetFloatParam( 0, 1f ); // 豆子体积含量：
		bean.transform.transform.parent = m_goBeans.transform;
		bean.transform.localPosition = pos;
		*/
    }

    Dictionary<int, CMonster> m_dicAllThorns = new Dictionary<int, CMonster>(); // 游戏世界中所有的刺（无论什么途径产生的）
    public void GenerateThorn()
    {
        /*
		vecTempPos = MapEditor.s_Instance.GetFakeRandomThornPos( (int)m_nCurGeneraedByMeThornTotalNum );
		vecTempPos.z = 0;
		CMonster thorn = ResourceManager.ReuseThorn ().GetComponent<CMonster> ();
		thorn.SetDead ( false );
		thorn.transform.parent = m_goThorns.transform;
		thorn.transform.localPosition = vecTempPos;

		thorn.SetFloatParam (0, 3f); // 体积含量
		thorn.SetSize( 2f );         // 自身尺寸

		thorn.SetIntParam ( 0, (int)CMonster.eThornType.scene );


		m_dicAllThorns[thorn.GetId()] = thorn;
		*/
    }


    // 重生场景刺
    public void RebornSceneThorn(int nThornIndex)
    {
        /*
        if (nThornIndex >= m_lstArenaThorn.Count)
        {
            return;
        }
        Thorn thorn = m_lstArenaThorn[nThornIndex];
        thorn.SetDead(true);
        Thorn.AddToRebornList(thorn);
		*/
    }

    void FixedUpdate()
    {


    }

    public float m_fForceSpitPercent = 0.0f;
    bool m_bForceSpit = false;
    float m_fSpitBallCurColdDownTime = 0.0f;
    public void BeginSpit()
    {
        if (m_bForceSpit)
        {
            return;
        }

        int nAvailableNum = m_MainPlayer.GetCurAvailableBallCount();
        if (nAvailableNum <= 0) {
            Main.s_Instance.g_SystemMsg.SetContent(CMsgSystem.s_Instance.GetMsgContentByType(CMsgSystem.eSysMsgType.reach_max_ball_num));
            return;
        }

        if (m_fSpitBallCurColdDownTime > 0.0f) { // ColdDown not completed
            g_SystemMsg.SetContent(CMsgSystem.s_Instance.GetMsgContentByType(CMsgSystem.eSysMsgType.cold_down_not_completed));
            return;
        }

        
        if (!m_MainPlayer.CheckIfCanSpitBall())
        {
            return;
        }
        

        //// 获取该技能当前等级参数
        //// 获取该技能当前的参数（依据该技能当前累积的技能点）
        bool bCanUseSkill = true;
        CSkillSystem.sSkillParam skill_param = CSkillSystem.s_Instance.GetSkillParamById(CSkillSystem.eSkillId.w_spit, ref bCanUseSkill);
        float fMpCost = skill_param.aryValues[0];
        m_fSpitBallTimeInterval = m_fSpitBallCurColdDownTime = skill_param.aryValues[1]; // cold down

        if (m_MainPlayer.IsHenzy())
        {
            m_fSpitBallCurColdDownTime = m_fSpitBallTimeInterval = m_MainPlayer.GetHenzyWColdDown();
        }

        float fMaxForceTime = skill_param.aryValues[2];
        float fShootDistance = skill_param.aryValues[3];
        float fShootRunTime = skill_param.aryValues[4];
        float fShellTime = skill_param.aryValues[5];

        if (!bCanUseSkill)
        {
            return;
        }

        //m_fSpitBallCurColdDownTime = m_fSpitBallTimeInterval;

        m_bForceSpit = true;
        m_fForceSpitPercent = 0.0f;

        m_MainPlayer.RefreshLiveBalls();
        //ball.ClearAvailableChildSize ();

        m_fForceSpitCurTime = 0.0f;

        m_MainPlayer.BeginForceSpit(fMpCost, fMaxForceTime, fShootDistance, fShootRunTime, fShellTime);
    }

    public void ClearSkillColdDown_SpitBall()
    {
        m_fSpitBallCurColdDownTime = 0f;
        m_imgSpitBall_Fill.fillAmount = 1;
    }

    public void ClearSkillColdDown_SpitBallHalf()
    {
        m_fSpitBallHalfTimeCount = 0f;
        m_imgLababa_Fill.fillAmount = 1;
    }

    void DoSpit()
    {

    }

    // 处于“视野保护状态”那一瞬间，镜头只能拉远不能拉近
    float m_fCurCamSize = 0f;
    float m_fCamSizeProtectTime = 0;
    public void BeginCameraSizeProtect(float fCamSizeProtectTime)
    {
        return;

        m_fCurCamSize = Camera.main.orthographicSize;
        m_fCamSizeProtectTime = fCamSizeProtectTime;
    }

    void CameraSizeProtectLoop()
    {
        if (m_fCamSizeProtectTime <= 0f) {
            return;
        }
        m_fCamSizeProtectTime -= Time.deltaTime;
    }

    public bool IsCameraSizeProtecting()
    {
        return m_fCamSizeProtectTime > 0f;
    }

    public void CancelSpit()
    {
        m_bForceSpit = false;
    }

    public void EndSpit()
    {
        Main.s_Instance.m_MainPlayer.ClearCosmosGunSight();

        if (!m_bForceSpit)
        {
            return;
        }

        int nAvailableNum = m_MainPlayer.GetCurAvailableBallCount();
        if (nAvailableNum <= 0) {
            CancelSpit();
            return;
        }

        m_bForceSpit = false;


        if (m_MainPlayer) {
            m_MainPlayer.DoSpit(m_nPlatform); 
        }

        /*

            foreach (Transform child in m_goMainPlayer.transform.transform) {
                Ball ball = child.gameObject.GetComponent<Ball> ();

                if (ball.IsEjecting ()) {
                    continue;
                }

                float fMotherSize = ball.GetSize ();
                float fMotherLeftSize = 0.0f;
                float fChildSize = ball.GetAvailableChildSize( ref fMotherLeftSize );
                if (fChildSize < BALL_MIN_SIZE || fMotherLeftSize < BALL_MIN_SIZE || fMotherLeftSize < fChildSize ) {
                    continue;
                }

                // 如果母球身上有刺，则按比例被子球带走
                //float fMotherThornSize = ball.GetThornSize ();
                //float fChildThornSize = ;
                //SetThornSize ( Mathf.Sqrt( fCurThornSize * fCurThornSize + fAddedThornSize * fAddedThornSize ) );
                //ball.SetThornSize ( ball.GetThornSize() * fMotherLeftSize / fMotherSize );

                ball.SetSize ( fMotherLeftSize );
                Main.s_Instance.s_Params[0] = eInstantiateType.Ball;
                Main.s_Instance.s_Params[1] = 222;
                Ball new_ball = Ball.NewOneBall( ball.photonView.ownerId );//PhotonInstantiate (m_preBall, Vector3.zero, Main.s_Instance.s_Params);
                //Ball new_ball = goNewBall.GetComponent<Ball> ();
                new_ball.SetSize ( fChildSize );
                new_ball.transform.parent = m_goMainPlayer.transform;
                ball.CalculateNewBallBornPosAndRunDire ( new_ball, ball._dirx, ball._diry );
                ball.RelateTarget ( new_ball );
                ball.BeginShell ();
                new_ball.SetThornSize ( ball.GetThornSize() * fChildSize / fMotherSize );
                new_ball.BeginShell ();
                new_ball.BeginEject ( ball.m_fSpitBallInitSpeed, ball.m_fSpitBallAccelerate );


            } // end foreach

            */




    }

    public int GetMainPlayerCurBallCount()
    {
        if (m_MainPlayer == null) {
            return 0;
        }

        return m_MainPlayer.GetCurMainPlayerBallCount();//m_goMainPlayer.transform.childCount;
    }

    float m_fForceSpitCurTime = 0.0f;

    public bool IsForceSpitting()
    {
        return m_bForceSpit;
    }

    // 在这里计算瞄准器的形态
    void ForceSpitting()
    {
        return;
        /*
                int nAvailableNum = m_MainPlayer.GetCurAvailableBallCount ();
                if (nAvailableNum <= 0) {
                    return;
                }

                if (!m_bForceSpit) {
                    return;
                }

                m_fForceSpitCurTime += Time.deltaTime;  
                m_fForceSpitPercent = m_fForceSpitCurTime / m_fForceSpitTotalTime; 
                if (m_fForceSpitPercent > 0.5f) {
                    m_fForceSpitPercent = 0.5f;
                }

                m_MainPlayer.Spitting ( m_fForceSpitPercent );
        */
    }

    // 变刺
    float m_fCurColdDownTime_BecomeThor_Count = 0.0f;
    float m_fCurColdDownTime_BecomeThorn = 0.0f;
    public void BecomeThorn()
    {
        if (m_fCurColdDownTime_BecomeThor_Count > 0)
        {
            return;
        }

        //// 先获取该技能当前的技能点等级对应的参数
        bool bCanUseSkill = true;
        CSkillSystem.sSkillParam skill_param = CSkillSystem.s_Instance.GetSkillParamById(CSkillSystem.eSkillId.t_become_thorn, ref bCanUseSkill);
        float fMpCost = skill_param.aryValues[0];                                 // 蓝耗
        float fColdDown = skill_param.aryValues[1];                             // cold down时间
        float fQianYao = skill_param.aryValues[2];                                // 前摇时间 
        float fSpeedChangePercent = skill_param.aryValues[3];            // 速度改变系数    
        float fDuration = skill_param.aryValues[4];                               // 持续时间
        string szExplodeConfigId = skill_param.szValue;                      // 炸球参数配置

        if (!bCanUseSkill)
        {
            return;
        }

        bool bRet = m_MainPlayer.CastSkill_BecomeThorn(fMpCost, fQianYao, fSpeedChangePercent, fDuration, szExplodeConfigId);
        if (!bRet)
        {
            return;
        }

        // 技能执行成功了再开始启动ColdDown，失败了不启动ColdDown
        m_fCurColdDownTime_BecomeThorn = fQianYao + fDuration + fColdDown;
        m_fCurColdDownTime_BecomeThor_Count = m_fCurColdDownTime_BecomeThorn;
    }

    // 猥琐地扩张
    float m_fUnfoldCurColdDownTime = 0.0f;
    public void Unfold()
    {


        /*
        if ( !m_MainPlayer.CheckIfCanUnfold())
        {
            return;
        }
        */

        if (m_fUnfoldCurColdDownTime > 0.0f)
        {
            return;
        }

        //// 先获取该技能当前的技能点等级对应的参数
        bool bCanUseSkill = true;
        CSkillSystem.sSkillParam skill_param = CSkillSystem.s_Instance.GetSkillParamById(CSkillSystem.eSkillId.e_unfold, ref bCanUseSkill);
        float fMpCost = skill_param.aryValues[0];
        m_fUnfoldTimeInterval = skill_param.aryValues[1];               // cold down时间

        if (m_MainPlayer.IsHenzy())
        {
            m_fUnfoldTimeInterval = m_MainPlayer.GetHenzyEColdDown();
        }

        m_fMainTriggerPreUnfoldTime = skill_param.aryValues[2];   // 前摇时间 
        float fUnfoldScale = skill_param.aryValues[3];                       // 膨胀系数    
        m_fMainTriggerUnfoldTime = skill_param.aryValues[4];        // 持续时间


        if (!bCanUseSkill)
        {
            return;
        }
        //// end s先获取该技能当前的技能点等级对应的参数

        bool bRet = m_MainPlayer.BeginPreUnfold(fMpCost, m_fMainTriggerPreUnfoldTime, fUnfoldScale, m_fMainTriggerUnfoldTime);
        if (!bRet)
        {
            return;
        }

        m_fUnfoldCurColdDownTime += (m_fMainTriggerPreUnfoldTime + m_fMainTriggerUnfoldTime + m_fUnfoldTimeInterval);
    }

    float m_fSpitSporeCurColdDownTime = 0.0f;
    float m_fSpitNailCurColdDownTime = 0.0f; // 吐钉子的ColdDown时间


    // 吐孢子
    /*
	public void SpitSpore()
	{
		if (m_fSpitSporeCurColdDownTime > 0.0f) { // ColdDown还没结束
			return;
		}

		m_fSpitSporeCurColdDownTime = m_fSpitSporeTimeInterval;

		foreach (Transform child in m_goMainPlayer.transform) {
			GameObject go = child.gameObject;
			Ball ball = go.GetComponent<Ball> ();
			if (ball == null) {
				continue;
			}	

			//Ball ballSpore =  ball.SpitSpore();
			ball.SpitBean();
		} // end foreach		
	}
	*/


    /*
	// 自动衰减
	float m_fAttenuateTimeCount = 0.0f;
	public void AutoAttenuate()
	{
		return;

        foreach (Transform child in m_goMainPlayer.transform)
        {
            GameObject go = child.gameObject;
            Ball ball = go.GetComponent<Ball>();
            if (ball == null)
            {
                continue;
            }
            ball.AutoAttenuateThorn();
         }

        m_fAttenuateTimeCount += Time.deltaTime;
		if (m_fAttenuateTimeCount < Main.s_Instance.m_fAutoAttenuateTimeInterval) {
			return;
		}
		m_fAttenuateTimeCount = 0.0f;
		foreach (Transform child in m_goMainPlayer.transform) {
			GameObject go = child.gameObject;
			Ball ball = go.GetComponent<Ball> ();
			if (ball == null) {
				continue;
			}
			ball.AutoAttenuate ( Main.s_Instance.m_fAutoAttenuate );
		}// end foreach		
	}
	*/


    public void Test_AddBallsSize()
    {
        if (m_nCheatMode == 0) {
            return;
        }

        m_MainPlayer.Test_AddAllBallSize(1000);
        /*
		foreach (Transform child in m_goMainPlayer.transform) {
			GameObject go = child.gameObject;
			Ball ball = go.GetComponent<Ball> ();
			if (ball == null) {
				continue;
			}	

			if (ball.IsDead ()) {
				continue;
			}

			ball.SetSize ( ball.GetSize() + 1.0f );

		}// end foreach	
		*/
    }

    List<Ball> lstTemp = new List<Ball>();
    public void Test_KingExplode()
    {
        _goCheat.SetActive(false);
        m_MainPlayer.OneBtnExplode(m_szCheatExplodeThornId);
    }

    bool m_bTestFucking = false;
    public void Test_Fuck()
    {
        m_bTestFucking = !m_bTestFucking;

        if (m_bTestFucking)
        {
            m_goJueShengQiu.SetActive(false);
        }
    }

    int m_nFuckCount = 0;
    float m_fFuckTime = 0f;
    void Test_Fuck_Loop()
    {
        if (!m_bTestFucking)
        {
            return;
        }

        m_fFuckTime += Time.deltaTime;
        if (m_fFuckTime < 3f)
        {
            return;
        }
        m_fFuckTime = 0f;

        if (m_nFuckCount == 0)
        {
            Test_KingExplode();
            m_nFuckCount = 1;
        }
        else
        {
            MergeAll();
            m_nFuckCount = 0;
        }
    }

    public void Test_AddThornSize()
    {
        foreach (Transform child in m_goMainPlayer.transform)
        {
            GameObject go = child.gameObject;
            Ball ball = go.GetComponent<Ball>();
            if (ball == null)
            {
                continue;
            }

            ball.SetThornSize(ball.GetThornSize() + 0.1f);

        }// end foreach	
    }

    public void Test_AddThornOfBallSize()
    {
        foreach (Transform child in m_goMainPlayer.transform) {
            GameObject go = child.gameObject;
            Ball ball = go.GetComponent<Ball>();
            if (ball == null) {
                continue;
            }

            if (ball.GetSize() < (2 * Main.BALL_MIN_SIZE)) {
                continue;
            }

            ball.SetThornSize(ball.GetThornSize() + Main.s_Instance.m_fThornSize);

            // 吃刺也会增加球本身的总体积
            ball.SetSize(ball.GetSize() + Main.s_Instance.m_fThornContributeToBallSize);


        }// end foreach	
    }

    //异步加载场景  
    IEnumerator LoadScene(string scene_name)
    {
        AccountData.s_Instance.asyncLoad = SceneManager.LoadSceneAsync(scene_name);

        while (!AccountData.s_Instance.asyncLoad.isDone)
        {
            yield return null;
        }


    }



    public void ExitGame()
    {
        // AccountManager.m_eSceneMode = AccountManager.eSceneMode.None;
        PhotonNetwork.Disconnect();

   //     StartCoroutine("LoadScene", "SelectCaster");

    }

    public void OnDisconnectedFromServer(NetworkDisconnection info)
    {
        Debug.Log("11111111111");
    }

    public void OnDisconnectedFromMasterServer(NetworkDisconnection info)
    {
        Debug.Log("222222222222");
    }

    public void MergeAll()
    {
        if (m_nCheatMode == 0) {
            return;
        }

        Main.s_Instance.BeginCameraSizeProtect(0.5f);
        m_MainPlayer.MergeAll();
    }

    public void Suicide()
    {
        if (m_nCheatMode == 0) {
            return;
        }
     
        m_MainPlayer.Suicide();
    
    }

    /*
        public void SyncMoveInfo( Vector3 vecWorldCursorPos )
        {
            if (m_MainPlayer) {
                m_MainPlayer.SyncMoveInfo ( vecWorldCursorPos );
            }
        }
    */
    List<Player> m_lstPlayers = new List<Player>();
	public void AddOnePlayer( Player player )
	{
		m_lstPlayers.Add ( player );
	}

	public void RemoveOnePlayer( Player player )
	{

    }

    /// <summary>
    /// 技能：金壳
    /// </summary>
    float m_fSkillColdDownCount_Gold = 0;
    float m_fSkillColdDown_Gold = 0;
    public void CastSkill_Gold()
    {
        //// 获取该技能当前的参数（依据该技能当前累积的技能点）
        bool bCanUseSkill = true;
        CSkillSystem.sSkillParam skill_param = CSkillSystem.s_Instance.GetSkillParamById(CSkillSystem.eSkillId.p_gold, ref bCanUseSkill);
        float fMpCost = skill_param.aryValues[0];                                // 蓝耗
        m_fSkillColdDown_Gold = skill_param.aryValues[1];                       // cold down
        float fDuration = skill_param.aryValues[2];                              // 持续时间
        float fSpeedAddPercent = skill_param.aryValues[3];                              // 速度加成

        if (!bCanUseSkill)
        {
            return;
        }
        bool bRet = m_MainPlayer.CastSkill_Gold(fMpCost, fDuration, fSpeedAddPercent);
        if (!bRet)
        {
            return;
        }

        m_fSkillColdDownCount_Gold = m_fSkillColdDown_Gold;
    }

    /// end 技能：金壳


    /// <summary>
    /// 技能：狂暴
    /// </summary>
    float m_fSkillColdDownCount_Henzy = 0;
    float m_fSkillColdDown_Henzy = 0;
    public void  CastSkill_Henzy()
    {
        //// 获取该技能当前的参数（依据该技能当前累积的技能点）
        bool bCanUseSkill = true;
        CSkillSystem.sSkillParam skill_param = CSkillSystem.s_Instance.GetSkillParamById(CSkillSystem.eSkillId.o_fenzy, ref bCanUseSkill);
        float fMpCost = skill_param.aryValues[0];                                // 蓝耗
        m_fSkillColdDown_Henzy = skill_param.aryValues[1];              // cold down
        float fSpeedChangePercent = skill_param.aryValues[2];            // 速度影响百分比
        float fWSkillColdDown = skill_param.aryValues[3];                   // W技能的ColdDown变为多少
        float fESkillColdDown = skill_param.aryValues[4];                   // E技能的ColdDown变为多少
        float fDuration = skill_param.aryValues[5];                               // 持续时间

        if (!bCanUseSkill)
        {
            return;
        }
        bool bRet = m_MainPlayer.CastSkill_Henzy(fMpCost, fDuration, fSpeedChangePercent, fWSkillColdDown, fESkillColdDown);
        if (!bRet)
        {
            return;
        }

        m_fSkillColdDownCount_Henzy = m_fSkillColdDown_Henzy;
    }

    /// end 技能：狂暴


    //// 技能：秒合
    float m_fSkillColdDownCount_MergeAll = 0;
    float m_fSkillColdDown_MergeAll = 0;
    public void CastSkill_MergeAll()
    {
        //// 获取该技能当前的参数（依据该技能当前累积的技能点）
        bool bCanUseSkill = true;
        CSkillSystem.sSkillParam skill_param = CSkillSystem.s_Instance.GetSkillParamById(CSkillSystem.eSkillId.i_merge, ref bCanUseSkill);
        float fMpCost = skill_param.aryValues[0];                                // 蓝耗
        m_fSkillColdDown_MergeAll = skill_param.aryValues[1];          // cold down
        float fQianYao = skill_param.aryValues[2];                              // 前摇时间

        if (!bCanUseSkill)
        {
            return;
        }
        bool bRet = m_MainPlayer.CastSkill_MergeAll(fMpCost, fQianYao);
        if (!bRet)
        {
            return;
        }

        m_fSkillColdDownCount_MergeAll = m_fSkillColdDown_MergeAll;
    }




/// <summary>
/// / 技能：魔盾
/// </summary>
    float m_fSkillColdDownCount_MagicShield = 0;
    float m_fSkillColdDown_MagicShield = 0;
     public void CastSkill_MagicShield()
    {
        //// 获取该技能当前的参数（依据该技能当前累积的技能点）
        bool bCanUseSkill = true;
        CSkillSystem.sSkillParam skill_param = CSkillSystem.s_Instance.GetSkillParamById(CSkillSystem.eSkillId.u_magicshield, ref bCanUseSkill);
        float fMpCost = skill_param.aryValues[0];                                // 蓝耗
        m_fSkillColdDown_MagicShield = skill_param.aryValues[1];        // cold down
        float fDuration = skill_param.aryValues[2];                              // 持续时间
        float fSpeedAffect = skill_param.aryValues[3];                              // 速度加成

        if (!bCanUseSkill)
        {
            return;
        }
        bool bRet = m_MainPlayer.CastSkill_MagicShield(fMpCost, fDuration, fSpeedAffect);
        if (!bRet)
        {
            return;
        }

        m_fSkillColdDownCount_MagicShield = m_fSkillColdDown_MagicShield;
    }

    //// 技能：湮灭
    float m_fSkillColdDown_Annihilate = 0f;
    float m_fSkillColdDownCount_Annihilate = 0f;
    public void CastSkill_Annihilate()
    {
        //// 获取该技能当前的参数（依据该技能当前累积的技能点）
        bool bCanUseSkill = true;
        CSkillSystem.sSkillParam skill_param = CSkillSystem.s_Instance.GetSkillParamById(CSkillSystem.eSkillId.y_annihilate, ref bCanUseSkill);
        float fMpCost = skill_param.aryValues[0];                        // 蓝耗
        m_fSkillColdDown_Annihilate = skill_param.aryValues[1];        // cold down
        float fAnnihilatePercent = skill_param.aryValues[2];         //  Percent
        float fDuration = skill_param.aryValues[3];                      // 持续时间
        float fSpeedAffect = skill_param.aryValues[4];                      // 速度加成

        if (!bCanUseSkill)
        {
            return;
        }
        bool bRet = m_MainPlayer.CastSkill_Annihilate(fMpCost, fAnnihilatePercent, fDuration, fSpeedAffect);
        if (!bRet)
        {
            return;
        }

        m_fSkillColdDownCount_Annihilate = m_fSkillColdDown_Annihilate;
    }


    /// <summary>
    /// / 技能：潜行
    /// </summary>
    float m_fSkillColdDownCount_Sneak = 0f;
    float m_fSkillColdDownCount = 0f;
    public void CastSkill_Sneak()
    {
        //// 获取该技能当前的参数（依据该技能当前累积的技能点）
        bool bCanUseSkill = true;
        CSkillSystem.sSkillParam skill_param = CSkillSystem.s_Instance.GetSkillParamById(CSkillSystem.eSkillId.r_sneak, ref bCanUseSkill);
        float fMpCost = skill_param.aryValues[0];            // 蓝耗
        m_fSkillColdDownCount = skill_param.aryValues[1];        // cold down
        float fSpeedPercent = skill_param.aryValues[2];          // 速度影响百分比
        float fDuration = skill_param.aryValues[3];          // 持续时间

        if (!bCanUseSkill)
        {
            return;
        }
        bool bRet = m_MainPlayer.CastSkill_Sneak( fMpCost, fSpeedPercent, fDuration );
        if (!bRet)
        {
            return;
        }
        
        m_fSkillColdDownCount_Sneak = m_fSkillColdDownCount;
    }


	// 一键分球
	public void OneBtnSplitBall()
	{
        /*
        //// 获取该技能当前的参数（依据该技能当前累积的技能点）
        bool bCanUseSkill = true;
        CSkillSystem.sSkillParam skill_param = CSkillSystem.s_Instance.GetSkillParamById(CSkillSystem.eSkillId.r_sneak, ref bCanUseSkill);
        float fMpCost = skill_param.aryValues[0];            // 蓝耗
        m_fSplitTimeInterval = skill_param.aryValues[1]; // cold down
        m_fSplitTimeCount = m_fSplitTimeInterval;
        float fDistance = skill_param.aryValues[2];          // 运行距离
        float fSplitBallNum = skill_param.aryValues[3];    // 分球数
        float fRunTime = skill_param.aryValues[4];          // 运行时间
        float fShellTime = skill_param.aryValues[5];          // 壳时间

        if (!bCanUseSkill)
        {
            return;
        }
        
        BeginCameraSizeProtect (fRunTime);
		if (m_nPlatform == 0) { // 这个流程只针对PC版。 手机版走另外的流程			
			if ( !m_MainPlayer.OneBtnSplitBall ( m_nPlatform, _playeraction.GetWorldCursorPos (), fMpCost, fDistance, fSplitBallNum, fRunTime, fShellTime) )
            {
                CancelSplit();
            }
		} else {
			m_MainPlayer.OneBtnSplitBall ( m_nPlatform, m_fCurOneBtnSplitDis );
		}
        */
	}

    public void CancelSplit()
    {
        m_imgOneBtnSplit_Fill.fillAmount = 1;
        m_fSplitTimeCount = 0;
    }

    // 二分分球
    float m_fSpitBallHalfTimeCount = 0f;
	public void	SpitBallHalf (){
        /*
        int nAvailableNum = m_MainPlayer.GetCurAvailableBallCount();
        if (nAvailableNum <= 0)
        {
            Main.s_Instance.g_SystemMsg.SetContent("队伍总球数已满，不能再生成新球");
            return;
        }

        //// 获取该技能当前的参数（依据该技能当前累积的技能点）
        bool bCanUseSkill = true;
        CSkillSystem.sSkillParam skill_param = CSkillSystem.s_Instance.GetSkillParamById(CSkillSystem.eSkillId.t_half_spit, ref bCanUseSkill);
        float fMpCost = skill_param.aryValues[0];
        m_fSpitBallHalfColddown = skill_param.aryValues[1]; // cold down
        m_fSpitBallHalfTimeCount = m_fSpitBallHalfColddown;
        float fQianYao = skill_param.aryValues[2];
        float fDistance = skill_param.aryValues[3];
        float fRunTime = skill_param.aryValues[4];
        float fShellTime = skill_param.aryValues[5];

        if (!bCanUseSkill)
        {
            return;
        }

        m_MainPlayer.PreCastSkill_SpitBallHalf(fMpCost, m_fSpitBallHalfColddown, fQianYao, fDistance, fRunTime, fShellTime);
        //	m_MainPlayer.SpitBallHalf ();
        */
    }

    public void CancelSpitBallHalf()
    {
       
    }

    // One Btn Split
    bool m_bPreOneBtnSplit = false;
	float m_fPreOneBtnSplitTimeCount = 0f;
	float m_fCurOneBtnSplitDis = 0f;
	int m_nSplitTouchFingerId = 0;
	Vector3 m_vecSplitTouchStartPos = new Vector3();
	Vector3 m_vecCurSplitTouchPos = new Vector3();
	Vector2 m_vecSplitDirction = new Vector2();

	public void BeginRSplit()
	{
		if (m_fSplitTimeCount > 0) {
			return; // ColdDown还没结束
		}
		m_fPreOneBtnSplitTimeCount = 0;
		m_bPreOneBtnSplit = true;
	}

	public void BeginPreOneBtnSplit( int nFingerId, Vector3 vecStartPos )
	{
		m_bPreOneBtnSplit = true;
		m_fPreOneBtnSplitTimeCount = 0f;
		m_nSplitTouchFingerId = nFingerId;
		m_vecSplitTouchStartPos = vecStartPos;
		m_vecCurSplitTouchPos = vecStartPos;
		m_fSplitTimeCount = 0f;
	}

	public void EndPreOneBtnSplit()
	{
		m_MainPlayer.BeginClearSpitTargetCount ( m_fSplitRunTime );
		m_bPreOneBtnSplit = false;
		OneBtnSplitBall ();
	}

	

	public Vector3 GetSplitDirction()
	{
		return m_vecSplitDirction;
	}

	
	

	public Sprite GetSpriteByPhotonOwnerId( int nOwnerId )
	{
		int nSpriteId = nOwnerId;
		if (nSpriteId < 0 )
		{
			nSpriteId = 0;
		}
		if (nSpriteId >= ResourceManager.s_Instance.m_aryBallSprites.Length) {
			nSpriteId = nSpriteId % ResourceManager.s_Instance.m_aryBallSprites.Length;
		}
		return ResourceManager.s_Instance.m_aryBallSprites[nSpriteId];
	}

	public int GetSpriteIdByPhotonOwnerId( int nOwnerId )
	{
		int nSpriteId = nOwnerId;
		if (nSpriteId < 0 )
		{
			nSpriteId = 0;
		}
		if (nSpriteId >= ResourceManager.s_Instance.m_aryBallSprites.Length) {
			nSpriteId = nSpriteId % ResourceManager.s_Instance.m_aryBallSprites.Length;
		}
		return nSpriteId;
	}

	public Sprite GetSpriteBySpriteId( int nSpriteId )
	{
		if (nSpriteId < 0 || nSpriteId >= ResourceManager.s_Instance.m_aryBallSprites.Length )
		{
			nSpriteId = 0;
		}
		return ResourceManager.s_Instance.m_aryBallSprites[nSpriteId];
	}

	public int GetSpriteArrayLength()
	{
		return ResourceManager.s_Instance.m_aryBallSprites.Length;
	}

	int m_nInitingBallsCount = 0;
	float m_fInitingBallsTime = 0f;
	/*
	void InitingBalls()
	{
		if (!m_bInitingAllBalls) {
			return;
		}

		if (m_MainPlayer == null || (!m_MainPlayer.photonView.isMine)) {
			return;
		}

		vecTempPos.x = -10000f;
		vecTempPos.y = -10000f;
		GameObject goBall = PhotonInstantiate(m_preBall, vecTempPos, Main.s_Instance.s_Params) ;
		goBall.transform.parent = m_MainPlayer.transform;
		goBall.transform.position = vecTempPos;
		Ball ball = goBall.GetComponent<Ball> ();
		m_MainPlayer.AddOneBallToRecycled ( ball );
		ball.SyncFullInfo ();
		if ( m_MainPlayer.GetCurTotalBallCountIncludeRecycled() >= m_fMaxBallNumPerPlayer) {
			EndInitingBalls ();
		}
	}
	*/
	void EndInitingBalls()
	{
		m_MainPlayer.EndInitAllBalls ();
		//m_MainPlayer.RecycleAllBalls ();
		m_bInitingAllBalls = false;
	}

	Vector3 m_vecStartPos = new Vector3();
	int m_nFingerId = 0;
	bool m_bSpitTouching = false;
	Vector3 m_vecCurPos = new Vector3();
	Vector2 m_vecSpitDirction = new Vector2();
	public void BeginSpitTouch( int nfingerId, Vector3 vecStartPos )
	{
		m_nFingerId = nfingerId;
		m_vecStartPos = vecStartPos;
		m_vecCurPos = vecStartPos;
		m_bSpitTouching = true;
	}

    bool m_bSpitTouching_Test = false;
    public void BeginSpitTouch_Test(Vector3 vecStartPos)
    {
        m_bSpitTouching_Test = true;
        m_vecStartPos = vecStartPos;
        m_vecCurPos = vecStartPos;
    }

	public void EndSpitTouch()
	{
		m_bSpitTouching = false;
		m_MainPlayer.BeginClearSpitTargetCount ( m_fSpitRunTime );
	}

    void SpitTouching_Test()
    {
        if ( !m_bSpitTouching_Test)
        {
            return;
        }

        m_vecCurPos.x = Input.mousePosition.x;
        m_vecCurPos.y = Input.mousePosition.y;
        m_vecSpitDirction = m_vecCurPos - m_vecStartPos;
        m_vecSpitDirction.Normalize();
        m_MainPlayer.SetSpecialDirection(m_vecSpitDirction);
    }

    public void EndSpitTouch_Test()
    {
        m_bSpitTouching_Test = false;
       
    }

    void SpitTouching()
	{
		if (!m_bSpitTouching) {
			return;
		}

		if (m_nFingerId != 0 && Input.touchCount < 2 ) {
			m_nFingerId = 0;
		}  

		Touch touch = Input.GetTouch ( m_nFingerId );
		m_vecCurPos.x += touch.deltaPosition.x;
		m_vecCurPos.y += touch.deltaPosition.y;
		m_vecSpitDirction = m_vecCurPos - m_vecStartPos;
		m_vecSpitDirction.Normalize ();
		_playeraction.m_vec2MoveInfo = m_vecSpitDirction;
        m_MainPlayer.SetSpecialDirection(_playeraction.m_vec2MoveInfo);
	}

	public int GetSpitFingerID()
	{
		return m_nFingerId;
	}

	public Vector2 GetSpitDirection()
	{
		return m_vecSpitDirction;
	}

    public void SetSpitDirection( Vector2 dir )
    {
        m_vecSpitDirction = dir;
    }

    public void SetSpitDirection( float dirx, float diry )
	{
		m_vecSpitDirction.x = dirx;
		m_vecSpitDirction.y = diry;
	}

	public void OnClickBtn_Reborn()
	{
        m_MainPlayer.DoReborn ();
		_uiDead.SetActive ( false );
		m_nDeadStatus = 0;
		_playeraction.Reset ();
		m_MainPlayer.Reset ();
        PlayerAction.s_Instance.StopAndMoveToCenter();
    }

	public void OnClickBtn_ReturnToHomePage()
	{
		PhotonNetwork.Disconnect ();
		SceneManager.LoadScene ( "SelectCaster" );
	}

    public void UseSkill_LaBaba( )
    {
        m_MainPlayer.UseSkill_Lababa();
    }

    float m_fLeftTime = 0f;
    public void SetLeftTime( float val )
    {
        m_fLeftTime = val;
    }

    public float GetLeftTime()
    {
        return m_fLeftTime;
    }

    float m_fGameStartTime = 0f;
    public void SetGameStartTime( float val )
    {
        m_fGameStartTime = val;

        CStarDust.s_Instance.MarkGameStartTimeSet();
    }

    public float GetGameStartTime()
    {
        return  m_fGameStartTime;
    }

    public void DoSomeInitAfterConfigLoad()
    {
        Main.BALL_MIN_SIZE = CyberTreeMath.Volume2Scale( Main.m_fBallMinVolume );

        if ( PhotonNetwork.isMasterClient )
        {
            SetGameStartTime( (float)PhotonNetwork.time );
            Main.s_Instance._txtTimeLeft.gameObject.SetActive(true);
        }

		// 刚出身是1级，加载1级的相关数据
		if (m_MainPlayer) {
            m_MainPlayer.SetLevel (1);
		}

        CItemSystem.s_Instance.InitItemSystem();
        PlayerAction.LoadSpitSkillParams();
    }

    float m_fProcessLeftTimeCount =1f;
    public void ProcessTimeLeft()
    {
        if ( !IsGameStarted() )
        {
            return;
        }

        // MapEditor配置加载好之后才能启动计时
        if ( !MapEditor.s_Instance.IsMapInitCompleted() )
        {
            return;
        }

        if (m_bGameOver)
        {
            return;
        }

        // 必须要MasterClient的全量同步过来之后，再开始计时（poppin do to 稍候改为更稳妥的同步模式，而不能单靠MasterClient）
        if (m_fGameStartTime == 0f)   // 如果m_fGameStartTime等于0，则说明初登时的全量同步失败了
        {
          //  m_MainPlayer.PleaseSyncThePublicInfo();
            return;
        }

        m_fProcessLeftTimeCount += Time.deltaTime;
        if (m_fProcessLeftTimeCount < 1f )
        {
            return;
        }
        m_fProcessLeftTimeCount = 0f;

        float fTimeEclapse = (float)PhotonNetwork.time - m_fGameStartTime;
      //  Debug.Log("fTimeEclapse = " + fTimeEclapse + " ,  " + CClassEditor.s_Instance.GetTimeOfOneGame());
		m_fLeftTime = CClassEditor.s_Instance.GetTimeOfOneGame() - fTimeEclapse;
        if (m_fLeftTime <= 0f)
        {
            _txtTimeLeft.text = "00:00";
            GameOver(); 
        }
        int nMinute = (int)( m_fLeftTime / 60f );
        int nSecond = (int)(m_fLeftTime - 60f * nMinute);
        string szMinute = nMinute >= 10 ? nMinute.ToString() : ( "0" + nMinute );
        string szSec = nSecond >= 10 ? nSecond.ToString() : ("0" + nSecond);
        _txtTimeLeft.text = szMinute + ":" + szSec;
    }

   bool m_bGameOver = false;
    public bool IsGameOver()
    {
        return m_bGameOver;
    }

   public  void GameOver( string szShengFuPlayer = "" )
    {
        // CPlayerManager.s_Instance.SetShengFuPanDingPlayer( szShengFuPlayer );

        //CJieSuanManager.s_Instance.Show();
        //_goGameOver.SetActive( true );

        CGameOverManager.s_Instance.BeginGameOver();

        m_bGameOver = true;

        
    }

    float m_fGameOverPanelTimeLapse = 0f;

    public void OnButtonClick_ShowSkillPointUI()
    {
        _panelSkillPoint.SetActive( true );
    }

    public static float GetTime()
    {
        return (float)PhotonNetwork.time;
    }

    public static bool IsMasterClient()
    {
        return PhotonNetwork.isMasterClient;
    }

    public void OnButtonClick_ChangeSkin()
    {
        m_MainPlayer.ChangeSkin();
    }


    public void OnButtonClick_Suicide()
    {
        Suicide();
        _goCheat.SetActive(false );
    }

    float m_fScreenWidth = 0f;
    float m_fScreenHeight = 0f;
    public bool IsInCamView( Vector3 pos )
    {
        vecTempPos = Camera.main.WorldToScreenPoint( pos );
        if (vecTempPos.x > -50 && vecTempPos.x < m_fScreenWidth && vecTempPos.y > -50 && vecTempPos.y < m_fScreenHeight) 
        {
            return true;
        }

        return false;
    }

    public bool IsBallInCamView( float fWorldLeft, float fCenterX, float fCenterY )
    {
        // left
        vecTempPos.x = fWorldLeft;
        vecTempPos.y = fCenterX;
        vecTempPos.z = 0f;
        vecTempPos = Camera.main.WorldToScreenPoint(vecTempPos);

        //center
        vecTempPos1.x = fCenterX;
        vecTempPos1.y = fCenterY;
        vecTempPos1.z = 0f;
        vecTempPos1 = Camera.main.WorldToScreenPoint(vecTempPos1);

        // radius_screen
        float fScreenRadius = vecTempPos1.x - vecTempPos.x;

        float fLeft = vecTempPos.x;
        if (fLeft > Screen.width + 20)
        {
            return false;
        }

        float fRight = vecTempPos1.x + fScreenRadius;
        if (fRight < -20)
        {
            return false;
        }

        float fTop = vecTempPos1.y + fScreenRadius; ;
        if (fTop < -20)
        {
            return false;
        }

        float fBottom = vecTempPos1.y - fScreenRadius;
        if( fBottom > Screen.height + 20 )
        {
            return false;
        }

        return true;
    }

    public enum ePlatformType
    {
        pc,
        mobile,
    };

    public ePlatformType GetPlatformType()
    {
        return (ePlatformType)m_nPlatform;
    }

    public void OnClick_LeaveGame()
    {
        ExitGame();
    }
    

    public void Test_AddPoint()
    {
        CSkillSystem.s_Instance.SetTotalPoint(CSkillSystem.s_Instance.GetTotalPoint() + 1);
    }

    public void Test_AddMoney()
    {
        CMoneySystem.s_Instance.SetMoney(CMoneySystem.s_Instance.GetMoney() + 100);
    }

    public void Test_AddExp()
    {
        CGrowSystem.s_Instance.AddExp(100);
    }


    public void Test_AddMP()
    {
        Main.s_Instance.m_MainPlayer.TestRPC();    
    }

    public InputField _inputCheatPanelExplodeThornId;
    public string m_szCheatExplodeThornId = "0_1";
    public void OnInputValueChanged_CheatPanelExplodeThornId()
    {
        m_szCheatExplodeThornId = _inputCheatPanelExplodeThornId.text;
    }


   
} // end Main
