﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CExplodeNode : MonoBehaviour
{

    public Player _Player;

    bool m_bExploding = false;
    bool m_bWaiting = false;
    bool m_bAutoMoveAfterExplodeCompleted = false;

    public Ball ballMother;
    public CMonsterEditor.sThornConfig explodeConfig;
    public bool m_bMotherMoving = true;
    public int nChildNum;
    public int nConfigChildNum;
    public float fExpectAngle = 0;
    public float fChildVolume;
    public float fMotherLeftVolume;
    public List<int> lstChildBallIndex = new List<int>();
    public Vector3 m_vecMotherPos = new Vector3();
    List<Vector2> m_lstDir = new List<Vector2>();
    public float m_fSpeed = 0f;
    public float m_fRunDistance = 0f;

    int m_nShiZi_BallNum_PerDir_Moving = 0;
    int m_nShiZi_BallNum_PerDir_NotMoving = 0;
    
    public Vector2 m_vecMotherDir = new Vector2();
    float m_fMotherDirAngle = 0f;

    /// <summary>
    ///  十字队形相关
    /// </summary>
    float m_fShiZi_ZuoCe_Angle = 0f;
    float m_fShiZi_YouCe_Angle = 0f;
    float m_fShiZi_HouFang_Angle = 0f;
    float m_fSegDis_Moving = 0f;
    
    Vector2 m_vecSHiZi_ZuoCe_Dir = new Vector2();
    Vector2 m_vecSHiZi_YouCe_Dir = new Vector2();
    Vector2 m_vecSHiZi_HouFang_Dir = new Vector2();

    int m_nShiZiStatus = 0;
    int m_nShiZiStatusNumCount = 0;
    int m_nZuoNumCount = 0;
    int m_nYouNumCount = 0;
    const int SHIZI_STATUS_QIAN = 0;
    const int SHIZI_STATUS_ZUOYOU = 1;
    const int SHIZI_STATUS_HOU = 2;

    // end 十字队形相关


    /// <summary>
    /// / 三角队形相关
    /// </summary>
    Vector2 m_vecSanJiao_ZuoCe_Dir = new Vector2();
    Vector2 m_vecSanJiao_YouCe_Dir = new Vector2();
    int m_nSanJiaoStatus = 0;
    int m_nSanJiaoStatusNumCount = 0;
    int m_nSanJiaoZuoNumCount = 0;
    int m_nSanJiaoYouNumCount = 0;
    const int SANJIAO_STATUS_QIAN = 0;
    const int SANJIAO_STATUS_ZUOYOU = 1;

    int m_nSanJiao_BallNum_PerDir_Moving = 0;
    float m_fSanJiao_SegDis_Moving = 0f;
    // end 三角队形相关

    int m_nCurChildNum = 0;
    int m_nExplodeRound = 0;
    float m_fSegDis = 0f;

    public const int EXPLODE_CHILD_NUM_PER_FRAME = 64;

    static Vector2 vec2Temp = new Vector2();
    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempDir = new Vector3();

    List<Vector3> m_lstTempEjectEndPos = new List<Vector3>();

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
    }

    public void SetPlayer(Player player)
    {
        _Player = player;
    }

    public void BeginExplode()
    {
        if (ballMother.IsExploding())
        {
            Debug.LogError("ballMother.IsExploding()");
            return;
        }

        if (ballMother.IsDead())
        {
            Debug.LogError("ballMother.IsDead()");
            return;
        }

        if (nChildNum == 0)
        {
            return;
        }

        // 正在喷射，被炸球了就中断喷射
        if (ballMother.IsEjecting())
        {
            ballMother.EndEject();
        }

        if (ballMother._Player.IsMainPlayer())
        {
            CAudioManager.s_Instance.PlayAudio(CAudioManager.eAudioId.e_audio_explode);
        }

        //m_bMotherMoving = ballMother._ba.GetRealTimeVelocity() > 0f;
        //m_vecMotherDir = ballMother.GetDir();
        //m_vecMotherPos = ballMother.GetPos();
        m_fMotherDirAngle = CyberTreeMath.Dir2Angle(m_vecMotherDir.x, m_vecMotherDir.y);

        // 改成最新的距离公式了
        m_fRunDistance = Ball.GetRunDistance(explodeConfig.fExplodeRunDistance, ballMother.GetRadius()) ;// explodeConfig.fExplodeRunDistance * ballMother.GetRadius();


        m_lstDir = MapEditor.s_Instance.GetExplodeDirList(nChildNum);
        m_fSpeed = explodeConfig.fExplodeRunTime;
        m_nCurChildNum = 0;
        m_nExplodeRound = 0;
        fExpectAngle = 360f / (float)nConfigChildNum;

        // [to youhua]这里大量的运算都只需要MainPlayer里做，然后直接同步给OtherClient. OtherClient完全没必要原封不动的再运算一遍
        // shizi
        if (explodeConfig.fExplodeFormationId == 1)
        {
            m_fShiZi_HouFang_Angle = m_fMotherDirAngle + 180f;
            m_nShiZiStatus = SHIZI_STATUS_QIAN;
            m_nShiZiStatusNumCount = 0;
            m_nZuoNumCount = 0;
            m_nYouNumCount = 0;

            m_fShiZi_ZuoCe_Angle = m_fMotherDirAngle + 90f;
            m_fShiZi_YouCe_Angle = m_fMotherDirAngle - 90f;
            m_vecSHiZi_ZuoCe_Dir.x = Mathf.Cos(CyberTreeMath.Angle2Radian(m_fShiZi_ZuoCe_Angle));
            m_vecSHiZi_ZuoCe_Dir.y = Mathf.Sin(CyberTreeMath.Angle2Radian(m_fShiZi_ZuoCe_Angle));
            m_vecSHiZi_YouCe_Dir.x = Mathf.Cos(CyberTreeMath.Angle2Radian(m_fShiZi_YouCe_Angle));
            m_vecSHiZi_YouCe_Dir.y = Mathf.Sin(CyberTreeMath.Angle2Radian(m_fShiZi_YouCe_Angle));
            m_vecSHiZi_HouFang_Dir.x = Mathf.Cos(CyberTreeMath.Angle2Radian(m_fShiZi_HouFang_Angle));
            m_vecSHiZi_HouFang_Dir.y = Mathf.Sin(CyberTreeMath.Angle2Radian(m_fShiZi_HouFang_Angle));
        }
        // end shizi


        // sanjiao
        if (explodeConfig.fExplodeFormationId == 2)
        {
            float fSanJiaoZuoCeAngle = m_fMotherDirAngle + 120f;
            float fSanJiaoYouCeAngle = m_fMotherDirAngle - 120f;
            m_vecSanJiao_ZuoCe_Dir.x = Mathf.Cos(CyberTreeMath.Angle2Radian(fSanJiaoZuoCeAngle));
            m_vecSanJiao_ZuoCe_Dir.y = Mathf.Sin(CyberTreeMath.Angle2Radian(fSanJiaoZuoCeAngle));
            m_vecSanJiao_YouCe_Dir.x = Mathf.Cos(CyberTreeMath.Angle2Radian(fSanJiaoYouCeAngle));
            m_vecSanJiao_YouCe_Dir.y = Mathf.Sin(CyberTreeMath.Angle2Radian(fSanJiaoYouCeAngle));

            m_nSanJiaoStatus = SANJIAO_STATUS_QIAN;
            m_nSanJiaoStatusNumCount = 0;
            m_nSanJiaoZuoNumCount = 0;
            m_nSanJiaoYouNumCount = 0;

            m_nSanJiao_BallNum_PerDir_Moving = /*nConfigChildNum*/nChildNum / 3;
            if (nChildNum % 3 != 0)
            {
                m_nSanJiao_BallNum_PerDir_Moving++;
            }

            m_fSanJiao_SegDis_Moving = m_fRunDistance / (float)m_nSanJiao_BallNum_PerDir_Moving;
        }
        // end sanjiao

        m_nShiZi_BallNum_PerDir_Moving = /*nConfigChildNum*/nChildNum / 4;
        if (nChildNum % 4 != 0 )
        {
            m_nShiZi_BallNum_PerDir_Moving++;
        }
        m_fSegDis_Moving = m_fRunDistance / (float)m_nShiZi_BallNum_PerDir_Moving;




        m_nShiZi_BallNum_PerDir_NotMoving = nChildNum / 4;
        if (nChildNum % 4 != 0)
        {
            m_nShiZi_BallNum_PerDir_NotMoving++;
        }
        m_fSegDis = m_fRunDistance / (float)m_nShiZi_BallNum_PerDir_NotMoving;

        //// 血迹

        
        // 喷射血迹
        vecTempPos = ballMother.GetPos();
        vecTempPos.x += ballMother.GetRadius() * ballMother.GetDir().x;
        vecTempPos.y += ballMother.GetRadius() * ballMother.GetDir().y;
        vecTempDir = ballMother.GetDir();
        float fScale = ballMother.GetSize() / CBloodStainManager.s_Instance.GetScaleRatioByType(CBloodStainManager.eBloodStainType.explode);
        CBloodStainManager.s_Instance.AddBloodStain(vecTempPos, vecTempDir, fScale, fScale, CBloodStainManager.eBloodStainType.explode, ballMother._Player.GetColor(),  CBloodStainManager.eDynamicEffectType.scale, 0.2f);
        //// end 血迹

        if ( !Main.s_Instance.m_MainPlayer.IsMoving() )
        {
            m_bAutoMoveAfterExplodeCompleted = true;
        }
        else
        {
            m_bAutoMoveAfterExplodeCompleted = false;
        }
        m_bExploding = true;
        m_bWaiting = false;


        if (fMotherLeftVolume <= 0f)
        {
         
        }
        else
        {
            // ballMother.Local_SetSize(fMotherLeftSize);
            ballMother.SetVolume( fMotherLeftVolume );
            ballMother.Local_SetShellInfo(_Player.GetRealShellTime(explodeConfig.fExplodeShellTime), 0f, Ball.eShellType.explode_ball);
            ballMother.Local_BeginShell();
        }

    }

    public bool IsExploding()
    {
        return m_bExploding;
    }

    float m_fWaitingTimeElapse = 0f;
    public bool DoWaiting()
    {
        if (!IsWaiting())
        {
            return false;
        }
        m_fWaitingTimeElapse -= Time.deltaTime;
        if (m_fWaitingTimeElapse <= 0f)
        {
            if ( _Player.IsMainPlayer() )
            {
                PlayerAction.s_Instance.StopAndMoveToCenter();
            }
            return true;
        }

        return false;
    }

    public bool IsWaiting()
    {
        return m_bWaiting;
    }

    // [to youhua] OtherClient不需要做炸球的各种运算，只需要MainPlayer计算好之后直接同步过去生成就好了
    public bool DoExplode()
    {
        for (int i = 0; i < EXPLODE_CHILD_NUM_PER_FRAME; i++)
        {
            int j = i + m_nExplodeRound * EXPLODE_CHILD_NUM_PER_FRAME;
            if (j >= lstChildBallIndex.Count) // 没有合适的球可以激活用来生产新球
            {
                EndExplode();
                return true;
            }


            int nChildBallIndex = lstChildBallIndex[j];
            int nDirIndex = m_nCurChildNum;
            if (nDirIndex >= m_lstDir.Count)
            {
                nDirIndex = 0;
            }
            Vector2 vecDir = m_lstDir[nDirIndex];

            Ball new_ball = null;


            switch ( (int)explodeConfig.fExplodeFormationId ) // 队形
            {
                case 0: // 圆形
                    {
                        if (nChildNum == 0) // 一个都炸不出来了
                        {
                            
                        }
                        else // 能炸出（从1个到配置的最大值不等）
                        {
                            if (m_bMotherMoving) // 母球运动中
                            {
                                new_ball = ExplodeChildBall_Moving_0(nChildBallIndex, explodeConfig, m_vecMotherPos.x, m_vecMotherPos.y, m_fSpeed, fChildVolume, m_fRunDistance);
                            }
                            else // 母球静止中
                            {
                                new_ball = ExplodeChildBall_0(nChildBallIndex, explodeConfig, m_vecMotherPos.x, m_vecMotherPos.y, m_fSpeed, fChildVolume, m_fRunDistance, vecDir);
                            }
                        }
                    }
                break;

                case 1: // 十字刺
                    {
                        if (nChildNum == 0) // 一个都炸不出来了
                        {

                        }
                        else // 能炸出（从1个到配置的最大值不等）
                        {
                            if (m_bMotherMoving)
                            {
                              
                                new_ball = ExplodeChildBall_Moving_1(nChildBallIndex, m_nCurChildNum, explodeConfig, m_vecMotherPos.x, m_vecMotherPos.y, m_fSpeed, fChildVolume, m_fRunDistance, m_fSegDis);
                            }
                            else
                            {
                                new_ball = ExplodeChildBall_1(nChildBallIndex, m_nCurChildNum, explodeConfig, m_vecMotherPos.x, m_vecMotherPos.y, m_fSpeed, fChildVolume, m_fRunDistance, m_fSegDis);
                            }
                        }
                    }
                    break;

                case 2: // 三角刺
                    {
                        if (nChildNum == 0) // 一个都炸不出来了
                        {

                        }
                        else // 能炸出（从1个到配置的最大值不等）
                        {
                            if (m_bMotherMoving)
                            {

                                new_ball = ExplodeChildBall_Moving_2(nChildBallIndex, m_nCurChildNum, explodeConfig, m_vecMotherPos.x, m_vecMotherPos.y, m_fSpeed, fChildVolume, m_fRunDistance, m_fSegDis);
                            }
                            else
                            {
                                new_ball = ExplodeChildBall_2(nChildBallIndex, m_nCurChildNum, explodeConfig, m_vecMotherPos.x, m_vecMotherPos.y, m_fSpeed, fChildVolume, m_fRunDistance, m_fSegDis);
                            }
                        }
                    }
                    break;
            }

            /*
            else
            { 
                if (explodeConfig.fExplodeFormationId == 0)
                {
                    new_ball = ExplodeChildBall_0(nChildBallIndex, explodeConfig, m_vecMotherPos.x, m_vecMotherPos.y, m_fSpeed, fChildVolume, m_fRunDistance, vecDir);
                }
                else if (explodeConfig.fExplodeFormationId == 1)
                {
                    new_ball = ExplodeChildBall_1(nChildBallIndex, m_nCurChildNum, explodeConfig, m_vecMotherPos.x, m_vecMotherPos.y, m_fSpeed, fChildVolume, m_fRunDistance, m_fSegDis);
                }

                else if (explodeConfig.fExplodeFormationId == 2)
                {
                    new_ball = ExplodeChildBall_2(nChildBallIndex, m_nCurChildNum, explodeConfig, m_vecMotherPos.x, m_vecMotherPos.y, m_fSpeed, fChildVolume, m_fRunDistance, m_fSegDis);
                }
            }
            */
            new_ball.SetSizeDirectly();

            //// 相机抬升相关
            /*
            Vector3 vecDest = new Vector3();
            vecDest.x = new_ball.GetPos().x + new_ball.GetEjectTotalDis() * new_ball.GetDir().x;
            vecDest.y = new_ball.GetPos().y + new_ball.GetEjectTotalDis() * new_ball.GetDir().y;
            vecDest.z = new_ball.GetRadius() * CtrlMode.m_fAcctackRadiusMultiple;
            m_lstTempEjectEndPos.Add(vecDest);
            */
            //// end 相机抬升相关

            m_nCurChildNum++;
            if (m_nCurChildNum >= nChildNum)
            {
                EndExplode();
                if ( m_bAutoMoveAfterExplodeCompleted )
                {
                    m_bWaiting = true;
                    m_fWaitingTimeElapse = 1f;
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        m_nExplodeRound++;


        return false;
    }

    void EndExplode()
    {
        m_bExploding = false;

        // 母球剩余
        if (fMotherLeftVolume <= 0f)
        {
            // _Player.DestroyBall(ballMother.GetIndex());
            ballMother.SetDead( true );
        }


        if (_Player.IsMainPlayer())
        {
            /*
            bool bNeedCamSizeProtect = CtrlMode.CheckCameraSizeProtectInfo(ref m_lstTempEjectEndPos); // 把刚才记录的“预测终点”值传入，计算(预测)镜头的抬升情况。
            if (bNeedCamSizeProtect)
            {
                float fXiShu = CtrlMode.GetChangeAmount()  / CtrlMode.CHANGE_AMOUNT_XIHSU;
                float fRaiseSpeed = Main.s_Instance.m_fRaiseSpeed * fXiShu;
                float fDownSpeed = Main.s_Instance.m_fDownSpeed * fXiShu;
                CtrlMode.BeginCameraSizeProtect(Main.s_Instance.m_fCamChangeDelayExplode, fRaiseSpeed, fDownSpeed);
            }
            */
        }

		Debug.Log ( "==========" );
    }

    // 圆形布阵且实际炸球数不足配置炸球数
    Ball ExplodeChildBall_Moving_0(int nChildBallIndex, CMonsterEditor.sThornConfig config, float fExplodePosX, float fExplodePosY, float fSpeed, float fChildVolume, float fRunDistance)
    {


        float fMyAngle = 0;
        int nIndex = m_nCurChildNum;
            if (m_nCurChildNum == 0) // 第一个球
            {
                fMyAngle = m_fMotherDirAngle;
            }
            else
            {
                int nDeShu = nIndex / 2;
                int nYuShu = nIndex % 2;
                if (nYuShu == 0)
                {
                    fMyAngle = m_fMotherDirAngle + fExpectAngle * nDeShu;
                }
                else
                {
                    nDeShu += 1;
                    fMyAngle = m_fMotherDirAngle - fExpectAngle * nDeShu;
                }
            }


        vecTempDir.x = Mathf.Cos( CyberTreeMath.Angle2Radian( fMyAngle ) );
        vecTempDir.y = Mathf.Sin( CyberTreeMath.Angle2Radian( fMyAngle ) );
        
        Ball new_ball = _Player.GetOneBallToReuse(nChildBallIndex);
        new_ball.SetDir(vecTempDir);
        new_ball.SetShellEnable(false);
        new_ball.Local_SetPos(m_vecMotherPos);
        new_ball.SetVolume(fChildVolume);

        

        new_ball.Local_SetShellInfo(_Player.GetRealShellTime(explodeConfig.fExplodeShellTime), 0f, Ball.eShellType.explode_ball);
        new_ball.BeginEject_BySpeed(m_fRunDistance, m_fSpeed, explodeConfig.fExplodeStayTime, true);
        //new_ball.BeginEject(fRunDistance, fSpeed, vecTempDir, Ball.eEjectMode.accelerated, config.fExplodeStayTime, true);
        new_ball.SetActive(true);

        return new_ball;
    }

    // 圆形布阵
    Ball ExplodeChildBall_0(int nChildBallIndex, CMonsterEditor.sThornConfig config, float fExplodePosX, float fExplodePosY, float fSpeed, float fChildVolume, float fRunDistance, Vector2 vecDire)
    {
        Ball new_ball = _Player.GetOneBallToReuse(nChildBallIndex); // 这种逻辑会不会有隐患？在网络丢包的情况下。 稍后再探讨，暂时这样

        vecTempPos = m_vecMotherPos;
        vecTempPos.x = fExplodePosX;
        vecTempPos.y = fExplodePosY;
        new_ball.SetShellEnable(false);


        new_ball.Local_SetPos(vecTempPos);

        
        new_ball.SetVolume( fChildVolume );
        new_ball.SetDir(vecDire);
        new_ball.Local_SetShellInfo(_Player.GetRealShellTime(config.fExplodeShellTime), 0f, Ball.eShellType.explode_ball);

        new_ball.BeginEject_BySpeed(fRunDistance, fSpeed, config.fExplodeStayTime, true);
        //new_ball.BeginEject(fRunDistance, fSpeed, vecDire, Ball.eEjectMode.accelerated, config.fExplodeStayTime, true);
        
        new_ball.SetActive(true);
        return new_ball;
    }

    // 十字布阵(母球运动状态)
    Ball ExplodeChildBall_Moving_1(int nChildBallIndex, int idx, CMonsterEditor.sThornConfig config, float fExplodePosX, float fExplodePosY, float fSpeed, float fChildSize, float fRunDistance, float fSegDistance)
    {
        Ball new_ball = null;

        switch ( m_nShiZiStatus )
        {
            case SHIZI_STATUS_QIAN:
                {
                    float fRealDis = (m_nShiZi_BallNum_PerDir_Moving - m_nShiZiStatusNumCount) * m_fSegDis_Moving;
                   
                    new_ball = _Player.GetOneBallToReuse(nChildBallIndex);

                    vecTempPos.x = fExplodePosX;
                    vecTempPos.y = fExplodePosY;
                    new_ball.SetShellEnable(false);

                    new_ball.Local_SetPos(vecTempPos);
                                        
                    new_ball.SetVolume(fChildVolume);
                    
                    new_ball.SetDir(m_vecMotherDir);

                    new_ball.Local_SetShellInfo(_Player.GetRealShellTime(config.fExplodeShellTime), 0f, Ball.eShellType.explode_ball);

                    new_ball.BeginEject_BySpeed(fRealDis, fSpeed, config.fExplodeStayTime, true);
                   // new_ball.BeginEject(fRealDis, fSpeed, new_ball.GetDir(), Ball.eEjectMode.accelerated, config.fExplodeStayTime, true);
                    new_ball.SetActive(true);

                    m_nShiZiStatusNumCount++;
                    if (m_nShiZiStatusNumCount >= m_nShiZi_BallNum_PerDir_Moving)
                    {
                        m_nShiZiStatus = SHIZI_STATUS_ZUOYOU;
                        m_nShiZiStatusNumCount = 0;
                    }
                }
                break;
            case SHIZI_STATUS_ZUOYOU:
                {
                    float fRealDis = 0;

                    new_ball = _Player.GetOneBallToReuse(nChildBallIndex);

                    vecTempPos.x = fExplodePosX;
                    vecTempPos.y = fExplodePosY;
                    new_ball.SetShellEnable(false);

                    new_ball.Local_SetPos(vecTempPos);

                    new_ball.SetVolume(fChildVolume);

                    if (m_nShiZiStatusNumCount % 2 == 0)
                    {
                        new_ball.SetDir( m_vecSHiZi_ZuoCe_Dir );
                        fRealDis = (m_nShiZi_BallNum_PerDir_Moving - m_nZuoNumCount) * m_fSegDis_Moving;
                        m_nZuoNumCount++;
                    }
                    else
                    {
                        fRealDis = (m_nShiZi_BallNum_PerDir_Moving - m_nYouNumCount) * m_fSegDis_Moving;
                        m_nYouNumCount++;
                        new_ball.SetDir( m_vecSHiZi_YouCe_Dir);
                    }

                    new_ball.Local_SetShellInfo(_Player.GetRealShellTime(config.fExplodeShellTime), 0f, Ball.eShellType.explode_ball);

                    new_ball.BeginEject_BySpeed(fRealDis, fSpeed, config.fExplodeStayTime, true);
                    //new_ball.BeginEject(fRealDis, fSpeed, new_ball.GetDir(), Ball.eEjectMode.accelerated, config.fExplodeStayTime, true);
                    new_ball.SetActive(true);

                    m_nShiZiStatusNumCount++;
                    if (m_nShiZiStatusNumCount >= ( m_nShiZi_BallNum_PerDir_Moving * 2 ))
                    {
                        m_nShiZiStatus = SHIZI_STATUS_HOU;
                        m_nShiZiStatusNumCount = 0;
                    }
                }
                break;
            case SHIZI_STATUS_HOU:
                {
                    float fRealDis = (m_nShiZi_BallNum_PerDir_Moving - m_nShiZiStatusNumCount) * m_fSegDis_Moving;

                    new_ball = _Player.GetOneBallToReuse(nChildBallIndex);

                    vecTempPos.x = fExplodePosX;
                    vecTempPos.y = fExplodePosY;
                    new_ball.SetShellEnable(false);

                    new_ball.Local_SetPos(vecTempPos);

                    new_ball.SetVolume(fChildVolume);

                    new_ball.SetDir(m_vecSHiZi_HouFang_Dir);

                    new_ball.Local_SetShellInfo(_Player.GetRealShellTime(config.fExplodeShellTime), 0f, Ball.eShellType.explode_ball);

                    new_ball.BeginEject_BySpeed(fRealDis, fSpeed, config.fExplodeStayTime, true);
                    //new_ball.BeginEject(fRealDis, fSpeed, new_ball.GetDir(), Ball.eEjectMode.accelerated, config.fExplodeStayTime, true);
                    new_ball.SetActive(true);
                    m_nShiZiStatusNumCount++;


                }
                break;


        }

        return new_ball;
    }

    // 十字布阵(母球静止状态)
    Ball ExplodeChildBall_1(int nChildBallIndex, int idx, CMonsterEditor.sThornConfig config, float fExplodePosX, float fExplodePosY, float fSpeed, float fChildSize, float fRunDistance, float fSegDistance)
    {
        int nDeShu = idx / 4;
        int nYuShu = idx % 4;
        if (nYuShu == 0)
        {
            vec2Temp.x = 0f;
            vec2Temp.y = 1f;
        }
        else if (nYuShu == 1)
        {
            vec2Temp.x = 0f;
            vec2Temp.y = -1f;
        }
        else if (nYuShu == 2)
        {
            vec2Temp.x = -1f;
            vec2Temp.y = 0f;
        }
        else if (nYuShu == 3)
        {
            vec2Temp.x = 1f;
            vec2Temp.y = 0f;
        }
        float fRealDis = (m_nShiZi_BallNum_PerDir_NotMoving - nDeShu) * fSegDistance;// (nDeShu + 1) * fSegDistance;
        if (fRealDis < 0)
        {
            fRealDis = 0;
        }
        Ball new_ball = _Player.GetOneBallToReuse(nChildBallIndex);

        vecTempPos.x = fExplodePosX;
        vecTempPos.y = fExplodePosY;
        new_ball.SetShellEnable(false);

        new_ball.Local_SetPos(vecTempPos);

        //new_ball.Local_SetSize(fChildSize);
        new_ball.SetVolume(fChildVolume);
        new_ball.SetDir(vec2Temp);
        new_ball.Local_SetShellInfo(_Player.GetRealShellTime(config.fExplodeShellTime), 0f, Ball.eShellType.explode_ball);

        new_ball.BeginEject_BySpeed(fRealDis, fSpeed, config.fExplodeStayTime, true);
        //new_ball.BeginEject(fRealDis, fSpeed, new_ball.GetDir(), Ball.eEjectMode.accelerated, config.fExplodeStayTime, true);
        new_ball.SetActive(true);
        return new_ball;
    }

    // 三角布阵(母球静止状态)
    Ball ExplodeChildBall_2(int nChildBallIndex, int idx, CMonsterEditor.sThornConfig config, float fExplodePosX, float fExplodePosY, float fSpeed, float fChildSize, float fRunDistance, float fSegDistance)
    {
        Ball new_ball = _Player.GetOneBallToReuse(nChildBallIndex);

        int nDeShu = idx / 3;
        int nYuShu = idx % 3;
        if (nYuShu == 0)
        {
            vec2Temp.x = 0f;
            vec2Temp.y = 1f;
        }
        else if (nYuShu == 1)
        {
            vec2Temp.x = 0.866f;
            vec2Temp.y = -0.5f;
        }
        else if (nYuShu == 2)
        {
            vec2Temp.x = -0.866f;
            vec2Temp.y = -0.5f;
        }

        float fRealDis = (nDeShu + 1) * fSegDistance;
        if (fRealDis < 0)
        {
            fRealDis = 0;
        }

        vecTempPos.x = fExplodePosX;
        vecTempPos.y = fExplodePosY;
        new_ball.SetShellEnable(false);



        new_ball.Local_SetPos(vecTempPos);

        //new_ball.Local_SetSize(fChildSize);
        new_ball.SetVolume(fChildVolume);
        new_ball.SetDir(vec2Temp);
        new_ball.Local_SetShellInfo(_Player.GetRealShellTime(config.fExplodeShellTime), 0f, Ball.eShellType.explode_ball);




        new_ball.BeginEject_BySpeed(fRealDis, fSpeed, config.fExplodeStayTime, true);
        //new_ball.BeginEject(fRealDis, fSpeed, new_ball.GetDir(), Ball.eEjectMode.accelerated, config.fExplodeStayTime, true);
        new_ball.SetActive(true);
        return new_ball;
    }


    // 三角布阵(母球运动状态)
    Ball ExplodeChildBall_Moving_2(int nChildBallIndex, int idx, CMonsterEditor.sThornConfig config, float fExplodePosX, float fExplodePosY, float fSpeed, float fChildSize, float fRunDistance, float fSegDistance)
    {
        Ball new_ball = null;

        switch (m_nSanJiaoStatus)
        {
            case SANJIAO_STATUS_QIAN:
                {
                    float fRealDis = (m_nSanJiao_BallNum_PerDir_Moving - m_nSanJiaoStatusNumCount) * m_fSanJiao_SegDis_Moving;

                    new_ball = _Player.GetOneBallToReuse(nChildBallIndex);

                    vecTempPos.x = fExplodePosX;
                    vecTempPos.y = fExplodePosY;
                    new_ball.SetShellEnable(false);

                    new_ball.Local_SetPos(vecTempPos);

                    new_ball.SetVolume(fChildVolume);

                    new_ball.SetDir(m_vecMotherDir);

                    new_ball.Local_SetShellInfo(_Player.GetRealShellTime(config.fExplodeShellTime), 0f, Ball.eShellType.explode_ball);

                    new_ball.BeginEject_BySpeed(fRealDis, fSpeed, config.fExplodeStayTime, true);
                    //new_ball.BeginEject(fRealDis, fSpeed, new_ball.GetDir(), Ball.eEjectMode.accelerated, config.fExplodeStayTime, true);
                    new_ball.SetActive(true);

                    m_nSanJiaoStatusNumCount++;
                    if (m_nSanJiaoStatusNumCount >= m_nSanJiao_BallNum_PerDir_Moving)
                    {
                        m_nSanJiaoStatus = SANJIAO_STATUS_ZUOYOU;
                        m_nSanJiaoStatusNumCount = 0;
                    }
                }
                break;
            case SANJIAO_STATUS_ZUOYOU:
                {
                    float fRealDis = 0;

                    new_ball = _Player.GetOneBallToReuse(nChildBallIndex);

                    vecTempPos.x = fExplodePosX;
                    vecTempPos.y = fExplodePosY;
                    new_ball.SetShellEnable(false);

                    new_ball.Local_SetPos(vecTempPos);

                    new_ball.SetVolume(fChildVolume);

                    if (m_nSanJiaoStatusNumCount % 2 == 0)
                    {
                        fRealDis = (m_nSanJiao_BallNum_PerDir_Moving - m_nSanJiaoZuoNumCount) * m_fSanJiao_SegDis_Moving;
                        m_nSanJiaoZuoNumCount++;
                        new_ball.SetDir(m_vecSanJiao_ZuoCe_Dir);
                    }
                    else
                    {
                        fRealDis = (m_nSanJiao_BallNum_PerDir_Moving - m_nSanJiaoYouNumCount) * m_fSanJiao_SegDis_Moving;
                        m_nSanJiaoYouNumCount++;
                        new_ball.SetDir(m_vecSanJiao_YouCe_Dir);
                    }

                    new_ball.Local_SetShellInfo(_Player.GetRealShellTime(config.fExplodeShellTime), 0f, Ball.eShellType.explode_ball);

                    new_ball.BeginEject_BySpeed(fRealDis, fSpeed, config.fExplodeStayTime, true);
                    //new_ball.BeginEject(fRealDis, fSpeed, new_ball.GetDir(), Ball.eEjectMode.accelerated, config.fExplodeStayTime, true);
                    new_ball.SetActive(true);

                    m_nSanJiaoStatusNumCount++;
                }
                break;
           


        }

        return new_ball;
    }
}