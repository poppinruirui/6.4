﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CGMSystem : MonoBehaviour {

	public static CGMSystem s_Instance;

	void Awake()
	{
		s_Instance = this;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ProcessInstruction( string szContent )
	{
		string[] ary = szContent.Split ( ' ' );
		switch (ary [0]) {
		case "buff":
		case "BUFF":
			{
				Main.s_Instance.m_MainPlayer.AddBuff ( int.Parse( ary [1] ) );
			}
			break;
		case "MP":
		case "mp":
			{
				Main.s_Instance.m_MainPlayer.AddMP ( int.Parse( ary [1] )  );
			}
			break;
		case "exp":
		case "EXP":
			{
				Main.s_Instance.m_MainPlayer.AddExp ( int.Parse( ary [1] )  );
			}
			break;

        case "money":
        case "MONEY":
            {
                    Main.s_Instance.m_MainPlayer.ChangeMoney(int.Parse(ary[1]));
                }
            break;


            case "point": // 技能点
            case "POINT":
                {
                    CSkillSystem.s_Instance.SetTotalPoint(CSkillSystem.s_Instance.GetTotalPoint() + int.Parse(ary[1]));
                }
                break;

        }

        if (szContent == "show me the money" || szContent == "SHOW ME THE MONEY")
        {
            Main.s_Instance.m_MainPlayer.AddMoney( 100000 );
        }

    }
}
