﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIItem : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
 {
    public static UIItem s_Instance;

    public CanvasGroup _canvasGroup;

    public Button _btnAddPoint;
    public Text _txtNum;
    public Text _txtName;
    public Text _txtDescValue;
    public Text _txtDesc;
    public Text _txtPrice;
    public Text _txtNextLevelDesc;
    public Image _imgMain;
    public GameObject _tips;
    public Image _imgLevel;
    public Button _btnQiPao;
    public Button _btnMain;

    int m_nShortcuteKey = 0;

    CItemSystem.sItemConfig m_Config;

    public CFrameAnimationEffect _effectCanLevelUp;

    void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
    //    _txtNum.text = "(LV." + 1 + ")";
    }
	
	// Update is called once per frame
	void Update () {

        SearchAvailablePos();

        TaleFadeLoop();
    }

    int m_nCurLevel = 0;
    public void SetItemInfo( CItemSystem.sItemConfig config, int nCurLevel, int nLevel)
    {
        m_nCurLevel = nCurLevel;
        m_Config = config;
        _txtName.text = "· " + config.szName;
        _txtPrice.text = config.dicValueByLevel[nLevel].nPrice.ToString();
        switch (config.nFunc)
        {
            case (int)CItemSystem.eItemFunc.shell_time:
                {
                    _txtDesc.text = config.szDesc;
                    _txtDescValue.text =  config.dicValueByLevel[nLevel].fValue * 100 + "%";
                }
                break;

            case (int)CItemSystem.eItemFunc.base_mp:
                {
                    _txtDesc.text = config.szDesc;
                    _txtDescValue.text = "+" +config.dicValueByLevel[nLevel].fValue.ToString();
                }
                break;

            case (int)CItemSystem.eItemFunc.base_size:
                {
                    _txtDesc.text = config.szDesc;
                    _txtDescValue.text = "+" + config.dicValueByLevel[nLevel].fValue.ToString();
                }
                break;
            case (int)CItemSystem.eItemFunc.base_speed:
                {
                    _txtDesc.text = config.szDesc;
                    _txtDescValue.text = "+" + config.dicValueByLevel[nLevel].fValue.ToString();

                }
                break;
        }

    }

 
    public void UpdateNum( int nNum )
    {
        //_txtNum.text = nNum.ToString();
        //_txtNum.text = "(LV." + nNum + ")";
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        //_tips.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)

    {
        //_tips.SetActive(false);
    }

    public void OnClickButton_Add()
    {
        CItemSystem.s_Instance.ExecuteItem(m_nShortcuteKey - 1);
    }

    public void SetAddPointButtonVisible( bool bVisible )
    {
        //_btnAddPoint.gameObject.SetActive(bVisible);
        _tips.gameObject.SetActive(bVisible);

        if (bVisible)
        {
            _effectCanLevelUp.gameObject.SetActive( true );
            _effectCanLevelUp.BeginPlay(true );
        }
        else
        {
            _effectCanLevelUp.gameObject.SetActive(false);
        }
    }

    bool m_bVisible = false;
    public const int COUNTER_POS_NUM = 2;
    int m_nCurPosIndex = 2;
    static Vector2 vec2TempPos = new Vector2();
    public void SetVisible( bool bVisible )
    {
        m_bVisible = bVisible;

        BeginTailFade();

        if ( !bVisible )
        {
            if (m_nCurPosIndex >= 0 && m_nCurPosIndex < COUNTER_POS_NUM)
            {
                CItemSystem.s_Instance.ReleasePos(m_nCurPosIndex);
            }
            m_nCurPosIndex = CItemSystem.s_Instance.m_aryCounterPos.Length;
        }
    }

    int m_nTaleFadeStatus = 0;// 0 - none  1 - 计时中  2 - 渐隐中
    float m_fTaleFadeAlpha = 1f;
    float m_fTaleFadeTimeElapse = 0f;
    void BeginTailFade()
    {
        m_fTaleFadeAlpha = 1f;
        m_nTaleFadeStatus = 1;
        m_fTaleFadeAlpha = 1f;
        _canvasGroup.alpha = m_fTaleFadeAlpha;
        m_fTaleFadeTimeElapse = 0f;
        _btnQiPao.enabled = true;
    }

    void EndTaleFade()
    {
        m_nTaleFadeStatus = 0;

        _btnQiPao.enabled = false;
    }

    void TaleFadeLoop()
    {
        if (m_nTaleFadeStatus == 0)
        {
            return;
        }

        m_fTaleFadeTimeElapse += Time.deltaTime;
        if (m_nTaleFadeStatus == 1)
        {
            if (m_fTaleFadeTimeElapse >= 2 )
            {
                m_nTaleFadeStatus = 2;
            }
        }

        if (m_nTaleFadeStatus == 2)
        {
            m_fTaleFadeAlpha -= Time.deltaTime;
            _canvasGroup.alpha = m_fTaleFadeAlpha;
            if (m_fTaleFadeAlpha <= 0)
            {
                EndTaleFade();
            }
        }
    }

    void SearchAvailablePos()
    {
        if ( !m_bVisible)
        {
            return;
        }

        int nRetIndex = CItemSystem.s_Instance.SearchAvailablePos(m_nCurPosIndex);
        if (nRetIndex == -1)
        {
            return;
        }
        vec2TempPos = CItemSystem.s_Instance.GetCounterPos(nRetIndex);
        if (m_nCurPosIndex >= 0 && m_nCurPosIndex < COUNTER_POS_NUM)
        {
            CItemSystem.s_Instance.ReleasePos(m_nCurPosIndex);
        }
        m_nCurPosIndex = nRetIndex;
        CItemSystem.s_Instance.TakePos(nRetIndex);
        vec2TempPos = CItemSystem.s_Instance.GetCounterPos(nRetIndex);
        this.transform.localPosition = vec2TempPos;
    }


    public bool GetVisible()
    {
        return m_bVisible;
    }


    public void OnClickButton_UseItem()
    {
        if ( m_Config.dicValueByLevel == null )
        {
            return;
        }

        CItemSystem.s_Instance.ExecuteItem( m_Config.nConfigId );
    }

    public void RefreshLevel(int nCurLevel)
    {
        float fFillAmount = (float)nCurLevel / (float)CItemSystem.ITEM_MAX_LEVEL;
        if (_imgLevel)
        {
            _imgLevel.fillAmount = fFillAmount;
        }
    }
}
