﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CSkillModuleForBall : MonoBehaviour {

    public Ball _ball;

    public GameObject _goEffectContainer;
    CCosmosEffect[] m_aryActiveEffect_QianYao = new CCosmosEffect[(int)CSkillSystem.eSkillId.total_num];
    CCosmosEffect[] m_aryActiveEffect_ChiXu = new CCosmosEffect[(int)CSkillSystem.eSkillId.total_num];

    //short[] m_arySkillStatus = new short[(int)CSkillSystem.eSkillId.total_num];
    short m_nSkillStatus = 0;
    CSkillSystem.eSkillId m_eSkillId = CSkillSystem.eSkillId.r_sneak;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void GetCurEffects(ref CCosmosEffect[] aryQianYao, ref CCosmosEffect[] aryChiXu)
    {
        aryQianYao = m_aryActiveEffect_QianYao;
        aryChiXu = m_aryActiveEffect_ChiXu;
    }

    public void ResetAllSkillStatus()
    {
        /*
        for ( int i = 0; i < m_arySkillStatus.Length; i++ )
        {
            m_arySkillStatus[i] = 0;
        }
        */
        m_nSkillStatus = 0;
    }

    public void SetCurSkillStatus(/*CSkillSystem.eSkillId eSkillId, */short nStatus )
    {
        //m_arySkillStatus[(int)eSkillId] = nStatus;
        m_nSkillStatus = nStatus;
    }

    
    public short GetCurSkillStatus(/*CSkillSystem.eSkillId eSkillId*/)
    {
        // return m_arySkillStatus[(int)eSkillId];
        return m_nSkillStatus;
    }

    public void UpdateSkillStatus(CSkillSystem.eSkillId eSkillId, short nSkillStatus)
    {
        m_eSkillId = eSkillId;

       if (m_nSkillStatus == nSkillStatus )
       {
            return;
       }

        if (m_nSkillStatus == 1 && nSkillStatus != 1 ) // 清除前摇特效
        {
            CCosmosEffect effect = m_aryActiveEffect_QianYao[(int)eSkillId];
            m_aryActiveEffect_QianYao[(int)eSkillId] = null;
			CEffectManager.s_Instance.DeleteSkillEffect(effect, eSkillId, CEffectManager.eSkillEffectType.qianyao);
        }

        if (m_nSkillStatus == 2 && nSkillStatus != 2)// 清除持续特效
        {
            CCosmosEffect effect = m_aryActiveEffect_ChiXu[(int)eSkillId];
            m_aryActiveEffect_ChiXu[(int)eSkillId] = null;
			CEffectManager.s_Instance.DeleteSkillEffect(effect, eSkillId, CEffectManager.eSkillEffectType.chixu);
        }


        if (nSkillStatus == 1)// 播放前摇特效
        {
			CCosmosEffect effect = null;
			if (eSkillId == CSkillSystem.eSkillId.i_merge) {

			} else {
				effect = CEffectManager.s_Instance.NewSkillEffect(eSkillId, CEffectManager.eSkillEffectType.qianyao);
				SetParent(effect, eSkillId);
				effect.SetLocalPos(Vector3.zero);
				effect.SetScale(CEffectManager.s_Instance.GetSkillEffectScale(eSkillId, CEffectManager.eSkillEffectType.qianyao));
			}

			if ( eSkillId == CSkillSystem.eSkillId.t_become_thorn )
			{
				CSkeletonAnimation skeleton_ani = (CSkeletonAnimation)effect;
				skeleton_ani.PlayAnimation (0, "ani04", 1, false);
			}
			else if ( eSkillId == CSkillSystem.eSkillId.i_merge )
			{
				
			}
			else
			{
            	CFrameAnimationEffect frame_ani_effect = (CFrameAnimationEffect)effect;
				frame_ani_effect.BeginPlay(false);
			}
            
            m_aryActiveEffect_QianYao[(int)eSkillId] = effect;
        }

        if (nSkillStatus == 2) // 播放持续特效
        {
			CCosmosEffect effect = CEffectManager.s_Instance.NewSkillEffect(eSkillId, CEffectManager.eSkillEffectType.chixu);
            SetParent(effect, eSkillId);
            effect.SetLocalPos( Vector3.zero );
			effect.SetScale( CEffectManager.s_Instance.GetSkillEffectScale(eSkillId, CEffectManager.eSkillEffectType.chixu) );
			if (eSkillId == CSkillSystem.eSkillId.t_become_thorn) {
				CSkeletonAnimation skeleton_ani = (CSkeletonAnimation)effect;
				skeleton_ani.PlayAnimation (0, "ani04_1", 0.2f, true);
			} else if (eSkillId == CSkillSystem.eSkillId.i_merge) {
				effect.BeginPlayRotation (50f);
			}
			else
			{
				CFrameAnimationEffect frame_ani_effect = (CFrameAnimationEffect)effect;
				frame_ani_effect.BeginPlay (true);
			}
            m_aryActiveEffect_ChiXu[(int)eSkillId] = effect;
	
        }

      

        //// 以上为技能特效 

        //// 以下为一些逻辑功能
        switch(eSkillId)
        {
            case CSkillSystem.eSkillId.r_sneak:
                {
                    if ( _ball.IsMainPlayer())
                    {
                        if ( nSkillStatus == 0)
                        {
							_ball.SetColliderDustEnable (true);
                        }
                        else
                        {
                            _ball.SetColliderDustEnable(false);
                        }
                    }
                }
                break;

        } // end switch
          //// end 逻辑功能


        // 设置最新的状态
        SetCurSkillStatus(nSkillStatus);
    }

    public void SetParent( CCosmosEffect efffect, CSkillSystem.eSkillId eSkillId)
    {
        efffect.transform.SetParent(_goEffectContainer.transform);
    }

    public void Clear()
    {
        //// 扩张技能的特效
        _ball._goEffectContainer_QianYao.gameObject.SetActive(false);
        _ball._effectUnfoldQianYao.gameObject.SetActive(false);
        ////

        for (int i = 0; i < m_aryActiveEffect_QianYao.Length; i++)
        {
            CCosmosEffect effect = m_aryActiveEffect_QianYao[i];
            if (effect == null)
            {
                continue;
            }
            m_aryActiveEffect_QianYao[i] = null;
            CEffectManager.s_Instance.DeleteSkillEffect(effect, (CSkillSystem.eSkillId)i, CEffectManager.eSkillEffectType.qianyao);
        }

        for (int i = 0; i < m_aryActiveEffect_ChiXu.Length; i++)
        {
            CCosmosEffect effect = m_aryActiveEffect_ChiXu[i];
            if (effect == null)
            {
                continue;
            }
            m_aryActiveEffect_ChiXu[i] = null;
            CEffectManager.s_Instance.DeleteSkillEffect(effect, (CSkillSystem.eSkillId)i, CEffectManager.eSkillEffectType.chixu);
        }
    }
}
