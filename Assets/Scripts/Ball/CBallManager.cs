﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CBallManager : MonoBehaviour {

    public static CBallManager s_Instance = null;

    // SetActive(false)完全可能不慎被重复执行，因此这里用来recyle的容易应该是dictionary而不是list。免得出Bug

    Dictionary<int, Ball> m_dicRecycledBalls_MainPlayer = new Dictionary<int, Ball>();
    Dictionary<int, Ball> m_dicRecycledBalls_OtherClient = new Dictionary<int, Ball>();


    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    int s_nBallGuid = 0;
    public Ball NewBall( bool bMainPlayer = false )
    {
        Ball ball = null;
        bool bExist = false;
        if (bMainPlayer)
        {
            if (m_dicRecycledBalls_MainPlayer.Count > 0)
            {
                ball = m_dicRecycledBalls_MainPlayer.Values.First();
                m_dicRecycledBalls_MainPlayer.Remove(m_dicRecycledBalls_MainPlayer.Keys.First());
                bExist = true;
            }
        }
        else
        {
            if (m_dicRecycledBalls_OtherClient.Count > 0)
            {
                ball = m_dicRecycledBalls_OtherClient.Values.First();
                m_dicRecycledBalls_OtherClient.Remove(m_dicRecycledBalls_OtherClient.Keys.First());
                bExist = true;
            }
        }

        if (!bExist)
        {
            ball = GameObject.Instantiate(Main.s_Instance.m_preBall).GetComponent<Ball>();
            ball.SetGuid(s_nBallGuid++);
        }

        return ball;
    }

    public void DeleteBall( Ball ball, bool bMainPlayer = false)
    {
        ball.transform.SetParent(CBallManager.s_Instance.transform);
        ball._PlayerIns.RemoveFromList(ball);
        ball.ClearSkillModule();

        if (bMainPlayer)
        {
            m_dicRecycledBalls_MainPlayer[ball.GetGuid()] = ball;
        }
        else
        {
            m_dicRecycledBalls_OtherClient[ball.GetGuid()] = ball;
        }
        
    }
}
