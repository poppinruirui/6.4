﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CGunSightManager : MonoBehaviour {

    public static CGunSightManager s_Instance = null;

    public GameObject m_preGunSight;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    List<CCosmosGunSight> m_lstRecycledGunSight = new List<CCosmosGunSight>();
    public CCosmosGunSight NewGunSight()
    {
        CCosmosGunSight gunsight = null;
        if (m_lstRecycledGunSight.Count > 0)
        {
            gunsight = m_lstRecycledGunSight[0];
            m_lstRecycledGunSight.RemoveAt(0);
            gunsight.gameObject.SetActive( true );
        }
        else
        {
            gunsight = GameObject.Instantiate(m_preGunSight).GetComponent<CCosmosGunSight>();
        }

        return gunsight;
    }

    public void DeleteGunSight(CCosmosGunSight gunsight )
    {
        gunsight.gameObject.SetActive(false);
        m_lstRecycledGunSight.Add(gunsight);
    }
}
