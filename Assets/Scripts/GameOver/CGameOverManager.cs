﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CGameOverManager : MonoBehaviour {

    public static CGameOverManager s_Instance = null;
    public GameObject _goPanel;

    public Text _txtRank; // 我的本场排名

    public static bool s_bNowGameOvering = false;

    const float c_fGameOverPanelMinTime = 3f;
    float m_fTimeElpase = 0f;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if ( !s_bNowGameOvering)
        {
            return;
        }

        TimeLoop();


        if ( Input.GetMouseButtonDown(0) && m_fTimeElpase > c_fGameOverPanelMinTime)
        {
            s_bNowGameOvering = false;
            StartCoroutine("LoadScene", "SelectCaster");
        }
	}

    //异步加载场景  
    IEnumerator LoadScene(string scene_name)
    {
        AccountData.s_Instance.asyncLoad = SceneManager.LoadSceneAsync(scene_name);

        while (!AccountData.s_Instance.asyncLoad.isDone)
        {
            yield return null;
        }


    }

    public void SetGameOverPanelVisible(  bool bVisible )
    {
        _goPanel.SetActive(bVisible);

        
    }

    public void BeginGameOver()
    {
        m_fTimeElpase = 0;
        s_bNowGameOvering = true;
        SetGameOverPanelVisible( true );
        CPaiHangBang_Mobile.s_Instance.RefreshPaiHangbangPlayerInfo(true);
        _txtRank.text = CPaiHangBang_Mobile.s_Instance.GetMyRank().ToString();
        Main.s_Instance.ExitGame();
    }

    public void EndGameOver()
    {

    }

    void TimeLoop()
    {
        m_fTimeElpase += Time.deltaTime;
    }
}
