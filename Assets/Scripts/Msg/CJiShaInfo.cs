﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CJiShaInfo : MonoBehaviour {

    public static CJiShaInfo s_Instance;

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();

    public float m_fShowTime = 5f;
    float m_fShowTimeCount = 0f;

    /// <summary>
    /// / UI
    /// </summary>
    public GameObject _panelJiSha;
    public Text _txtKillerName;
    public Text _txtDeadMeatName;
    public Image _imgKillerAvatar;
    public Image _imgDeadMeatAvatar;

    public GameObject _panelJiSha_PC;
    public Text _txtKillerName_PC;
    public Text _txtDeadMeatName_PC;
    public Image _imgKillerAvatar_PC;
    public Image _imgDeadMeatAvatar_PC;

    public GameObject _panelJiSha_MOBILE;
    public Text _txtKillerName_MOBILE;
    public Text _txtDeadMeatName_MOBILE;
    public Image _imgKillerAvatar_MOBILE;
    public Image _imgDeadMeatAvatar_MOBILE;

    public Image _imgBgBar;

    public Text _txtAssistTitle;
    public GameObject _goAssistAvatarContainer;
    public GameObject _preAvatar;

    public Text _txtJiSha;

    public CanvasGroup _CanvasGroup;

    public GameObject _containerKillerAvatar;
    public GameObject _containerDeadmeatAvatar;

    public Toggle _toggleNoName;

    /// <summary>
    /// / end UI
    /// </summary>

    bool m_bShow = false;

    public float m_fAssistAvatarGap = 64f;
    public float m_fAssistAvatarScale = 0.8f;
    public float m_fDuoShaTime = 60f; // 多杀是以60秒为时限的
    public float m_fAssistTime = 30f; // 助攻有效时间
    public float m_nContinuousKillStartThreshold = 3; // 连续击杀起步门槛（3次）
    public float m_fFadeTime = 0.5f;


    /// <summary>
    /// / have name?
    /// </summary>
    public bool m_bShowName = false;

    public float m_fAssistContainerPosY = -35f;

    public float m_fNoNameModeKillerAvatarPosX = 175;
    public float m_fNoNameModeDeadMeatAvatarPosX = -215;
    public float m_fNoNameBgBarScale = 0.4f;
    public float m_fAssistContainerPosX_NoName = 235f;

    public float m_fHaveNameBgWidth = 570;
    public float m_fAssistContainerPosX_HaveName = 80f;
    /// end have name?
    


    public bool m_bFirstBlood = true;

    public string[] m_aryDuoShaSystemInfo;
    public string[] m_aryContinuousKillSystemInfo;
    public string m_szFirstBloodSystemInfo;
    public string m_szJiShaSystemInfo;

    List<CAvatar> m_lstAssistAvatar = new List<CAvatar>();
    const int MIN_COMMAND_LENGTH = 5;
    const int COMMAND_HAVE_ASSIST_MIN_LENGTH = 8;

    public enum eJiShaSystemMsgType
    {
        e_jisha,     // 击杀
        e_duosha,    // 多杀
        e_lianxusha, // 连续杀
    };

    public struct sJiShaSystemMsgParam
    {
        public eJiShaSystemMsgType type;
        public string szEaterName;
        public string szDeadmeatName;
        public Sprite sprKillerAvatar;
        public Sprite sprDeadMeatAvatar;
        public int[] aryAssistId;
        public int nDuoShaNum;
        public int nLianXuShaNum;
        public bool bFirstBlood;
    };

    List<sJiShaSystemMsgParam> m_lstJiShaSysMsgQueue = new List<sJiShaSystemMsgParam>(); // 击杀信息显示队列

    // Use this for initialization
    void Start() {

    }

    private void Awake()
    {
        s_Instance = this;
    }

    // Update is called once per frame
    void Update() {
        Counting();

        InitUI();

        QueueLoop();
    }

    private void FixedUpdate()
    {
        FadeLoop();
    }

    bool m_bUIInited = false;
    void InitUI()
    {
        if (Main.s_Instance == null)
        {
            return;
        }

        if (m_bUIInited)
        {
            return;
        }
        /*
        if ( Main.s_Instance.GetPlatformType() == Main.ePlatformType.pc )
        {
            _panelJiSha       =_panelJiSha_PC;
            _txtKillerName=       _txtKillerName_PC;
            _txtDeadMeatName=     _txtDeadMeatName_PC;
            _imgKillerAvatar=     _imgKillerAvatar_PC;
            _imgDeadMeatAvatar=   _imgDeadMeatAvatar_PC;
        }
        else if (Main.s_Instance.GetPlatformType() == Main.ePlatformType.mobile)
        {*/
        _panelJiSha = _panelJiSha_MOBILE;
        _txtKillerName = _txtKillerName_MOBILE;
        _txtDeadMeatName = _txtDeadMeatName_MOBILE;
        _imgKillerAvatar = _imgKillerAvatar_MOBILE;
        _imgDeadMeatAvatar = _imgDeadMeatAvatar_MOBILE;
        // }

        m_bUIInited = true;

    }

    
    public void PushOneJiShaSysMsgIntoQueue(eJiShaSystemMsgType type,
                                            CPlayerIns playerInsKIller,
                                            CPlayerIns playerInsDeadMeat

        )
    {
        string szKillerName = playerInsKIller.GetPlayerName();
        string szDeadMeatName = playerInsDeadMeat.GetPlayerName();
        int nKillerSkinId = playerInsKIller.GetSkinId();
        int nDeadMeatSkinId = playerInsDeadMeat.GetSkinId();
        Sprite sprSkiller = AccountData.s_Instance.GetSpriteByItemId(nKillerSkinId);
        Sprite sprDeadMeat = AccountData.s_Instance.GetSpriteByItemId(nDeadMeatSkinId);
        sJiShaSystemMsgParam param = new sJiShaSystemMsgParam();
        param.type = type;
        param.szEaterName = szKillerName;
        param.szDeadmeatName = szDeadMeatName;
        param.sprKillerAvatar = sprSkiller;
        param.sprDeadMeatAvatar = sprDeadMeat;
        m_lstJiShaSysMsgQueue.Add(param);
    }


    public void PushOneJiShaSysMsgIntoQueue(
                        eJiShaSystemMsgType type,
                        int nKillerId,
                        int nDeadMeatId,
                        ref List<int> lstAssist,
                        bool bFirstBlood,
                        int nDuoSha,
                        int nLianXuSha
                    )
    {
        string szKillerName = "我日你个龟";
        string szDeadMeatName = "你妹啊";
        Player playerEater = CPlayerManager.s_Instance.GetPlayer(nKillerId);
        if (playerEater)
        {
            szKillerName = playerEater.GetPlayerName();
        }
        else
        {
            szKillerName = "Player_" + nKillerId;
        }
        Player playerDeadMeat = CPlayerManager.s_Instance.GetPlayer(nDeadMeatId);
        if (playerDeadMeat)
        {
            szDeadMeatName = playerDeadMeat.GetPlayerName();
        }
        else
        {
            szDeadMeatName = "Player_" + nDeadMeatId;
        }
        Sprite sprKillerAvatar = ResourceManager.s_Instance.GetSkinSprite(nKillerId);
        Sprite sprDeadMeatAvatar = ResourceManager.s_Instance.GetSkinSprite(nDeadMeatId);

        sJiShaSystemMsgParam param = new sJiShaSystemMsgParam();
        param.type = type;
        param.szEaterName = szKillerName;
        param.szDeadmeatName = szDeadMeatName;
        param.sprKillerAvatar = sprKillerAvatar;
        param.sprDeadMeatAvatar = sprDeadMeatAvatar;
        param.bFirstBlood = bFirstBlood;
        param.aryAssistId = new int[lstAssist.Count];
        lstAssist.CopyTo(param.aryAssistId);
        param.nDuoShaNum = nDuoSha;
        param.nLianXuShaNum = nLianXuSha;

        if (type >= eJiShaSystemMsgType.e_duosha)
        {
            m_lstJiShaSysMsgQueue.Add(param);
        }
        else
        {
            bool bInserted = false;
            for ( int i = 0; i < m_lstJiShaSysMsgQueue.Count; i++ )
            {
                sJiShaSystemMsgParam node = m_lstJiShaSysMsgQueue[i];
                if ( node.type >= eJiShaSystemMsgType.e_duosha )
                {
                    m_lstJiShaSysMsgQueue.Insert(i, param);
                    bInserted = true;
                    break;
                }
            }
            if (!bInserted)
            {
                m_lstJiShaSysMsgQueue.Add(param);
            }
        }
    }

    List<int> lstAssistId = new List<int>();
    public void ParseAndExecCommands(string szCommands)
    {
        string[] aryCommand = szCommands.Split('\n');
        for (int i = 0; i < aryCommand.Length; i++)
        {
            int nCommandLen = aryCommand[i].Length;
            if ( nCommandLen < MIN_COMMAND_LENGTH)
            {
                continue;
            }

            string[] aryParam = aryCommand[i].Split( ' ' );
            int nType = int.Parse(aryParam[0]);
            switch( (eJiShaSystemMsgType)nType)
            {
                case eJiShaSystemMsgType.e_jisha:
                    {
                        int nEaterId = int.Parse(aryParam[1]);
                        int nDeadMeatId = int.Parse(aryParam[2]);
                        bool bFirstBlood = int.Parse(aryParam[3]) > 0;
                        if (nCommandLen > COMMAND_HAVE_ASSIST_MIN_LENGTH) // 有助攻
                        {
                            lstAssistId.Clear();
                            string[] aryAssistId = aryParam[4].Split( ',' ) ;
                            for ( int j = 0; j < aryAssistId.Length; j++ )
                            {
                                lstAssistId.Add( int.Parse(aryAssistId[j]) );
                            }
                        }

                        PushOneJiShaSysMsgIntoQueue(eJiShaSystemMsgType.e_jisha, nEaterId, nDeadMeatId, ref lstAssistId, bFirstBlood, 0, 0);
                    }
                    break;
                case eJiShaSystemMsgType.e_duosha:
                    {
                        int nEaterId = int.Parse(aryParam[1]);
                        int nNum = int.Parse(aryParam[2]);
                        PushOneJiShaSysMsgIntoQueue(eJiShaSystemMsgType.e_duosha, nEaterId, 0, ref lstAssistId, false, nNum, 0);
                    }
                    break;
                case eJiShaSystemMsgType.e_lianxusha:
                    {
                        int nEaterId = int.Parse(aryParam[1]);
                        int nNum = int.Parse(aryParam[2]);
                        PushOneJiShaSysMsgIntoQueue(eJiShaSystemMsgType.e_lianxusha, nEaterId, 0, ref lstAssistId, false, 0, nNum );
                    }
                    break;
            } // end switch
        }
    }

    void QueueLoop()
    {
        if (m_bShow)
        {
            return;
        }

        if (m_lstJiShaSysMsgQueue.Count == 0)
        {
            return;
        }

        sJiShaSystemMsgParam param = m_lstJiShaSysMsgQueue[0];
        m_lstJiShaSysMsgQueue.RemoveAt(0);

        Show(
            param.type,
            param.szEaterName, 
            param.szDeadmeatName, 
            param.sprKillerAvatar,
            param.sprDeadMeatAvatar,
            ref param.aryAssistId,
            param.bFirstBlood,
            param.nDuoShaNum,
            param.nLianXuShaNum
            );
    }

    // right here
    public void Show(
                        eJiShaSystemMsgType type,
                        string szKillerName,
                        string szDeadMeatName,
                        Sprite sprKillerAvatar,
                        Sprite sprDeadMeatAvatar,
                        ref int[] aryAssist,
                        bool bFirstBlood,
                        int nDuoSha,
                        int nLianXuSha)
    {
        _CanvasGroup.alpha = 1f;
        _panelJiSha.SetActive( true );
        m_bShow = true;
        m_fShowTimeCount = 0;

        //// show name or not
        m_bShowName = true;
        if ( m_bShowName )
        {
            vecTempPos.x = 0;
            vecTempPos.y = 0;
            vecTempPos.z = 0;
         
            _txtKillerName.gameObject.SetActive( true );
            _txtDeadMeatName.gameObject.SetActive(true);

          
            vecTempScale.x = 1f;
            vecTempScale.y = 1f;
            vecTempScale.z = 1f;
            _imgBgBar.rectTransform.localScale = vecTempScale;
        }
        else
        {
            vecTempPos.y = 0;
            vecTempPos.z = 0;
            vecTempPos.x = m_fNoNameModeKillerAvatarPosX;
            _containerKillerAvatar.transform.localPosition = vecTempPos;
            vecTempPos.x = m_fNoNameModeDeadMeatAvatarPosX;
            _containerDeadmeatAvatar.transform.localPosition = vecTempPos;
            _txtKillerName.gameObject.SetActive(false);
            _txtDeadMeatName.gameObject.SetActive(false);

            vecTempPos.x = m_fAssistContainerPosX_NoName;
            vecTempPos.y = m_fAssistContainerPosY;
            _goAssistAvatarContainer.transform.localPosition = vecTempPos;

            vecTempScale.x = m_fNoNameBgBarScale;
            vecTempScale.y = 1f;
            vecTempScale.z = 1f;
            _imgBgBar.rectTransform.localScale = vecTempScale;

        }

        //// end show name or not


        _txtAssistTitle.gameObject.SetActive(false);
        for (int i = 0; i < m_lstAssistAvatar.Count; i++)
        {
            CAvatar avatar = m_lstAssistAvatar[i];
            avatar.gameObject.SetActive(false);
        }


        if (type == eJiShaSystemMsgType.e_jisha)
        {
            if (m_bShowName)
            {
                           _txtDeadMeatName.gameObject.SetActive(true);
            }
            _containerDeadmeatAvatar.SetActive(true);
            _txtKillerName.text = szKillerName;
            _txtDeadMeatName.text = szDeadMeatName;
            _imgKillerAvatar.sprite = sprKillerAvatar;
            _imgDeadMeatAvatar.sprite = sprDeadMeatAvatar;
            ShowAssistInfo(ref aryAssist);
            if (bFirstBlood)
            {
                _txtJiSha.text = m_szFirstBloodSystemInfo;
            }
            else
            {
                _txtJiSha.text = m_szJiShaSystemInfo;
            }
        }
        else
        {
            _containerDeadmeatAvatar.SetActive(false);
            _txtDeadMeatName.gameObject.SetActive(false);

            int nIndex = 0;
            if (type == eJiShaSystemMsgType.e_duosha)
            {
                nIndex = nDuoSha;
                if (nIndex >= m_aryDuoShaSystemInfo.Length)
                {
                    nIndex = m_aryDuoShaSystemInfo.Length - 1;
                }
                _txtJiSha.text = m_aryDuoShaSystemInfo[nIndex];
            }
            else if (type == eJiShaSystemMsgType.e_lianxusha)
            {
                nIndex = nLianXuSha;
                if (nIndex >= m_aryContinuousKillSystemInfo.Length)
                {
                    nIndex = m_aryContinuousKillSystemInfo.Length - 1;
                }
                _txtJiSha.text = m_aryContinuousKillSystemInfo[nIndex];
            }
        }
    }

    void UpdateDuoShaoStatus(  )
    {
        /*
        int nIndex = m_nDuoSha - 1;
        if (nIndex >= m_aryDuoShaSystemInfo.Length)
        {
            nIndex = m_aryDuoShaSystemInfo.Length - 1;
        }
        _txtJiSha.text = m_aryDuoShaSystemInfo[nIndex];
        */
    }

    public void Hide()
    {
        m_bShow = false;
        _panelJiSha.gameObject.SetActive(false);
    }

    void Counting()
    {
        if ( !m_bShow)
        {
            return;
        }
        m_fShowTimeCount += Time.deltaTime;
        if (m_fShowTimeCount >= m_fShowTime)
        {
            BeginFade();//Hide();
        }
    }

    bool m_bFading = false;
    void BeginFade()
    {
        m_bFading = true;
    }

    void FadeLoop()
    {
        if ( !m_bFading )
        {
            return;
        }

        float fFade = Time.fixedDeltaTime / 0.5f;
        _CanvasGroup.alpha -= fFade;
        if (_CanvasGroup.alpha <= 0)
        {
            EndFade();
        }
    }

    void EndFade()
    {
        m_bFading = false;
        m_bShow = false;
    }

    void ShowAssistInfo( ref int[] lstAssist )
    {
        return;

        if (lstAssist.Length == 0)
        {
            return;
        }

        _txtAssistTitle.gameObject.SetActive(true);
        for ( int i = 0; i < lstAssist.Length; i++ )
        {
            CAvatar avatar = null;
            if (m_lstAssistAvatar.Count <= i)
            {
                avatar = GameObject.Instantiate( _preAvatar ).GetComponent<CAvatar>();
                vecTempPos.x = i * m_fAssistAvatarGap;
                vecTempPos.y = 0f;
                avatar.transform.parent = _goAssistAvatarContainer.transform;
                avatar.transform.localPosition = vecTempPos;
                vecTempScale.x = m_fAssistAvatarScale;
                vecTempScale.y = m_fAssistAvatarScale;
                vecTempScale.z = 0f;
                avatar.transform.localScale = vecTempScale;
                m_lstAssistAvatar.Add(avatar);
            }
            avatar = m_lstAssistAvatar[i];
            avatar.SetAvatar(lstAssist[i]);
            avatar.gameObject.SetActive( true );
        }
    }

    public void OnToggleValueChanged_NoName()
    {
        m_bShowName = !_toggleNoName.isOn;
    }
}
