using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerAction : MonoBehaviour
{
    static Vector3 vecTempPos = new Vector3();

    public Canvas _canvas;
    public GameObject _global;
    public GameObject _center;
    public GameObject _world_cursor;
    public GameObject _screen_cursor;

    public Vector2 _cursor_direction = Vector2.zero;
    public Vector2 _joystick_movement = Vector2.zero;


    float _cursor_velocity = 8.0f;
    float _cursor_realtime_velocity = 8.0f;
    
    float _cursor_radius = 0.0f;
    float _cursor_move_time_velocity = 0.0f;
    public const float CURSOR_MOVE_TIME = 0.5f;

    public float _ball_velocity = 0.2f;
    float _ball_realtime_velocity = 0.2f;
	public float _ball_realtime_velocity_without_size = 0.2f;

    public Vector3 _center_move_velocity = Vector3.zero;

    public const float CENTER_SLOW_MOVE_TIME = 10.0f;
    public const float CENTER_FAST_MOVE_TIME = 1.0f;

    public float _center_move_time_velocity = 0.0f;
    public float _center_move_time = CENTER_FAST_MOVE_TIME;
    
    bool _release_joystick_keep_position = false;
   public  bool _release_joystick_fast_stop = false;        
    bool _release_joystick_fast_rotation = false;    
    
    Vector2 _touch_screen_position = Vector2.zero;
    Vector3 _touch_world_position  = Vector3.zero;

    const float _touch_stop_interval = 0.2f;
    float _touch_start_time = 0.0f;

    public float joystick_radius_multiple = 1.5f;

    public static PlayerAction s_Instance = null;

    void Awake()
    {
        s_Instance = this;
    }

    void Start()
    {
		Reset ();
    }

    void OnDestroy()
    {

    }

	public void Reset()
	{
		_ball_realtime_velocity = 0f;
		_ball_realtime_velocity_without_size = 0f;
	}

    public Vector3 GetWorldCursorPos()
    {
        return this._world_cursor.transform.position;
    }

    public Vector2 GetSpecialDirection()
    {
        return m_vec2MoveInfo;
    }

    public float GetSpeed()
    {
        return _ball_realtime_velocity_without_size;
    }


    float m_nFrameCount = 0;
	const float c_nPlayerMoveInfoSyncInterval = 0.5f; // 500ms同步一次，一秒钟大概可以同步2次
	public Vector2 m_vec2MoveInfo = new Vector2();


    void FixedUpdate()
    {
        DoUpdate();
    }

    double m_dMouseDownTime = 0;
    void Update()
    {
        //        DoUpdate();
        if ( Input.GetMouseButtonDown(0) )
        {
            this._touch_start_time = Main.GetTime();
        }

        ProcessStopAndMoveToCenter();
    }

    void ProcessStopAndMoveToCenter()
    {
        if ( UIManager.IsPointerOverUI() )
        {
            return;
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (CtrlMode.GetCtrlMode() == CtrlMode.CTRL_MODE_JOYSTICK)
            {
                double dTimeElapse = Main.GetTime() - m_dMouseDownTime;

                if (dTimeElapse < 0.5f)
                {
                    StopAndMoveToCenter();
                }
            }
        }

        if (Input.GetMouseButtonDown(0))
        {
            m_dMouseDownTime = Main.GetTime();
        }
    }

    public void DoUpdate()
    { 
		if ( Main.s_Instance == null || Main.s_Instance.m_MainPlayer == null) {

			return;
		}

        if ( Main.s_Instance.IsGameOver() )
		{
            return;
        }

		if (!Main.s_Instance || !Main.s_Instance.m_MainPlayer/* || !Main.s_Instance.m_MainPlayer.IsMoving ()*/) {

			return;
		}

        if ( AccountManager.m_eSceneMode != AccountManager.eSceneMode.Game )
        {
			
            return;
        }


        //this.UpdateIndicators();
        this.UpdateCursorDirection();
		if (/*CtrlMode.GetCtrlMode() != CtrlMode.CTRL_MODE_NONE*/true) {
            this.UpdateCursorPosition();
            /*
            Vector3 balls_center_position = Vector3.zero;
            Vector3 balls_min_position = Vector3.zero;
            Vector3 balls_max_position = Vector3.zero;
            */
            this.UpdateBallsPositions(ref balls_center_position,
                                      ref balls_min_position,
                                      ref balls_max_position);
            this.UpdateCenterPosition(balls_center_position);
            CtrlMode.UpdateCameraViewPort(ref balls_min_position,
                                          ref balls_max_position);
        }
        else {
            this.CheckPlayingStatus();
        }
	}

    public Vector3 balls_center_position = Vector3.zero;
    public Vector3 balls_min_position = Vector3.zero;
    public Vector3 balls_max_position = Vector3.zero;

    void CheckPlayingStatus()
    {
        SceneAction scene_action =
            this._global.GetComponent<SceneAction>();
        GUIAction gui_action =
            this._global.GetComponent<GUIAction>();
        if (this.IsMouseDown() &&
            scene_action.GetSceneStatus() == SceneStatus.SceneStatusPlaying) {
            CtrlMode.SetCtrlMode(CtrlMode.CTRL_MODE_HAND);
            CtrlMode.SetDirIndicatorType(CtrlMode.DIR_INDICATOR_LINE);
            gui_action.UpdateCtrlModeDropDown(CtrlMode.CTRL_MODE_HAND);
            gui_action.UpdateDirIndicatorDropDown(CtrlMode.DIR_INDICATOR_LINE);
            this._center_move_time = PlayerAction.CENTER_SLOW_MOVE_TIME;
        }
    }
    
    public void UpdateIndicators()
    {
        switch (CtrlMode.GetCtrlMode()) {
			/*
            case CtrlMode.CTRL_MODE_NONE:
                this._world_cursor.GetComponent<Renderer>().enabled = false;
                this._screen_cursor.SetActive(false);
                this._center.GetComponent<Renderer>().enabled = false;                
                break;
			*/
           case CtrlMode.CTRL_MODE_HAND:
                this._world_cursor.GetComponent<Renderer>().enabled = false;
                this._screen_cursor.SetActive(true);
                this._center.GetComponent<Renderer>().enabled = false;
                break;
            case CtrlMode.CTRL_MODE_JOYSTICK:
                this._world_cursor.GetComponent<Renderer>().enabled = false;
                this._screen_cursor.SetActive(false);
                this._center.GetComponent<Renderer>().enabled = false;
                break;
            case CtrlMode.CTRL_MODE_WORLD_CURSOR:
                this._world_cursor.GetComponent<Renderer>().enabled = true;
                this._screen_cursor.SetActive(false);                
                this._center.GetComponent<Renderer>().enabled = false;
                break;                
            case CtrlMode.CTRL_MODE_SCREEN_CURSOR:                
                this._world_cursor.GetComponent<Renderer>().enabled = false;
                this._screen_cursor.SetActive(true);                                
                this._center.GetComponent<Renderer>().enabled = false;
                break;
            default:
                this._world_cursor.GetComponent<Renderer>().enabled = false;
                this._screen_cursor.SetActive(true);
                this._center.GetComponent<Renderer>().enabled = true;
                break;
        }
    }

    public void StopAndMoveToCenter()
    {
        if (Main.s_Instance == null || Main.s_Instance.m_MainPlayer == null)
        {
            return;
        }

        this._joystick_movement = Vector2.zero;
        this._cursor_realtime_velocity = 0.0f;
        this._release_joystick_fast_stop = false;
        Main.s_Instance.m_MainPlayer.SetMoving(false); // 停止移动
        Main.s_Instance.m_MainPlayer.BeginMoveToCenter(); // 主角所有的球球向中心点靠拢
    }

    public void UpdateCursorPosition()
    {
        if (CtrlMode.GetCtrlMode() == CtrlMode.CTRL_MODE_HAND ||
            CtrlMode.GetCtrlMode() == CtrlMode.CTRL_MODE_SCREEN_CURSOR &&
            this._release_joystick_keep_position) {

            this._world_cursor.transform.position =
                new Vector3(this._touch_world_position.x,
                            this._touch_world_position.y,
                            this._world_cursor.transform.position.z);                            

            Vector2 canvas_point = Vector2.zero;
            if (RectTransformUtility.ScreenPointToLocalPointInRectangle(this._canvas.GetComponent<RectTransform>(),
                                                                        this._touch_screen_position,
                                                                        this._canvas.worldCamera,
                                                                        out canvas_point)) {
                this._screen_cursor.GetComponent<RectTransform>().anchoredPosition = canvas_point;
            }
	
        }
        else if (CtrlMode.GetCtrlMode() == CtrlMode.CTRL_MODE_JOYSTICK) { // 摇杆模式下，点击一下屏幕就是“急停”操作
            Vector3 position = Vector3.zero;
            if (this._release_joystick_fast_stop) { // 
                position = this._center.transform.position;
                /*
                this._joystick_movement = Vector2.zero;
                this._cursor_realtime_velocity = 0.0f;
                this._release_joystick_fast_stop = false;
				Main.s_Instance.m_MainPlayer.SetMoving ( false ); // 停止移动
				Main.s_Instance.m_MainPlayer.BeginMoveToCenter(); // 主角所有的球球向中心点靠拢
                */
            }
            else {

                if ( this._joystick_movement.magnitude > 0.05f )
                {
                    Main.s_Instance.m_MainPlayer.SetMoving(true);
                }


                float radius = Mathf.Lerp(0.0f,
                                          Mathf.Max(CtrlMode._world_port_h, CtrlMode._world_port_v) * 0.5f,
                                          this._joystick_movement.magnitude);
                this._cursor_radius =
                    Mathf.SmoothDamp(this._cursor_radius,
                                     radius,
                                     ref this._cursor_move_time_velocity,
                                     PlayerAction.CURSOR_MOVE_TIME);

                this._cursor_radius = m_fBallTeamRadius * joystick_radius_multiple; // 之前之所以所有球球的运行轨迹都是平行的，就是这个“鼠标点”太远太远了。稍微缩短点就好了

                position = 
                    this._center.transform.position +
                    new Vector3(this._cursor_direction.x,
                                this._cursor_direction.y, 0) * this._cursor_radius;
           }
            this._world_cursor.transform.position = position;

        }        
        else if (CtrlMode.GetCtrlMode() == CtrlMode.CTRL_MODE_WORLD_CURSOR) {
            float joystick_magnitude = this._joystick_movement.magnitude;
            this._cursor_realtime_velocity = Mathf.Lerp(0.0f,
                                                        this._cursor_velocity,
                                                        joystick_magnitude);
            Vector3 position =
                this._world_cursor.transform.position + 
                new Vector3(this._cursor_direction.x,
                            this._cursor_direction.y, 0) *
                this._cursor_realtime_velocity * Time.fixedDeltaTime;
            position.z = -1;
            position.x = Mathf.Min(position.x, this._center.transform.position.x + CtrlMode._world_port_h * 0.5f);
            position.x = Mathf.Max(position.x, this._center.transform.position.x - CtrlMode._world_port_h * 0.5f);
            position.y = Mathf.Min(position.y, this._center.transform.position.y + CtrlMode._world_port_v * 0.5f);
            position.y = Mathf.Max(position.y, this._center.transform.position.y - CtrlMode._world_port_v * 0.5f);
            this._world_cursor.transform.position = position;
        }
        else if (CtrlMode.GetCtrlMode() == CtrlMode.CTRL_MODE_SCREEN_CURSOR) {
            float joystick_magnitude = this._joystick_movement.magnitude;
            this._cursor_realtime_velocity = Mathf.Lerp(0.0f,
                                                        this._cursor_velocity,
                                                        joystick_magnitude);
            this._touch_world_position =
                this._world_cursor.transform.position + 
                new Vector3(this._cursor_direction.x,
                            this._cursor_direction.y, 0) *
                this._cursor_realtime_velocity * Time.fixedDeltaTime;
            this._touch_world_position.x =
                Mathf.Min(this._touch_world_position.x,
                          this._center.transform.position.x + CtrlMode._world_port_h * 0.5f);
            this._touch_world_position.x =
                Mathf.Max(this._touch_world_position.x,
                          this._center.transform.position.x - CtrlMode._world_port_h * 0.5f);
            this._touch_world_position.y =
                Mathf.Min(this._touch_world_position.y,
                          this._center.transform.position.y + CtrlMode._world_port_v * 0.5f);
            this._touch_world_position.y =
                Mathf.Max(this._touch_world_position.y,
                          this._center.transform.position.y - CtrlMode._world_port_v * 0.5f);
            this._world_cursor.transform.position = this._touch_world_position;
            this._touch_screen_position = Camera.main.WorldToScreenPoint(this._touch_world_position);
            Vector2 canvas_point = Vector2.zero;
            if (RectTransformUtility.ScreenPointToLocalPointInRectangle(this._canvas.GetComponent<RectTransform>(),
                                                                        this._touch_screen_position,
                                                                        this._canvas.worldCamera,
                                                                        out canvas_point)) {
                this._screen_cursor.GetComponent<RectTransform>().anchoredPosition = canvas_point;
            }
        }
    }
    /*
    public void UpdateBallsPositions(ref Vector3 balls_center_position,
                                 ref Vector3 balls_min_position,
                                 ref Vector3 balls_max_position)
    {
        balls_center_position.x = 0.0f;
        balls_center_position.y = 0.0f;
        balls_min_position.x = 0.0f;
        balls_min_position.y = 0.0f;
        balls_max_position.x = 0.0f;
        balls_max_position.y = 0.0f;
        int nCount = 0;
        float fTotalWeight = 0.0f;

        bool bFirst = true;

        foreach (Transform child in Main.s_Instance.m_goMainPlayer.transform)
        {
            GameObject goBall = child.gameObject;
            Ball ball = goBall.GetComponent<Ball>();
            if ( ball.IsEjecting() )
            {
                continue;
            }

            if (bFirst)
            {
                balls_min_position.x = ball.transform.position.x;
                balls_max_position.x = ball.transform.position.x;
                balls_min_position.y = ball.transform.position.x;
                balls_max_position.y = ball.transform.position.x;
            }
            else
            {
                balls_min_position.x = Mathf.Min(balls_min_position.x, ball.transform.position.x);
                balls_max_position.x = Mathf.Max(balls_max_position.x, ball.transform.position.x);
                balls_min_position.y = Mathf.Min(balls_min_position.y, ball.transform.position.y);
                balls_max_position.y = Mathf.Max(balls_max_position.y, ball.transform.position.y);
            }

            float fWeight = ball.GetSize() * ball.GetSize() * ball.GetSize(); // 体积作为权重，影响中心点计算
            balls_center_position.x += ball.transform.position.x * fWeight;
            balls_center_position.y += ball.transform.position.y * fWeight;
            fTotalWeight += fWeight;
            nCount++;

            bFirst = false;

            BallAction ball_action = ball.GetComponent<BallAction>();
            if (CtrlMode.GetCtrlMode() == CtrlMode.CTRL_MODE_JOYSTICK)
            {
                this._ball_realtime_velocity =
                    Mathf.Lerp(0, Main.s_Instance.m_fBallBaseSpeed, this._joystick_movement.magnitude);
            }
            else
            {
                this._ball_realtime_velocity = Main.s_Instance.m_fBallBaseSpeed;
            }

            

            ball_action.UpdatePosition(this._world_cursor.transform.position, this._ball_realtime_velocity);

        } // end foreach
        balls_center_position.x = balls_center_position.x / nCount / fTotalWeight;
        balls_center_position.y = balls_center_position.y / nCount / fTotalWeight;
    }
    */

	public Vector3 m_vecBallsCenter = new Vector3();
	public Vector3 GetBallsCenter()
	{
		return m_vecBallsCenter;
	}

	public Dictionary<int, float> dicTempGroupInGrass = new Dictionary<int, float> ();

    public static void  LoadSpitSkillParams( int nLevel = 1 )
    {
        CSkillSystem.sSkillParam skill_params = CSkillSystem.s_Instance.GetSkillParamsByIdAndLevel( CSkillSystem.eSkillId.w_spit , nLevel);
        CtrlMode.m_fAcctackRadiusMultiple = skill_params.aryValues[3];
    }

    public static float GetAttackDistance(float fRadius)
    {
        return Ball.GetRunDistance(CtrlMode.m_fAcctackRadiusMultiple, fRadius );
       // return (CtrlMode.m_fAcctackRadiusMultiple  * Mathf.Sqrt(2 * fRadius) + fRadius);
    }

    float m_fPkSelfBallTimeCount = 0f;
    public static int s_nfLeftBallIndex = 0;
    public static int s_nRightBallIndex = 0;
    public static int s_nTopBallIndex = 0;
    public static int s_nBottomBallIndex = 0;
    public static int s_nBiggestBallIndex = 0;

    List<Ball> m_lstMostBigBallsToShowName = new List<Ball>();

    void InsertSortForMostBigBallsToShowName( Ball ball )
    {
        for ( int i = 0; i < m_lstMostBigBallsToShowName.Count; i++ )
        {
            if ( ball.GetVolume() > m_lstMostBigBallsToShowName[i].GetVolume())
            {
                m_lstMostBigBallsToShowName.Insert( i, ball);
                return;
            }
        }
        m_lstMostBigBallsToShowName.Add(ball);
    }

    // [to youhua]这个函数内容比较复杂，应该有一些需要优化的地方，比如GetAttackDistance()就明显的有冗余运算
     public void UpdateBallsPositions(ref Vector3 balls_center_position,
                                     ref Vector3 balls_min_position,
                                     ref Vector3 balls_max_position)
    {
        if (Main.s_Instance == null || Main.s_Instance.m_goMainPlayer == null) {
			return;
		}


        //GameObject[] balls = GameObject.FindGameObjectsWithTag("Ball");
        //for (int i=0; i<balls.Length; ++i) {
		bool bFirst = true;
		bool bShitFirst = true;
        float fTotalWeight = 0.0f;
        float fBiggestWeight = 0;
        //balls_center_position = Vector3.zero;

		m_lstMoveOrderX.Clear();
		m_lstMoveOrderY.Clear();
        List<Ball> lst = null;
        Main.s_Instance.m_MainPlayer.GetBallList( ref lst );
        int nLiveBallNum = 0;

        float fPlayerbaseSpeed = Main.s_Instance.m_MainPlayer.GetBaseSpeed();

        if (CtrlMode.GetCtrlMode() == CtrlMode.CTRL_MODE_JOYSTICK)
        {
            this._ball_realtime_velocity_without_size =
                Mathf.Lerp(0, fPlayerbaseSpeed/*Main.s_Instance.m_fBallBaseSpeed*/, this._joystick_movement.magnitude);

        }
        else
        {
            this._ball_realtime_velocity_without_size = fPlayerbaseSpeed/*Main.s_Instance.m_fBallBaseSpeed*/;
        }


        m_lstMostBigBallsToShowName.Clear();
        for ( int i = 0; i < lst.Count; i++ ) // 散户球的移动
		{
			Ball the_ball = lst[i];

            InsertSortForMostBigBallsToShowName(the_ball);

            nLiveBallNum++;

            float fWeight = the_ball.GetVolume(); // 体积作为权重，影响中心点计算
            float fAttackDis = GetAttackDistance(the_ball.GetRadius());

            fTotalWeight += fWeight;
      
            if (bShitFirst)
            {
              //  balls_center_position = the_ball.transform.position;
                fBiggestWeight = fWeight;
                s_nBiggestBallIndex = the_ball.GetIndex();
                bShitFirst = false; 
            }
            else
            {
               // float k = fWeight / fTotalWeight;
               // balls_center_position = ( balls_center_position * ( 2 - k ) + the_ball.transform.position * k ) / 2;

                if (fWeight > fBiggestWeight)
                {
                    fBiggestWeight = fWeight;
                    s_nBiggestBallIndex = the_ball.GetIndex();
                }
            }
            
            if (bFirst) {
				balls_min_position.x = the_ball.transform.position.x - fAttackDis;
				balls_min_position.y = the_ball.transform.position.y - fAttackDis;
				balls_max_position.x = the_ball.transform.position.x + fAttackDis;
                balls_max_position.y = the_ball.transform.position.y + fAttackDis;

                s_nBottomBallIndex = the_ball.GetIndex();
                s_nTopBallIndex = the_ball.GetIndex();
                s_nfLeftBallIndex = the_ball.GetIndex();
                s_nRightBallIndex = the_ball.GetIndex();

                bFirst = false;
			}
            /*
			balls_min_position.x = Mathf.Min((the_ball.transform.position.x -
                the_ball.GetRadius()),
                                                 balls_min_position.x);
            */
            float fLeftPos = the_ball.transform.position.x - fAttackDis;
            if (fLeftPos < balls_min_position.x)
            {
                balls_min_position.x = fLeftPos;
                s_nfLeftBallIndex = the_ball.GetIndex();
            }

            /*
			balls_min_position.y = Mathf.Min((the_ball.transform.position.y -
                the_ball.GetRadius()),
                                                 balls_min_position.y);
            */
            float fBottomPos = the_ball.transform.position.y - fAttackDis;
            if (fBottomPos < balls_min_position.y)
            {
                balls_min_position.y = fBottomPos;
                s_nBottomBallIndex = the_ball.GetIndex();
            }


            /*
			balls_max_position.x = Mathf.Max((the_ball.transform.position.x +
                the_ball.GetRadius()),
                                                 balls_max_position.x);
            */
            float fRightPos = the_ball.transform.position.x + fAttackDis;
            if (fRightPos > balls_max_position.x)
            {
                balls_max_position.x = fRightPos;
                s_nRightBallIndex = the_ball.GetIndex();
            }



      
            float fTopPos = the_ball.transform.position.y + fAttackDis;
            if (fTopPos > balls_max_position.y)
            {
                balls_max_position.y = fTopPos;
                s_nTopBallIndex = the_ball.GetIndex();
            }


			if (Main.s_Instance.m_MainPlayer.IsMoving ()  && (!Main.s_Instance.m_MainPlayer.DoNotMove())) {
				the_ball._ba.UpdatePosition (this._world_cursor.transform.position, this._ball_realtime_velocity_without_size ); 
				
			}
        } //// end for

        Main.s_Instance.m_MainPlayer.ClearMostBigBallsToShowName();
        int nNum = m_lstMostBigBallsToShowName.Count;
        if (nNum > Main.MAX_BALL_NUM_TO_SHOW_NAME)
        {
            nNum = Main.MAX_BALL_NUM_TO_SHOW_NAME;
        }
        for (int i = 0; i < nNum; i++)
        {
            Main.s_Instance.m_MainPlayer.AddMostBigBallsToShowName(m_lstMostBigBallsToShowName[i].GetIndex());
        }


        if (nLiveBallNum > 0 )
        {
            Main.s_Instance.m_MainPlayer.SetTotalVolume(fTotalWeight);
            m_vecBallsCenter.x = ( balls_min_position.x + balls_max_position.x ) / 2f;
            m_vecBallsCenter.y = ( balls_min_position.y + balls_max_position.y ) / 2f;
            balls_center_position = m_vecBallsCenter;

            if (m_bFirstForBallsCenterMovement)
            {
                m_bFirstForBallsCenterMovement = false;
            }
            else
            {
                m_vecLastFrameBallsCenterMovement = balls_center_position - m_vecLastFrameBallsPostion;
            }
            m_vecLastFrameBallsPostion = balls_center_position;
        }
        Main.s_Instance.m_MainPlayer.SetCurLiveBallNum(nLiveBallNum);

        CalculateBallsTeamRadius(); // 计算整个队伍的逻辑半径
    }

	public Ball GetTheBiggistBall()
	{
		return m_lstMostBigBallsToShowName [0];
	}

    public Vector3 m_vecLastFrameBallsCenterMovement = new Vector3();
    public Vector3 m_vecLastFrameBallsPostion = new Vector3();
    bool m_bFirstForBallsCenterMovement = true;

    float m_fBallTeamRadius = 0;
    void CalculateBallsTeamRadius()
    {
        
        m_fBallTeamRadius = Vector2.Distance(balls_center_position, balls_min_position) ;

        float fDis2 = Vector2.Distance(balls_center_position, balls_max_position);
        if (fDis2  > m_fBallTeamRadius)
        {
            m_fBallTeamRadius = fDis2;
        }
        

        vecTempPos = balls_min_position;
        vecTempPos.y = balls_max_position.y;
        float fDis3 = Vector2.Distance(balls_center_position, vecTempPos);

        if (fDis3 > m_fBallTeamRadius)
        {
            m_fBallTeamRadius = fDis3;
        }

        vecTempPos = balls_max_position;
        vecTempPos.y = balls_min_position.y;
        float fDis4 = Vector2.Distance(balls_center_position, vecTempPos);

        if (fDis4 > m_fBallTeamRadius)
        {
            m_fBallTeamRadius = fDis4;
        }

    }

    // nAxis: 0 - X    1 - Y
    void SortMoveOrder( ref List<int> lst, List<Ball> lstBalls, int nAxis )
	{
		int nTemp = 0;
		for (int i = 0; i < lst.Count - 1; i++) {
			for (int j = i + 1; j < lst.Count; j++) {
				Ball ball1 = lstBalls [i];
				Ball ball2 = lstBalls [j];
				float val1 = (nAxis == 0?ball1.GetPos().x:ball1.GetPos().y);
				float val2 = (nAxis == 0?ball2.GetPos().x:ball2.GetPos().y);
				if (val1 > val2) {
					nTemp = lst [i];
					lst [i] = lst[j];
					lst [j] = nTemp;
				}
 			}
		}
	}
	List<int> m_lstMoveOrderX = new List<int>();
	List<int> m_lstMoveOrderY = new List<int>();
    

	bool IsMouseDown()
    {
        return Input.GetMouseButton(0) && (!UIManager.IsPointerOverUI());
         //   !this._global.GetComponent<GUIAction>().Intersect(Input.mousePosition);
    }


    public void UpdateCursorDirection()
    {
        if (this.IsMouseDown()) {
            if (this._release_joystick_keep_position) {
                //this._touch_start_time = Main.GetTime();
            }
            this._release_joystick_keep_position = false;
        }
        

        if (CtrlMode.GetCtrlMode() == CtrlMode.CTRL_MODE_HAND ||
            CtrlMode.GetCtrlMode() == CtrlMode.CTRL_MODE_SCREEN_CURSOR &&
            this._release_joystick_keep_position) {
            /*
            if (this.IsMouseDown()) {
				if (Main.s_Instance.m_nPlatform == 0) {
					this._touch_screen_position = Input.mousePosition;
				} else {
					Touch touch;
					if (Input.touchCount >= 1) {
							if (!Main.s_Instance.IsForceSpitting ()) {
								touch = Input.GetTouch (0);
							} else {
								if (Main.s_Instance.GetSpitFingerID () == 0) {
									touch = Input.GetTouch (1);
								} else {
									touch = Input.GetTouch (0);
								}
							}
							this._touch_screen_position = touch.position;
					 	}
	
				}
				
            }

            */
            if ( true/*this.IsMouseDown()*/  )
            {
                this._touch_screen_position = Input.mousePosition;
            }

            Vector3 position = Camera.main.ScreenToWorldPoint(this._touch_screen_position);
            this._touch_world_position.x = position.x;
            this._touch_world_position.y = position.y;
            Vector2 direction = this._touch_world_position - this._center.transform.position;
            direction.Normalize();
            this._cursor_direction = direction;
        }
        else if (CtrlMode.GetCtrlMode() == CtrlMode.CTRL_MODE_JOYSTICK ||
                 CtrlMode.GetCtrlMode() == CtrlMode.CTRL_MODE_WORLD_CURSOR ||
                 CtrlMode.GetCtrlMode() == CtrlMode.CTRL_MODE_SCREEN_CURSOR) {
            this._cursor_direction = this._joystick_movement.normalized;

        }
    }

    public void UpdateCenterPosition(Vector3 balls_center_position)
    {
        /*
        if (!Mathf.Approximately(this._center_move_time, PlayerAction.CENTER_FAST_MOVE_TIME)) {
            this._center_move_time =
                Mathf.SmoothDamp(this._center_move_time,
                                 PlayerAction.CENTER_FAST_MOVE_TIME,
                                 ref this._center_move_time_velocity,
                                 PlayerAction.CENTER_SLOW_MOVE_TIME);
        }
        */

        float fZoomTime = CtrlMode._zoom_time;
        this._center.transform.position = 
                    Vector3.SmoothDamp(this._center.transform.position,
                               balls_center_position,
                               ref this._center_move_velocity,
                               fZoomTime);


    }
    
    public void OnMoveStart()
    {
        // Debug.Log("OnMoveStart");
    }

    public void OnMove(Vector2 axis_vals)
    {
        this._joystick_movement = axis_vals;
    }
    
    public void OnMoveSpeed(Vector2 axis_speed_vals)
    {
        // Debug.Log("OnMoveSpeed: " + axis_speed_vals);
    }
    
    public void OnMoveEnd()
    {
       //  Debug.Log("OnMoveEnd");
    }

    public void OnTouchEnd()
    {

        this._release_joystick_keep_position = true;
        if (CtrlMode.GetCtrlMode() != CtrlMode.CTRL_MODE_JOYSTICK) {
            this._joystick_movement = Vector2.zero;
            this._cursor_realtime_velocity = 0.0f;
        }
        else {
            float now = Main.GetTime();
            float fDeltaTime = now - this._touch_start_time;
           // Debug.Log("fDeltaTime=" + fDeltaTime);

            if (fDeltaTime < CCheat.s_Instance._fClickToStopMoveInterval /*PlayerAction._touch_stop_interval*/ ) {
				//this._release_joystick_fast_stop = true;
			}
        }
    }

    public void OnDownUp()
    {
         Debug.Log("OnDownUp");        
    }
    
    public void OnDownDown()
    {
         Debug.Log("OnDownDown");
    }

    public void OnDownRight()
    {
         Debug.Log("OnDownRight");
    }

    public void OnDownLeft()
    {
         Debug.Log("OnDownLeft");
    }

    public void OnPressUp()
    {
         Debug.Log("OnPressUp");        
    }
    
    public void OnPressDown()
    {
         Debug.Log("OnPressDown");
    }

    public void OnPressRight()
    {
         Debug.Log("OnPressRight");
    }

    public void OnPressLeft()
    {
         Debug.Log("OnPressLeft");
    }    
};
