﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Xml;

public class CPveEditor : MonoBehaviour {

    public GameObject[] m_arySubPanel;

	static Vector3 vecTempPos = new Vector3();

	public static CPveEditor s_Instance;

	public Button _btnGrass;
	public Button _btnSpray;

	public InputField _inputSelectObjScaleX;
	public InputField _inputSelectObjScaleY;
	public InputField _inputSelectObjRotation;

	public Button _btnDelCurSelectedObj;

	CPveObj m_CurSelectedObj = null;

	void Awake()
	{
		s_Instance = this;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		ProcessMouseInput ();
		ProcessKeyInput ();
	}

	public void SelectObj( CPveObj obj  )
	{
		m_CurSelectedObj = obj;

		UpdateSelectObjUi ();
	}

	void UpdateSelectObjUi()
	{
		if (m_CurSelectedObj == null) {
			return;
		}
		_inputSelectObjScaleX.text = m_CurSelectedObj.GetScale ().x.ToString();
		_inputSelectObjScaleY.text = m_CurSelectedObj.GetScale ().y.ToString();
		_inputSelectObjRotation.text = m_CurSelectedObj.GetRotation ().ToString();
	}

	public CPveObj GetCurSelectedObj()
	{
		return m_CurSelectedObj;
	}

	public void CancelCurSelectObj()
	{
		m_CurSelectedObj = null;
	}

	// 判断当前是否点击在了UI上
	public bool IsPointerOverUI()
	{

		PointerEventData eventData = new PointerEventData(UnityEngine.EventSystems.EventSystem.current);
		eventData.pressPosition = Input.mousePosition;
		eventData.position = Input.mousePosition;

		List<RaycastResult> list = new List<RaycastResult>();
		UnityEngine.EventSystems.EventSystem.current.RaycastAll(eventData, list);

		return list.Count > 0;

	}

	void ProcessMouseInput()
	{
		if (IsPointerOverUI ()) { // 点在了界面UI上
			return;
		}

		ProcessDragObj ();
	}

	

	void ProcessDragObj()
	{
		if (AccountManager.m_eSceneMode == AccountManager.eSceneMode.Game)
		{
			return;
		}

		if (m_CurSelectedObj == null) {
			return;
		}

		if (!Input.GetMouseButton (0)) {
			return;
		}

		float fZ =  m_CurSelectedObj.GetPos ().z;
		vecTempPos = Camera.main.ScreenToWorldPoint (Input.mousePosition); 
		vecTempPos.z = fZ;
		m_CurSelectedObj.SetPos ( vecTempPos);
	}

	void ProcessKeyInput()
	{
		if ( Input.GetKeyDown( KeyCode.Escape ) )
		{
			CancelCurSelectObj();
		}

	}

	public void SavePve( XmlDocument xmlDoc, XmlNode nodePve )
	{
		XmlNode nodeGrass = xmlDoc.CreateElement("Grass");
		nodePve.AppendChild(nodeGrass);
		CGrassEditor.s_Instance.SaveGrass ( xmlDoc, nodeGrass );

		XmlNode nodeSpray = xmlDoc.CreateElement("Spray");
		nodePve.AppendChild(nodeSpray);
		CSpryEditor.s_Instance.SaveSpray ( xmlDoc, nodeSpray );
	}

	public void GeneratePve( XmlNode nodePve )
	{
		if (nodePve == null) {
			return;
		}

		XmlNode nodeGrass = nodePve.SelectSingleNode ("Grass");
		if (nodeGrass != null ) {
			CGrassEditor.s_Instance.GenerateGrass (nodeGrass);
		}

		XmlNode nodeSpray = nodePve.SelectSingleNode ("Spray");
		if (nodeSpray != null ) {
			CSpryEditor.s_Instance.GenerateSpray (nodeSpray);
		}

	}

	public void OnInputValueChanged_CurSelectedObjScaleX()
	{
		if (m_CurSelectedObj == null) {
			return;
		}

		float val = float.Parse ( _inputSelectObjScaleX.text );
		m_CurSelectedObj.SetScale (val, m_CurSelectedObj.GetScale().y);

	}

	public void OnInputValueChanged_CurSelectedObjScaleY()
	{
		if (m_CurSelectedObj == null) {
			return;
		}

		float val = float.Parse ( _inputSelectObjScaleY.text );
		m_CurSelectedObj.SetScale ( m_CurSelectedObj.GetScale().x, val );
	}

	public void OnInputValueChanged_CurSelectedObjRotation()
	{
		if (m_CurSelectedObj == null) {
			return;
		}
		float val = float.Parse ( _inputSelectObjRotation.text );
		m_CurSelectedObj.SetRotation ( val );
	}

	public void OnClickBtn_DelCurSelectedObj()
	{
		if (m_CurSelectedObj == null) {
			return;
		}

		GameObject.Destroy ( m_CurSelectedObj.gameObject );
		m_CurSelectedObj = null;
	}

    public void OnButtonClick_Grass()
    {
        for (int i = 0; i < m_arySubPanel.Length; i++)
        {
            m_arySubPanel[i].SetActive(false );
        }
        m_arySubPanel[0].SetActive(true);

    }

    public void OnButtonClick_Spray()
    {
        for (int i = 0; i < m_arySubPanel.Length; i++)
        {
            m_arySubPanel[i].SetActive(false);
        }
        m_arySubPanel[1].SetActive(true);
    }
}
