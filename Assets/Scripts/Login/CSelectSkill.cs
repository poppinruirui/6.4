﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Xml;
/*
    public enum eSkillId
    {
        q_spore = 0,
        w_spit,
        e_unfold, // 猥琐扩展
        r_sneak, // 潜行
        t_become_thorn, // 变刺
        y_annihilate, // 湮灭
        u_magicshield, // 魔盾
        i_merge,     // 秒合
        o_fenzy,     // 狂暴
        p_gold,      // 金
    };

*/
public class CSelectSkill : MonoBehaviour
{

    public static CSelectSkill s_Instance;

    public Text[] _aryTitle;
    public Text[] _aryValuesLevel0;
    public Text[] _aryValuesLevel1;
    public Text[] _aryValuesLevel2;
    public List<Text[]> m_lstValuesLevels = new List<Text[]>();

    public Button[] m_arySkillType;

    public Text _txtPlayerName;

    public GameObject _panelSkillDetail;
    public GameObject _panelSelectSkill;
    public GameObject _panelSelectGameModePage;

    public Button _btnQianXing;
    public Button _btnBianCi;
    public Button _btnYanMie;
    public Button _btnMoDun;
    public Button _btnMiaoHe;
    public Button _btnKuangBao;
    public Button _btnJinKe;

    public Text _txtSkillName;
    public Text _txtSkillDesc;
    public Button _btnPlayGame;
    public Button _btnReturnLastPage;

    public Image _imgSkillIcon;

    CSkillSystem.eSkillId m_eCurSelectSkillId = CSkillSystem.eSkillId.i_merge;

    public CSkillDetailGrid[] m_arySkillDetailGrid;
    public Sprite[] m_arySkillIconSprite;
    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start()
    {
       //

        m_lstValuesLevels.Add(_aryValuesLevel0);
        m_lstValuesLevels.Add(_aryValuesLevel1);
        m_lstValuesLevels.Add(_aryValuesLevel2);

        UpdateSkillButtonsColor(0);
    }

    // Update is called once per frame
    void Update()
    {
        SelectDefaultSkill();
    }

    bool m_bDefaultSkillSelected = false;
    void SelectDefaultSkill()
    {
        if (m_bDefaultSkillSelected)
        {
            return;
        }

        if ( !m_bMapParsed )
        {
            return;
        }

        m_eCurSelectSkillId = CSkillSystem.eSkillId.i_merge;
        EnterSkillDetailPage();
        UpdateSkillButtonsColor((int)m_eCurSelectSkillId);
        m_bDefaultSkillSelected = true;
    }

    public CSkillSystem.eSkillId GetCurSelectSkill()
    {
        return m_eCurSelectSkillId;
    }

    public void OnClickButtonSkill_QianXing()
    {
        m_eCurSelectSkillId = CSkillSystem.eSkillId.r_sneak;
        EnterSkillDetailPage();
    }

    public void OnClickButtonSkill_BianCi()
    {
        m_eCurSelectSkillId = CSkillSystem.eSkillId.t_become_thorn;
        EnterSkillDetailPage();
    }

    public void OnClickButtonSkill_YanMie()
    {
        m_eCurSelectSkillId = CSkillSystem.eSkillId.y_annihilate;
        EnterSkillDetailPage();
    }

    public void OnClickButtonSkill_MoDun()
    {
        m_eCurSelectSkillId = CSkillSystem.eSkillId.u_magicshield;
        EnterSkillDetailPage();
    }

    public void OnClickButtonSkill_MiaoHe()
    {
        m_eCurSelectSkillId = CSkillSystem.eSkillId.i_merge;
        EnterSkillDetailPage();
    }

    public void OnClickButtonSkill_KuangBao()
    {
        m_eCurSelectSkillId = CSkillSystem.eSkillId.o_fenzy;
        EnterSkillDetailPage();
    }

    public void OnClickButtonSkill_JinKe()
    {
        m_eCurSelectSkillId = CSkillSystem.eSkillId.p_gold;
        EnterSkillDetailPage();
    }

    void EnterSkillDetailPage()
    {
       // _panelSelectSkill.SetActive(false);
        _panelSkillDetail.SetActive(true);
		CAudioManager.s_Instance.PlayAudio ( CAudioManager.eAudioId.e_audio_flip_page );

        _imgSkillIcon.sprite = m_arySkillIconSprite[(int)m_eCurSelectSkillId];

        string szSkillName = "";
        string szSkillDetail = "";
        switch (m_eCurSelectSkillId)
        {
            case CSkillSystem.eSkillId.r_sneak:
                {
                    szSkillName = "潜行";
                    szSkillDetail = "敌方无法吃你，移动速度加快";
                }
                break;
            case CSkillSystem.eSkillId.t_become_thorn:
                {
                    szSkillName = "变刺";
                    szSkillDetail = "自己身体变成刺，地方2吃了你会炸";
                }
                break;
            case CSkillSystem.eSkillId.y_annihilate:
                {
                    szSkillName = "湮灭";
                    szSkillDetail = "敌方吃了你，不但不增加体积，反而减少体积";
                }
                break;
            case CSkillSystem.eSkillId.u_magicshield:
                {
                    szSkillName = "魔法盾";
                    szSkillDetail = "吃了刺不会炸";
                }
                break;
            case CSkillSystem.eSkillId.i_merge:
                {
                    szSkillName = "秒合";
                    szSkillDetail = "队伍中所有球秒合到最大那个大球身上";
                }
                break;
            case CSkillSystem.eSkillId.o_fenzy:
                {
                    szSkillName = "狂暴";
                    szSkillDetail = "各种无CD，移动速度加快";
                }
                break;
            case CSkillSystem.eSkillId.p_gold:
                {
                    szSkillName = "金属壳";
                    szSkillDetail = "敌方的壳和自己的壳会碰撞";
                }
                break;

        } // end switch

        _txtSkillName.text = szSkillName;
        _txtSkillDesc.text = m_dicSkillDesc[(int)m_eCurSelectSkillId];

        Dictionary<int, sSkillDetail> dic = m_arySkillDetail[(int)m_eCurSelectSkillId];
        for ( int i = 0; i < CSkillSystem.SELECT_SKILL_MAX_POINT; i++ )
        {
            m_arySkillDetailGrid[i].ParseData(dic, i + 1, (int)m_eCurSelectSkillId);
        }
                
    }

    public void OnClickButton_ReturnToLastPage()
    {
		CAudioManager.s_Instance.PlayAudio ( CAudioManager.eAudioId.e_audio_flip_page );
        _panelSelectSkill.SetActive(true);
        _panelSkillDetail.SetActive(false);
    }

    public void OnClickButton_ReturnToSelectGameMdoePage()
    {
		CAudioManager.s_Instance.PlayAudio ( CAudioManager.eAudioId.e_audio_flip_page );
        _panelSelectSkill.SetActive(false);
        _panelSelectGameModePage.SetActive(true);
    }

    public void LoadMap(string szRoomName)
    {
        // poppin test

        szRoomName = "hgr";
		if (CSelectRoomManager.s_bEnterTestRoom) {
			szRoomName = "test3";
		}
        string szFileName = AccountManager.url + AccountManager.GetPrefix(1) + szRoomName + ".xml";
        StartCoroutine(ParseMap(szFileName));
    }

    
    public struct sSkillDetail
    {
        public float fSpeedAffect;
        public float fMpCost;
        public float fDuration;
        public float fColdDown;

        public string[] aryValues;
    };

    bool m_bMapParsed = false;
    Dictionary<int, sSkillDetail>[] m_arySkillDetail = new Dictionary<int, sSkillDetail>[10]; // dictionary的key是技能等级
    IEnumerator ParseMap(string szFileName)
    {
        // myXmlDoc.Load(szFileName);
        WWW www = new WWW(szFileName);
        yield return www; // 等待下载
        XmlNode root = null;

        XmlDocument myXmlDoc = StringManager.CreateXmlByText(www.text, ref root);
        XmlNode nodeSkillDesc = root.SelectSingleNode("SkillDesc");
        ParseSkillDesc(nodeSkillDesc);
        XmlNode nodeSkill = root.SelectSingleNode("Skill");

        for (int i = 3; i < nodeSkill.ChildNodes.Count; i++) // 遍历每一种技能. i就是SkillId
        {
            Dictionary<int, sSkillDetail> dic = new Dictionary<int, sSkillDetail>();

            string[] aryValues = nodeSkill.ChildNodes[i].InnerText.Split( ',' );
            for (int j = 0; j < 3; j++) // 遍历这个技能的每一个等级
            {
                int k = CSkillSystem.SKILL_PARAM_TOTAL_NUM * j;
                sSkillDetail info = new sSkillDetail();

                info.aryValues = new string[CSkillSystem.SKILL_PARAM_TOTAL_NUM];
                info.aryValues[0] = aryValues[k];
                info.aryValues[1] = aryValues[1 + k];
                info.aryValues[2] = aryValues[2 + k];
                info.aryValues[3] = aryValues[3 + k];
                info.aryValues[4] = aryValues[4 + k];
                info.aryValues[5] = aryValues[5 + k];

                /*
                info.fMpCost = 0f;
                float.TryParse(aryValues[0 + k], out info.fMpCost);

                info.fDuration = 0f;
                string szDuration = "0";
                string szSpeedAffect = "0";
                string szColdDown = aryValues[1 + k];
                switch (i)
                {
                    case 3:
                        {
                            szDuration = aryValues[3 + k];
                            szSpeedAffect = aryValues[2 + k];
                        }
                        break;
                    case 4:
                        {
                            szDuration = aryValues[4 + k];
                            szSpeedAffect = aryValues[3 + k];
                        }
                        break;
                    case 5:
                        {
                            szDuration = aryValues[3 + k];
                            szSpeedAffect = aryValues[4 + k];
                        }
                        break;
                    case 6:
                        {
                            szDuration = aryValues[2 + k];
                            szSpeedAffect = aryValues[3 + k];
                        }
                        break;
                    case 7:
                        {

                        }
                        break;
                    case 8:
                        {
                            szDuration = aryValues[5 + k];
                            szSpeedAffect = aryValues[2 + k];
                        }
                        break;
                    case 9:
                        {
                            szDuration = aryValues[2 + k];
                            szSpeedAffect = aryValues[3 + k];
                        }
                        break;
                } // end switch

                float.TryParse(szDuration, out info.fDuration);
                float.TryParse(szColdDown, out info.fColdDown);
                float.TryParse(szSpeedAffect, out info.fSpeedAffect);
                */
                dic[j+1] = info; // j+1 就是当前等级
            } // end for j

            m_arySkillDetail[i] = dic;
        } // end for i


        XmlNode nodeCommon = root.SelectSingleNode("common");
        XmlNode nodePlayerNumToStart = nodeCommon.SelectSingleNode("PlayerNumToStartGame");
        /*
        for ( int i = 0; i < nodeCommon.ChildNodes.Count; i++ )
        {
            if (nodeCommon.ChildNodes[i].Name == "PlayerNumToStartGame")
            {
                nodePlayerNumToStart = nodeCommon.ChildNodes[i];
                break;
            }
        }
        */
        if (nodePlayerNumToStart != null)
        {
            int nPlayerNumToStart = 1;
            if ( int.TryParse(nodePlayerNumToStart.InnerText, out nPlayerNumToStart) )
            {
                MapEditor.SetPlayerNumToStartGame(nPlayerNumToStart);
            }
        }
        

        m_bMapParsed = true;
    }

    Dictionary<int, string> m_dicSkillDesc = new Dictionary<int, string>();
    void ParseSkillDesc( XmlNode node )
    {
        if (node == null)
        {
            return;
        }


        for (int i = 0; i < node.ChildNodes.Count; i++)
        {
            int nSkillId = i + CSkillSystem.SELECTABLE_SKILL_START_ID;
            m_dicSkillDesc[nSkillId] = node.ChildNodes[i].InnerText;
        }
    }

    static Color colorSkillButtonLight = new Color(1, 1, 1, 1f);
    static Color colorSkillButtonDark = new Color(1, 1, 1, 0.5f);
    void UpdateSkillButtonsColor( int nSkillId )
    {
        for ( int i = 0;i < m_arySkillType.Length; i++ )
        {
            Button btn = m_arySkillType[i];
            if (btn == null )
            {
                continue;
            }
            if (i == nSkillId)
            {
                btn.gameObject.GetComponent<Image>().color = colorSkillButtonLight;
            }
            else
            {
                btn.gameObject.GetComponent<Image>().color = colorSkillButtonDark;
            }
        }
    }

    public void OnClickButton_3()
    {
        int nSkillId = 3;
        UpdateSkillButtonsColor( nSkillId );
        m_eCurSelectSkillId = (CSkillSystem.eSkillId)nSkillId;
        EnterSkillDetailPage();
    }

    public void OnClickButton_4()
    {
        int nSkillId = 4;
        UpdateSkillButtonsColor(nSkillId);
        m_eCurSelectSkillId = (CSkillSystem.eSkillId)nSkillId;
        EnterSkillDetailPage();
    }

    public void OnClickButton_5()
    {
        int nSkillId = 5;
        UpdateSkillButtonsColor(nSkillId);
        m_eCurSelectSkillId = (CSkillSystem.eSkillId)nSkillId;
        EnterSkillDetailPage();
    }

    public void OnClickButton_6()
    {
        int nSkillId = 6;
        UpdateSkillButtonsColor(nSkillId);
        m_eCurSelectSkillId = (CSkillSystem.eSkillId)nSkillId;
        EnterSkillDetailPage();
    }

    public void OnClickButton_7()
    {
        int nSkillId = 7;
        UpdateSkillButtonsColor(nSkillId);
        m_eCurSelectSkillId = (CSkillSystem.eSkillId)nSkillId;
        EnterSkillDetailPage();
    }

    public void OnClickButton_8()
    {
        int nSkillId = 8;
        UpdateSkillButtonsColor(nSkillId);
        m_eCurSelectSkillId = (CSkillSystem.eSkillId)nSkillId;
        EnterSkillDetailPage();
    }

    public void OnClickButton_9()
    {
        int nSkillId = 9;
        UpdateSkillButtonsColor(nSkillId);
        m_eCurSelectSkillId = (CSkillSystem.eSkillId)nSkillId;
        EnterSkillDetailPage();
    }

    public void OnClickButton_PrevPage()
    {
        AccountManager.s_Instance._panelSkill.SetActive( false );
        //  AccountManager.s_Instance. _panelPlayerName.SetActive(true);
        AccountManager.s_Instance._panelMainLognIn.SetActive(true);
    }

}