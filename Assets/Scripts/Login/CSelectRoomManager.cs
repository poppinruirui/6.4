﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CSelectRoomManager : MonoBehaviour
{
    public static CSelectRoomManager s_Instance = null;

    public GameObject _panelSelectRoom;
    public CCommonJinDuTiao _jindutiao;

    public Toggle _toggleEnterTestRoom;

    public CRoom[] m_aryRooms;
    public Sprite[] m_aryRoomAvatar;

    public Sprite m_sprNotSelected;
    public Sprite m_sprSelected;

    /// <summary>
    /// UI
    /// </summary>
    public Text _txtCurSelectedRoomName;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
		CSelectSkill.s_Instance.LoadMap ("");

        // 初始化房间
        for ( int i = 0; i < m_aryRooms.Length; i++ )
        {
            CRoom room = m_aryRooms[i];
            room.SetAvatar(m_aryRoomAvatar[i]);
            room.SetRoomName( "测试房间" + (i+1) );
            room.SetRoomIndex( i );
        }

       
    }

    CRoom m_CurSelectedRoom = null;
    public void SelectRoom( CRoom room )
    {
        if (m_CurSelectedRoom != null)
        {
            m_CurSelectedRoom.SetSelected( false );
        }
        m_CurSelectedRoom = room;
        m_CurSelectedRoom.SetSelected(true);
        _txtCurSelectedRoomName.text = m_CurSelectedRoom.GetRoomName();
    }
    
        // Update is called once per frame
    void Update () {
       
    }

    public void EnterRoom( int nRoomIndex )
    {
        AccountManager.s_Instance.PlaySelectedRoom_Test(nRoomIndex);
        ShowJinDuTiao();
    }

    public void OnClickButton_0()
    {
        AccountManager.s_Instance.PlaySelectedRoom_Test( 0 );
        ShowJinDuTiao();
    }

    public void OnClickButton_1()
    {
        AccountManager.s_Instance.PlaySelectedRoom_Test(1);
        ShowJinDuTiao();
    }


    public void OnClickButton_2()
    {
        AccountManager.s_Instance.PlaySelectedRoom_Test(2);
        ShowJinDuTiao();
    }

    public void ShowJinDuTiao()
    {
        _jindutiao.gameObject.SetActive( true );
        _jindutiao.SetInfo( "正在登入游戏" );
    }

    public void OnClickButton_PrevPage()
    {
        SetSelectRoomPanelVisible(false);

    }

    public void OnClickButton_EnterSelectRoomPanel()
    {
        SetSelectRoomPanelVisible( true );
    }

    public void SetSelectRoomPanelVisible( bool bVisible )
    {
        _panelSelectRoom.SetActive(bVisible);
    }

    public static bool s_bEnterTestRoom = false;
    public void OnToggleValueChanged_EnterTestRoom()
    {
        s_bEnterTestRoom = _toggleEnterTestRoom.isOn;
		CSelectSkill.s_Instance.LoadMap ("");
    }
}
