﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Shapes2D;

public class CRebornSpot : MonoBehaviour {

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();
    static Color tempColor = new Color();

    public Shape _shapeInner;
    public Shape _shapeOuuter;

    public float m_fAlphaSpeed = 1f;

    float m_fTotalTime = 0f;
    float m_fTimeCounting = 0f;

    float m_fBlingSpeed = 1f;

    public TextMesh _txtLeftTime;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        

    }

    private void FixedUpdate()
    {
       // AlphaLoop();
        TimeCounting();
    }

    public void SetPos(Vector3 pos)
    {
        this.transform.position = pos;
    }

    public void SetScale(float scale )
    {
        vecTempScale.x = scale;
        vecTempScale.y = scale;
        vecTempScale.z = 1f;
        this.transform.localScale = vecTempScale;
    }

    public void SetBlingSpeed( float fBlingSpeed )
    {
        m_fBlingSpeed = fBlingSpeed;
        m_fAlpha = m_fBlingSpeed;
    }

    float m_fAlpha = 1f;
    int m_nAlphaStatus = 0;
    void AlphaLoop()
    {
        if (m_nAlphaStatus == 0)
        {
            m_fAlpha -= Time.fixedDeltaTime * m_fAlphaSpeed;
            tempColor = _shapeOuuter.settings.fillColor;
            tempColor.a = m_fAlpha;
            _shapeOuuter.settings.fillColor = tempColor;
            _shapeOuuter.settings.outlineColor = tempColor;
            _shapeInner.settings.fillColor = tempColor;

            if (m_fAlpha <= 0)
            {
                m_fAlpha = 0;
                m_nAlphaStatus = 1;
            }

        }
        else if (m_nAlphaStatus == 1)
        {
            m_fAlpha += Time.fixedDeltaTime * m_fAlphaSpeed;

           
            tempColor = _shapeOuuter.settings.fillColor;
            tempColor.a = m_fAlpha;
            _shapeOuuter.settings.fillColor = tempColor;
            _shapeOuuter.settings.outlineColor = tempColor;
            _shapeInner.settings.fillColor = tempColor;

            if (m_fAlpha >= 1)
            {
                m_fAlpha = 1;
                m_nAlphaStatus = 0;
            }
        }
    }

    public void BeginTimeCount( float fTotalTime )
    {
        m_fTotalTime = fTotalTime;
        m_fTimeCounting = 0f;
        _shapeInner.settings.endAngle = 0.1f;
    }

    void TimeCounting()
    {
        m_fTimeCounting += Time.fixedDeltaTime;
        if (m_fTimeCounting >= m_fTotalTime)
        {
            EndTimeCount();
        }

        _shapeInner.settings.endAngle = 360f * m_fTimeCounting / m_fTotalTime;

        float fLeftTime = m_fTotalTime - m_fTimeCounting;
        if (fLeftTime < 0f)
        {
            _txtLeftTime.text = "";
        }
        else
        {
            if (fLeftTime < 1f)
            {
                _txtLeftTime.text = fLeftTime.ToString("f1");
            }
            else
            {
                _txtLeftTime.text = fLeftTime.ToString("f0");
            }
        }
    }

    void EndTimeCount()
    {
        
    }
}
