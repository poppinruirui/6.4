﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using WebAuthAPI;
using Spine.Unity;

public class ShoppingMallManager : MonoBehaviour {

    public float m_fItemCounterInitScale = 1f;
    public float m_fItemCounterSelectedScale = 1.1f;


    public CCyberTreeList m_clItems;

    public GameObject m_preShoppingCounter;

    public Sprite m_sprNotEquiped; // 未装备（无论是否已购买都一样）
    public Sprite m_sprEquiped;    // 已装备

    public Sprite m_sprMoneyBg_NotBought;
    public Sprite m_sprMoneyBg_Bought;
    public float m_fMoneyBgWidth_NotBought;
    public float m_fMoneyBgWidth_Bought;
    public float m_fMoneyBgHeight = 68f;

    public Sprite[] m_aryMoneyTypeSpr;


    public Color m_colorNotBought;
    public Color m_colorBought;


    public Sprite[] m_aryBuyButtonStatus;

    /// <summary>
    /// /Page的资源
    /// </summary>
    public string[] m_aryPageTitle; // 标题
    public Sprite m_sprPageSelected;
    public Sprite m_sprPageNotSelected;
    public Color m_colorPageSelected_TextContent;
    public Color m_colorPageNotSelected_TextContent;
    public Color m_colorPageSelected_TextOutline;
    public Color m_colorPageNotSelected_TextOutline;
    public Image[] m_aryPageButtons;
    public Text[] m_aryPageTitleText;
    public Outline8[] m_aryPageTitleOutline;

    public string m_szExpireInfo = "还有30天过期";

    public int m_nFontSize_NotBought = 40;
    public int m_nFontSize_Bought = 33;
    ////


    public Text m_txtXiaoLianBiNum;
    public Text m_txtMieBiNum;

    public GameObject _panelMall;

    public CRechargeCounter[] m_aryRechargeCounters;


    /// <summary>
    /// / confirm ui
    /// </summary>
    public Button _btnConfirmBuy;
    public Button _btnCancelBuy;
    public GameObject _panelConfirmBuyUI;
    public Text _txtItemName;
    public Text _txtCost;
    public Image _imgMoneyType;
    public Image _imgConfirmBuyAvatar;
    //// end confirm ui

    /// <summary>
    /// 充值界面
    public CBuyConfirmUI _RechargeConfirmUI; 
    
    /// </summary>
    public GameObject _panelRecharge;

    public CyberTreeScrollView _svItemList;

    public enum eMoneyType
    {
        mengbi,
        xiaolianbi,
    };

    public enum eShoppingMallPageType
    {
        putongpifu,
        yishujiapifu,
    };

    eShoppingMallPageType m_eCurPage = eShoppingMallPageType.putongpifu;

    Dictionary<eShoppingMallPageType, List<CShoppingCounter>> m_dicItemId = new Dictionary<eShoppingMallPageType, List<CShoppingCounter>>();

    public static ShoppingMallManager s_Instance;


    static int m_sCurEquipedSkinId = AccountData.INVALID_SKIN_ID;
    static string s_szCurEquipSkinPath = "";
    CShoppingCounter m_CurBuyingCounter = null;
    CShoppingCounter m_CurEquipedCounter = null;

    public static int GetCurEquipedSkinId()
    {
        return m_sCurEquipedSkinId;
    }

    public static void SetCurEquippedSkinId( int nId )
    {
        m_sCurEquipedSkinId = nId;
    }

    public static string GetCurEqupiedSkinPath()
    {
        return AccountData.GenerateSkinPathById(m_sCurEquipedSkinId);
    }

    private void Awake()
    {
        s_Instance = this;
       
    }

    public SkeletonGraphic m_skeletonGraphic;

    // Use this for initialization
    void Start() {

        AccountData.SkinJSON[] lstItems = null;
        AccountData.s_Instance.GetItemList(ref lstItems );

        List<CShoppingCounter> lstPuTongPiFu = new List<CShoppingCounter>();
        for ( int i = 0; i < lstItems.Length; i++ )
        {
            AccountData.SkinJSON item = lstItems[i];
            CShoppingCounter counter = GameObject.Instantiate(m_preShoppingCounter).GetComponent<CShoppingCounter>();
            counter.SetItemName(item.name);
            counter.SetItemDesc( item.description );
            if ( item.costcoins > 0 )
            {
                
                counter.SetMoneyType(eMoneyType.xiaolianbi);
                counter.SetMoneyValue(item.costcoins);
            }
            else
            {
                counter.SetMoneyType(eMoneyType.mengbi);
                counter.SetMoneyValue(item.costdiamonds);
            }
            counter.SetItemId( item.id );
            counter.SetSprite(AccountData.s_Instance.GetSpriteByItemPath( item.path ) );
            counter.SetItemPath(item.path);

            UpdateCounterStatus(counter);

            lstPuTongPiFu.Add(counter);
        }
        m_dicItemId[eShoppingMallPageType.putongpifu] = lstPuTongPiFu;


        UpdateShowList();
        UpdateChargeList();
        UpdateCoinNum();

        PrepareForLoadSkeleton();
    }

    public void UpdateCounterStatus(CShoppingCounter counter = null)
    {
        if (counter == null)
        {
            counter = m_CurBuyingCounter;
        }
        if (counter == null)
        {
            Debug.LogError("counter == null");
            return;
        }

        int nItemId = counter.GetItemId();

        if (AccountData.s_Instance.CheckIfBought(nItemId)) // 已购买
        {
            if ( GetCurEquipedSkinId() == nItemId)
            {
                counter.SetItemStatus(CShoppingCounter.eItemStatus.equipped);
                m_CurEquipedCounter = counter;
            }
            else
            {
                counter.SetItemStatus(CShoppingCounter.eItemStatus.equip);
            }
        }
        else // 未购买
        {
            counter.SetItemStatus(CShoppingCounter.eItemStatus.buy);
        }
    }

    public CShoppingCounter GetCurBuyingCounter()
    {
        return m_CurBuyingCounter;
    }

    public void UpdateCoinNum()
    {
        if (m_txtMieBiNum != null)
        {
            m_txtMieBiNum.text = AccountData.s_Instance.GetMoneyNum(AccountData.eMoneyType.mengbi).ToString();
        }
        m_txtXiaoLianBiNum.text = AccountData.s_Instance.GetMoneyNum(AccountData.eMoneyType.xiaolianbi).ToString();
    }

    void UpdateChargeList()
    {
        return;

        ChargeProductJSON[] lstChargeProducts = null;
        AccountData.s_Instance.GetChargeProductList( ref lstChargeProducts);
        for ( int i = 0; i < lstChargeProducts.Length; i++ )
        {
            if ( i >= m_aryRechargeCounters.Length )
            {
                return;
            }
            ChargeProductJSON product = lstChargeProducts[i];
            CRechargeCounter counter = m_aryRechargeCounters[i];
            counter.SetGainMoneyNum( product.earndiamonds );
            counter.SetChargeProductId( product.productguid );
        }
    }

    void UpdateShowList()
    {
        /*
        for (int i = 0; i < m_aryPageButtonBg.Length; i++)
        {
            if ((int)m_eCurPage == i)
            {
                m_aryPageButtonBg[i].sprite = m_sprPageSelected;
                m_aryPageButtonName[i].sprite = m_aryPageNameSpr_Selected[i];
            }
            else
            {
                m_aryPageButtonBg[i].sprite = m_sprPageNotSelected;
                m_aryPageButtonName[i].sprite = m_aryPageNameSpr_NotSelected[i];
            }
        }
        */

        for ( int i = 0; i < m_aryPageButtons.Length; i++ )
        {
            if ((int)m_eCurPage == i) // 选中此页
            {
                m_aryPageButtons[i].sprite = m_sprPageSelected;
                m_aryPageTitleText[i].color = m_colorPageSelected_TextContent;
                m_aryPageTitleOutline[i].effectColor = m_colorPageSelected_TextOutline;
            }
            else // 未选中此页
            {
                m_aryPageButtons[i].sprite = m_sprPageNotSelected;
                m_aryPageTitleText[i].color = m_colorPageNotSelected_TextContent;
                m_aryPageTitleOutline[i].effectColor = m_colorPageNotSelected_TextOutline;
            }
        }

        //m_clItems.ClearItems();
        _svItemList.ClearItems();
        List<CShoppingCounter> lst = null;
        if (m_dicItemId.TryGetValue(m_eCurPage, out lst))
        {
            for (int i = 0; i < lst.Count; i++)
            {
                lst[i].SetIndex( i );
                lst[i].gameObject.SetActive( true );
                _svItemList.AddItem(lst[i].gameObject);
            }
        }
        m_skeletonGraphic.gameObject.SetActive(false);

    }

    // Update is called once per frame
    void Update() {
        TapToExit();

        BuyResultLoop();
    }

    bool m_bRecharging = true;
    void TapToExit()
    {
        if (!m_bRecharging)
        {
            return;
        }

        if (Input.GetMouseButtonDown(0) && !UIManager.IsPointerOverUI() )
        {
            _panelRecharge.SetActive(false);
            _panelMall.SetActive(true);
        }
    }

    public void OnClickButton_Page_0()
    {
        int nPageIndex = 0;
        m_eCurPage = (eShoppingMallPageType)nPageIndex;

        UpdateShowList();
    }

    public void OnClickButton_Page_1()
    {
        int nPageIndex = 1;
        m_eCurPage = (eShoppingMallPageType)nPageIndex;
        UpdateShowList();
    }


    public void OnClickButton_Page_2()
    {
        int nPageIndex = 2;
        m_eCurPage = (eShoppingMallPageType)nPageIndex;

        UpdateShowList();
    }


    public void ShowConfirmBuyPanel(CShoppingCounter counter)
    {
        _txtCost.text = counter.GetMoneyValue().ToString();
        _imgMoneyType.sprite = m_aryMoneyTypeSpr[(int)counter.GetMoneyType()];
        _txtItemName.text = counter.GetItemName();
        _panelConfirmBuyUI.SetActive(true);
        _imgConfirmBuyAvatar.sprite = counter.GetSprite();
        m_CurBuyingCounter = counter;
    }

    public void OnClickButton_ConfirmBuy()
    {
        AccountData.s_Instance.Buy(m_CurBuyingCounter.GetItemId(), (int)m_CurBuyingCounter.GetMoneyType(), m_CurBuyingCounter.GetMoneyValue());
    //    _panelConfirmBuyUI.SetActive(false);
    }

    float m_fBuyResultTimeCount = 0f;
    public void BeginBuyResult( string szResult )
    {
        CBuyResult.s_Instance.Show(szResult);
        m_fBuyResultTimeCount = 1f;
    }

    void BuyResultLoop()
    {
        if (m_fBuyResultTimeCount <= 0)
        {
            return;
        }

        m_fBuyResultTimeCount -= Time.deltaTime;
        if (m_fBuyResultTimeCount <= 0)
        {
            EndBuyResult();
        }
    }

    void EndBuyResult()
    {
        CBuyResult.s_Instance.Hide();
        _panelConfirmBuyUI.SetActive(false);
    }

    public void OnClickButton_CancelBuy()
    {
        _panelConfirmBuyUI.SetActive(false);
    }
    
    public void DoEquip(CShoppingCounter counter)
    {
        counter.SetItemStatus(CShoppingCounter.eItemStatus.equipped);
        m_sCurEquipedSkinId = counter.GetItemId();
        s_szCurEquipSkinPath = counter.GetItemPath();
        if (m_CurEquipedCounter != null)
        {
            m_CurEquipedCounter.SetItemStatus(CShoppingCounter.eItemStatus.equip);
        }
        m_CurEquipedCounter = counter;
        AccountData.s_Instance.SaveEquippedSkinInfo();
    }

    public void OnClickButton_BackToLoginPage()
    {
        StartCoroutine("LoadScene", "SelectCaster");
        
    }

    //异步加载场景  
    IEnumerator LoadScene(string scene_name)
    {
        AccountData.s_Instance.asyncLoad = SceneManager.LoadSceneAsync(scene_name);

        while (!AccountData.s_Instance.asyncLoad.isDone)
        {
            yield return null;
        }

       
    }
    

    public void OnClickButton_EnterRecahrgePage()
    {
        _panelRecharge.SetActive( true );
        _panelMall.SetActive(false); 
    }

    public void OnClickButton_CancelRecharge()
    {
        ShoppingMallManager.s_Instance._RechargeConfirmUI.gameObject.SetActive(false);
    }

    public void OnClickButton_ConfirmRecharge()
    {
        //  AccountManager.AddMoney( ShoppingMallManager.eMoneyType.mengbi,  CBuyConfirmUI.s_Instance.m_nGainNum );
        // ShoppingMallManager.s_Instance._RechargeConfirmUI.gameObject.SetActive(false);
        AccountData.s_Instance.DoCharge(CBuyConfirmUI.s_Instance.GetProductId());
        ShoppingMallManager.s_Instance._RechargeConfirmUI.gameObject.SetActive(false);
    }

    public CShoppingCounter m_CurSelectedCounter = null;

    public GameObject m_mutiskeletonGraphic;

    Dictionary<int, Material> m_dicMaterialForSkeleton = new Dictionary<int, Material>();
    public Material CreateMaterialForSkeleton(int nSkinId)
    {
        Material mat = null;
        if (m_dicMaterialForSkeleton.TryGetValue(nSkinId, out mat))
        {
            return mat;
        }
        Sprite spr = AccountData.s_Instance.GetSpriteByItemId(nSkinId);
        mat = new Material(Shader.Find("Spine/Skeleton"));
        if ( spr == null )
        {
            spr = m_sprNoSkin;
        }
        mat.SetTexture("_MainTex", spr.texture);
        m_dicMaterialForSkeleton[nSkinId] = mat;

        return mat;
    }

    Dictionary<int, SkeletonGraphic> m_dicSkeletonForSkin = new Dictionary<int, SkeletonGraphic>();
    public SkeletonGraphic CombineSkeletonGraphicToSkin( SkeletonGraphic skeletonGraphic, int nSkinId)
    {
        Material mat = CreateMaterialForSkeleton(nSkinId);
        skeletonGraphic.skeletonDataAsset.atlasAssets[0].materials[0] = mat;
        m_dicSkeletonForSkin[nSkinId] = skeletonGraphic;
        return skeletonGraphic;
    }

    public Sprite m_sprNoSkin;

    public void PrepareForLoadSkeleton()
    {
        CreateMaterialForSkeleton(-1);
    }
}
