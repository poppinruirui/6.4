﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMergeSelfTrigger : MonoBehaviour
{

    public Ball _ball;
    public Collider2D _TriggerSelf;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        //     Debug.Log("Self 我日你个龟: ");

        if (other.tag != "MouthSelf")
        {
            // Debug.Log("ignore222");
            Physics2D.IgnoreCollision(_TriggerSelf, other);
            return;
        }
        else
        {
            Ball ballOther = other.transform.parent.gameObject.GetComponent<Ball>();
            if (!Ball.IsTeammate(_ball, ballOther))
            {
                //  Debug.Log("ignore333");
                Physics2D.IgnoreCollision(_TriggerSelf, ballOther._TriggerMergeSelf);
                return;
            }

        }


        // 自家的两个球球PK流程（合球）
        if (this._ball._Player.IsMainPlayer() && other.transform.gameObject.tag == "MouthSelf")
        {
            Ball ballOpponent = other.transform.parent.gameObject.GetComponent<Ball>();
            if (Ball.IsTeammate(_ball, ballOpponent)) // 是自己的球
            {
                // Debug.Log( "self merge" );

                this._ball.AddToPKList(ballOpponent);

                //// 不应碰撞的壳之间建立“互相忽略”关系
                // 作一些碰撞忽略操作
                Physics2D.IgnoreCollision(this._ball._ColliderDust, ballOpponent._ColliderDust);
                Physics2D.IgnoreCollision(this._ball._ColliderDust, ballOpponent._Collider);
                Physics2D.IgnoreCollision(this._ball._ColliderDust, ballOpponent._ColliderTemp);
                Physics2D.IgnoreCollision(ballOpponent._ColliderDust, this._ball._ColliderDust);
                Physics2D.IgnoreCollision(ballOpponent._ColliderDust, this._ball._Collider);
                Physics2D.IgnoreCollision(ballOpponent._ColliderDust, this._ball._ColliderTemp);


                // 金壳（金壳不作用于自家的球）
                Physics2D.IgnoreCollision(this._ball._ColliderTemp, ballOpponent._ColliderTemp);
            }
            else // 非自己的球
            {
                Physics2D.IgnoreCollision(this._ball._Collider, ballOpponent._Collider);
            }
        }

    }

    void OnTriggerExit2D(Collider2D other)
    {
        // 两个球球PK
        if (this._ball._Player.IsMainPlayer() && other.transform.gameObject.tag == "MouthSelf")
        {
            Ball ballOpponent = other.transform.parent.gameObject.GetComponent<Ball>();
            if (ballOpponent)
            {
                this._ball.RemoveFromPKList(ballOpponent);
            }
        }
    }
}
