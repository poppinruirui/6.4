﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Xml;

public class CControl : MonoBehaviour {

    public static CControl s_Instance;

    public InputField _inputBallTeamRadiusMultiple;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SaveControl(XmlDocument xmlDoc, XmlNode node)
    {
        StringManager.CreateNode(xmlDoc, node, "Control", PlayerAction.s_Instance.joystick_radius_multiple.ToString() );
    }

    public void GenerateControl(XmlNode node)
    {
        if (node != null)
        {
            float val = 1.5f;
            if (float.TryParse(node.InnerText, out val))
            {
                PlayerAction.s_Instance.joystick_radius_multiple = val;
            }
        }
        _inputBallTeamRadiusMultiple.text = PlayerAction.s_Instance.joystick_radius_multiple.ToString();
    }

    public void OnInputValueChanged_BallTeamRadiusMultiple()
    {
        float val = 1.5f;
        if ( float.TryParse(_inputBallTeamRadiusMultiple.text, out val) )
        {
            PlayerAction.s_Instance.joystick_radius_multiple = val;
        }
    }
}
