﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Xml;
public class CSkillSystem : MonoBehaviour {

    static Color colorTemp = new Color();

    public Image _imgSpitSporeButtonBg;
    
    public GameObject _goCastButtonPanel_PC;
    public GameObject _goCastButtonPanel_Mobile;

    public const int MAIN_SKILL_MAX_POINT = 5;
    public const int SELECT_SKILL_MAX_POINT = 3;
    public const int SKILL_PARAM_TOTAL_NUM = 6;
    

    public int m_nMax_R_Num;

    public Sprite[] _arySkillLevelUpSprites;
    public Sprite[] _arySkillArrowAppearSprites;
    public Sprite[] _arySkillArrowBreatheSprites;
    public Sprite[] _arySkillSelectSkillSprites;

    public Button _btnPrevBtn; // 上一页
    public Button _btnNextBtn; // 下一页

    int m_nCurPageIndex = 0;
    public GameObject[] _aryPages;

    public const int SELECTABLE_SKILL_START_ID = 3;
    public const int SELECT_SKILL_MAX_ID = 9;
    public const int INVALID_SKILL_ID = -1;
    int m_nPickSkillId = INVALID_SKILL_ID;

    public const int SKILL_NUM = 5; // 总共有多少种技能
    public enum eSkillId
    {
        q_spore = 0,
        w_spit,
        e_unfold, // 猥琐扩展
        r_sneak, // 潜行
        t_become_thorn, // 变刺
        y_annihilate, // 湮灭
        u_magicshield, // 魔盾
        i_merge,     // 秒合
        o_fenzy,     // 狂暴
        p_gold,      // 金


        total_num, // 总数
    };

    public struct sSkillParam
    {
        public int nCurLevel;
        public bool bAvailable; 
        public float[] aryValues; // 每个level的数值
        public string szValue;
    };
    sSkillParam tempSkillParam;

    public struct sSkillConfig
    {
        public int nSkillId;
        public string szName;
        public string szDesc;
        public int m_nMaxPoint; // 天赋点最高值
        // ???? 待定
    };

    public struct sSkillInfo
    {
        public int nSkillId;
        public int nMaxPoint;
        public int nCurPoint;
    };
    sSkillInfo tempSkillInfo;
    Dictionary<int, sSkillInfo> m_dicSkillInfo = new Dictionary<int, sSkillInfo>();

    public enum eSkillSysProgressBarType
    {
        none,
        t_spit_ball,  // 二分分球
        w_spit_ball, // 蓄力分球
        i_merge_all,  // 秒合
    };
    eSkillSysProgressBarType m_eCurSkillType = eSkillSysProgressBarType.none;

    public CProgressBar _progressBar_PC;
    public CProgressBar _progressBar_MOBILE;
    CProgressBar _progressBar;

    public GameObject _panelThis;

    public Text _txtProgressBar;
    public Image _imgProgressBar;

    public Text _txtCurTotalPoints;

    public static  CSkillSystem s_Instance;

    int m_nPoints = 0; // 当前拥有的总点数

    
    /// <summary>
    /// UI
    /// </summary>
    public CUISkill[] _arySkillCounter;

    public UISkillConfig[] _arySkillParamsConfig;

    public UISkillCastButton[] _arySkillCastButton;
    public UISkillCastButton[] _arySkillCastButton_PC;
    public UISkillCastButton[] _arySkillCastButton_Mobile;

    /// <summary>
    /// 技能描述的编辑
    /// </summary>
    public GameObject _panelEditSkillDesc;
    public InputField _inputSkillDescInput;
    public Button _btnQuitSkillDescEdit;
    public Toggle _toggleShowCOlor;
    public Dropdown _dropdownSkillId;

    void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
        InitAllSkills();
        SetTotalPoint(0);

        InitDropDown_SkillId();

        // spit spore
        m_SpitSporeParam.fEjectSpeed = 120f;
        m_SpitSporeParam.fCostMotherVolumePercent = 0.01f;
        m_SpitSporeParam.nNumPerSecond = 2;
        m_SpitSporeParam.fSpitInterval = 1.0f / (float)m_SpitSporeParam.nNumPerSecond;
        m_SpitSporeParam.fRadiusMultiple = 4f;
        m_SpitSporeParam.fPushThornDis = 1f;
        m_SpitSporeParam.fSporeMinVolume = 10;
        m_SpitSporeParam.fSporeMaxVolume = 100;
    }

    void InitDropDown_SkillId()
    {
        List<string> showNames = new List<string>();

        showNames.Add("潜行");
        showNames.Add("变刺");
        showNames.Add("湮灭");
        showNames.Add("魔盾");
        showNames.Add("秒合");
        showNames.Add("狂暴");
        showNames.Add("金壳");
       

        UIManager.UpdateDropdownView( _dropdownSkillId, showNames);

        for ( int i = SELECTABLE_SKILL_START_ID; i <= SELECT_SKILL_MAX_ID; i++  )
        {
            m_dicSkillDesc[i] = "";
        }


        m_nCurEditingSkillDescSkillId = SELECTABLE_SKILL_START_ID;
        m_dicSkillDesc[m_nCurEditingSkillDescSkillId] = "";
    }

    bool m_bUiInited = false;
    void InitUI()
    {
        if (m_bUiInited)
        {
            return;
        }

        if ( Main.s_Instance == null )
        {
            return;
        }

        if ( Main.s_Instance.GetPlatformType() == Main.ePlatformType.pc )
        {
            _goCastButtonPanel_Mobile.SetActive( false );

            //_progressBar = _progressBar_PC;
            _progressBar = _progressBar_MOBILE;


            for (int i = 0; i < _arySkillCastButton.Length; i++)
            {
                _arySkillCastButton[i] = _arySkillCastButton_PC[i];
            }


            
        }
        else if (Main.s_Instance.GetPlatformType() == Main.ePlatformType.mobile)
        {
            _goCastButtonPanel_PC.SetActive( false );

            _progressBar = _progressBar_MOBILE;

            for (int i = 0; i < _arySkillCastButton.Length; i++ )
            {
                _arySkillCastButton[i] = _arySkillCastButton_Mobile[i];
            }
        }

        m_bUiInited = true;
    }

    // Update is called once per frame
    float m_fRefreshCastButtonTimeCount = 0f;
	void Update () {

        InitUI();

        if (m_fRefreshCastButtonTimeCount >= 0.5f)
        {
            RefreshCastButtonAvailable();
            m_fRefreshCastButtonTimeCount = 0f;
        }
        m_fRefreshCastButtonTimeCount += Time.deltaTime;
    }

    // poppin test
    public void InitAllSkills()
    {
        /*
        int nIndex = (int)eSkillId.q_spore;
        tempSkillInfo = new sSkillInfo();
        tempSkillInfo.nSkillId = nIndex;
        tempSkillInfo.nMaxPoint = 5;
        tempSkillInfo.nCurPoint = 0;
        m_dicSkillInfo[tempSkillInfo.nSkillId] = tempSkillInfo;
        // _arySkillCounter[nIndex].SetMaxNum(tempSkillInfo.nMaxPoint);
        _arySkillCounter[nIndex].Init(tempSkillInfo);
        _arySkillCastButton[nIndex].Init(tempSkillInfo);

        nIndex = (int)eSkillId.w_spit;
        tempSkillInfo = new sSkillInfo();
        tempSkillInfo.nSkillId = nIndex;
        tempSkillInfo.nMaxPoint = 5;
        tempSkillInfo.nCurPoint = 0;
        m_dicSkillInfo[tempSkillInfo.nSkillId] = tempSkillInfo;
        // _arySkillCounter[nIndex].SetMaxNum(tempSkillInfo.nMaxPoint);
        _arySkillCounter[nIndex].Init(tempSkillInfo);
        _arySkillCastButton[nIndex].Init(tempSkillInfo);

        nIndex = (int)eSkillId.e_unfold;
        tempSkillInfo = new sSkillInfo();
        tempSkillInfo.nSkillId = nIndex;
        tempSkillInfo.nMaxPoint = 5;
        tempSkillInfo.nCurPoint = 0;
        m_dicSkillInfo[tempSkillInfo.nSkillId] = tempSkillInfo;
        //_arySkillCounter[nIndex].SetMaxNum(tempSkillInfo.nMaxPoint);
        _arySkillCounter[nIndex].Init(tempSkillInfo);
        _arySkillCastButton[nIndex].Init(tempSkillInfo);

        nIndex = (int)eSkillId.r_split;
        tempSkillInfo = new sSkillInfo();
        tempSkillInfo.nSkillId = nIndex;
        tempSkillInfo.nMaxPoint = 5;
        tempSkillInfo.nCurPoint = 0;
        m_dicSkillInfo[tempSkillInfo.nSkillId] = tempSkillInfo;
        //_arySkillCounter[nIndex].SetMaxNum(tempSkillInfo.nMaxPoint);
        _arySkillCounter[nIndex].Init(tempSkillInfo);
        _arySkillCastButton[nIndex].Init(tempSkillInfo);

        nIndex = (int)eSkillId.t_half_spit;
        tempSkillInfo = new sSkillInfo();
        tempSkillInfo.nSkillId = nIndex;
        tempSkillInfo.nMaxPoint = 5;
        tempSkillInfo.nCurPoint = 0;
        m_dicSkillInfo[tempSkillInfo.nSkillId] = tempSkillInfo;
        // _arySkillCounter[nIndex].SetMaxNum(tempSkillInfo.nMaxPoint);
        _arySkillCounter[nIndex].Init(tempSkillInfo);
        _arySkillCastButton[nIndex].Init(tempSkillInfo);
        */
    }

    public string GetPickSkillNameById( int nSkillId )
    {
        switch (nSkillId)
        {
            case 3:
                {
                    return "潜";
                }
                break;
            case 4:
                {
                    return "刺";
                }
                break;
            case 5:
                {
                    return "湮";
                }
                break;
            case 6:
                {
                    return "免";
                }
                break;
            case 7:
                {
                    return "秒";
                }
                break;
            case 8:
                {
                    return "暴";
                }
                break;
            case 9:
                {
                    return "金";
                }
                break;

        } // end switch

        return "日";
    }

    public void ShowProgressBar(eSkillSysProgressBarType type )
    {
        
        _progressBar.gameObject.SetActive(true);
        m_eCurSkillType = type;
        switch (m_eCurSkillType)
        {
            case eSkillSysProgressBarType.t_spit_ball:
                {
                    _progressBar.SetCaption( "二分分球前摇" );
                }
                break;
            case eSkillSysProgressBarType.w_spit_ball:
                {
                    _progressBar.SetCaption( "力度分球时限" );
                }
                break;
            case eSkillSysProgressBarType.i_merge_all:
                {
                    _progressBar.SetCaption( "秒合前摇" );
                }
                break;
        }
        
    }

    public void UpdateProgressData( float fCurValue, float fTotalValue )
    {
        if (fTotalValue == 0)
        {
            return;
        }

        _progressBar.SetfillAmount( fCurValue / fTotalValue );
    }

    public void HideProgressBar()
    {
        _progressBar.gameObject.SetActive( false );
    }

    public int GetSkillLevel( CSkillSystem.eSkillId eId )
    {
        if (!m_dicSkillInfo.TryGetValue((int)eId, out tempSkillInfo ))
        {
            Debug.LogError("GetSkillLevel error");
            return 0;
        }

        return tempSkillInfo.nCurPoint;
    }

    public sSkillInfo GetSkillInfoById( int nSkillId )
    {
        if ( nSkillId == 0 )
        {
            return tempSkillInfo;
        }
        m_dicSkillInfo.TryGetValue(nSkillId, out tempSkillInfo);
        return tempSkillInfo;
    }

    public void SetPickSkillId( int nPickSkillId)
    {
        m_nPickSkillId = nPickSkillId;
    }

    public int GetPickSkillId()
    {
        return m_nPickSkillId;
    }

    public int GetSelectSkillId()
    {
        return m_nPickSkillId;
    }

    public int GetSelectSkillCurPoint()
    {
        return m_nSelectSkillCurPoint;
    }

    public void PickSkill( int nSkillId )
    {
        SetPickSkillId(nSkillId);
        m_dicSkillInfo.TryGetValue(nSkillId, out tempSkillInfo);
        _arySkillCastButton[SELECTABLE_SKILL_START_ID].SetPickSkillCounterInfo(tempSkillInfo);

    }

    public void AddPickSkillPoint()
    {
        if ( GetPickSkillId() == INVALID_SKILL_ID )
        {
          //  Main.s_Instance.g_SystemMsg.SetContent("技能ID无效");
            return;
        }

        AddPoint(GetPickSkillId());
    }

    public void SetSkillLevel(int nSkillId, int nCurLevel)
    {

        if (nSkillId < SELECTABLE_SKILL_START_ID)
        {
            tempSkillInfo = GetSkillInfoById(nSkillId);
            tempSkillInfo.nCurPoint = nCurLevel;
            m_dicSkillInfo[nSkillId] = tempSkillInfo;

            _arySkillCastButton[nSkillId].SetCurLevel(tempSkillInfo.nCurPoint);
        }
        else
        {
            m_nSelectSkillCurPoint = nCurLevel;
            _arySkillCastButton[SELECTABLE_SKILL_START_ID].SetCurLevel(m_nSelectSkillCurPoint);
        }

        RefreshAddPointButton();

    }

    int m_nSelectSkillCurPoint = 0;
    public void AddPoint(int nSkillId, bool bDirectly = false )
    {
        if (!bDirectly)
        {
            if (m_nPoints <= 0)
            {
               // Main.s_Instance.g_SystemMsg.SetContent("当前已经没有点数了，不能为技能加点");
                return;
            }
        }
        tempSkillInfo = GetSkillInfoById(nSkillId);
        bool bFull = false;
        if (nSkillId < SELECTABLE_SKILL_START_ID) // 基础技能
        {
            if (tempSkillInfo.nCurPoint >= MAIN_SKILL_MAX_POINT)
            {
                bFull = true;
            }
        }
        else // 可选技能（R技能）
        {
            if (tempSkillInfo.nCurPoint >= SELECT_SKILL_MAX_POINT)
            {
                bFull = true;
            }
        }

        if (bFull)
        {
           // Main.s_Instance.g_SystemMsg.SetContent("该技能已加满，不能再加了！");
            return;
        }
        

        tempSkillInfo.nCurPoint++;
        if ( nSkillId >= SELECTABLE_SKILL_START_ID )
        {
            m_nSelectSkillCurPoint++;
            Main.s_Instance.m_MainPlayer.OnSkillLevelChanged(tempSkillInfo.nSkillId, m_nSelectSkillCurPoint);
        }
        else
        {
            Main.s_Instance.m_MainPlayer.OnSkillLevelChanged(tempSkillInfo.nSkillId, tempSkillInfo.nCurPoint);
        }
        m_dicSkillInfo[tempSkillInfo.nSkillId] = tempSkillInfo;

        if (nSkillId == (int)eSkillId.w_spit)
        {
            PlayerAction.LoadSpitSkillParams(tempSkillInfo.nCurPoint);
        }
       

        if (nSkillId < SELECTABLE_SKILL_START_ID)
        {
            _arySkillCastButton[nSkillId].SetCurLevel(tempSkillInfo.nCurPoint);
        }
        else
        {
            _arySkillCastButton[SELECTABLE_SKILL_START_ID].SetCurLevel(m_nSelectSkillCurPoint);
        }

        RefreshAddPointButton();

        if (!bDirectly)
        {
            SetTotalPoint(GetTotalPoint() - 1); // 消耗1点的“技能点数”
        }
    }

   


    public bool CheckIfCanAddPoint( int nSkillId)
    {
        if (m_nPoints <= 0)
        {
        //    Main.s_Instance.g_SystemMsg.SetContent("当前已经没有点数了，不能为技能加点");
            return false;
        }

        tempSkillInfo = GetSkillInfoById(nSkillId);
        if (tempSkillInfo.nCurPoint >= tempSkillInfo.nMaxPoint)
        {
           // Main.s_Instance.g_SystemMsg.SetContent("该技能已加满，不能再加了！");
            return false;
        }

        return true;
    }

    public void SetTotalPoint( int val )
    {
        m_nPoints = val;
        _txtCurTotalPoints.text = m_nPoints.ToString();

        if (Main.s_Instance)
        {
            Main.s_Instance.m_MainPlayer.OnTotalPointChanged(m_nPoints);
        }

        RefreshAddPointButton();
    }

    public void RefreshAddPointButton()
    {
        for (int i = 0; i < _arySkillCastButton.Length; i++)
        {
            if (_arySkillCastButton[i] == null)
            {
                continue;
            }

            bool bShow = false;

            if (m_nPoints > 0)
            {
                if (i >= SELECTABLE_SKILL_START_ID) // 可选技能
                {
                    /*
                    if (m_nSelectSkillCurPoint < SELECT_SKILL_MAX_POINT && Main.s_Instance.m_MainPlayer.GetLevel() >= 6 ) // poppiin test 为了测试技能特效，暂时关掉
                    {
                        if (m_nSelectSkillCurPoint < SELECT_SKILL_MAX_POINT)
                        {
                            _arySkillCastButton[i].SetAddPointButtonVisible(true);
                            bShow = true;
                        }
                    }
                    */
                    if (m_nSelectSkillCurPoint < 1) // 技能点当前为0
                    {
                        if (Main.s_Instance.m_MainPlayer.GetLevel() >= 4)
                        {
                            bShow = true;
                        }

                    }
                    else if (m_nSelectSkillCurPoint < 2) // 技能点当前为1
                    {
                        if (Main.s_Instance.m_MainPlayer.GetLevel() >= 8)
                        {
                            bShow = true;
                        }
                    }
                    else if (m_nSelectSkillCurPoint < 3)// 技能点当前为2
                    {

                        if (Main.s_Instance.m_MainPlayer.GetLevel() >= 12)
                        {
                            bShow = true;
                        }
                    }
                    else// 技能点当前为3（已满级）
                    {

                    }

                }
                else // 主技能
                {
                    tempSkillInfo = GetSkillInfoById(i);
                    /*
                    if (tempSkillInfo.nCurPoint < MAIN_SKILL_MAX_POINT )
                    {

                        _arySkillCastButton[i].SetAddPointButtonVisible(true);
                        bShow = true;
                    }
                    */
                    if (tempSkillInfo.nCurPoint < 1)// 技能点当前为0
                    {
                        bShow = true;
                    }
                    else if (tempSkillInfo.nCurPoint < 2)// 技能点当前为1
                    {
                        if (Main.s_Instance.m_MainPlayer.GetLevel() >= 3)
                        {
                            bShow = true;
                        }
                    }
                    else if (tempSkillInfo.nCurPoint < 3)// 技能点当前为2
                    {
                        if (Main.s_Instance.m_MainPlayer.GetLevel() >= 5)
                        {
                            bShow = true;
                        }
                    }
                    else if (tempSkillInfo.nCurPoint < 4)// 技能点当前为3
                    {
                        if (Main.s_Instance.m_MainPlayer.GetLevel() >= 7)
                        {
                            bShow = true;
                        }
                    }

                    else if (tempSkillInfo.nCurPoint < 5)// 技能点当前为4
                    {
                        if (Main.s_Instance.m_MainPlayer.GetLevel() >= 9)
                        {
                            bShow = true;
                        }
                    }
                    else// 技能点当前为5(已满级)
                    {

                    }
                }
            }


            _arySkillCastButton[i].SetAddPointButtonVisible(bShow);


            
        } // end for
    }

    public void SetSkillColdownActive( eSkillId id, bool bActive)
    {
        if ( (int)id < SELECTABLE_SKILL_START_ID )
        {
            _arySkillCastButton[(int)id].SetSkillColdownActive(bActive);
        }
        else
        {
            _arySkillCastButton[SELECTABLE_SKILL_START_ID].SetSkillColdownActive(bActive);
        }
    }


    public void SetSkillColdownInfo(eSkillId id, float fFillAmount, string szLeftTime )
    {
        if ((int)id < SELECTABLE_SKILL_START_ID)
        {
            _arySkillCastButton[(int)id].SetSkillColdownInfo(fFillAmount, szLeftTime);
        }
        else
        {
            _arySkillCastButton[SELECTABLE_SKILL_START_ID].SetSkillColdownInfo(fFillAmount, szLeftTime);
        }
    }


    /*
    void SetAddPointButtonVisible( bool bVisible )
    {
        for (int i = 0; i < _arySkillCastButton.Length; i++)
        {
            if (_arySkillCastButton[i] == null)
            {
                continue;
            }
            _arySkillCastButton[i].SetAddPointButtonVisible(bVisible);
        }
    }
    */

    public int GetTotalPoint()
    {
        return m_nPoints;
    }

    public float GetSkillPointAffectValue( eSkillId nSkillId )
    {
        float val = 0;

        switch(nSkillId)
        {
            case eSkillId.e_unfold: // 猥琐扩张
                {
                    tempSkillInfo = GetSkillInfoById( (int)nSkillId);
                    val = 0.3f * tempSkillInfo.nCurPoint; // poppin test
                }
                break;

        } // end switch

        return val;
    }

    public void OnButtonClick_Close()
    {
        _panelThis.gameObject.SetActive( false );
    }

    public void SaveSkillParamConfig(XmlDocument xmlDoc, XmlNode node)
    {
        StringManager.CreateNode(xmlDoc, node, "SpitSpore", GetSpitSporeConfigContent()  );

        for ( int i = 1; i < _arySkillParamsConfig.Length; i++ )
        {
            if (_arySkillParamsConfig[i] == null)
            {
                continue;
            }
            StringManager.CreateNode(xmlDoc, node, "S" + i, _arySkillParamsConfig[i].GetConfigContent());
        } // end i
    }

    public void SaveSkillDesc(XmlDocument xmlDoc, XmlNode node)
    {
        foreach( KeyValuePair<int, string> pair in m_dicSkillDesc )
        {
            StringManager.CreateNode(xmlDoc, node, "C" , pair.Value );
        }
    }

    public string GetSkillDescById( int nSkillId )
    {
        return m_dicSkillDesc[nSkillId];
    }

    public void GenerateSkillDesc(XmlNode node)
    {
        if (node == null)
        {
            return;
        }


        for (int i = 0; i < node.ChildNodes.Count; i++)
        {
            int nSkillId = i + SELECTABLE_SKILL_START_ID;
            m_dicSkillDesc[nSkillId] = node.ChildNodes[i].InnerText;
            if ( i == 0 )
            {
                _inputSkillDescInput.text = m_dicSkillDesc[nSkillId];
            }


        }

        
    }

 

    public void GenerateSkillParamConfig( XmlNode node)
    {
        if ( node == null )
        {
            return;
        }


        for ( int i = 0; i < node.ChildNodes.Count; i++ )
        {
            if (node.ChildNodes[i].Name == "SpitSpore")
            {
                GenerateSpitSporeParamConfig(node.ChildNodes[i]);
                continue;
            }

            string[] aryValues = node.ChildNodes[i].InnerText.Split( ',' );
            _arySkillParamsConfig[i].SetConfigContent(aryValues, (CSkillSystem.eSkillId)i);
            
            tempSkillInfo = new sSkillInfo();
            tempSkillInfo.nSkillId = i; 
            tempSkillInfo.nMaxPoint = _arySkillParamsConfig[i].GetMaxPoint();
            tempSkillInfo.nCurPoint = 0;
            m_dicSkillInfo[tempSkillInfo.nSkillId] = tempSkillInfo;
            if (i < _arySkillCounter.Length && _arySkillCounter[i])
            {
                _arySkillCounter[i].Init(tempSkillInfo);
            }

            if (i < SELECTABLE_SKILL_START_ID)
            {
                _arySkillCastButton[i].Init(tempSkillInfo);
            }
            else
            {
                _arySkillCastButton[SELECTABLE_SKILL_START_ID].Init(tempSkillInfo);
            }
        } // end i

        // 暂定
       // AddPoint((int)eSkillId.q_spore, true); // 一出来“吐孢子”技能就可用
        PickSkill( (int)CSelectSkill.s_Instance.GetCurSelectSkill() );     // 一出来就选好了“可选”技能
    }

    void UpdateSkillPointUI()
    {

    }

    public sSkillParam GetSkillParamById( eSkillId eSkillId, ref bool bCanUse )
    {
        int nSkillId = (int)eSkillId;
        sSkillInfo skill_point_info = GetSkillInfoById(nSkillId );
        int nCurPoint = nSkillId < CSkillSystem.SELECTABLE_SKILL_START_ID ? skill_point_info.nCurPoint : CSkillSystem.s_Instance.GetSelectSkillCurPoint() ;
        if (nCurPoint == 0)
        {
            Main.s_Instance.g_SystemMsg.SetContent(CMsgSystem.s_Instance.GetMsgContentByType(CMsgSystem.eSysMsgType.skill_not_activate));
            bCanUse = false;
            return tempSkillParam;
        }
        bCanUse = true;
        if (nCurPoint > skill_point_info.nMaxPoint)
        {
            nCurPoint = skill_point_info.nMaxPoint;
        }
        return _arySkillParamsConfig[nSkillId].GetSkillParamByLevel(nCurPoint);
    }

    public sSkillParam GetSkillParamsByIdAndLevel(eSkillId eSkillId, int nLevel )
    {
        return _arySkillParamsConfig[(int)eSkillId].GetSkillParamByLevel(nLevel);
    }
    public void OnClickButton_PrevPage()
    {
        if (  m_nCurPageIndex == 0 )
        {
            return;
        }
        _aryPages[m_nCurPageIndex].SetActive( false );
        m_nCurPageIndex--;
        _aryPages[m_nCurPageIndex].SetActive(true);
    }

    public void OnClickButton_NextPage()
    {
        if (m_nCurPageIndex == _aryPages.Length - 1)
        {
            return;
        }
        _aryPages[m_nCurPageIndex].SetActive(false);
        m_nCurPageIndex++;
        _aryPages[m_nCurPageIndex].SetActive(true);

    }

    public void RefreshCastButtonAvailable()
    {
        for ( int i = 0; i < _arySkillCastButton.Length; i++ )
        {
            if (_arySkillCastButton[i] == null)
            {
                continue;
            }
            _arySkillCastButton[i].RefreshStatus();
        }
    }

    public bool IsSkillColdDowning( eSkillId id )
    {
        return Main.s_Instance.IsSkillColdDowning(id);
    }

    public Sprite GetSkillSprite( int nSkillId )
    {
        if (_arySkillSelectSkillSprites == null)
        {
            return null;
        }

        if (nSkillId >= _arySkillSelectSkillSprites.Length)
        {
            return null;
        }
               

        return _arySkillSelectSkillSprites[nSkillId];

    }

    /// <summary>
    /// /编辑技能的描述
    /// </summary>
    Dictionary<int, string> m_dicSkillDesc = new Dictionary<int, string>();
    string m_szCurEditingSkillDesc = "";
    int m_nCurEditingSkillDescSkillId = 0;
    public void OnInputValueChange_EditSkillDesc()
    {
        m_szCurEditingSkillDesc = _inputSkillDescInput.text;
        m_dicSkillDesc[m_nCurEditingSkillDescSkillId] = m_szCurEditingSkillDesc;
    }

    public Text _textDesc;

    public void OnToggleValueChange_ShowCOlor()
    {
        _textDesc.supportRichText = _toggleShowCOlor.isOn;
    }

    public void OnButtonClick_EnterSkillDescEdit()
    {
        _panelEditSkillDesc.SetActive(true);
    }


    public void OnButtonClick_QuitSkillDescEdit()
    {
        _panelEditSkillDesc.SetActive( false );
    }

    public void OnDropdownValueChange_SkillId()
    {
        m_nCurEditingSkillDescSkillId = _dropdownSkillId.value + SELECTABLE_SKILL_START_ID;
        if ( !m_dicSkillDesc.TryGetValue(m_nCurEditingSkillDescSkillId, out m_szCurEditingSkillDesc) )
        {
            m_szCurEditingSkillDesc = "";
            m_dicSkillDesc[m_nCurEditingSkillDescSkillId] = m_szCurEditingSkillDesc;
        }

        _inputSkillDescInput.text = m_szCurEditingSkillDesc;
    }

    //// end 编辑技能的描述

    
    public struct sSpitSpoereParam
    {
        public float fRadiusMultiple;
        public float fCostMotherVolumePercent;
        public int nNumPerSecond;
        public float fSpitInterval;
        public float fEjectSpeed;
        public float fPushThornDis;
        public float fSporeMinVolume;
        public float fSporeMaxVolume;
    };

    public sSpitSpoereParam m_SpitSporeParam = new sSpitSpoereParam();

    string GetSpitSporeConfigContent()
    {
        string szContent = m_SpitSporeParam.fRadiusMultiple + "," + m_SpitSporeParam.fCostMotherVolumePercent + "," + m_SpitSporeParam.nNumPerSecond + "," + m_SpitSporeParam.fEjectSpeed + "," + m_SpitSporeParam.fPushThornDis + "," + m_SpitSporeParam.fSporeMinVolume + "," + m_SpitSporeParam.fSporeMaxVolume;
        return szContent;
    }

    void GenerateSpitSporeParamConfig(XmlNode node)
    {
        string[] ary = node.InnerText.Split(',');
        if ( !float.TryParse(ary[0], out m_SpitSporeParam.fRadiusMultiple) )
        {
            m_SpitSporeParam.fRadiusMultiple = 4f;
        }
        if (!float.TryParse(ary[1], out m_SpitSporeParam.fCostMotherVolumePercent))
        {
            m_SpitSporeParam.fCostMotherVolumePercent = 0.01f;
        }
        if (!int.TryParse(ary[2], out m_SpitSporeParam.nNumPerSecond))
        {
            m_SpitSporeParam.nNumPerSecond = 2;
          
        }
        m_SpitSporeParam.fSpitInterval = 1.0f / (float)m_SpitSporeParam.nNumPerSecond;
        if (!float.TryParse(ary[3], out m_SpitSporeParam.fEjectSpeed))
        {
            m_SpitSporeParam.fEjectSpeed = 120f;
        }
        if (!float.TryParse(ary[4], out m_SpitSporeParam.fPushThornDis))
        {
            m_SpitSporeParam.fPushThornDis = 1f;
        }
        if ( ary.Length <= 5 || !float.TryParse(ary[5], out m_SpitSporeParam.fSporeMinVolume))
        {
            m_SpitSporeParam.fPushThornDis = 10f;
        }
        if (ary.Length <= 6 || !float.TryParse(ary[6], out m_SpitSporeParam.fSporeMaxVolume))
        {
            m_SpitSporeParam.fPushThornDis = 100f;
        }


        _inputRadiusMultiple.text = m_SpitSporeParam.fRadiusMultiple.ToString();
        _inputCostMotherVolumePercent.text = m_SpitSporeParam.fCostMotherVolumePercent.ToString();
        _inputnNumPerSecond.text = m_SpitSporeParam.nNumPerSecond.ToString();
        _inputEjectSpeed.text = m_SpitSporeParam.fEjectSpeed.ToString();
        _inputPushThornDis.text = m_SpitSporeParam.fPushThornDis.ToString();
        _inputSporeMinVolume.text = m_SpitSporeParam.fSporeMinVolume.ToString();
        _inputSporeMaxVolume.text = m_SpitSporeParam.fSporeMaxVolume.ToString();

    }

    public void GetSpitSporeParamConfig( ref sSpitSpoereParam config )
    {
        config = m_SpitSporeParam;
    }


    public InputField _inputRadiusMultiple;
    public InputField _inputCostMotherVolumePercent;
    public InputField _inputnNumPerSecond;
    public InputField _inputEjectSpeed;
    public InputField _inputPushThornDis;
    public InputField _inputSporeMinVolume;
    public InputField _inputSporeMaxVolume;


    public void OnInputValueChanged_RadiusMultiple()
    {
        float val = 4f;
        if ( !float.TryParse(_inputRadiusMultiple.text, out val) )
        {
            val = 4f;
        }
        m_SpitSporeParam.fRadiusMultiple = val;
    }

    public void OnInputValueChanged_CostMotherVolumePercent()
    {
        float val = 0.01f;
        if (!float.TryParse(_inputCostMotherVolumePercent.text, out val))
        {
            val = 0.01f;
        }
        m_SpitSporeParam.fCostMotherVolumePercent = val;
    }

    public void OnInputValueChanged_inputnNumPerSecond()
    {
        int val = 2;
        if (!int.TryParse(_inputnNumPerSecond.text, out val))
        {
            val = 2;
        }
        m_SpitSporeParam.nNumPerSecond = val;
    }

    public void OnInputValueChanged_EjectSpeed()
    {
        float val = 120f;
        if (!float.TryParse(_inputEjectSpeed.text, out val))
        {
            val = 120f;
        }
        m_SpitSporeParam.fEjectSpeed = val;
    }

    public void OnInputValueChanged_PushThornDis()
    {
        float val = 1f;
        if (!float.TryParse(_inputPushThornDis.text, out val))
        {
            val = 1f;
        }
        m_SpitSporeParam.fPushThornDis = val;
    }

    public void OnInputValueChanged_SporeMinVolume()
    {
        float val = 10f;
        if (!float.TryParse( _inputSporeMinVolume.text, out val))
        {
            val = 10f;
        }
        m_SpitSporeParam.fSporeMinVolume = val;
    }

    public void OnInputValueChanged_SporeMaxVolume()
    {
        float val = 100f;
        if (!float.TryParse(_inputSporeMaxVolume.text, out val))
        {
            val = 100f;
        }
        m_SpitSporeParam.fSporeMaxVolume = val;
    }

    

    int m_nSporeBtnBlingCount = 0;
    public void SporeBtnBling()
    {
        if (m_nSporeBtnBlingCount == 0)
        {
            _imgSpitSporeButtonBg.color = Color.black;
            m_nSporeBtnBlingCount = 1;
        }
        else
        {
            _imgSpitSporeButtonBg.color = m_colorBling;
            m_nSporeBtnBlingCount = 0;
        }
    }

    public Color m_colorBling;
    public void EndSporeBtnBling(  )
    {
        _imgSpitSporeButtonBg.color = Color.black;
    }
   
 }
