﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CDeadManager : MonoBehaviour {

    public static CDeadManager s_Instance = null;

    public GameObject _panleMain;
    public Text _txtRebornWaitingTimeLeft;

    float m_fRebornWaitingTimeLeft = 0f;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        RebornWaitingTimeLoop();

    }

    public void BeginRebornWaitingTimeLoop( float fTotalTime )
    {
        _panleMain.SetActive( true );
        m_fRebornWaitingTimeLeft = fTotalTime;
    }

    void RebornWaitingTimeLoop()
    {
        if (m_fRebornWaitingTimeLeft <= 0f)
        {
            return;
        }

        m_fRebornWaitingTimeLeft -= Time.deltaTime;

        _txtRebornWaitingTimeLeft.text = m_fRebornWaitingTimeLeft.ToString("f0") + "秒后开始重生";

        if (m_fRebornWaitingTimeLeft <= 0f)
        {
            EndRebornWaitingTimeLoop();
            return;  
        }

    }

    void EndRebornWaitingTimeLoop()
    {
        _panleMain.SetActive(false);
    }
}

