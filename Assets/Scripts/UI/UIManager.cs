﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System.Runtime.InteropServices;

public class UIManager : MonoBehaviour {

    public const string TAG_UI_PC = "UI_GAME";
    public const string TAG_UI_MOBILE = "UI_GAME_MOBILE";

    public const float c_StandardWidth = 1920f;
    public const float c_StandardHeight = 1080f;
    static Vector3 vecTempPos = new Vector3();

    GameObject[] m_aryUIPanelGame_PC ;
    GameObject[] m_aryUIPanelGame_MOBIE;

    public static UIManager s_Instance;

    public CanvasScaler _canvasScaler;
    public Canvas _canvas;

    public Image _imgYaoGanKnob;
    bool m_bYaoGanKnobVisible = true;
    public ETCJoystick _etcJoyStick;

    public GraphicRaycaster _graphicRaycaster;
    public EventSystem eventSystem;

    /*
    [DllImport("__Internal")]
    private static extern string _getDeviceName();

    [DllImport("__Internal")]
    private static extern void _showAlertView(string str);
    */

    public GameObject _uiAll;

    private void Awake()
    {
        s_Instance = this;
    }

    void RefreshUIPos()
    {
     


    }
    public bool CheckCyberTreeItem(Vector2 pos)
    {
        if (_graphicRaycaster == null || eventSystem == null)
        {
            return true;
        }

        List<GameObject> objList = new List<GameObject>();
        PointerEventData eventData = new PointerEventData(eventSystem);
        eventData.pressPosition = pos;
        eventData.position = pos;

        List<RaycastResult> list = new List<RaycastResult>();
        _graphicRaycaster.Raycast(eventData, list);
        if (list.Count > 0)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].gameObject.tag == "CyberTreeItem")
                {
                    return true;
                }
            }
        }
        else
        {
            
        }

        return false;
    }

    // Use this for initialization
    void Start () {

        m_aryUIPanelGame_PC = GameObject.FindGameObjectsWithTag(TAG_UI_PC);
        m_aryUIPanelGame_MOBIE = GameObject.FindGameObjectsWithTag(TAG_UI_MOBILE);

        if ( AccountManager.m_eSceneMode == AccountManager.eSceneMode.MapEditor )
        {
            if (_canvasScaler)
            {
                _canvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ConstantPixelSize;
                _canvasScaler.scaleFactor = 1.6f;
            }
        }
        ProcessIPhone();
    }

    void ProcessIPhone()
    {
        /*
        if (Application.platform != RuntimePlatform.IPhonePlayer)
        {
            MapEditor.s_Instance._txtDebugInfo.text = Application.platform.ToString();
            return;
        }

        MapEditor.s_Instance._txtDebugInfo.text = Camera.main.orthographicSize.ToString();
        */
    }

    public void SetUiGameVisible( bool val )
    {/*
        if (Main.s_Instance.GetPlatformType() == Main.ePlatformType.pc)
        {
            for (int i = 0; i < m_aryUIPanelGame_PC.Length; i++)
            {
                m_aryUIPanelGame_PC[i].SetActive(val);
            }
        }
        else if (Main.s_Instance.GetPlatformType() == Main.ePlatformType.mobile)
        {*/
            for (int i = 0; i < m_aryUIPanelGame_MOBIE.Length; i++)
            {
                m_aryUIPanelGame_MOBIE[i].SetActive(val);
            }
       // }
    }

	// Update is called once per frame
	void Update () {
        RefreshUIPos();

        PeocessYaoGan();
    }

    void PeocessYaoGan()
    {
        PeocessYaoGan_Mobile();
    }

    void PeocessYaoGan_Mobile()
    {
        if (Main.s_Instance == null)
        {
            return;
        }

        if ( Main.s_Instance.GetPlatformType() != Main.ePlatformType.mobile )
        {
            if (_imgYaoGanKnob)
            {
                _imgYaoGanKnob.gameObject.SetActive(false);
                return;
            }
        }

        if (_etcJoyStick == null)
        {
            return;
        }

        if ( _etcJoyStick.visible )
        {
            if (m_bYaoGanKnobVisible)
            {
                _imgYaoGanKnob.gameObject.SetActive(false);
                m_bYaoGanKnobVisible = false;
            }
        }
        else
        {
            if (!m_bYaoGanKnobVisible)
            {
                _imgYaoGanKnob.gameObject.SetActive(true);
                m_bYaoGanKnobVisible = true;
            }

        }
    }

    public static  void UpdateDropdownView( Dropdown dropdownItem,  List<string> showNames)
	{
		if (dropdownItem == null) {
			return;
		}

		dropdownItem.options.Clear();
		Dropdown.OptionData tempData;
		for (int i = 0; i < showNames.Count; i++)
		{
			tempData = new Dropdown.OptionData();
			tempData.text = showNames[i];
			dropdownItem.options.Add(tempData);
		}
		if (showNames.Count > 0) {
			dropdownItem.captionText.text = showNames [0];
		}
	}

    // 判断当前是否点击在了UI上
    public static bool IsPointerOverUI()
    {

        PointerEventData eventData = new PointerEventData(UnityEngine.EventSystems.EventSystem.current);
        eventData.pressPosition = Input.mousePosition;
        eventData.position = Input.mousePosition;

        List<RaycastResult> list = new List<RaycastResult>();
        UnityEngine.EventSystems.EventSystem.current.RaycastAll(eventData, list);

        return list.Count > 0;

    }

    public static void UpdateDropdownItems(Dropdown dropdownItem, List<string> showNames)
    {
        dropdownItem.options.Clear();
        Dropdown.OptionData tempData;
        for (int i = 0; i < showNames.Count; i++)
        {
            tempData = new Dropdown.OptionData();
            tempData.text = showNames[i];
            dropdownItem.options.Add(tempData);
        }
        dropdownItem.captionText.text = showNames[0];
    }
}
