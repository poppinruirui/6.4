﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CEffectManager : MonoBehaviour
{


	public static CEffectManager s_Instance = null;


    /// <summary>
    /// prefab
    /// </summary>
    public GameObject[] m_arySkillEffect_QianYao;
    public GameObject[] m_arySkillEffect_ChiXu;
    /// end prefab

    public enum eSkillEffectType
    {
        qianyao,  // 前摇
        chixu,    // 持续
    }

    public float[] m_arySkillEffectScale_QianYao;
    public float[] m_arySkillEffectScale_ChiXu;

    Dictionary<CSkillSystem.eSkillId, List<CCosmosEffect>> m_dicRecycledSkillEffect_QianYao = new Dictionary<CSkillSystem.eSkillId, List<CCosmosEffect>>();
    Dictionary<CSkillSystem.eSkillId, List<CCosmosEffect>> m_dicRecycledSkillEffect_ChiXu = new Dictionary<CSkillSystem.eSkillId, List<CCosmosEffect>>();

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        MapEditor.s_Instance._txtDebugInfo.text = m_nNewTimes + "_" + m_nDelTimes;
    }

    int m_nNewTimes = 0;
    public CCosmosEffect NewSkillEffect(CSkillSystem.eSkillId eSkillId, eSkillEffectType eType)
    {
		CCosmosEffect effect = null;
	
     
        GameObject[] aryPrefab = null;

        Dictionary<CSkillSystem.eSkillId, List<CCosmosEffect>> dic = null;
        if (eType == eSkillEffectType.qianyao)
        {
            dic = m_dicRecycledSkillEffect_QianYao;
            aryPrefab = m_arySkillEffect_QianYao;
        }
        else if (eType == eSkillEffectType.chixu)
        {
            dic = m_dicRecycledSkillEffect_ChiXu;
            aryPrefab = m_arySkillEffect_ChiXu;
        }
        else
        {
            return null;
        }

        if (eType == eSkillEffectType.chixu)
        {
            m_nNewTimes++;
        }
		effect = GameObject.Instantiate(aryPrefab[(int)eSkillId]).GetComponent<CCosmosEffect>();
 	    return effect;


        bool bExist = false;

        List<CCosmosEffect> lst = null;
        if (dic.TryGetValue(eSkillId, out lst))
        {
			if ( lst != null && lst.Count > 0)
            {
                effect = lst[0];
                effect.gameObject.SetActive(true);
                lst.RemoveAt(0);
                bExist = true;
            }
        }
        else
        {
            lst = new List<CCosmosEffect>();
            dic[eSkillId] = lst;
        }


        if (!bExist)
        {
            effect = GameObject.Instantiate(aryPrefab[(int)eSkillId]).GetComponent<CCosmosEffect>();
			//effect.gameObject.name = eSkillId + "_" + (m_nGuid++).ToString ();
        }

        return effect;
    }

	int m_nGuid = 0;
    int m_nDelTimes = 0;
    public void DeleteSkillEffect(CCosmosEffect effect, CSkillSystem.eSkillId eSkillId, eSkillEffectType eType)
    {
        if (effect == null)
        {
            return;
        }

        if (eType == eSkillEffectType.chixu)
        {
            m_nDelTimes++;
        }
        GameObject.Destroy ( effect.gameObject );
		return;

        Dictionary<CSkillSystem.eSkillId, List<CCosmosEffect>> dic = null;
        if (eType == eSkillEffectType.qianyao)
        {
            dic = m_dicRecycledSkillEffect_QianYao;
        }
        else if (eType == eSkillEffectType.chixu)
        {
            dic = m_dicRecycledSkillEffect_ChiXu;
        }

        List<CCosmosEffect> lst = null;
        if (dic.TryGetValue(eSkillId, out lst))
        {

        }
        else
        {
            lst = new List<CCosmosEffect>();
            dic[eSkillId] = lst;
        }
        effect.transform.SetParent( this.transform );
        effect.gameObject.SetActive( false );
        lst.Add(effect);
		dic [eSkillId] = lst;
    }

    public float GetSkillEffectScale( CSkillSystem.eSkillId eSkillId, CEffectManager.eSkillEffectType eType )
    {
        if ( eType == eSkillEffectType.qianyao )
        {
            return m_arySkillEffectScale_QianYao[(int)eSkillId];
        }
        else if (eType == eSkillEffectType.chixu)
        {
            return m_arySkillEffectScale_ChiXu[(int)eSkillId];
        }

        return 0;
    }

    public void RecycleEffectsFromBall( Ball ball )
    {
        CCosmosEffect[] aryQianYao = null;
        CCosmosEffect[] aryChiXu = null;
        ball.GetCurEffects( ref aryQianYao, ref aryChiXu);
        for ( int i = 0; i < aryQianYao.Length; i++ )
        {
            CCosmosEffect effect = aryQianYao[i];
            if ( effect == null )
            {
                continue;
            }
            aryQianYao[i] = null;
            DeleteSkillEffect(effect, (CSkillSystem.eSkillId)i, eSkillEffectType.qianyao);
        }

        for (int i = 0; i < aryChiXu.Length; i++)
        {
            CCosmosEffect effect = aryChiXu[i];
            if (effect == null)
            {
                continue;
            }
            aryChiXu[i] = null;
            DeleteSkillEffect(effect, (CSkillSystem.eSkillId)i, eSkillEffectType.chixu);
        }
    }











}



