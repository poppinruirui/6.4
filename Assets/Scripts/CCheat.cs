﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CCheat : MonoBehaviour {

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();

    public static CCheat s_Instance;

    public bool m_bActive = false;

    public GameObject _goPanel;

    public Toggle _toggleFixedRebornPos;

    public GameObject _goMoveToCenter;

    /// <summary>
    /// / 击杀信息相关
    /// </summary>
    public InputField _inputJiShaTestScript;

    public Toggle _toggleXingNengCeShi;
  
    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
        InitJiShaInfo();

		if (_inputFakeAccount) {
			_inputFakeAccount.text = "11111111111";
		}

       
    }


	
	// Update is called once per frame
	void Update () {

    
        return;

        ShitTest();

        AutoExplodeLoop();

        FPS_Calc();

        AutoMove();
        
    }

    public bool GetActive()
    {
        return m_bActive;
    }

    public void SetActive( bool bActive )
    {
        m_bActive = bActive;
        _goPanel.SetActive( bActive );
    }

    public void OnClick_Cheat()
    {
        SetActive( !GetActive() );
    }

    void InitJiShaInfo()
    {
     
    }

    List<int> lstAssist = new List<int>();
    public void OnClickButton_JiSha()
    {
        lstAssist.Clear();
        string[] aryAssist;
        /*
        if (_inputZhuGong.text.Length > 0)
        {
            aryAssist = _inputZhuGong.text.Split(',');
            for (int i = 0; i < aryAssist.Length; i++)
            {
                lstAssist.Add(int.Parse(aryAssist[i]));
            }
        }
        
        CJiShaInfo.s_Instance.PushOneJiShaSysMsgIntoQueue("啊啊啊", "不不不",
                                     ResourceManager.s_Instance.GetBallSpriteByPlayerId(1),
                                     ResourceManager.s_Instance.GetBallSpriteByPlayerId(2),
                                     ref lstAssist,
                                     true,
                                     Main.s_Instance.m_MainPlayer.GetDuoSha(),
                                     Main.s_Instance.m_MainPlayer.GetContinuousKillNum()
                                  );
                                  */


       CJiShaInfo.s_Instance.ParseAndExecCommands(_inputJiShaTestScript.text );

    }

    public bool GetXingNengCeShi()
    {
        return _toggleXingNengCeShi.isOn;
    }


    public void OnBtnClick_PerformanceTest()
    {
        Main.s_Instance.m_MainPlayer.ViolentlyGenerateBalls();
    }

    public InputField _inputBallsNum;
    public InputField _inputBallMoveTimeInterval;
    public void ViolentlyGenerateBalls()
    {
        int nBallNum = 250;
        for (int i = 0; i < nBallNum; i++)
        {
            GenrateOneBall();
        }
    }

    public void SetSomeThingVisible( float fThreshold, float fLowerThreshold, bool bVisible )
    {
        for ( int i = 0; i < m_lstTestBalls.Count; i++ )
        {
            Ball ball = m_lstTestBalls[i];

            if (!bVisible)
            {
                if (ball.transform.localScale.x < fThreshold)
                {
                    ball.SetPlayerNameVisible(false);
                    ball.SetStaticShellVisible(false);
                    ball.SetDynamicShellVisible(false);
                }
            }
            else
            {
                if (ball.transform.localScale.x < fThreshold && ball.transform.localScale.x >= fLowerThreshold)
                {
                    ball.SetPlayerNameVisible(true);
                    ball.SetStaticShellVisible(true);
                    ball.SetDynamicShellVisible(true);
                }
            }
        }
    }

    float m_fBallMoveTimeInterval = 0f;
    public void OnInputValueChange_BallMoveTimeInterval()
    {
        m_fBallMoveTimeInterval = float.Parse(_inputBallMoveTimeInterval.text);
    }

    public int m_nShitCount = 1000;
    int m_nShitSkinCount = 0;
    float m_fShitTimeCount = 0;
    int m_nShitSkinIndex = 0;
    public void ShitTest()
    {
        if (m_nShitCount > 500)
        {
            return;
        }

        m_fShitTimeCount += Time.deltaTime;
        if (m_fShitTimeCount <= 0.3f)
        {
            return;
        }
        m_fShitTimeCount = 0;

       
        for ( int i = 0; i < 5; i++ )
        {
        //    GenrateOneBall();
        }
    }

    List<Ball> m_lstTestBalls = new List<Ball>();

    void GenrateOneBall()
    {
        /*
        m_nShitSkinCount++;
        if (m_nShitSkinCount >= 32)
        {
            m_nShitSkinIndex++;
            m_nShitSkinCount = 0;
        }

        Ball ball = GameObject.Instantiate(Main.s_Instance.m_preBall).GetComponent<Ball>();
        ball.GenerateMeshShape(m_nShitSkinIndex);
        ball._Trigger.enabled = false;
        ball._TriggerMergeSelf.enabled = false;
        ball._Collider.enabled = false;
        ball._ColliderDust.enabled = false;
        ball._ColliderTemp.enabled = false;



        vecTempPos.x =  (float)UnityEngine.Random.Range(-200, 200);
        vecTempPos.y = (float)UnityEngine.Random.Range(-200, 200);
        ball.transform.position = vecTempPos;

        float fScale =  (float)UnityEngine.Random.Range(5, 20);

        vecTempScale.x = fScale;
        vecTempScale.y = fScale;
        vecTempScale.z = 1f;
        ball.transform.localScale = vecTempScale;


        ball.SetSortingOrder(0);
        ball.SetPlayerName("测试球");
        //ball.BeginAutoMove();
        m_lstTestBalls.Add(ball);
        m_nShitCount++;
        */
    }

    bool m_bTestBallsMoving = false;
    public void OnClickButton_TestBallsMove()
    {
        CAutoTest.s_Instance.BeginPlayerAutoTest( Main.s_Instance.m_MainPlayer );

        if (m_bTestBallsMoving)
        {
            for ( int i = 0; i < m_lstTestBalls.Count; i++ )
            {
                Ball ball = m_lstTestBalls[i];
                ball.SetAutoMoving(0);
        
            }
            m_bTestBallsMoving = false;
            //Main.s_Instance.m_MainPlayer.SetTestBallMove( false );
        }
        else
        {
            for (int i = 0; i < m_lstTestBalls.Count; i++)
            {
                Ball ball = m_lstTestBalls[i];
                ball.SetAutoMoving(1);

            }
            m_bTestBallsMoving = true;
            //Main.s_Instance.m_MainPlayer.SetTestBallMove(true);
        }


    }

    float m_fAutoMoveTimeElapse = 0f;
    void AutoMove()
    {
        if ( !m_bTestBallsMoving)
        {
            return;
        }

        float fSpeed = 20f * Time.deltaTime;

        m_fAutoMoveTimeElapse += Time.deltaTime;
        if (m_fAutoMoveTimeElapse < m_fBallMoveTimeInterval)
        {
            return;
        }
        m_fAutoMoveTimeElapse = 0;



        for (int i = 0; i < m_lstTestBalls.Count; i++)
        {
            Ball ball = m_lstTestBalls[i];
            ball.AutoMoveLoop(fSpeed);

        }
    }

    public Toggle _toggleRigidMove;
    public bool isRigidMove()
    {
        return _toggleRigidMove.isOn;
    }

    public InputField _inputTRiangleNum;
    public  void OnClickButton_ChangeTriangleNum()
    {
        int nNum = 18;
        if ( !int.TryParse( _inputTRiangleNum.text, out nNum) )
        {
            nNum = 18;
        }
        if (nNum < 1 )
        {
            nNum = 18;
        }
        Main.s_Instance.m_MainPlayer.ChangeTriangleNum(nNum);
    }

    int m_nAutoExplode = 0;
    float m_fAutoEXplodeTimeCount = 0f;
    public void OnClickButton_AutoExplodeAndMerge()
    {
        if (m_nAutoExplode == 0)
        {
            m_nAutoExplode = 1;
        }
        else
        {
            m_nAutoExplode = 0;
        }


    } 

    void AutoExplodeLoop()
    {
        if (m_nAutoExplode == 0)
        {
            return;
        }

        m_fAutoEXplodeTimeCount += Time.deltaTime;
        if (m_fAutoEXplodeTimeCount > 3f)
        {
            m_fAutoEXplodeTimeCount = 0f;
            if (m_nAutoExplode == 1)
            {
                Main.s_Instance.Test_KingExplode();

                m_nAutoExplode = 2;
            }
            else if (m_nAutoExplode == 2)
            {
                Main.s_Instance.MergeAll();

                m_nAutoExplode = 1;
            }

        }
    }

    float m_fFrameTime = 0f;
    int m_nFrameCount = 0;
    float m_fFPSCalcCount = 0f;
    const float FPSCalcInterval = 5f;
    void FPS_Calc()
    {
        if (Main.s_Instance == null)
        {
            return;
        }

        m_nFrameCount++;

        m_fFPSCalcCount += Time.deltaTime;
        if(m_fFPSCalcCount >= FPSCalcInterval)
        {
            m_fFPSCalcCount = 0f;
            float fps = (float)m_nFrameCount / FPSCalcInterval;

          //  Main.s_Instance.m_textDebugInfo.text = "fps = " + fps;
            m_nFrameCount = 0;
        }
    }

    //// 视野裁剪相关
    public InputField _inputMannualControlViewSize;
    public Toggle _toggleMannualControlViewSize;
    float m_fMannualControlViewSize = 400f;
    public bool IsMannualControlViewSize()
    {
        return _toggleMannualControlViewSize.isOn;
    }
    
    public float GetMannualControlViewSize()
    {
        return m_fMannualControlViewSize;
    }

    public void OnInputValueChanged_MannualControlViewSize()
    {
        float val = 40f;
        if ( float.TryParse(_inputMannualControlViewSize.text, out val) )
        {
            m_fMannualControlViewSize = val;
        }

    }
    //// end 视野裁剪相关


    public InputField _inputClickToStopMove;
    public float _fClickToStopMoveInterval = 0.5f;
    public void OnInputvalueChanged_ClickToStopMove()
    {
        _fClickToStopMoveInterval = float.Parse(_inputClickToStopMove.text);
    }

    public void OnButtonClick_AddBloodStain()
    {
        Main.s_Instance.m_MainPlayer.Test_AddBloodStain();
    }

    public void OnClickButton_ZhangYu()
    {
        Main.s_Instance.m_MainPlayer.BecomeZhangYu();
    }

    public InputField _inputMaxCamSize;
    public Toggle _tggleMaxCamSizeLimit;
    public void OnToggleValueChange_MaxCamSizeLimit()
    {
        float val = 100f;
        if ( !float.TryParse( _inputMaxCamSize.text, out val ) )
        {
            val = 100f;
        }


        CCameraManager.s_Instance.SetMaxCamSizeLimit(_tggleMaxCamSizeLimit.isOn, val);
    }

    /// -------------------- 登录界面上的cheat
    public GameObject _panelHomePageCheatPanel;
    bool m_bHomePageCheatPanelShowing = false;
    public InputField _inputStreetNews;
    public InputField _inputHotEvents;
    
    public void OnClickButton_ShowCheatPanelOnHomePage()
    {
        SetCheatPanelOnHomePageVisible( !m_bHomePageCheatPanelShowing);
    }

    public void SetCheatPanelOnHomePageVisible( bool bVisible )
    {
        _panelHomePageCheatPanel.SetActive(bVisible);
        m_bHomePageCheatPanelShowing = bVisible;
    }

    public void OnClickButton_SendStreetNews()
    {
        CAccountSystem.s_Instance.SendStreetNews(_inputStreetNews.text);

        SetCheatPanelOnHomePageVisible( false );
    }

    public void OnClickButton_SendHotEvents()
    {
        CAccountSystem.s_Instance.SendHotEvents(_inputHotEvents.text);

        SetCheatPanelOnHomePageVisible(false);
    }

    public Text _txtDebugInfo;

    public InputField _inputBallIndex;
    public void OnClickButton_AddShell()
    {
        int nBallIndex = 0;
        if ( !int.TryParse(_inputBallIndex.text, out nBallIndex) )
        {
            return;
        }
        Ball ball = Main.s_Instance.m_MainPlayer.GetBallByIndex(nBallIndex);
        if ( ball == null )
        {
            return;
        }
        ball.Local_SetShellInfo(100, 0);
        ball.Local_BeginShell();
    }

	public InputField _inputFakeAccount;
	public void OnClickButton_FakeAccountLogin()
	{
		AccountManager.FakeAccountLogin ( _inputFakeAccount.text, _inputFakeAccount.text);
		_goPanel.SetActive ( false );
	}

	//  故意断线
	public void OnClickButton_Disconnect()
	{
		PhotonNetwork.Disconnect ();
		_goPanel.SetActive ( false );
	}

    ///// 条纹系统
    public InputField _inputSize;
    public InputField _inputInterval;
    public void OnInputValueChanged_WaveSize()
    {
        float val = 1;
        if ( !float.TryParse(_inputSize.text, out val) )
        {

        }
        CWaveManager.s_Instance.SetSize(val);
    }


    public void OnInputValueChanged_WaveInterval()
    {
        float val = 1;
        if (!float.TryParse(_inputInterval.text, out val))
        {

        }
        CWaveManager.s_Instance.SetInterval(val);
    }

    ///// end 条纹系统

}
