﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Spine.Unity;

public class ResourceManager : MonoBehaviour
{

    public static ResourceManager s_Instance = null;

    public GameObject m_preCiChuXian;

    public Font[] m_font;

    public GameObject m_preBuff;
    public GameObject m_preGrass;
    public GameObject m_preSpray;
    public GameObject m_preSceneBall;
    public GameObject m_preGunsight;
    public GameObject m_preSpore;
    public GameObject m_preThorn;

    public GameObject m_preExplodeEvent;

    public GameObject m_preRebornSpot;

    public Material m_preBallSpineMaterial;

    public GameObject m_preMiaoHeEffect;
    public GameObject m_preItemBuyEffect;

    public string[] m_arySzPlayerColor;
    Color[] m_aryPlayerColors = new Color[32];

    Material m_matSpriteDefault;

    public Sprite[] _arySkinTextures;
    public const int MAX_SKIN_NUM = 20;
    public const int NUM_OF_SEGMENTS = 32;
    List<Material> m_arySkinMaterials = new List<Material>();
    public ImprovedPerlin m_Perlin = new ImprovedPerlin();

    public GameObject m_preBean;

    public Material m_matGray;

    public Material m_matThorn;
    public Sprite m_sprThorn;

    static string[] m_arySZPlayerColor = { "#FF2366", "#FD51D9","#E92C81", "#56B2BA", "#0B78E3","#FACE15","#8D4DE8","#6859EA" };
    static Color[] m_aryColorPlayerColor;

    Shader m_shaderDefaultSkinShader = null;
    Dictionary<int, Material> m_dicSkinId2Materials = new Dictionary<int, Material>();

    public Sprite m_sprDefault;

    public Font GetFont( int nIndex )
    {
        return m_font[nIndex];
    }

    public static void InitPlayerColors()
    {
        m_aryColorPlayerColor = new Color[m_arySZPlayerColor.Length];
        for (int i = 0; i < m_aryColorPlayerColor.Length; i++)
        {
            Color color = new Color();
            if (ColorUtility.TryParseHtmlString(m_arySZPlayerColor[i],
                                               out color))
            {
                m_aryColorPlayerColor[i] = color;
            }
        }
    }

    public static Color GetPlayerColorById( int nId )
    {
        int nIndex = nId % m_aryColorPlayerColor.Length;
        return m_aryColorPlayerColor[nIndex];
    }

    void Awake()
    {
        InitPlayerColors();

        //// DontDestroyOnLoad很坑，每当回到这个场景，该对象会再创建一次，导致这个对象可能变得无限多个。所以要处理一下
        if (s_Instance != null)
        {
            GameObject.Destroy(this.gameObject);


            return;
        }
        s_Instance = this;
        GameObject.DontDestroyOnLoad(this);
        //// 


        m_lstRecycledThorn.Clear();
        m_lstRecycledBuff.Clear();
        m_lstRecycledGrass.Clear();
        m_lstRecycledTarget.Clear();

        

        m_shaderDefaultSkinShader = Shader.Find("Custom/DefaultSkinShader");
        m_matSpriteDefault =  new Material(Shader.Find("Sprites/Default"));
     /*
        for (int i = 0; i < MAX_SKIN_NUM; i++)
        {
            Material mat = new Material(sha);
            mat.mainTexture =  _arySkinTextures[i].texture;
            m_arySkinMaterials.Add(mat);
        }
      */

    }

    // Use this for initialization
    void Start()
    {
        GeneratePlayerColors();

    }


    // Update is called once per frame
    void Update()
    {

    }

    void GeneratePlayerColors()
    {
        for ( int i = 0; i < m_arySzPlayerColor.Length; i++ )
        {
            Color color = new Color();
            if (ColorUtility.TryParseHtmlString(m_arySzPlayerColor[i],
                                               out color))
            {
                m_aryPlayerColors[i] = color;
            }
        }
    }

    public Color GetColorByPlayerId( int nPlayerId )
    {
        int nIndex = nPlayerId % m_aryPlayerColors.Length;
        return m_aryPlayerColors[nIndex];
    }

    public GameObject PrefabInstantiate(string szPrefabName)
    {
        return GameObject.Instantiate((GameObject)Resources.Load(szPrefabName));
    }



    public static void DestroyPolygon(Polygon polygon)
    {
        if (polygon != null)
        {
            GameObject.Destroy(polygon.gameObject);
        }
    }

    static List<SpitBallTarget> m_lstRecycledTarget = new List<SpitBallTarget>();
    public static SpitBallTarget ReuseTarget()
    {
        SpitBallTarget target = null;
        if (m_lstRecycledTarget.Count == 0)
        {
            target = (GameObject.Instantiate(Main.s_Instance.m_preSpitBallTarget)).GetComponent<SpitBallTarget>();
            return target;
        }
        target = m_lstRecycledTarget[0];
        target.gameObject.SetActive(true);
        m_lstRecycledTarget.RemoveAt(0);
        return target;
    }

    public static void RecycleTarget(SpitBallTarget target)
    {
        target.gameObject.SetActive(false);
        m_lstRecycledTarget.Add(target);
    }

    static List<CMonster> m_lstRecycledBean = new List<CMonster>();
    public static CMonster ReuseBean()
    {
        CMonster bean = null;
        if (m_lstRecycledBean.Count == 0)
        {
            bean = (GameObject.Instantiate(Main.s_Instance.m_preBean)).GetComponent<CMonster>();
            return bean;
        }
        bean = m_lstRecycledBean[0];
        m_lstRecycledBean.RemoveAt(0);
        bean.gameObject.SetActive(true);
        bean.SetDead(false);
        return bean;
    }

    public static void RecycleMonster(CMonster monster)
    {
        monster.SetDead(true);
        monster.gameObject.SetActive(false);
        m_lstRecycledBean.Add(monster);
    }


    static List<CMonster> m_lstRecycledThorn = new List<CMonster>();
    static long m_nGuidCount = 0;
    public CMonster ReuseMonster()
    {
        CMonster thorn = null;

        if (m_lstRecycledThorn.Count == 0)
        {
            thorn = (GameObject.Instantiate(m_preThorn)).GetComponent<CMonster>();
            return thorn;
        }
        thorn = m_lstRecycledThorn[0];
        m_lstRecycledThorn.RemoveAt(0);
        thorn.gameObject.SetActive(true);
        thorn.SetDead(false);
        thorn.Reset();
        return thorn;

    }


    Dictionary<int, List<float>> m_dicSceneBeanRebornQueue = new Dictionary<int, List<float>>();

    static List<CBuff> m_lstRecycledBuff = new List<CBuff>();
    public static CBuff ReuseBuff()
    {
        CBuff buff = null;
        if (m_lstRecycledBuff.Count > 0)
        {
            buff = m_lstRecycledBuff[0];
            m_lstRecycledBuff.RemoveAt(0);
        }
        if (buff == null)
        {
            buff = GameObject.Instantiate(s_Instance.m_preBuff).GetComponent<CBuff>();
        }
        return buff;
    }


    public static void RecycleBuff(CBuff buff)
    {
        m_lstRecycledBuff.Add(buff);
    }

    // =====================================================================================================================

    //// ! ---- Grass
    public CGrass ReuseGrass()
    {
        CGrass grass = null;
        if (m_lstRecycledGrass.Count > 0)
        {
            grass = m_lstRecycledGrass[0];
            grass.SetActive(true);
            m_lstRecycledGrass.RemoveAt(0);
        }
        if (grass == null)
        {
            grass = GameObject.Instantiate(m_preGrass).GetComponent<CGrass>();
        }
        return grass;
    }

    List<CGrass> m_lstRecycledGrass = new List<CGrass>();
    public void RecycleGrass(CGrass grass)
    {
        grass.SetActive(false);
        m_lstRecycledGrass.Add(grass);
    }


    /// ! ---- end Grass


    /// !--- Spray
    public CSpray ReuseSpray()
    {
        return GameObject.Instantiate(m_preSpray).GetComponent<CSpray>();
    }
    /// !---- end Spray
    /// 

    /// ! ---- Scene Ball
    /// 
    public CMonster ReuseSceneBall()
    {
        CMonster scene_ball = null;
        if (m_lstRecycledSceneBall.Count > 0)
        {
            scene_ball = m_lstRecycledSceneBall[0];
            scene_ball.SetActive(true);
            m_lstRecycledSceneBall.RemoveAt(0);
        }
        if (scene_ball == null)
        {
            scene_ball = GameObject.Instantiate(m_preSceneBall).GetComponent<CMonster>();
        }
        return scene_ball;
    }

    List<CMonster> m_lstRecycledSceneBall = new List<CMonster>();
    public void RecycleSceneBall(CMonster scene_ball)
    {
        m_lstRecycledSceneBall.Add(scene_ball);
    }

    /// ! ----end  Scene Ball
    /// 

    List<CGunsight> m_lstRecycledGunsight = new List<CGunsight>();
    public CGunsight NewGunsight()
    {
        CGunsight gunsight = null;
        if (m_lstRecycledGunsight.Count > 0)
        {
            gunsight = m_lstRecycledGunsight[0];
            gunsight.gameObject.SetActive(true);
            m_lstRecycledGunsight.RemoveAt(0);
        }
        else
        {
            gunsight = GameObject.Instantiate(m_preGunsight).GetComponent<CGunsight>();
        }

        return gunsight;
    }

    public void RemoveGunsight(CGunsight gunsight)
    {
        gunsight.gameObject.SetActive(false);
        m_lstRecycledGunsight.Add(gunsight);
    }

    public Material CreateBallSpineMaterial(int nSpriteId)
    {
        Material material = new Material(Shader.Find("Spine/Skeleton"));
        material.SetTexture(0, Main.s_Instance.GetSpriteBySpriteId(nSpriteId).texture);
        return material;
    }

    public Sprite m_sprShit;
    public Sprite[] m_aryBallSprites;
    static Color tempColor = new Color(0f, 0f, 0f, 0f);
    void ShitTest()
    {
        for (int i = 0; i < m_sprShit.texture.width; i++)
        {
            for (int j = 0; j < m_sprShit.texture.height; j++)
            {
                Color color = m_sprShit.texture.GetPixel(i, j);
                if (color.a < 0.15f)
                {
                    m_sprShit.texture.SetPixel(i, j, tempColor);
                }

            }
        }

        byte[] bytes = m_sprShit.texture.EncodeToPNG();
        if (bytes != null && bytes.Length > 0)
        {
            File.WriteAllBytes("file_process/wori001.png", bytes);
        }
    }

    public Material[] m_aryBallSkeletonMaterial;
    public Material GetBallSkeletonMaterial(int nIndex)
    {
        if (nIndex >= m_aryBallSkeletonMaterial.Length)
        {
            nIndex = nIndex % m_aryBallSkeletonMaterial.Length;
        }
        return m_aryBallSkeletonMaterial[nIndex];
    }


    public Sprite GetBallSpriteByPlayerId(int nPlayerId)
    {
        /* 暂时停用老的头像流程
        int nIndex = nPlayerId;
        if (nIndex >= m_aryBallSprites.Length)
        {
            nIndex = nIndex % m_aryBallSprites.Length;
        }
        return m_aryBallSprites[nIndex];
        */
        return GetSkinSprite(nPlayerId);
    }

    /// <summary>
    /// / 秒合特效
    /// </summary>
    /// <returns></returns>
    List<CMiaoHeEffect> m_lstRecycledMiaoHeEffect = new List<CMiaoHeEffect>();
    public CMiaoHeEffect NewMiaoHeEffect()
    {
        CMiaoHeEffect effect = null;

		effect = GameObject.Instantiate(m_preMiaoHeEffect).GetComponent<CMiaoHeEffect>();
		return effect;

        if (m_lstRecycledMiaoHeEffect.Count > 0)
        {
            effect = m_lstRecycledMiaoHeEffect[0];
            effect.gameObject.SetActive( true );
            effect.Reset();
            m_lstRecycledMiaoHeEffect.RemoveAt(0);
        }
        else
        {
            effect = GameObject.Instantiate(m_preMiaoHeEffect).GetComponent<CMiaoHeEffect>();
        }
        return effect;
    }

    public void DeleteMiaoHeEffect(CMiaoHeEffect effect)
    {
        effect.gameObject.SetActive( false );
        m_lstRecycledMiaoHeEffect.Add(effect);
    }
    //// end 秒合特效


    //// 道具购买特效
    List<CItemBuyEffect> m_lstRecycledItemBuyEffect = new List<CItemBuyEffect>();
    public CItemBuyEffect NewItemBuyEffect()
    {
        CItemBuyEffect effect = null;
        if (m_lstRecycledItemBuyEffect.Count > 0)
        {
            effect = m_lstRecycledItemBuyEffect[0];
            effect.gameObject.SetActive(true);
            //effect.Reset();
            m_lstRecycledItemBuyEffect.RemoveAt(0);
        }
        else
        {
            effect = GameObject.Instantiate(m_preItemBuyEffect).GetComponent<CItemBuyEffect>();
        }
        return effect;
    }

    public void DeleteItemBuyEffect(CItemBuyEffect effect)
    {
        effect.gameObject.SetActive(false);
        m_lstRecycledItemBuyEffect.Add(effect);
    }

    //// end 道具购买特效


    public Material GetSkinMaterial( int nSkinId )
    {
        Material mat = null;
        if (m_dicSkinId2Materials.TryGetValue(nSkinId, out mat))
        {
            return mat;
        }

        mat = new Material( m_shaderDefaultSkinShader);
        Sprite spr = AccountData.s_Instance.GetSpriteByItemId(nSkinId);
        if ( spr == null )
        {
            return null;
        }
        mat.mainTexture = spr.texture;
        m_dicSkinId2Materials[nSkinId] = mat;
        return mat;
        /*
        if (nIndex >= m_arySkinMaterials.Count)
        {
            nIndex = nIndex % m_arySkinMaterials.Count;
        }

        return m_arySkinMaterials[nIndex];
        */
    }

    public Material GetShellMaterial()
    {
        return m_matSpriteDefault;
    }

    
    public Sprite GetSkinSprite( int nIndex )
    {
        if (nIndex >= _arySkinTextures.Length)
        {
            nIndex = nIndex % _arySkinTextures.Length;
        }

        return _arySkinTextures[nIndex];
    }

    //// 最新的豆子流程
    public CBean NewBean()
    {
        return GameObject.Instantiate(m_preBean).GetComponent<CBean>();
    }

    //// end 最新的豆子流程

    ///// “刺出现”特效
    List<SkeletonAnimation> m_lstRecycledCiChuXianAni = new List<SkeletonAnimation>();
    public SkeletonAnimation NewCiChuXianAni()
    {
        SkeletonAnimation ani = null;
        if (m_lstRecycledCiChuXianAni.Count > 0)
        {
            ani = m_lstRecycledCiChuXianAni[0];
            ani.gameObject.SetActive( true );
            m_lstRecycledCiChuXianAni.RemoveAt(0);
        }
        else
        {
            ani = GameObject.Instantiate( m_preCiChuXian ).GetComponent<SkeletonAnimation>();
        }
        return ani;
    }

    public void DeleteCiChuXianAni(SkeletonAnimation ani)
    {
        ani.gameObject.SetActive(false);
        m_lstRecycledCiChuXianAni.Add( ani );
    }
    ///// end “刺出现”特效



}