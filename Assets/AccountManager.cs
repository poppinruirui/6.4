﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;  
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Xml;
using System.Net;
using System.IO;

public class AccountManager : Photon.PunBehaviour {

    public static bool s_UseCheDan = false;
    public Toggle _toggleUseCheDan;

    public GameObject _uiLoggin;

    public static AccountManager s_Instance = null;
    public static int s_nCurSelectRoomIndex = 0;

    static string m_szCurAccount = "";

    bool m_bLogin = false;

    public const string c_szRoomListPathAndFileName_Server = "\\\\192.168.31.1\\tddownload/RoomList.xml";
    public const string c_szRoomListPath_Server = "\\\\192.168.31.1\\tddownload";
    public static string c_szYingShe = "z:/tddownload/";
    public static string url = "http://39.108.53.61:5566/data/";


    //// !---- UI
    //    public InputField _inputAccount;
    //    public Text _txtCurAccount;
    //    public GameObject _panelBeforeLogin;
    //    public GameObject _panelAfterLogin;
    public GameObject _panelSelectGameModePage;
    public GameObject _panelPlayerInfo;
    public GameObject _panelPlayerName;
    public GameObject _panelSkill;
    public GameObject _panelMainLognIn;

    public Dropdown _dropdownRoomList;
    public Dropdown _dropdownSkills;
    public GameObject _panelNewRoom;
    public InputField _inputNewRoomName;
    public ComoList _lstRoomList;
    public ComoList _lstPlayerList;
    public GameObject _panelLogin;
    public InputField _inputPlayerName;
    public InputField _inputNewNickName;
    public Image _imgAvatar;
    public Text[] _aryMoney;
    public Toggle _toggleObserve;
    public Text _txtUserName;
    public Text _txtPassword;

    public MsgBox m_MsgBox;

    List<string> m_lstRoomList = new List<string>();

    public static int m_nCurSelectedIndex = 0;
    public static string m_szCurSelectedRoomName = "";


    XmlDocument _XmlDoc;
    XmlNode _root;

    public enum eSceneMode
    {
        None,
        Game,
        MapEditor,
    };

    public static eSceneMode m_eSceneMode = eSceneMode.MapEditor;
    public static bool m_bInGame = false;

    public GameObject _uiLogin;
    public GameObject _uiQueue;

    static int[] m_aryMoney = new int[2];

	public static void SetAccount(string szAccount)
	{
		m_szCurAccount = szAccount;
	}

	public static string GetAccount()
	{
		return m_szCurAccount;
	}

    public static  void CostMoney(ShoppingMallManager.eMoneyType type, int num)
    {
        m_aryMoney[(int)type] -= num;
        ShoppingMallManager.s_Instance.UpdateCoinNum();
    }

    public static void AddMoney(ShoppingMallManager.eMoneyType type, int num)
    {
        m_aryMoney[(int)type] += num;
        ShoppingMallManager.s_Instance.UpdateCoinNum();
    }

    static bool s_bMoneyInited = false;
    public static void InitMoney()
    {
        if (s_bMoneyInited)
        {
            return;
        }


        m_aryMoney[0] = 10000;
        m_aryMoney[1] = 10000;

        s_bMoneyInited = true;
    }


    public static int GetMoneyNum(int nType)
    {
        return m_aryMoney[nType];
    }

    void Awake()     
    {



        if (_inputPlayerName != null)
        {
            _inputPlayerName.text = s_szCurEnteredPlayerName;
        }

       
        ;

        InitMoney();

      

        s_Instance = this;

        InitPhoton();

        JiuJie();

       // _lstRoomList.delegateMethodOnSelect = new ComoList.DelegateMethod_OnSelected(this.OnSelected);
       // m_MsgBox.delegateMethodOnClickButton = new MsgBox.DelegateMethod_OnClickButton(this.OnMsgBoxClickButton);

        if (_dropdownSkills) {
            s_nSelectedSkillId = _dropdownSkills.value + 3;
        }

       
    }



    public void OnMsgBoxClickButton(int nParam)
    {
        if (nParam == 1) { // 是否要删除房间
            DoDeleteSelectedRoom();
        }
    }

    public void OnSelected(int nIndex)
    {
        m_nCurSelectedIndex = nIndex;
    }

    public static void JiuJie()
    {
        return;

        //WebRequest request = WebRequest.Create( "z:/shit/test.txt");

        //WebResponse pose = request.GetResponse();

        //        FileStream fs = new FileStream("z:/shit/test.txt", FileMode.Open, FileAccess.Read);
        try
        {
            WebClient client = new WebClient();
            client.DownloadFile(c_szYingShe + "/test.txt", Application.streamingAssetsPath + "/test.txt");
        }
        catch (System.Exception e)
        {
            c_szYingShe = "z:/";
        }
    }




    public static string _gameVersion = "1";
    void InitPhoton()
    {
        PhotonNetwork.autoJoinLobby = false;
        PhotonNetwork.automaticallySyncScene = true;
    }


    public void PlayRoom()
    {
        m_bInGame = true;
		if (!PhotonNetwork.connected) {
			PhotonNetwork.ConnectToRegion (CloudRegionCode.cn, _gameVersion);
		} else {
			OnConnectedToMaster ();
		
		}


    }

    void OnConnectedToMaster()
    {
        PhotonNetwork.JoinOrCreateRoom(m_szCurSelectedRoomName/*_lstRoomList.GeCurSelectedItemContent()*/, new RoomOptions() { MaxPlayers = 20 }, null);
        PhotonNetwork.playerName = GetPlayerName();
    }



    // Use this for initialization
    IEnumerator Start() {



        if (_inputPlayerName) {
			_inputPlayerName.text = s_szCurEnteredPlayerName;
		}
        UpdateSomeInfo();
        m_bInGame = false;

        // 从服务器下载房间列表文件
        //string szClientName = c_szYingShe + "RoomList.xml";
        //XmlDocument XmlDoc = IOManager.LoadXmlFile( szClientName, ref root );
        string szFileName = url + "RoomList.xml";
        WWW www = new WWW(szFileName);
        yield return www; // 等待下载
        _XmlDoc = StringManager.CreateXmlByText(www.text, ref _root);

        m_lstRoomList.Clear();
        for (int i = 0; i < _root.ChildNodes.Count; i++)
        {
            if (_root.ChildNodes[i].InnerText.Length == 0)
            {
                continue;
            }
            AddOneRoom(_root.ChildNodes[i].InnerText, 0);
        }

        RefreshRoomListDropDown();
        UIManager.UpdateDropdownView(_dropdownRoomList, m_lstRoomList);
        if (m_lstRoomList.Count > 0) {
            if (!AccountManager.m_bInGame)
            {
                SetCurSelectedRoom(m_lstRoomList[_dropdownRoomList.value]);
            }

        }

        if (_dropdownRoomList) {
            _dropdownRoomList.value = s_nCurSelectRoomIndex;
        }

        List<string> lstSkills = new List<string>();
        lstSkills.Add("潜行");
        lstSkills.Add("变刺");
        lstSkills.Add("湮灭");
        lstSkills.Add("刺免疫");
        lstSkills.Add("秒合");
        lstSkills.Add("狂暴");
        UIManager.UpdateDropdownView(_dropdownSkills, lstSkills);

    }

    public static int s_nSelectedSkillId = 3;

    /*
     void RefreshPlayerList()
     {
         if ( !AccountManager.m_bInGame )
         {
             return;
         }

         if (_lstPlayerList == null)
         {
             return;
         }

         if (m_fRefreshPlayerListTimeCount < c_fRefreshPlayerListInterval)
         {
             m_fRefreshPlayerListTimeCount += Time.deltaTime;
             return;
         }
         m_fRefreshPlayerListTimeCount = 0f;

         _lstPlayerList.ClearAll();
         lstTempPlayerAccount.Clear();

         for (int i = 0; i < PhotonNetwork.playerList.Length; i++)
         {
             PhotonPlayer photon_player = PhotonNetwork.playerList[i];
             sPlayerAccount info;
             info.szName = photon_player.NickName;
             info.nOwnerId = photon_player.ID;
             Player player = CPlayerManager.s_Instance.GetPlayer(photon_player.ID);
             if (player == null)
             {
                 Debug.LogError("RefreshPlayerList() bug, player null!");
                 continue;
             }
             int nNumOfLiveBalls = 0;
             info.fTotalVolume = player.GetTotalVolume( ref nNumOfLiveBalls);

             // insert sort
             bool bInserted = false;
             for ( int j = 0; j < lstTempPlayerAccount.Count; j++ )
             {
                 if (info.fTotalVolume > lstTempPlayerAccount[j].fTotalVolume )
                 {
                     lstTempPlayerAccount.Insert(j, info);
                     bInserted = true;
                     break;
                 }
             } // end j
             if (!bInserted)
             {
                 lstTempPlayerAccount.Add( info );
             }

         } // end i

         for (  int i = 0; i < lstTempPlayerAccount.Count; i++)
         {
             if (lstTempPlayerAccount[i].nOwnerId == Main.s_Instance.m_MainPlayer.GetOwnerId())
             {
                 m_nMainPlayerRank = i + 1;
             }
             _lstPlayerList.AddItem(lstTempPlayerAccount[i].szName);
         }

         CGrowSystem.s_Instance.SetRank(m_nMainPlayerRank, lstTempPlayerAccount.Count);
     }

     */
    int m_nMainPlayerRank = 0;
    public int GetMainPlayerCurRank()
    {
        return m_nMainPlayerRank;
    }

    void RefreshRoomListDropDown()
    {
     //   _lstRoomList.ClearAll();

        /*
        for (int i = 0; i < m_lstRoomList.Count; i++) {
            _lstRoomList.AddItem(m_lstRoomList[i]);

        }
        */
        UIManager.UpdateDropdownView(_dropdownRoomList, m_lstRoomList);

        /*
        if (m_lstRoomList.Count > 0) {
            _lstRoomList.SelectByIndex(m_nCurSelectedIndex);
        }
        */

    }

    // Update is called once per frame
    void Update() {
        //RefreshPlayerList();

        if (EventSystem.current.currentSelectedGameObject)
        {
            if (EventSystem.current.currentSelectedGameObject.name == "inputPlayerName ")
            {
                _txtUserName.gameObject.SetActive(false);
            }
            if (EventSystem.current.currentSelectedGameObject.name == "inputPassword")
            {
                _txtPassword.gameObject.SetActive(false);
            }
        }
    }

    public void CreateNewRoom()
    {
        _panelNewRoom.SetActive(true);
    }


	public void EnterSelectGameModePage()
    {
        if (!CheckIfAccountValid(GetPlayerName()))
        {
            //m_MsgBox.ShowMsg("用户名无效，请重新输入", MsgBox.eMsgBoxType.e_Msgbox_Type_OkOnly);
            //CMsgBox.s_Instance.Show("请输入有效用户名");
            //return;
        }


        //        _panelSelectGameModePage.SetActive(true);
        _panelSkill.SetActive( true );
      //  _panelPlayerInfo.SetActive(true);
        _panelPlayerName.SetActive( false );
        _panelMainLognIn.SetActive( false );
        CSelectSkill.s_Instance._txtPlayerName.text = GetPlayerName();
      //  CSelectSkill.s_Instance.LoadMap(AccountManager.s_Instance.m_szCurSelectedRoomName);
        CAudioManager.s_Instance.PlayAudio(CAudioManager.eAudioId.e_audio_enter_game);
    }

    public void PlaySelectedRoom_Test( int nRoomIndex )
    {
        m_eSceneMode = eSceneMode.Game; // “运行游戏”模式
        m_szCurSelectedRoomName = "room" + nRoomIndex;
        MapEditor.SetCurRoom(m_szCurSelectedRoomName);
        MapEditor.SetMainPlayerName(GetPlayerName());
        PlayRoom();
        CAudioManager.s_Instance.PlayAudio(CAudioManager.eAudioId.e_audio_enter_game);
    }

    public void PlaySelectedRoom()
    {
        //_panelLogin.SetActive ( false );

        m_eSceneMode = eSceneMode.Game; // “运行游戏”模式
                                        /*
                                        if (!CheckIfCurSelectedRoomValid(_lstRoomList.GeCurSelectedItemContent()))
                                        {
                                            m_MsgBox.ShowMsg ( "都木有选中房间啊!!", MsgBox.eMsgBoxType.e_Msgbox_Type_OkOnly );
                                            return;
                                        }
                                        //m_MsgBox.ShowMsg ( "正在登入房间，请稍候............", MsgBox.eMsgBoxType.e_Msgbox_Type_None );

                                        MapEditor.SetCurRoom ( _lstRoomList.GeCurSelectedItemContent() );
                                        MapEditor.SetMainPlayerName ( _inputPlayerName.text );
                                        PlayRoom( _lstRoomList.GeCurSelectedItemContent() );
                                        */
        if (!CheckIfCurSelectedRoomValid(m_szCurSelectedRoomName))
        {
            m_MsgBox.ShowMsg("都木有选中房间啊!!", MsgBox.eMsgBoxType.e_Msgbox_Type_OkOnly);
            return;
        }

        if (!CheckIfAccountValid(GetPlayerName()))
        {
            //m_MsgBox.ShowMsg("用户名无效，请重新输入", MsgBox.eMsgBoxType.e_Msgbox_Type_OkOnly);
            CMsgBox.s_Instance.Show("请输入有效用户名");
            return;
        }

        // m_MsgBox.ShowMsg ( "正在登入房间，请稍候............", MsgBox.eMsgBoxType.e_Msgbox_Type_None );
        //_uiLoggin.SetActive(t  fplarue);
        MapEditor.SetCurRoom(m_szCurSelectedRoomName);
        MapEditor.SetMainPlayerName(GetPlayerName());
        PlayRoom();
        _uiLoggin.SetActive(true);
        _uiLoggin.GetComponent<CCommonJinDuTiao>().SetCurPercent( 99f );
        _uiLoggin.GetComponent<CCommonJinDuTiao>().SetInfo( "正在登入排队界面......" );
        CAudioManager.s_Instance.PlayAudio(CAudioManager.eAudioId.e_audio_enter_game);
    }

	/*
	public string GetPlayerName()
    {
        return _inputPlayerName.text.Trim();
    }
	*/
	public static string GetPlayerName()
	{
		return s_szPlayerName;
	}

	static string s_szPlayerName = "";

	public static void SetPlayerName( string szPlayerName )
	{
		s_szPlayerName = szPlayerName;
	}

	public static void FakeAccountLogin( string szAccount, string szPlayerName )
	{
		SetAccount (szAccount);
		SetPlayerName (szPlayerName);
		AccountManager.s_Instance.EnterSelectGameModePage ();
	}

    public string GetSelectedRoom()
    {
        return m_szCurSelectedRoomName;
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("cur player count: " + PhotonNetwork.room.playerCount);
              
       
        //CQueueManager.s_Instance.StartQueue();
        
        if (PhotonNetwork.isMasterClient)
        {
            PhotonNetwork.LoadLevel("Scene");

        }
    }

	public bool CheckIfCurSelectedRoomValid( string szRoomName )
    {
		if (szRoomName.Length == 0)
        {
            return false;
        }

        return true;
    }

    public void EditSelectedRoom()
    {
      m_eSceneMode = eSceneMode.MapEditor; // “地图编辑器”模式
        
		/*
		if (!CheckIfCurSelectedRoomValid(_lstRoomList.GeCurSelectedItemContent()))
        {
			m_MsgBox.ShowMsg ( "都木有选中房间啊!!", MsgBox.eMsgBoxType.e_Msgbox_Type_OkOnly );
            return;
        }

		MapEditor.SetCurRoom ( _lstRoomList.GeCurSelectedItemContent() );  
		*/
		if (!CheckIfCurSelectedRoomValid( m_szCurSelectedRoomName ))
		{
			m_MsgBox.ShowMsg ( "都木有选中房间啊!!", MsgBox.eMsgBoxType.e_Msgbox_Type_OkOnly );
			return;
		}

		MapEditor.SetCurRoom ( m_szCurSelectedRoomName );  

        SceneManager.LoadScene( "Scene" ) ;

    }

	public void DoDeleteSelectedRoom()
	{
		string szRoomName = m_szCurSelectedRoomName;//_lstRoomList.GeCurSelectedItemContent ();
		if (!CheckIfCurSelectedRoomValid (szRoomName)) {
			Debug.LogError ( "!CheckIfCurSelectedRoomValid (szRoomName)" );
			return;
		}

		bool bFound = false;
		for (int i = 0; i < m_lstRoomList.Count; i++) {
			string szTemp = m_lstRoomList [i];
			if (szRoomName == m_lstRoomList [i]) {
				m_lstRoomList.RemoveAt (i);
				bFound = true;
				break;
			}
		}

		if (!bFound) {
			Debug.LogError ( "if (!bFound) " );
			return;
		}

		RefreshRoomListDropDown();;
		UpdateRoomListXmlFile ();
	}

	public void DeleteSelectedRoom()
	{
		if (_lstRoomList.GeCurSelectedItemContent ().Length == 0) {
			m_MsgBox.ShowMsg ("都木有选中房间啊", MsgBox.eMsgBoxType.e_Msgbox_Type_OkOnly);
			return;
		}

		string szContent = "即将删除的房间是 【" + m_szCurSelectedRoomName/*_lstRoomList.GeCurSelectedItemContent ()*/ + "】\n";
		szContent += "将直接删除服务器端存档文件，请谨慎操作。";
		m_MsgBox.ShowMsg (szContent, MsgBox.eMsgBoxType.e_Msgbox_Type_YesNo, 1);
	}

    public void CreateNewRoom_OK()
    {
        bool bEmpty = ( m_lstRoomList.Count == 0 );
        string szNewRoomName = _inputNewRoomName.text;

		if (!CheckIfCurSelectedRoomValid( szNewRoomName ) ) {
			m_MsgBox.ShowMsg ( "输入的房间名为空或为非法字符,请重新输入。", MsgBox.eMsgBoxType.e_Msgbox_Type_OkOnly );
			return;
		}

		for (int i = 0; i < m_lstRoomList.Count; i++) {
			if (szNewRoomName == m_lstRoomList [i]) {
				m_MsgBox.ShowMsg ( "这个房间名已经存在了, 请重新输入。", MsgBox.eMsgBoxType.e_Msgbox_Type_OkOnly );
				return;
			}
		}

		string szRoomName = _inputNewRoomName.text;

		//// create an empty xml file
		XmlDocument xmlDoc = new XmlDocument();
		XmlNode node = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", "");
		xmlDoc.AppendChild(node);
		XmlNode root = xmlDoc.CreateElement("root");
		xmlDoc.AppendChild(root);
		StartCoroutine ( DoSaveMapXmlFile( szRoomName, xmlDoc.InnerXml ) );
		////


		AddOneRoom(szRoomName, 1);
        RefreshRoomListDropDown();
		_lstRoomList.SelectByIndex ( _lstRoomList.GetCount() - 1 );
        ClosePanelNewRoom();
        if (bEmpty) // 列表如果本来为空，现在创建了第一条记录，则自动选择第一条记录。
        {
            _dropdownRoomList.captionText.text = szNewRoomName;
            //m_szCurSelectedRoomName = szNewRoomName;
            SetCurSelectedRoom(szNewRoomName);
        }

        UpdateRoomListXmlFile();
    }

	IEnumerator DoSaveMapXmlFile( string szRoomName, string szContent )
	{
		WWWForm wwwForm = new WWWForm ();
		wwwForm.AddField (  "name",  AccountManager.GetPrefix( 1 ) + szRoomName + ".xml"  );  
		wwwForm.AddField (  "content",  szContent );
		wwwForm.AddBinaryData( "content", System.Text.Encoding.Default.GetBytes (szContent ), "2_" + szRoomName, "xml" );
		WWW www = new WWW(AccountManager.url, wwwForm);
		yield return www;
		Debug.Log ( www.text );  
	}

    void AddOneRoom( string szRoomName, int op )
    {
        m_lstRoomList.Add(szRoomName);
    }

    public static void SaveAndUploadMapFile( XmlDocument XmlDoc, string szFileName )
    {
        string szServerName = IOManager.GenerateServerPath() + "/" + szFileName + ".xml";
        string szClientName = IOManager.GenerateClientPath()  + "/" + szFileName + ".xml";
        szClientName = c_szYingShe + szFileName + ".xml";
        XmlDoc.Save( szClientName );
        //IOManager.Upload( szClientName, szServerName );
    }

    void UpdateRoomListXmlFile()
    {
		/*
        string szClientName = IOManager.GenerateClientPath() + "/RoomList.xml";
        string szServerName = IOManager.GenerateServerPath() + "/RoomList.xml";
        XmlNode root = null;
        szClientName = c_szYingShe + "RoomList.xml";
        XmlDocument XmlDoc = IOManager.LoadXmlFile( szClientName, ref root );
		*/
        _root.RemoveAll();
        for (int i = 0; i < m_lstRoomList.Count; i++)
        {
            IOManager.CreateNode( _XmlDoc, _root, "P", m_lstRoomList[i]);
        }
		string szContent = _XmlDoc.InnerXml;
		StartCoroutine (  UploadFile(_XmlDoc.InnerXml ) );
    }

	IEnumerator UploadFile( string szContent )
	{
		WWWForm wwwForm = new WWWForm ();
		wwwForm.AddField (  "name",  "RoomList.xml"  );  
		wwwForm.AddField (  "content",  szContent );
		wwwForm.AddBinaryData( "content", System.Text.Encoding.Default.GetBytes (szContent ), "RoomList", "xml" );
		WWW www = new WWW(url, wwwForm);
		yield return www;
		Debug.Log ( www.text );  
	}

    public void ClosePanelNewRoom()
    {
        _panelNewRoom.SetActive( false );
        _inputNewRoomName.text = "";
    }

    public void CreateNewRoom_Cancel()
    {
        ClosePanelNewRoom();
    }

    void SetCurSelectedRoom( string szRoomName)
    {
        m_szCurSelectedRoomName = szRoomName;
       // CSelectSkill.s_Instance.LoadMap(AccountManager.m_szCurSelectedRoomName);
    }

    public void OnDropDownValueChanged_RoomList()
    {
        // m_szCurSelectedRoomName = _dropdownRoomList.captionText.text;
        SetCurSelectedRoom(_dropdownRoomList.captionText.text); 

         s_nCurSelectRoomIndex = _dropdownRoomList.value;
    }

    public void OnDropDownValueChanged_SelectSkill()
    {
        s_nSelectedSkillId = _dropdownSkills.value + 3;
    }

    public static int GetSelectSkillId()
    {
        return s_nSelectedSkillId;
    }


    public bool CheckIfAccountValid( string szAccount )
    {
        if (szAccount.Length == 0)
        {
            return false;
        }

        return true;
    }

    public bool GetIfLogin()
    {
        return m_bLogin;
    }
	/*
    public void LogIn()
    {
        if (!CheckIfAccountValid( _inputAccount.text ))
        {
            return;
        }

        m_szCurAccount = _inputAccount.text;
        _panelBeforeLogin.SetActive( false );
        _panelAfterLogin.SetActive( true );
        _txtCurAccount.text = m_szCurAccount;
        m_bLogin = true;
    }

    public void LogOut()
    {
        m_szCurAccount = "";
        _panelBeforeLogin.SetActive( true );
        _panelAfterLogin.SetActive( false );
        _txtCurAccount.text = "";
        _inputAccount.text = "";
        m_bLogin = false;
    }
	*/
    public void EnterMySpace()
    {
        if (!GetIfLogin())
        {
            return;
        }
        
        SceneManager.LoadScene( "SelectQu" );
    }

    public void OnInputValueChanged_Account()
    {

    }

	public static string GetPrefix( int nType )
	{
		string szPrefix = "";
		switch (nType) {
		case 1:
			{
				szPrefix = "Room_";
			}
			break;
		case 2:
			{
				szPrefix = "Polygon_";
			}
			break;
		}
		return szPrefix;
	}

	public static bool s_bObserve = false;
	public void OnToggleObserveValueChanged()
	{
		s_bObserve = _toggleObserve.isOn;
	}

    public void OnToggleValueChanged_UseCheDan()
    {
        s_UseCheDan = _toggleUseCheDan.isOn;
    }

    public void OnClickButton_Robot()
    {
        SceneManager.LoadScene("Robot");
    }


    public void OnClickButton_GoToShoppinMall()
    {
        SceneManager.LoadScene("ShoppinMall");
    }


    public void OnClickButton_EnterNewNickNamePanel()
    {
        _panelMainLognIn.SetActive( false );
        _panelPlayerName.SetActive( true );
    }

    public void OnClickButton_EnterNewNickNameFinished()
    {
        _panelMainLognIn.SetActive(true);
        _panelPlayerName.SetActive(false);

        _inputPlayerName.text = _inputNewNickName.text; 
    }


    public void UpdateSomeInfo()
    {
        /*
         for ( int i = 0; i < m_aryMoney.Length; i++ )
        {
            _aryMoney[i].text = m_aryMoney[i].ToString();
        }

        _imgAvatar.sprite = CSkinManager.LoadSkinById( ShoppingMallManager.GetCurEquipedSkinId() );
        if (ShoppingMallManager.GetCurEquipedSkinId() == 0)
        {

            _imgAvatar.gameObject.SetActive( false );
        }
        else
        {
            _imgAvatar.gameObject.SetActive(true);
        }
        */
    }

    public static string s_szCurEnteredPlayerName = "";
    public void OnInputValueChanged_EnterName()
    {
        s_szCurEnteredPlayerName = _inputPlayerName.text;
        
    }


}
