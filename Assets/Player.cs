using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;
using System.Linq;
public class Player : Photon.PunBehaviour, IPunObservable
{
    Color m_colorMe = new Color();

    int m_nSkinId = AccountData.INVALID_SKIN_ID;

    public bool _isRebornProtected = true;

    public Color _color_inner;
    public Color _color_ring;
    public Color _color_poison;

    float m_fProtectTime = 0.0f;

    public List<Ball> m_lstBalls = null;
    public Dictionary<int, Ball> m_dicActiveBalls = new Dictionary<int, Ball>();
    int m_nBallIndex = 0;

    public Vector2 m_vecPos = new Vector2();

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempSize = new Vector3();
    static Vector3 vecTempPos1 = new Vector3();
    static Vector2 vec2Temp = new Vector2();
    static Color colorTemp = new Color();

    public CSkillManager m_SkillManager;

    public CPlayerIns _PlayerIns;
    public void SetPlayerIns( CPlayerIns playerIns )
    {
        _PlayerIns = playerIns;
    }

    public void SetSkinId(int nSkinId)
    {
        m_nSkinId = nSkinId;
    }

    public int GetSkinId()
    {
        if (_PlayerIns)
        {
            return _PlayerIns.GetSkinId();
        }
        return m_nSkinId;
    }

    float m_fTotalEatArea = 0f; // 这个特指“吃别人玩家球”体积
    float m_fTotalEatFoodArea = 0f; // 这个是广义的吃食体积，包括吃别人的玩家球，以及豆子、刺、场景静态球

    int m_nKillCount = 0;
    int m_nBeKilled = 0;
    int m_IncAssistAttackCount = 0;
    int m_nEatThornNum = 0; // 炸刺数

    int m_nContinuousKillNum = 0; // 连续击杀数
    Dictionary<int, float> m_dicDuoShaoRecord = new Dictionary<int, float>();

    int m_nDuoSha = 0;
    public void AddOneDuoShaRecord(int nPlayerId)
    {
        m_nDuoSha = 0;
        m_dicDuoShaoRecord[nPlayerId] = Main.GetTime();

        foreach (KeyValuePair<int, float> pair in m_dicDuoShaoRecord) // 多杀只计算杀不同玩家的次数，杀同一个不算
        {
            float fTimeElapse = Main.GetTime() - pair.Value;
            if (fTimeElapse > CJiShaInfo.s_Instance.m_fDuoShaTime)
            {
                continue;
            }
            m_nDuoSha++;
        }

        if (m_nDuoSha > 0)
        {
            //   Debug.Log(m_nDuoSha + " 杀！！！");
        }
    }

    public void SetDuoSha(int nNum)
    {
        m_nDuoSha = nNum;
    }

    public int GetDuoSha()
    {
        return m_nDuoSha;
    }

    // 增加一次“连续击杀数”
    public void IncContinuousKillNum()
    {
        m_nContinuousKillNum++;
    }

    // “连续击杀数”清零
    public void ClearContinuousKillNum()
    {
        m_nContinuousKillNum = 0;
    }

    // 获取“连续击杀数”
    public int GetContinuousKillNum()
    {
        return m_nContinuousKillNum;
    }

    public void SetContinuousKillNum(int nNum)
    {
        m_nContinuousKillNum = nNum;
    }

    public int GetKillCount()
    {
        return m_nKillCount;
    }

    public void SetKillCount(int val)
    {
        m_nKillCount = val;
    }

    public int GetBeKilledCount()
    {
        return m_nBeKilled;
    }

    public void SetBeKilledCount(int val)
    {
        m_nBeKilled = val;
    }


    public int GetAssistAttackCount()
    {
        return m_IncAssistAttackCount;
    }

    public void SetAssistAttackCount(int val)
    {
        m_IncAssistAttackCount = val;
    }


    public int GetEatThornNum()
    {
        return m_nEatThornNum;
    }

    public void SetEatThornNum(int val)
    {
        m_nEatThornNum = val;
    }

    public void OnEatThornNumChanged( int nNum )
    {
        photonView.RPC("RPC_OnEatThornNumChanged", PhotonTargets.All, nNum);
    }

    [PunRPC]
    public void RPC_OnEatThornNumChanged(int nNum)
    {
        CPlayerManager.s_Instance.SetPlayerData_Common(GetAccount(), CPlayerManager.ePlayerCommonInfoType.eat_thorn_num, (float)nNum);
    }


    int[] m_aryItemNum = new int[4];
    public void SetItemNum(int nIndex, int nNum)
    {
        if (nIndex >= m_aryItemNum.Length)
        {
            return;
        }
        m_aryItemNum[nIndex] = nNum;
    }

    public int GetItemNum(int nIndex)
    {
        if (IsMainPlayer())
        {
            return CItemSystem.s_Instance.GetCurNum(nIndex);
        }
        else
        {
            if (nIndex >= m_aryItemNum.Length)
            {
                return 0;
            }

            return m_aryItemNum[nIndex];
        }

    }


    // 玩家的基础数值
    public struct sPlayerBaseData
    {
        public float fBaseSpeed;
        public float fBaseMp;
        public float fBaseSize;
        public float fBaseShellTime;
        public float fBaseSpitDistance;
    };
    sPlayerBaseData m_BaseData;

    float m_fBaseVolumeByLevel = 0f; // 随等级增加的基础体积

    // Use this for initialization
    void Start()
    {
        Init();

        if (PhotonNetwork.isMasterClient)
        {
            // poppin test
            //SyncQueueInfo();
        }
        else
        {
            if (IsMainPlayer())
            {
                PleaseSyncMyDataIfExist();
            }
        }


    }



    void Awake()
    {
        CPlayerManager.s_Instance.AddPlayer(this);

        int nPlayerId = photonView.ownerId;

    }

    public void Reset()
    {
        SetMoving(false);
    }

    void FixedUpdate()
    {
        //  MoveOtherClientBalls();

    }


    public void Ignore_Test()
    {
        for (int i = 0; i < m_lstBalls.Count - 1; i++)
        {
            Ball ball1 = m_lstBalls[i];


            for (int j = i; j < m_lstBalls.Count; j++)
            {
                Ball ball2 = m_lstBalls[j];
                Physics2D.IgnoreCollision(ball1._Collider, ball2._Collider, false);
            }
        }
    }
    // [to youhua] 很多操作都不需要每帧执行，优化一下，看看能不能提一些内容出来在其它不那么频繁的场合执行
    // Update is called once per frame
    void Update()
    {
        if (m_bObserve)
        {
            return;
        }

        if (Main.s_Instance && Main.s_Instance.IsGameOver())
        {
            return;
        }

        QueryingFromMasterClient();
        QueryingFromMainPlayer();

        //AutoTestMove();
        MiaoHeEffectLoop();

        Loop_1_Second(); // 一秒一次的轮询

        ProtectTimeCount();
        //SyncBallsInfo ();
        //Splitting ();
        //Syncing_FullInfo ();
        //PreDrawLoop ();

        //SpittingSpore ();
        SpittingSpore_New();

        Attenuate();
        // MoveToCenter ();
        //TestExplode ();
        


        AdjustMoveInfo();
        SyncDustInfo();
        SyncPaiHangBangInfo();

        // AdjustRelatedInfo();

        ClearSpitTargetCounting();
        //SyncPosAfterEjectCounting ();
        ShowDebugInfo();


        //BuffLoop ();

        //UpdateBuffEffect ();
        UpdateMoveSpeed();

        Spitting();
        // PreCastSkill_Loop_SpitBallHalf();

        // 技能：变刺
        //BecomeThorn_QianYaoLoop();
        //BecomeThorn_ActAsThornLoop();
        // end 技能：变刺

        // 技能：潜行
        // SneakingLoop();
        // end 技能：潜行

        // 技能：湮灭
        //SkillLoop_Annihilate();
        //end  技能：湮灭

        // 技能：魔盾
        //SkillLoop_MagicShield();
        // end 技能：魔盾

        // 技能：秒合
        //SkillLoop_MiaoHe();
        // end 技能：秒合

        // 技能：狂暴
        //HenzyLoop();
        // end 技能：狂暴

        // 技能：金壳
        //SkillLoop_Gold();
        // end 技能：金壳

        // 技能：膨胀
        //SkillLoop_Unfold();
       // PreUnfoldLoop();
        // end 技能：膨胀

        EatThornSyncQueueLoop();


      //  DoingExplode();
      //  OneSecondIntervalLoop();

        SyncPlayerCommonInfo();

        ProcessPK_New();
    }

    void ProcessPK_New()
    {
        if ( !IsMainPlayer() )
        {
            return;
        }

        for ( int i = 0; i < m_lstBalls.Count; i++ )
        {
            Ball ball = m_lstBalls[i]; 
            if ( ball.IsDead() )
            {
                continue;
            }
            ball.PKLoop_Self();
            ball.PKLoop_Other();
            ball.PKLoop_Monster();
        }
    }


    float m_fLoop_1_Second_TimeElapse = 0f;
    void Loop_1_Second()
    {
        m_fLoop_1_Second_TimeElapse += Time.deltaTime;
        if (m_fLoop_1_Second_TimeElapse < 1f)
        {
            return;
        }
        m_fLoop_1_Second_TimeElapse = 0f;

        ProcessBallSizeChange();
    }

    void ProcessBallSizeChange()
    {
        foreach (KeyValuePair<int, Ball> pair in m_dicActiveBalls)
        {
            pair.Value.BallSizeChange();
        }
    }

    /*
    float m_fOneSecondIntervalLoopTimeElapse = 0f;
    void OneSecondIntervalLoop()
    {
        m_fOneSecondIntervalLoopTimeElapse += Time.deltaTime;
        if (m_fOneSecondIntervalLoopTimeElapse < 1f)
        {
            return;
        }
        m_fOneSecondIntervalLoopTimeElapse = 0f;

        // 更新分球壳的显示
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            if (ball.IsDead() || (!ball.GetActive()))
            {
                continue;
            }

            ball.UpdateShell();
        }
    }
    */

    static Vector2 vecTempDir = new Vector2(0f, 1f);

    void ShowDebugInfo()
    {
        if (!IsMainPlayer())
        {
            return;
        }
        /*
        string szContent = "";
        szContent += "当前总体积： " + m_fTotalArea.ToString("f2") + "\n";
        szContent += "基础体积：" + GetBornBaseTiJi() + "\n";
        szContent += "（原始配置:" + Main.s_Instance.m_fBornMinTiJi + ",道具加成:" + m_BaseData.fBaseSize + "，等级加成:" + m_fBaseVolumeByLevel + ")\n";
        szContent += "累计吃球体积：" + m_fTotalEatArea.ToString( "f2" ) + "\n";
		szContent += "速度系数：" + m_fBaseSpeed +  "\n";
        szContent += "分球距离加成：" + ( (GetBaseSpitDistance() * 100 ) + "%") + "\n";
        szContent += "壳时间减：" + GetDecreaseShellTime() + "秒\n";
      //  MapEditor.s_Instance.UpdateDebugInfo(szContent);
       */
        CGrowSystem.s_Instance.SetCurTotalVolume(GetTotalVolume());
        CGrowSystem.s_Instance.SetPlayerSpeed(m_fBaseSpeed);

    }

    public int GetOwnerId()
    {
        return photonView.ownerId;
    }

    public bool IsMainPlayer()
    {
        return photonView.isMine;
    }


    public int GetCurAvailableBallCount()
    {
        return (int)Main.s_Instance.m_fMaxBallNumPerPlayer - GetCurLiveBallNum();
    }

    [PunRPC]
    public void RPC_GiveMeYourCoreInfo()
    {
        if (!IsMainPlayer())
        {
            return;
        }

        photonView.RPC("RPC_HereIsMyCoreInfo", PhotonTargets.Others, GetAccount(), GetPlayerName() ,GetSkinId());
    }

    [PunRPC]
    public void RPC_HereIsMyCoreInfo(string szAccount, string szPlayerName, int nSkinId )
    {
        SetAccount(szAccount);
        SetPlayerName(szPlayerName);
        SetSkinId(nSkinId);
       // CPlayerManager.s_Instance.TryInitBallsForThisPlayer(szAccount, nSkinId, GetOwnerId(), GetPlayerName());
    }



    public void Init() // 这里相当于Player的构造函数。一个Player实例诞生之时所执行的一系列操作
    {
        _isRebornProtected = true; // 第一次出生时一定是处于“重生保护状态”

        if (Main.s_Instance == null)
        {
            Debug.LogError("Main.s_Instance == null ");
            return;
        }

        if (Main.s_Instance.m_goBalls == null)
        {
            Debug.LogError(" Main.s_Instance.m_goBalls == null");
            return;
        }

        this.gameObject.name = "player_" + photonView.ownerId;
        this.gameObject.transform.parent = Main.s_Instance.m_goBalls.transform;


        int color_index = 0;
        _color_ring = ColorPalette.RandomOuterRingColor(ref color_index);
        _color_inner = ColorPalette.RandomInnerFillColor(photonView.ownerId/*color_index*/ );
        _color_poison = ColorPalette.RandomPoisonFillColor(photonView.ownerId/*color_index*/);

        if (IsMainPlayer())
        {
            this.gameObject.layer = (int)CLayerManager.eLayerId.main_player;
        }
        else
        {
            this.gameObject.layer = (int)CLayerManager.eLayerId.other_player;
        }


        m_colorMe = ResourceManager.s_Instance.GetColorByPlayerId(GetOwnerId());
    }


    /// <summary>
    /// / 断线重连最新机制
    /// </summary>
    bool m_bQueryingFromMasterClientDataReceived = false;
    float m_fQueryingFromMasterClientTimeElapse = 10f;
    void QueryingFromMasterClient()
    {
        if (m_bQueryingFromMasterClientDataReceived)
        {
            return;
        }

        if ( !IsMainPlayer() )
        {
            return;
        }

        m_fQueryingFromMasterClientTimeElapse += Time.deltaTime;
        if (m_fQueryingFromMasterClientTimeElapse < 1f)
        {
            return;
        }
        m_fQueryingFromMasterClientTimeElapse = 0f;

        photonView.RPC("RPC_QueryingFromMasterClient", PhotonTargets.All, GetAccount() );// 这里必须是All，因为有可能遇到MasterClient自己，比如MasterClient自己刚登入时
    }

    [PunRPC]
    public void RPC_QueryingFromMasterClient( string szAccount )
    {
        if ( !PhotonNetwork.isMasterClient )
        {
            return;
        }

        CPlayerIns playerIns = CPlayerManager.s_Instance.GetPlayerInsByAccount(szAccount);
        byte[] bytesBallsData = null;
        if (playerIns == null)
        {

        }
        else
        {
            int nBlobSize = 0;
            bytesBallsData = playerIns.GenerateLiveBallData(ref nBlobSize);
        }

        byte[] bytesCommonData = null;

        photonView.RPC("RPC_QueryingFromMasterClientDataBack", PhotonTargets.All, szAccount, bytesBallsData, bytesCommonData); // 这里必须是All，因为有可能遇到MasterClient自己，比如MasterClient自己刚登入时
    }

    [PunRPC]
    public void RPC_QueryingFromMasterClientDataBack( string szAccount, byte[] bytesBallsData, byte[] bytesCommonData)
    {
        Player player = CPlayerManager.s_Instance.GetPlayerByAccount(szAccount);
        if (player == null)
        {
            return;
        }
        player.QueryingFromMasterClientDataBack(bytesBallsData, bytesCommonData);
    }

    public void QueryingFromMasterClientDataBack( byte[] bytesBallsData, byte[]  bytesCommonData)
    {
        if (m_bQueryingFromMasterClientDataReceived) // 因为网络延迟之类的问题可能会重复请求数据，因此这里作一个容错：不要重复解析数据
        {
            return;
        }
        m_bQueryingFromMasterClientDataReceived = true;
        photonView.RPC("RPC_QueryFromMasterClientForOrphanBallsData", PhotonTargets.All);
        // Debug.Log("m_bQueryingFromMasterClientDataReceived !!");

        if ( !IsMainPlayer() )
        {
            return;
        }

        // [to do]bytes包含有BallsData和CommonData。这里先处理BallsData，稍后再作CommonData
        

        // 原则：皮肤以新的为准，颜色以旧的为准
        int nSkinId = GetSkinId();
        int nColorId = GetOwnerId();
        if (bytesBallsData == null)
        {

        }
        else
        {
            StringManager.BeginPopData(bytesBallsData);
            int nLiveBallNUm = StringManager.PopData_Int();
            nColorId = StringManager.PopData_Short();
        }

        CPlayerManager.s_Instance.TryInitBallsForThisPlayer(bytesBallsData, GetAccount(), nSkinId, nColorId, GetPlayerName());
    }
    // --------------------
    bool m_bQueryingFromMainPlayerDataReceived = false;
    float m_fQueryingFromMainPlayerTimeElapse = 10f;
    public void QueryingFromMainPlayer()
    {
        if (m_bQueryingFromMainPlayerDataReceived)
        {
            return;
        }

        if (IsMainPlayer())
        {
            return;
        }

        m_fQueryingFromMainPlayerTimeElapse += Time.deltaTime;
        if (m_fQueryingFromMainPlayerTimeElapse < 1f)
        {
            return;
        }
        m_fQueryingFromMainPlayerTimeElapse = 0f;

        photonView.RPC("RPC_QueryingFromMainPlayer", PhotonTargets.All );
    }

    [PunRPC]
    public void RPC_QueryingFromMainPlayer()
    {
        if ( !IsMainPlayer() )
        {
            return;
        }

        if ( !GetMyDataLoaded()) // 自己的数据都还没准备好（读档），就暂时不要同步给其它玩家
        {
            return;
        }

        CPlayerIns playerIns = CPlayerManager.s_Instance.GetPlayerInsByAccount(GetAccount());
        byte[] bytesBallsData = null;
        if (playerIns == null)
        {
            
        }
        else
        {
            int nBlobSize = 0;
            bytesBallsData = playerIns.GenerateLiveBallData(ref nBlobSize);
        }

        byte[] bytesCommonData = null;

        photonView.RPC("RPC_QueryingFromMainPlayerDataBack", PhotonTargets.All, GetAccount(), bytesBallsData, bytesCommonData, GetSkinId(), playerIns.GetColorId(), GetPlayerName()); 

    }

    [PunRPC]
    public void RPC_QueryingFromMainPlayerDataBack( string szAccount, byte[] bytesBallsData, byte[] bytesCommonData, int nSkinId, int nColorId, string szPlayerName )
    {
        if (m_bQueryingFromMainPlayerDataReceived)
        {
            return;
        }
        m_bQueryingFromMainPlayerDataReceived = true;
       // Debug.Log("m_bQueryingFromMainPlayerDataReceived !!");

        if ( IsMainPlayer() )
        {
            return;
        }

        SetAccount(szAccount);

        CPlayerManager.s_Instance.TryInitBallsForThisPlayer(bytesBallsData, szAccount, nSkinId, nColorId, szPlayerName);
    }
    //-------------------------------------------

    [PunRPC]
    public void RPC_QueryFromMasterClientForOrphanBallsData()
    {
        if (!PhotonNetwork.isMasterClient)
        {
            return;
        }

        byte[] bytesOrphanBallsData = CPlayerManager.s_Instance.GenerateOrphanBallsData();

        if (bytesOrphanBallsData == null) // 当前木有孤儿球
        {

        }
        else
        {
            photonView.RPC("RPC_OrphanBallsDataBack", PhotonTargets.Others, bytesOrphanBallsData);
        }
    }

    bool m_bOrphanBallsDataReceived = false;

    [PunRPC]
    public void RPC_OrphanBallsDataBack( byte[] bytes )
    {
        if (m_bOrphanBallsDataReceived) // Photon引擎不能点对点单独发送协议，每次发送都是同房间群发。因此，别的客户端的相同请求，会被群发都自己这里。这里必须判断一下，自己已经执行过就不要重复执行了
        {
            return;
        }
        m_bOrphanBallsDataReceived = true;


        CPlayerManager.s_Instance.AnalyzeOrphanBallData(bytes);


    }








    /// end 断线重连最新机制















    [PunRPC]
    public void RPC_SyncMeOrphanBallsInfo()
    {
        if ( !PhotonNetwork.isMasterClient )
        {
            return;
        }

        byte[] bytes = CPlayerManager.s_Instance.GenerateOrphanBallsData();
        photonView.RPC("RPC_HereAreTheOrphanBallsData", PhotonTargets.Others, bytes);
    }

    [PunRPC]
    public void RPC_HereAreTheOrphanBallsData( byte[] bytes )
    {
        CPlayerManager.s_Instance.AnalyzeOrphanBallData(bytes);
    }

    public Color GetColor()
    {
        return m_colorMe;
    }

    public Color GetNoSkinColor()
    {
        return ResourceManager.s_Instance.GetColorByPlayerId(GetOwnerId() + 1);
    }

    public void PlayerLevelUpEffect()
    {
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            if (ball.IsDead() || !ball.GetActive())
            {
                continue;
            }
            ball._effectPlayerLevelUp.BeginPlay(false);
        }
    }

    // 因为球球数众多，必须分帧处理
    bool m_bIniting = false;
    bool m_bInitCompleted = false;
    bool m_bBallInstantiated = false;

    public bool IsInitCompleted()
    {
        return m_bInitCompleted;
    }


    void BeginInit()
    {
        m_bIniting = true;
    }

    void EndInit()
    {
        DoSomeCollisionIgnore();

        m_bIniting = false;
        m_bBallInstantiated = true;

        // 本机的客户端资源初始化完毕，同房间的其它玩家可以对我来一次全量同步了，把你们的当前信息统统发过来
        //PleaseFuckMyAss();
    }

    public void PleaseFuckMyAss()
    {
        photonView.RPC("RPC_FuckMe", PhotonTargets.Others);
    }

    const float c_fInitingBallsInterval = 0.1f;
    float m_fInitingBallsTimeCount = 0f;
    void Initing()
    {
        if (!m_bIniting)
        {
            return;
        }

        // 配置文件初始化成功之前，不要初始化Player。因为网络读取文件是异步的。
        if (!MapEditor.s_Instance.IsMapInitCompleted())
        {
            return;
        }

        m_fInitingBallsTimeCount += Time.deltaTime;
        if (m_fInitingBallsTimeCount < c_fInitingBallsInterval)
        {
            return;
        }
        m_fInitingBallsTimeCount = 0;


        if (m_lstBalls.Count >= Main.s_Instance.m_fMaxBallNumPerPlayer)
        {
            EndInit();
            return;
        }

        Ball ball = GameObject.Instantiate(Main.s_Instance.m_preBall).GetComponent<Ball>();
        vecTempPos.x = 10000;
        vecTempPos.y = 10000;
        ball.SetPos(vecTempPos);
        AddOneBall(ball); // other player
    }

    public void FuckYou()
    {

    }

    [PunRPC]
    public void RPC_FuckMe()
    {
        if (!photonView.isMine)
        {
            return;
        }

        if (PhotonNetwork.isMasterClient)
        {
            SyncPublicInfo();
        }
    }

    public void SyncQueueInfo()
    {
        if (PhotonNetwork.isMasterClient)
        {
            photonView.RPC("RPC_SyncQueueInfo", PhotonTargets.Others, CQueueManager.s_Instance.IsQueueFinished());
        }
    }

    [PunRPC]
    public void RPC_SyncQueueInfo(bool bIsQueueFinished)
    {
        if (PhotonNetwork.isMasterClient)
        {
            return;
        }

        if (bIsQueueFinished)
        {
            if (!CQueueManager.s_Instance.IsQueueFinished())
            {
                CQueueManager.s_Instance.EndQueue();
            }
        }
        else
        {
            CQueueManager.s_Instance.StartQueue();
        }
    }

    public void PhotonRemoveObject(GameObject go)
    {
        PhotonNetwork.Destroy(go);
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {

    }

    public void BeginProtect()
    {
        photonView.RPC("RPC_BeginProtect", PhotonTargets.All);
    }

    [PunRPC]
    public void RPC_BeginProtect()
    {
        m_fProtectTime = Main.s_Instance.m_fBornProtectTime;
        _isRebornProtected = true;

        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            //ball.SetRebornProtectEfffect( true );
        }
    }

    void ProtectTimeCount()
    {
        if (!_isRebornProtected)
        {
            return;
        }
        m_fProtectTime -= Time.deltaTime;
        if (m_fProtectTime <= 0.0f)
        {
            EndProtect();
        }
    }

    public bool IsProtect()
    {
        //return m_fProtectTime > 0.0f;
        return _isRebornProtected;
    }

    public void EndProtect()
    {
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            ///ball.SetRebornProtectEfffect(false);
        }
        _isRebornProtected = false;
    }

    int m_nKingExplodeIndex = 0;
    bool m_bKingExploding = false;
    public void KingExplode()
    {

    }


    [PunRPC]
    public void RPC_SyncSprayInfo(int nSprayGUID, int nDrawStatus, float fParam1, float fParam2, float fParam3)
    {
        MapEditor.s_Instance.SyncSprayInfo(nSprayGUID, nDrawStatus, fParam1, fParam2, fParam3);
    }

    string m_szPlayerName = "";
    public string GetPlayerName()
    {
        return m_szPlayerName;
    }

    public Sprite GetAvatar()
    {
        return m_sprCurAvatar;
    }

    public void SetPlayerName(string szPlayerName)
    {
        m_szPlayerName = szPlayerName;
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            ball.SetPlayerName(m_szPlayerName);
        }
    }

    string m_szAccount = "";
    public void SetAccount(string szAccount)
    {
        m_szAccount = szAccount;

        CPlayerManager.s_Instance.SetPlayerByAccount(m_szAccount, this);
    }

    public string GetAccount()
    {
        return m_szAccount;
    }

    // 凡事要一分为二的来看。编程准则中强调的“尽量不要去遍历”，指的是大数量级的列表，比如几十万个节点的列表。这种几十个节点的小微列表怎么不可以去遍历？
    // 抛开数量级谈优化都是耍流氓。 为了一些根本没必要的优化，倒弄一些Bug出来。


    public Ball GetOneLiveBall(ref int nLiveNum)
    {
        Ball ball = null;
        nLiveNum = 0;
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            if (!(m_lstBalls[i].IsDead()))
            {
                ball = m_lstBalls[i];
                nLiveNum++;
            }
        }
        return ball;
    }



    static byte[] _bytesPublicInfo = new byte[1024];
    public void SyncPublicInfo() // [to youhua] 等6月7号版本过了，把这里优化一下。
    {
        StringManager.BeginPushData(_bytesPublicInfo);
        StringManager.PushData_Float(Main.s_Instance.GetGameStartTime());
        short byFirstBlood = CJiShaInfo.s_Instance.m_bFirstBlood ? (short)1 : (short)0;
        StringManager.PushData_Short(byFirstBlood);

        //// ！---- 刺的变动信息

        // 当前处于销毁状态的刺
        List<CMonster> lstDestroyedMonsters = CClassEditor.s_Instance.GetSyncPublicList_DestroyedMonsters();
        int nNum = lstDestroyedMonsters.Count;
        StringManager.PushData_Short((short)nNum);
        for (int i = 0; i < nNum; i++)
        {
            CMonster monster = lstDestroyedMonsters[i];
            StringManager.PushData_Int((int)monster.GetGuid());
            StringManager.PushData_Float(monster.GetDeadOccurTime());
        }

        // 当前处于位置变动状态的刺
        Dictionary<int, CMonster> dicPushedThorns = CClassEditor.s_Instance.GetSyncPublicList_PushedMonsters();
        StringManager.PushData_Short((short)dicPushedThorns.Count);
        foreach (KeyValuePair<int, CMonster> pair in dicPushedThorns)
        {
            StringManager.PushData_Int((int)pair.Value.GetGuid());
            StringManager.PushData_Float(pair.Value.GetPos().x);
            StringManager.PushData_Float(pair.Value.GetPos().y);
        }
        //// ！---- end 刺的变动信息

        photonView.RPC("RPC_SyncPublicInfo", PhotonTargets.All, _bytesPublicInfo);
    }

    bool m_bPublicInfoInited = false;
    [PunRPC]
    public void RPC_SyncPublicInfo(byte[] bytes)
    {
        if (m_bPublicInfoInited)
        {
            return;
        }
        m_bPublicInfoInited = true;

        StringManager.BeginPopData(bytes);
        Main.s_Instance.SetGameStartTime(StringManager.PopData_Float());
        Main.s_Instance.ProcessTimeLeft();
        Main.s_Instance._txtTimeLeft.gameObject.SetActive(true);

        short byFirstBlood = StringManager.PopData_Short();
        if (byFirstBlood == 1)
        {
            CJiShaInfo.s_Instance.m_bFirstBlood = true;
        }
        else
        {
            CJiShaInfo.s_Instance.m_bFirstBlood = false;
        }

        int nNumOfDestroyedThorn = StringManager.PopData_Short();
        for (int i = 0; i < nNumOfDestroyedThorn; i++)
        {
            int nThornId = StringManager.PopData_Int();
            float fOccurTime = StringManager.PopData_Float();
            CClassEditor.s_Instance.RemoveThorn(nThornId, fOccurTime);
        }

        int nNumOfPushedThorns = StringManager.PopData_Short();
        for (int i = 0; i < nNumOfPushedThorns; i++)
        {
            int nThornId = StringManager.PopData_Int();
            float fPosX = StringManager.PopData_Float();
            float fPosY = StringManager.PopData_Float();
            vecTempPos.x = fPosX;
            vecTempPos.y = fPosY;
            CMonster monster = CClassEditor.s_Instance.GetMonsterByGuid(nThornId);
            vecTempPos.z = -monster.GetSize();
            monster.SetPos(vecTempPos);
        }


    }

    int m_nPlayerPlatform = 0;
    public int GetPlatformType()
    {
        return m_nPlayerPlatform;
    }

    static byte[] _bytesFullInfo = new byte[3072];
    public void SyncFullInfo()
    {
        int nPointer = 0;

        // 昵称
        string szPlayerName = GetPlayerName();
        byte[] bytesPlayerName = StringManager.String2Bytes(szPlayerName);
        int nLenOfPlayerName = bytesPlayerName.Length;
        byte[] bytesLenOfPlayerName = BitConverter.GetBytes(nLenOfPlayerName);
        bytesLenOfPlayerName.CopyTo(_bytesFullInfo, nPointer);
        nPointer += sizeof(byte);
        bytesPlayerName.CopyTo(_bytesFullInfo, nPointer);
        nPointer += nLenOfPlayerName;

        // 账号：
        short nLenOfAccount = 11; // 这个版本账号就是中国大陆手机号，肯定是11位。暂时这样，稍后账号系统修改了再说
        string szAccount = AccountManager.GetAccount();
        byte[] bytesAccount = StringManager.String2Bytes(szAccount);
        BitConverter.GetBytes(nLenOfAccount).CopyTo(_bytesFullInfo, nPointer);
        nPointer += sizeof(short);
        bytesAccount.CopyTo(_bytesFullInfo, nPointer);
        nPointer += nLenOfAccount;

        //// 技能状态
        BitConverter.GetBytes(CSkillSystem.s_Instance.GetSelectSkillId()).CopyTo(_bytesFullInfo, nPointer);
        nPointer += sizeof(short);
        BitConverter.GetBytes(CSkillSystem.s_Instance.GetSkillLevel(CSkillSystem.eSkillId.w_spit)).CopyTo(_bytesFullInfo, nPointer);
        nPointer += sizeof(short);
        BitConverter.GetBytes(CSkillSystem.s_Instance.GetSkillLevel(CSkillSystem.eSkillId.e_unfold)).CopyTo(_bytesFullInfo, nPointer);
        nPointer += sizeof(short);
        BitConverter.GetBytes(CSkillSystem.s_Instance.GetSelectSkillCurPoint()).CopyTo(_bytesFullInfo, nPointer);
        nPointer += sizeof(short);


        // 设备平台
        BitConverter.GetBytes((int)Main.s_Instance.GetPlatformType()).CopyTo(_bytesFullInfo, nPointer);
        nPointer += sizeof(int);

        /// ! -- 成长系统信息 
        byte byLevel = (byte)GetLevel();
        BitConverter.GetBytes(byLevel).CopyTo(_bytesFullInfo, nPointer);
        nPointer += sizeof(byte);
        /// ! -- end 成长系统信息 

        BitConverter.GetBytes(GetSkinId()).CopyTo(_bytesFullInfo, nPointer);
        nPointer += sizeof(int);


        int nNum = GetCurLiveBallNum();
        BitConverter.GetBytes(nNum).CopyTo(_bytesFullInfo, nPointer);
        nPointer += sizeof(byte);
        BitConverter.GetBytes(m_nCurSkinIndex).CopyTo(_bytesFullInfo, nPointer);
        nPointer += sizeof(ushort);
        BitConverter.GetBytes(IsMoving() ? 1 : 0).CopyTo(_bytesFullInfo, nPointer);
        nPointer += sizeof(byte);



        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            if (ball.IsDead())
            {
                continue;
            }
            if (ball.GetIndex() != i)
            {
                Debug.LogError("从个的？？ball.GetIndex () != i");
                continue;
            }



            float fSize = ball.GetSize();
            ushort usSize = (ushort)(fSize * 100f);

            float fWorldPosX = ball.GetPos().x;
            float fWorldPosY = ball.GetPos().y;


            float fPosX = ball.GetPos().x;
            float fPosY = ball.GetPos().y;

            float fCurShellTime = ball.GetCurShellTime();
            float fTotalShellTime = ball.GetTotalShellTime();

            BitConverter.GetBytes(ball.GetIndex()).CopyTo(_bytesFullInfo, nPointer);
            nPointer += sizeof(byte);
            BitConverter.GetBytes(usSize).CopyTo(_bytesFullInfo, nPointer);
            nPointer += sizeof(ushort);
            BitConverter.GetBytes(fPosX).CopyTo(_bytesFullInfo, nPointer);
            nPointer += sizeof(float);
            BitConverter.GetBytes(fPosY).CopyTo(_bytesFullInfo, nPointer);
            nPointer += sizeof(float);
            BitConverter.GetBytes((ushort)fCurShellTime).CopyTo(_bytesFullInfo, nPointer);
            nPointer += sizeof(ushort);
            BitConverter.GetBytes((ushort)fTotalShellTime).CopyTo(_bytesFullInfo, nPointer);
            nPointer += sizeof(ushort);
        } // end for


        photonView.RPC("RPC_SyncFullInfo", PhotonTargets.Others, _bytesFullInfo);
    }

    [PunRPC]
    public void RPC_SyncFullInfo(byte[] bytes)
    {
        return;

        if (photonView.isMine)
        { // 全量信息是同步给其它客户端的，自己没必要再去解析一遍
            return;
        }

        if (!IsPlayerInitCompleted())
        {
            return;
        }

        int nPointer = 0;
        int nLenOfPlayerName = (byte)BitConverter.ToChar(bytes, nPointer);
        nPointer += sizeof(byte);
        string szPlayerName = StringManager.Bytes2String(bytes, nPointer, nLenOfPlayerName);
        SetPlayerName(szPlayerName);
        nPointer += nLenOfPlayerName;

        // 账号方面
        int nLenOfAccount = (short)BitConverter.ToInt16(bytes, nPointer);
        nPointer += sizeof(short);
        string szAccount = StringManager.Bytes2String(bytes, nPointer, nLenOfAccount);
        SetAccount(szAccount);
        nPointer += nLenOfAccount;

        //// 技能状态
        int nSelectedSkillId = (short)BitConverter.ToInt16(bytes, nPointer);
        nPointer += sizeof(short);
        int nSkill_W_level = (short)BitConverter.ToInt16(bytes, nPointer);
        nPointer += sizeof(short);
        int nSkill_E_level = (short)BitConverter.ToInt16(bytes, nPointer);
        nPointer += sizeof(short);
        int nSkill_R_level = (short)BitConverter.ToInt16(bytes, nPointer);
        nPointer += sizeof(short);
        CPlayerManager.s_Instance.SetPlayerData_Common(GetAccount(), CPlayerManager.ePlayerCommonInfoType.skill_r_id, (float)nSelectedSkillId);
        CPlayerManager.s_Instance.SetPlayerData_Common(GetAccount(), CPlayerManager.ePlayerCommonInfoType.skill_w_level, (float)nSkill_W_level);
        CPlayerManager.s_Instance.SetPlayerData_Common(GetAccount(), CPlayerManager.ePlayerCommonInfoType.skill_e_level, (float)nSkill_E_level);
        CPlayerManager.s_Instance.SetPlayerData_Common(GetAccount(), CPlayerManager.ePlayerCommonInfoType.skill_r_level, (float)nSkill_R_level);
        //// end 技能状态

        m_nPlayerPlatform = BitConverter.ToInt32(bytes, nPointer);
        nPointer += sizeof(int);

        //// ! ---- 成长系统信息
        byte byLevel = (byte)BitConverter.ToChar(bytes, nPointer);
        nPointer += sizeof(byte);
        Local_SetLevel(byLevel);
        //// ! ---- end 成长系统信息

        int nSkinId = BitConverter.ToInt32(bytes, nPointer);
        nPointer += sizeof(int);
        SetSkinId(nSkinId);
       // BeginGenerateMeshShape();

        int nNum = (byte)BitConverter.ToChar(bytes, nPointer);
        nPointer += sizeof(byte);
        m_nCurSkinIndex = (ushort)BitConverter.ToUInt16(bytes, nPointer);
        nPointer += sizeof(ushort);
        //SetSkin ( m_nCurSkinIndex );
        byte byIsMoving = (byte)BitConverter.ToChar(bytes, nPointer);
        nPointer += sizeof(byte);
        Local_SetMoving(byIsMoving == 1 ? true : false);


        int nNumOfBallInGroup = 0;
        for (int i = 0; i < nNum; i++)
        {
            int nIndex = (byte)BitConverter.ToChar(bytes, nPointer);
            nPointer += sizeof(byte);
            if (nIndex < 0 || nIndex >= m_lstBalls.Count)
            {
                Debug.LogError("从个的？？nIndex < 0 || nIndex >= m_lstBalls.Count");
                continue;
            }
            Ball ball = m_lstBalls[nIndex];
            ball.SetDead(false);
            ball.SetActive(true);
            ushort usSize = BitConverter.ToUInt16(bytes, nPointer);
            nPointer += sizeof(ushort);
            float fSize = (float)usSize / 100f;
            ball.SetSize(fSize);

            float fPosX = BitConverter.ToSingle(bytes, nPointer);
            nPointer += sizeof(float);
            float fPosY = BitConverter.ToSingle(bytes, nPointer);
            nPointer += sizeof(float);

            vecTempPos.x = fPosX;
            vecTempPos.y = fPosY;
            vecTempPos.z = -fSize;

            ball.SetPos(vecTempPos);


            float fCurShellTime = BitConverter.ToUInt16(bytes, nPointer);
            nPointer += sizeof(ushort);
            float fTotalShellTime = BitConverter.ToUInt16(bytes, nPointer);
            nPointer += sizeof(ushort);

            /*
			ball.Local_SetShellInfo (fTotalShellTime, fCurShellTime);
			if ( fTotalShellTime > 0 )
			{
				ball.Local_BeginShell ();
			}
            */
        }

        m_bInitCompleted = true;
    }

    public void SetPlayerInitCompleted(bool val)
    {
        m_bInitCompleted = val;
    }

    public bool IsPlayerInitCompleted()
    {
        return m_bInitCompleted;
    }

    bool m_bObserve = false;
    public void SetObserve(bool val)
    {
        m_bObserve = val;
        if (m_bObserve)
        {
            this.gameObject.SetActive(false);
        }
    }
    /*
	void Syncing_FullInfo()
	{
		return;

		if (m_SyncFullInfoIndex >= m_lstBalls.Count) {
			m_bSyncingFullInfo = false;
			return;
		}

		if (!m_bSyncingFullInfo) {
			return;
		}

		Ball ball = m_lstBalls[m_SyncFullInfoIndex];
		if (ball && ( !ball.IsDead() ) ) {
			ball.SyncFullInfo ();
		}

		m_SyncFullInfoIndex++;
	}
	*/
    public Vector3 m_vWorldCursorPos = new Vector3();
    public Vector2 m_vSpecialDirecton = new Vector2();
    public float m_fRealTimeSpeedWithoutSize;
    float m_fSyncMoveInfoCount = 0f;

    public float GetRealTimeSpeedWithSize()
    {
        return m_fRealTimeSpeedWithoutSize;
    }

    float m_fBaseSpeed = 0;
    public float GetBaseSpeed()
    {

            return m_fBaseSpeed;
     
       
    }

    public void SetBaseSpeed(float fBaseSpeed)
    {
        m_fBaseSpeed = fBaseSpeed;
    }

    public Vector3 GetWorldCursorPos()
    {
        return m_vWorldCursorPos;
    }

    float m_fShellTimeBuffEffect = 1f;
    void UpdateBuffEffect_ShellTime()
    {
        m_fShellTimeBuffEffect = 1f;
        CBuffEditor.sBuffConfig buff_config;
        for (int i = 0; i < m_lstBuff.Count; i++)
        {
            CBuff buff = m_lstBuff[i];
            buff_config = buff.GetConfig();
            if (buff_config.func == (int)CBuffEditor.eBuffFunc.shell_time)
            {
                m_fShellTimeBuffEffect *= (1f + buff_config.aryValues[0]);
            }
        }
    }

    public float GetBuffEffect_ShellTime()
    {
        return m_fShellTimeBuffEffect;
    }
    public void SetBuffEffect_ShellTime(float val)
    {
        m_fShellTimeBuffEffect = val;
    }

    void UpdateMoveSpeed()
    {
        if (!IsMainPlayer())
        { // 移动速度由 MainPlayer实时同步，每个客户端只管维护自己的速度变化，不用管别的Player的速度实时变化
            return;
        }

        m_fBaseSpeed = Main.s_Instance.m_fBallBaseSpeed; // 这是配置的基础移动速度

        // 接下来是道具的加成
        m_fBaseSpeed += m_BaseData.fBaseSpeed;


        //// ! ---- Buff 对速度的影响
        CBuffEditor.sBuffConfig buff_config;
        for (int i = 0; i < m_lstBuff.Count; i++)
        {
            CBuff buff = m_lstBuff[i];
            buff_config = buff.GetConfig();
            if (buff_config.func == (int)CBuffEditor.eBuffFunc.player_move_speed)
            {
                m_fBaseSpeed *= (1f + buff_config.aryValues[0]);
            }
        }

        // 变刺之后速度会受到影响
        if (IsBecomeThorn())
        {
            m_fBaseSpeed *= (1f + m_fSpeedChangePercent_BecomeThorn);
        }

        // 潜行之后速度会受到影响
        if (IsSneaking())
        {
            m_fBaseSpeed *= (1f + m_fSkillSpeedChangePercent_Sneak);
        }

        // 湮灭状态
        if (IsAnnihilaing())
        {
            m_fBaseSpeed *= (1f + GetAnnihilateSpeedAffect());
        }

        // 魔盾状态
        if (IsMagicShielding())
        {
            m_fBaseSpeed *= (1f + GetSkillMagicShieldSpeedAffect());
        }

        // 狂暴状态
        if (IsHenzy())
        {
            m_fBaseSpeed *= (1f + GetHenzySpeedChangePercent());
        }

        // 金壳状态
        if (IsGold())
        {
            m_fBaseSpeed *= (1f + GetSkillGoldSpeedAddPercent());
        }

        // poppin test 背景音乐的播放速度与玩家的移动速度成正比
        /*
        if (m_fBaseSpeed > Main.s_Instance.m_fBallBaseSpeed)
        {
            CAudioManager.s_Instance.audio_main_bg.pitch = 1f + (m_fBaseSpeed - Main.s_Instance.m_fBallBaseSpeed) / 500f;
        }
        else if (m_fBaseSpeed < Main.s_Instance.m_fBallBaseSpeed)
        {
            CAudioManager.s_Instance.audio_main_bg.pitch = 1f - ( Main.s_Instance.m_fBallBaseSpeed- m_fBaseSpeed) / 500f;
        }
        else
        {
            CAudioManager.s_Instance.audio_main_bg.pitch = 1f;
        }
        */

    }


    static Vector2 vec2TempPos = new Vector2();
    static Vector3 vec3TempPos = new Vector3();
    bool m_bTestMove = false;
    public void SetTestBallMove(bool bMove)
    {
        photonView.RPC("RPC_SetTestBallMove", PhotonTargets.All, bMove);
    }

    [PunRPC]
    public void RPC_SetTestBallMove(bool bMove)
    {

        m_bTestMove = bMove;
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            ball.SetAutoMoving(1);

        }
    }

    void AutoTestMove()
    {
        if (!m_bTestMove)
        {
            return;
        }

        float fSpeed = 10f * Time.deltaTime;

        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            ball.AutoMoveLoop(fSpeed);

        }
    }

    /*
    void MoveOtherClientBalls()
    {
        if (IsMainPlayer())
        {
            return;
        }

        if (m_bTestMove)
        {
            return;
        }

        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            if (!ball.GetActive())
            {
                continue;
            }
              
            ball.Move();
        }
    }
    */

    public void Local_DoMove(float fWorldCursorPosX, float fWorldCursorPosY, float fSpecialDirectionX, float fSpecialDirectionY, float fRealTimeSpeedWithoutSize)
    {
        m_fRealTimeSpeedWithoutSize = fRealTimeSpeedWithoutSize;
        m_vWorldCursorPos.x = fWorldCursorPosX;
        m_vWorldCursorPos.y = fWorldCursorPosY;
        /*
        if (photonView.isMine)
        {
            return;
        }

        m_fRealTimeSpeedWithoutSize = fRealTimeSpeedWithoutSize;
        m_vSpecialDirecton.x = fSpecialDirectionX;
        m_vSpecialDirecton.y = fSpecialDirectionY;
        m_vWorldCursorPos.x = fWorldCursorPosX;
        m_vWorldCursorPos.y = fWorldCursorPosY;
        vec3TempPos.x = fWorldCursorPosX;
        vec3TempPos.y = fWorldCursorPosY;
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            if (ball.IsDead() || !ball.GetActive())
            {
                continue;
            }

            ball.BeginMove(vec3TempPos, m_fRealTimeSpeedWithoutSize);
        }

        */

    }

    bool m_bMoving = false;
    public void SetMoving(bool val)
    {
        /*
        if (!photonView.isMine)
        {
            return;
        }
        photonView.RPC("RPC_SetMoving", PhotonTargets.All, val);
        */
        Local_SetMoving( val );
    }

    [PunRPC]
    public void RPC_SetMoving(bool val)
    {
        Local_SetMoving(val);
    }

    public void Local_SetMoving(bool val)
    {
        m_bMoving = val;
        if (m_bMoving)
        {
            EndMoveToCenter();
        }
        else
        {
            for (int i = 0; i < m_lstBalls.Count; i++)
            {
                Ball ball = m_lstBalls[i];
                ball._ba.SetRealTimeVelocity(0f);
                ball.SetSpeed(0f);
            }
        }

    }

    public bool IsMoving()
    {
        return m_bMoving;
    }



    public void RemoveOneBall(Ball ball)
    {
        return;// No!!!!!!!!!!!!!

        m_lstBalls.Remove(ball);
    }



    float m_fClearSpitTargetTotalTime = 0f;
    public void ClearSpitTarget()
    {
        for (int i = 0; i < m_aryGunsight.Length; i++)
        {
            CGunsight gunsight = m_aryGunsight[i];
            if (gunsight == null)
            {
                continue;
            }
            gunsight.gameObject.SetActive(false);
        }

    }

    public void BeginClearSpitTargetCount(float fTotalTime)
    {
        EndClearSpitTargetCount();
        return;


        m_fClearSpitTargetTotalTime = fTotalTime;
    }

    void ClearSpitTargetCounting()
    {
        if (m_fClearSpitTargetTotalTime <= 0f)
        {
            return;
        }
        m_fClearSpitTargetTotalTime -= Time.deltaTime;
        if (m_fClearSpitTargetTotalTime <= 0f)
        {
            EndClearSpitTargetCount();
        }
    }

    public void EndClearSpitTargetCount()
    {
        ClearSpitTarget();
    }

    int m_nSpitIndex = 0;
    bool m_bSpitting = false;
    /*
        public void DoSpit( int nPlatform )
        {
            Main.s_Instance.BeginCameraSizeProtect ( Main.s_Instance.m_fSpitRunTime );
            photonView.RPC ( "RPC_DoSpit", PhotonTargets.All, m_fSpitPercent, nPlatform, PhotonNetwork.time );
        }

        [PunRPC]
        public void RPC_DoSpit( float fPercent, int nPlatform, double dOccurTime )
        {
            float fDelayTime = (float)( PhotonNetwork.time - dOccurTime );
            for ( int i = 0; i < m_lstBalls.Count; i++ ) {
                Ball ball = m_lstBalls[i];
                if (ball.IsDead ()) {
                    continue;
                }
                ball.CalculateChildSize ( fPercent );
                float fMotherLeftSize = 0f;
                float fChildSize = ball.GetAvailableChildSize( ref fMotherLeftSize );
                if (fChildSize < Main.BALL_MIN_SIZE || fMotherLeftSize < Main.BALL_MIN_SIZE || fMotherLeftSize < fChildSize ) {
                    continue;
                }
                ball.Spit ( fDelayTime, nPlatform, fChildSize, fMotherLeftSize);
            }

            if (photonView.isMine) {
                BeginClearSpitTargetCount (Main.s_Instance.m_fSpitRunTime);
            }

        }
    */
    public bool CheckIfCanSpitBallHalf()
    {
        /*
		if (GetMP () < m_fSpitBallHalfMpCost) {
			Main.s_Instance.g_SystemMsg.SetContent (CMsgSystem.s_Instance.GetMsgContentByType( CMsgSystem.eSysMsgType.mp_not_enough ));
			return false;
		}
        */

        bool bSpitSucceeded = false;
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            if (ball.IsDead())
            {
                continue;
            }

            ball.CalculateChildSize(0.5f); // 因为是“二分技能”，所以直接按50%的分球比例来预判
            float fMotherLeftSize = 0f;
            float fChildSize = ball.GetAvailableChildSize(ref fMotherLeftSize);

            if (fChildSize < Main.BALL_MIN_SIZE || fMotherLeftSize < Main.BALL_MIN_SIZE || fMotherLeftSize < fChildSize)
            {
                continue;
            }

            bSpitSucceeded = true;
            break;
        } // end for

        if (!bSpitSucceeded)
        {
            //  Main.s_Instance.g_SystemMsg.SetContent("队伍中所有母球当前体积都不足以二分分球。");
            return false;
        }


        return true;
    }

    public void Cancel_SpitBallHalf()
    {
        if (m_nPreCastSkill_Status_SpitHalf != 2)
        {
            return;
        }

        m_nPreCastSkill_Status_SpitHalf = 0;
        m_bCastSkillWhickNeedStopMoving = false;
        m_bPreCastSkill_SpitHalf = false;
    }

    float m_fSpitBallHalfMpCost = 0f;
    float m_fSpitBallHalfColddown = 0f;
    float m_fSpitBallHalfQianYao = 0f;
    float m_fSpitBallHalfDistance = 0f;
    float m_fSpitBallHalfRunTime = 0f;
    float m_fSpitBallHalfShellTime = 0f;
    public void PreCastSkill_SpitBallHalf(float fMpCost, float fColddown, float fQianYao, float fDistance, float fRunTime, float fShellTime)
    {
        m_fSpitBallHalfMpCost = fMpCost;
        m_fSpitBallHalfColddown = fColddown;
        m_fSpitBallHalfQianYao = fQianYao;
        m_fSpitBallHalfDistance = fDistance;
        m_fSpitBallHalfRunTime = fRunTime;
        m_fSpitBallHalfShellTime = fShellTime;

        if (m_nPreCastSkill_Status_SpitHalf != 0)
        {
            return;
        }

        if (!CheckIfCanSpitBallHalf())
        {
            RPC_CancelSpitBallHalf();
            return;
        }

        m_nPreCastSkill_Status_SpitHalf = 1;

        CSkillSystem.s_Instance.ShowProgressBar(CSkillSystem.eSkillSysProgressBarType.t_spit_ball);
        photonView.RPC("RPC_PreCastSkill_SpitBallHalf", PhotonTargets.All, fMpCost, fQianYao, fDistance, fRunTime, fShellTime);
    }

    [PunRPC]
    public void RPC_PreCastSkill_SpitBallHalf(float fMpCost, float fQianYao, float fDistance, float fRunTime, float fShellTime)
    {
        m_fSpitBallHalfMpCost = fMpCost;
        m_fSpitBallHalfQianYao = fQianYao;
        m_fSpitBallHalfDistance = fDistance;
        m_fSpitBallHalfRunTime = fRunTime;
        m_fSpitBallHalfShellTime = fShellTime;

        m_nPreCastSkill_Status_SpitHalf = 1;
        m_bPreCastSkill_SpitHalf = true;
        m_bCastSkillWhickNeedStopMoving = true;
        m_fPreCastSkill_SpitHalf_TimeCount = 0;

    }

    int m_nPreCastSkill_Status_SpitHalf = 0;
    bool m_bPreCastSkill_SpitHalf = false;
    float m_fPreCastSkill_SpitHalf_TimeCount = 0f;

    public int GetSpitBallHalfStatus()
    {
        return m_nPreCastSkill_Status_SpitHalf;
    }

    void PreCastSkill_Loop_SpitBallHalf()
    {
        if (!m_bPreCastSkill_SpitHalf)
        {
            return;
        }

        if (m_nPreCastSkill_Status_SpitHalf != 1)
        {
            return;
        }

        //// ! ---- show atrget
        ShowTarget(0.5f, m_fSpitBallHalfDistance);
        //// ! ---- end show target

        CSkillSystem.s_Instance.UpdateProgressData(m_fPreCastSkill_SpitHalf_TimeCount, m_fSpitBallHalfQianYao);

        m_fPreCastSkill_SpitHalf_TimeCount += Time.deltaTime;
        if (m_fPreCastSkill_SpitHalf_TimeCount >= m_fSpitBallHalfQianYao)
        {
            if (IsMainPlayer())
            {
                CSkillSystem.s_Instance.HideProgressBar();
                SpitBallHalf();
            }
        }
    }

    void ShowTarget(float fPercent, float fSpitBallDistance)
    {
        int nAvailableNum = GetCurAvailableBallCount();
        int nCount = 0;
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            if (ball.IsDead())
            {
                continue;
            }

            if (!ball.CheckIfCanForceSpit())
            {
                continue;
            }

            ball.CalculateChildSize(fPercent);
            float fMotherLeftSize = 0f;
            float fChildSize = ball.GetAvailableChildSize(ref fMotherLeftSize);
            if (fChildSize < Main.BALL_MIN_SIZE || fMotherLeftSize < Main.BALL_MIN_SIZE || fMotherLeftSize < fChildSize)
            {
                continue;
            }

            ball.ShowTarget(fSpitBallDistance);

            nCount++;
            if (nCount >= nAvailableNum)
            {
                break;
            }
        } // end for
    }

    void EndPreCastSkill_SpitBallHalf()
    {


    }

    public void SpitBallHalf()
    {
        m_nPreCastSkill_Status_SpitHalf = 0;
        m_bPreCastSkill_SpitHalf = false;

        if (!CheckIfCanSpitBallHalf())
        {
            photonView.RPC("RPC_CancelSpitBallHalf", PhotonTargets.All);
            return;
        }

        SetMP(GetMP() - m_fSpitBallHalfMpCost);

        float fShellTime = GetRealShellTime(m_fSpitBallHalfShellTime);
        photonView.RPC("RPC_SpitBallHalf", PhotonTargets.All, (float)PhotonNetwork.time, fShellTime);
    }

    [PunRPC]
    public void RPC_CancelSpitBallHalf()
    {
        Main.s_Instance.ClearSkillColdDown_SpitBallHalf();
        CSkillSystem.s_Instance.HideProgressBar();
        ClearSpitTarget();
    }



    public bool CheckIfCanSpitBall()
    {
        /*
		if (GetMP () < m_fSpitBallCostMp) {
			Main.s_Instance.g_SystemMsg.SetContent (CMsgSystem.s_Instance.GetMsgContentByType(CMsgSystem.eSysMsgType.mp_not_enough));
			return false;
		}
        */

        bool bSpitSucceeded = false;
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            if (ball.IsDead())
            {
                continue;
            }

            //ball.CalculateChildSize_ByVolume( Main.m_fBallMinVolume ); // 按分出最小单位球来预判，看看能否成功分球
            ball.CalculateChildSize_BySize(Main.BALL_MIN_SIZE);
            float fMotherLeftSize = 0f;
            float fChildSize = ball.GetAvailableChildSize(ref fMotherLeftSize);

            if (fChildSize < Main.BALL_MIN_SIZE || fMotherLeftSize < Main.BALL_MIN_SIZE || fMotherLeftSize < fChildSize)
            {
                continue;
            }

            bSpitSucceeded = true;
            break;
        } // end for

        if (!bSpitSucceeded)
        {
            //  Main.s_Instance.g_SystemMsg.SetContent("队伍中所有母球当前体积都不足以分球。");
            return false;
        }

        return true;
    }

    public void SetSpitPercent(float fPercent)
    {
        m_fSpitPercent = fPercent;
    }

    int m_nWSpitStatus = 0; // 0 - None  1 - 蓄力阶段
    static byte[] _bytesSpitBall = new byte[512];
    public void DoSpit(int nPlatform)
    {
        EndMoveToCenter();

        m_nWSpitStatus = 0;
        if (!CheckIfCanSpitBall())
        {
            photonView.RPC("RPC_CancelSpit", PhotonTargets.All);
            return;
        }

        SetMP(GetMP() - m_fSpitBallCostMp);

        Main.s_Instance.BeginCameraSizeProtect(m_fSpitBallRunTime);
        CSkillSystem.s_Instance.HideProgressBar();

        float fShellTime = GetRealShellTime(m_fSpitBallShellTime);

        StringManager.BeginPushData(_bytesSpitBall);
        StringManager.PushData_Int(0);
        StringManager.PushData_Short((short)nPlatform);
        StringManager.PushData_Float(Main.s_Instance._playeraction.GetWorldCursorPos().x);
        StringManager.PushData_Float(Main.s_Instance._playeraction.GetWorldCursorPos().y);
        StringManager.PushData_Float(GetSpecialDirection().x);
        StringManager.PushData_Float(GetSpecialDirection().y);
        StringManager.PushData_Float(m_fSpitBallDistance);
        StringManager.PushData_Float(m_fSpitBallRunTime);
        StringManager.PushData_Float(fShellTime);



        //// 新流程，优先从大的球分
        List<Ball> lst = null;
        GetSortedBallList(ref lst);
        //// 

        int nNum = 0;
        //for (int i = 0; i < m_lstBalls.Count; i++)
        for (int i = 0; i < lst.Count; i++)
        {
            Ball ball = lst[i];
            if (ball.IsDead())
            {
                continue;
            }

            if (ball.DoNotSpit())
            {
                continue;
            }

            float fMotherLeftVolume = 0f;
            float fChildVolume = 0;
            bool bCanSpit = ball.CalculateForceSpitBallVolumeInfo(m_fSpitPercent, ref fMotherLeftVolume, ref fChildVolume);
            if (!bCanSpit)
            {
                continue;
            }

            Ball ballChild = _PlayerIns.GetOneBallToReuse();
            if (ballChild == null)
            {
                // 队伍中无闲置的球可用了，无法生成新的小球
                continue;
            }

            StringManager.PushData_Short((short)ball.GetIndex());
            StringManager.PushData_Short((short)ballChild.GetIndex());
            StringManager.PushData_Float(fMotherLeftVolume);
            StringManager.PushData_Float(fChildVolume);
            nNum++;
            // ball.Spit(fDelayTime, nPlatform, fChildSize, fMotherLeftSize, fWorldCursorPosX, fWorldCursorPosY, fSpeicalDirX, fSpeicalDirY, fSpitBallRunDistance, fSpitBallRunTime, fShellTime);
        }
        StringManager.SetCurPointerPos(0);
        StringManager.PushData_Int(nNum);
        ////


        RPC_DoSpit( _bytesSpitBall);
    }

    [PunRPC]
    public void RPC_CancelSpit()
    {
        ClearCosmosGunSight();
        CSkillSystem.s_Instance.HideProgressBar();
        Main.s_Instance.ClearSkillColdDown_SpitBall();
        ClearSpitTarget();
        EndForcing();
    }

    List<Vector3> m_lstTempEjectEndPos = new List<Vector3>();

    // [to youhua]分球动作和瞄准器都可以用同步的，OtherClient无须计算

    [PunRPC]
    public void RPC_DoSpit(byte[] bytes)
    {
        m_nWSpitStatus = 0;



        EndForcing();


        if (!IsPlayerInitCompleted())
        {
            return;
        }


        if (IsMainPlayer())
        {
            CAudioManager.s_Instance.PlayAudio(CAudioManager.eAudioId.e_audio_spit);
        }


        m_lstTempEjectEndPos.Clear();

        StringManager.BeginPopData(bytes);
        int nNum = StringManager.PopData_Int();
        int nPlatform = StringManager.PopData_Short();
        float fWorldCursorPosX = StringManager.PopData_Float();
        float fWorldCursorPosY = StringManager.PopData_Float();
        float fSpeicalDirX = StringManager.PopData_Float();
        float fSpeicalDirY = StringManager.PopData_Float();
        float fSpitBallRunDistance = StringManager.PopData_Float();
        float fSpitBallRunTime = StringManager.PopData_Float();
        float fShellTime = StringManager.PopData_Float();
        for (int i = 0; i < nNum; i++)
        {
            int nMotherIndex = StringManager.PopData_Short();
            int nChildIndex = StringManager.PopData_Short();
            Ball ballMother = GetBallByIndex(nMotherIndex);

            float fMotherLeftVolume = StringManager.PopData_Float();
            float fChildVolume = StringManager.PopData_Float();

            if (!ballMother.GetActive())
            {
                // Debug.Log(ballMother.GetIndex() +  "  木有在视野内，不执行分球");
            }
            else
            {

                // right here
                Ball ballChild = GetBallByIndex(nChildIndex);// GetOneBallToReuse(nChildIndex);

                // Debug.Log(ballMother.GetIndex() + " 执行了分球");
                ballMother.SpitBall(ballChild, fMotherLeftVolume, fChildVolume, nPlatform, fWorldCursorPosX, fWorldCursorPosY, fSpeicalDirX, fSpeicalDirY, fSpitBallRunDistance, fSpitBallRunTime, fShellTime);

                /*
                // 预测新分出的球的运行轨迹终点，并记录下来
                if (IsMainPlayer())
                {
                    Vector3 vecDest = new Vector3();
                    vecDest.x = ballChild.GetPos().x + ballChild.GetEjectTotalDis() * ballChild.GetDir().x;
                    vecDest.y = ballChild.GetPos().y + ballChild.GetEjectTotalDis() * ballChild.GetDir().y;
                    vecDest.z = ballChild.GetRadius() * CtrlMode.m_fAcctackRadiusMultiple;
                    m_lstTempEjectEndPos.Add(vecDest);
                }
                */
            }
        }


        BeginClearSpitTargetCount(fSpitBallRunTime);
        /*
        if ( IsMainPlayer() )
        {
            //Ball ballBiggest = GetBallByIndex( PlayerAction.s_nBiggestBallIndex );
            //float fDistance = ballBiggest.GetRadius() * (CtrlMode.m_fAcctackRadiusMultiple + 1);
            //float fTime = fDistance / CtrlMode.m_fSpitBallSpeed;
            //CtrlMode.BeginCameraSizeProtect(fTime , Main.s_Instance.m_fRaiseSpeed / 2f, Main.s_Instance.m_fDownSpeed / 2f);   
            bool bNeedCamSizeProtect = CtrlMode.CheckCameraSizeProtectInfo( ref m_lstTempEjectEndPos ); // 把刚才记录的“预测终点”值传入，计算(预测)镜头的抬升情况。
            if (bNeedCamSizeProtect)
            {
                float fXiShu = CtrlMode.GetChangeAmount() / CtrlMode.CHANGE_AMOUNT_XIHSU;
                float fRaiseSpeed = Main.s_Instance.m_fRaiseSpeed * fXiShu;
                float fDownSpeed = Main.s_Instance.m_fDownSpeed * fXiShu;
                CtrlMode.BeginCameraSizeProtect( Main.s_Instance.m_fCamChangeDelaySpit , fRaiseSpeed, fDownSpeed);
            }
        }
        */
    }



    /*
    [PunRPC]
	public void RPC_DoSpit( float fPercent, int nPlatform, double dOccurTime, float fWorldCursorPosX, float fWorldCursorPosY, float fSpeicalDirX, float fSpeicalDirY, float fSpitBallRunDistance, float fSpitBallRunTime, float fShellTime)
	{


        m_nWSpitStatus = 0;

        EndForcing();

        if (!IsPlayerInitCompleted())
        {
            return;
        }

        if ( IsMainPlayer() )
        {
            CAudioManager.s_Instance.PlayAudio( CAudioManager.eAudioId.e_audio_spit );
        }

		float fDelayTime = (float)( PhotonNetwork.time - dOccurTime );

        

        BeginClearSpitTargetCount (fSpitBallRunTime);
	}
    */

    public void SetSpitParams(float fShootDistance, float fShootSpeed, float fShellTime)
    {
        m_fSpitBallDistance = fShootDistance;
        m_fSpitBallRunTime = fShootSpeed;
        m_fSpitBallShellTime = fShellTime;
    }

    float m_fSpitBallCostMp = 0;
    float m_fSpitBallDistance = 0;
    float m_fSpitBallRunTime = 0;
    float m_fSpitBallShellTime = 0;
    public void BeginForceSpit(float fCostMp, float fMaxForceTime, float fShootDistance, float fShootRunTime, float fShellTime)
    {
        m_fSpitBallShellTime = fShellTime;
        m_fSpitBallCostMp = fCostMp;
        m_fSpitBallDistance = fShootDistance;
        m_fSpitBallRunTime = fShootRunTime;
        m_fForceSpitTotalTime = fMaxForceTime;

        if (!CheckIfCanSpitBall())
        {

            return;
        }


       // CSkillSystem.s_Instance.ShowProgressBar(CSkillSystem.eSkillSysProgressBarType.w_spit_ball);

        //photonView.RPC("RPC_BeginForceSpit", PhotonTargets.All, fCostMp, fMaxForceTime, fShootDistance, fShootRunTime, fShellTime);
        RPC_BeginForceSpit(fCostMp, fMaxForceTime, fShootDistance, fShootRunTime, fShellTime);
    }

    public void EndForcing()
    {
        m_fForcing = false;
        m_fSpitPercent = 0f;
        m_fForceSpitCurTime = 0f;
    }

    [PunRPC]
    public void RPC_BeginForceSpit(float fCostMp, float fMaxForceTime, float fShootDistance, float fShootRunTime, float fShellTime)
    {
        m_fSpitBallShellTime = fShellTime;
        m_fSpitBallCostMp = fCostMp;
        m_fSpitBallDistance = fShootDistance;
        m_fSpitBallRunTime = fShootRunTime;
        m_fForceSpitTotalTime = fMaxForceTime;
        m_nWSpitStatus = 1;
        m_fForcing = true;
    }

    float m_fForceSpitTotalTime = 0;
    bool m_fForcing = false;
    float m_fForceSpitCurTime = 0f;
    float m_fSpitPercent = 0f;
    public void Spitting( /*float fPercent*/ )
    {
        if (!m_fForcing)
        {
            return;
        }

        if (m_nWSpitStatus != 1)
        {
            return;
        }

        if (IsMainPlayer())
        {
            CSkillSystem.s_Instance.UpdateProgressData(m_fForceSpitCurTime, m_fForceSpitTotalTime);
        }


        m_fForceSpitCurTime += Time.deltaTime;
        m_fSpitPercent = m_fForceSpitCurTime / m_fForceSpitTotalTime;
        if (m_fSpitPercent > 0.5f)
        {
            m_fSpitPercent = 0.5f;
        }
        if (m_fForceSpitCurTime >= m_fForceSpitTotalTime)
        {
            if (IsMainPlayer())
            {
                Main.s_Instance.EndSpit();
                return;
            }
        }

        // “瞄准器”机制重新设计了。因为现在是按视野优化，球球不显示的时候未必瞄准器不显示
        List<Ball> lst = null;
        GetSortedBallList(ref lst);
        //for (int i = 0; i < m_lstBalls.Count; i++)
        int nCount = 0;
        int nAvailable = GetCurAvailableBallCount();
        nCount = lst.Count > nAvailable ? nAvailable : lst.Count;
        for (int i = 0; i < nCount; i++)
        {
            Ball ball = lst[i];
            if (ball.IsDead() || (!ball.GetActive()))
            {
                continue;
            }

            int nPlatform = 0;
            if (IsMainPlayer())
            {
                nPlatform = (int)Main.s_Instance.GetPlatformType();
            }
            else
            {
                nPlatform = GetPlatformType();
            }

            if (nPlatform == (int)Main.ePlatformType.mobile)
            {
                vec2Temp = GetSpecialDirection();
                if (vec2Temp.x == 0f && vec2Temp.y == 0)
                {
                    vec2Temp = ball.GetDir();
                    SetSpecialDirection(vec2Temp);
                }
            }
            else
            {
                vec2Temp = ball.GetDir();
            }

            ShowCosmosGunSight(ball, vec2Temp, m_fSpitPercent, m_fSpitBallDistance);
            //ShowGunsight(ball, vec2Temp, m_fSpitPercent, m_fSpitBallDistance);
        } // end for

    }

    public void ClearCosmosGunSight()
    {
        for ( int i = 0; i < m_lstBalls.Count; i++ )
        {
            Ball ball = m_lstBalls[i];
            if ( ball._gunsight != null )
            {
                CGunSightManager.s_Instance.DeleteGunSight(ball._gunsight);
                ball._gunsight = null;
            }
        }
    }
    CGunsight[] m_aryGunsight = new CGunsight[Main.MAX_BALL_NUM];
    public void ShowCosmosGunSight(Ball ball, Vector2 dir, float fPercent, float fDistanceMultiple)
    {
        float fMotherLeftVolume = 0;
        float fChildVolume = 0;
        bool bCanSpit = ball.CalculateForceSpitBallVolumeInfo(m_fSpitPercent, ref fMotherLeftVolume, ref fChildVolume);
        if (!bCanSpit)
        {
            return;
        }

        float fRadius = 0f;
        float fChildSize = CyberTreeMath.Volume2Scale(fChildVolume, ref fRadius);
        if (ball._gunsight == null)
        {
            ball._gunsight = CGunSightManager.s_Instance.NewGunSight();
        }

        ball._gunsight.UpdateStatus(ball.GetPos(), dir, ball.GetRadius(), fChildSize, fDistanceMultiple);
    }
    public void ShowGunsight(Ball ball, Vector2 dir, float fPercent, float fDistanceMultiple)
    {
        CGunsight gunsight = m_aryGunsight[ball.GetIndex()];
        if (gunsight == null)
        {
            gunsight = ResourceManager.s_Instance.NewGunsight();
            m_aryGunsight[ball.GetIndex()] = gunsight;
        }
        float fChildSize = 0f;
        float fMotherVolume = ball.GetVolume();
        float fChildVolume = fMotherVolume * fPercent;
        if (fChildVolume < Main.m_fBallMinVolume)
        {
            fChildVolume = Main.m_fBallMinVolume;
        }
        float fMotherLeftVolume = fMotherVolume - fChildVolume;
        if (fMotherLeftVolume < Main.m_fBallMinVolume)
        {
            return;
        }
        fChildSize = CyberTreeMath.Volume2Scale(fChildVolume);

        gunsight.SetInfo(ball.GetPos(), dir, ball.GetRadius(), fChildSize, fDistanceMultiple);
        gunsight.gameObject.SetActive(true);
    }

    void EndSpit()
    {
        m_bSpitting = false;
    }

    float m_fPreDrawCount = 0.0f;
    void PreDrawLoop()
    {
        if (MapEditor.GetEatMode() != MapEditor.eEatMode.xiqiu)
        {
            return;
        }

        if (m_fPreDrawCount < 0.3f)
        {
            m_fPreDrawCount += Time.deltaTime;
            return;
        }
        m_fPreDrawCount = 0.0f;

        for (int i = m_lstBalls.Count - 1; i >= 0; i--)
        {
            Ball ball = m_lstBalls[i];
            if (ball.IsDead())
            {
                continue;
            }
            ball.DoDraw();
        }
    }

    Vector3 m_vecBalslsCenter = new Vector3();
    float m_fDistanceToSlowDown  =0;
    public Vector3 GetBallsCenter(ref Ball biggest_ball)
    {
        if (m_lstBalls.Count == 0)
        {
            biggest_ball = null;
            return m_vecBalslsCenter;
        }

        m_vecBalslsCenter.x = 0f;
        m_vecBalslsCenter.y = 0f;
        
        bool bFirst = true;
        biggest_ball = null;
        float fTotalSize = 0;
        int nNum = m_lstBalls.Count;
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            if (ball.IsDead())
            {
                continue;
            }

            if (bFirst)
            {
                biggest_ball = ball;
                bFirst = false;
            }
            else
            {
                if (ball.GetVolume() > biggest_ball.GetVolume())
                {
                    biggest_ball = ball;
                }
            }

            fTotalSize += ball.GetSize();

            
            vecTempPos = ball.GetPos();
            m_vecBalslsCenter.x += vecTempPos.x;
            m_vecBalslsCenter.y += vecTempPos.y;
        }
        m_vecBalslsCenter.x /= nNum;
        m_vecBalslsCenter.y /= nNum;

        m_fDistanceToSlowDown = biggest_ball.GetRadius();



        return m_vecBalslsCenter;
    }


    public void MergeAll()
    {
        DoMergeAllToTheBiggestBall();
    }

    List<int> m_lstToMergeAllBalls = new List<int>();
    
    
    public void Suicide()
    {
        for (int i = m_lstBalls.Count - 1; i >= 0; i--) // 要移除节点的时候，必须反向遍历List
        {
            Ball ball = m_lstBalls[i];
            if (!ball.IsDead())
            {
                ball.SetDead( true );
            }
        }
    }

    static List<CMiaoHeEffect> s_lstMiaoHeEffect = new List<CMiaoHeEffect>();

	public void DoMergeAll_New()
	{
		Ball ballBiggist = PlayerAction.s_Instance.GetTheBiggistBall ();

		photonView.RPC("RPC_GenerateMiaoHeEffect", PhotonTargets.All, ballBiggist.GetIndex(), ballBiggist.GetPos().x, ballBiggist.GetPos().y);

		for (int i = m_lstBalls.Count - 1; i >= 0; i--) { // 如果中途要删除元素，则必须反向遍历
			Ball ball = m_lstBalls [i];
			if (ball == ballBiggist) {
				continue;
			}
			ball.SetDead( true );


		} // end for

		ballBiggist.SetVolume ( GetTotalVolume() );
	}

	[PunRPC]
	public void RPC_GenerateMiaoHeEffect( int nBiggistBallIndex, float fDestPosX, float fDestPosY )
	{
		return;

		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			if (ball.GetIndex () == nBiggistBallIndex) {
				continue;
			}
			CMiaoHeEffect effectMiaoHe = ResourceManager.s_Instance.NewMiaoHeEffect();
			vecTempPos.x = fDestPosX;
			vecTempPos.y = fDestPosY;
			vecTempPos.z = 0;
			effectMiaoHe.SetDest (vecTempPos);
			effectMiaoHe.SetScale(ball.GetSize() / 2f);
			effectMiaoHe.SetPos(ball.GetPos());
			effectMiaoHe.SetSprite(AccountData.s_Instance.GetSpriteByItemId(GetSkinId()) );
			effectMiaoHe.SetStartTime(Main.GetTime());
			AddToMiaoHeEffectList(effectMiaoHe);
			s_lstMiaoHeEffect.Add(effectMiaoHe);
		}
	}


    // 把所有的球秒合到最大的球身上去
    public void DoMergeAllToTheBiggestBall()
    {
        if (!IsMainPlayer()) 
        {
            return;
        }

        s_lstMiaoHeEffect.Clear();

        float fTotalVolume = 0f;
        Ball biggestBall = null;

        for ( int i = m_lstBalls.Count - 1; i >= 0; i--) // 如果中途要删除元素，则必须反向遍历
        {
            Ball ball = m_lstBalls[i];


            if (ball.IsDead())
            {
                continue;
            }

            fTotalVolume += ball.GetVolume();
            if (biggestBall == null)
            {
                biggestBall = ball;
                continue;
            }

            if (biggestBall.GetVolume() < ball.GetVolume())
            {
                biggestBall.SetDead(true);
                biggestBall = ball;
            }
            else
            {
                ball.SetDead(true);

                // 产生一个秒合特效
                
                CMiaoHeEffect effectMiaoHe = ResourceManager.s_Instance.NewMiaoHeEffect();
				effectMiaoHe.SetScale(ball.GetSize() / 2f);
                effectMiaoHe.SetPos(ball.GetPos());
                effectMiaoHe.SetSprite(AccountData.s_Instance.GetSpriteByItemId(GetSkinId()) );
                effectMiaoHe.SetStartTime(Main.GetTime());
                AddToMiaoHeEffectList(effectMiaoHe);
                s_lstMiaoHeEffect.Add(effectMiaoHe);
                
              
                
            }
        } // end for
	
        biggestBall.SetVolume(fTotalVolume);

        // 秒合特效
        /*
        CFrameAnimationEffect effect = biggestBall.GetSkillEffect(CSkillSystem.eSkillId.i_merge, 1);
        if (effect)
        {
            effect.gameObject.SetActive(true);
            effect.BeginPlay(false);
        }
        */

        CtrlMode.BeginCamSizeLock(1f); // 镜头锁1秒，不要抬升或下降
    }

    List<CMiaoHeEffect> m_lstMiaoHeEffect = new List<CMiaoHeEffect>();
    void AddToMiaoHeEffectList(CMiaoHeEffect effect)
    {
        m_lstMiaoHeEffect.Add(effect);
    }

    void MiaoHeEffectLoop()
    {
        if (m_lstMiaoHeEffect.Count == 0)
        {
            return;
        }

        for (int i = m_lstMiaoHeEffect.Count - 1; i >= 0; i--)
        {
            CMiaoHeEffect effect = m_lstMiaoHeEffect[i];
			effect.MoveToDest ();
			if (effect.CheckIfArrived ()) {
				ResourceManager.s_Instance.DeleteMiaoHeEffect(effect);
				m_lstMiaoHeEffect.RemoveAt(i);
				continue;
			}
			/*
            if (effect.IsStart())
            {
                if (
                    effect._ballDest == null ||
                    effect._ballDest.IsDead() ||
                    !effect._ballDest.GetActive() ||
                    Vector2.Distance(effect.GetPos(), effect._ballDest.GetPos()) <= 10f
                    )
                {
                    ResourceManager.s_Instance.DeleteMiaoHeEffect(effect);
                    m_lstMiaoHeEffect.RemoveAt(i);
                    continue;
                }
            }
            effect.MoveToDest();
			*/
        }
    }

    public void Test_AddAllBallSize(float fVal)
    {
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            if (ball.IsDead())
            {
                continue;
            }
            ball.SetVolume(ball.GetVolume() + fVal);
        }
    }

    public void SetThornOfBallSize(int nIndex, float fSize)
    {
        photonView.RPC("RPC_SetThornOfBallSize", PhotonTargets.All, nIndex, fSize);
    }

    [PunRPC]
    public void RPC_SetThornOfBallSize(int nIndex, float fSize)
    {
        if (nIndex < 0 || nIndex >= m_lstBalls.Count)
        {
            Debug.LogError("从个的？？nIndex < 0 || nIndex >= m_lstBalls.Count");
            return;
        }

        Ball ball = m_lstBalls[nIndex];
        if (ball.IsDead())
        {
            Debug.LogError("从个的？？这个球都死球了还在SetSize");
            return;
        }

        ball.SetThornSize(fSize);
    }

    public void SetBallSize(int nIndex, float fSize)
    {
        if (!photonView.isMine)
        {
            return;
        }

        photonView.RPC("RPC_SetBallSize", PhotonTargets.All, nIndex, fSize);
    }

    [PunRPC]
    public void RPC_SetBallSize(int nIndex, float fSize)
    {
        if (nIndex < 0 || nIndex >= m_lstBalls.Count)
        {
            Debug.LogError("从个的？？nIndex < 0 || nIndex >= m_lstBalls.Count");
            return;
        }

        Ball ball = m_lstBalls[nIndex];
        if (ball.IsDead())
        {
            Debug.LogError("从个的？？这个球都死球了还在SetSize");
            return;
        }

        ball.Local_SetSize(fSize);
    }

    public void DeleteBall(int nIndex)
    {
        photonView.RPC("RPC_DeleteBall", PhotonTargets.All, nIndex);
    }

    [PunRPC]
    public void RPC_DeleteBall(int nIndex)
    {
        if (nIndex < 0 || nIndex >= m_lstBalls.Count)
        {
            Debug.LogError("nIndex < 0 || nIndex >= m_lstBalls.Count");
            return;
        }

        Ball ball = m_lstBalls[nIndex];
        ball.SetDead(true);
    }

    public struct sSplitInfo
    {
        public Vector3 pos;
        public Vector2 dir;
        public int my_max_split_num;
        public int my_real_split_num;
        public int split_couting;
        public float real_split_size;
        public float seg_dis;
        public bool dead_after_split;
        public Ball _ball;


    };

    List<sSplitInfo> m_lstRecycledSpiltInfo = new List<sSplitInfo>();
    sSplitInfo ReuseSplitInfo()
    {
        sSplitInfo info;
        if (m_lstRecycledSpiltInfo.Count > 0)
        {
            info = m_lstRecycledSpiltInfo[0];
            m_lstRecycledSpiltInfo.RemoveAt(0);
            return info;
        }
        info = new sSplitInfo();
        return info;
    }

    void RecycleSplitInfo(sSplitInfo info)
    {
        m_lstRecycledSpiltInfo.Add(info);
    }

    void ClearSplitInfoList()
    {
        for (int i = 0; i < m_lstSplitBalls.Count; i++)
        {
            RecycleSplitInfo(m_lstSplitBalls[i]);
        }
        m_lstSplitBalls.Clear();
    }

    public bool CheckIfCanSplit()
    {
        /*
		if (GetMP () < Main.s_Instance.m_fSplitCostMP) {
			Main.s_Instance.g_SystemMsg.SetContent (CMsgSystem.s_Instance.GetMsgContentByType(CMsgSystem.eSysMsgType.mp_not_enough));
			return false;
		}
        */
        return true;
    }

    bool m_bSplitting = false;
    int m_nCurSplitTimes = 0;
    int m_nRealSplitNum = 0;
    List<sSplitInfo> m_lstSplitBalls = new List<sSplitInfo>();
    static byte[] _bytesSplit = new byte[90];
    public bool OneBtnSplitBall(int nPlatform, Vector3 vecWorldCursorPos, float fMpCost, float fDistance, float fSplitBallNum, float fRunTime, float fShellTime)
    {
        // 废弃以前的“R键分球”流程
        int nCurLiveBallNum = 0;
        Ball ball = GetOneLiveBall(ref nCurLiveBallNum); // 暂定只有一个球的时候才分
        if (nCurLiveBallNum > 1)
        {
            //   Main.s_Instance.g_SystemMsg.SetContent( "只有一个球的时候才能一键分" );
            return false;
        }

        // 计算这个球可不可以分够策划配置的个数，如果不能，那么可以具体可以分多少个
        int nExpectNum = (int)fSplitBallNum;
        int nRealNum = 0;
        float fChildSize = 0.0f;
        float fMotherVolume = ball.GetVolume();
        float fChildVolume = 0f;
        for (nRealNum = nExpectNum; nRealNum >= 2; nRealNum--)
        { // 至少分为2个，最多分策划配置的个数
            fChildVolume = fMotherVolume / nRealNum;
            fChildSize = CyberTreeMath.Volume2Scale(fChildVolume);
            if (fChildSize >= Main.BALL_MIN_SIZE)
            {
                break;
            }
        }
        int nCurAvailableNum = GetCurAvailableBallCount();
        if (nRealNum > nCurAvailableNum - 1)
        {
            nRealNum = nCurAvailableNum - 1;
        }
        if (nRealNum < 2)
        {
            // Main.s_Instance.g_SystemMsg.SetContent("当前的母球体积不足以一键分球");
            return false;
        }

        if (!CheckIfCanSplit())
        {
            return false;
        }

        SetMP(GetMP() - fMpCost);

        SetBallSize(ball.GetIndex(), fChildSize);
        //ball.Local_SetSize(fChildSize);

        int nChildNum = nRealNum - 1;

        Vector2 vecSplitDir = vecWorldCursorPos - ball.GetPos();
        vecSplitDir.Normalize();
        float fTotalDistance = Vector2.Distance(vecWorldCursorPos, ball.GetPos());
        if (fTotalDistance > fDistance)
        {
            fTotalDistance = fDistance;
        }

        int nPointer = 0;
        BitConverter.GetBytes(nRealNum).CopyTo(_bytesSplit, nPointer);
        nPointer += sizeof(byte);
        BitConverter.GetBytes(ball.GetIndex()).CopyTo(_bytesSplit, nPointer);
        nPointer += sizeof(byte);
        BitConverter.GetBytes(vecSplitDir.x).CopyTo(_bytesSplit, nPointer);
        nPointer += sizeof(float);
        BitConverter.GetBytes(vecSplitDir.y).CopyTo(_bytesSplit, nPointer);
        nPointer += sizeof(float);
        BitConverter.GetBytes(fTotalDistance).CopyTo(_bytesSplit, nPointer);
        nPointer += sizeof(float);
        BitConverter.GetBytes(fRunTime).CopyTo(_bytesSplit, nPointer);
        nPointer += sizeof(float);
        BitConverter.GetBytes(fShellTime).CopyTo(_bytesSplit, nPointer);
        nPointer += sizeof(float);
        BitConverter.GetBytes(fChildSize).CopyTo(_bytesSplit, nPointer);
        nPointer += sizeof(float);
        BitConverter.GetBytes(PhotonNetwork.time).CopyTo(_bytesSplit, nPointer);
        nPointer += sizeof(double);

        GetSomeLiveBallIndex(nChildNum);
        BitConverter.GetBytes(nChildNum).CopyTo(_bytesSplit, nPointer);
        nPointer += sizeof(byte);

        if (nChildNum != m_lstSomeAvailabelBallIndex.Count)
        {
            Debug.LogError("有Bug：nChildNum != m_lstSomeAvailabelBallIndex.Count");
        }

        for (int i = 0; i < m_lstSomeAvailabelBallIndex.Count; i++)
        { // 比想要分的个数(nRealNum)少一个，因为母球自己还占一个
            BitConverter.GetBytes(m_lstSomeAvailabelBallIndex[i]).CopyTo(_bytesSplit, nPointer);
            nPointer += sizeof(byte);
        }

        photonView.RPC("RPC_OneBtnSplitBall_New", PhotonTargets.All, _bytesSplit);

        return true;
    }

    [PunRPC]
    public void RPC_OneBtnSplitBall_New(byte[] bytes)
    {
    }

    public float GetRealShellTime(float fConfigShellTime)
    {
        // Buff对壳时间的影响
        fConfigShellTime *= GetBuffEffect_ShellTime();

        // 道具对壳的时间的影响
        fConfigShellTime *= (1 + GetDecreaseShellTime());

        return fConfigShellTime;
    }

    public struct sExplodeInfo
    {

    };

    List<int> m_lstToExplodeBalls = new List<int>();
    public void OneBtnExplode(string szEXplodeThornId)
    {/*
		m_lstToExplodeBalls.Clear ();
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			if (ball.IsDead ()) {
				continue;
			}
			m_lstToExplodeBalls.Add ( ball.GetIndex() );
		}

        string szThornId = "";
        int nIndex = GetOwnerId();
        if ( nIndex > 4 )
        {
            nIndex = GetOwnerId() % 4;
        }
        if ( nIndex == 1 )
        {
            szThornId = "0_3";
        }
        else if (nIndex == 2)
        {
            szThornId = "0_4";
        }
        else if (nIndex == 3)
        {
            szThornId = "0_5";
        }
        else if (nIndex == 4)
        {
            szThornId = "0_6";
        }
        szThornId = "0_5";
       */
        int nLiveBallNum = GetCurLiveBallNum();
        if (nLiveBallNum > 1)
        {
            return;
        }

        CMonsterEditor.sThornConfig config = CMonsterEditor.s_Instance.GetMonsterConfigById(szEXplodeThornId);
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            if (ball.IsDead())
            {
                continue;
            }
            ball.Explode(config);
            CGrowSystem.s_Instance.SetEatThornNum(CGrowSystem.s_Instance.GetEatThornNum() + 1);
            return;
        }
    }


    public int GetCurMainPlayerBallCount()
    {
        return m_lstBalls.Count;
    }

    public int GetCurBallCount()
    {
        return m_lstBalls.Count;
    }

    public int GetCurTotalBallCountIncludeRecycled()
    {
        return (m_lstBalls.Count + m_lstRecycledBalls.Count);
    }

    public int GetAvailableBallCount()
    {
        return (int)Main.s_Instance.m_fMaxBallNumPerPlayer - GetCurBallCount();
    }




    public Ball GetOneAvailableBall()
    {
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            if (ball && (!ball.IsDead()))
            {
                return ball;
            }
        }
        return null;
    }

    public List<Ball> GetBallList()
    {
        return m_lstBalls;
    }

    public void SetBallList(List<Ball> lstBalls )
    {
        m_lstBalls = lstBalls;
    }

    public void GetSortedBallList(ref List<Ball> lstBalls)
    {
        SortBallsByVolume();
        lstBalls = m_lstSortedBallList;
    }

    public void GetBallList(ref List<Ball> lstBalls)
    {
        lstBalls = m_lstBalls;
    }


    public bool CheckIfCanSpitSpore()
    {
        /*
		if (GetMP () < m_fSpitSporeMpCost) {
			Main.s_Instance.g_SystemMsg.SetContent (CMsgSystem.s_Instance.GetMsgContentByType(CMsgSystem.eSysMsgType.mp_not_enough));
			return false;
		}
        */

        return true;
    }

    float m_fUpdateBuffCount = 0f;
    const float c_UpdateBuffInterval = 1f;
    void UpdateBuffEffect()
    {
        m_fUpdateBuffCount += Time.deltaTime;
        if (m_fUpdateBuffCount < c_UpdateBuffInterval)
        {
            return;
        }
        m_fUpdateBuffCount = 0;

        UpdateBuffEffect_ShellTime();
    }



    float UpdateBuffEffect_SpitSporeSpeed()
    {
        float fSpitSporeInterval = m_fSpitSporeTimeInterval;/*Main.s_Instance.m_fSpitSporeTimeInterval*/;
        CBuffEditor.sBuffConfig buff_config;
        for (int i = 0; i < m_lstBuff.Count; i++)
        {
            CBuff buff = m_lstBuff[i];
            buff_config = buff.GetConfig();
            if (buff_config.func == (int)CBuffEditor.eBuffFunc.player_spit_spore_speed)
            {
                fSpitSporeInterval *= (1f + buff_config.aryValues[0]);
            }
        }

        return fSpitSporeInterval;

    }


    float UpdateBuffEffect_SpitSporeDis()
    {
        //m_fSpitSporeDis = Main.s_Instance.m_fSpitSporeRunDistance;
        float fSpitSporeDis = m_fSpitSporeDis;
        CBuffEditor.sBuffConfig buff_config;
        for (int i = 0; i < m_lstBuff.Count; i++)
        {
            CBuff buff = m_lstBuff[i];
            buff_config = buff.GetConfig();
            if (buff_config.func == (int)CBuffEditor.eBuffFunc.player_spit_spore_distance)
            {
                fSpitSporeDis *= (1f + buff_config.aryValues[0]);
            }
        }

        return fSpitSporeDis;

    }

    string m_szSpitSporeMonsterId = "";
    float m_fSpitSporeDis = 0f;
    float m_fSpitSporeTimeInterval = 0f;
    float m_fSpitSporeMpCost = 0f;
    bool m_bSpittingSpore = false;
    float m_fSporeEjectSpeed = 0;
    int m_nCurSporeLevel = 0;

    public void BeginSpitSpore()
    {
        m_fGenerateSporeIdTimeCount = 1f;


        // 播放吐孢子的骨骼动画
        /*
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            CSpineManager.s_Instance.PlayBallAnimation(ball, CSpineManager.eBallSpineType.spit_spore, true, 0.2f);
        }
        */

        /*
		if (!CheckIfCanSpitSpore ()) {
			return;
		}
        */
        //// 获取该技能当前的参数（依据该技能当前累积的技能点）

        /*
        bool bCanUseSkill = true;
        CSkillSystem.sSkillParam skill_param = CSkillSystem.s_Instance.GetSkillParamById(CSkillSystem.eSkillId.q_spore, ref bCanUseSkill);
        if (bCanUseSkill)
        {
            float fMpCost = skill_param.aryValues[0];
            float fShootSpeed = skill_param.aryValues[2];
            float fShootDistance = skill_param.aryValues[3];
            string szMonsterId = skill_param.szValue;
            m_nCurSporeLevel = skill_param.nCurLevel;
            photonView.RPC("RPC_BeginSpitSpore", PhotonTargets.All, fMpCost, fShootSpeed, fShootDistance, szMonsterId);
        }
        */


        m_bSpittingSpore = true;
    }

    public int GetCurSporeLevel()
    {
        return m_nCurSporeLevel;
    }

    [PunRPC]
    public void RPC_BeginSpitSpore(float fMpCost, float fShootSpeed, float fShootDistance, string szMonsterId)
    {
        m_fGenerateSporeIdTimeCount = 1f;
        m_szSpitSporeMonsterId = szMonsterId;
        m_fSpitSporeDis = fShootDistance;// Main.s_Instance.m_fSpitSporeRunDistance;
        m_fSpitSporeTimeInterval = fShootSpeed;// Main.s_Instance.m_fSpitSporeTimeInterval;
        m_fSpitSporeMpCost = fMpCost;
        m_bSpittingSpore = true;

        // 播放吐孢子的骨骼动画
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            CSpineManager.s_Instance.PlayBallAnimation(ball, CSpineManager.eBallSpineType.spit_spore, true);
        }

        m_fSpitSporeTimeCount = 100f;

    }

    public bool IsSpittingSpore()
    {
        return m_bSpittingSpore;
    }

    List<uint> m_lstSporeToSpit = new List<uint>();
    List<uint> lstSporeToSpitTemp = new List<uint>();
    float m_fSpitSporeInterval = 0f;
    float m_fSpitSporeDistance = 0f;
    float m_fSpitSporeTimeCount = 0f;
    float m_fGenerateSporeIdTimeCount = 1f;
    int m_nSporeNumPerSec = 0;
    uint m_uSporeGuid = 0;
    static byte[] _bytesSyncSporeId = new byte[300];
    void GenerateSporeId()
    {
        if (!IsMainPlayer())
        {
            return;
        }

        if (!m_bSpittingSpore)
        {
            return;
        }

        m_fGenerateSporeIdTimeCount += Time.deltaTime;
        if (m_fGenerateSporeIdTimeCount < 0.5f)
        {
            return;
        }
        m_fGenerateSporeIdTimeCount = 0f;

        /////
        if (m_lstSporeToSpit.Count < 64)
        {
            lstSporeToSpitTemp.Clear();
            for (int i = 0; i < 64; i++)
            {
                uint uPlayerSporeId = m_uSporeGuid++;
                m_lstSporeToSpit.Add(uPlayerSporeId);
                lstSporeToSpitTemp.Add(uPlayerSporeId);
            }
            StringManager.BeginPushData(_bytesSyncSporeId);
            StringManager.PushData_Short((short)lstSporeToSpitTemp.Count);
            for (int i = 0; i < lstSporeToSpitTemp.Count; i++)
            {
                StringManager.Push_Uint(lstSporeToSpitTemp[i]);
            }
            lstSporeToSpitTemp.Clear();

            int nBlobSize = StringManager.GetBlobSize();
            //byte[] bytes = StringManager.GetSuitableBlob(nBlobSize);
            //Array.Copy(_bytesSyncSporeId, _bytesSyncSporeId.Take(nBlobSize).ToArray(), nBlobSize);
            if (nBlobSize >= 8)
            {
                photonView.RPC("RPC_SyncSporeId", PhotonTargets.Others, _bytesSyncSporeId.Take(nBlobSize).ToArray());
            }
        }

        //// end

    }

    [PunRPC]
    public void RPC_SyncSporeId(byte[] bytes)
    {
        if (IsMainPlayer())
        {
            return;
        }

        StringManager.BeginPopData(bytes);
        int num = StringManager.PopData_Short();
        for (int i = 0; i < num; i++)
        {
            uint uPlayerGuid = StringManager.PopData_Uint();
            m_lstSporeToSpit.Add(uPlayerGuid);
        }
    }

    List<CSpore> m_lstSporeToSync = new List<CSpore>();
    uint m_uSporeIdCount = 0;
    public uint GenerateSporeGuid()
    {
        return m_uSporeIdCount++;
    }

    float m_fSpitSporeBlingCount = 0;
    public void SpittingSpore_New()
    {
        if (!IsMainPlayer()) // 只有MainPlayer才主动执行吐孢子动作。其余客户端的孢子都是主循环同步过去的
        {
            return;
        }

        if (!m_bSpittingSpore)
        {
            return;
        }

        m_fSpitSporeBlingCount += Time.deltaTime;
        if (m_fSpitSporeBlingCount >= CSkillSystem.s_Instance.m_SpitSporeParam.fSpitInterval * 0.5f)
        {
            CSkillSystem.s_Instance.SporeBtnBling();

            m_fSpitSporeBlingCount = 0;
        }

        m_fSpitSporeTimeCount += Time.deltaTime;
        if (m_fSpitSporeTimeCount < CSkillSystem.s_Instance.m_SpitSporeParam.fSpitInterval)
        {
            return;
        }
        m_fSpitSporeTimeCount = 0;
       
        CAudioManager.s_Instance.PlayAudio(CAudioManager.eAudioId.e_audio_spore);

        bool bSpitSucceed = false;
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            if (ball.IsDead())
            {
                continue;
            }

            if ( Main.s_Instance.m_MainPlayer.m_ballBiggest == ball && Main.s_Instance.m_MainPlayer.GetCurLiveBallNum() > 1 )
            {
                continue;
            }

            CSpore spore = ball.SpitSpore();
            if (spore != null)
            {
                bSpitSucceed = true;
                m_lstSporeToSync.Add(spore);
            }
        } // end for

        if (!bSpitSucceed)
        {
            Main.s_Instance.g_SystemMsg.SetContent(CMsgSystem.s_Instance.GetMsgContentByType(CMsgSystem.eSysMsgType.not_enough_volume_to_Spit_spore));
        }

        SyncSporeInfo();
    }

    // 同步孢子信息[to youhua] 这里可以做视野裁剪
    byte[] _bytesSpore = new byte[1024];
    public void SyncSporeInfo()
    {
        if (m_lstSporeToSync.Count == 0)
        {
            return;
        }

        StringManager.BeginPushData(_bytesSpore);
        StringManager.PushData_Short((short)m_lstSporeToSync.Count);
        for (int i = 0; i < m_lstSporeToSync.Count; i++)
        {
            CSpore spore = m_lstSporeToSync[i];
            if (spore.isDead())
            {
                continue;
            }
            StringManager.Push_Uint(spore.GetSporeId());
            StringManager.PushData_Float(spore.GetVolume());
            StringManager.PushData_Float(spore.GetPos().x);
            StringManager.PushData_Float(spore.GetPos().y);
            StringManager.PushData_Float(spore.GetDir().x);
            StringManager.PushData_Float(spore.GetDir().y);
            StringManager.PushData_Float(spore.GetInitSpeed().x);
            StringManager.PushData_Float(spore.GetInitSpeed().y);
            StringManager.PushData_Float(spore.GetAccelerate().x);
            StringManager.PushData_Float(spore.GetAccelerate().y);
        }
        m_lstSporeToSync.Clear();

        photonView.RPC("RPC_SyncSporeInfo", PhotonTargets.Others, _bytesSpore.Take(StringManager.GetBlobSize()).ToArray());
    }

    [PunRPC]
    public void RPC_SyncSporeInfo(byte[] bytes)
    {
        StringManager.BeginPopData(bytes);
        short num = StringManager.PopData_Short();
        for (int i = 0; i < num; i++)
        {
            uint uSporeId = StringManager.PopData_Uint();
            int nPlayerId = GetOwnerId();
            string szKey = CSporeManager.GenerateKey(nPlayerId, uSporeId);
            if (CSporeManager.s_Instance.CheckIfAlreadyDestroyed(szKey))
            {
                continue;
            }
            CSpore spore = CSporeManager.s_Instance.NewSpore();

            spore.SetPlayerId(nPlayerId);
            spore.SetSporeId(uSporeId);
            spore.SetColor(GetColor());
            CSporeManager.s_Instance.AddSpore(spore);
            float fVolume = StringManager.PopData_Float();
            spore.SetVolume(fVolume);
            vecTempPos.x = StringManager.PopData_Float();
            vecTempPos.y = StringManager.PopData_Float();
            spore.SetPos(vecTempPos);
            // dir
            vecTempDir.x = StringManager.PopData_Float();
            vecTempDir.y = StringManager.PopData_Float();

            // init-speed
            vec2TempPos.x = StringManager.PopData_Float();
            vec2TempPos.y = StringManager.PopData_Float();

            // accelerate
            vec3TempPos.x = StringManager.PopData_Float();
            vec3TempPos.y = StringManager.PopData_Float();
            spore.BeginEject_OtherClient(vecTempDir, vec2TempPos, vec3TempPos, CSpore.eEjectMode.accelerated);
        }
    }

    public void SpittingSpore()
    {
        if (!m_bSpittingSpore)
        {
            return;
        }

        if (IsMainPlayer())
        {
            if (!CheckIfCanSpitSpore())
            {
                EndSpitSpore();
                return;
            }
        }

        m_fSpitSporeInterval = UpdateBuffEffect_SpitSporeSpeed();
        m_fSpitSporeDistance = UpdateBuffEffect_SpitSporeDis();
        //Main.s_Instance.g_SystemMsg.SetContent ("配置的射击时间间隔和距离分别是：" + "(" + Main.s_Instance.m_fSpitSporeTimeInterval + "," + Main.s_Instance.m_fSpitSporeRunDistance +") , Buff加成后是：（" + m_fSpitSporeTimeInterval + "," + m_fSpitSporeDis + ")" );

        m_fSpitSporeTimeCount += Time.deltaTime;
        if (m_fSpitSporeTimeCount < m_fSpitSporeInterval/*Main.s_Instance.m_fSpitSporeTimeInterval*/)
        {
            return;
        }

        if (IsMainPlayer())
        {
            CAudioManager.s_Instance.PlayAudio(CAudioManager.eAudioId.e_audio_spore);
        }
        if (m_lstSporeToSpit.Count == 0)
        {
            return;
        }

        m_fSpitSporeTimeCount = 0f;

        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            if (ball.IsDead())
            {
                continue;
            }
            uint uPlayerSporeGuid = m_lstSporeToSpit[0];
            m_lstSporeToSpit.RemoveAt(0);
            bool bRet = ball.SpitBean(m_fSpitSporeDistance, m_szSpitSporeMonsterId, uPlayerSporeGuid);
            if (bRet)
            {
                if (IsMainPlayer())
                {
                    SetMP(GetMP() - m_fSpitSporeMpCost/*Main.s_Instance.m_fSpitSporeCostMP*/ );
                }
            }
        }
    }

    public void EndSpitSpore()
    {
        CSkillSystem.s_Instance.EndSporeBtnBling( );
        photonView.RPC("RPC_EndSpitSpore", PhotonTargets.All);
    }

    [PunRPC]
    public void RPC_EndSpitSpore()
    {
        m_bSpittingSpore = false;

        // 停止吐孢子的骨骼动画
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            // CSpineManager.s_Instance.StopBallAnimation(ball, 0);
        }
    }

    float m_nAttenuateTime = 0;


    public void BeginBeanSprayPreDraw(bool bAll, int nSprayId = 0)
    {
        photonView.RPC("RPC_BeginBeanSprayPreDraw", PhotonTargets.All, (float)PhotonNetwork.time, bAll, nSprayId);
    }

    [PunRPC]
    public void RPC_BeginBeanSprayPreDraw(float fPreDrawBeginTinme, bool bAll, int nSprayId)
    {
        MapEditor.s_Instance.BeginSprayPreDraw(fPreDrawBeginTinme, bAll, nSprayId);
    }

    public void SprayEatBall(int nSprayGUID, float fEatSize)
    {
        photonView.RPC("RPC_SprayEatBall", PhotonTargets.All, nSprayGUID, fEatSize);

    }

    [PunRPC]
    public void RPC_SprayEatBall(int nSprayGUID, float fEatSize)
    {
        MapEditor.s_Instance.SprayEatBall(nSprayGUID, fEatSize);
    }

    public static Player FindPlayerByOwnerId(int nOwnerId)
    {
        GameObject goPlayer = GameObject.Find("Balls/player_" + nOwnerId);
        if (goPlayer == null)
        {
            Debug.LogError("player error");
            return null;
        }
        Player player = goPlayer.GetComponent<Player>();
        return player;
    }

    public Ball FindBallByViewId(int nViewId)
    {
        /*
		for (int i = 0; i < m_lstBalls.Count; i++) { // 这种查找效率太低，后期优化一下
			if (m_lstBalls [i] != null && m_lstBalls [i].photonView.viewID == nViewId) {
				return m_lstBalls [i];
			}
		}
		*/

        return null;
    }

    public void CaptureOneBall(int nSprayID, int nOwnerId, int nViewId)
    {
        photonView.RPC("RPC_CaptureOneBall", PhotonTargets.All, nSprayID, nOwnerId, nViewId);

    }

    [PunRPC]
    public void RPC_CaptureOneBall(int nSprayID, int nOwnerId, int nViewID)
    {
        Spray spray = MapEditor.s_Instance.FindSprayById(nSprayID);
        if (spray == null)
        {
            Debug.LogError("RPC_CaptureOneBall spray null");
            return;
        }
        Player player = Player.FindPlayerByOwnerId(nOwnerId);
        if (player == null)
        {
            Debug.LogError("RPC_CaptureOneBall player null");
            return;
        }
        Ball ball = player.FindBallByViewId(nViewID);
        if (ball == null)
        {
            Debug.LogError("RPC_CaptureOneBall ball null");
            return;
        }
        spray.DoCaptureOneBall(ball);
    }

    public void ReleaseOneBall(int nSprayID, int nOwnerId, int nViewId)
    {
        photonView.RPC("RPC_ReleaseOneBall", PhotonTargets.All, nSprayID, nOwnerId, nViewId);

    }

    [PunRPC]
    public void RPC_ReleaseOneBall(int nSprayID, int nOwnerId, int nViewID)
    {
        Spray spray = MapEditor.s_Instance.FindSprayById(nSprayID);
        if (spray == null)
        {
            Debug.LogError("RPC_CaptureOneBall spray null");
            return;
        }
        Player player = Player.FindPlayerByOwnerId(nOwnerId);
        if (player == null)
        {
            Debug.LogError("RPC_CaptureOneBall player null");
            return;
        }
        Ball ball = player.FindBallByViewId(nViewID);
        if (ball == null)
        {
            Debug.LogError("RPC_CaptureOneBall ball null");
            return;
        }
        spray.DoReleaseOneBall(ball);
    }


    List<int> m_lstLiveBall = new List<int>();

    public void RefreshLiveBalls()
    {

    }

    public Ball GetBallByIndex(int nIndex)
    {
        for ( int i = 0; i < m_lstBalls.Count; i++ )
        {
            Ball ball = m_lstBalls[i];
            if (ball.GetIndex() == nIndex)
            {
                return ball;
            }
        }
        return null;
    }

    int m_nBallNum = 0;
    Ball ReallyInstantiateOneBall()
    {
        vecTempPos.x = -10000f;
        vecTempPos.y = -10000f;
        GameObject goBall = Main.s_Instance.PhotonInstantiate(Main.s_Instance.m_preBall, vecTempPos, Main.s_Instance.s_Params);
        goBall.transform.parent = this.transform;
        goBall.transform.position = vecTempPos;
        Ball ball = goBall.GetComponent<Ball>();
        return ball;
    }

    public void AddOneBall(Ball ball)
    {
        int nIndex = m_lstBalls.Count;
        m_lstBalls.Add(ball);
        ball.SetIndex(nIndex);
        ball.transform.parent = this.transform;
        ball.SetPlayer(this);
        ball.SetDead(true);
    }
    /*
    const float c_fGenerateMeshShapeInterval = 0.1f;
    float m_fGenerateMeshShapeTimeElapse = 0f;
    int m_nGenerateMeshShapeBallIndex = 0;
    int m_nGenerateMeshShapeStatus = 0;// 0 - not begin  1 - doing  2 -done
    public void GenerateMeshShape()
    {
        if (m_nGenerateMeshShapeStatus != 1)
        {
            return;
        }

        Ball ball = m_lstBalls[m_nGenerateMeshShapeBallIndex];
        ball.GenerateMeshShape();
        m_nGenerateMeshShapeBallIndex++;
        if (m_nGenerateMeshShapeBallIndex >= Main.s_Instance.m_fMaxBallNumPerPlayer)
        {
            m_nGenerateMeshShapeStatus = 2;
        }
    }

    public void BeginGenerateMeshShape()
    {
        m_nGenerateMeshShapeStatus = 1;
    }
    */
    List<int> m_lstSomeAvailabelBallIndex = new List<int>();
    public void GetSomeLiveBallIndex(int nNeedNum)
    {
        m_lstSomeAvailabelBallIndex.Clear();
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            if (ball.IsDead())
            {
                m_lstSomeAvailabelBallIndex.Add(ball.GetIndex());
            }
            if (m_lstSomeAvailabelBallIndex.Count >= nNeedNum)
            {
                return;
            }
        }
    }

    public void DoSomeCollisionIgnore()
    {
        for (int i = 0; i < m_lstBalls.Count - 1; i++)
        {
            Ball ball1 = m_lstBalls[i];


            for (int j = i; j < m_lstBalls.Count; j++)
            {
                Ball ball2 = m_lstBalls[j];

                Physics2D.IgnoreCollision(ball1._ColliderDust, ball2._ColliderDust);
                Physics2D.IgnoreCollision(ball1._ColliderDust, ball2._Collider);
                Physics2D.IgnoreCollision(ball1._ColliderDust, ball2._ColliderTemp);

                Physics2D.IgnoreCollision(ball2._ColliderDust, ball1._ColliderDust);
                Physics2D.IgnoreCollision(ball2._ColliderDust, ball1._Collider);
                Physics2D.IgnoreCollision(ball2._ColliderDust, ball1._ColliderTemp);


                Physics2D.IgnoreCollision(ball1._ColliderTemp, ball2._ColliderTemp);
                Physics2D.IgnoreCollision(ball1._ColliderTemp, ball2._Collider);
                Physics2D.IgnoreCollision(ball1._Collider, ball2._ColliderTemp);

                Physics2D.IgnoreCollision(ball1._Trigger, ball2._ColliderDust);
                Physics2D.IgnoreCollision(ball1._Trigger, ball2._Collider);
                Physics2D.IgnoreCollision(ball1._Trigger, ball2._ColliderTemp);
                Physics2D.IgnoreCollision(ball2._Trigger, ball1._ColliderDust);
                Physics2D.IgnoreCollision(ball2._Trigger, ball1._Collider);
                Physics2D.IgnoreCollision(ball2._Trigger, ball1._ColliderTemp);


                Physics2D.IgnoreCollision(ball1._TriggerMergeSelf, ball2._ColliderDust);
                Physics2D.IgnoreCollision(ball1._TriggerMergeSelf, ball2._Collider);
                Physics2D.IgnoreCollision(ball1._TriggerMergeSelf, ball2._ColliderTemp);
                Physics2D.IgnoreCollision(ball2._TriggerMergeSelf, ball1._ColliderDust);
                Physics2D.IgnoreCollision(ball2._TriggerMergeSelf, ball1._Collider);
                Physics2D.IgnoreCollision(ball2._TriggerMergeSelf, ball1._ColliderTemp);


                Physics2D.IgnoreCollision(ball2._Trigger, ball1._Trigger);
                Physics2D.IgnoreCollision(ball2._Trigger, ball1._TriggerMergeSelf);
                Physics2D.IgnoreCollision(ball1._Trigger, ball2._TriggerMergeSelf);


                if (!IsMainPlayer())
                {
                    Physics2D.IgnoreCollision(ball2._TriggerMergeSelf, ball1._TriggerMergeSelf);
                }
            } // end for j
        } // end for i
    }


  

    public Ball GetOneBallToReuse(int nIndex) // 新的“复用”机制下，m_lstBalls的下标不能作为球球的索引号了
    {
        for ( int i = 0; i < m_lstBalls.Count; i++ )
        {
            Ball ball = m_lstBalls[i];
            if (ball.GetIndex() == nIndex)
            {
                return ball;
            }
        }

        Debug.LogError( "球球没找到." );
        return null;
    }

    public List<Ball> m_lstRecycledBalls = new List<Ball>();


    public void EndInitAllBalls()
    {

    }

    bool m_bDead = false;
    public bool CheckIfPlayerDead(int nEaterOwnerId = 0)
    {
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            if (!ball.IsDead())
            { // 只要还有一个球没死，玩家就没死
                m_bDead = false;
                return m_bDead;
            }
        }
        m_bDead = true;

        return m_bDead;
    }

    int m_nEaterId = -1;
    public void AnnouceWhoKillWhom()
    {
        if (m_nEaterId > 0)
        {
            photonView.RPC("RPC_AnnouceWhoKillWhom", PhotonTargets.All, m_nEaterId, GetOwnerId(), CJiShaInfo.s_Instance.m_bFirstBlood);
        }
        m_nEaterId = -1;
    }

    void ProcessKillExp(int nEaterId, int nBeEatenerId)
    {
        if (Main.s_Instance.m_MainPlayer == null) // 观察者客户端木有m_MainPlayer
        {
            return;
        }

        if (Main.s_Instance.m_MainPlayer.GetOwnerId() != nEaterId)
        {
            return;
        }

        Player playerBeEatener = CPlayerManager.s_Instance.GetPlayer(nBeEatenerId);
        CGrowSystem.sLevelConfig level_config = CGrowSystem.s_Instance.GetLevelConfigById(playerBeEatener.GetLevel());
        Main.s_Instance.m_MainPlayer.AddExp(level_config.fKillGainExp);
        Main.s_Instance.m_MainPlayer.AddMoney(level_config.fKillGainMoney);
        //  Main.s_Instance.g_SystemMsg.SetContent ("击杀【" + playerBeEatener.GetLevel() +"】级玩家，获得经验值" + level_config.fKillGainExp + ",获得金钱:" + level_config.fKillGainMoney);
    }

    [PunRPC]
    public void RPC_JiSha( string szEaterAccount, string szDeadMeatAccount )
    {
        CPlayerIns playerInsEater = CPlayerManager.s_Instance.GetPlayerInsByAccount(szEaterAccount);
        CPlayerIns playerInsDeadMeat = CPlayerManager.s_Instance.GetPlayerInsByAccount(szDeadMeatAccount);
        if (playerInsEater == null || playerInsDeadMeat == null )
        {
            Debug.LogError("playerInsEater == null || playerInsDeadMeat == null");
            return;
        }

        CJiShaInfo.s_Instance.PushOneJiShaSysMsgIntoQueue(CJiShaInfo.eJiShaSystemMsgType.e_jisha, playerInsEater, playerInsDeadMeat);


    }

    [PunRPC]
    public void RPC_AnnouceWhoKillWhom(int nEaterId, int nDeadMeatId, bool bFirstBlood)
    {
        Player playerEater = CPlayerManager.s_Instance.GetPlayer(nEaterId);
        if (playerEater == null)
        {
            Debug.LogError("有Bug! playerEater == null");
            return;
        }

        playerEater.IncContinuousKillNum(); // 增加一次连续击杀数
        int nLianXuJiShaNum = playerEater.GetContinuousKillNum();



        Player playerDeadMeat = CPlayerManager.s_Instance.GetPlayer(nDeadMeatId);
        if (playerDeadMeat == null)
        {
            Debug.LogError("有Bug! playerDeadMeat == null");
            return;
        }

        playerDeadMeat.ClearContinuousKillNum(); // 如果死亡，连续击杀数清零
        playerDeadMeat.SetDead(true);

        CJiShaInfo.s_Instance.m_bFirstBlood = false;
        if (bFirstBlood)
        {
            //Debug.Log( "第一滴血！" );
        }


        playerEater.AddOneDuoShaRecord(nDeadMeatId); // 多杀

        int nDuoShaNum = playerEater.GetDuoSha();

        Main.s_Instance.ShowWhoKillWhom(playerEater, playerDeadMeat);
        CPlayerManager.s_Instance.RecordWhoKillWhom(nEaterId, nDeadMeatId);

        List<int> lstAssist = playerDeadMeat.GetAssistPlayerIdList();
        CJiShaInfo.s_Instance.PushOneJiShaSysMsgIntoQueue(CJiShaInfo.eJiShaSystemMsgType.e_jisha,
            nEaterId,
            nDeadMeatId,
            ref lstAssist,
            bFirstBlood,
            0,
            0
            );

        if (nDuoShaNum >= 2)
        {
            CJiShaInfo.s_Instance.PushOneJiShaSysMsgIntoQueue(CJiShaInfo.eJiShaSystemMsgType.e_duosha,
           nEaterId,
           nDeadMeatId,
           ref lstAssist,
           bFirstBlood,
           nDuoShaNum,
           0
           );
        }


        if (nLianXuJiShaNum >= CJiShaInfo.s_Instance.m_nContinuousKillStartThreshold)
        {
            CJiShaInfo.s_Instance.PushOneJiShaSysMsgIntoQueue(CJiShaInfo.eJiShaSystemMsgType.e_lianxusha,
            nEaterId,
            nDeadMeatId,
            ref lstAssist,
            bFirstBlood,
            0,
            nLianXuJiShaNum
            );
        }

    }

    public bool IsDead()
    {
        //return m_bDead;
        if (_PlayerIns)
        {
            return _PlayerIns.IsDead();
        }
        else
        {
            return true;
        }
    }

    bool m_bReborn = false;
    public void SetRebornCompleted(bool val)
    {
        m_bReborn = val;
    }

    public bool IsRebornCompleted()
    {
        return m_bReborn;
    }

    /*
    public void BeginMoveToCenter()
    {
        Ball ballBIggest = null;
        GetBallsCenter(ref ballBIggest, ref vecTempPos);
        
    }
    */

    // 主角所有的球球向中心点靠拢
    public void UpdateMoveToCenterStatus(Ball ball)
    {
        if ( !IsMovingToCenter() )
        {
            return;
        }

        ball.BeginMoveToDestination(m_vecBalslsCenter);
    }
    
    bool m_bAllBallsMovingToCenter = false;
    public Ball m_ballBiggest = null;
    public void BeginMoveToCenter()
    {
        // bool bBiggestBallAsCenter = true;
        m_ballBiggest = null;
        vecTempPos = GetBallsCenter(ref m_ballBiggest);
        /*
        if (bBiggestBallAsCenter)
        {
            photonView.RPC("RPC_BeginMoveToCenter", PhotonTargets.All, vecTempPos.x, vecTempPos.y);
           
        }
        else
        {
            photonView.RPC("RPC_BeginMoveToCenter", PhotonTargets.All, m_vecBalslsCenter.x, m_vecBalslsCenter.y);
        }
        */
       
        RPC_BeginMoveToCenter(m_ballBiggest, vecTempPos.x, vecTempPos.y);
    }

    public bool IsMovingToCenter()
    {
        return m_bAllBallsMovingToCenter;
    }

   
    [PunRPC]
    public void RPC_BeginMoveToCenter( Ball ballBiggest,  float fCenterX, float fCenterY)
    {
        m_bAllBallsMovingToCenter = true;
        if (ballBiggest)
        {
           // CCheat.s_Instance._goMoveToCenter.SetActive( true );
           // CCheat.s_Instance._goMoveToCenter.transform.position = ballBiggest.GetPos();
            ballBiggest.EndMoveToDestination();
            ballBiggest._rigid.bodyType = RigidbodyType2D.Kinematic;
        }

        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            if ( ball == ballBiggest)
            {
                continue;
            }

            ball.BeginMoveToDestination(ballBiggest.GetPos());
        }
    }
    

    void MoveToCenter()
    {
        /*
		if (!m_bAllBallsMovingToCenter) {
			return;
		}

		bool bEnd = true;
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			if (ball.IsDead ()) {
				continue;
			}
			if (ball.GetMovingToCenterStatus () == 2) {
				continue;
			}
			bEnd = false;

			if (ball.GetMovingToCenterStatus () == 0) { // 还没开始移动
				ball.SetBallsCenter ( m_vecBalslsCenter );
				ball.SetMovingToCenterStatus(1);
				ball.CalculateMoveToCenterRealSpeed ();
			}
			else if (ball.GetMovingToCenterStatus () == 1) { // 移动中
				GetBallsCenter ();
				ball.SetBallsCenter ( m_vecBalslsCenter );
				ball.CalculateMoveToCenterRealSpeed ();
				ball.DoMovingToCenter();
				//if (CyberTreeMath.CheckIfArriveDest (ball.GetPos (), m_vecBalslsCenter, 1f)) {
				//	ball.SetMovingToCenterStatus (2);
				//}
			}
		}
		if (bEnd) {
			EndMoveToCenter ();
		}
        */
    }

    public void StartMove()
    {
        EndMoveToCenter();
    }

    public void StopMove()
    {

    }

    public void EndMoveToCenter()
    {
        m_ballBiggest = null;
        m_bAllBallsMovingToCenter = false;
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            ball.EndMoveToDestination();
        }
    }

    public void UpdateIndicatorType(int type)
    {
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            if (type == CtrlMode.DIR_INDICATOR_ARROW)
            {

            }
            else if (type == CtrlMode.DIR_INDICATOR_LINE)
            {

            }
        }

    }

    public void SetPos(Vector2 pos)
    {
        m_vecPos = pos;
    }

    public Vector2 GetPos()
    {
        return m_vecPos;
    }

    int m_nTestExplodeStatus = 0; // 0 - none   1 - 正在炸开    2 - 正在收回

    bool m_bTestPlayer = false;
    void TestExplode()
    {
        /*
		if (!m_bTestPlayer) {
			return;
		}

		if (m_nTestExplodeStatus == 0) {
			int nShit = (int)UnityEngine.Random.Range ( 0.0f, 50.0f );
			if (nShit == 5) {
				float fRunDistance = Main.s_Instance.m_fExplodeRunDistanceMultiple * 20.0f;
				float fInitSpeed = CyberTreeMath.GetV0 (fRunDistance, Main.s_Instance.m_fExplodeRunTime  );
				float fAccelerate = CyberTreeMath.GetA ( fRunDistance, Main.s_Instance.m_fExplodeRunTime);
				for (int i = 0; i < m_lstBalls.Count; i++) {
					Ball ball = m_lstBalls [i];
					Vector2 vecDire = MapEditor.s_Instance.GetExplodeDirection ( i );
					ball._dirx = vecDire.x;
					ball._diry = vecDire.y;
					ball.Local_SetShellInfo ( 8f, 0f, Ball.eShellType.explode_ball );
					ball.Local_BeginEject (fInitSpeed, fAccelerate, Main.s_Instance.m_fExplodeRunTime, false, false, Main.s_Instance.m_fExplodeStayTime);
				}

				m_fExplodeTimeCount = 0f;
				SetTestExplodeStatus (1);
			}
		} else if (m_nTestExplodeStatus == 1) {
			m_fExplodeTimeCount += Time.deltaTime;
			if (m_fExplodeTimeCount >= Main.s_Instance.m_fExplodeRunTime) {
				RPC_BeginMoveToCenter ();
				m_fMoveToCenterTimeCount = 0f;
				SetTestExplodeStatus (2);
			}
		} else if (m_nTestExplodeStatus == 2) {
			m_fMoveToCenterTimeCount += Time.deltaTime;
			if (m_fMoveToCenterTimeCount >= 16f ) {
				for (int i = 0; i < m_lstBalls.Count; i++) {
					Ball ball = m_lstBalls [i];
					ball.Local_SetPos ( GetPos() );
				}
				SetTestExplodeStatus (0);
			}
		}
		*/
    }

   

    void SetTestExplodeStatus(int nStatus)
    {
        m_nTestExplodeStatus = nStatus;
    }

    public void SetTestPlayer(bool bVal)
    {
        m_bTestPlayer = bVal;
    }

    Vector2 m_vecSpecailDirection = new Vector2();
    public void SetSpecialDirection(Vector2 dir)
    {
        m_vecSpecailDirection = dir;
    }

    public Vector2 GetSpecialDirection()
    {
        return m_vecSpecailDirection;
    }


    float m_nAdjustMoveInfoFrameCount = 0f;
    public const float c_nAdjustMoveInfoSyncInterval = 0.1f; // 多少秒校验一次
    static byte[] _bytesAdjustMoveInfo = new byte[10240]; // 这个数据块的大小是可以优化的 poppin to do

    byte[] _bytestRelatedInfo = new byte[1024];
    void AdjustRelatedInfo()
    {
        if (m_lstBeingPushedThorn.Count == 0)
        {
            return;
        }

        StringManager.BeginPushData(_bytestRelatedInfo);
        int nNumOfBeingPushedThorn = m_lstBeingPushedThorn.Count;
        StringManager.PushData_Short((short)nNumOfBeingPushedThorn);
        for (int i = m_lstBeingPushedThorn.Count - 1; i >= 0; i--)
        {
            CMonster monster = m_lstBeingPushedThorn[i];
            CClassEditor.s_Instance.ReInsertMonsterTosBinaryList(monster);
            StringManager.PushData_Int((int)monster.GetGuid());
            StringManager.PushData_Float(monster.GetPos().x);
            StringManager.PushData_Float(monster.GetPos().y);

            if (PhotonNetwork.time - monster.GetFloatParam(0) >= 1f)
            {
                m_lstBeingPushedThorn.RemoveAt(i);
                continue;
            }

        } // end for 

        int nBlobSize = StringManager.GetBlobSize();
        // byte[] bytes = StringManager.GetSuitableBlob(nBlobSize);
        //  Array.Copy(_bytestRelatedInfo, bytes, nBlobSize);
        if (nBlobSize >= 8)
        {
            photonView.RPC("RPC_AdjustRelatedInfo", PhotonTargets.Others, _bytestRelatedInfo.Take(nBlobSize).ToArray());
        }

    }

    [PunRPC]
    public void RPC_AdjustRelatedInfo(byte[] bytes)
    {
        if (!IsPlayerInitCompleted())
        {
            return;
        }

        if (photonView.isMine)
        { // 全量信息是同步给其它客户端的，自己没必要再去解析一遍
            return;
        }

        if (!IsInitCompleted())
        {
            return;
        }

        StringManager.BeginPopData(bytes);
        int nNumOfBeingPushedThorns = StringManager.PopData_Short();
        for (int i = 0; i < nNumOfBeingPushedThorns; i++)
        {
            int nGuidOfThorn = StringManager.PopData_Int();
            CMonster monster = CClassEditor.s_Instance.GetMonsterByGuid((long)nGuidOfThorn);
            if (monster == null)
            {
                Debug.LogError("RPC_AdjustRelatedInfo");
                continue;
            }
            float fPosX = StringManager.PopData_Float();
            float fPosY = StringManager.PopData_Float();
            vecTempPos.x = fPosX;
            vecTempPos.y = fPosY;
            vecTempPos.z = -monster.GetSize();
            monster.SetPos(vecTempPos);
            CClassEditor.s_Instance.ReInsertMonsterTosBinaryList(monster);
        }
    }

    const float c_fSyncMoveInfoInterval = 0.1f;
    float m_nSyncMoveInfoTimeCount = 0f;
    static byte[] _bytesSyncMoveInfo = new byte[1024];
    void SyncMoveInfo()
    {
        /*
        m_nSyncMoveInfoTimeCount += Time.deltaTime;
        if (m_nSyncMoveInfoTimeCount < c_fSyncMoveInfoInterval)
        {
            return;
        }
        m_nSyncMoveInfoTimeCount = 0f;

        StringManager.BeginPushData(_bytesSyncMoveInfo);
        StringManager.PushData_Float(Main.s_Instance._playeraction._world_cursor.transform.position.x);
        StringManager.PushData_Float(Main.s_Instance._playeraction._world_cursor.transform.position.y);
        StringManager.PushData_Float(Main.s_Instance._playeraction._ball_realtime_velocity_without_size);
        StringManager.PushData_Float(GetBaseSpeed());
        StringManager.PushData_Float(Main.s_Instance._playeraction.GetSpecialDirection().x);
        StringManager.PushData_Float(Main.s_Instance._playeraction.GetSpecialDirection().y);
        int nBlobLength = StringManager.GetBlobSize();
        photonView.RPC("RPC_SyncMoveInfo", PhotonTargets.Others, _bytesSyncMoveInfo.Take(nBlobLength).ToArray());
        */
    }

    [PunRPC]
    public void RPC_SyncMoveInfo(byte[] bytes)
    {
        /*
        StringManager.BeginPopData(bytes);

        float fWorldCursorPosX = StringManager.PopData_Float();
        float fWorldCursorPosY = StringManager.PopData_Float();
        float fPlayerRealTimeSpeedWithoutSize = StringManager.PopData_Float();
        float fPlayerBaseSpeed = StringManager.PopData_Float();
        SetBaseSpeed(fPlayerBaseSpeed);
        Local_DoMove(fWorldCursorPosX, fWorldCursorPosY, 0, 0, fPlayerRealTimeSpeedWithoutSize);

        // 双摇杆相关
        float fSpecialDirX = StringManager.PopData_Float();
        float fSpecialDirY = StringManager.PopData_Float();
        vec2Temp.x = fSpecialDirX;
        vec2Temp.y = fSpecialDirY;
        SetSpecialDirection(vec2Temp);
        */
    }

    // 整个帧同步的关键就是这个环节。只要牢牢的抓好这个同步流程，就能保障整个游戏正常运行下去。
    // 同时，最有可能出现性能问题的也是这个环节，后期作多用户同时在线外网测试的时候再优化
    void AdjustMoveInfo(int nDirect = 0, int nOtherOperate = 0)
    {
        if (!m_bMyDataLoaded)
        {
            return;
        }

        if (!photonView.isMine)
        {
            return;
        }

        if (!MapEditor.s_Instance.IsMapInitCompleted())
        {
            return;
        }

        if (m_lstBalls.Count == 0 )
        {  
            return;
        }

        if (_PlayerIns == null)
        {
            return;
        }

       
            m_nAdjustMoveInfoFrameCount += Time.deltaTime;
            if (m_nAdjustMoveInfoFrameCount < c_nAdjustMoveInfoSyncInterval)
            {
                return;
            }
            m_nAdjustMoveInfoFrameCount = 0f;
      
        
        StringManager.BeginPushData(_bytesAdjustMoveInfo);

        //// 把队伍的边界坐标同步过去，这样可以先判断整个队伍在不在视野里面，没必要一直对每个球球作裁剪判断
        float fLeft = PlayerAction.s_Instance.balls_min_position.x;
        float fBottom = PlayerAction.s_Instance.balls_min_position.y;
        float fRight = PlayerAction.s_Instance.balls_max_position.x;
        float fTop = PlayerAction.s_Instance.balls_max_position.y;
        StringManager.PushData_Float(fLeft);
        StringManager.PushData_Float(fBottom);
        StringManager.PushData_Float(fRight);
        StringManager.PushData_Float(fTop);

        // 总体积
        StringManager.PushData_Float(GetTotalVolume());


        int nBallNum = m_lstBalls.Count;
        StringManager.PushData_Short( (short)nBallNum);
                
        // 移动操作
        StringManager.PushData_Float(Main.s_Instance._playeraction._world_cursor.transform.position.x);
        StringManager.PushData_Float(Main.s_Instance._playeraction._world_cursor.transform.position.y);
        StringManager.PushData_Float(Main.s_Instance._playeraction._ball_realtime_velocity_without_size);
        StringManager.PushData_Float(GetBaseSpeed());

        //// Moving
        short byMoving = IsMoving()?(short)1: (short)0;
        StringManager.PushData_Short(byMoving);

        //// MovingToCenter
        short byMovingToCenter = IsMovingToCenter() ?(short)1:(short)0;
        StringManager.PushData_Short(byMovingToCenter);
        if (byMovingToCenter == 1)
        {
            if ( m_ballBiggest )
            {
                StringManager.PushData_Short( (short)m_ballBiggest.GetIndex());
            }
            else
            {
                StringManager.PushData_Short((short)Main.INVALID_BALL_INDEX);
            }
            StringManager.PushData_Float(m_vecBalslsCenter.x);
            StringManager.PushData_Float(m_vecBalslsCenter.y);
        }
        //// end MovingToCenter

        //// 技能相关
        //  ! -- 可选技能
        short nSkillId = (short)_PlayerIns.GetSkillId();
		short nSkillStatus = (short)_PlayerIns.GetSkillStatus((CSkillSystem.eSkillId )nSkillId);
        StringManager.PushData_Short(nSkillId);
        StringManager.PushData_Short(nSkillStatus);

        // 扩张技能
        short nKuoZhangStatus = 0;
        float fKuoZhangParam1 = 0;
        float fKuoZhangParam2 = 0;
        _PlayerIns.GetKuoZhangParams( ref nKuoZhangStatus, ref fKuoZhangParam1, ref fKuoZhangParam2);
        StringManager.PushData_Short(nKuoZhangStatus);
        StringManager.PushData_Float(fKuoZhangParam1);
        StringManager.PushData_Float(fKuoZhangParam2);

        //// end  技能相关




        int nMostBigBallNum = _PlayerIns.m_dicMostBigBallsToShowName.Count;
        StringManager.PushData_Short((short)nMostBigBallNum);
        foreach ( KeyValuePair<int, int> pair in _PlayerIns.m_dicMostBigBallsToShowName )
        {
            StringManager.PushData_Short( (short)pair.Key );
        }
        

        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];

            StringManager.PushData_Short( (short)ball.GetIndex() );


            float fPosX = ball.GetPos().x;
            float fPosY = ball.GetPos().y;
            float fSize = ball.GetSize();
            float fDirX = ball.GetDir().x;
            float fDirY = ball.GetDir().y;
            float fRealTimeSpeed = ball.GetRealTimeSpeed();


            StringManager.PushData_Float(fPosX);
            StringManager.PushData_Float(fPosY);
            StringManager.PushData_Float(fSize);
            //StringManager.PushData_Float(fDirX);
            //StringManager.PushData_Float(fDirY);
            StringManager.PushData_Float(fRealTimeSpeed);
          
            float fCurShellTime = ball.GetCurShellTime();
            float fTotalShellTime = ball.GetTotalShellTime();
            StringManager.PushData_Float(fCurShellTime);
            StringManager.PushData_Float(fTotalShellTime);

            short byDoNotMove = ball.DoNotMove() ?(short)1: (short)0;
            StringManager.PushData_Short(byDoNotMove);
        } // end for


        // BitConverter.GetBytes(nBallNum).CopyTo(_bytesAdjustMoveInfo, sizeof(int));

        int nBlobSize = StringManager.GetBlobSize();

       
       photonView.RPC("RPC_AdjustMoveInfo", PhotonTargets.Others, _bytesAdjustMoveInfo.Take(nBlobSize).ToArray());
      
      


        AdjustRelatedInfo();

    }



    [PunRPC]
    public void RPC_AdjustMoveInfo(byte[] bytes)
    {
        if (!IsPlayerInitCompleted())
        {
            return;
        }

        if (!IsInitCompleted())
        {
            return;
        }

        if ( _PlayerIns == null )
        {
            return;
        }

        CPlayerManager.s_Instance.SetPlayerData_RealTime(GetAccount(), bytes); // 先把其它玩家的状态信息一股脑儿的记下来再说

        StringManager.BeginPopData(bytes);


        float fLeft = StringManager.PopData_Float(); // BitConverter.ToSingle(bytes, nPointer);
        float fBottom = StringManager.PopData_Float(); // BitConverter.ToSingle(bytes, nPointer);
        float fRight = StringManager.PopData_Float(); // BitConverter.ToSingle(bytes, nPointer);
        float fTop = StringManager.PopData_Float(); // BitConverter.ToSingle(bytes, nPointer);

        SetTotalVolume(StringManager.PopData_Float()); // 总体积

        // 对方整个队伍都不在自己的视野范围内
        if (!CCameraManager.s_Instance.CheckIfBallTeamInView(fLeft, fBottom, fRight, fTop))
        {
            // 如果目前存在对方正在显示的球，则全部移除
            _PlayerIns.RemoveAllBalls();

            return;
        }
        

        int nBallsNum = StringManager.PopData_Short();//  BitConverter.ToInt32(bytes, nPointer);

        SetCurLiveBallNum(nBallsNum);

        float fWorldCursorPosX = StringManager.PopData_Float();// BitConverter.ToSingle(bytes, nPointer);
        float fWorldCursorPosY = StringManager.PopData_Float();// BitConverter.ToSingle(bytes, nPointer);
        float fPlayerRealTimeSpeedWithoutSize = StringManager.PopData_Float();// BitConverter.ToSingle(bytes, nPointer);
        float fPlayerBaseSpeed = StringManager.PopData_Float();// BitConverter.ToSingle(bytes, nPointer);
        SetBaseSpeed(fPlayerBaseSpeed);

        //// moving
        int byMoving = StringManager.PopData_Short();
        m_bMoving = (byMoving == 1) ?true:false;

        //// moving to center
        int byCPosBallIndex = Main.INVALID_BALL_INDEX;
        
        int byMovingToCenter = StringManager.PopData_Short();
        m_bAllBallsMovingToCenter = (byMovingToCenter == 1) ?true:false;

        if (m_bAllBallsMovingToCenter)
        {
            byCPosBallIndex = StringManager.PopData_Short();
            if (byCPosBallIndex == Main.INVALID_BALL_INDEX) // no c-pos ball
            {
              
            }
            else
            {
                // 别人的C位球，此时未必在我的视野范围内
            }
            m_vecBalslsCenter.x = StringManager.PopData_Float();
            m_vecBalslsCenter.y = StringManager.PopData_Float();
        }
        //// end Moving to CEnter

        //// 技能相关
        short nSkillId = StringManager.PopData_Short(); // [to youhua]其实对方的技能一登录就知道了，不需要在这里高频同步
        short nSkillStatus = StringManager.PopData_Short();

        // 扩张技能
        short nStatus = StringManager.PopData_Short();
        float fParam1 = StringManager.PopData_Float();
        float fParam2 = StringManager.PopData_Float();
        _PlayerIns.SetKuoZhangParams(nStatus, fParam1, fParam2);

        //// end 技能相关


        _PlayerIns.ClearMostBigBallsToShowName();
        int nMostBigBallNum = StringManager.PopData_Short();
        for ( int i = 0; i < nMostBigBallNum; i++ )
        {
            int nBallIndex = StringManager.PopData_Short();
            _PlayerIns.AddMostBigBallsToShowName(nBallIndex);
        }
       
        for ( int i = 0; i < nBallsNum; i++ ) // 对方当前的活球信息
        {
            Ball ball = null;

            int nBallIndex = StringManager.PopData_Short();
            float fPosX = StringManager.PopData_Float();
            float fPosY = StringManager.PopData_Float();


            float fSize = StringManager.PopData_Float();
            //float fDirX = StringManager.PopData_Float();
            //float fDirY = StringManager.PopData_Float();
            float fRealTimeSpeed = StringManager.PopData_Float();
            float fCurShellTime = StringManager.PopData_Float();
            float fTotalShellTime = StringManager.PopData_Float();

            int byDoNotMove = StringManager.PopData_Short();

            if (CCameraManager.s_Instance.CheckIfBallInView(fPosX, fPosY, CyberTreeMath.Scale2Radius( fSize ))) // 在视野范围内（活球不一定显示，还要在视野范围内才显示）
            {
                bool bNew = false;
                ball = _PlayerIns.GetLiveBall(nBallIndex);   
                if ( ball == null )
                {
                    ball = CBallManager.s_Instance.NewBall();
                    _PlayerIns.ProcessAfterReuse( ball, nBallIndex );
                    bNew = true;
                }
                else
                {

                }

                //// Moving To Center 相关
                if (m_bAllBallsMovingToCenter && nBallIndex != byCPosBallIndex) // C位球不能动
                {
                    ball.BeginMoveToDestination(m_vecBalslsCenter);
                }

                if ( !m_bAllBallsMovingToCenter)
                {
                    ball.EndMoveToDestination();
                }

                // C位球
                if (nBallIndex == byCPosBallIndex)
                {
                    ball.EndMoveToDestination();
                    ball._rigid.bodyType = RigidbodyType2D.Kinematic;
                }
                else
                {
                    ball._rigid.bodyType = RigidbodyType2D.Dynamic;
                }

                //// end “Moving To Center 相关”

                ball.SetDoNotMove(byDoNotMove == 1?true:false);

                ball.SetSize(fSize);

                vecTempPos.x = fPosX;
                vecTempPos.y = fPosY;
                vecTempPos.z = -fSize;
                

                if ( !ball.HaveShell() && fCurShellTime > 0)
                {
                    ball.Local_SetShellInfo(fTotalShellTime, fCurShellTime);
                    ball.Local_BeginShell();
                    ball.UpdateShell();
                }

                if (bNew)
                {
                    ball.SetPos(vecTempPos);
                    ball.SetSizeDirectly();
                }
                else
                {
                    ball.BeginInterpolate(vecTempPos);

                    _PlayerIns.UpdateSkillsStatusForBall(ball); // 如果是new的，这个操作在上面就已经做了
                }

                ball.SetPicked( true );



            }
            else // 不在视野范围内的直接算“死球”。
            {
                
            }
            


        } // end for  ( int i = 0; i < nBallsNum; i++ )

        _PlayerIns.SetSkillStatus((CSkillSystem.eSkillId)nSkillId, nSkillStatus);

        // [to youhua]原则上来说不应该在高频执行的模块进行List遍历，暂时这样。稍后优化
        for ( int i = m_lstBalls.Count - 1; i >= 0; i-- ) 
        {
            Ball ball = m_lstBalls[i];
            if ( !ball.IsPicked() )
            {
                ball.SetDead( true );
                continue;
            }
            ball.SetPicked(false);
        }

        // 上一轮同步时还在显示，这一轮同步已经不在（死亡、或者在视野之外）了的球球，则销毁

       Local_DoMove(fWorldCursorPosX, fWorldCursorPosY, 0, 0, fPlayerRealTimeSpeedWithoutSize);// [to youhua]运动的流畅性要优化一下这里面

        // 注意：新的机制下，m_lstBalls的下标并不能作为球球的索引号了
        /*
                for (int i = 0; i < m_lstBalls.Count; i++)
                {

                    bool bDead = BitConverter.ToBoolean(bytes, nPointer);
                    nPointer += sizeof(bool);

                    Ball ball = m_lstBalls[i];
                    if (bDead) // 死球
                    {
                        ball.SetDead(true);
                        continue;
                    }



                    float fSize = BitConverter.ToSingle(bytes, nPointer);
                    nPointer += sizeof(float);

                    float fPosX = BitConverter.ToSingle(bytes, nPointer);
                    nPointer += sizeof(float);
                    float fPosY = BitConverter.ToSingle(bytes, nPointer);
                    nPointer += sizeof(float);


                    float fCurShellTime = BitConverter.ToSingle(bytes, nPointer);
                    nPointer += sizeof(float);
                    float fTotalShellTime = BitConverter.ToSingle(bytes, nPointer);
                    nPointer += sizeof(float);

                    bool bEjecting = BitConverter.ToBoolean(bytes, nPointer);
                    nPointer += sizeof(bool);



                    //// 解决网络丢包问题的关键之关键就在这一步。视野裁剪。
                    ball.SetDead(false);

                    bool bLastFrameActvie = ball.GetActive();
                    float fLastFrameTotalShellTime = ball.GetTotalShellTime();

                    if ( CCameraManager.s_Instance.CheckIfBallInView(fPosX, fPosY, fSize))
                    {
                        vecTempPos.x = fPosX;
                        vecTempPos.y = fPosY;
                        vecTempPos.z = -fSize;

                        ball.SetPos(vecTempPos);
                        ball.SetSize(fSize);



                        if (bTakeMyDataOnLoaded)
                        {
                            float fRadius = 0f;
                            ball.SetVolume(CyberTreeMath.Scale2Volume(fSize, ref fRadius));
                            ball.SetRadius(fRadius);
                        }

                        ball.SetActive(true);

                        if (ball.IsEjecting() && !bEjecting)
                        {
                            ball.SlowDownEject();
                        }

                        if (!bLastFrameActvie)
                        {

                        }
                        else
                        {

                        }

                        if (fLastFrameTotalShellTime == 0 && fTotalShellTime > 0 && fCurShellTime > 0)
                        {
                            ball.Local_SetShellInfo(fTotalShellTime, fCurShellTime);
                            ball.Local_BeginShell();
                            ball.UpdateShell();
                        }
                    }
                    else
                    {
                        ball.SetActive(false);
                    }

                    //// end 视野裁剪



                } // end for ball_list

                */

    }


    byte[] _bytesDusts = new byte[2000];
    const float c_fSyncDustInterval = 1f;
    float m_fSyncDustTimeCount = 0f;
    public void SyncDustInfo()
    {
        if (!IsMainPlayer())
        {
            return;
        }

        m_fSyncDustTimeCount += Time.deltaTime;
        if (m_fSyncDustTimeCount < c_fSyncDustInterval)
        {
            return;
        }
        m_fSyncDustTimeCount = 0f;

        StringManager.BeginPushData(_bytesDusts);
        short nDustNum = (short)m_dicSyncDust.Count;
        if (nDustNum == 0)
        {
            return;
        }

        StringManager.PushData_Short(nDustNum);
        foreach (KeyValuePair<int, CDust> pair in m_dicSyncDust)
        {
            CDust dust = pair.Value;
            StringManager.PushData_Short((short)dust.GetId());
            StringManager.PushData_Float(dust.GetPos().x);
            StringManager.PushData_Float(dust.GetPos().y);
            StringManager.PushData_Float(dust.GetRotation());
        }
        int nBlobLength = StringManager.GetBlobSize();
        photonView.RPC("RPC_SyncDustInfo", PhotonTargets.Others, _bytesDusts.Take(nBlobLength).ToArray());
    }


    [PunRPC]
    public void RPC_SyncDustInfo(byte[] bytes)
    {
        StringManager.BeginPopData(bytes);
        int nDustNum = StringManager.PopData_Short();
        for (int i = 0; i < nDustNum; i++)
        {
            int nDustdId = StringManager.PopData_Short();
            vecTempPos.x = StringManager.PopData_Float();
            vecTempPos.y = StringManager.PopData_Float();
            vecTempPos.z = 0f;
            float fRotation = StringManager.PopData_Float();
            CStarDust.s_Instance.SetDustPos(nDustdId, vecTempPos);
            CStarDust.s_Instance.SetDustRotation(nDustdId, fRotation);
            CStarDust.s_Instance.AddDustToBeSyncedList(nDustdId);
        }
    }

    static byte[] _bytesPaiHangBang = new byte[64];
    float m_fPaiHangBangSyncTimeCount = 0f;
    public void SyncPaiHangBangInfo()
    {
        if (!photonView.isMine)
        {
            return;
        }

        if (!MapEditor.s_Instance.IsMapInitCompleted())
        {
            return;
        }

        m_fPaiHangBangSyncTimeCount += Time.deltaTime;
        if (m_fPaiHangBangSyncTimeCount < 10f) // 暂定10秒更新一次排行榜的“非重要信息”(而重要信息是实时更新，包括：等级、当前总体积)
        {
            return;
        }
        m_fPaiHangBangSyncTimeCount = 0f;

        StringManager.BeginPushData(_bytesPaiHangBang);
        StringManager.PushData_Float( GetTotalVolume() );
        StringManager.PushData_Short((short)GetKillCount());        // 击杀数
        StringManager.PushData_Short((short)GetBeKilledCount());    // 被杀数
        StringManager.PushData_Short((short)CGrowSystem.s_Instance.GetAssistAttackCount()); // 助攻数
        StringManager.PushData_Short((short)CItemSystem.s_Instance.GetCurNum(2)); // 紫水晶
        StringManager.PushData_Short((short)CItemSystem.s_Instance.GetCurNum(3)); // 黄水晶
        StringManager.PushData_Short((short)GetEatThornNum()); // 吃刺数
        StringManager.PushData_Short((short)CSkillSystem.s_Instance.GetSelectSkillId());
        StringManager.PushData_Short((short)CSkillSystem.s_Instance.GetSelectSkillCurPoint());
        int nBlobLength = StringManager.GetBlobSize();
        //byte[] bytes = StringManager.GetSuitableBlob(nBlobLength);
        //Array.Copy(_bytesPaiHangBang, _bytesPaiHangBang.Take(nBlobLength).ToArray(), nBlobLength);
        if (nBlobLength >= 8)
        {
            photonView.RPC("RPC_SyncPaiHangBangInfo", PhotonTargets.Others, _bytesPaiHangBang.Take(nBlobLength).ToArray());
        }
    }

    [PunRPC]
    public void RPC_SyncPaiHangBangInfo(byte[] bytes)
    {
        StringManager.BeginPopData(bytes);
        float fTotalVolume = StringManager.PopData_Float();
        int nKillCount = StringManager.PopData_Short();
        int nBeingKilledCount = StringManager.PopData_Short();
        int nAssistCount = StringManager.PopData_Short();
        int nItem2 = StringManager.PopData_Short();
        int nItem3 = StringManager.PopData_Short();
        int nEatThornNum = StringManager.PopData_Short();
        int nSelectedSkillId = StringManager.PopData_Short();
        int nSelectedSkillLelve = StringManager.PopData_Short();
        SetTotalVolume(fTotalVolume);
        SetEatThornNum(nEatThornNum);
        SetBeKilledCount(nBeingKilledCount);
        SetAssistAttackCount(nAssistCount);
        SetItemNum(2, nItem2);
        SetItemNum(3, nItem3);
        SetSelectedSkillId(nSelectedSkillId);
        SetSelectedSkillLevel(nSelectedSkillLelve);
    }





    public void ProcessOtherOperate(int nOtherOperate)
    {
        switch (nOtherOperate)
        {
            case 1:
                {
                    EndEject();
                }
                break;
        }
    }

    public void EndEject()
    {
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            if (ball.IsDead())
            {
                continue;
            }
            ball.EndEject();
        }
    }

    public void EatBall(int nPlayerPhotonID, int nBallIndex)
    {

    }

    [PunRPC]
    public void RPC_BeEaten(int nBallIndex, int nEaterOwnerId)
    {
        Ball ball = m_lstBalls[nBallIndex];

        ball.SetActive(false);
        if (photonView.isMine)
        {
            DestroyBall(nBallIndex, nEaterOwnerId);

            if (nEaterOwnerId != GetOwnerId())
            {
                //
#if UNITY_ANDROID
                  Handheld.Vibrate();
#endif
#if UNITY_IPHONE
                  Handheld.Vibrate();
#endif
#if UNITY_WIN
                  
#endif
            }
        }
    }

    public float GetTotalEatArea()
    {
        return m_fTotalEatArea;
    }

    // 衰减统一由Player来操盘。之前的机制是每个球球自己去做衰减
    public float GetEatVolume()
    {
        if (m_fTotalArea == 0)
        {
            return 0;
        }
        return (m_fTotalArea - GetBornBaseTiJi());
    }

    public void SetTotalEatArea(float val)
    {
        m_fTotalEatArea = val;
    }

    public void IncKillCount()
    {
        m_nKillCount++;
        if (IsMainPlayer())
        {
            CGrowSystem.s_Instance.SetKillCount(m_nKillCount);
        }
    }



    public void IncBeKilledCount()
    {
        m_nBeKilled++;
        if (IsMainPlayer())
        {
            CGrowSystem.s_Instance.SetBeingKilledCount(m_nBeKilled);
        }
    }


    [PunRPC]
    public void RPC_EatShengFuPanDingMonster()
    {
        // Main.s_Instance.m_ShengFuPanDingMonster.SetDead( true );
        Main.s_Instance.GameOver(GetPlayerName());
    }

    [PunRPC]
    public void RPC_EatOrphanBall( string szAccount, int nBallIndex )
    {
        CPlayerIns playerIns = CPlayerManager.s_Instance.GetPlayerInsByAccount(szAccount);
        if (playerIns == null)
        {
            Debug.LogError("playerIns == null" );
            return;
        }

        playerIns.DestroyBall(nBallIndex);
    }

    [PunRPC]
    public void RPC_EatSucceed(int nOpponentPlayerId, int nOpponentBallIndex, int nBallIndex, float fNewVolume, float fTotalEatArea, float fExplodeChildVolume, string szBecomeThornConfigId)
    {
        // 吃球操作可以由吃球者和被吃者发起。但是不能重复执行。
        if (!photonView.isMine)
        { // 必须由吃球者的MainPlayer来执行吃球操作，否则时序会混乱
            return;
        }

        Player playerOppoent = CPlayerManager.s_Instance.GetPlayer(nOpponentPlayerId);
        Ball ballOpponent = playerOppoent.GetBallByIndex(nOpponentBallIndex);

        Ball ball = GetBallByIndex(nBallIndex);
        ball.SetVolume(fNewVolume);


        if (fExplodeChildVolume > 0)
        {
            CMonsterEditor.sThornConfig config = CMonsterEditor.s_Instance.GetMonsterConfigById(szBecomeThornConfigId);
            ball.Explode_CausedBySkill(config, fExplodeChildVolume);
        }
    }

    public void DestroyMonster(CMonster monster)
    {
        monster.SetDead(true);

        // 吃场景上的野怪（豆子、刺 等）一律同步。 但是要做一个同步队列，打包同步，不要实时同步
        if (IsMainPlayer())
        {
            if (monster.GetMonsterBuildingType() == CMonsterEditor.eMonsterBuildingType.spore)
            {
                m_lstEatSporeSyncQueue.Add(monster);
            }
            else
            {
                m_lstEatThornSyncQueue.Add(monster);
            }
        }
    }

    public void AddEatenBeanToSyncQueue(int nBeanGuid, int nBeanIdx)
    {
        CBeanCollection.sBeanSyncInfo info;
        info.collection_guid = (ushort)nBeanGuid;
        info.bean_idx = (ushort)nBeanIdx;
        info.fDeadTime = Main.GetTime();
        m_lstEatBeanSyncQueue.Add(info);
    }

    List<CBeanCollection.sBeanSyncInfo> m_lstEatBeanSyncQueue = new List<CBeanCollection.sBeanSyncInfo>(); // 场景豆子
    List<CMonster> m_lstEatThornSyncQueue = new List<CMonster>(); // 其余的刺和豆子
    List<CMonster> m_lstEatSporeSyncQueue = new List<CMonster>(); // 孢子


    static byte[] _bytesEatThornSync = new byte[10240];
    float m_fEatThornSyncCount = 0f;
    void EatThornSyncQueueLoop()
    {
        m_fEatThornSyncCount += Time.deltaTime;
        if (m_fEatThornSyncCount < 1f)
        {
            return;
        }
        m_fEatThornSyncCount = 0f;

        StringManager.BeginPushData(_bytesEatThornSync);
        StringManager.PushData_Short((short)m_lstEatThornSyncQueue.Count);
        StringManager.PushData_Float(Main.GetTime());
        for (int i = 0; i < m_lstEatThornSyncQueue.Count; i++)
        {
            StringManager.PushData_Int((int)m_lstEatThornSyncQueue[i].GetGuid());
        }

        StringManager.PushData_Short((short)m_lstEatSporeSyncQueue.Count);
        for (int i = 0; i < m_lstEatSporeSyncQueue.Count; i++)
        {
            StringManager.Push_Uint((uint)m_lstEatSporeSyncQueue[i].GetGuid());
            StringManager.Push_Uint((uint)m_lstEatSporeSyncQueue[i].GetPlayer().GetOwnerId());
        }

        StringManager.PushData_Short((short)m_lstEatBeanSyncQueue.Count);
        for (int i = 0; i < m_lstEatBeanSyncQueue.Count; i++)
        {
            StringManager.PushData_Short((short)m_lstEatBeanSyncQueue[i].collection_guid);
            StringManager.PushData_Short((short)m_lstEatBeanSyncQueue[i].bean_idx);
        }


        m_lstEatThornSyncQueue.Clear();
        m_lstEatSporeSyncQueue.Clear();
        m_lstEatBeanSyncQueue.Clear();

        int nBlobSize = StringManager.GetBlobSize();
        // byte[] bytes = StringManager.GetSuitableBlob(nBlobSize);
        // Array.Copy(_bytesEatThornSync, bytes, nBlobSize);
        if (nBlobSize > 0)
        {
            photonView.RPC("RPC_EatThornSync", PhotonTargets.All, _bytesEatThornSync.Take(nBlobSize).ToArray());
        }


    }

    [PunRPC]
    public void RPC_EatThornSync(byte[] bytes)
    {
        StringManager.BeginPopData(bytes);
        int num = StringManager.PopData_Short();
        float fTime = StringManager.PopData_Float();
        for (int i = 0; i < num; i++)
        {
            long lGuid = StringManager.PopData_Int();
            CClassEditor.s_Instance.RemoveThorn(lGuid, fTime);
        }

        num = StringManager.PopData_Short();
        for (int i = 0; i < num; i++)
        {
            uint uPlayerSporeGuid = StringManager.PopData_Uint();
            uint uPlayerId = StringManager.PopData_Uint();
            CClassEditor.s_Instance.RemoveSpore(uPlayerSporeGuid, uPlayerId);
        }

        num = StringManager.PopData_Short(); ;
        for (int i = 0; i < num; i++)
        {
            short uCollectionGuid = StringManager.PopData_Short();
            short uBeanIdx = StringManager.PopData_Short();
            CMonsterEditor.s_Instance.RemoveBean(uCollectionGuid, uBeanIdx);
        }
    }

    [PunRPC]
    public void EatBall_Process(string szEaterAcccount, int nEaterBallIndex, string szPoorThingAcccount, int nPoorThingBallIndex, float fEaterNewVolume, float fExplodeChildVolume, string szBecomeThornConfigId)
    {
        CPlayerIns playerIns = CPlayerManager.s_Instance.GetPlayerInsByAccount(szEaterAcccount);
        if (playerIns == null)
        {
            return;
        }
        if (!playerIns.IsMainPlayer())
        {
            return;
        }
        Ball ball = playerIns.GetBallByIndex(nEaterBallIndex);
        ball.SetVolume(fEaterNewVolume);


        if (fExplodeChildVolume > 0)
        {
            CMonsterEditor.sThornConfig config = CMonsterEditor.s_Instance.GetMonsterConfigById(szBecomeThornConfigId);
            ball.Explode_CausedBySkill(config, fExplodeChildVolume);
        }
    }

    // 现在不依托于Player（一掉线就自动销毁），也不依托于PhotonPlayerId（重连之后这个连接号会重新分配，变成另外一个数字）
    [PunRPC]
    public void DestroyBall_New(string szEaterAcccount, int nEaterBallIndex, string szPoorThingAcccount, int nPoorThingBallIndex)
    {
        CPlayerIns playerIns = CPlayerManager.s_Instance.GetPlayerInsByAccount(szPoorThingAcccount);
        if (playerIns == null)
        {
            return;
        }
        playerIns.RemoveOneBall(nPoorThingBallIndex);

        if (playerIns.IsMainPlayer() || (playerIns.IsOrphan() && PhotonNetwork.isMasterClient))//[orphan]
        {
            if ( playerIns.CheckIfPlayerDead() ) // 判断下玩家当前被击杀没有
            {
                if (!StringManager.Equal(szEaterAcccount, szPoorThingAcccount))
                {
                    photonView.RPC("RPC_JiSha", PhotonTargets.All, szEaterAcccount, szPoorThingAcccount);
                }
            }
        }
    }

    public void DestroyBall(int nBallIndex, int nEaterOwnerId = 0, int nEaterBallIndex = -1) // 必须由MainPlayer来执行销毁球操作，否则时序会混乱
    {
        if (!photonView.isMine)
        { // 必须由MainPlayer来执行销毁球操作，否则时序会混乱
            return;
        }
        photonView.RPC("RPC_DestroyBall", PhotonTargets.All, nBallIndex, nEaterOwnerId, nEaterBallIndex);
    }

    // right here
    [PunRPC]
    public void RPC_DestroyBall(int nBallIndex, int nEaterOwnerId = 0, int nEaterBallIndex = -1)
    {
        Ball ball = m_lstBalls[nBallIndex];
        ball.SetDead(true, nEaterOwnerId);
        m_nEaterId = nEaterOwnerId;
        if (GetOwnerId() == ball._Player.GetOwnerId())
        {
            if (CheckIfPlayerDead(nEaterOwnerId))// 判断下整个玩家死没有
            {
                int nTheDeadMeatId = GetOwnerId();
                CalculateTheAssistAttackInfo(nEaterOwnerId, nTheDeadMeatId);
                ProcessKillExp(nEaterOwnerId, nTheDeadMeatId);
            }
        }

        if (nEaterOwnerId > 0)
        {
            RecordTheAssistAttackTime(nEaterOwnerId);
        }

        if (nEaterBallIndex >= 0)
        {
            Player playerEater = CPlayerManager.s_Instance.GetPlayer(nEaterOwnerId);
            if (playerEater)
            {
                Ball ballEater = playerEater.GetBallByIndex(nEaterBallIndex);
                if (!ballEater.IsDead() && ballEater.GetActive())
                {
                    //// 血迹
                    if (!Ball.IsTeammate(ball, ballEater))
                    {
                        // 喷射血迹
                        vecTempPos = ballEater.GetPos();
                        vecTempPos.x += ballEater.GetRadius() * ballEater.GetDir().x * 1.2f;
                        vecTempPos.y += ballEater.GetRadius() * ballEater.GetDir().y * 1.2f;
                        vecTempDir = ballEater.GetDir();
                        float fScale = ball.GetSize() / CBloodStainManager.s_Instance.GetScaleRatioByType(CBloodStainManager.eBloodStainType.dead_spray);
                        CBloodStainManager.s_Instance.AddBloodStain(vecTempPos, vecTempDir, fScale, fScale, CBloodStainManager.eBloodStainType.dead_spray, ball._Player.GetColor());

                        // 原地血迹
                        vecTempPos = ballEater.GetPos();
                        CBloodStainManager.s_Instance.AddBloodStain(vecTempPos, vecTempDir, fScale, fScale, CBloodStainManager.eBloodStainType.dead_static, ball._Player.GetColor());
                    }
                    //// end 血迹
                }
            }
        }
    }

    Dictionary<int, float> m_dicAssistAttack = new Dictionary<int, float>();
    public void RecordTheAssistAttackTime(int nEaterOwnerId)
    {
        if (nEaterOwnerId == 0)
        {
            return;
        }

        m_dicAssistAttack[nEaterOwnerId] = Main.GetTime();
    }

    List<int> m_lstValidAssitPlayerId = new List<int>();
    public void CalculateTheAssistAttackInfo(int nKillerId, int nTheDeadMeatId)
    {
        m_lstValidAssitPlayerId.Clear();
        foreach (KeyValuePair<int, float> pair in m_dicAssistAttack)
        {
            if (Main.GetTime() - pair.Value > CJiShaInfo.s_Instance.m_fAssistTime) // 超过了助攻有效时间
            {
                continue;
            }

            // 击杀者就不要算在助攻里面了
            if (pair.Key == nKillerId)
            {
                continue;
            }

            // 自己吃自己的球不算助攻
            if (nTheDeadMeatId == pair.Key)
            {
                continue;
            }

            Player player = CPlayerManager.s_Instance.GetPlayer(pair.Key);

            m_lstValidAssitPlayerId.Add(pair.Key);
            player.IncAssistAttackCount();
        }
    }

    public List<int> GetAssistPlayerIdList()
    {
        return m_lstValidAssitPlayerId;
    }


    public void IncAssistAttackCount()
    {
        m_IncAssistAttackCount++;
        if (IsMainPlayer())
        {
            CGrowSystem.s_Instance.SetAssistAttackCount(m_IncAssistAttackCount);
        }
    }

    public void ProcessPlayerDead()
    {
        if (!IsMainPlayer())
        {
            return;
        }

        float fBaseVolumeByItem = GetBaseVolumeByItem();
        fBaseVolumeByItem *= (1 + Main.s_Instance.m_fBaseVolumeyByItemDecreasedPercentWhenDead);
        //        Main.s_Instance.g_SystemMsg.SetContent( "死亡导致道具加成的基础体积衰减了：" + (Main.s_Instance.m_fBaseVolumeyByItemDecreasedPercentWhenDead * 100).ToString( "f0" ) +  "%" );
        SetBaseVolumeByItem(fBaseVolumeByItem);
    }

    float m_fExplodeChildNumBuffEffect = 1f;
    float m_fExplodeMotherLeftBuffEffect = 1f;
    public float GetExplodeChildNumBuffEffect()
    {
        return m_fExplodeChildNumBuffEffect;
    }

    public float GetExplodeMotherLeftBuffEffect()
    {
        return m_fExplodeMotherLeftBuffEffect;
    }

    public void UpdateBuffEffect_Explode()
    {
        m_fExplodeChildNumBuffEffect = 1f;
        m_fExplodeMotherLeftBuffEffect = 0;
        CBuffEditor.sBuffConfig buff_config;
        for (int i = 0; i < m_lstBuff.Count; i++)
        {
            CBuff buff = m_lstBuff[i];
            buff_config = buff.GetConfig();
            if (buff_config.func == (int)CBuffEditor.eBuffFunc.explode_child_num)
            {
                m_fExplodeChildNumBuffEffect *= (1f + buff_config.aryValues[0]);
            }
            if (buff_config.func == (int)CBuffEditor.eBuffFunc.explode_mother_left_percent)
            {
                m_fExplodeMotherLeftBuffEffect += buff_config.aryValues[0];
            }
        }
    }

    public void RushDueToExplode(int nBallIndex, float fRunDistanceRadiusMultiple, float fRunSpeed)
    {
        photonView.RPC("RPC_RushDueToExplode", PhotonTargets.All, nBallIndex, fRunDistanceRadiusMultiple, fRunSpeed);
    }

    [PunRPC]
    public void RPC_RushDueToExplode(int nBallIndex, float fRunDistanceRadiusMultiple, float fRunSpeed)
    {
        Ball ball = GetBallByIndex(nBallIndex);
        ball.BeginEject_BySpeed(fRunDistanceRadiusMultiple * ball.GetRadius(), fRunSpeed);
    }

    static byte[] _bytesExplodeBall = new byte[1024];
    public void ExplodeBall(int nBallIndex, string szThornConfigId, int nConfigChildNum, int nChildNum, float fChildVolume, float fMotherLeftVolume, bool bMotherMoving)
    {
        Ball ballMother = GetBallByIndex(nBallIndex);

        StringManager.BeginPushData(_bytesExplodeBall);
        StringManager.PushData_Int(0);
        StringManager.PushData_Int(0);
        StringManager.PushData_Int(nBallIndex);
        StringManager.PushData_Float(ballMother.GetPos().x);
        StringManager.PushData_Float(ballMother.GetPos().y);
        StringManager.PushData_Float(ballMother.GetDir().x);
        StringManager.PushData_Float(ballMother.GetDir().y);
        StringManager.PushData_Float(fChildVolume);
        StringManager.PushData_Float(fMotherLeftVolume);
        StringManager.PushData_Short(bMotherMoving ? (short)1 : (short)0);
        int nRealChildNum = 0;
        for (int i = 0; i < nChildNum; i++) // 开始分配小球份额。预期炸的球数，不一定是实际炸的球数，还要看有没有闲置的小球可以用来生成新球
        {
            Ball ball = _PlayerIns.GetOneBallToReuse();
            if (ball == null)
            {
                break;
            }
            nRealChildNum++;
            StringManager.PushData_Short((short)ball.GetIndex());
        }
        StringManager.SetCurPointerPos(0);
        StringManager.PushData_Int(nConfigChildNum);
        StringManager.PushData_Int(nRealChildNum);

        RPC_WoriniGeGui( szThornConfigId, _bytesExplodeBall);

    }


    public void RPC_WoriniGeGui(string szThornConfigId, byte[] bytes)
    {
        if (!IsPlayerInitCompleted())
        {
            return;
        }


        StringManager.BeginPopData(bytes);


        int nConfigChildNum = StringManager.PopData_Int();
        int nChildNum = StringManager.PopData_Int();
        int nMotherBallIndex = StringManager.PopData_Int();
        float fMotherPosX = StringManager.PopData_Float();
        float fMotherPosY = StringManager.PopData_Float();
        float fMotherDirX = StringManager.PopData_Float();
        float fMotherDirY = StringManager.PopData_Float();
        Ball ballMother = GetBallByIndex(nMotherBallIndex);

        if (!IsMainPlayer() && !ballMother.GetActive())
        {
            return;
        }

        CExplodeNode node = GetOneExplodeNode();
        node.m_vecMotherPos.x = fMotherPosX;
        node.m_vecMotherPos.y = fMotherPosY;
        node.m_vecMotherDir.x = fMotherDirX;
        node.m_vecMotherDir.y = fMotherDirY;
        node.SetPlayer(this);
        node.nChildNum = nChildNum;
        node.nConfigChildNum = nConfigChildNum;
        node.ballMother = ballMother;
        node.fChildVolume = StringManager.PopData_Float();
        node.fMotherLeftVolume = StringManager.PopData_Float();
        node.m_bMotherMoving = (StringManager.PopData_Short() == (short)1);
        node.explodeConfig = CMonsterEditor.s_Instance.GetMonsterConfigById(szThornConfigId);
        for (int i = 0; i < node.nChildNum; i++)
        {
            int nChildBallIndex = StringManager.PopData_Short();
            node.lstChildBallIndex.Add(nChildBallIndex);
        }

        node.BeginExplode();

        _PlayerIns.AddOneNodeToExplodingList(node);
    }

    List<CExplodeNode> m_lstExplodingNode = new List<CExplodeNode>();
    List<CExplodeNode> m_lstRecycledExplodeNode = new List<CExplodeNode>();
    public CExplodeNode GetOneExplodeNode()
    {
        CExplodeNode node;
        if (m_lstRecycledExplodeNode.Count == 0)
        {
            node = GameObject.Instantiate(Main.s_Instance.m_preExplodeNode).GetComponent<CExplodeNode>();
        }
        else
        {
            node = m_lstRecycledExplodeNode[0];
            node.gameObject.SetActive(true);
            node.lstChildBallIndex.Clear();
            m_lstRecycledExplodeNode.RemoveAt(0);
        }

        return node;
    }

    public void RecycleOneExplodeNode(CExplodeNode node)
    {
        node.gameObject.SetActive(false);
        m_lstRecycledExplodeNode.Add(node);
    }

    public void AddOneNodeToExplodingList(CExplodeNode node)
    {
        m_lstExplodingNode.Add(node);
    }

    void DoingExplode()
    {
        for (int i = m_lstExplodingNode.Count - 1; i >= 0; i--)
        {
            CExplodeNode node = m_lstExplodingNode[i];
            bool bEnd = false;
            if (node.IsExploding())
            {
                bEnd = node.DoExplode();
            }
            else
            {
                if (node.IsWaiting())
                {
                    bEnd = node.DoWaiting();
                }
                else
                {
                    bEnd = true;
                }
            }

            if (bEnd)
            {
                m_lstExplodingNode.RemoveAt(i);
                RecycleOneExplodeNode(node);
            }
        }
    }



    /*
    [PunRPC]
	public void RPC_WoriniGeGui( int nBallIndex, string szThornConfigId, int nChildNum, float fChildSize, float fMotherLeftSize, double dOccurTime )
	{

        if (  !IsPlayerInitCompleted())
        {
            return;  
        }
        Ball ball = m_lstBalls[nBallIndex];
		ball.DoExplode (szThornConfigId, nChildNum, fChildSize, fMotherLeftSize, dOccurTime );
	}
    */
    int m_nCurSkinIndex = 0;

    public void ChangeSkin()
    {
        if (photonView.isMine)
        {
            m_nCurSkinIndex++;
            if (m_nCurSkinIndex >= Main.s_Instance.GetSpriteArrayLength())
            {
                m_nCurSkinIndex = 0;
            }
            photonView.RPC("RPC_ChangeSkin", PhotonTargets.All, m_nCurSkinIndex);
        }
    }


    [PunRPC]
    public void RPC_ChangeSkin(int nIndex)
    {
        SetSkin(nIndex);
    }


    Sprite m_sprCurAvatar = null;
    public void SetSkin(int nIndex)
    {
        m_nCurSkinIndex = nIndex;
        Sprite sprite = Main.s_Instance.GetSpriteBySpriteId(nIndex);
        m_sprCurAvatar = sprite;

        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            ball.SetSprite(nIndex);
        }
    }

    public void SetDead(bool bDead)
    {
        m_bDead = bDead;
        _PlayerIns.SetDead(bDead);
    }

    public void PleaseSyncMyDataIfExist()
    {
        photonView.RPC("RPC_PleaseSyncMyDataIfExist", PhotonTargets.All, GetAccount());
    }

    [PunRPC]
    public void RPC_PleaseSyncMyDataIfExist(string szAccount)
    {
        if (!PhotonNetwork.isMasterClient) // Master-Client负责同步（这里必须做容错，不然网络丢包就麻烦了）[to rongcuo]
        {
            return;
        }

        // common data
        CPlayerManager.sPlayerInfoCommon info_common = null;
        CPlayerManager.s_Instance.GetPlayerData_Common(szAccount, ref info_common);
        byte[] bytes_common = null;
        if (info_common != null)
        {
            bytes_common = info_common.GenerateDataBlob();
        }
        else
        {
            return;
        }


        photonView.RPC("RPC_CommonData", PhotonTargets.All, szAccount, bytes_common);
    }

    bool m_bMyDataLoaded = false;

    public void SetMyDataLoaded( bool bLoaded )
    {
        m_bMyDataLoaded = bLoaded;
    }

    public bool GetMyDataLoaded()
    {
        return m_bMyDataLoaded;
    }

    //[to rongcuo] 这里如果网络丢包来，会出现巨大的Bug，必须做容错。
    [PunRPC]
    public void RPC_CommonData(string szAccount,  byte[] bytes_common)
    {
        if (!StringManager.Equal(GetAccount(), szAccount))
        {
            return;
        }

        if (bytes_common == null)
        {
            return;
        }

        AnalyzeCommonData(bytes_common);
    }

        /*
        [PunRPC]
        public void RPC_TakeYourData( string szAccount, byte[] bytes_balls, byte[] bytes_common)
        {
            if (!StringManager.Equal(GetAccount(), szAccount))
            {
                return;
            }

            if (bytes_balls != null) // 有球球信息
            {
                // AnalyzeBallData(bytes_balls);
                _PlayerIns.AnalyzeBallData(bytes_balls);
            }
            else// 没有球球信息（本局首登，或者球球留在场景上被吃完了）
            {
                if ( IsMainPlayer() )
                {
                    DoReborn();
                }
                else
                {
                    // do nothing
                }
            }

            if (bytes_common != null) // 有common信息
            {
                // 记录下common信息

                // 如果是MainPlayer,就立刻使用这些Common信息
            }
            else // 没有common信息（本局首登）
            {
                // do nothing
            }

            DoSomethingWhenLogin();
            m_bMyDataLoaded = true; 
        }
        */

        void AnalyzeBallData( byte[] bytes )
     {
        StringManager.BeginPopData( bytes );
        int nLiveBallNum = StringManager.PopData_Int();
        int nLenOfAccount = StringManager.PopData_Short();
        string szAccount = StringManager.PopData_String(nLenOfAccount);
        Debug.Log( "获取到【" + szAccount + "】的球球信息");
        for ( int i = 0; i < nLiveBallNum; i++ )
        {
            int nBallIndex = StringManager.PopData_Short();
            float fSize = StringManager.PopData_Float();
            float fPosX = StringManager.PopData_Float();
            float fPosY = StringManager.PopData_Float();
            float fCurShellTime = StringManager.PopData_Float();
            float fTotalShellTime = StringManager.PopData_Float();
            Ball ball = m_lstBalls[nBallIndex];
            ball.SetDead( false );
            ball.SetSize(fSize);
            float fRadius = 0f;
            ball.SetVolume(CyberTreeMath.Scale2Volume(fSize, ref fRadius));
            ball.SetRadius(fRadius);
            ball.SetActive(true);
            vecTempPos.x = fPosX;
            vecTempPos.y = fPosY;
            vecTempPos.z = -fSize;

            ball.SetPos(vecTempPos);

            ball.Local_SetShellInfo(fTotalShellTime, fCurShellTime);
            ball.Local_BeginShell();
            ball.UpdateShell();

            _PlayerIns.SetOneBallUsed(nBallIndex);
        }
    }


    public void AnalyzeCommonData(byte[] bytes)
    {
        if (bytes == null)
        {
            return;
        }

        StringManager.BeginPopData(bytes);
        int nLevel = StringManager.PopData_Short();
        short nMoney = StringManager.PopData_Short();
        short nExp = StringManager.PopData_Short();
        float fBaseVolumeByLevel = StringManager.PopData_Float();
        float fBaseSpeed = StringManager.PopData_Float();
        float fBaseShellTime = StringManager.PopData_Float();
        short nItemLevel_BaseSpeed = StringManager.PopData_Short();
        short nItemLevel_BaseShellTime = StringManager.PopData_Short();
        short nSelectedSkillId = StringManager.PopData_Short();
        short nSkillWLevel = StringManager.PopData_Short();
        short nSkillELevel = StringManager.PopData_Short();
        short nSkillRLevel = StringManager.PopData_Short();
        short nSkillPoint = StringManager.PopData_Short();
        short nEatThornNum = StringManager.PopData_Short();
        RecoverExp(nExp);
        RecoverLevel(nLevel); // 这儿不能用SetLevel(), 因为SetLevel()里面有增量操作，会加钱、加点、加基础体积 等
        RecoverMoney(nMoney);
        RecoverBaseSpeed(fBaseSpeed);
        RecoverBaseShellTime(fBaseShellTime);
        RecoverBaseVolumeByLevel(fBaseVolumeByLevel);
        RecoverItemLevel(CItemSystem.eItemFunc.base_speed, nItemLevel_BaseSpeed);
        RecoverItemLevel(CItemSystem.eItemFunc.shell_time, nItemLevel_BaseShellTime);
        RecoverSkillLevel(CSkillSystem.eSkillId.w_spit, nSkillWLevel);
        RecoverSkillLevel(CSkillSystem.eSkillId.e_unfold, nSkillELevel);
        RecoverSkillLevel((CSkillSystem.eSkillId)nSelectedSkillId, nSkillRLevel);
        RecoverSkillPoint(nSkillPoint);
        RecoverEatThornNum(nEatThornNum);

        CItemSystem.s_Instance.RefreshAddPointButton(); // 道具界面刷新
        CSkillSystem.s_Instance.RefreshAddPointButton();
        CSkillSystem.s_Instance.RefreshCastButtonAvailable();
    }

    public void DoReborn()
    {
        vecTempPos = MapEditor.s_Instance.GetRebornSpotRandomPos();

        Ball ball = _PlayerIns.GetOneBallToReuse();
        if (ball == null)
        {
            Debug.LogError("DoReborn() ball == null ");
            return;
        }
     
        BeginProtect();

        // 重生
        ball.SetVolume(GetBornBaseTiJi());
        vecTempPos.z = -ball.GetSize();
        ball.SetPos(vecTempPos);

        SetRebornCompleted(true);

        m_bDead = false;

        //ball.CheckClassStatus();

        RebornInit();
        //ball.SetActive( true );

        SetDead(false);

        if (IsMainPlayer())
        {
            ball.SetSelfTriggerEnable(true);
            EndMoveToCenter();
            Local_SetMoving( false );
        }
    }

    public void DoSomethingWhenLogin()
    {
        BeginProtect(); // 重生保护
        SetRebornCompleted(true);
        SetDead(false);

        
    }

    public void RebornInit()
    {
        m_dicAssistAttack.Clear();

        m_ballBiggest = null;
        m_bAllBallsMovingToCenter = false;
        m_bMoving = false;
		_PlayerIns.SetSkillStatus (_PlayerIns.GetSkillId (), 0);
    }

    public float GetBornBaseTiJi()
    {
        // 目前基础体积是三个部分组成：1、最原始配置的基础体积；  2、道具加成的基础体积(貌似目前策划层面 废弃了这种道具)； 3、随等级增加的基础体积
        m_fBaseArea = Main.s_Instance.m_fBornMinTiJi + m_BaseData.fBaseSize + m_fBaseVolumeByLevel;
        return m_fBaseArea;
    }

    float m_SyncPosAfterEjectTotalTime = 0f;
    public void BeginSyncPosAfterEject(float fTotalTime)
    {
        m_SyncPosAfterEjectTotalTime = fTotalTime;
    }

    public void EndShell()
    {
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            ball.Local_EndShell();
        }
    }

    //// 技能：魔盾
    float m_fSkillMpCost_MagicShield = 0f;
    float m_fSkillDuration_MagicShield = 0f;
    bool m_bMagicShielding = false;
    float m_fSkillSpeedAffect_MagicShield = 0;

    public bool CheckIfCanCastSkill_MagicShield()
    {
        /*
        if (GetMP() < m_fSkillMpCost_MagicShield)
        {
            Main.s_Instance.g_SystemMsg.SetContent(CMsgSystem.s_Instance.GetMsgContentByType(CMsgSystem.eSysMsgType.mp_not_enough));
            return false;
        }
        */
        return true;
    }

    public bool CastSkill_MagicShield(float fMpCost, float fDuration, float fSpeedAffect)
    {
        m_fSkillMpCost_MagicShield = fMpCost;
        m_fSkillDuration_MagicShield = fDuration;
        m_fSkillSpeedAffect_MagicShield = fSpeedAffect;
        if (!CheckIfCanCastSkill_MagicShield())
        {
            return false;
        }

        // 耗蓝
        //SetMP(GetMP() - m_fSkillMpCost_MagicShield);

        // photonView.RPC("RPC_CastSkill_MagicShield", PhotonTargets.All, fDuration, fSpeedAffect);
		RPC_CastSkill_MagicShield(fDuration, fSpeedAffect);

        return true;
    }

    [PunRPC]
    public void RPC_CastSkill_MagicShield(float fDuration, float fSpeedAffect)
    {
        m_fSkillDuration_MagicShield = fDuration;
		m_fSkillSpeedAffect_MagicShield = fSpeedAffect;
		_PlayerIns.BeginSkill (CSkillSystem.eSkillId.u_magicshield, fSpeedAffect, fDuration);
		/*
        m_bMagicShielding = true;
        m_fSkillLoopCount_MagicShield = 0;
   
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            //ball.SetMagicShielding(true);

            ball.BeginSkillEffect(CSkillSystem.eSkillId.u_magicshield);
            ball.SetEffectStatus((int)CSkillSystem.eSkillId.u_magicshield, 1);
        }
		*/
    }

    public float GetSkillMagicShieldSpeedAffect()
    {
        return m_fSkillSpeedAffect_MagicShield;
    }

    public bool IsMagicShielding()
    {
		if (_PlayerIns == null) {
			return false;
		}
		return _PlayerIns.GetSkillStatus (CSkillSystem.eSkillId.u_magicshield) > 0;
       // return m_bMagicShielding;
    }

    float m_fSkillLoopCount_MagicShield = 0;
    void SkillLoop_MagicShield()
    {
        if (!m_bMagicShielding)
        {
            return;
        }

        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            ball.EffectLoop(CSkillSystem.eSkillId.u_magicshield);
        }

        m_fSkillLoopCount_MagicShield += Time.deltaTime;
        if (m_fSkillLoopCount_MagicShield >= m_fSkillDuration_MagicShield)
        {
            EndSkill_MagicShield();
        }

    }

    void EndSkill_MagicShield()
    {
        m_bMagicShielding = false;

        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            ball.EndSkillEffect(CSkillSystem.eSkillId.u_magicshield);
        }
    }

    //// end  技能：魔盾

    /// <summary>
    /// / 技能：湮灭
    /// </summary>
    /// <returns></returns>

    float m_fSkillMpCost_Annihilate = 0f;
    float m_fSkillPercent_Annihilate = 0f;
    float m_fSkillDuration_Annihilate = 0f;
    float m_fSkillSpeedAffect_Annihilate = 0f;
    bool m_bAnnihilating = false;

    public bool CastSkill_Annihilate(float fMpCost, float fAnnihilatePercent, float fDuration, float fSpeedAffect)
    {
        m_fSkillMpCost_Annihilate = fMpCost;
        m_fSkillPercent_Annihilate = fAnnihilatePercent;
        m_fSkillDuration_Annihilate = fDuration;
        m_fSkillSpeedAffect_Annihilate = fSpeedAffect;
        if (!CheckIfCanCastSkill_Annihilate())
        {
            return false;
        }

        // 耗蓝
        //SetMP(GetMP() - m_fSkillMpCost_Annihilate);

       // photonView.RPC("RPC_CastSkill_Annihilate", PhotonTargets.All, fAnnihilatePercent, fDuration, fSpeedAffect);
		RPC_CastSkill_Annihilate(fAnnihilatePercent, fDuration, fSpeedAffect);

        return true;
    }

    [PunRPC]
    public void RPC_CastSkill_Annihilate(float fAnnihilatePercent, float fDuration, float fSpeedAffect)
    {
        m_fSkillPercent_Annihilate = fAnnihilatePercent;
        m_fSkillDuration_Annihilate = fDuration;
		_PlayerIns.BeginSkill ( CSkillSystem.eSkillId.y_annihilate, fAnnihilatePercent, fDuration );
        //m_bAnnihilating = true;
        //m_fSkillLoopCount_Annihilate = 0;
        //m_fSkillSpeedAffect_Annihilate = fSpeedAffect;
		/*
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            //   ball.SetAnnihilate(true);

            ball.BeginSkillEffect(CSkillSystem.eSkillId.y_annihilate);
            ball.SetEffectStatus((int)CSkillSystem.eSkillId.y_annihilate, 1);
        }
		*/
    }

    public float GetAnnihilateSpeedAffect()
    {
        return m_fSkillSpeedAffect_Annihilate;
    }

    public bool IsAnnihilaing()
    {
		if (_PlayerIns == null) {
			return false;
		}
		return _PlayerIns.IsYanMie ();
        //return m_bAnnihilating;
    }

    float m_fSkillLoopCount_Annihilate = 0f;
    void SkillLoop_Annihilate()
    {
        if (!m_bAnnihilating)
        {
            return;
        }

        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            ball.EffectLoop(CSkillSystem.eSkillId.y_annihilate);
        }

        m_fSkillLoopCount_Annihilate += Time.deltaTime;
        if (m_fSkillLoopCount_Annihilate >= m_fSkillDuration_Annihilate)
        {
            EndSkill_Annihilate();
        }

    }

    void EndSkill_Annihilate()
    {
        m_bAnnihilating = false;

        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            //ball.SetAnnihilate(false);
            ball.EndSkillEffect(CSkillSystem.eSkillId.y_annihilate);

            /*
            colorTemp = ball._srMouth.color;
            colorTemp.a = 1f;
            ball._srMouth.color = colorTemp;
            */
        }
    }

    public float GetAnnihilatePercent()
    {
        return m_fSkillPercent_Annihilate;
    }

    public bool CheckIfCanCastSkill_Annihilate()
    {
        /*
        if (GetMP() < m_fSkillMpCost_Annihilate)
        {
            Main.s_Instance.g_SystemMsg.SetContent(CMsgSystem.s_Instance.GetMsgContentByType(CMsgSystem.eSysMsgType.mp_not_enough));
            return false;
        }
        */
        return true;
    }

    //// end 技能：湮灭

    //// 技能：金壳
    float m_fSkillMpCost_Gold = 0;
    float m_fSkillGold_SpeedAddPercent = 0;
    float m_fGold_Duration = 0;
    bool m_bGold = false;
    public bool CheckIfCanCastSkillGold()
    {
        /*
        if (GetMP() < m_fSkillMpCost_Gold)
        {
            Main.s_Instance.g_SystemMsg.SetContent(CMsgSystem.s_Instance.GetMsgContentByType(CMsgSystem.eSysMsgType.mp_not_enough));
            return false;
        }
        */

        return true;
    }


    public bool CastSkill_Gold(float fMpCost, float fDuration, float fSpeedAddPercent)
    {
        m_fSkillMpCost_Gold = fMpCost;
        m_fGold_Duration = fDuration;


        if (!CheckIfCanCastSkillGold())
        {
            return false;
        }


        //SetMP(GetMP() - m_fSkillMpCost_Gold);

        //photonView.RPC("RPC_CastSkill_Gold", PhotonTargets.All, fDuration, fSpeedAddPercent);
		RPC_CastSkill_Gold(fDuration, fSpeedAddPercent);

        return true;
    }

    [PunRPC]
    public void RPC_CastSkill_Gold(float fDuration, float fSpeedAddPercent)
    {
        m_fSkillGold_SpeedAddPercent = fSpeedAddPercent;
        m_fGold_Duration = fDuration;
		_PlayerIns.BeginSkill ( CSkillSystem.eSkillId.p_gold, fSpeedAddPercent,fDuration );
        /*
		m_bGold = true;
        m_fGoldLoopCount = 0f;
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            //ball.BeginEffect( CEffect.eEffectType.gold_shell );
            ball.BeginSkillEffect(CSkillSystem.eSkillId.p_gold);
            ball.SetEffectStatus((int)CSkillSystem.eSkillId.p_gold, 1);
        }
		*/
    }

    public float GetSkillGoldSpeedAddPercent()
    {
        return m_fSkillGold_SpeedAddPercent;
    }

    float m_fGoldLoopCount = 0f;
    void SkillLoop_Gold()
    {
        if (!m_bGold)
        {
            return;
        }

        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            ball.EffectLoop(CSkillSystem.eSkillId.p_gold);
        }

        m_fGoldLoopCount += Time.deltaTime;
        if (m_fGoldLoopCount >= m_fGold_Duration)
        {
            EndGold();
        }
    }

    void EndGold()
    {
        m_bGold = false;
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            // ball.EndEffect(CEffect.eEffectType.gold_shell);
            ball.EndSkillEffect(CSkillSystem.eSkillId.p_gold);
        }
    }

    public bool IsGold()
    {
		if (_PlayerIns == null) {
			return false;
		}
		return _PlayerIns.GetSkillStatus (CSkillSystem.eSkillId.p_gold) > 0;
        //return m_bGold;
    }


    //// end 技能：金壳

    /// 技能：狂暴
    float m_fSkillMpCost_Henzy = 0;
    float m_fHenzy_Duration = 0;
    float m_fHenzy_SpeedChangePercent = 0;
    float m_fHenzy_WColdDown = 0;
    float m_fHenzy_EColdDown = 0;
    bool m_bHenzy = false;
    public bool CheckIfCanCastSkillHenzy()
    {
        /*
        if (GetMP() < m_fSkillMpCost_Henzy)
        {
            Main.s_Instance.g_SystemMsg.SetContent(CMsgSystem.s_Instance.GetMsgContentByType(CMsgSystem.eSysMsgType.mp_not_enough));
            return false;
        }
        */

        return true;
    }

    public bool CastSkill_Henzy(float fCostMp, float fDuration, float fSpeedChangePercent, float fWColdDown, float fEColdDown)
    {
        m_fSkillMpCost_Henzy = fCostMp;
        m_fHenzy_Duration = fDuration;
        m_fHenzy_SpeedChangePercent = fSpeedChangePercent;
        m_fHenzy_WColdDown = fWColdDown;
        m_fHenzy_EColdDown = fEColdDown;

        if (!CheckIfCanCastSkillHenzy())
        {
            return false;
        }

        //SetMP(GetMP() - m_fSkillMpCost_Henzy);

        //photonView.RPC("RPC_CastSkill_Henzy", PhotonTargets.All, fDuration, fSpeedChangePercent, fWColdDown, fEColdDown);
		RPC_CastSkill_Henzy(fDuration, fSpeedChangePercent, fWColdDown, fEColdDown);

        return true;
    }

    [PunRPC]
    public void RPC_CastSkill_Henzy(float fDuration, float fSpeedChangePercent, float fWColdDown, float fEColdDown)
    {
        m_fHenzy_Duration = fDuration;
        m_fHenzy_SpeedChangePercent = fSpeedChangePercent;
        m_fHenzy_WColdDown = fWColdDown;
        m_fHenzy_EColdDown = fEColdDown;
		_PlayerIns.BeginSkill (CSkillSystem.eSkillId.o_fenzy, fSpeedChangePercent, fDuration);
		/*
		m_fHenzyLoopCount = 0;
        m_bHenzy = true;


        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            ball._goEffectContainer_KuangBao.gameObject.SetActive(true);
            ball._effectKuangBao_QianYao.gameObject.SetActive(true);
            ball._effectKuangBao_QianYao.BeginPlay(false);
            ball.SetEffectStatus((int)CSkillSystem.eSkillId.o_fenzy, 1);
        }
		*/
    }

    float m_fHenzyLoopCount = 0f;
    void HenzyLoop()
    {
        if (!m_bHenzy)
        {
            return;
        }

        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            ball.EffectLoop(CSkillSystem.eSkillId.o_fenzy);
        }

        m_fHenzyLoopCount += Time.deltaTime;
        if (m_fHenzyLoopCount >= m_fHenzy_Duration)
        {
            EndHenzy();
        }
    }

    void EndHenzy()
    {
        m_bHenzy = false;
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];

            ball._goEffectContainer_KuangBao.gameObject.SetActive(false);
            ball._effectKuangBao_Up.gameObject.SetActive(false);
            // ball._effectKuangBao_Down.gameObject.SetActive(false);
        }
    }

    public bool IsHenzy()
    {
		if (_PlayerIns == null) {
			return false;
		}
		return _PlayerIns.GetSkillStatus (CSkillSystem.eSkillId.o_fenzy) > 0;
       // return m_bHenzy;
    }

    public float GetHenzySpeedChangePercent()
    {
        return m_fHenzy_SpeedChangePercent;
    }

    public float GetHenzyWColdDown()
    {
        return m_fHenzy_WColdDown;
    }

    public float GetHenzyEColdDown()
    {
        return m_fHenzy_EColdDown;
    }

    /// end 技能：狂暴 


    /// <summary>
    /// / 技能：秒合
    /// </summary>
    /// <returns></returns>
    float m_fSkillMpCost_MergeAll = 0f;
    float m_fSkillQianYao_MergeAll = 0f;
    public bool CheckIfCanCastSkill_MergeAll()
    {
        /*
        if (GetMP() < m_fSkillMpCost_MergeAll)
        {
            Main.s_Instance.g_SystemMsg.SetContent(CMsgSystem.s_Instance.GetMsgContentByType(CMsgSystem.eSysMsgType.mp_not_enough));
            return false;
        }
        */
        return true;
    }

    public bool CastSkill_MergeAll(float fMpCost, float fQianYao)
    {
        m_fSkillMpCost_MergeAll = fMpCost;
        m_fSkillQianYao_MergeAll = fQianYao;

        if (!CheckIfCanCastSkill_MergeAll())
        {
            return false;
        }

        // 耗蓝
        //SetMP(GetMP() - m_fSkillMpCost_MergeAll);

        //CSkillSystem.s_Instance.ShowProgressBar(CSkillSystem.eSkillSysProgressBarType.i_merge_all);

        //photonView.RPC("RPC_CastSkill_MergeAll", PhotonTargets.All, fQianYao);
		RPC_CastSkill_MergeAll(fQianYao );

        return true;
    }

    [PunRPC]
    public void RPC_CastSkill_MergeAll(float fQianYao)
    {
        m_fSkillQianYao_MergeAll = fQianYao;
        m_bMiaoHeQianYao = true;
        m_fSkillQianYaoCount_MergeAll = 0f;
		_PlayerIns.BeginSkill (CSkillSystem.eSkillId.i_merge, 0, m_fSkillQianYao_MergeAll);
		_PlayerIns.SetQianYaoTime (0);
		Local_SetMoving ( false );
		/*
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            ball._effectMergeAll_QianYao.gameObject.SetActive(true);
        }
		*/
    }

	public bool IsMianHe()
    {
		return _PlayerIns.GetSkillStatus(CSkillSystem.eSkillId.i_merge) > 0;
    }

    bool m_bMiaoHeQianYao = false;
    float m_fSkillQianYaoCount_MergeAll = 0f;
    public bool IsMiaoHeQianYao()
    {
        return m_bMiaoHeQianYao;
    }

    float m_fMiaoHeQiaoYaoRotation;
    void SkillLoop_MiaoHe()
    {
		return;

        if (!m_bMiaoHeQianYao)
        {
            return;
        }
        CSkillSystem.s_Instance.UpdateProgressData(m_fSkillQianYaoCount_MergeAll, m_fSkillQianYao_MergeAll);
        m_fSkillQianYaoCount_MergeAll += Time.deltaTime;

        //// 前摇特效（旋转）
        m_fMiaoHeQiaoYaoRotation += Time.deltaTime * 60f;
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            ball._effectMergeAll_QianYao.Rotating(m_fMiaoHeQiaoYaoRotation);
        }
        //// end 前摇特效

        if (m_fSkillQianYaoCount_MergeAll >= m_fSkillQianYao_MergeAll)
        {
            DoMergeAll();
            m_bMiaoHeQianYao = false;
            CSkillSystem.s_Instance.HideProgressBar();

            for (int i = 0; i < m_lstBalls.Count; i++)
            {
                Ball ball = m_lstBalls[i];
                ball._effectMergeAll_QianYao.gameObject.SetActive(false);
            }
        }
    }

    public bool IsMiaoHeQiaoYao()
    {
        return m_bMiaoHeQianYao;
    }

    void DoMergeAll()
    {
        MergeAll();
    }













    //// 技能：潜行
    public bool CheckIfCanCastSkill_Sneak()
    {
        /*
        if (GetMP() < m_fSkillMpCost_Sneak)
        {
            Main.s_Instance.g_SystemMsg.SetContent(CMsgSystem.s_Instance.GetMsgContentByType(CMsgSystem.eSysMsgType.mp_not_enough));
            return false;
        }
        */

        return true;
    }

    float m_fSkillMpCost_Sneak = 0f;
    float m_fSkillSpeedChangePercent_Sneak = 0f;
    float m_fSkillDuration_Sneak = 0f;
    bool m_bSneaking = false;
    public bool CastSkill_Sneak(float fMpCost, float fSpeedPercent, float fDuration)
    {
        m_fSkillMpCost_Sneak = fMpCost;
        m_fSkillSpeedChangePercent_Sneak = fSpeedPercent;
        m_fSkillDuration_Sneak = fDuration;

        if (!CheckIfCanCastSkill_Sneak())
        {
            return false;
        }


        // 耗蓝
        SetMP(GetMP() - m_fSkillMpCost_Sneak);

        // photonView.RPC("RPC_CastSkill_Sneak", PhotonTargets.All, fSpeedPercent, fDuration);
        RPC_CastSkill_Sneak(fSpeedPercent, fDuration);


        return true;
    }

    //[PunRPC]
    public void RPC_CastSkill_Sneak(float fSpeedPercent, float fDuration)
    {
        m_fSkillSpeedChangePercent_Sneak = fSpeedPercent;
        m_fSkillDuration_Sneak = fDuration;

		_PlayerIns.BeginSkill( CSkillSystem.eSkillId.r_sneak, fSpeedPercent, fDuration);
        /*

        SetSneaking(true);

        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            
            ball.BeginSkillEffect(CSkillSystem.eSkillId.r_sneak);
            ball.SetEffectStatus((int)CSkillSystem.eSkillId.r_sneak, 1);
        }


        m_fSkillDurationCount_Sneak = 0;
        */
    }

    public void SetSneaking(bool val)
    {
        m_bSneaking = val;
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            ball.SetSneak(m_bSneaking);
        }

    }

    public bool IsSneaking()
    {
		if (_PlayerIns == null) {
			return false;
		}
		return _PlayerIns.GetSkillStatus (CSkillSystem.eSkillId.r_sneak) > 0;
      //  return m_bSneaking;
    }

    float m_fSkillDurationCount_Sneak = 0;
    void SneakingLoop()
    {
        if (!m_bSneaking)
        {
            return;
        }

     
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            ball.EffectLoop(CSkillSystem.eSkillId.r_sneak);
        }
       

        m_fSkillDurationCount_Sneak += Time.deltaTime;
        if (m_fSkillDurationCount_Sneak >= m_fSkillDuration_Sneak)
        {
            EndSneak();
        }
    }

    void EndSneak()
    {
        m_bSneaking = false;
        SetSneaking(false);

        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            // ball.EndEffect(CEffect.eEffectType.gold_shell);
            ball.EndSkillEffect(CSkillSystem.eSkillId.r_sneak);
            // ball._skeletonAnimatnion.skeleton.a = 1f;
            /*
            colorTemp = ball._srMouth.color;
            colorTemp.a = 1f;
            ball._srMouth.color = colorTemp;
            */
        }
    }

    //// end 【潜行】技能

    /// <summary>
    /// 技能： 变刺
    /// </summary>
    /// <returns></returns>
    float m_fMpCost_BecomeThorn = 0f;
    float m_fQianYao_BecomeThorn = 0f;
    float m_fSpeedChangePercent_BecomeThorn = 0f;
    float m_fDuration_BecomeThorn = 0f;
    string m_szExplodeConfigId_BecomeThorn = "";

    public bool CheckIfCanCastSkill_BecomeThorn()
    {
        /*
        if ( GetMP() < m_fMpCost_BecomeThorn )
        {
            Main.s_Instance.g_SystemMsg.SetContent(CMsgSystem.s_Instance.GetMsgContentByType(CMsgSystem.eSysMsgType.mp_not_enough));
            return false;
        }
        */

        return true;
    }

    public bool CastSkill_BecomeThorn(float fMpCost_BecomeThorn, float fQianYao_BecomeThorn, float fSpeedChangePercent_BecomeThorn, float fDuration_BecomeThorn, string szExplodeConfigId)
    {
        m_fMpCost_BecomeThorn = fMpCost_BecomeThorn;

        if (!CheckIfCanCastSkill_BecomeThorn())
        {
            return false;
        }

        // 耗蓝
        //SetMP(GetMP() - m_fMpCost_BecomeThorn);

        //photonView.RPC("RPC_CastSkill_BecomeThorn", PhotonTargets.All, fQianYao_BecomeThorn, fSpeedChangePercent_BecomeThorn, fDuration_BecomeThorn, szExplodeConfigId);
		RPC_CastSkill_BecomeThorn( fQianYao_BecomeThorn, fSpeedChangePercent_BecomeThorn, fDuration_BecomeThorn, szExplodeConfigId );
        return true;
    }

    [PunRPC]
    public void RPC_CastSkill_BecomeThorn(float fQianYao_BecomeThorn, float fSpeedChangePercent_BecomeThorn, float fDuration_BecomeThorn, string szExplodeConfigId)
    {
        m_fQianYao_BecomeThorn = fQianYao_BecomeThorn;
        m_fSpeedChangePercent_BecomeThorn = fSpeedChangePercent_BecomeThorn;
        m_fDuration_BecomeThorn = fDuration_BecomeThorn;
        m_szExplodeConfigId_BecomeThorn = szExplodeConfigId;

		_PlayerIns.BeginSkill( CSkillSystem.eSkillId.t_become_thorn,fSpeedChangePercent_BecomeThorn,  fDuration_BecomeThorn );

      //  BecomeThorn_BeginQianYao();
    }

    public string GetBecomeThornExplodeConfigId()
    {
        return m_szExplodeConfigId_BecomeThorn;
    }

    float m_fBecomeThornTimeCount = 0f;
    int m_nBecomeThornStatus = 0; // 0 - None  1 - QiaoYao  2 - BecomeThorn
    public void BecomeThorn_BeginQianYao()
    {
        m_nBecomeThornStatus = 1;
        m_fBecomeThornTimeCount = 0;
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];

            ball._skeaniEffectBianCi.gameObject.SetActive(true);
            ball._skeaniEffectBianCi.state.SetAnimation(0, "ani04", true);
            ball._skeaniEffectBianCi.state.TimeScale = 1f;

        }
    }

    public void BecomeThorn_QianYaoLoop()
    {
        if (m_nBecomeThornStatus != 1)
        {
            return;
        }
        m_fBecomeThornTimeCount += Time.deltaTime;
        if (m_fBecomeThornTimeCount < m_fQianYao_BecomeThorn)
        {
            return;
        }
        BecomeThorn_EndQianYao();
    }

    public void BecomeThorn_EndQianYao()
    {
        m_fBecomeThornTimeCount = 0;
        m_nBecomeThornStatus = 2;
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            //ball._effectBecomeThorn_QianYao.gameObject.SetActive( false );
            //         ball._effectBecomeThorn_Become.gameObject.SetActive(true);
            //         ball._effectBecomeThorn_Become.Play();
            //            ball.SetSprite( CMonsterEditor.s_Instance.GetSpriteByMonsterType(CMonsterEditor.eMonsterBuildingType.thorn ));
            //            ball.SetMainSprVisible( true );
            //            ball.SetSkeletonVisible( false );
            //            ball.SetColor( Color.red );


            ball._skeaniEffectBianCi.gameObject.SetActive(true);
            ball._skeaniEffectBianCi.state.SetAnimation(0, "ani04_1", true);
            ball._skeaniEffectBianCi.state.TimeScale = 0.2f;


        }
    }

    public void BecomeThorn_ActAsThornLoop()
    {
        if (m_nBecomeThornStatus != 2)
        {
            return;
        }
        m_fBecomeThornTimeCount += Time.deltaTime;
        if (m_fBecomeThornTimeCount < m_fDuration_BecomeThorn)
        {
            return;
        }
        BecomeThorn_End();
    }

    public void BecomeThorn_End()
    {
        m_nBecomeThornStatus = 0;
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            //ball._effectBecomeThorn_Become.gameObject.SetActive(true);
            // ball._effectBecomeThorn_Become.Play();
            ball._skeaniEffectBianCi.gameObject.SetActive(false);
            //            ball.SetColor( Color.white );
        }

        if (IsMainPlayer())
        {
            photonView.RPC("RPC_EndBecomeThorn", PhotonTargets.All);
        }
    }

    [PunRPC]
    public void RPC_EndBecomeThorn()
    {
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            ball.SetMainSprVisible(false);
            ball.SetSkeletonVisible(true);
        }

    }

    public bool IsBecomeThorn()
    {
		if (_PlayerIns == null) {
			return false;
		}
		return _PlayerIns.GetSkillStatus (CSkillSystem.eSkillId.t_become_thorn) > 0;
        // return m_nBecomeThornStatus == 2;
    }

    public bool IsBecomeThorn_QianYao()
    {
        return m_nBecomeThornStatus == 1;
    }

    //// end 技能：变刺

    public bool CheckIfCanUnfold()
    {
        /*
		if (GetMP () < m_fUnfoldCostMp) {
			Main.s_Instance.g_SystemMsg.SetContent (CMsgSystem.s_Instance.GetMsgContentByType(CMsgSystem.eSysMsgType.mp_not_enough));
			return false;
		}
        */

        return true;
    }

    float m_fUnfoldCostMp = 0f;
    float m_fPreUnfoldTime = 0f;
    float m_fPreUnfoldCurTimeLapse = 0f;

    float m_fUnfoldScale = 0f;
    float m_fUnfoldKeepTime = 0f;
    float m_fUnfoldCurTimeLapse = 0f;

    int m_nSkillUnfoldStatus = 0; // 0 - None  1 - pre_unfold  2 - unfold
    public bool BeginPreUnfold(float fMpCost, float fMainTriggerPreUnfoldTime, float fUnfoldScale, float fUnfoldKeepTime)
    {
        m_fUnfoldCostMp = fMpCost;
        m_fPreUnfoldTime = fMainTriggerPreUnfoldTime;
        m_fUnfoldScale = fUnfoldScale;
        m_fUnfoldKeepTime = fUnfoldKeepTime;

        if (!CheckIfCanUnfold())
        {
            return false;
        }

        // SetMP(GetMP() - m_fUnfoldCostMp);

        // photonView.RPC("RPC_BeginPreUnfold", PhotonTargets.All, fMainTriggerPreUnfoldTime, fUnfoldScale, fUnfoldKeepTime);
        _PlayerIns.BeginSkill_KuoZhang(fMainTriggerPreUnfoldTime, fUnfoldKeepTime, fUnfoldScale ); 


        return true;
    }

    

    float m_fPreUnfoldTimeElpase = 0f;
    void PreUnfoldLoop()
    {
        /*
        if (m_nSkillUnfoldStatus != 1)
        {
            return;
        }

        m_fPreUnfoldTimeElpase += Time.deltaTime;
        if (m_fPreUnfoldTimeElpase < 0.2f)
        {
            return;
        }
        m_fPreUnfoldTimeElpase = 0f;

        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            if (ball.IsDead() || !ball.GetActive())
            {
                ball.UpdateUnfoldQianYao(m_fPreUnfoldAngle);
            }
        }
        */
    }

    const float c_fPreUnfoldInterval = 0.15f;
    float m_fPreUnfoldTimeElapse = 0f;
    void SkillLoop_Unfold()
    {
        if (m_nSkillUnfoldStatus == 1) // pre_unfold
        {
            m_fPreUnfoldCurTimeLapse += Time.deltaTime;
            float fTimeLapsePercent = m_fPreUnfoldCurTimeLapse / m_fPreUnfoldTime;
            float fPreUnfoldAngle = fTimeLapsePercent * 360f;

            m_fPreUnfoldTimeElapse += Time.deltaTime;
            if (m_fPreUnfoldTimeElapse >= c_fPreUnfoldInterval)
            {
                for (int i = 0; i < m_lstBalls.Count; i++)
                {
                    Ball ball = m_lstBalls[i];
                    if (ball.IsDead() || !ball.GetActive())
                    {
                        continue;
                    }
                    ball.UpdateUnfoldQianYao(fPreUnfoldAngle);
                }
                m_fPreUnfoldTimeElapse = 0f;
            }

            if (m_fPreUnfoldCurTimeLapse >= m_fPreUnfoldTime)
            {
                BeginUnfold();
            }
        }
        else if (m_nSkillUnfoldStatus == 2) // unfold
        {
            m_fUnfoldCurTimeLapse += Time.deltaTime;
            if (m_fUnfoldCurTimeLapse >= m_fUnfoldKeepTime)
            {
                EndUnfold();
                m_nSkillUnfoldStatus = 0;
            }
        }
    }

    float m_fCurSkillPointAffect_Unfold = 0f;
    public float GetCurSkillPointAffect_Unfold()
    {
        return m_fCurSkillPointAffect_Unfold;
    }

    [PunRPC]
    public void RPC_BeginPreUnfold(float fMainTriggerPreUnfoldTime, float fUnfoldScale, float fUnfoldKeepTime)
    {
        m_fPreUnfoldCurTimeLapse = 0;
        m_fPreUnfoldTime = fMainTriggerPreUnfoldTime;
        m_fUnfoldScale = fUnfoldScale;
        m_fUnfoldKeepTime = fUnfoldKeepTime;


        float fScale = GetUnfoldSkillParam_UnfoldScale();
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            ball.BeginPreUnfold(fScale);
        }

        m_nSkillUnfoldStatus = 1;


    }

    public float GetUnfoldSkillParam_PreUnfoldTime()
    {
        return m_fPreUnfoldTime;
    }

    public float GetUnfoldSkillParam_UnfoldScale()
    {
        return m_fUnfoldScale;
    }

    public float GetUnfoldSkillParam_UnfoldKeepTim()
    {
        return m_fUnfoldKeepTime;
    }

    public void BeginUnfold()
    {
        m_fUnfoldCurTimeLapse = 0;
        m_nSkillUnfoldStatus = 2;

        if (IsMainPlayer())
        {
            CAudioManager.s_Instance.PlayAudio(CAudioManager.eAudioId.e_audio_unfold);
        }


        float fScale = GetUnfoldSkillParam_UnfoldScale();


        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            ball.BeginUnfold(fScale);
            ball.EndPreUnfold();
        }
    }

    public void EndUnfold()
    {
        m_nSkillUnfoldStatus = 0;
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            ball.EndUnfold();
        }
    }

    public bool IsUnfolding()
    {
        return m_nSkillUnfoldStatus == 2;
    }

    public void DestroyMonster_Scene(CMonster thorn)
    {
        photonView.RPC("RPC_DestroyMonster_Scene", PhotonTargets.All, thorn.GetGuid(), (float)PhotonNetwork.time);
    }

    [PunRPC]
    public void RPC_DestroyMonster_Scene(long nThornId, float fOccurTime)
    {
        CClassEditor.s_Instance.RemoveThorn(nThornId, fOccurTime);
    }

    public void RecycleBabaThorn(int nThornIndex)
    {
        m_SkillManager.RecycleBabaThorn(nThornIndex);
    }



    // 使用“拉粑粑”技能
    public void UseSkill_Lababa()
    {
        m_SkillManager.UseSkill(CSkillManager.eSkillType.lababa);
    }

    // 计算当前的总体积
    float m_fBaseArea = 0f; // 基础体积
    float m_fTotalArea = 0f;
    int m_nCurLiveBallNum = 0;
    float m_fCalcTotalAreaCount = 0f;


    public void SetCurLiveBallNum(int num)
    {
        m_nCurLiveBallNum = num;
        if (IsMainPlayer())
        {
            CGrowSystem.s_Instance.SetLiveBallNum(num);
        }
    }

    public int GetCurLiveBallNum()
    {
        return m_nCurLiveBallNum;
    }


    /// <summary>
    /// ! ---- buff 
    /// </summary>
    List<CBuff> m_lstBuff = new List<CBuff>();
    public void AddBuff(int nBuffId)
    {
        if (!IsMainPlayer())
        { // 由MainPlayer来发起Buff的生成事件
            return;
        }

        if (!CBuffEditor.s_Instance.CheckIfBuffIdValid(nBuffId))
        {
            Debug.Log("buff id invalid");
            return;
        }

        CBuff buff = CreateBuff(nBuffId, m_lBuffGUID++);

        //m_lstBuff.Add ( buff );
        // Buff不能叠加，只能覆盖
        bool bCover = false;
        for (int i = 0; i < m_lstBuff.Count; i++)
        {
            CBuff the_buf = m_lstBuff[i];
            if (the_buf.GetConfig().id == nBuffId)
            {
                m_lstBuff[i] = buff;
                ResourceManager.RecycleBuff(the_buf);
                bCover = true;
                break;
            }
        }

        if (!bCover)
        {
            m_lstBuff.Add(buff);

        }

        photonView.RPC("RPC_AddBuff", PhotonTargets.All, nBuffId, (long)buff.GetGUID(), buff.GetTime());
    }

    public void Local_AddBuff(int nBuffId, long lGUID, float fLeftTime)
    {
        CBuff buff = CreateBuff(nBuffId, lGUID);
        buff.SetTime(fLeftTime);
        bool bCover = false;
        for (int i = 0; i < m_lstBuff.Count; i++)
        {
            CBuff the_buf = m_lstBuff[i];
            if (the_buf.GetConfig().id == nBuffId)
            {
                m_lstBuff[i] = buff;
                ResourceManager.RecycleBuff(the_buf);
                bCover = true;
                break;
            }
        }
        if (!bCover)
        {
            m_lstBuff.Add(buff);
        }
    }

    static long m_lBuffGUID = 0;
    public static CBuff CreateBuff(int nBuffId, long lGUID)
    {
        CBuff buff = ResourceManager.ReuseBuff();
        buff.SetGUID(lGUID);
        CBuffEditor.sBuffConfig config = CBuffEditor.s_Instance.GetBuffConfigById(nBuffId);
        buff.RefreshConfig(config);
        return buff;
    }


    [PunRPC]
    public void RPC_AddBuff(int nBuffId, long lGUID, float fLeftTime)
    {
        if (IsMainPlayer())
        {
            return;
        }

        Local_AddBuff(nBuffId, lGUID, fLeftTime);
    }

    public void RemoveBuff(CBuff buff)
    {
        m_lstBuff.Remove(buff);
        ResourceManager.RecycleBuff(buff);

        photonView.RPC("RPC_RemoveBuff", PhotonTargets.All, buff.GetGUID());
    }

    [PunRPC]
    public void RPC_RemoveBuff(long lGUID)
    {
        if (IsMainPlayer())
        {
            return;
        }
        for (int i = 0; i < m_lstBuff.Count; i++)
        {
            CBuff buff = m_lstBuff[i];
            if (buff.GetGUID() == lGUID)
            {
                m_lstBuff.Remove(buff);
                ResourceManager.RecycleBuff(buff);
                return;
            }
        }
    }




    void BuffLoop()
    {
        if (!IsMainPlayer())
        { // 暂定这样：Buff由 MainPlayer统一维护，实时同步给其余客户端，其余客户端只负责显示
            return;
        }
        string szContent = "";
        for (int i = m_lstBuff.Count - 1; i >= 0; i--)
        {
            CBuff buff = m_lstBuff[i];
            float fLeftTime = buff.GetTime();
            fLeftTime -= Time.deltaTime;
            buff.SetTime(fLeftTime);
            if (buff.CheckIfEnd())
            {
                RemoveBuff(buff);
            }

            /*
            player_move_speed,            // 玩家移动速度
		player_spit_spore_speed,      // 吐孢子速度
		player_spit_spore_distance,   // 吐孢子距离
		explode_child_num,            // 影响炸球的分球数
		explode_mother_left_percent,  // 影响炸球时母球的保留体积
		shell_time,                   // 影响壳的持续时间
            */
            string szDesc = "";
            switch ((CBuffEditor.eBuffFunc)buff.GetConfig().func)
            {
                case CBuffEditor.eBuffFunc.player_move_speed:
                    {
                        szDesc = "影响移动速度：" + buff.GetConfig().aryValues[0] * 100 + "%";
                    }
                    break;
                case CBuffEditor.eBuffFunc.player_spit_spore_speed:
                    {
                        szDesc = "影响吐孢子速度";
                    }
                    break;
                case CBuffEditor.eBuffFunc.player_spit_spore_distance:
                    {
                        szDesc = "影响吐孢子距离";
                    }
                    break;
                case CBuffEditor.eBuffFunc.explode_child_num:
                    {
                        szDesc = "影响分球个数";
                    }
                    break;
                case CBuffEditor.eBuffFunc.explode_mother_left_percent:
                    {
                        szDesc = "影响分球母体残留百分比";
                    }
                    break;
                case CBuffEditor.eBuffFunc.shell_time:
                    {
                        szDesc = "影响壳的时间";
                    }
                    break;

            }
            // debug info
            szContent += "Buff:" + szDesc + "(" + fLeftTime.ToString("f0") + ")" + "\n";

        }  // end for
        CMsgSystem.s_Instance.ShowMsg(0, szContent);
    }

    //// ! ---- end buff


    //// ! ----  Level
    /// 这些数据只是本机MainPlayer用，又何须挂在Player类里呢？就挂在成长系统（CGrowSystem）里，免得维护两份数据
    /// 
    public void SetMP(float val)
    {
        if (IsMainPlayer())
        {
            CGrowSystem.s_Instance.UpdateMP(val);
        }
    }

    public float GetMP()
    {
        return CGrowSystem.s_Instance.GetMP();
    }


    public void AddMP(float val)
    {
        CGrowSystem.s_Instance.AddMP(val);
    }

    public void AddExp(int val)
    {
        CGrowSystem.s_Instance.AddExp(val);
    }

    public int GetExp()
    {
        return CGrowSystem.s_Instance.GetExp();
    }

    int m_nLevel;
    public void SetLevel(int val)
    {
        photonView.RPC("RPC_SetLevel", PhotonTargets.All, val);
    }

    [PunRPC]
    public void RPC_SetLevel(int val)
    {
        Local_SetLevel(val);
    }

    public void Local_SetLevel(int val)
    {
        m_nLevel = val;
        if (IsMainPlayer())
        {
            CGrowSystem.s_Instance.UpdateLevel(m_nLevel);
        }
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            ball.SetPlayerLevel(m_nLevel);
        }
        SetPlayerName(GetPlayerName());

        CPlayerManager.s_Instance.SetPlayerData_Common(GetAccount(), CPlayerManager.ePlayerCommonInfoType.level, (float)m_nLevel);
    }

    public void RecoverLevel(int nLevel)
    {
        m_nLevel = nLevel;
        SetPlayerName(GetPlayerName());
        CGrowSystem.s_Instance.SetCurLevel(m_nLevel);
        CGrowSystem.s_Instance.UpdateLevelUI(m_nLevel);
        CGrowSystem.s_Instance.UpdateNextLevelInfo();
    }

    public void RecoverMoney(int nMoney)
    {
        CMoneySystem.s_Instance.SetMoney(nMoney);
    }

    public void RecoverExp(int nExp)
    {
        CGrowSystem.s_Instance.SetExp(nExp);
    }

    public int GetLevel()
    {
        return m_nLevel;
    }

    ///  ! ---- end Level

    /// ! ---- Money
    public void AddMoney(int val)
    {
        CMoneySystem.s_Instance.SetMoney(GetMoney() + val);
    }

    public void CostMoney(int val)
    {
        CMoneySystem.s_Instance.SetMoney(GetMoney() - val);
    }

    public void ChangeMoney(int val)
    {
        CMoneySystem.s_Instance.SetMoney(GetMoney() + val);
    }

    public int GetMoney()
    {
        return CMoneySystem.s_Instance.GetMoney();
    }



    bool m_bCastSkillWhickNeedStopMoving = false;
    public bool IsCastSkillWhickNeedStopMoving()
    {
        return m_bCastSkillWhickNeedStopMoving;
    }

    public bool DoNotMove()
	{

        if (IsMianHe())
        {
            return true;
        }

        return false;
    }

    public void RecoverBaseSpeed(float fBaseSpeed)
    {
        m_BaseData.fBaseSpeed = fBaseSpeed;
    }


    public void AddBaseSpeed(float val)
    {
        //  Main.s_Instance.g_SystemMsg.SetContent( "基础移动速度增加：" + val );
        m_BaseData.fBaseSpeed += val;
        OnBaseSpeedChange();
    }

    public void OnBaseSpeedChange()
    {
        photonView.RPC("RPC_OnBaseSpeedChange", PhotonTargets.All, m_BaseData.fBaseSpeed);
    }


    [PunRPC]
    public void RPC_OnBaseSpeedChange(float fBaseSpeed)
    {
        CPlayerManager.s_Instance.SetPlayerData_Common(GetAccount(), CPlayerManager.ePlayerCommonInfoType.base_basespeed, fBaseSpeed);
    }

    public void AddBaseVolumeByLevel(float fVolume)
    {
        m_fBaseVolumeByLevel += fVolume;
        AddTotalVolume(fVolume);
    }

    public void SetBaseVolumeByLevel(float val)
    {
        m_fBaseVolumeByLevel = val;
        OnBaseVolumeByLevelChanged();
    }

    public void OnBaseVolumeByLevelChanged()
    {
        photonView.RPC("RPC_OnBaseVolumeByLevelChanged", PhotonTargets.All, m_fBaseVolumeByLevel);
    }

    public void RecoverBaseVolumeByLevel(float fBaseVolumeByLevel)
    {
        m_fBaseVolumeByLevel = fBaseVolumeByLevel;
    }

    [PunRPC]
    public void RPC_OnBaseVolumeByLevelChanged(float fBaseVolumeByLevel)
    {
        CPlayerManager.s_Instance.SetPlayerData_Common(GetAccount(), CPlayerManager.ePlayerCommonInfoType.base_basevolume_by_level, fBaseVolumeByLevel);
    }

    public float GetBaseVolumeByLevel(float val)
    {
        return m_fBaseVolumeByLevel;
    }

    public void AddTotalVolume(float val)
    {
        int nNumOfLiveBalls = GetCurLiveBallNum(); ;
        float fAddedVolumePerBall = val / nNumOfLiveBalls;
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            if (ball.IsDead())
            {
                continue;
            }
            float fVolume = ball.GetVolume();
            fVolume += fAddedVolumePerBall;
            ball.SetVolume(fVolume);
            //ball.Local_SetSize( CyberTreeMath.Volume2Scale(fVolume) );
        }
    }

    // 获取当前由道具加成的基础体积值
    public float GetBaseVolumeByItem()
    {
        return m_BaseData.fBaseSize;
    }

    public void SetBaseVolumeByItem(float val)
    {
        m_BaseData.fBaseSize = val;
    }

    public void AddBaseSize(float val)
    {
        //    Main.s_Instance.g_SystemMsg.SetContent("道具使基础体积加了：" + val);
        m_BaseData.fBaseSize += val;
        SetBaseVolumeByItem(m_BaseData.fBaseSize);
        AddTotalVolume(val);
    }

    public float GetTotalVolume(ref int nNumOfLiveBalls)
    {
        float fTotalVolume = 0f;
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            if (ball.IsDead())
            {
                continue;
            }
            fTotalVolume += ball.GetVolume();
            nNumOfLiveBalls++;
        }
        return fTotalVolume;
    }

    public void SetTotalVolume(float val)
    {
        m_fTotalArea = val;
    }

    public float GetTotalVolume()
    {
        return m_fTotalArea;
    }

    public void AddBaseMP(float val)
    {
        //  Main.s_Instance.g_SystemMsg.SetContent("基础蓝增加：" + val);
        m_BaseData.fBaseMp += val;

        CGrowSystem.s_Instance.UpdateMaxMP();
    }

    public float GetBaseMp()
    {
        return m_BaseData.fBaseMp;
    }

    public void AddBaseSpitDistance(float val)
    {
        //   Main.s_Instance.g_SystemMsg.SetContent("基础分球距离：" + val);
        m_BaseData.fBaseSpitDistance += val;
    }

    public float GetBaseSpitDistance()
    {
        return m_BaseData.fBaseSpitDistance;
    }

    public void DecreaseShellTime(float val)
    {
        //  Main.s_Instance.g_SystemMsg.SetContent("壳时间变为原来的：" + val);
        m_BaseData.fBaseShellTime = val;
        OnBaseShellTimeChanged();
    }

    public void RecoverBaseShellTime(float fBaseShellTime)
    {
        m_BaseData.fBaseShellTime = fBaseShellTime;
    }

    public float GetDecreaseShellTime()
    {

        return m_BaseData.fBaseShellTime;
    }

    public void SetDecreaseShellTime(float val)
    {
        m_BaseData.fBaseShellTime = val;
        OnBaseShellTimeChanged();
    }

    public void OnBaseShellTimeChanged()
    {
        photonView.RPC("RPC_OnBaseShellTimeChanged", PhotonTargets.All, m_BaseData.fBaseShellTime);
    }

    [PunRPC]
    public void RPC_OnBaseShellTimeChanged(float fBaseShellTime)
    {
        CPlayerManager.s_Instance.SetPlayerData_Common(GetAccount(), CPlayerManager.ePlayerCommonInfoType.base_baseshelltime, fBaseShellTime);
    }

    public void PushThorn(float fSporeRadius, long lThornGuid, Vector3 pos, Vector2 dir, Vector2 sporeDir)
    {
        photonView.RPC("RPC_PushThorn", PhotonTargets.All, fSporeRadius, lThornGuid, pos.x, pos.y, dir.x, dir.y, sporeDir.x, sporeDir.y);
    }

    [PunRPC]
    public void RPC_PushThorn(float fSporeRadius, long lThornGuid, float fPosX, float fPosY, float fDirX, float fDirY, float fSporeDirX, float fSporeDirY)
    {
        CMonster monster = CClassEditor.s_Instance.GetMonsterByGuid(lThornGuid);

        if (IsMainPlayer())
        {
            AddBeingPushedThorn(monster);
        }

        // 根据孢子的体积，计算出推刺距离(推的远度与体积成正比)
        float fDis = fSporeRadius * CSkillSystem.s_Instance.m_SpitSporeParam.fPushThornDis; // 规则暂定：推刺距离与孢子的等级呈线性正比

        vec2Temp.x = fDirX;
        vec2Temp.y = fDirY;

        float fRotationSpeed = 0;

        if (fDirX > 0 && fDirY > 0)
        {
            if ((fSporeDirX > 0 && fSporeDirY < 0) || fSporeDirX > 0 && fSporeDirY > 0)
            {
                fRotationSpeed = 2;
            }
            else
            {
                fRotationSpeed = -2;
            }
        }
        else if (fDirX > 0 && fDirY < 0)
        {
            if ((fSporeDirX > 0 && fSporeDirY < 0) || fSporeDirX < 0 && fSporeDirY < 0)
            {
                fRotationSpeed = 2;
            }
            else
            {
                fRotationSpeed = -2;
            }
        }
        else if (fDirX < 0 && fDirY < 0)
        {
            if ((fSporeDirX < 0 && fSporeDirY > 0) || fSporeDirX < 0 && fSporeDirY < 0)
            {
                fRotationSpeed = 2;
            }
            else
            {
                fRotationSpeed = -2;
            }
        }
        else if (fDirX < 0 && fDirY > 0)
        {
            if ((fSporeDirX > 0 && fSporeDirY > 0) || fSporeDirX < 0 && fSporeDirY > 0)
            {
                fRotationSpeed = 2;
            }
            else
            {
                fRotationSpeed = -2;
            }
        }



        monster.SetDir(vec2Temp);
        monster.BeginEject(fDis, 0.5f, fRotationSpeed);

        CClassEditor.s_Instance.AddToPushedThornList(monster);
    }

    List<CMonster> m_lstBeingPushedThorn = new List<CMonster>();
    public void AddBeingPushedThorn(CMonster monster)
    {
        for (int i = 0; i < m_lstBeingPushedThorn.Count; i++)
        {
            if (m_lstBeingPushedThorn[i].GetGuid() == monster.GetGuid())
            {
                m_lstBeingPushedThorn[i].SetFloatParam(0, (float)PhotonNetwork.time);
                return;
            }
        }
        monster.SetFloatParam(0, (float)PhotonNetwork.time);
        m_lstBeingPushedThorn.Add(monster);
    }


    public void PublishMessage(string szContent)
    {
        photonView.RPC("RPC_PublishMessage", PhotonTargets.All, szContent);
    }

    [PunRPC]
    public void RPC_PublishMessage(string szContent)
    {
        //   CChatSystem.s_Instance.ReceiveMessage( m_szPlayerName , szContent );
    }

    public void SyncDustFullInfo(byte[] bytes)
    {
        photonView.RPC("RPC_SyncDustFullInfo", PhotonTargets.Others, bytes);
    }

    [PunRPC]
    public void RPC_SyncDustFullInfo(byte[] bytes)
    {
        StringManager.BeginPopData(bytes);
        int nShit = StringManager.PopData_Short();
        float fTime = StringManager.PopData_Float();
        int nNum = StringManager.PopData_Short();
        for (int i = nShit; i < nNum; i += 1)
        {
            vecTempPos.x = StringManager.PopData_Float();
            vecTempPos.y = StringManager.PopData_Float();
            CStarDust.s_Instance.SetDustPos(i, vecTempPos);
        }
    }

    List<CDust> m_lstSyncDust = new List<CDust>();
    Dictionary<int, CDust> m_dicSyncDust = new Dictionary<int, CDust>();
    public void AddDustToSyncPool(CDust dust)
    {
        /*
        for ( int i = 0; i < m_lstSyncDust.Count; i++ )
        {
            CDust node_dust = m_lstSyncDust[i];
            if (node_dust.GetId() == dust.GetId())
            {
                node_dust.SetCollisionTime( Main.GetTime() );
                return;
            }
        }

        dust.SetCollisionTime(Main.GetTime());
        m_lstSyncDust.Add( dust );
		*/
        m_dicSyncDust[dust.GetId()] = dust;
    }

    public void RemoveDustFromSyncPool(CDust dust)
    {
        m_dicSyncDust.Remove(dust.GetId());
    }

    // 衰减    
    float m_fAttenuateTimeLapse = 0f;
    public void Attenuate()
    {
        if (!IsMainPlayer())
        {
            return;
        }

        if (IsDead())
        {
            return;
        }

        m_fAttenuateTimeLapse += Time.deltaTime;
        if (m_fAttenuateTimeLapse < 1f)
        {
            return;
        }
        m_fAttenuateTimeLapse = 0f;

        // 只把吃进去的体积拿来衰减。基础体积不参与衰减
        // 基础体积分为三个部分：游戏初始时赋予的 + 道具加成的（貌似目前这种道具废弃了） + 升级时加成的
        float fPlayerEatVolume = GetEatVolume();
        if (fPlayerEatVolume == 0f)
        {
            return;
        }
        int nCurLiveBall = GetCurLiveBallNum();
        if (nCurLiveBall == 0)
        {
            return;
        }
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            if (ball.IsDead())
            {
                continue;
            }
            ball.DoAttenuate(fPlayerEatVolume);
        }
    }

    public Material[] s_aryMat = new Material[1];

    public int GetSelectedSkillLevel()
    {
        if (IsMainPlayer())
        {
            m_nSelectedSkillLevel = CSkillSystem.s_Instance.GetSelectSkillCurPoint();
        }
        return m_nSelectedSkillLevel;
    }

    int m_nSelectedSkillLevel = 0;
    public void SetSelectedSkillLevel(int nLevel)
    {
        m_nSelectedSkillLevel = nLevel;
    }

    int m_nSelectedSkillId = 0;
    public void SetSelectedSkillId(int nSkillId)
    {
        m_nSelectedSkillId = nSkillId;
    }

    public int GetSelectedSkillId()
    {
        if (IsMainPlayer())
        {
            m_nSelectedSkillId = CSkillSystem.s_Instance.GetSelectSkillId();
        }

        return m_nSelectedSkillId;
    }

    public void Like(int nCounterId)
    {
        photonView.RPC("RPC_Like", PhotonTargets.All, nCounterId);
    }

    [PunRPC]
    public void RPC_Like(int nCounterId)
    {
        CJieSuanManager.s_Instance.Like(nCounterId, IsMainPlayer());
    }

    // 球体按大小排序
    List<Ball> m_lstSortedBallList = new List<Ball>();
    public void SortBallsByVolume()
    {
        m_lstSortedBallList.Clear();
        bool bFirst = true;
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            if (ball.IsDead())
            {
                continue;
            }

            if (bFirst)
            {
                m_lstSortedBallList.Add(ball);
                bFirst = false;
            }
            else
            {
                bool bInserted = false;
                for (int j = 0; j < m_lstSortedBallList.Count; j++)
                {
                    Ball ball_node = m_lstSortedBallList[j];
                    if (ball.GetVolumeWithoutRealTimeCalculate() > ball_node.GetVolumeWithoutRealTimeCalculate())
                    {
                        m_lstSortedBallList.Insert(j, ball);
                        bInserted = true;
                        break;
                    }
                }
                if (!bInserted)
                {
                    m_lstSortedBallList.Add(ball);
                }

            }

        }
    }

    byte[] _bytesTest = new byte[10000];
    public void TestRPC()
    {
        _bytesTest[0] = 0;
        _bytesTest[1] = 1;
        _bytesTest[2] = 2;
        _bytesTest[3] = 3;
        _bytesTest[4] = 4;
        _bytesTest[5] = 5;
        _bytesTest[6] = 6;
        _bytesTest[7] = 7;
        _bytesTest[8] = 8;

        photonView.RPC("RPC_Shit", PhotonTargets.All, _bytesTest.Take(3).ToArray());
    }

    [PunRPC]
    public void RPC_Shit(byte[] bytes)
    {
        Debug.Log(bytes.Length);
    }

    List<CSpore> m_lstEatenSpore = new List<CSpore>();
    public void EatSpore(CSpore spore)
    {
        spore.SetDead(true);

        photonView.RPC("RPC_EatSpore", PhotonTargets.All, spore.GetKey());
    }


    [PunRPC]
    public void RPC_EatSpore(string szKey)
    {
        CSporeManager.s_Instance.DeleteSpore(szKey);
    }

    public void SyncEatBeanData(byte[] bytes, int length)
    {
        photonView.RPC("RPC_SyncEatBeanData", PhotonTargets.All, bytes.Take(length).ToArray(), Main.GetTime());
    }

    [PunRPC]
    public void RPC_SyncEatBeanData(byte[] bytes, float fTime)
    {
        StringManager.BeginPopData(bytes);
        int num = StringManager.PopData_Short();
        for (int i = 0; i < num; i++)
        {
            short nGuid = StringManager.PopData_Short();
            CBeanManager.s_Instance.EatBean(nGuid, fTime);
        }
    }

    public void SlowDownEject(int nBallIndex)
    {
        //   photonView.RPC("RPC_SlowDownEject", PhotonTargets.Others, nBallIndex );
    }

    [PunRPC]
    public void RPC_SlowDownEject(int nBallIndex)
    {
        Ball ball = GetBallByIndex(nBallIndex);
        ball.SlowDownEject();
    }

    public void EatBean_Test(CBean bean)
    {
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            if (!ball.IsDead())
            {
                ball.EatBean_New(bean);
                return;
            }
        }


    }


    public void ViolentlyGenerateBalls()
    {
        photonView.RPC("RPC_ViolentlyGenerateBalls", PhotonTargets.All);
    }

    [PunRPC]
    public void RPC_ViolentlyGenerateBalls()
    {
        CCheat.s_Instance.ViolentlyGenerateBalls();
    }


    public void ChangeTriangleNum(int nNum)
    {
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            ball.RedrawPolygons(nNum);
        }
    }

    /// <summary>
    /// / Gesture 手势
    /// </summary>
    byte[] _bytesGestures = new byte[1024];
    public void CastGestures(ref List<int> lstGesturesId)
    {
        int nNum = lstGesturesId.Count;
        StringManager.BeginPushData(_bytesGestures);
        StringManager.PushData_Short((short)nNum);
        for (int i = 0; i < lstGesturesId.Count; i++)
        {
            StringManager.PushData_Short((short)lstGesturesId[i]);
        }
        int nBlobLen = StringManager.GetBlobSize();
        photonView.RPC("RPC_CastGestures", PhotonTargets.All, _bytesGestures.Take(nBlobLen).ToArray());


    }

    List<int> m_lstCastingGestures = new List<int>();

    Vector2 vecGestureDir = new Vector2();

    [PunRPC]
    public void RPC_CastGestures(byte[] bytes)
    {
        float fOffsetX = 0f, fOffsetY = 0f;
        Ball ball = GetTheBallNearestToScreenCenter(ref fOffsetX, ref fOffsetY);
        if (ball == null)
        {
            return;
        }

        m_lstCastingGestures.Clear();

        StringManager.BeginPopData(bytes);
        short nNum = StringManager.PopData_Short();
        for (int i = 0; i < nNum; i++)
        {
            int nGestureId = StringManager.PopData_Short();
            m_lstCastingGestures.Add(nGestureId);
        }

        CGestureManager.s_Instance.CastGestures(ref m_lstCastingGestures, GetPlayerName(), ball, fOffsetX, fOffsetY);
    }

    Ball GetTheBallNearestToScreenCenter(ref float fOffsetX, ref float fOffsetY)
    {
        vecTempPos = CCameraManager.s_Instance.GetSceenCenter();

        Ball ball = null;
        bool bFirst = true;
        float fMinDis = 0f;
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball the_ball = m_lstBalls[i];
            if (the_ball.IsDead() || !the_ball.GetActive())
            {
                continue;
            }

            if (bFirst)
            {
                ball = the_ball;
                bFirst = false;

                vecTempDir = vecTempPos - the_ball.GetPos();
                vecTempDir.Normalize();

                vecTempPos1.x = the_ball.GetPos().x + the_ball.GetSize() * vecTempDir.x;
                vecTempPos1.y = the_ball.GetPos().y + the_ball.GetSize() * vecTempDir.y;
                fMinDis = Vector2.Distance(vecTempPos, vecTempPos1);
                fOffsetX = the_ball.GetPos().x - vecTempPos.x;
                fOffsetY = the_ball.GetPos().y - vecTempPos.y;
                continue;
            }

            vecTempDir = vecTempPos - the_ball.GetPos();
            vecTempDir.Normalize();
            vecTempPos1.x = the_ball.GetPos().x + the_ball.GetSize() * vecTempDir.x;
            vecTempPos1.y = the_ball.GetPos().y + the_ball.GetSize() * vecTempDir.y;
            float fDis = Vector2.Distance(vecTempPos, vecTempPos1);
            if (fDis < fMinDis)
            {
                fOffsetX = the_ball.GetPos().x - vecTempPos.x;
                fOffsetY = the_ball.GetPos().y - vecTempPos.y;
                fMinDis = fDis;
                ball = the_ball;
            }

        }

        return ball;
    }

    public void Test_AddBloodStain()
    {
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            if (ball.IsDead() || !ball.GetActive())
            {
                continue;
            }

            vecTempPos = ball.GetPos();
            vecTempPos.x += ball.GetRadius() * ball.GetDir().x;
            vecTempPos.y += ball.GetRadius() * ball.GetDir().y;
            vecTempDir = ball.GetDir();
            float fScale = ball.GetSize() / CBloodStainManager.s_Instance.GetScaleRatioByType(CBloodStainManager.eBloodStainType.dead_spray);
            CBloodStainManager.s_Instance.AddBloodStain(vecTempPos, vecTempDir, fScale, fScale, CBloodStainManager.eBloodStainType.dead_spray, ball._Player.GetColor());

            // 原地血迹
            vecTempPos = ball.GetPos();
            CBloodStainManager.s_Instance.AddBloodStain(vecTempPos, vecTempDir, fScale, fScale, CBloodStainManager.eBloodStainType.dead_static, ball._Player.GetColor());

        }
    }

    public void BecomeZhangYu()
    {
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            ball.BecomeZhangYu();
        }
    }

    public void AddActiveBall(Ball ball)
    {
        m_dicActiveBalls[ball.GetIndex()] = ball;
    }

    public void RemoveActiveBall(Ball ball)
    {
        m_dicActiveBalls.Remove(ball.GetIndex());
    }

    float m_fSyncPlayerCommonInfoTimeElapse = 0;
    byte[] _bytesCommonInfo = new byte[16];
    void SyncPlayerCommonInfo()
    {
        if (!IsMainPlayer()) // self
        {
            return;
        }

        m_fSyncPlayerCommonInfoTimeElapse += Time.deltaTime;
        if (m_fSyncPlayerCommonInfoTimeElapse < 0.5f)
        {
            return;
        }
        m_fSyncPlayerCommonInfoTimeElapse = 0f;

        StringManager.BeginPushData(_bytesCommonInfo);
        bool bMoneyChanged = CMoneySystem.s_Instance.GetMoneyChanged();
        if (bMoneyChanged)
        {
            StringManager.PushData_Short((short)GetMoney());
            CMoneySystem.s_Instance.SetMoneyChanged(false);
        }
        else
        {
            StringManager.PushData_Short(-1);
        }

        bool bExpChanged = CGrowSystem.s_Instance.GetExpChanged();
        if (bExpChanged)
        {
            StringManager.PushData_Short((short)GetExp());
            CGrowSystem.s_Instance.SetExpChanged(false);
        }
        else
        {
            StringManager.PushData_Short(-1);
        }

        if ((!bMoneyChanged) && (!bExpChanged))
        {
            return;
        }

        photonView.RPC("RPC_SyncPlayerCommonInfo", PhotonTargets.Others, _bytesCommonInfo);
    }

    [PunRPC]
    public void RPC_SyncPlayerCommonInfo(byte[] bytes)
    {
        StringManager.BeginPopData(bytes);
        int nMoney = StringManager.PopData_Short();
        int nExp = StringManager.PopData_Short();

        if (nMoney > 0)
        {
            CPlayerManager.s_Instance.SetPlayerData_Common(GetAccount(), CPlayerManager.ePlayerCommonInfoType.money, (float)nMoney);
        }

        if (nExp > 0)
        {
            CPlayerManager.s_Instance.SetPlayerData_Common(GetAccount(), CPlayerManager.ePlayerCommonInfoType.exp, (float)nExp);
        }
    }

    public void RecoverEatThornNum( int nNum )
    {
        CGrowSystem.s_Instance.SetEatThornNum(nNum);
    }

    public void RecoverSkillPoint( int nCurTotalPoint )
    {
        CSkillSystem.s_Instance.SetTotalPoint(nCurTotalPoint);
    }

    public void RecoverSkillLevel(CSkillSystem.eSkillId eId, int nCurLevel)
    {
        CSkillSystem.s_Instance.SetSkillLevel((int)eId, nCurLevel);
    }

    public void RecoverItemLevel(CItemSystem.eItemFunc eItemId, int nCurLevel)
    {
        SetItemNum((int)eItemId, nCurLevel);
        CItemSystem.s_Instance.SetItemLevel(eItemId, nCurLevel);
    }

    public void OnItemLevelChanged(int nItemId, int nCurLevel)
    {
        photonView.RPC("RPC_OnItemLevelChanged", PhotonTargets.All, nItemId, nCurLevel);
    }

    [PunRPC]
    public void RPC_OnItemLevelChanged(int nItemId, int nCurLevel)
    {
        if (nItemId == (int)CItemSystem.eItemFunc.base_speed)
        {
            CPlayerManager.s_Instance.SetPlayerData_Common(GetAccount(), CPlayerManager.ePlayerCommonInfoType.item_basespeed_level, (float)nCurLevel);
        }
        else if (nItemId == (int)CItemSystem.eItemFunc.shell_time)
        {
            CPlayerManager.s_Instance.SetPlayerData_Common(GetAccount(), CPlayerManager.ePlayerCommonInfoType.item_baseshelltime_level, (float)nCurLevel);
        }
    }


    public void OnSkillLevelChanged(int nSkillId, int nCurLevel)
    {
        photonView.RPC("RPC_OnSkillLevelChanged", PhotonTargets.All, nSkillId, nCurLevel);
    }

    [PunRPC]
    public void RPC_OnSkillLevelChanged(int nSkillId, int nCurLevel)
    {
        if (nSkillId == (int)CSkillSystem.eSkillId.w_spit)
        {
            CPlayerManager.s_Instance.SetPlayerData_Common(GetAccount(), CPlayerManager.ePlayerCommonInfoType.skill_w_level, (float)nCurLevel);
        }
        else if (nSkillId == (int)CSkillSystem.eSkillId.e_unfold)
        {
            CPlayerManager.s_Instance.SetPlayerData_Common(GetAccount(), CPlayerManager.ePlayerCommonInfoType.skill_e_level, (float)nCurLevel);
        }
        else if (nSkillId >= (int)CSkillSystem.eSkillId.r_sneak && nSkillId <= (int)CSkillSystem.eSkillId.p_gold)
        {
            CPlayerManager.s_Instance.SetPlayerData_Common(GetAccount(), CPlayerManager.ePlayerCommonInfoType.skill_r_level, (float)nCurLevel);

        }
    }


    public void OnTotalPointChanged(int nCurTotalPoint)
    {
        photonView.RPC("RPC_OnTotalPointChanged", PhotonTargets.All, nCurTotalPoint);
    }

    [PunRPC]
    public void RPC_OnTotalPointChanged(int nCurTotalPoint)
    {
        CPlayerManager.s_Instance.SetPlayerData_Common(GetAccount(), CPlayerManager.ePlayerCommonInfoType.skill_point, (float)nCurTotalPoint);


    }

    public void AddMostBigBallsToShowName( int nBallIndex )
    {
        if (_PlayerIns)
        {
            _PlayerIns.AddMostBigBallsToShowName(nBallIndex);
        }
    }

    public void ClearMostBigBallsToShowName()
    {
        if (_PlayerIns)
        {
            _PlayerIns.ClearMostBigBallsToShowName();
        }
    }
















}// end Player
