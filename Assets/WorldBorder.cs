﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldBorder : MonoBehaviour {

	public BoxCollider2D _colliderEdge;
    public LineRenderer _lr;

    Vector3 vecTempPos = new Vector3();
    Vector2 vecTemp2 = new Vector2();
    Vector2[] m_aryColliderPoints = new Vector2[2];


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}

   
    public void SetSize(float width, float height, int type)
    {
        if (_colliderEdge == null)
        {
			_colliderEdge = this.gameObject.AddComponent<BoxCollider2D>();
        }

        switch (type)
        {
            case 0: // top
                {
                    vecTempPos.x = -width / 2.0f;
                    vecTempPos.y = height / 2.0f;
                    vecTempPos.z = 1.0f;
                    _lr.SetPosition(0, vecTempPos);
                    vecTemp2.x = vecTempPos.x;
                    vecTemp2.y = vecTempPos.y;
                    m_aryColliderPoints[0] = vecTemp2;
                    vecTempPos.x = width / 2.0f;
                    vecTempPos.y = height / 2.0f;
                    vecTempPos.z = 1.0f;
                    _lr.SetPosition(1, vecTempPos);
                    vecTemp2.x = vecTempPos.x;
                    vecTemp2.y = vecTempPos.y;
                    m_aryColliderPoints[1] = vecTemp2;

				_colliderEdge.size = new Vector3( width, 20f, 1f )  ;
				_colliderEdge.offset = new Vector3( 0f, height / 2 + 10f, 0f );


                }
                break;
            case 1: // bottom
                {
                    vecTempPos.x = -width / 2.0f;
                    vecTempPos.y = -height / 2.0f;
                    vecTempPos.z = 1.0f;
                    _lr.SetPosition(0, vecTempPos);
                    vecTemp2.x = vecTempPos.x;
                    vecTemp2.y = vecTempPos.y;
                    m_aryColliderPoints[0] = vecTemp2;

                    vecTempPos.x = width / 2.0f;
                    vecTempPos.y = -height / 2.0f;
                    vecTempPos.z = 1.0f;
                    _lr.SetPosition(1, vecTempPos);
                    vecTemp2.x = vecTempPos.x;
                    vecTemp2.y = vecTempPos.y;
                    m_aryColliderPoints[1] = vecTemp2;
                //    _colliderEdge.points = m_aryColliderPoints;
				_colliderEdge.size = new Vector3( width, 20f, 1f )  ;
				_colliderEdge.offset = new Vector3( 0f, -height / 2 - 10f, 0f );
                }
                break;
            case 2: // left
                {
                    vecTempPos.x = -width / 2.0f;
                    vecTempPos.y = height / 2.0f;
                    vecTempPos.z = 1.0f;
                    _lr.SetPosition(0, vecTempPos);
                    vecTemp2.x = vecTempPos.x;
                    vecTemp2.y = vecTempPos.y;
                    m_aryColliderPoints[0] = vecTemp2;

                    vecTempPos.x = -width / 2.0f;
                    vecTempPos.y = -height / 2.0f;
                    vecTempPos.z = 1.0f;
                    _lr.SetPosition(1, vecTempPos);
                    vecTemp2.x = vecTempPos.x;
                    vecTemp2.y = vecTempPos.y;
                    m_aryColliderPoints[1] = vecTemp2;
                   // _colliderEdge.points = m_aryColliderPoints;
				_colliderEdge.size = new Vector3( 20f, height, 1f )  ;
				_colliderEdge.offset = new Vector3( -width / 2f -10f , 0f , 0f );

                }
                break;
            case 3: // right
                {
                    vecTempPos.x = width / 2.0f;
                    vecTempPos.y = height / 2.0f;
                    vecTempPos.z = 1.0f;
                    _lr.SetPosition(0, vecTempPos);
                    vecTemp2.x = vecTempPos.x;
                    vecTemp2.y = vecTempPos.y;
                    m_aryColliderPoints[0] = vecTemp2;

                    vecTempPos.x = width / 2.0f;
                    vecTempPos.y = -height / 2.0f;
                    vecTempPos.z = 1.0f;
                    _lr.SetPosition(1, vecTempPos);
                    vecTemp2.x = vecTempPos.x;
                    vecTemp2.y = vecTempPos.y;
                    m_aryColliderPoints[1] = vecTemp2;
                 //   _colliderEdge.points = m_aryColliderPoints;
				_colliderEdge.size = new Vector3( 20f, height, 1f )  ;
				_colliderEdge.offset = new Vector3( width / 2f + 10f , 0f , 0f );
                }
                break;
        }


		_colliderEdge.enabled = true;
    }
}
