﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CPlayerIns : MonoBehaviour {

    string m_szAccount = "";
    List<Ball> m_lstBalls = new List<Ball>();
    public Player _Player = null;

    static Vector3 vecTempPos = new Vector3();

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        InitingBalls();

        IntervalLoop_0_3();
        IntervalLoop_1();
        IntervalLoop_0_1();

        DoingExplode();

        if (IsMainPlayer())
        {
			SkillLoop ();
        }


    }

    public void SetPlayer(Player player)
    {
        _Player = player;
        if (_Player != null) // 如果遇到“孤儿球”群体，这里_Player就会为null
        {
            _Player.SetBallList(m_lstBalls);
        }
    }

    public Player GetPlayer()
    {
        return _Player;
    }

    public void SetAccount(string szAccount)
    {
        m_szAccount = szAccount;
    }

    public string GetAccount()
    {
        return m_szAccount;
    }

    bool m_bIniting = false;
    int m_nSkinId = 0;
    Color m_Color;
    int m_nColorId = 0;
    string m_szPlayerName = "";
    byte[] m_bytesBallsData = null;
    float m_fBeginInitingTime = 0;
    public void BeginInitBalls(byte[] bytesBallsData, string szAccount, int nSkinId, int nColor, string szPlayerName)
    {
        m_fBeginInitingTime = Main.GetTime();
        m_bytesBallsData = bytesBallsData;
        SetColorId( nColor );
        m_Color = ResourceManager.s_Instance.GetColorByPlayerId(m_nColorId);
        SetSkinId(nSkinId );
        SetAccount(szAccount);
        m_bIniting = true;
        m_nInitingIndex = 0;

        SetPlayerName(szPlayerName);
    }

    public void SetPlayerName( string szPlayerName)
    {
       m_szPlayerName = szPlayerName;
    }

    public string GetPlayerName()
    {
        return m_szPlayerName;
    }

    public void SetColorId( int nColorId )
    {
        m_nColorId = nColorId;
    }

    public int GetColorId()
    {
        return m_nColorId;
    }

    public void SetSkinId( int nSkinId )
    {
        m_nSkinId = nSkinId;
    }

    public int GetSkinId()
    {
        return m_nSkinId;
    }

    const float c_fInitingBallsInterval = 0.1f;
    float m_fInitingBallsTimeCount = 0f;
    int m_nInitingIndex = 0;
    void InitingBalls()
    {
        if (!m_bIniting)
        {
            return;
        }

        // 配置文件初始化成功之前，不要初始化Player。因为网络读取文件是异步的。
        if (!MapEditor.s_Instance.IsMapInitCompleted())
        {
            return;
        }

        EndInitBalls();
        /*
        if (_Player != null && _Player.IsMainPlayer())
        {

        }
        else
        {
            m_fInitingBallsTimeCount += Time.deltaTime;
            if (m_fInitingBallsTimeCount < c_fInitingBallsInterval)
            {
                return;
            }
            m_fInitingBallsTimeCount = 0;
        }

  

        if ( m_nInitingIndex >= Main.s_Instance.m_fMaxBallNumPerPlayer )
        {
            EndInitBalls();
            return;
        }

       

        // 兼容所有的情况，比如：这群球先是作为“孤儿球”被实例化出来；后来它们的Player上线了，则重新接管它们。甚至，作为孤儿球的身份实例化到一部分的时候，Player突然上线了。都可以兼容，不会出错。
        Ball ball = null;
        if (m_nInitingIndex < m_lstBalls.Count)
        {
            ball = m_lstBalls[m_nInitingIndex];
            ball.SetPlayer(_Player); // ghost ball在这里得到的Player是null。 如果该player后来又上线了，则可以重新接管自己的球球
        }
        else
        {
            ball = GameObject.Instantiate(Main.s_Instance.m_preBall).GetComponent<Ball>();
            ball.SetIndex(m_nInitingIndex);
            ball.SetPlayer(_Player); // ghost ball在这里得到的Player是null。 如果该player后来又上线了，则可以重新接管自己的球球
            ball.GenerateMeshShape(m_nSkinId, m_Color);
            ball.transform.SetParent(this.transform);
            ball.SetDead(true);
            ball.SetPlayerIns(this);
            m_lstBalls.Add(ball);
        }

        ball.SetPlayerName(m_szPlayerName);


        m_nInitingIndex++;
        */
    }

    // Master-Client负责同步（这里必须做容错，不然网络丢包就麻烦了）[to rongcuo]
    // 容错机制：如果规定时间内没有收到返回包，则再发一次，直到收到为止
    void EndInitBalls()
    {
        m_bIniting = false;
        DoSomeCollisionIgnore();
        PlayerAction.s_Instance.StopAndMoveToCenter();

        float fTimeElapseSinceBegin = Main.GetTime() - m_fBeginInitingTime;

        bool bBallAlreadyExists = false;
        if (m_bytesBallsData != null)
        {
            bBallAlreadyExists = AnalyzeBallData(m_bytesBallsData, fTimeElapseSinceBegin);
        }

        if (_Player)
        {

            _Player.SetPlayerInitCompleted(true);
            _Player.PleaseFuckMyAss();
            ResetBallsToUse();

            if (_Player.IsMainPlayer()) // 本客户端MainPlayer，包括master-client
            {
                if ( !bBallAlreadyExists )
                {
                    _Player.DoReborn();
                }
            }
            else // 其它客户端的player
            {
               
            }

            _Player.SetMyDataLoaded(true);
            _Player.DoSomethingWhenLogin();
        }
        else // 孤儿球（其所属的Player已经掉线）
        {

        }
       
       
    }

    public Ball GetBallByIndex( int nBallIndex )
    {
        for ( int i = 0; i < m_lstBalls.Count; i++ )
        {
            Ball ball = m_lstBalls[i];
            if ( ball.GetIndex() == nBallIndex)
            {
                return ball;
            }
        }
        return null;
    }

    // [to youhua] 关于碰撞器的优化还要继续做，做到极致。
    public void DoSomeCollisionIgnore()
    {
        for (int i = 0; i < m_lstBalls.Count - 1; i++)
        {
            Ball ball1 = m_lstBalls[i];


            for (int j = i; j < m_lstBalls.Count; j++)
            {
                Ball ball2 = m_lstBalls[j];

                Physics2D.IgnoreCollision(ball1._ColliderDust, ball2._ColliderDust);
                Physics2D.IgnoreCollision(ball1._ColliderDust, ball2._Collider);
                Physics2D.IgnoreCollision(ball1._ColliderDust, ball2._ColliderTemp);

                Physics2D.IgnoreCollision(ball2._ColliderDust, ball1._ColliderDust);
                Physics2D.IgnoreCollision(ball2._ColliderDust, ball1._Collider);
                Physics2D.IgnoreCollision(ball2._ColliderDust, ball1._ColliderTemp);


                Physics2D.IgnoreCollision(ball1._ColliderTemp, ball2._ColliderTemp);
                Physics2D.IgnoreCollision(ball1._ColliderTemp, ball2._Collider);
                Physics2D.IgnoreCollision(ball1._Collider, ball2._ColliderTemp);

                Physics2D.IgnoreCollision(ball1._Trigger, ball2._ColliderDust);
                Physics2D.IgnoreCollision(ball1._Trigger, ball2._Collider);
                Physics2D.IgnoreCollision(ball1._Trigger, ball2._ColliderTemp);
                Physics2D.IgnoreCollision(ball2._Trigger, ball1._ColliderDust);
                Physics2D.IgnoreCollision(ball2._Trigger, ball1._Collider);
                Physics2D.IgnoreCollision(ball2._Trigger, ball1._ColliderTemp);


                Physics2D.IgnoreCollision(ball1._TriggerMergeSelf, ball2._ColliderDust);
                Physics2D.IgnoreCollision(ball1._TriggerMergeSelf, ball2._Collider);
                Physics2D.IgnoreCollision(ball1._TriggerMergeSelf, ball2._ColliderTemp);
                Physics2D.IgnoreCollision(ball2._TriggerMergeSelf, ball1._ColliderDust);
                Physics2D.IgnoreCollision(ball2._TriggerMergeSelf, ball1._Collider);
                Physics2D.IgnoreCollision(ball2._TriggerMergeSelf, ball1._ColliderTemp);


                Physics2D.IgnoreCollision(ball2._Trigger, ball1._Trigger);
                Physics2D.IgnoreCollision(ball2._Trigger, ball1._TriggerMergeSelf);
                Physics2D.IgnoreCollision(ball1._Trigger, ball2._TriggerMergeSelf);


                if (!IsMainPlayer())
                {
                    Physics2D.IgnoreCollision(ball2._TriggerMergeSelf, ball1._TriggerMergeSelf);
                }
                else
                {
                    Physics2D.IgnoreCollision(ball2._TriggerMergeSelf, ball1._TriggerMergeSelf, false);
                }
            } // end for j
        } // end for i
    }

    public void DoSomeCollisionIgnore_New( Ball ball1 )
    {
        for (int i = 0; i < m_lstBalls.Count - 1; i++)
        {
     
                Ball ball2 = m_lstBalls[i];

                Physics2D.IgnoreCollision(ball1._ColliderDust, ball2._ColliderDust);
                Physics2D.IgnoreCollision(ball1._ColliderDust, ball2._Collider);
                Physics2D.IgnoreCollision(ball1._ColliderDust, ball2._ColliderTemp);

                Physics2D.IgnoreCollision(ball2._ColliderDust, ball1._ColliderDust);
                Physics2D.IgnoreCollision(ball2._ColliderDust, ball1._Collider);
                Physics2D.IgnoreCollision(ball2._ColliderDust, ball1._ColliderTemp);


                Physics2D.IgnoreCollision(ball1._ColliderTemp, ball2._ColliderTemp);
                Physics2D.IgnoreCollision(ball1._ColliderTemp, ball2._Collider);
                Physics2D.IgnoreCollision(ball1._Collider, ball2._ColliderTemp);

                Physics2D.IgnoreCollision(ball1._Trigger, ball2._ColliderDust);
                Physics2D.IgnoreCollision(ball1._Trigger, ball2._Collider);
                Physics2D.IgnoreCollision(ball1._Trigger, ball2._ColliderTemp);
                Physics2D.IgnoreCollision(ball2._Trigger, ball1._ColliderDust);
                Physics2D.IgnoreCollision(ball2._Trigger, ball1._Collider);
                Physics2D.IgnoreCollision(ball2._Trigger, ball1._ColliderTemp);


                Physics2D.IgnoreCollision(ball1._TriggerMergeSelf, ball2._ColliderDust);
                Physics2D.IgnoreCollision(ball1._TriggerMergeSelf, ball2._Collider);
                Physics2D.IgnoreCollision(ball1._TriggerMergeSelf, ball2._ColliderTemp);
                Physics2D.IgnoreCollision(ball2._TriggerMergeSelf, ball1._ColliderDust);
                Physics2D.IgnoreCollision(ball2._TriggerMergeSelf, ball1._Collider);
                Physics2D.IgnoreCollision(ball2._TriggerMergeSelf, ball1._ColliderTemp);


                Physics2D.IgnoreCollision(ball2._Trigger, ball1._Trigger);
                Physics2D.IgnoreCollision(ball2._Trigger, ball1._TriggerMergeSelf);
                Physics2D.IgnoreCollision(ball1._Trigger, ball2._TriggerMergeSelf);


                if (!IsMainPlayer())
                {
                    Physics2D.IgnoreCollision(ball2._TriggerMergeSelf, ball1._TriggerMergeSelf);
                }
           
        } // end for i
    }

    public bool IsOrphan()
    {
        return _Player == null;
    }

    public bool IsMainPlayer()
    {
        if (_Player != null )
        {
            return _Player.IsMainPlayer();
        }

        return false;
    }

    byte[] _bytesLiveBallData = new byte[10240];
    public byte[] GenerateLiveBallData( ref int nBlobSize)
    {
        // [to youhua] 断线重连这一块稍后完善
        return null;

        StringManager.BeginPushData(_bytesLiveBallData);
        StringManager.PushData_Int(0);
        StringManager.PushData_Short( (short)m_nColorId ); // 色号（每个玩家分配到一个专属颜色）
        
        int nLiveBallNum = 0;
        for ( int i = 0; i < m_lstBalls.Count; i++ )
        {
            Ball ball = m_lstBalls[i];
            if ( ball.IsDead() )
            {
                continue;
            }
            StringManager.PushData_Short( (short)ball.GetIndex() );
            StringManager.PushData_Float( ball.GetSize() );
            StringManager.PushData_Float(ball.GetPosX());
            StringManager.PushData_Float(ball.GetPosY());
            StringManager.PushData_Float(ball.GetCurShellTime());
            StringManager.PushData_Float(ball.GetTotalShellTime());
            nLiveBallNum++;
        }

        nBlobSize = StringManager.GetBlobSize();
        StringManager.SetCurPointerPos( 0 );
        StringManager.PushData_Int(nLiveBallNum);
       
        if (nLiveBallNum > 0)
        {
            return _bytesLiveBallData.Take(nBlobSize).ToArray();
        }
        else
        {
            return null;
        }
        
    }

    public bool AnalyzeBallData(byte[] bytes, float fTimeElapseSinceBegin)
    {
        StringManager.BeginPopData(bytes);
        int nLiveBallNum = StringManager.PopData_Int();
        if (nLiveBallNum == 0)
        {
            return false;
        }
        /*
        int nLenOfAccount = StringManager.PopData_Short();
        string szAccount = StringManager.PopData_String(nLenOfAccount);
        */
        int nColorId = StringManager.PopData_Short();

        for (int i = 0; i < nLiveBallNum; i++)
        {
            int nBallIndex = StringManager.PopData_Short();
            float fSize = StringManager.PopData_Float();
            float fPosX = StringManager.PopData_Float();
            float fPosY = StringManager.PopData_Float();
            float fCurShellTime = StringManager.PopData_Float();
            float fTotalShellTime = StringManager.PopData_Float();
            Ball ball = m_lstBalls[nBallIndex];
            ball.SetDead(false);
            ball.SetSize(fSize);
            float fRadius = 0f;
            ball.SetVolume(CyberTreeMath.Scale2Volume(fSize, ref fRadius));
            ball.SetRadius(fRadius);
            ball.SetActive(true);
            vecTempPos.x = fPosX;
            vecTempPos.y = fPosY;
            vecTempPos.z = -fSize;

            ball.SetPos(vecTempPos);

            fCurShellTime += fTimeElapseSinceBegin;
            ball.Local_SetShellInfo(fTotalShellTime, fCurShellTime);
            ball.Local_BeginShell();
            ball.UpdateShell();


            SetOneBallUsed(nBallIndex);
        }

        return true;
    }

    public void DestroyBall( int nBallIndex )
    {
        Ball ball = m_lstBalls[nBallIndex];
        ball.SetDead( true );
    }


    // 0.3秒的轮询
    float m_fIntervalLoopTimeElapse_0_3 = 0f;
    void IntervalLoop_0_3()
    {
        m_fIntervalLoopTimeElapse_0_3 += Time.deltaTime;
        if (m_fIntervalLoopTimeElapse_0_3 < 0.3f)
        {
            return;
        }
        m_fIntervalLoopTimeElapse_0_3 = 0f;

        // 更新分球壳的显示
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            if (ball.IsDead() || (!ball.GetActive()))
            {
                continue;
            }

            ball.UpdateShell();
        }


    
    }

    // 1秒的轮询
    float m_fIntervalLoopTimeElapse_1 = 0f;
    void IntervalLoop_1()
    {
        m_fIntervalLoopTimeElapse_1 += Time.deltaTime;
        if (m_fIntervalLoopTimeElapse_1 < 1f)
        { 
            return;
        }
        m_fIntervalLoopTimeElapse_1 = 0f;

        ///
        ProcessPlayerNameVisible();

        ///
    }

    // 0.1秒的轮询
    float m_fIntervalLoopTimeElapse_0_1 = 0f;
    void IntervalLoop_0_1()
    {
        m_fIntervalLoopTimeElapse_0_1 += Time.deltaTime;
        if (m_fIntervalLoopTimeElapse_0_1 < 0.1f)
        {
            return;
        }
        m_fIntervalLoopTimeElapse_0_1 = 0f;

        ProcessPlayerName();
    }



    void ProcessPlayerName()
    {
        float fCamSize = CCameraManager.s_Instance.GetCurCameraSize();
        for ( int i = 0; i < m_lstBalls.Count; i++ )
        {
            Ball ball = m_lstBalls[i];
            if ( ball.IsDead() )
            {
                continue;
            }
            if ( !ball.GetActive() )
            {
                continue;
            }
            ball.ProcessPlayerName(fCamSize); // [to youhua]这儿是可以优化的，利用球球尺寸的排序
        }
    }

    void ProcessPlayerNameVisible()
    {
        float fCamSize = CCameraManager.s_Instance.GetCurCameraSize();
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
         
            ball.ProcessPlayerNameVisible(fCamSize);
        }
        
    }

    // 衰减    
    float m_fAttenuateTimeLapse = 0f;
    public void Attenuate()
    {
        /*
        if ( _Player )
        {
            if ( !_Player.IsMainPlayer() )
            {
                return;
            }
        }
        else
        {

        }

        m_fAttenuateTimeLapse += Time.deltaTime;
        if (m_fAttenuateTimeLapse < 1f)
        {
            return;
        }
        m_fAttenuateTimeLapse = 0f;

        // 只把吃进去的体积拿来衰减。基础体积不参与衰减
        // 基础体积分为三个部分：游戏初始时赋予的 + 道具加成的（貌似目前这种道具废弃了） + 升级时加成的
        float fPlayerEatVolume = GetEatVolume();
        if (fPlayerEatVolume == 0f)
        {
            return;
        }
        int nCurLiveBall = GetCurLiveBallNum();
        if (nCurLiveBall == 0)
        {
            return;
        }
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            if (ball.IsDead())
            {
                continue;
            }
            ball.DoAttenuate(fPlayerEatVolume);
        }
        */
    }

    public void RemoveFromList( Ball ball )
    {
        m_lstBalls.Remove( ball );
    }

    Ball[] m_aryLiveBalls = new Ball[64];
    public List<int> m_lstNewDeadBall = new List<int>();
    public void SetLiveBall( int nBallIndex, Ball ball )
    {
        m_aryLiveBalls[nBallIndex] = ball;
    }
    
    public Ball GetLiveBall(int nBallIndex)
    {
        return m_aryLiveBalls[nBallIndex];
    }
    
    public void RemoveOneBall(int nBallIndex)
    {
        Ball ball = GetLiveBall(nBallIndex);
        if ( ball == null )
        {
            return;
        }
        ball.SetDead(true);
       
        // 最好不要遍历列表。
        /*
        for (int i = m_lstBalls.Count - 1; i >= 0; i--)
        {
            Ball ball = m_lstBalls[i];
            if (ball.GetIndex() == nBallIndex)
            {
                ball.SetDead(true);
                return;
            }
        }
        */
    }

    public void RemoveAllBalls()
    {
        for (int i = m_lstBalls.Count - 1; i >= 0; i--) // 注意，新的复用机制下，m_lstBalls的下标不能直接用来作球球的索引号了
        {
            Ball ball = m_lstBalls[i];
            ball.SetDead(true);
        }
    }

    bool m_bDead = false;
    public bool CheckIfPlayerDead(int nEaterOwnerId = 0)
    {
        if ( m_lstBalls.Count == 0 )
        {
            m_bDead = true;
        }
        else
        {
            m_bDead = false;
        }
       
        return m_bDead;
    }

    public bool IsDead()
    {
        return m_bDead;
    }

    public void SetDead( bool bDead )
    {
        m_bDead = bDead;
    }

    ///// 炸球相关


    List<CExplodeNode> m_lstExplodingNode = new List<CExplodeNode>();
    List<CExplodeNode> m_lstRecycledExplodeNode = new List<CExplodeNode>();
    public CExplodeNode GetOneExplodeNode()
    {
        CExplodeNode node;
        if (m_lstRecycledExplodeNode.Count == 0)
        {
            node = GameObject.Instantiate(Main.s_Instance.m_preExplodeNode).GetComponent<CExplodeNode>();
        }
        else
        {
            node = m_lstRecycledExplodeNode[0];
            node.gameObject.SetActive(true);
            node.lstChildBallIndex.Clear();
            m_lstRecycledExplodeNode.RemoveAt(0);
        }

        return node;
    }

    public void RecycleOneExplodeNode(CExplodeNode node)
    {
        node.gameObject.SetActive(false);
        m_lstRecycledExplodeNode.Add(node);
    }

    public void AddOneNodeToExplodingList(CExplodeNode node)
    {
        m_lstExplodingNode.Add(node);
    }

    void DoingExplode()
    {
        for (int i = m_lstExplodingNode.Count - 1; i >= 0; i--)
        {
            CExplodeNode node = m_lstExplodingNode[i];
            bool bEnd = false;
            if (node.IsExploding())
            {
                bEnd = node.DoExplode();
            }
            else
            {
                if (node.IsWaiting())
                {
                    bEnd = node.DoWaiting();
                }
                else
                {
                    bEnd = true;
                }
            }

            if (bEnd)
            {
                m_lstExplodingNode.RemoveAt(i);
                RecycleOneExplodeNode(node);
            }
        }
    }



    ///// end 炸球相关


    Dictionary<int, int> m_dicBallsToReuse_New = new Dictionary<int, int>(); // 不能用List， List稍候不慎就会重复添加
    Dictionary<int, Ball> m_dicBallsToReuse = new Dictionary<int, Ball>(); // 不能用List， List稍候不慎就会重复添加

    public void ResetBallsToUse()
    {
        for (int i = 0; i < Main.s_Instance.m_fMaxBallNumPerPlayer; i++)
        {
            m_dicBallsToReuse_New[i] = i;
        }
    }

    public void AddBallToReuseList(Ball ball)
    {
        m_dicBallsToReuse_New[ball.GetIndex()] = ball.GetIndex();
    }

    public Ball GetOneBallToReuse() // 这个流程只是MainPlayer用。OtherClient不会去做决策的，都是被动接受同步
    {
        if (m_dicBallsToReuse_New.Count == 0)
        {
            return null; // 该木有可以用的闲置球球了
        }
        Ball ball = null;

        int nBallIndex = m_dicBallsToReuse_New.Values.First();
        m_dicBallsToReuse_New.Remove(nBallIndex);

        ball = CBallManager.s_Instance.NewBall( true );

        ProcessAfterReuse( ball, nBallIndex);


        return ball;
    }

    public void ProcessAfterReuse( Ball ball, int nBallIndex )
    {
        if (!LastTimeIsMyBall(ball))
        {
            ball.GenerateMeshShape(GetSkinId(), ResourceManager.s_Instance.GetColorByPlayerId(GetColorId()));
        }

        ball.SetPlayerLevel( _Player.GetLevel() );
        ball.SetPlayerName(GetPlayerName());
        ball.SetIndex(nBallIndex);
        ball.SetAccount(GetAccount());
        ball.SetPlayer(_Player);
        ball.SetPlayerIns(this);
        ball.transform.SetParent(this.transform);
        DoSomeCollisionIgnore_New(ball);
        m_lstBalls.Add(ball);
        DoSomeThingWhenReuse(ball);
        ball.SetDead(false);
        ball.SetActive(true);

        float fCamSize = CCameraManager.s_Instance.GetCurCameraSize();

        ball.ProcessPlayerName(fCamSize);
        ball.ProcessPlayerNameVisible(fCamSize);

    }

    public void DoSomeThingWhenReuse( Ball ball )  
    {
        ball.RebornInit();

        DoSomeCollisionIgnore_New( ball ); // 碰撞器方面的，稍后专门梳理一次

        _Player.UpdateMoveToCenterStatus(ball);

        UpdateSkillsStatusForBall( ball ); 
    }

   

    public void UpdateEffect( Ball ball )
    {

    }

    public bool LastTimeIsMyBall(Ball ball)
    {
        return StringManager.Equal(ball.GetAccount(), GetAccount());
    }

    public void SetOneBallUsed(int nBallIndex)
    {
        /*
        Ball ball = null;
        if (!m_dicBallsToReuse.TryGetValue(nBallIndex, out ball))
        {
            return;
        }
        m_dicBallsToReuse.Remove(nBallIndex);
        */
        m_dicBallsToReuse_New.Remove(nBallIndex);
    }

    public void AddMostBigBallsToShowName(int nBallIndex)
    {
        m_dicMostBigBallsToShowName[nBallIndex] = 1;
    }


    public void ClearMostBigBallsToShowName()
    {
        m_dicMostBigBallsToShowName.Clear();
    }

    ////////////////////////// 以下为技能相关 /////////////////////////////
    // 0 - 无  1 - 前摇  2 - 持续
    public short[] m_arySkillStatus = new short[(int)CSkillSystem.eSkillId.total_num];
    //public short m_nSkillStatus = 0;
    CSkillSystem.eSkillId m_eSkillId = CSkillSystem.eSkillId.r_sneak;

    public CSkillSystem.eSkillId GetSkillId()
    {
        return m_eSkillId;
    }

    public void SetSkillStatus(CSkillSystem.eSkillId eSkillId, short nStatus )
    {
       m_arySkillStatus[(int)eSkillId] = nStatus;
       // m_nSkillStatus = nStatus;
        m_eSkillId = eSkillId;

        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            ball.UpdateSkillStatus(eSkillId, nStatus);
        }
    }

    // 暂定每个玩家只能使用一种技能
	public short GetSkillStatus(CSkillSystem.eSkillId eSkillId)
    {
       // return m_nSkillStatus;
      return m_arySkillStatus[(int)eSkillId];
    }

    public void UpdateSkillsStatusForBall( Ball ball )
    {
        /// 可选技能的
		ball.UpdateSkillStatus( m_eSkillId, m_arySkillStatus[(int)m_eSkillId]);

        /// 扩张技能的
        /// right here
        UpdateStatusKuoZhang_ForBall( ball, m_eKuoZhangStatus, m_fKuoKuoZhangParam1, m_fKuoKuoZhangParam2);
    }

	/// <summary>
	/// / 湮灭
	/// </summary>
	float m_fSkillTimeElapse = 0f; // 暂定一个玩家同时只能使用一个技能
	float m_fSkillQianYaoTime = 0.5f;
	public void SetQianYaoTime( float fQianYaoTime )
	{
		m_fSkillQianYaoTime = fQianYaoTime;
	}

	public void BeginSkill ( CSkillSystem.eSkillId eSkillId, float fSpeedPercent, float fDuration )
	{
		m_eSkillId = eSkillId;
		SetSkillStatus( eSkillId, 1 ); // 技能启动，1表示前摇阶段
		m_fSkillTotalTime = fDuration;

		m_fSkillTimeElapse = 0f;
	}

	public bool IsYanMie()
	{
		return GetSkillStatus (CSkillSystem.eSkillId.y_annihilate) > 0;
	}

	void SkillLoop()
	{
        ///// 扩张
        SkillLoop_KuoZhang();
        ////

        int nCurStatus = GetSkillStatus( m_eSkillId);
		if (nCurStatus == 0)
		{
			return;
		}

		m_fSkillTimeElapse += Time.deltaTime;
		if ( ( nCurStatus == 1 ) && ( m_fSkillTimeElapse >= m_fSkillQianYaoTime ))
		{
			SetSkillStatus(m_eSkillId, 2 ); // 前摇结束，进入正式阶段
		}

		if (m_fSkillTimeElapse >= m_fSkillTotalTime)
		{
			EndSkill();
		}


 
    }

	public void EndSkill()
	{
		SetSkillStatus(m_eSkillId, 0 );

		if (m_eSkillId == CSkillSystem.eSkillId.i_merge) {
			_Player.DoMergeAll_New ();
		}
	}
	/// end 湮灭
       
    //// --------- 潜行
    float m_fSkillTimeElapse_Sneak = 0f;
    float m_fSkillTotalTime = 0;
    float m_fSkillQianYaoTime_Sneak = 0.5f;
    public void BeginSkillSneak( float fSpeedPercent, float fDuration )
    {
        SetSkillStatus( CSkillSystem.eSkillId.r_sneak, 1 ); // 技能启动，1表示前摇阶段
        m_fSkillTotalTime = fDuration;

        m_fSkillTimeElapse_Sneak = 0f;
    }

	public bool IsSneak()
	{
		return GetSkillStatus (CSkillSystem.eSkillId.r_sneak) > 0;
	}

    void SkillLoop_Sneak()
    {
		int nCurStatus = GetSkillStatus( CSkillSystem.eSkillId.r_sneak );
        if (nCurStatus == 0)
        {
            return;
        }

        m_fSkillTimeElapse_Sneak += Time.deltaTime;
        if ( ( nCurStatus == 1 ) && ( m_fSkillTimeElapse_Sneak >= m_fSkillQianYaoTime_Sneak ))
        {
            SetSkillStatus(CSkillSystem.eSkillId.r_sneak, 2 ); // 前摇结束，进入正式阶段
        }

        if (m_fSkillTimeElapse_Sneak >= m_fSkillTotalTime)
        {
            EndSkillSneak();
        }
    }

    public void EndSkillSneak()
    {
        SetSkillStatus(CSkillSystem.eSkillId.r_sneak, 0 );
       
    }


    //// ---------- end 潜行

    /// <summary>
    /// / 扩张
    /// </summary>
    short m_nSKillStatus_KuoZhang = 0; // 0 - 无  1 - 前摇   2 - 持续 
    float m_nSKill_QianYao_KuoZhang = 0; // 前摇时间
    float m_nSKill_ChiXu_KuoZhang = 0; // 持续时间
    float m_fTimeElapse_KuoZhang = 0; // 计时器
    float m_fKuoZhangAngle = 0f;
    float m_fKuoZhangUnfoldScale = 0;
    public void BeginSkill_KuoZhang(float fQianYaoTime, float fChiXuTime, float fUnfoldScale)
    {
        m_nSKill_QianYao_KuoZhang = fQianYaoTime;
        m_nSKill_ChiXu_KuoZhang = fChiXuTime;
        m_fKuoZhangUnfoldScale = fUnfoldScale;
        m_fTimeElapse_KuoZhang = 0;

        UpdateSkillStatus_KuoZhang( 1, fUnfoldScale);
    }

    void SkillLoop_KuoZhang()
    {
        if (m_nSKillStatus_KuoZhang == 0)
        {
            return;
        }

        m_fTimeElapse_KuoZhang += Time.deltaTime; 


        if (m_nSKillStatus_KuoZhang == 1) // 前摇期
        {
            float fTimeLapsePercent = m_fTimeElapse_KuoZhang / m_nSKill_QianYao_KuoZhang;
            m_fKuoZhangAngle = fTimeLapsePercent * 360f;
           
            UpdateSkillStatus_KuoZhang(1, m_fKuoZhangUnfoldScale,  m_fKuoZhangAngle);

            if (m_fTimeElapse_KuoZhang >= m_nSKill_QianYao_KuoZhang)
            {
                UpdateSkillStatus_KuoZhang(2, m_fKuoZhangUnfoldScale);
                m_fTimeElapse_KuoZhang = 0;
            }
        }

        if (m_nSKillStatus_KuoZhang == 2) // 持续期
        {
            if (m_fTimeElapse_KuoZhang >= m_nSKill_ChiXu_KuoZhang)
            {
                UpdateSkillStatus_KuoZhang(0, 0);
            }
        }
    }

    public enum eKuoZhangStatus
    {
        e_none,
        e_begin_qianyao,
        e_update_qianyao,
        e_end_qianYao,
        e_begin_chixu,
        e_end_chixu,
    };

    public void UpdateStatusKuoZhang_ForBall( Ball ball, eKuoZhangStatus eStatus, float fParam1, float fParam2 = 0)
    {
        switch(eStatus)
        {
            case eKuoZhangStatus.e_begin_qianyao:
                {
                    ball.BeginUnfoldQianYao(fParam1);
                }
                break;
            case eKuoZhangStatus.e_update_qianyao:
                {
                    ball.BeginUnfoldQianYao(fParam1);
                    ball.UpdateUnfoldQianYao(fParam2);
                }
                break;
            case eKuoZhangStatus.e_end_qianYao:
                {
                    ball.EndPreUnfold();
                }
                break;
            case eKuoZhangStatus.e_begin_chixu:
                {
                    ball.SetMouthScale(fParam1);
                }
                break;
            case eKuoZhangStatus.e_end_chixu:
                {
                    ball.EndUnfold();
                }
                break;

        } // end switch
    }
    
    public void GetKuoZhangParams( ref short nStatus,ref float fParam1, ref float fParam2)
    {
        nStatus = (short)m_eKuoZhangStatus;
        fParam1 = m_fKuoKuoZhangParam1;
        fParam2 = m_fKuoKuoZhangParam2;
    }

    public void SetKuoZhangParams( short nStatus,  float fParam1,  float fParam2)
    {
        m_eKuoZhangStatus = (eKuoZhangStatus)nStatus;
        m_fKuoKuoZhangParam1 = fParam1;
        m_fKuoKuoZhangParam2 = fParam2;
    }

    // MainPlayer和OtehrClient通吃的流程
    float m_fKuoKuoZhangParam1 = 0;
    float m_fKuoKuoZhangParam2 = 0;
    eKuoZhangStatus m_eKuoZhangStatus = eKuoZhangStatus.e_begin_qianyao;
    public void UpdateSkillStatus_KuoZhang( short nStatus, float fParam1, float fParam2 = 0 )
    {
 
        m_fKuoKuoZhangParam1 = fParam1;
        m_fKuoKuoZhangParam2 = fParam2;
        if (nStatus == 1)
        {
            if (m_nSKillStatus_KuoZhang != 1)
            {
                for (int i = 0; i < m_lstBalls.Count; i++)
                {
                    Ball ball = m_lstBalls[i];
                    //ball.BeginUnfoldQianYao(fParam1);
                    UpdateStatusKuoZhang_ForBall(ball, eKuoZhangStatus.e_begin_qianyao, fParam1, 0);
                }
                m_eKuoZhangStatus = eKuoZhangStatus.e_begin_qianyao;
            }
            else
            {
                for (int i = 0; i < m_lstBalls.Count; i++)
                {
                    Ball ball = m_lstBalls[i];
                    // ball.UpdateUnfoldQianYao(fParam1);
                    UpdateStatusKuoZhang_ForBall(ball, eKuoZhangStatus.e_update_qianyao, fParam1, fParam2);
                }
                m_eKuoZhangStatus = eKuoZhangStatus.e_update_qianyao;
            }
        }

        if (nStatus == 2)
        {
            if (m_nSKillStatus_KuoZhang != 2)
            {
                for (int i = 0; i < m_lstBalls.Count; i++)
                {
                    Ball ball = m_lstBalls[i];
                    //ball.SetMouthScale(fParam1);
                    UpdateStatusKuoZhang_ForBall(ball, eKuoZhangStatus.e_begin_chixu, fParam1);
                }
                m_eKuoZhangStatus = eKuoZhangStatus.e_begin_chixu;
            }
        }

        if (m_nSKillStatus_KuoZhang == 1 && nStatus != 1) // 结束前摇
        {
            for (int i = 0; i < m_lstBalls.Count; i++)
            {
                Ball ball = m_lstBalls[i];
                //ball.EndPreUnfold();
                UpdateStatusKuoZhang_ForBall(ball, eKuoZhangStatus.e_end_qianYao, fParam1);
            }
            m_eKuoZhangStatus = eKuoZhangStatus.e_end_qianYao;
        }

        if (m_nSKillStatus_KuoZhang == 2 && nStatus != 2) // 结束持续
        {
            for (int i = 0; i < m_lstBalls.Count; i++)
            {
                Ball ball = m_lstBalls[i];
                // ball.EndUnfold();
                UpdateStatusKuoZhang_ForBall(ball, eKuoZhangStatus.e_end_chixu, fParam1);
            }
            m_eKuoZhangStatus = eKuoZhangStatus.e_none;
        }

        m_nSKillStatus_KuoZhang = nStatus;
    }
    

    //// end 扩张



    public Dictionary<int, int> m_dicMostBigBallsToShowName = new Dictionary<int, int>();
    public bool IsMostBigBallsToShowName( int nBallIndex )
    {
        int nVal = 0;
        return m_dicMostBigBallsToShowName.TryGetValue(nBallIndex, out nVal);
    }

	public bool CheckIfCanEatThorn()
	{
		if (IsSneak ()) {
			return false;
		}

		return true;
	}

	public bool CheckIfCanEatBean()
	{
		if (IsSneak ()) {
			return false;
		}

			return true;
	}
}
