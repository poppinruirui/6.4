﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System.IO;
using System.Net;
//using OpenWinForm = System.Windows.Forms;

public class MapEditor : MonoBehaviour {

	public static MapEditor s_Instance = null;

    public enum eEatMode
    {
        yaqiu,      // 压球模式
        xiqiu,      // 吸球模式
    };
    static eEatMode m_eEatMode = eEatMode.yaqiu;

    float m_fMinCameraSize = 30f;
    float m_fMaxCameraSize = 120.0f;

    float m_fCameraZoomSpeed = 50.0f;
	float m_fBallSizeAffectCamSize = 1f;
    public Ball m_ReferBall;

    float m_fWorldSizeX = 128f;
	float m_fWorldSizeY = 128f;
    float m_fWorldWidth = 300.0f;
    float m_fWorldHeight = 200.0f;
	float m_fHalfWorldWidth = 0f;
	float m_fHalfWorldHeight = 0f;
    public WorldBorder m_goWorldBorderTop;
    public WorldBorder m_goWorldBorderBottom;
    public WorldBorder m_goWorldBorderLeft;
    public WorldBorder m_goWorldBorderRight;

    List<Vector3> m_lstRebornPos = new List<Vector3>();

    public Vector3 vecTemp = new Vector3();
	public Vector3 vecTempPos = new Vector3();
	public Vector3 vecTempScale = new Vector3();

    public GameObject m_preGrassTile;
	public GameObject m_preGridLine;

	public GameObject m_goGridLineContainer;
	public GameObject m_goGrassContainer;
    public GameObject m_goPolygonContainer;
    public GameObject m_goBeanSprayContainer;
    public GameObject m_goRebornSpotContainer;
    public GameObject m_goColorThornContainer;

    const int c_GridNum = 1024;

	
    Dictionary<string, float> m_dicDelProtectTime = new Dictionary<string, float>();
    Dictionary<int, Polygon> m_dicGrass = new Dictionary<int, Polygon>();

    static Vector3 s_vecTempPos = new Vector3();


    float m_fGrassTileWidth = 2.46f;
	float m_fGrassTileHeight = 2.46f;
	float m_fGrassHalfTileWidth = 0.0f;
	float m_fGrassHalfTileHeight = 2.46f;

    MapObj m_CurSelectedObj = null;
    MapObj m_CurMovingObj = null;
    Vector3 m_vecLastMousePos = new Vector3();

    //// !! ----  UI

    public GameObject Screen_Cursor;
    public GameObject UI_MiniMap;
        
    public GameObject[] m_arySubPanel = new GameObject[24];
    public Dropdown dropdownItem;

	public GameObject _panelLogin;

	// Music & Audio
	public AudioSource _musicMain;

	// Test
	public InputField _inputTest;
	public Text _txtDebugInfo;
	public Button _btnTest;
	public Dictionary<int, string> _dicTest = new Dictionary<int, string> ();

    // Common

    /// <summary>
    /// / 射球参数相关
    /// </summary>
    public InputField _inputA;
    public InputField _inputB;
    public InputField _inputX;

    public InputField _inputTimeBeforeShowDeadUI;
    public InputField _inputTimeTimeOfOneRound;
    public InputField _inputBaseVolumeDecreasedPercentWhenDead;

    public InputField _inputClassCircleRadius;
    public InputField _inputClassThreshold;

    public InputField _inputWorldSizeX;
	public InputField _inputWorldSizeY;
    public InputField _inputfieldCamSizeMax;
    public InputField _inputfieldCamSizeMin;


    public InputField _inputCamShangFuXiShu;

    /// <summary>
    /// / 镜头抬升相关的参数输入框（目前确定使用的就是这几个参数，其余的暂时废弃）
    /// </summary>
    public InputField _inputCamRaiseSpeed;
    public InputField _inputCamShangFuAccelerate;
    public InputField _inputCamChangeDelaySpit;
    public InputField _inputCamChangeDelayExplode;
    public InputField _inputfieldCamRespondSpeed;
    public InputField _inputCamTimeBeforeDown;
    /// / end 


    public InputField _inputCamDownSpeed;

    public InputField _inputfieldCamSizeXiShu;
    public InputField _inputfieldCamZoomSpeed;
    public InputField _inputBornProtectTime;
    public InputField _inputfieldBallSizeAffectCamSize;
    public InputField _inputfieldBeanNumPerRange;
    public InputField _inputfieldBallBaseSpeed;
    public InputField _inputfieldMaxSpeed;
    public InputField _inputfieldSpeedRadiusKaiFangFenMu;
    public InputField _inputfieldBornMinDiameter;

    public InputField _inputfieldThornNumPerRange;
    public InputField _inputfieldGenerateNewBeanTimeInterval;
    public InputField _inputfieldGenerateNewThornTimeInterval;
    public InputField _inputfieldBeanSize;
    public InputField _inputfieldThornSize;
    public InputField _inputfieldThornContributeToBallSize;
	public InputField _inputfieldSpitSporeCostMP;
	public InputField _inputfieldSpitSporeThornId;
    public InputField _inputfieldSpitSporeRunDistance;
	public InputField _inputfieldSpitSporeBeanType;
    public InputField _inputfieldSpitRunTime;
	public InputField _inputfieldSpitBallRunTime;
    public InputField _inputfieldSpitBallRunDistanceMultiple;
	public InputField _inputfieldSpitBallCostMP;
	public InputField _inputfieldSpitBallStayTime;
    public InputField _inputfieldSpitBallSpeed;
    public InputField _inputfieldAutoAttenuate;
	public InputField _inputfieldThornAttenuate;
    public InputField _inputfieldAutoAttenuateTimeInterval;
    public InputField _inputfieldForceSpitTotalTime;
    public InputField _inputfieldShellShrinkTotalTime;
	public InputField _inputfieldSplitShellTotalTime;
	public InputField _inputfieldExplodeShellTotalTime;
    public InputField _inputfieldSpitBallTimeInterval;
    public InputField _inputfieldSpitSporeTimeInterval;
    public InputField _inputfieldMinDiameterToEatThorn;
    public InputField _inputfieldExplodeRunDistanceMultiple; // right here
	public InputField _inputfieldExplodeStayTime;
	public InputField _inputfieldExplodePercent;
	public InputField _inputfieldExplodeExpectNum;
	public InputField _inputfieldExplodeRunTime;
    public InputField _inputfieldMaxBallNumPerPlayer;
    public InputField _inputfieldSplitNum;
    public InputField _inputfieldSplitRunTime;
    public InputField _inputfieldSplitMaxDistance;
    public InputField _inputfieldSplitTimeInterval;
	public InputField _inputfieldSplitCostMP;
	public InputField _inputfieldSplitStayTime;

	// 一些编辑模块独立出来了
	public CMonsterEditor _MonsterEditor;
	public CClassEditor _ClassEditor;

    // EatStyle
    public Dropdown _dropdownEatMode;
	public InputField _inputUnfoldCostMP;
    public InputField _inputMainTriggerPreUnfoldTime;
    public InputField _inputUnfoldSale;
    public InputField _inputMainTriggerUnfoldTime;
    public InputField _inputUnfoldTimeInterval;
    public InputField _inputYaQiuTiaoJian;
    public InputField _inputYaQiuBaiFenBi;
    public InputField _inputYaQiuBaiFenBi_Self;
    public InputField _inputXiQiuSuDu;


    // Polygon
    public Slider _sliderScaleX;
    public Slider _sliderScaleY;
    public Slider _sliderRotation;
    public Text _txtPolygonTitle;
    public Toggle _toogleIsGrass;
    public Toggle _toogleIsGrassSeed;
    public InputField _inputfieldSeedGrassLifeTime;

	public InputField _inputPolygonScaleX;
	public InputField _inputPolygonScaleY;
	public InputField _inputPolygonRotation;
	public Toggle _toogleIsHardObstacle;
	public ComoList _PolygonList;



    // Bean Spray
    public Slider _sliderDensity;
    public Slider _sliderMaxDis;
    public Slider _sliderBeanLifeTime;
    public Text _txtDensity;
    public Text _txtMaxDis;
    public Text _txtBeanLifeTime;
    public Toggle _toogleShowRealtimeBeanSpray;
    
	public InputField _inputfieldDensity;
	public InputField _inputfieldMeatDensity;
	public InputField _inputfieldMaxDis;
	public InputField _inputfieldMinDis;
	public InputField _inputfieldBeanLifeTime;
	public InputField _inputfieldMeat2BeanSprayTime;
	public InputField _inputfieldSpraySize;
	public InputField _inputfieldDrawSpeed;
	public InputField _inputfieldDrawArea;
	public InputField _inputfieldPreDrawTime;
	public InputField _inputfieldDrawTime;
	public InputField _inputfieldDrawInterval;

	public InputField _inputfieldThornChance;
	public InputField _inputfieldSprayInterval;

    public Text _txtThornChace;
    int m_nThornChance = 0;

    // Color Thorn
    public Dropdown dropdownColorThorn;
    public Image m_imgColorThorn;
    public InputField m_inputfieldColorThornThornSize;
    public InputField m_inputfieldColorThornBallSize;
	public InputField m_inputfieldColorThornRebornTime;
    public Toggle m_toggleAddThorn;
    public Toggle m_toggleRemoveThorn;
    public static Dictionary<int, Thorn.ColorThornParam> m_dicColorThornParam = new Dictionary<int, Thorn.ColorThornParam>();
    public InputField m_inputfieldLababaAreaThreshold; // 队伍总体积达到多少才能拉粑粑
    public InputField m_inputfieldLababaRunTime;          // 拉粑粑的喷射距离
    public InputField m_inputfieldLababaRunDistance;          // 拉粑粑的喷射时间
    public InputField m_inputfieldLababaColdDown;          // 拉粑粑的ColdDown时间
    public InputField m_inputfieldLababaAreaCostPercent;          // 拉一次粑粑损失百分之多少的体积（队伍中每一个球皆损失）
    public InputField m_inputfieldLababaLiveTime;          // 粑粑的存活时间

    // DaTaoSha
    public Dropdown _dropdownFoodType;
	public InputField _inputName;
	public InputField _inputDesc;
	public InputField _inputValue;
	public InputField _inputNum;
	public InputField _inputTotalTime;
	public InputField _inputMinScale;
	public Slider _sliderCircleSizeRange;
	public struct sBeiShuDouZiConfig
	{
		public int id;
		public float value;
		public int num;
		public string name;
		public string desc;
	};

	sBeiShuDouZiConfig m_CurEditingBeiShuDouZiConfig;
	List<sBeiShuDouZiConfig> m_lstBeiShuDouZi = new List<sBeiShuDouZiConfig>();
	
    //// !! ---- end UI

    ////


	public float m_fYaQiuTiaoJian = 1.0f;
	public float m_fYaQiuBaiFenBi = 0.5f;
    public float m_fYaQiuBaiFenBi_Self = 0.5f;
    public float m_fXiQiuSuDu = 1.0f;


    public enum eMapEditorOp
    {
        common,             // 整体操作
        eat_mode,           // 吃球模式
        polygon,            // 多边形
        bean_spray,         // 豆子喷泉
        color_thorn,        // 彩色刺
		dataosha,           // 大逃杀
        self_rotate_obj,    // 自旋转物体
        lever,              // 杠杆
        chopper_tunnel,     // 铡刀隧道

    };

    eMapEditorOp m_eOp = eMapEditorOp.common;

	

	// Use this for initialization
	void Start () {

        CAudioManager.s_Instance.StopMainBg(CAudioManager.eMainBg.login);
        CAudioManager.s_Instance.PlayMainBg(CAudioManager.eMainBg.arena);

        if (AccountManager.m_eSceneMode == AccountManager.eSceneMode.None) {
			return;
		}

		if (AccountManager.m_eSceneMode == AccountManager.eSceneMode.MapEditor) {
						Camera.main.backgroundColor = Color.black;
		}

		_PolygonList.delegateMethodOnSelect = new ComoList.DelegateMethod_OnSelected ( this.OnSelected );

		StartCoroutine( LoadPolygonList () );

		if (m_szCurRoom == "") {
            return;
		}

          LoadMap( m_szCurRoom ); 

        if ( AccountManager.m_eSceneMode != AccountManager.eSceneMode.Game )
        {
            Screen_Cursor.SetActive( false );
          //  UI_MiniMap.SetActive( false );
        }

        SetCamSize(Camera.main.orthographicSize);
       //  _inputfieldCamSizeMax.text = m_fMaxCameraSize.ToString();
    }

    void Awake()
	{
		s_Instance = this;

		m_fGrassHalfTileWidth = m_fGrassTileWidth / 2.0f;
		m_fGrassHalfTileHeight = m_fGrassTileHeight / 2.0f;

        m_lstRebornPos.Clear();


    }

	bool m_bMapInitCompleted = false;
	public bool IsMapInitCompleted()
	{
		return m_bMapInitCompleted;
	}

	// Update is called once per frame
	void Update () {
        // poppin test for xingnengceshi
        return;

        ProcessInput();

    }

    public void Init()
    {
        InitGrid();
        InitDropDownItems();
        //InitDropDownColorThorn();
        //InitDropDownEatMode();

    }

	public void DrawBgLine()
	{
        return;

		int nNumX = (int)(m_fWorldSizeX / Main.s_Instance.m_fGridLineGap);
		int nNumY = (int)(m_fWorldSizeY / Main.s_Instance.m_fGridLineGap);
		for (int i = 0; i < nNumX; i++) { // 画竖线
			GameObject goLine = GameObject.Instantiate (Main.s_Instance.m_preGridLine);
			goLine.transform.parent = m_goGridLineContainer.transform;
			LineRenderer line = goLine.GetComponent<LineRenderer> ();
			vecTempPos.x = -m_fHalfWorldWidth + i * Main.s_Instance.m_fGridLineGap;
			vecTempPos.y = m_fHalfWorldHeight;
			vecTempPos.z = 0;
			line.SetPosition (0, vecTempPos);
			vecTempPos.x = -m_fHalfWorldWidth + i * Main.s_Instance.m_fGridLineGap;
			vecTempPos.y = -m_fHalfWorldHeight;
			vecTempPos.z = 0;
			line.SetPosition (1, vecTempPos);
		} // end i

		for (int i = 0; i < nNumY; i++) { // 画横线
			GameObject goLine = GameObject.Instantiate (Main.s_Instance.m_preGridLine);
			goLine.transform.parent = m_goGridLineContainer.transform;
			LineRenderer line = goLine.GetComponent<LineRenderer> ();
			vecTempPos.x = -m_fHalfWorldWidth;
			vecTempPos.y = -m_fHalfWorldHeight + i * Main.s_Instance.m_fGridLineGap;
			vecTempPos.z = 0;
			line.SetPosition (0, vecTempPos);
			vecTempPos.x = m_fHalfWorldWidth;
			vecTempPos.y = -m_fHalfWorldHeight + i * Main.s_Instance.m_fGridLineGap;
			vecTempPos.z = 0;
			line.SetPosition (1, vecTempPos);
		} // end i
	}

    public static void SetEatMode( eEatMode val )
    {
        m_eEatMode = val;
    }

    public static eEatMode GetEatMode()
    {
        return m_eEatMode;
    }

    void InitDropDownEatMode()
    {
        List<string> showNames = new List<string>();

        showNames.Add( "压球(含猥琐扩张)");
        showNames.Add( "吸球 ");
      
        UpdateDropdownView( _dropdownEatMode, showNames);
    }



    void InitDropDownItems()
    {
        List<string> showNames = new List<string>();

        showNames.Add( "基础");
		showNames.Add( "野怪");
		showNames.Add( "战场纵深与胜负判定");
		showNames.Add( "Buff系统");
		showNames.Add( "成长系统");
		showNames.Add( "PVE");
		showNames.Add( "道具");
        showNames.Add("技能");
        showNames.Add("表情系统");

        UpdateDropdownView( dropdownItem, showNames);
    }

	void InitDropDownBeiShuDouZi()
	{
		List<string> showNames = new List<string>();
		for (int i = 0; i < m_lstBeiShuDouZi.Count; i++) {
			showNames.Add ( m_lstBeiShuDouZi[i].name );
		}
		UpdateDropdownView(_dropdownFoodType, showNames);
	}

    void InitDropDownColorThorn()
    {
        List<string> showNames = new List<string>();
        showNames.Add("白");
        showNames.Add("红");
        showNames.Add("橙");
        showNames.Add("黄");
        showNames.Add("绿");
        showNames.Add("青");
        showNames.Add("蓝");
        showNames.Add("紫");
        UpdateDropdownView(dropdownColorThorn, showNames);
        UpdateThornColor(0);
    }

    void UpdateDropdownView( Dropdown dropdownItem,  List<string> showNames)
    {
        dropdownItem.options.Clear();
        Dropdown.OptionData tempData;
        for (int i = 0; i < showNames.Count; i++)
        {
            tempData = new Dropdown.OptionData();
            tempData.text = showNames[i];
            dropdownItem.options.Add(tempData);
        }
        dropdownItem.captionText.text = showNames[0];
    }

    public static Thorn.ColorThornParam GetColorThornParamByIdx(int nColorIdx)
    {
        Thorn.ColorThornParam param;
        if (!m_dicColorThornParam.TryGetValue(nColorIdx, out param))
        {
            param = new Thorn.ColorThornParam();
            param.ball_size = 0.0f;
            param.thorn_size = 0.0f;
			param.reborn_time = 60f;
            m_dicColorThornParam[nColorIdx] = param;
        }
        return param;
    }

    void UpdateThornColor( int nColorIdx )
    {
        m_nCurSelectedColorThornIdx = nColorIdx;
           Color color = Thorn.GetColorByIdx(nColorIdx);
        m_imgColorThorn.color = color;
        Thorn.ColorThornParam param;
        param = GetColorThornParamByIdx(nColorIdx);
        m_inputfieldColorThornThornSize.text = param.thorn_size.ToString("f5");
        m_inputfieldColorThornBallSize.text = param.ball_size.ToString("f5");
		m_inputfieldColorThornRebornTime.text = param.reborn_time.ToString("f5");
    }

    int m_nCurSelectedColorThornIdx = 0;
    public void OnDropDownValueChanged_ThornColor()
    {
        int nColorIdx = dropdownColorThorn.value;
        UpdateThornColor(nColorIdx);
    }

    public void OnInputFieldContentChanged_ThornSize()
    {
        float fThornSize = 0.0f;
        if ( !float.TryParse( m_inputfieldColorThornThornSize.text, out fThornSize))
        {
            return;
        }

        Thorn.ColorThornParam param;
        param = GetColorThornParamByIdx(m_nCurSelectedColorThornIdx);
        param.thorn_size = fThornSize;
        m_dicColorThornParam[m_nCurSelectedColorThornIdx] = param;
    }

    /// <summary>
    /// / !---- 拉粑粑相关
    /// </summary>
    public void OnInputFieldContentChanged_LababaAreaThreshold()
    {
        float val = 0f;
        if (!float.TryParse( m_inputfieldLababaAreaThreshold .text, out val))
        {
            return;
        }

        Main.s_Instance.m_fLababaAreaThreshold = val;
    }

    public void OnInputFieldContentChanged_LababaRunDistance()
    {
        float val = 0f;
        if (!float.TryParse(m_inputfieldLababaRunDistance .text, out val))
        {
            return;
        }

        Main.s_Instance.m_fLaBabaRunDistance = val;
    }

    public void OnInputFieldContentChanged_LababaRunTime()
    {
        float val = 0f;
        if (!float.TryParse(m_inputfieldLababaRunTime.text, out val))
        {
            return;
        }

        Main.s_Instance.m_fLaBabaRunTime = val;
    }

    public void OnInputFieldContentChanged_LababaLiveTime()
    {
        float val = 0f;
        if (!float.TryParse(m_inputfieldLababaLiveTime.text, out val))
        {
            return;
        }

        Main.s_Instance.m_fLababaLiveTime = val;
    }

    public void OnInputFieldContentChanged_LababaAreaCostPercent()
    {
        float val = 0f;
        if (!float.TryParse(m_inputfieldLababaAreaCostPercent.text, out val))
        {
            return;
        }

        Main.s_Instance.m_fLababaSizeCostPercent = val;
    }

    public void OnInputFieldContentChanged_LababaColddown()
    {
        float val = 0f;
        if (!float.TryParse(m_inputfieldLababaColdDown.text, out val))
        {
            return;
        }

        Main.s_Instance.m_fLababaColdDown = val;
    }
    /// / !---- end 拉粑粑相关

    public void OnInputFieldContentChanged_BallSize()
    {
        float fBallSize = 0.0f;
        if (!float.TryParse(m_inputfieldColorThornBallSize.text, out fBallSize))
        {
            return;
        }

        Thorn.ColorThornParam param;
        param = GetColorThornParamByIdx(m_nCurSelectedColorThornIdx);
        param.ball_size = fBallSize;
        m_dicColorThornParam[m_nCurSelectedColorThornIdx] = param;
    }

	public void OnInputFieldContentChanged_ColorThornRebornTime()
	{
		float fRebornTime = 0.0f;
		if (!float.TryParse(m_inputfieldColorThornRebornTime.text, out fRebornTime))
		{
			return;
		}

		Thorn.ColorThornParam param;
		param = GetColorThornParamByIdx(m_nCurSelectedColorThornIdx);
		param.reborn_time = fRebornTime;
		m_dicColorThornParam[m_nCurSelectedColorThornIdx] = param;
	}

    public void OnToggleValueChanged_AddThorn()
    {
        if (m_toggleAddThorn.isOn)
        {
            m_toggleRemoveThorn.isOn = false;
        }

    }

    public void OnToggleValueChanged_RemoveThorn()
    {
        if (m_toggleRemoveThorn.isOn)
        {
            m_toggleAddThorn.isOn = false;
        }
   
    }

    public void RemoveCurSelectedThorn()
    {
		/*
        if ( m_eOp != eMapEditorOp.color_thorn )
        {
            return;
        }

        if ( m_CurSelectedObj == null )
        {
            return;
        }

        Thorn thorn = (Thorn)m_CurSelectedObj;
        if (thorn == null)
        {
            return;
        }

        thorn.DestroyMe();
		*/
    }

    public void OnDropDownValueChanged()
    {
        SwitchOp( (eMapEditorOp)dropdownItem.value );
    }

    void SwitchOp(eMapEditorOp op )
    {
        m_eOp = op;


        for ( int i = 0; i < m_arySubPanel.Length; i++ )
        {
            GameObject panel = m_arySubPanel[i];
            if ( panel == null )
            {
                continue;
            }
            panel.SetActive( false );
        }

        int nIndex = (int)m_eOp;
        if (nIndex < m_arySubPanel.Length)
        {
            GameObject cur_panel = m_arySubPanel[nIndex];
            if (cur_panel)
            {
                cur_panel.SetActive(true);
            }
        }
    }


	void Enter_Common_SubPanel()
	{
		
	}

    public void InitGrid()
	{
		return;

		for (int i = -c_GridNum; i < c_GridNum; i++) {
				GameObject goLine = GameObject.Instantiate (m_preGridLine);
				goLine.transform.parent = m_goGridLineContainer.transform;
				LineRenderer lr = goLine.GetComponent<LineRenderer>();
            lr.startColor = Color.gray;
            lr.endColor = Color.gray;
            vecTempPos.x = m_fGrassTileWidth * (i + 0.5f);
				vecTempPos.y = -10000.0f;
				lr.SetPosition ( 0, vecTempPos );
				vecTempPos.y = 10000.0f;
				lr.SetPosition ( 1, vecTempPos );
		}

		for (int i = -c_GridNum; i < c_GridNum; i++) {
			GameObject goLine = GameObject.Instantiate (m_preGridLine);
			goLine.transform.parent = m_goGridLineContainer.transform;
			LineRenderer lr = goLine.GetComponent<LineRenderer>();
            lr.startColor = Color.gray;
            lr.endColor = Color.gray;
            vecTempPos.y = m_fGrassTileHeight * (i + 0.5f);
			vecTempPos.x = -10000.0f;
			lr.SetPosition ( 0, vecTempPos );
			vecTempPos.x = 10000.0f;
			lr.SetPosition ( 1, vecTempPos );
		}
	}

    void GetMousePosition( ref int nX, ref int nY )
    {
        vecTempPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        float x = 0.0f, y = 0.0f;
        if (vecTempPos.x > 0.0f)
        {
            x = vecTempPos.x + m_fGrassHalfTileWidth;
        }
        else if (vecTempPos.x < 0.0f)
        {
            x = vecTempPos.x - m_fGrassHalfTileWidth;
        }

        if (vecTempPos.y > 0.0f)
        {
            y = vecTempPos.y + m_fGrassHalfTileHeight;
        }
        else if (vecTempPos.y < 0.0f)
        {
            y = vecTempPos.y - m_fGrassHalfTileHeight;
        }

        nX = (int)(x / m_fGrassTileWidth);
        nY = (int)(y / m_fGrassTileHeight);
    }

    bool CheckIfCanMoveObj()
    {
        /*
        if ( m_eOp == eMapEditorOp.color_thorn && ( m_toggleAddThorn.isOn || m_toggleRemoveThorn.isOn ) )
        {
            return false;
        }
        */
        return true;
    }
    bool m_bFirstMouseLefDown = true;
    void ProcessInput()
	{
        int nX = 0, nY = 0;


		if (Main.s_Instance && Main.s_Instance.m_nObservor == 1) {
			ProcessMouseWheel ();

			ProcessDragCamera ();
		}

        if ( AccountManager.m_eSceneMode != AccountManager.eSceneMode.MapEditor )
        {
            return;
        }

		if (IsPointerOverUI ()) {
			return;
		}




        GetMousePosition(ref nX, ref nY);
        

        if (Input.GetMouseButtonDown (0)) {
            ProcessColorThorn();

        } // end if (Input.GetMouseButtonDown (0))

        if (Input.GetMouseButton(0))
        {
            vecTempPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (m_bFirstMouseLefDown)
            {
                m_vecLastMousePos = vecTempPos;
                m_bFirstMouseLefDown = false;
            }
            else
            {
                if (m_CurMovingObj)
                {
                    Vector3 vecDelta = vecTempPos - m_vecLastMousePos;
                    m_vecLastMousePos = vecTempPos;
                    vecTempPos = m_CurMovingObj.transform.position;
                    vecTempPos.x += vecDelta.x;
                    vecTempPos.y += vecDelta.y;
                    m_CurMovingObj.transform.position = vecTempPos;

                }
            }
        }
        else
        {
            m_bFirstMouseLefDown = true;
        }

        if ( Input.GetMouseButtonUp(0) )
        {
            m_CurMovingObj = null;
        }

        if (Input.GetKey( KeyCode.Escape )  )
        {
            m_eOp = eMapEditorOp.common;
            PickPolygon( null );
        }

    }

    void ProcessColorThorn()
    {
		return;

        if ( m_eOp != eMapEditorOp.color_thorn )
        {
            return;
        }

        if ( m_toggleAddThorn.isOn ) // 添加一个彩色刺
        {
            AddOneThorn(Camera.main.ScreenToWorldPoint(Input.mousePosition), m_nCurSelectedColorThornIdx, true);
        }
        else // 删除一个彩色刺
        {
            
        }
    }

	void AddOneThorn( Vector3 pos, int nColorIdx, bool bColorThorn = false )
    {
        GameObject goThorn = null;
        goThorn = GameObject.Instantiate((GameObject)Resources.Load("hedge"));
        goThorn.transform.parent = m_goColorThornContainer.transform;
        goThorn.transform.position = pos;
        Thorn thorn = goThorn.GetComponent<Thorn>();
        thorn.SetColorIdx(nColorIdx);
        thorn.m_eObjType = MapObj.eMapObjType.thorn;
		thorn.SetIsColorThorn ( bColorThorn );
    }

    
    bool CheckIfDelProtect( int nX, int nY )
    {
        string key = nX + "," + nY;
        float val = 0.0f;
        if ( m_dicDelProtectTime.TryGetValue(key, out val) )
        {
            if ( Time.time - val > 0.5f)
            {
                m_dicDelProtectTime.Remove(key);
                return false;
            }
            else
            {
                return true;
            }
        }

        return false; 
    }



	void ProcessMouseWheel()
	{
		if (Input.GetAxis ("Mouse ScrollWheel") < 0) {
            //Camera.main.orthographicSize += 1.0f;//Time.fixedDeltaTime * 1.0f;
            SetCamSize(Camera.main.orthographicSize + 2.0f);
        } else if (Input.GetAxis ("Mouse ScrollWheel") >0) {
            //Camera.main.orthographicSize -= 1.0f;//Time.fixedDeltaTime * 1.0f;
            SetCamSize(Camera.main.orthographicSize - 2.0f);
        }
	}

    void SetCamSize( float fSize )
    {
        if (fSize < 5.0f)
        {
            fSize = 5.0f;
        }
        Camera.main.orthographicSize = fSize;
        //_textCamSizeCur.text = Camera.main.orthographicSize.ToString();
    }

	bool m_bFirstMouseRightDown = true;
	void ProcessDragCamera ()
	{
		if (Input.GetMouseButton(1)) {
			vecTempPos = Camera.main.ScreenToWorldPoint ( Input.mousePosition );
			if (m_bFirstMouseRightDown == true) {
				m_vecLastMousePos = vecTempPos;
				m_bFirstMouseRightDown = false;
				return;
			}
			Vector3 vecDelta = vecTempPos - m_vecLastMousePos;
			Camera.main.transform.position -= vecDelta / 2.0f;
			m_vecLastMousePos = vecTempPos;
            vecTempPos = Camera.main.transform.position;
            vecTempPos.z = -14;
            Camera.main.transform.position = vecTempPos;

        } else {
			m_bFirstMouseRightDown = true;
		}
	}


	// 判断当前是否点击在了UI上
	public bool IsPointerOverUI()
	{

		PointerEventData eventData = new PointerEventData(UnityEngine.EventSystems.EventSystem.current);
		eventData.pressPosition = Input.mousePosition;
		eventData.position = Input.mousePosition;
	
		List<RaycastResult> list = new List<RaycastResult>();
		UnityEngine.EventSystems.EventSystem.current.RaycastAll(eventData, list);
	
		return list.Count > 0;

	}

    public void SaveEatMode( XmlDocument xmlDoc, XmlNode nodeEatMode )
    {
        CreateNode(xmlDoc, nodeEatMode, "MainTriggerPreUnfoldTime", Main.s_Instance.m_fMainTriggerPreUnfoldTime.ToString( "f5" ) );
        CreateNode(xmlDoc, nodeEatMode, "MainTriggerUnfoldTime", Main.s_Instance.m_fMainTriggerUnfoldTime.ToString( "f5" ) );
        CreateNode(xmlDoc, nodeEatMode, "UnfoldSale", Main.s_Instance.m_fUnfoldSale.ToString( "f5" ) );
        CreateNode(xmlDoc, nodeEatMode, "UnfoldTimeInterval", Main.s_Instance.m_fUnfoldTimeInterval.ToString( "f5" ) );
		CreateNode(xmlDoc, nodeEatMode, "UnfoldCostMP", Main.s_Instance.m_fUnfoldCostMP.ToString( "f5" ) );
        CreateNode(xmlDoc, nodeEatMode, "YaQiuTiaoJian", m_fYaQiuTiaoJian.ToString( "f5" ) );
        CreateNode(xmlDoc, nodeEatMode, "YaQiuBaiFenBi", m_fYaQiuBaiFenBi.ToString( "f5" ) );
        CreateNode(xmlDoc, nodeEatMode, "YaQiuBaiFenBi_Self", m_fYaQiuBaiFenBi_Self.ToString("f5"));
        CreateNode(xmlDoc, nodeEatMode, "XiQiuSuDu", m_fXiQiuSuDu.ToString( "f5" ) );

        CreateNode(xmlDoc, nodeEatMode, "EatMode", ((int)m_eEatMode).ToString( "f5" ) );
    }


	public void SaveCommon2(XmlDocument xmlDoc, XmlNode nodeCommon)
	{
		string szContent = "";
		foreach (Transform child in m_goRebornSpotContainer.transform) {
			szContent += child.transform.position.x + "," + child.transform.position.y + "|";
		}
		CreateNode(xmlDoc, nodeCommon, "RebornSpot", szContent );
	}

    public void SaveCommon(XmlDocument xmlDoc, XmlNode nodeCommon)
    {
        CreateNode(xmlDoc, nodeCommon, "EjectParamA", m_fEjectParamA.ToString());
        CreateNode(xmlDoc, nodeCommon, "EjectParamB", m_fEjectParamB.ToString());
        CreateNode(xmlDoc, nodeCommon, "EjectParamX", m_fEjectParamX.ToString());

        CreateNode(xmlDoc, nodeCommon, "PlayerNumToStartGame", m_nPlayerNumToStartGame.ToString());
        CreateNode(xmlDoc, nodeCommon, "WorldSizeX", m_fWorldSizeX.ToString( "f5" ) );
		CreateNode(xmlDoc, nodeCommon, "WorldSizeY", m_fWorldSizeY.ToString( "f5" ) );

		CreateNode(xmlDoc, nodeCommon, "MaxCameraSize", m_fMaxCameraSize.ToString( "f5" ) );
        CreateNode(xmlDoc, nodeCommon, "MinCameraSize", m_fMinCameraSize.ToString("f5"));
        CreateNode(xmlDoc, nodeCommon, "CamSizeXiShu", Main.s_Instance.m_fCamSizeXiShu.ToString("f5"));
        CreateNode(xmlDoc, nodeCommon, "CamZoomSpeed", m_fCameraZoomSpeed.ToString( "f5" ) );

        CreateNode(xmlDoc, nodeCommon, "CamShangFuXiShu", Main.s_Instance.m_fShangFuXiShu.ToString("f5"));
        CreateNode(xmlDoc, nodeCommon, "CamDownSpeed", Main.s_Instance.m_fDownSpeed.ToString("f5"));

        //// 镜头抬升相关
        CreateNode(xmlDoc, nodeCommon, "CamRaiseAccelerate", Main.s_Instance.m_fCamRaiseAccelerate.ToString("f5"));               // 上浮期间加速度
        CreateNode(xmlDoc, nodeCommon, "CamRaiseInitSpeedXiShu", Main.s_Instance.m_fRaiseSpeed.ToString("f5"));           // 抬升初速度
        CreateNode(xmlDoc, nodeCommon, "CamChangeDelaySpit", Main.s_Instance.m_fCamChangeDelaySpit.ToString("f5"));       // 抬升前滞留时间(分球)
        CreateNode(xmlDoc, nodeCommon, "CamChangeDelayExplode", Main.s_Instance.m_fCamChangeDelayExplode.ToString("f5"));  // 抬升前滞留时间(炸球)
        CreateNode(xmlDoc, nodeCommon, "CamRespondSpeed", Main.s_Instance.m_fCamRespondSpeed.ToString("f5"));
        CreateNode(xmlDoc, nodeCommon, "TimeBeforeCamDown", Main.s_Instance.m_fTimeBeforeCamDown.ToString("f5"));
        
        //// end 镜头抬升相关



        CreateNode(xmlDoc, nodeCommon, "BornProtectTime", Main.s_Instance.m_fBornProtectTime.ToString("f5"));
        CreateNode(xmlDoc, nodeCommon, "BallSizeAffectCamSize", m_fBallSizeAffectCamSize.ToString( "f5" ) );
        CreateNode(xmlDoc, nodeCommon, "BeanNumPerRange", Main.s_Instance.m_fBeanNumPerRange.ToString( "f5" ) );
        CreateNode(xmlDoc, nodeCommon, "BallBaseSpeed", Main.s_Instance.m_fBallBaseSpeed.ToString( "f5" ) );
        CreateNode(xmlDoc, nodeCommon, "MaxSpeed", Main.s_Instance.m_fMaxPlayerSpeed.ToString("f5"));
        CreateNode(xmlDoc, nodeCommon, "BallSpeedRadiusKaiFangFenMu", Main.s_Instance.m_fBallSpeedRadiusKaiFangFenMu.ToString("f5"));
        //CreateNode(xmlDoc, nodeCommon, "BornMinDiameter", Main.s_Instance.m_fBornMinDiameter.ToString( "f5" ) );
        CreateNode(xmlDoc, nodeCommon, "BornMinDiameter", Main.s_Instance.m_fBornMinTiJi.ToString("f5"));
        CreateNode(xmlDoc, nodeCommon, "ThornNumPerRange", Main.s_Instance.m_fThornNumPerRange.ToString( "f5" ) );
        CreateNode(xmlDoc, nodeCommon, "GenerateNewBeanTimeInterval", Main.s_Instance.m_fGenerateNewBeanTimeInterval.ToString( "f5" ) );
        CreateNode(xmlDoc, nodeCommon, "GenerateNewThornTimeInterval", Main.s_Instance.m_fGenerateNewThornTimeInterval.ToString( "f5" ) );
        CreateNode(xmlDoc, nodeCommon, "BeanSize", Main.s_Instance.m_fBeanSize.ToString( "f5" ) );
        CreateNode(xmlDoc, nodeCommon, "ThornSize", Main.s_Instance.m_fThornSize.ToString( "f5" ) );
        CreateNode(xmlDoc, nodeCommon, "ThornContributeToBallSize", Main.s_Instance.m_fThornContributeToBallSize.ToString( "f5" ) );
        CreateNode(xmlDoc, nodeCommon, "SpitSporeRunDistance", Main.s_Instance.m_fSpitSporeRunDistance.ToString( "f5" ) );
		CreateNode(xmlDoc, nodeCommon, "SpitSporeBeanType", Main.s_Instance.m_szSpitSporeBeanType);
		CreateNode(xmlDoc, nodeCommon, "SpitRunTime", Main.s_Instance.m_fSpitRunTime.ToString( "f5" ) );
		CreateNode(xmlDoc, nodeCommon, "SpitBallRunTime", Main.s_Instance.m_fSpitBallRunTime.ToString( "f5" ) );
        CreateNode(xmlDoc, nodeCommon, "SpitBallRunDistanceMultiple", Main.s_Instance.m_fSpitBallRunDistanceMultiple.ToString( "f5" ) );
		CreateNode(xmlDoc, nodeCommon, "SpitBallCostMP", Main.s_Instance.m_fSpitBallCostMP.ToString( "f5" ) );
		CreateNode(xmlDoc, nodeCommon, "SpitBallStayTime", Main.s_Instance.m_fSpitBallStayTime.ToString( "f1" ) );
        CreateNode(xmlDoc, nodeCommon, "SpitBallSpeed", Main.s_Instance.m_fSpitBallRunSpeed.ToString("f1"));
        CreateNode(xmlDoc, nodeCommon, "AutoAttenuate", Main.s_Instance.m_fAutoAttenuate.ToString( "f5" ) );
		CreateNode(xmlDoc, nodeCommon, "ThornAttenuate", Main.s_Instance.m_fThornAttenuate.ToString( "f5" ) );
        CreateNode(xmlDoc, nodeCommon, "ForceSpitTotalTime", Main.s_Instance.m_fForceSpitTotalTime.ToString( "f5" ) );
        CreateNode(xmlDoc, nodeCommon, "ShellShrinkTotalTime", Main.s_Instance.m_fShellShrinkTotalTime.ToString( "f5" ) );
		CreateNode(xmlDoc, nodeCommon, "SplitShellShrinkTotalTime", Main.s_Instance.m_fSplitShellShrinkTotalTime.ToString( "f5" ) );
		CreateNode(xmlDoc, nodeCommon, "SplitCostMP", Main.s_Instance.m_fSplitCostMP.ToString( "f5" ) );
		CreateNode(xmlDoc, nodeCommon, "SplitStayTime", Main.s_Instance.m_fSplitStayTime.ToString( "f1" ) );
		CreateNode(xmlDoc, nodeCommon, "ExplodeShellShrinkTotalTime", Main.s_Instance.m_fExplodeShellShrinkTotalTime.ToString( "f5" ) );
        CreateNode(xmlDoc, nodeCommon, "SpitBallTimeInterval", Main.s_Instance.m_fSpitBallTimeInterval.ToString( "f5" ) );
        CreateNode(xmlDoc, nodeCommon, "SpitSporeTimeInterval", Main.s_Instance.m_fSpitSporeTimeInterval.ToString( "f5" ) );
		CreateNode(xmlDoc, nodeCommon, "SpitSporeCostMP", Main.s_Instance.m_fSpitSporeCostMP.ToString( "f5" ) );
		CreateNode(xmlDoc, nodeCommon, "MinDiameterToEatThorn", Main.s_Instance.m_fMinDiameterToEatThorn.ToString( "f5" ) );
        CreateNode(xmlDoc, nodeCommon, "ExplodeRunDistanceMultiple", Main.s_Instance.m_fExplodeRunDistanceMultiple.ToString( "f5" ) );
		CreateNode(xmlDoc, nodeCommon, "ExplodeStayTime", Main.s_Instance.m_fExplodeStayTime.ToString( "f5" ) );
		CreateNode(xmlDoc, nodeCommon, "ExplodePercent", Main.s_Instance.m_fExplodePercent.ToString( "f5" ) );
		CreateNode(xmlDoc, nodeCommon, "ExplodeExpectNum", Main.s_Instance.m_fExpectExplodeNum.ToString( "f0" ) );
		CreateNode(xmlDoc, nodeCommon, "ExplodeRunTime", Main.s_Instance.m_fExplodeRunTime.ToString( "f5" ) );
        CreateNode(xmlDoc, nodeCommon, "MaxBallNumPerPlayer", Main.s_Instance.m_fMaxBallNumPerPlayer.ToString( "f5" ) );
        CreateNode(xmlDoc, nodeCommon, "SplitNum", Main.s_Instance.m_nSplitNum.ToString( "f0" ) );
        CreateNode(xmlDoc, nodeCommon, "SplitRunTime", Main.s_Instance.m_fSplitRunTime.ToString( "f5" ) );
        CreateNode(xmlDoc, nodeCommon, "SplitMaxDistance", Main.s_Instance.m_fSplitMaxDistance.ToString( "f5" ) );
        CreateNode(xmlDoc, nodeCommon, "SplitTimeInterval", Main.s_Instance.m_fSplitTimeInterval.ToString( "f5" ) );
        CreateNode(xmlDoc, nodeCommon, "TimeBeforeUiDead", Main.s_Instance.m_fTimeBeforeUiDead.ToString("f5"));
        CreateNode(xmlDoc, nodeCommon, "TimeOfOneGame", Main.s_Instance.m_fTimeOfOneGame.ToString("f5"));
        CreateNode(xmlDoc, nodeCommon, "BallMinArea", Main.m_fBallMinVolume.ToString("f5"));
        CreateNode(xmlDoc, nodeCommon, "BaseVolumeDecreasedPercentWhenDead", Main.s_Instance.m_fBaseVolumeyByItemDecreasedPercentWhenDead.ToString("f5"));
        
    }
		
    public void SaveGrass(XmlDocument xmlDoc, XmlNode nodeGrass)
	{
		foreach (Transform child in m_goGrassContainer.transform)
        {
            Polygon polygon = child.gameObject.GetComponent<Polygon>();
            if ( !polygon.GetIsGrass() )
            {
                continue;
            }
			string content = polygon.m_szFileName + "," + child.localPosition.x + "," + child.localPosition.y + "," + child.localScale.x + "," + child.localScale.y + "," + polygon.m_fRotatoinZ + "," + ((polygon.GetIsGrassSeed()?"True":"False") + "," + polygon.GetSeedGrassLifeTime());   
            CreateNode(xmlDoc, nodeGrass, "P", content);
        }
	}

    public void SavePolygons(XmlDocument xmlDoc, XmlNode nodePolygon )
    {
        foreach (Transform child in m_goPolygonContainer.transform)
        {
            Polygon polygon = child.gameObject.GetComponent<Polygon>();
            if (polygon.GetIsGrass())
            {
                continue;
            }
			string content = polygon.m_szFileName + "," + child.localPosition.x + "," + child.localPosition.y + "," + child.localScale.x + "," + child.localScale.y + "," + polygon.m_fRotatoinZ + "," + (polygon.IsHardObstacle()?1:0);
            CreateNode(xmlDoc, nodePolygon, "P", content);
        }
    }

	public void SaveFakeRandomThornPos( XmlDocument xmlDoc, XmlNode node )
	{
		// 先根据场景的长、宽，计算出刺的总个数
		int nNum = (int)(Main.s_Instance.m_fThornNumPerRange * m_fWorldSizeX * m_fWorldSizeY);
		float fWorldLeft = -m_fWorldSizeX / 2.0f;
		float fWorldRight = m_fWorldSizeX / 2.0f; 
		float fWorldBottom = -m_fWorldSizeY / 2.0f;
		float fWorldTop = m_fWorldSizeY / 2.0f;
		for (int i = 0; i < nNum; i++) {
			vecTempPos.x = (float)UnityEngine.Random.Range ( fWorldLeft, fWorldRight );
			vecTempPos.y = (float)UnityEngine.Random.Range ( fWorldBottom, fWorldTop );
			string content = vecTempPos.x + "," + vecTempPos.y;
			CreateNode(xmlDoc, node, "P", content);
		}
	}

    public void SaveBeanSpray(XmlDocument xmlDoc, XmlNode nodeSpray)
    {
        foreach (Transform child in m_goBeanSprayContainer.transform)
        {
            Spray spray = child.gameObject.GetComponent<Spray>();
			string content = spray.transform.position.x + ","
			                 + spray.transform.position.y + ","
			                 + spray.GetDensity () + ","
			                 + spray.GetMeatDensit () + ","
			                 + spray.GetMaxDis () + ","
			                 + spray.GetMinDis () + ","
			                 + spray.GetBeanLifeTime () + ","
			                 + spray.GetMeat2BeanSprayTime () + ","
			                 + spray.GetSpraySize () + ","
			                 + spray.GetDrawSpeed () + ","
			                 + spray.GetDrawArea () + ","
			                 + spray.GetPreDrawTime () + ","
			                 + spray.GetDrawTime () + ","
			                 + spray.GetDrawInterval () + ","
			                 + spray.GetThornChance () + ","
			                 + spray.GetSprayInterval ();
            CreateNode(xmlDoc, nodeSpray, "P", content);
        }
    }

    public void SaveColorThorn(XmlDocument xmlDoc, XmlNode nodeThorn)
    {
		string szParams = "";
		foreach( KeyValuePair<int, Thorn.ColorThornParam> pair in m_dicColorThornParam )
		{
			szParams += pair.Key + "," + pair.Value.thorn_size + "," + pair.Value.ball_size + "," + pair.Value.reborn_time + "|";
		}
		CreateNode(xmlDoc, nodeThorn, "Params", szParams);

        foreach (Transform child in m_goColorThornContainer.transform)
        {
            Thorn thorn = child.gameObject.GetComponent<Thorn>();
            string content = thorn.transform.position.x + "," + thorn.transform.position.y + "," + thorn.GetColorIdx();
            CreateNode(xmlDoc, nodeThorn, "P", content);
        }
    }

    public void SaveLababa(XmlDocument xmlDoc, XmlNode nodeLababa)
    {
        CreateNode(xmlDoc, nodeLababa, "LababaAreaThreshold",  Main.s_Instance.m_fLababaAreaThreshold.ToString());
        CreateNode(xmlDoc, nodeLababa, "LaBabaRunDistance", Main.s_Instance.m_fLaBabaRunDistance.ToString());
        CreateNode(xmlDoc, nodeLababa, "LaBabaRunTime", Main.s_Instance.m_fLaBabaRunTime.ToString());
        CreateNode(xmlDoc, nodeLababa, "LababaLiveTime", Main.s_Instance.m_fLababaLiveTime.ToString());
        CreateNode(xmlDoc, nodeLababa, "LababaColdDown", Main.s_Instance.m_fLababaColdDown.ToString());
        CreateNode(xmlDoc, nodeLababa, "LababaSizeCostPercent", Main.s_Instance.m_fLababaSizeCostPercent.ToString());
    }

	

    public void CreateNode(XmlDocument xmlDoc,XmlNode parentNode,string name,string value)  
	{  
		XmlNode node = xmlDoc.CreateNode(XmlNodeType.Element, name, null);  
		node.InnerText = value;  
		parentNode.AppendChild(node);  
	}

    Dictionary<string, float> m_dicEatModeConfig = new Dictionary<string, float>();
    public void GenerateEatMode( XmlNode nodeEatMode )
    {
        float val = 0;
        m_dicEatModeConfig.Clear();
        if (nodeEatMode != null)
        {
            for (int i = 0; i < nodeEatMode.ChildNodes.Count; i++)
            {
                m_dicEatModeConfig[nodeEatMode.ChildNodes[i].Name] = float.Parse(nodeEatMode.ChildNodes[i].InnerText);
            }
        }

		if ( !GetEatModeConfig( "UnfoldCostMP", ref Main.s_Instance.m_fUnfoldCostMP ))
		{
			Main.s_Instance.m_fUnfoldCostMP = 0f;
		}
		_inputUnfoldCostMP.text = Main.s_Instance.m_fUnfoldCostMP.ToString( "f5" );


        if ( !GetEatModeConfig( "MainTriggerPreUnfoldTime", ref Main.s_Instance.m_fMainTriggerPreUnfoldTime ))
        {
            Main.s_Instance.m_fMainTriggerPreUnfoldTime = 2f;
        }
        _inputMainTriggerPreUnfoldTime.text = Main.s_Instance.m_fMainTriggerPreUnfoldTime.ToString( "f5" );

        if ( !GetEatModeConfig( "MainTriggerUnfoldTime", ref Main.s_Instance.m_fMainTriggerUnfoldTime ))
        {
            Main.s_Instance.m_fMainTriggerUnfoldTime = 2f;
        }
        _inputMainTriggerUnfoldTime.text = Main.s_Instance.m_fMainTriggerUnfoldTime.ToString( "f5" );

        if ( !GetEatModeConfig( "UnfoldSale", ref Main.s_Instance.m_fUnfoldSale ))
        {
            Main.s_Instance.m_fUnfoldSale = 1.5f;
        }
        _inputUnfoldSale.text = Main.s_Instance.m_fUnfoldSale.ToString( "f5" );

        if ( !GetEatModeConfig( "UnfoldTimeInterval", ref Main.s_Instance.m_fUnfoldTimeInterval ))
        {
            Main.s_Instance.m_fUnfoldTimeInterval = 2f;
        }
        _inputUnfoldTimeInterval.text = Main.s_Instance.m_fUnfoldTimeInterval.ToString( "f5" );

        if ( !GetEatModeConfig( "YaQiuTiaoJian", ref m_fYaQiuTiaoJian ))
        {
            m_fYaQiuTiaoJian = 1.5f;
        }
        _inputYaQiuTiaoJian.text = m_fYaQiuTiaoJian.ToString( "f5" );

        if ( !GetEatModeConfig( "YaQiuBaiFenBi", ref m_fYaQiuBaiFenBi ))
        {
            m_fYaQiuBaiFenBi = 0.4f;
        }
        _inputYaQiuBaiFenBi.text = m_fYaQiuBaiFenBi.ToString( "f5" );

        if (!GetEatModeConfig("YaQiuBaiFenBi_Self", ref m_fYaQiuBaiFenBi_Self))
        {
            m_fYaQiuBaiFenBi_Self = 0.4f;
        }
        _inputYaQiuBaiFenBi_Self.text = m_fYaQiuBaiFenBi_Self.ToString("f5");


        if ( !GetEatModeConfig( "XiQiuSuDu", ref m_fXiQiuSuDu ))
        {
            m_fXiQiuSuDu = 1f;
        }
        _inputXiQiuSuDu.text = m_fXiQiuSuDu.ToString( "f5" );

        if ( !GetEatModeConfig( "EatMode", ref val ))
        {
            val = 0f;
        }
       // m_eEatMode = (eEatMode)val;
        //_dropdownEatMode.value = (int)m_eEatMode;
    }

    public bool GetEatModeConfig( string key, ref float val )
    {
        return m_dicEatModeConfig.TryGetValue( key, out val );
    }

	Dictionary<string, float> m_dicCommonConfig = new Dictionary<string, float> ();
	bool GetValueByKey( Dictionary<string, float> dic, string key, ref float val )
	{
		return dic.TryGetValue ( key, out val );
	}

	public static void SetCamZoomTime( float fZoomTime )
	{
		CtrlMode._zoom_time = fZoomTime;
	}

    public void SetCamRespondSpeed( float fCamRespondSpeed)
    {
        Main.s_Instance.m_fCamRespondSpeed = fCamRespondSpeed;
    }
    
    public void SetCamSizeXiShu( float fCamSizeXiShu )
    {
        Main.s_Instance.m_fCamSizeXiShu = fCamSizeXiShu;
    }

	public static void SetBallSizeAffectCamSize( float val )
	{
		CtrlMode._ball_scale_zoom_factor = val;
	}

	Dictionary<string, string> m_dicConfigCommon2 = new Dictionary<string, string> ();
	public void GenerateCommon2(XmlNode nodeCommon)
	{
		if (nodeCommon == null) {
			return;
		}

		m_lstRebornPos.Clear();
		for (int i = 0; i < nodeCommon.ChildNodes.Count; i++) {
			m_dicConfigCommon2[nodeCommon.ChildNodes [i].Name] = nodeCommon.ChildNodes [i].InnerText;
		}

		string szContent = "0,0|";
		if (!m_dicConfigCommon2.TryGetValue ("RebornSpot", out szContent)) {
			szContent = "0,0|";
		}
		string[] aryContent = szContent.Split ( '|' );
		for (int i = 0; i < aryContent.Length; i++) {
			if (aryContent [i].Length == 0) {
				continue;
			}
			string[] aryPos = aryContent [i].Split ( ',' );
			Vector3 pos = new Vector3 ();
			pos.x = float.Parse( aryPos[0] ); 
			pos.y = float.Parse (aryPos [1]); 
			pos.z = 0f;
			m_lstRebornPos.Add ( pos );
			AddRebornSpot (pos);
		}





    }

    void GenerateRebornSporRanPos()
    {
        sRebornRanPos pos1 = new sRebornRanPos();
        pos1.vecMinPos.x = -m_fHalfWorldWidth;
        pos1.vecMinPos.y = CClassEditor.s_Instance.GetClassCircleRadius();
        pos1.vecMaxPos.x = m_fHalfWorldWidth;
        pos1.vecMaxPos.y = m_fHalfWorldHeight;


        sRebornRanPos pos2 = new sRebornRanPos();
        pos2.vecMinPos.x = -m_fHalfWorldWidth;
        pos2.vecMinPos.y = m_fHalfWorldHeight;
        pos2.vecMaxPos.x = m_fHalfWorldWidth;
        pos2.vecMaxPos.y = -CClassEditor.s_Instance.GetClassCircleRadius();

        sRebornRanPos pos3 = new sRebornRanPos();
        pos3.vecMinPos.x = -m_fHalfWorldWidth;
        pos3.vecMinPos.y = -m_fHalfWorldHeight;
        pos3.vecMaxPos.x = -CClassEditor.s_Instance.GetClassCircleRadius();
        pos3.vecMaxPos.y = m_fHalfWorldHeight;

        sRebornRanPos pos4 = new sRebornRanPos();
        pos4.vecMinPos.x = CClassEditor.s_Instance.GetClassCircleRadius();
        pos4.vecMinPos.y = -m_fHalfWorldHeight;
        pos4.vecMaxPos.x = m_fHalfWorldWidth;
        pos4.vecMaxPos.y = m_fHalfWorldHeight;

        m_lstRebornRandomPos.Add(pos1);
        m_lstRebornRandomPos.Add(pos2);
        m_lstRebornRandomPos.Add(pos3);
        m_lstRebornRandomPos.Add(pos4);


    }

    List<Vector2> m_lstFakeRandomThornPos = new List<Vector2> ();
	public void GenerateFakeRandomThornPos( XmlNode node )
	{
		if (node == null) {
			return;
		}
		for (int i = 0; i < node.ChildNodes.Count; i++) {
			string[] ary = node.ChildNodes [i].InnerText.Split( ',' );
			Vector2 pos = new Vector2 ();
			pos.x = float.Parse ( ary[0] );
			pos.y = float.Parse ( ary[1] );
			m_lstFakeRandomThornPos.Add ( pos );
		}
	}

	public Vector2 GetFakeRandomThornPos( int nIndex )
	{
		if (nIndex >= m_lstFakeRandomThornPos.Count) {
			return Vector2.zero;
		}
		return m_lstFakeRandomThornPos [nIndex];
	}

	Dictionary<int, List<Vector2>> m_dicExplodeDir = new Dictionary<int, List<Vector2>> ();
	List<Vector2> CalculateExplodeDirection( int nChildNum )
	{
		List<Vector2> lst = new List<Vector2> ();
		Vector2 vec = new Vector2 ();
		float fDivideAngle = ( 360f / nChildNum ) * Mathf.PI / 180f;
		for (int i = 0; i < nChildNum; i++) {
			float fAngle = fDivideAngle * i;
			vec.x = Mathf.Sin ( fAngle );
			vec.y = Mathf.Cos ( fAngle );
			lst.Add ( vec );
		}
		m_dicExplodeDir [nChildNum] = lst;
		return lst;
	}

	public List<Vector2> GetExplodeDirList( int nChildNum )
	{
		List<Vector2> lst = null;
		if (!m_dicExplodeDir.TryGetValue (nChildNum, out lst)) {
			lst = CalculateExplodeDirection ( nChildNum );
		}
		return lst;
	}
		

    public void GenerateCommon(XmlNode nodeCommon)
    {
		float val = 0f;

		m_dicCommonConfig.Clear ();
		if (nodeCommon != null) {
			for (int i = 0; i < nodeCommon.ChildNodes.Count; i++) {
                if (float.TryParse(nodeCommon.ChildNodes[i].InnerText, out val))
                {

                }
				m_dicCommonConfig [nodeCommon.ChildNodes [i].Name] = val;

                // poppin test
                if (nodeCommon.ChildNodes[i].Name == "SpitSporeBeanType")
                {
                    Main.s_Instance.m_szSpitSporeBeanType = nodeCommon.ChildNodes[i].InnerText;
                    _inputfieldSpitSporeBeanType.text = Main.s_Instance.m_szSpitSporeBeanType;
                }
			}
		}

        //// 射球参数相关
        if (!GetValueByKey(m_dicCommonConfig, "EjectParamA", ref m_fEjectParamA))
        {
            m_fEjectParamA = 2f;
        }
        _inputA.text = m_fEjectParamA.ToString("f1");
        if (!GetValueByKey(m_dicCommonConfig, "EjectParamB", ref m_fEjectParamB))
        {
            m_fEjectParamB = 0f;
        }
        _inputB.text = m_fEjectParamB.ToString("f1");
        if (!GetValueByKey(m_dicCommonConfig, "EjectParamX", ref m_fEjectParamX))
        {
            m_fEjectParamX = 0.5f;
        }
        _inputX.text = m_fEjectParamX.ToString("f2");
        //// end 射球参数相关

        float fPlayerNumToStartGame = 1f;
        if (!GetValueByKey(m_dicCommonConfig, "PlayerNumToStartGame", ref fPlayerNumToStartGame))
        {
            m_nPlayerNumToStartGame = 1;
        }
        else
        {
            m_nPlayerNumToStartGame = (int)fPlayerNumToStartGame;
        }
        _inputPlayerNumToStartGame.text = m_nPlayerNumToStartGame.ToString();

        //m_fWorldSize = nodeCommon != null && nodeCommon.ChildNodes.Count > nIndex?float.Parse( nodeCommon.ChildNodes[nIndex++].InnerText ):128;
        if (!GetValueByKey (m_dicCommonConfig, "WorldSizeX", ref m_fWorldSizeX)) {
			m_fWorldSizeX = 128f;
		}
        SetWorldSizeX(m_fWorldSizeX);
		_inputWorldSizeX.text = m_fWorldSizeX.ToString( "f5" );
		if (!GetValueByKey (m_dicCommonConfig, "WorldSizeY", ref m_fWorldSizeY)) {
			m_fWorldSizeY = 128f;
		}
        SetWorldSizeY(m_fWorldSizeY);
        _inputWorldSizeY.text = m_fWorldSizeY.ToString( "f5" );

        //// camera
        //m_fMaxCameraSize = nodeCommon != null && nodeCommon.ChildNodes.Count > nIndex?float.Parse( nodeCommon.ChildNodes[nIndex++].InnerText ):60f;
		if (!GetValueByKey (m_dicCommonConfig, "MaxCameraSize", ref m_fMaxCameraSize)) {
			m_fMaxCameraSize = 60f;
		}
		_inputfieldCamSizeMax.text = m_fMaxCameraSize.ToString( "f5" );

        if (!GetValueByKey(m_dicCommonConfig, "MinCameraSize", ref m_fMinCameraSize))
        {
            m_fMinCameraSize = 30f;
        }
        _inputfieldCamSizeMin.text = m_fMinCameraSize.ToString("f5");


        //// 镜头抬升相关
        if (!GetValueByKey(m_dicCommonConfig, "CamRespondSpeed", ref Main.s_Instance.m_fCamRespondSpeed))
        {
            Main.s_Instance.m_fCamRespondSpeed = 50f;
        }
        _inputfieldCamRespondSpeed.text = Main.s_Instance.m_fCamRespondSpeed.ToString("f5");


        if (!GetValueByKey(m_dicCommonConfig, "CamRaiseInitSpeedXiShu", ref Main.s_Instance.m_fRaiseSpeed))
        {
            Main.s_Instance.m_fRaiseSpeed = 100f;
        }
        _inputCamRaiseSpeed.text = Main.s_Instance.m_fRaiseSpeed.ToString("f5");


        if (!GetValueByKey(m_dicCommonConfig, "CamRaiseAccelerate", ref Main.s_Instance.m_fCamRaiseAccelerate))
        {
            Main.s_Instance.m_fCamRaiseAccelerate = 100f;
        }
        _inputCamShangFuAccelerate.text = Main.s_Instance.m_fCamRaiseAccelerate.ToString("f5");



        if (!GetValueByKey(m_dicCommonConfig, "CamChangeDelaySpit", ref Main.s_Instance.m_fCamChangeDelaySpit))
        {
            Main.s_Instance.m_fCamChangeDelaySpit = 100f;
        }
        _inputCamChangeDelaySpit.text = Main.s_Instance.m_fCamChangeDelaySpit.ToString("f5");



        if (!GetValueByKey(m_dicCommonConfig, "CamChangeDelayExplode", ref Main.s_Instance.m_fCamChangeDelayExplode))
        {
            Main.s_Instance.m_fCamChangeDelayExplode = 100f;
        }
        _inputCamChangeDelayExplode.text = Main.s_Instance.m_fCamChangeDelayExplode.ToString("f5");

        if (!GetValueByKey(m_dicCommonConfig, "TimeBeforeCamDown", ref Main.s_Instance.m_fTimeBeforeCamDown))
        {
            Main.s_Instance.m_fTimeBeforeCamDown = 0.1f;
        }
        _inputCamTimeBeforeDown.text = Main.s_Instance.m_fTimeBeforeCamDown.ToString("f5");

        

        //// end 镜头抬升相关





        //m_fCameraZoomSpeed = nodeCommon != null && nodeCommon.ChildNodes.Count > nIndex?float.Parse( nodeCommon.ChildNodes[nIndex++].InnerText ):60f;
        if (!GetValueByKey (m_dicCommonConfig, "CamZoomSpeed", ref m_fCameraZoomSpeed)) {
			m_fCameraZoomSpeed = 0.5f;
		}
		//_inputfieldCamZoomSpeed.text = m_fCameraZoomSpeed.ToString( "f5" );
		//SetCamZoomTime (m_fCameraZoomSpeed)  ;


        if (!GetValueByKey(m_dicCommonConfig, "BornProtectTime", ref Main.s_Instance.m_fBornProtectTime ))
        {
            Main.s_Instance.m_fBornProtectTime = 5f;
        }
        _inputBornProtectTime.text = Main.s_Instance.m_fBornProtectTime.ToString("f5");
        
        if (!GetValueByKey (m_dicCommonConfig, "BallSizeAffectCamSize", ref m_fBallSizeAffectCamSize)) {
			m_fBallSizeAffectCamSize = 0.5f;
		}
		_inputfieldBallSizeAffectCamSize.text = m_fBallSizeAffectCamSize.ToString( "f5" );
		SetBallSizeAffectCamSize (m_fBallSizeAffectCamSize)  ;


		//Main.s_Instance.m_fBeanNumPerRange = nodeCommon != null && nodeCommon.ChildNodes.Count > nIndex?float.Parse( nodeCommon.ChildNodes[nIndex++].InnerText ):0.1f;
		if (!GetValueByKey (m_dicCommonConfig, "BeanNumPerRange", ref Main.s_Instance.m_fBeanNumPerRange)) {
			Main.s_Instance.m_fBeanNumPerRange = 0.01f;
		}
		_inputfieldBeanNumPerRange.text = Main.s_Instance.m_fBeanNumPerRange.ToString( "f5" );
        
		//Main.s_Instance.m_fBallBaseSpeed = nodeCommon != null && nodeCommon.ChildNodes.Count > nIndex?float.Parse( nodeCommon.ChildNodes[nIndex++].InnerText ):24f;
		if (!GetValueByKey (m_dicCommonConfig, "BallBaseSpeed", ref Main.s_Instance.m_fBallBaseSpeed)) {
			Main.s_Instance.m_fBallBaseSpeed = 24f;
		}
		_inputfieldBallBaseSpeed.text = Main.s_Instance.m_fBallBaseSpeed.ToString( "f5" );

        if (!GetValueByKey(m_dicCommonConfig, "MaxSpeed", ref Main.s_Instance.m_fMaxPlayerSpeed))
        {
            Main.s_Instance.m_fMaxPlayerSpeed = 24f;
        }
        _inputfieldMaxSpeed.text = Main.s_Instance.m_fMaxPlayerSpeed.ToString("f5");


        if (!GetValueByKey(m_dicCommonConfig, "BallSpeedRadiusKaiFangFenMu", ref Main.s_Instance.m_fBallSpeedRadiusKaiFangFenMu))
        {
            Main.s_Instance.m_fBallSpeedRadiusKaiFangFenMu = 1f;
        }
        _inputfieldSpeedRadiusKaiFangFenMu.text = Main.s_Instance.m_fBallSpeedRadiusKaiFangFenMu.ToString("f5");

        

            //Main.s_Instance.m_fBornMinDiameter = nodeCommon != null && nodeCommon.ChildNodes.Count > nIndex?float.Parse( nodeCommon.ChildNodes[nIndex++].InnerText ):4f;
        if (!GetValueByKey (m_dicCommonConfig, "BornMinDiameter", ref Main.s_Instance.m_fBornMinTiJi)) {
			Main.s_Instance.m_fBornMinTiJi = 64f;
		}
		_inputfieldBornMinDiameter.text = Main.s_Instance.m_fBornMinTiJi.ToString( "f5" );

        if (!GetValueByKey(m_dicCommonConfig, "BallMinArea", ref Main.m_fBallMinVolume))
        {
            Main.m_fBallMinVolume = 1f;
        }
        _inputBallMinArea.text = Main.m_fBallMinVolume.ToString("f5");
        


        //Main.s_Instance.m_fThornNumPerRange = nodeCommon != null && nodeCommon.ChildNodes.Count > nIndex?float.Parse( nodeCommon.ChildNodes[nIndex++].InnerText ):0.001f;
        if (!GetValueByKey (m_dicCommonConfig, "ThornNumPerRange", ref Main.s_Instance.m_fThornNumPerRange)) {
			Main.s_Instance.m_fThornNumPerRange = 0.005f;
		}
		_inputfieldThornNumPerRange.text = Main.s_Instance.m_fThornNumPerRange.ToString( "f5" );

        //Main.s_Instance.m_fGenerateNewBeanTimeInterval = nodeCommon != null && nodeCommon.ChildNodes.Count > nIndex?float.Parse( nodeCommon.ChildNodes[nIndex++].InnerText ):300f;
		if (!GetValueByKey (m_dicCommonConfig, "GenerateNewBeanTimeInterval", ref Main.s_Instance.m_fGenerateNewBeanTimeInterval)) {
			Main.s_Instance.m_fGenerateNewBeanTimeInterval = 20f;
		}
		_inputfieldGenerateNewBeanTimeInterval.text = Main.s_Instance.m_fGenerateNewBeanTimeInterval.ToString( "f5" );

        //Main.s_Instance.m_fGenerateNewThornTimeInterval = nodeCommon != null && nodeCommon.ChildNodes.Count > nIndex?float.Parse( nodeCommon.ChildNodes[nIndex++].InnerText ):300f;
		if (!GetValueByKey (m_dicCommonConfig, "GenerateNewThornTimeInterval", ref Main.s_Instance.m_fGenerateNewThornTimeInterval)) {
			Main.s_Instance.m_fGenerateNewThornTimeInterval = 20f;
		}
		_inputfieldGenerateNewThornTimeInterval.text = Main.s_Instance.m_fGenerateNewThornTimeInterval.ToString( "f5" );

       // Main.s_Instance.m_fBeanSize = nodeCommon != null && nodeCommon.ChildNodes.Count > nIndex?float.Parse( nodeCommon.ChildNodes[nIndex++].InnerText ):0.8f;
  		if (!GetValueByKey (m_dicCommonConfig, "BeanSize", ref Main.s_Instance.m_fBeanSize)) {
			Main.s_Instance.m_fBeanSize = 0.8f;
		}
		 _inputfieldBeanSize.text = Main.s_Instance.m_fBeanSize.ToString( "f5" );
        
		//Main.s_Instance.m_fThornSize = nodeCommon != null && nodeCommon.ChildNodes.Count > nIndex?float.Parse( nodeCommon.ChildNodes[nIndex++].InnerText ):12f;
		if (!GetValueByKey (m_dicCommonConfig, "ThornSize", ref Main.s_Instance.m_fThornSize)) {
			Main.s_Instance.m_fThornSize = 3f;
		}
		_inputfieldThornSize.text = Main.s_Instance.m_fThornSize.ToString( "f5" );
        
		//Main.s_Instance.m_fThornContributeToBallSize = nodeCommon != null && nodeCommon.ChildNodes.Count > nIndex?float.Parse( nodeCommon.ChildNodes[nIndex++].InnerText ):8f;
		if (!GetValueByKey (m_dicCommonConfig, "ThornContributeToBallSize", ref Main.s_Instance.m_fThornContributeToBallSize)) {
			Main.s_Instance.m_fThornContributeToBallSize = 2f;
		}
		_inputfieldThornContributeToBallSize.text = Main.s_Instance.m_fThornContributeToBallSize.ToString( "f5" );
        
		//Main.s_Instance.m_fSpitSporeRunDistance = nodeCommon != null && nodeCommon.ChildNodes.Count > nIndex?float.Parse( nodeCommon.ChildNodes[nIndex++].InnerText ):2f;
		if (!GetValueByKey (m_dicCommonConfig, "SpitSporeRunDistance", ref Main.s_Instance.m_fSpitSporeRunDistance)) {
			Main.s_Instance.m_fSpitSporeRunDistance = 4f;
		}
		_inputfieldSpitSporeRunDistance.text = Main.s_Instance.m_fSpitSporeRunDistance.ToString( "f5" );
        

			//Main.s_Instance.m_fSpitRunTime = nodeCommon != null && nodeCommon.ChildNodes.Count > nIndex?float.Parse( nodeCommon.ChildNodes[nIndex++].InnerText ):1f;
        
		if (!GetValueByKey (m_dicCommonConfig, "SpitRunTime", ref Main.s_Instance.m_fSpitRunTime)) {
			Main.s_Instance.m_fSpitRunTime = 1f;
		}
		_inputfieldSpitRunTime.text = Main.s_Instance.m_fSpitRunTime.ToString( "f5" );
		if (!GetValueByKey (m_dicCommonConfig, "SpitBallRunTime", ref Main.s_Instance.m_fSpitBallRunTime)) {
			Main.s_Instance.m_fSpitBallRunTime = 0.5f;
		}
		_inputfieldSpitBallRunTime.text = Main.s_Instance.m_fSpitBallRunTime.ToString( "f5" );

        //Main.s_Instance.m_fSpitBallRunDistanceMultiple = nodeCommon != null && nodeCommon.ChildNodes.Count > nIndex?float.Parse( nodeCommon.ChildNodes[nIndex++].InnerText ):2f;
		if (!GetValueByKey (m_dicCommonConfig, "SpitBallRunDistanceMultiple", ref Main.s_Instance.m_fSpitBallRunDistanceMultiple)) {
			Main.s_Instance.m_fSpitBallRunDistanceMultiple = 2f;
		}
		_inputfieldSpitBallRunDistanceMultiple.text = Main.s_Instance.m_fSpitBallRunDistanceMultiple.ToString( "f5" );


		if (!GetValueByKey (m_dicCommonConfig, "SpitBallCostMP", ref Main.s_Instance.m_fSpitBallCostMP)) {
			Main.s_Instance.m_fSpitBallCostMP = 2f;
		}
		_inputfieldSpitBallCostMP.text = Main.s_Instance.m_fSpitBallCostMP.ToString( "f5" );

		if (!GetValueByKey (m_dicCommonConfig, "SpitBallStayTime", ref Main.s_Instance.m_fSpitBallStayTime)) {
			Main.s_Instance.m_fSpitBallStayTime = 2f;
		}
		_inputfieldSpitBallStayTime.text = Main.s_Instance.m_fSpitBallStayTime.ToString( "f1" );

        if (!GetValueByKey(m_dicCommonConfig, "SpitBallSpeed", ref Main.s_Instance.m_fSpitBallRunSpeed))
        {
            Main.s_Instance.m_fSpitBallRunSpeed = 150f;
        }
        _inputfieldSpitBallSpeed.text = Main.s_Instance.m_fSpitBallRunSpeed.ToString("f1");
        
        //Main.s_Instance.m_fAutoAttenuate = nodeCommon != null && nodeCommon.ChildNodes.Count > nIndex?float.Parse( nodeCommon.ChildNodes[nIndex++].InnerText ):0.1f;
        if (!GetValueByKey (m_dicCommonConfig, "AutoAttenuate", ref Main.s_Instance.m_fAutoAttenuate)) {
			Main.s_Instance.m_fAutoAttenuate = 0.01f;
		}
		_inputfieldAutoAttenuate.text = Main.s_Instance.m_fAutoAttenuate.ToString( "f5" );

		if (!GetValueByKey (m_dicCommonConfig, "ThornAttenuate", ref Main.s_Instance.m_fThornAttenuate)) {
			Main.s_Instance.m_fThornAttenuate = 0.01f;
		}
		_inputfieldThornAttenuate.text = Main.s_Instance.m_fThornAttenuate.ToString( "f5" );

        
		//Main.s_Instance.m_fForceSpitTotalTime = nodeCommon != null && nodeCommon.ChildNodes.Count > nIndex?float.Parse( nodeCommon.ChildNodes[nIndex++].InnerText ):1f;
		if (!GetValueByKey (m_dicCommonConfig, "ForceSpitTotalTime", ref Main.s_Instance.m_fForceSpitTotalTime)) {
			Main.s_Instance.m_fForceSpitTotalTime = 1f;
		}
		_inputfieldForceSpitTotalTime.text = Main.s_Instance.m_fForceSpitTotalTime.ToString( "f5" );
        
		//Main.s_Instance.m_fShellShrinkTotalTime = nodeCommon != null && nodeCommon.ChildNodes.Count > nIndex?float.Parse( nodeCommon.ChildNodes[nIndex++].InnerText ):5f;
		if (!GetValueByKey (m_dicCommonConfig, "ShellShrinkTotalTime", ref Main.s_Instance.m_fShellShrinkTotalTime)) {
			Main.s_Instance.m_fShellShrinkTotalTime = 5f;
		}
		_inputfieldShellShrinkTotalTime.text = Main.s_Instance.m_fShellShrinkTotalTime.ToString( "f5" );
        

		if (!GetValueByKey (m_dicCommonConfig, "SplitShellShrinkTotalTime", ref Main.s_Instance.m_fSplitShellShrinkTotalTime)) {
			Main.s_Instance.m_fSplitShellShrinkTotalTime = 5f;
		}
		_inputfieldSplitShellTotalTime.text = Main.s_Instance.m_fSplitShellShrinkTotalTime.ToString( "f5" );

		if (!GetValueByKey (m_dicCommonConfig, "SplitCostMP", ref Main.s_Instance.m_fSplitCostMP)) {
			Main.s_Instance.m_fSplitCostMP = 0f;
		}
		_inputfieldSplitCostMP.text = Main.s_Instance.m_fSplitCostMP.ToString( "f5" );

		if (!GetValueByKey (m_dicCommonConfig, "SplitStayTime", ref Main.s_Instance.m_fSplitStayTime)) {
			Main.s_Instance.m_fSplitStayTime = 0f;
		}
		_inputfieldSplitStayTime.text = Main.s_Instance.m_fSplitStayTime.ToString( "f1" );




		if (!GetValueByKey (m_dicCommonConfig, "ExplodeShellShrinkTotalTime", ref Main.s_Instance.m_fExplodeShellShrinkTotalTime)) {
			Main.s_Instance.m_fExplodeShellShrinkTotalTime = 5f;
		}
		_inputfieldExplodeShellTotalTime.text = Main.s_Instance.m_fExplodeShellShrinkTotalTime.ToString( "f5" );

		//Main.s_Instance.m_fSpitBallTimeInterval = nodeCommon != null && nodeCommon.ChildNodes.Count > nIndex?float.Parse( nodeCommon.ChildNodes[nIndex++].InnerText ):1f;
		if (!GetValueByKey (m_dicCommonConfig, "SpitBallTimeInterval", ref Main.s_Instance.m_fSpitBallTimeInterval)) {
			Main.s_Instance.m_fSpitBallTimeInterval = 1f;
		}
		_inputfieldSpitBallTimeInterval.text = Main.s_Instance.m_fSpitBallTimeInterval.ToString( "f5" );
       
		// Main.s_Instance.m_fSpitSporeTimeInterval = nodeCommon != null && nodeCommon.ChildNodes.Count > nIndex?float.Parse( nodeCommon.ChildNodes[nIndex++].InnerText ):1f;
		if (!GetValueByKey (m_dicCommonConfig, "SpitSporeTimeInterval", ref Main.s_Instance.m_fSpitSporeTimeInterval)) {
			Main.s_Instance.m_fSpitSporeTimeInterval = 1f;
		}
		_inputfieldSpitSporeTimeInterval.text = Main.s_Instance.m_fSpitSporeTimeInterval.ToString( "f5" );
  
		if (!GetValueByKey (m_dicCommonConfig, "SpitSporeCostMP", ref Main.s_Instance.m_fSpitSporeCostMP)) {
			Main.s_Instance.m_fSpitSporeCostMP = 0f;
		}
		_inputfieldSpitSporeCostMP.text = Main.s_Instance.m_fSpitSporeCostMP.ToString( "f5" );


		//Main.s_Instance.m_fMinDiameterToEatThorn = nodeCommon != null && nodeCommon.ChildNodes.Count > nIndex?float.Parse( nodeCommon.ChildNodes[nIndex++].InnerText ):3.5f;
		if (!GetValueByKey (m_dicCommonConfig, "MinDiameterToEatThorn", ref Main.s_Instance.m_fMinDiameterToEatThorn)) {
			Main.s_Instance.m_fMinDiameterToEatThorn = 3.5f;
		}
		_inputfieldMinDiameterToEatThorn.text = Main.s_Instance.m_fMinDiameterToEatThorn.ToString( "f5" );
        
		//Main.s_Instance.m_fExplodeRunDistanceMultiple = nodeCommon != null && nodeCommon.ChildNodes.Count > nIndex?float.Parse( nodeCommon.ChildNodes[nIndex++].InnerText ):2f;
		if (!GetValueByKey (m_dicCommonConfig, "ExplodeRunDistanceMultiple", ref Main.s_Instance.m_fExplodeRunDistanceMultiple)) {
			Main.s_Instance.m_fExplodeRunDistanceMultiple = 2f;
		}
		_inputfieldExplodeRunDistanceMultiple.text = Main.s_Instance.m_fExplodeRunDistanceMultiple.ToString( "f5" );
        
		//Main.s_Instance.m_fExplodeRunDistanceMultiple = nodeCommon != null && nodeCommon.ChildNodes.Count > nIndex?float.Parse( nodeCommon.ChildNodes[nIndex++].InnerText ):2f;
		if (!GetValueByKey (m_dicCommonConfig, "ExplodeStayTime", ref Main.s_Instance.m_fExplodeStayTime)) {
			Main.s_Instance.m_fExplodeStayTime = 0.3f;
		}
		_inputfieldExplodeStayTime.text = Main.s_Instance.m_fExplodeStayTime.ToString( "f5" );

	
		if (!GetValueByKey (m_dicCommonConfig, "ExplodePercent", ref Main.s_Instance.m_fExplodePercent)) {
			Main.s_Instance.m_fExplodePercent = 0.7f;
		}
		_inputfieldExplodePercent.text = Main.s_Instance.m_fExplodePercent.ToString( "f5" );
	
		if (!GetValueByKey (m_dicCommonConfig, "ExplodeExpectNum", ref Main.s_Instance.m_fExpectExplodeNum)) {
			Main.s_Instance.m_fExpectExplodeNum = 8f;
		}
		_inputfieldExplodeExpectNum.text = Main.s_Instance.m_fExpectExplodeNum.ToString( "f0" );
		//CalculateExplodeDirection ( (int)Main.s_Instance.m_fExpectExplodeNum );

		if (!GetValueByKey (m_dicCommonConfig, "ExplodeRunTime", ref Main.s_Instance.m_fExplodeRunTime)) {
			Main.s_Instance.m_fExplodeRunTime = 0.5f;
		}
		_inputfieldExplodeRunTime.text = Main.s_Instance.m_fExplodeRunTime.ToString( "f5" );





		//Main.s_Instance.m_fMaxBallNumPerPlayer = nodeCommon != null && nodeCommon.ChildNodes.Count > nIndex?float.Parse( nodeCommon.ChildNodes[nIndex++].InnerText ):64f;

		if (!GetValueByKey (m_dicCommonConfig, "MaxBallNumPerPlayer", ref Main.s_Instance.m_fMaxBallNumPerPlayer)) {
			Main.s_Instance.m_fMaxBallNumPerPlayer = 64f;
		}
		_inputfieldMaxBallNumPerPlayer.text = Main.s_Instance.m_fMaxBallNumPerPlayer.ToString( "f5" );

        //Main.s_Instance.m_nSplitNum = nodeCommon != null && nodeCommon.ChildNodes.Count > nIndex?int.Parse( nodeCommon.ChildNodes[nIndex++].InnerText ):10;
		if (!GetValueByKey (m_dicCommonConfig, "SplitNum", ref val)) {
			Main.s_Instance.m_nSplitNum = 10;
		} else {
			Main.s_Instance.m_nSplitNum = (int)val;
		}
		_inputfieldSplitNum.text = Main.s_Instance.m_nSplitNum.ToString( "f5" );
        
		//Main.s_Instance.m_fSplitRunTime = nodeCommon != null && nodeCommon.ChildNodes.Count > nIndex?float.Parse( nodeCommon.ChildNodes[nIndex++].InnerText ):2f;
		if (!GetValueByKey (m_dicCommonConfig, "SplitRunTime", ref Main.s_Instance.m_fSplitRunTime)) {
			Main.s_Instance.m_fSplitRunTime = 2f;
		}
		_inputfieldSplitRunTime.text = Main.s_Instance.m_fSplitRunTime.ToString( "f5" );
        
		//Main.s_Instance.m_fSplitMaxDistance = nodeCommon != null && nodeCommon.ChildNodes.Count > nIndex?float.Parse( nodeCommon.ChildNodes[nIndex++].InnerText ):40f;
		if (!GetValueByKey (m_dicCommonConfig, "SplitMaxDistance", ref Main.s_Instance.m_fSplitMaxDistance)) {
			Main.s_Instance.m_fSplitMaxDistance = 40f;
		}
		_inputfieldSplitMaxDistance.text = Main.s_Instance.m_fSplitMaxDistance.ToString( "f5" );
        
		//Main.s_Instance.m_fSplitTimeInterval = nodeCommon != null && nodeCommon.ChildNodes.Count > nIndex?float.Parse( nodeCommon.ChildNodes[nIndex++].InnerText ):2f;
		if (!GetValueByKey (m_dicCommonConfig, "SplitTimeInterval", ref Main.s_Instance.m_fSplitTimeInterval)) {
			Main.s_Instance.m_fSplitTimeInterval = 2f;
		}
		_inputfieldSplitTimeInterval.text = Main.s_Instance.m_fSplitTimeInterval.ToString( "f5" );

        if (!GetValueByKey(m_dicCommonConfig, "TimeBeforeUiDead", ref Main.s_Instance.m_fTimeBeforeUiDead))
        {
            Main.s_Instance.m_fTimeBeforeUiDead = 3f;
        }
        _inputTimeBeforeShowDeadUI.text = Main.s_Instance.m_fTimeBeforeUiDead.ToString("f5");

        if (!GetValueByKey(m_dicCommonConfig, "BaseVolumeDecreasedPercentWhenDead", ref Main.s_Instance.m_fBaseVolumeyByItemDecreasedPercentWhenDead))
        {
            Main.s_Instance.m_fBaseVolumeyByItemDecreasedPercentWhenDead = -0.1f;
        }
        _inputBaseVolumeDecreasedPercentWhenDead.text = Main.s_Instance.m_fBaseVolumeyByItemDecreasedPercentWhenDead.ToString("f5");


        if (!GetValueByKey(m_dicCommonConfig, "TimeOfOneGame", ref Main.s_Instance.m_fTimeOfOneGame))
        {
            Main.s_Instance.m_fTimeOfOneGame = 300f;
        }
        _inputTimeTimeOfOneRound.text = Main.s_Instance.m_fTimeOfOneGame.ToString("f5");

        //Main.s_Instance.m_ShengFuPanDingMonster.SetSize( Main.s_Instance.m_fShengFuPanDingSize );

        /*
		string szContent = "0,0";
		if (!GetValueByKey (m_dicCommonConfig, "RebornSpot", ref Main.s_Instance.m_fSplitTimeInterval)) {
			szContent = "0,0";
		}
		*/

        UpdateWorldBorder();
        //m_fBornMinDiameter
        /*
        if (nodeCommon.ChildNodes.Count < 1)
        {
            return;
        }


        XmlNode node = nodeCommon.ChildNodes[0];
        string content = node.InnerText;
        string[] ary = content.Split(',');
        if (ary.Length < 2)
        {
            return;
        }

        m_fWorldWidth = float.Parse(ary[0]);
        m_fWorldHeight = float.Parse(ary[1]);
		m_fHalfWorldWidth = m_fWorldWidth / 2f;
		m_fHalfWorldHeight = m_fWorldHeight / 2f;
        _sliderWorldWidth.value = m_fWorldWidth;
        _sliderWorldHeight.value = m_fWorldHeight;

        UpdateWorldBorder();

        if (nodeCommon.ChildNodes.Count >= 2)
        {
            node = nodeCommon.ChildNodes[1];
            content = node.InnerText;
            ary = content.Split(',');
            for (int i = 0; i < ary.Length; i++)
            {
                if (ary[i] == "")
                {
                    continue;
                }
                if (i % 2 == 0)
                {
                    vecTempPos.x = float.Parse(ary[i]);
                }
                else
                {
                    vecTempPos.y = float.Parse(ary[i]);
                    vecTempPos.z = 0.0f;
                    //if (AccountManager.m_eSceneMode != AccountManager.eSceneMode.Game)
                    // {
                    AddRebornSpot(vecTempPos);
                    // }
                    m_lstRebornPos.Add(vecTempPos);
                }


            }
        }

        if (nodeCommon.ChildNodes.Count >= 3)
        {
            node = nodeCommon.ChildNodes[2];
            m_fThornAttenuateSpeed = float.Parse(node.InnerText);
            _sliderThornAttenuateSpeed.value = m_fThornAttenuateSpeed;
            _txtThornAttenuateSpeed.text = "刺的衰减速度(每秒)：\n" + ( m_fThornAttenuateSpeed);
        }

        if (nodeCommon.ChildNodes.Count >= 4)
        {
            node = nodeCommon.ChildNodes[3];
            m_fMaxCameraSize = float.Parse(node.InnerText);
            _inputfieldCamSizeMax.text = m_fMaxCameraSize.ToString();
        }

        if (nodeCommon.ChildNodes.Count >= 5)
        {
            node = nodeCommon.ChildNodes[4];
            m_fSeedGrassLifeTime = float.Parse(node.InnerText);
            _inputfieldSeedGrassLifeTime.text = m_fSeedGrassLifeTime.ToString();
        }

		if (nodeCommon.ChildNodes.Count >= 6) {
			node = nodeCommon.ChildNodes[5];
			ary = node.InnerText.Split( ',' );
			m_fYaQiuTiaoJian = float.Parse ( ary[0] );
			m_fYaQiuBaiFenBi = float.Parse ( ary[1] );
			_inputYaQiuTiaoJian.text = m_fYaQiuTiaoJian.ToString ();
			_inputYaQiuBaiFenBi.text = m_fYaQiuBaiFenBi.ToString ();
		}

		if (nodeCommon.ChildNodes.Count >= 7) {
			node = nodeCommon.ChildNodes [6];
			m_fXiQiuSuDu = float.Parse ( node.InnerText );
			_inputXiQiuSuDu.text = m_fXiQiuSuDu.ToString ();
		}
        */
    } // end generate common


    int m_nGrassGUID = 0;
    public void GenerateGrass( XmlNode nodeGrass )
	{
        if (nodeGrass == null)
        {
            return;
        }

        for (int i = 0; i < nodeGrass.ChildNodes.Count; i++)
        {
            XmlNode node = nodeGrass.ChildNodes[i];
            string key = node.InnerText;
            string[] ary = key.Split(',');
            string filename = ary[0];
            float posX = float.Parse(ary[1]);
            float posY = float.Parse(ary[2]);
            float scaleX = float.Parse(ary[3]);
            float scaleY = float.Parse(ary[4]);
            float rotationZ = float.Parse(ary[5]);
            bool bSeed = false;
            float fSeedGrassLifeTime = 0.0f;
            if (ary.Length > 6)
            {
                string szShit = ary[6];
                bSeed = bool.Parse(szShit/*ary[6]*/);
            }
			float fGrassLifeTime = float.Parse ( ary[7] );

  			// right here 1
			//string[] aryPointPos = PolygonEditor.LoadXML( PolygonEditor.GetPolygonFileName( filename ) );
            Polygon polygon = PolygonEditor.NewPolygon();
			//StartCoroutine ( LoadPolygonByXml( filename,polygon ) );
            polygon.m_szFileName = filename;
           // polygon.GeneratePolygon(aryPointPos, false);
			AddGrassToMap ( polygon );
            vecTempPos.x = posX;
            vecTempPos.y = posY;
            vecTempPos.z = -400.0f;
            polygon.transform.localPosition = vecTempPos;
            vecTemp.x = scaleX;
            vecTemp.y = scaleY;
            vecTemp.z = 1.0f;
            //polygon.transform.localScale = vecTemp;
            polygon.SetScale(vecTemp);
            polygon.transform.localRotation = Quaternion.identity;
            polygon.transform.Rotate(0.0f, 0.0f, rotationZ);
            polygon.m_fRotatoinZ = rotationZ;

            polygon.SetIsGrass(true);
            polygon.SetIsGrassSeed(bSeed);
			polygon.SetSeedGrassLifeTime ( fGrassLifeTime );

            polygon.SetGUID(m_nGrassGUID);
            m_dicGrass[m_nGrassGUID] = polygon;
            m_nGrassGUID++;
        } // end for
        
    }

	List<Polygon> m_lstGrass = new List<Polygon> ();
	public void AddGrassToMap ( Polygon polygon )
	{
		polygon.transform.parent = m_goGrassContainer.transform;
		m_lstGrass.Add ( polygon );
	}

    public void GenerateBeanSpray(XmlNode nodeSpray)
    {
        if (nodeSpray == null)
        {
            return;
        }

        for (int i = 0; i < nodeSpray.ChildNodes.Count; i++)
		{
			Spray spray = AddOneBeanSpray();
            XmlNode node = nodeSpray.ChildNodes[i];
            string key = node.InnerText;
            string[] ary = key.Split(',');
            vecTempPos.x = float.Parse(ary[0]);
            vecTempPos.y = float.Parse(ary[1]);
			vecTempPos.z = 0.0f;
			spray.SetGUID ( i );
			spray.transform.localPosition = vecTempPos;
			spray.SetDensity (float.Parse(ary[2]));	_inputfieldDensity.text = spray.GetDensity ().ToString ("f5");
			spray.SetMeatDensit (float.Parse(ary [3])); _inputfieldMeatDensity.text = spray.GetMeatDensit ().ToString ("f5");	
			spray.SetMaxDis (float.Parse(ary[4]));_inputfieldMaxDis.text = spray.GetMaxDis ().ToString ("f5");	
			spray.SetMinDis (float.Parse(ary[5]));	_inputfieldMinDis.text = spray.GetMinDis ().ToString ("f5");
			spray.SetBeanLifeTime (float.Parse(ary[6]));	_inputfieldBeanLifeTime.text = spray.GetBeanLifeTime ().ToString ("f5");
			spray.SetMeat2BeanSprayTime (float.Parse(ary[7]));	_inputfieldMeat2BeanSprayTime.text = spray.GetMeat2BeanSprayTime ().ToString ("f5");
			spray.SetSpraySize (float.Parse(ary[8]));	_inputfieldSpraySize.text = spray.GetSpraySize ().ToString ("f5");
			spray.SetDrawSpeed (float.Parse(ary[9]));	_inputfieldDrawSpeed.text = spray.GetDrawSpeed ().ToString ("f5");
			spray.SetDrawArea (float.Parse(ary[10]));	_inputfieldDrawArea.text = spray.GetDrawArea ().ToString ("f5");
			spray.SetPreDrawTime (float.Parse(ary[11]));	_inputfieldPreDrawTime.text = spray.GetPreDrawTime ().ToString ("f5");
			spray.SetDrawTime (float.Parse(ary[12]));	_inputfieldDrawTime.text = spray.GetDrawTime ().ToString ("f5");
			spray.SetDrawInterval ( float.Parse(ary[13]) );	_inputfieldDrawInterval.text = spray.GetDrawInterval ().ToString ("f5");
			spray.SetThornChance (int.Parse(ary[14]));	_inputfieldThornChance.text = spray.GetThornChance ().ToString ("f5");
			spray.SetSprayIntrerval (float.Parse(ary[15]));	_inputfieldSprayInterval.text = spray.GetSprayInterval ().ToString ("f5");

        }

        /*
		if (AccountManager.m_eSceneMode == AccountManager.eSceneMode.Game) {
			if (PhotonNetwork.isMasterClient) { // 所有场景物件的事件都由MasterClient统一发起（如果某MasterClient下线了，系统会自动移交MasterClient身份给另一个玩家）
				Main.s_Instance.m_MainPlayer.BeginBeanSprayPreDraw ( true );
			}
		}
        */
    }

	public void SyncSprayInfo(int nSprayGUID, int nDrawStatus, float fParam1, float fParam2, float fParam3)
	{
		if (nSprayGUID >= m_lstSprays.Count) {
			Debug.LogError ( "spray guid error" );
			return;
		}

		Spray spray = m_lstSprays [nSprayGUID];
		if (spray == null) {
			return;
		}

		spray.SetDrawStatus ( nDrawStatus, fParam1, fParam2, fParam3 );
	}

	public List<Spray> GetSprayList()
	{
		return m_lstSprays;
	}

    public void GenerateLaBaba(XmlNode nodeLababa)
    {
        if (nodeLababa == null)
        {
            Debug.LogError("Bug! nodeLababa == null");
            return;
        }

        for (int i = 0; i < nodeLababa.ChildNodes.Count; i++)
        {
            XmlNode node = nodeLababa.ChildNodes[i];
            switch (node.Name)
            {
                case  "LababaAreaThreshold":
                    {
                        m_inputfieldLababaAreaThreshold.text = node.InnerText;
                    }
                    break;
                case "LaBabaRunDistance":
                    {
                        m_inputfieldLababaRunDistance.text = node.InnerText;
                    }
                    break;
                case "LaBabaRunTime":
                    {
                        m_inputfieldLababaRunTime.text = node.InnerText;
                    }
                    break;
                case "LababaLiveTime":
                    {
                        m_inputfieldLababaLiveTime.text = node.InnerText;
                    }
                    break;
                case "LababaColdDown":
                    {
                        m_inputfieldLababaColdDown.text = node.InnerText;
                    }
                    break;
                case "LababaSizeCostPercent":
                    {
                        m_inputfieldLababaAreaCostPercent.text = node.InnerText;
                    }
                    break;

            }
            // end switch

        }

    }

    public void GenerateColorThorn(XmlNode nodeThorn)
    {
        if (nodeThorn == null)
        {
            return;
        }

        if (nodeThorn.ChildNodes.Count < 0)
        {
            return;
        }
		/* 
        for (int i = 0; i < Thorn.c_aryThornColor.Length; i++)
        {
            XmlNode node = nodeThorn.ChildNodes[i];
            string key = node.InnerText;
            string[] ary = key.Split(',');
            float fThornSize = float.Parse(ary[0]);
            float fBallSize  = float.Parse(ary[1]);
            Thorn.ColorThornParam param;
            param.thorn_size = fThornSize;
            param.ball_size = fBallSize;  
            m_dicColorThornParam[i] = param;
            //            AddOneBeanSpray(vecTempPos, fDensity, fMaxDis, fBeanLifeTime, nThornChance);
        }
		*/
		string szParams = nodeThorn.ChildNodes [0].InnerText;
		string[] aryParams = szParams.Split ('|');
		for (int i = 0; i < aryParams.Length; i++) {
			if (aryParams [i].Length == 0) {
				continue;
			}
			string[] arySubParams = aryParams [i].Split ( ',' );
			Thorn.ColorThornParam param;
			int nKey = int.Parse( arySubParams[0] );
			param.thorn_size = float.Parse( arySubParams[1] );
			param.ball_size = float.Parse( arySubParams[2] );  
			param.reborn_time = float.Parse( arySubParams[3] );  
			m_dicColorThornParam[nKey] = param;
		}
		UpdateThornColor(0);

        for ( int i = 1; i < nodeThorn.ChildNodes.Count; i++ )
        {
            XmlNode node = nodeThorn.ChildNodes[i];
            string key = node.InnerText;
            string[] ary = key.Split(',');
            vecTempPos.x = float.Parse(ary[0]);
            vecTempPos.y = float.Parse(ary[1]);
            int nColorIdx = int.Parse(ary[2]);
            AddOneThorn(vecTempPos, nColorIdx, true); 
        }
    }

	IEnumerator LoadPolygonByXml(bool bPolygonEditor,
                                                     string szPolygonName,
                                                      Polygon polygon,
                                                      float fPosX,
                                                      float fPosY,
                                                      float fScaleX,
                                                      float fScaleY,
                                                      float fRotation,
                                                      int    nHardObstacle,
                                                      bool  bIsGrass,
                                                      bool  bIsGrassSeed
                                                      )
	{
        polygon.m_szFileName = szPolygonName;

        string szFileName = AccountManager.url + AccountManager.GetPrefix(2) + szPolygonName + ".xml";
		WWW www = new WWW ( szFileName );
		yield return www;
		XmlNode root = null;
		XmlDocument myXmlDoc = StringManager.CreateXmlByText ( www.text, ref root );         
		string[] aryPointPos = new string[root.ChildNodes.Count];
		for (int i = 0; i < root.ChildNodes.Count; i++)
		{
			XmlNode node = root.ChildNodes[i];
			aryPointPos[i] = node.InnerText;
		}
		polygon.GeneratePolygon ( aryPointPos );
     
        polygon.transform.parent = m_goPolygonContainer.transform;
        vecTempPos.x = fPosX;
        vecTempPos.y = fPosY;
        vecTempPos.z = 0.0f;
        polygon.transform.localPosition = vecTempPos;
        vecTemp.x = fScaleX;
        vecTemp.y = fScaleY;
        vecTemp.z = 1.0f;
        polygon.transform.localScale = vecTemp;
        polygon.transform.localRotation = Quaternion.identity;
        polygon.transform.Rotate(0.0f, 0.0f, fRotation);
        polygon.m_fRotatoinZ = fRotation;
        polygon.SetIsGrass(bIsGrass);
        polygon.SetHardObstacle( nHardObstacle );
        polygon.SetIsGrassSeed (bIsGrassSeed);

    }

    public void GeneratePolygon( XmlNode nodePolygon )
    {
        if (nodePolygon == null)
        {
            return;
        }

        for (int i = 0; i < nodePolygon.ChildNodes.Count; i++)
        {
            XmlNode node = nodePolygon.ChildNodes[i];
            string key = node.InnerText;
            string[] ary = key.Split(',');
            string filename = ary[0];
            float posX = float.Parse(ary[1]);
            float posY = float.Parse(ary[2]);
            float scaleX = float.Parse(ary[3]);
            float scaleY = float.Parse(ary[4]);
            float rotationZ = float.Parse(ary[5]);
			int nHardObstacle = int.Parse (ary[6]);

			//string[] aryPointPos = PolygonEditor.LoadXML( PolygonEditor.GetPolygonFileName( filename )  );
            Polygon polygon = PolygonEditor.NewPolygon();
            polygon.m_szFileName = filename;
            StartCoroutine (LoadPolygonByXml(
                                                                    true,
                                                                    filename,
                                                                    polygon,
                                                                    posX,
                                                                    posY,
                                                                    scaleX,
                                                                    scaleY,
                                                                    rotationZ,
                                                                    nHardObstacle,
                                                                    false,
                                                                    false
                                                                   )
                                   ); 
            /*
            polygon.transform.parent = m_goPolygonContainer.transform;
            vecTempPos.x = posX;
            vecTempPos.y = posY;
            vecTempPos.z = 0.0f;
            polygon.transform.localPosition = vecTempPos;
            vecTemp.x = scaleX;
            vecTemp.y = scaleY;
            vecTemp.z = 1.0f;
            polygon.transform.localScale = vecTemp;
            polygon.transform.localRotation = Quaternion.identity;
            polygon.transform.Rotate(0.0f, 0.0f, rotationZ);
            polygon.m_fRotatoinZ = rotationZ;
            polygon.SetIsGrass(false);
			polygon.SetHardObstacle( nHardObstacle );
			polygon.SetIsGrassSeed (false);
            */
        } // end for
    }

    public  void LoadMap( string szRoomName )
    {
        // poppin test
        szRoomName = "hgr";
        if ( CSelectRoomManager.s_bEnterTestRoom )
        {
            szRoomName = "test3";
        }
        

        // poppin test for xml
        string szFileName = AccountManager.url + AccountManager.GetPrefix( 1 ) + szRoomName + ".xml";
        //string szFileName = Application.streamingAssetsPath + "/map001.xml"; ;
        StartCoroutine( GenerateMap(szFileName));
        //  GenerateMap(szFileName);
    }

    static XmlDocument s_xmldocMapData = null;

    // poppin to youhua 这里可以搞个预加载，这样进游戏之后场景可以秒展现，没有卡顿感 。
        IEnumerator GenerateMap( string szFileName )
       {

        Debug.Log( "到底是哪个：" + szFileName);
        WWW www = new WWW( szFileName );
        yield return www; // 等待下载
        XmlNode root = null;
        XmlDocument myXmlDoc = StringManager.CreateXmlByText ( www.text, ref root );

/*
    void GenerateMap(string szFileName)
    { 
        XmlDocument myXmlDoc = new XmlDocument();
        myXmlDoc.Load(szFileName);



        XmlNode root = null;
        root = myXmlDoc.SelectSingleNode( "root" );
*/

        XmlNode nodeCommon = root.SelectSingleNode("common");
        GenerateCommon(nodeCommon);

        XmlNode nodeCommon2 = root.SelectSingleNode("common2");
        GenerateCommon2(nodeCommon2);
        /*
		XmlNode nodeBean = root.SelectSingleNode("Bean");
		_MonsterEditor.GenerateBean(nodeBean);
		*/

        // 记得先加载Thorn再加载Class，因为后者要用到前者的数据
        XmlNode nodeThorn = root.SelectSingleNode("Thorn");
        CMonsterEditor.s_Instance.GenerateThorn(nodeThorn);

        XmlNode nodeClass = root.SelectSingleNode("Class");
        CClassEditor.s_Instance.GenerateClass(nodeClass);

        XmlNode nodeMannualMonster = root.SelectSingleNode("MannualMonster");
        CClassEditor.s_Instance.GenerateMannualMonster(nodeMannualMonster);

        if (AccountManager.m_eSceneMode == AccountManager.eSceneMode.Game)
        {
            XmlNode nodeFakeRandom = root.SelectSingleNode("FakeRandomThornPos");
            CClassEditor.s_Instance.GenerateFakeRandonThorns(nodeFakeRandom);
        }
      
        XmlNode nodeGrow = root.SelectSingleNode("Grow");
        CGrowSystem.s_Instance.GenerateGrow(nodeGrow);

       // XmlNode nodePve = root.SelectSingleNode("Pve");
       // CPveEditor.s_Instance.GeneratePve(nodePve);

        XmlNode nodeItem = root.SelectSingleNode("ItemSystem");
        CItemSystem.s_Instance.GenerateItem(nodeItem);

        XmlNode nodeSkill = root.SelectSingleNode("Skill");
        CSkillSystem.s_Instance.GenerateSkillParamConfig(nodeSkill);

        XmlNode nodeSkillDesc = root.SelectSingleNode("SkillDesc");
        CSkillSystem.s_Instance.GenerateSkillDesc(nodeSkillDesc);
        
        XmlNode nodeGesture = root.SelectSingleNode("Gesture");
        CGestureManager.s_Instance.Load(nodeGesture);

        XmlNode nodeEatMode = root.SelectSingleNode("eatmode");
        GenerateEatMode(nodeEatMode);

        XmlNode nodePolygon = root.SelectSingleNode("polygon");
        GeneratePolygon(nodePolygon);

        XmlNode nodeGrass = root.SelectSingleNode("grass");
        GenerateGrass(nodeGrass);


        XmlNode nodeLababa = root.SelectSingleNode("Lababa");
        GenerateLaBaba(nodeLababa);

        XmlNode nodeSpray = root.SelectSingleNode("beanspray");
        GenerateBeanSpray(nodeSpray);

        XmlNode nodeDust = root.SelectSingleNode("StarDust");
        CStarDust.s_Instance.RecordDustNode(nodeDust);

        XmlNode nodeDeadReborn = root.SelectSingleNode("DeadReborn");
        CCameraManager.s_Instance.GenerateRebornSpotParam(nodeDeadReborn);
        
        GenerateRebornSporRanPos();

        XmlNode nodeControl = root.SelectSingleNode("Control");
        CControl.s_Instance.GenerateControl (nodeControl);




        m_bMapInitCompleted = true;

		DrawBgLine ();

        Main.s_Instance.DoSomeInitAfterConfigLoad();

        if (AccountManager.m_eSceneMode == AccountManager.eSceneMode.Game)
        {
            Main.s_Instance.Born();
        }

        
        if (CWarFog.s_Instance)
        {
            CWarFog.s_Instance.SetWorldInfo(CClassEditor.s_Instance.GetClassCircleRadius(), GetWorldSizeX(), GetWorldSizeY());
        }
    }



    

    public void SaveMap()
    {
       XmlDocument xmlDoc = new XmlDocument();

        //创建类型声明节点  
        XmlNode node = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", "");
        xmlDoc.AppendChild(node);
        //创建根节点  
        XmlNode root = xmlDoc.CreateElement("root");
        xmlDoc.AppendChild(root);

        //// !---- 保存Common信息
        XmlNode nodeCommon = xmlDoc.CreateElement("common");
        root.AppendChild(nodeCommon);
        SaveCommon( xmlDoc, nodeCommon);

		XmlNode nodeCommon2 = xmlDoc.CreateElement("common2");
		root.AppendChild(nodeCommon2);
		SaveCommon2( xmlDoc, nodeCommon2);

		//// !---- 保存Monster - Bean信息
		/// 
		/*
		XmlNode nodeBean = xmlDoc.CreateElement("Bean");
		root.AppendChild(nodeBean);
		_MonsterEditor.SaveBean ( xmlDoc, nodeBean);
		*/
		//// !---- 保存Monster - Thorn信息
		XmlNode nodeThorn = xmlDoc.CreateElement("Thorn");
		root.AppendChild(nodeThorn);
		CMonsterEditor.s_Instance.SaveThorn ( xmlDoc, nodeThorn);

		//// !---- 保存“阶级圈”相关数据
		XmlNode nodeClass = xmlDoc.CreateElement("Class");
		root.AppendChild(nodeClass);
		CClassEditor.s_Instance.SaveClass ( xmlDoc, nodeClass);

        //// ! ---- 保存“手工怪”相关数据
        XmlNode nodeMannualMonster = xmlDoc.CreateElement("MannualMonster");
        root.AppendChild(nodeMannualMonster);
        CClassEditor.s_Instance.SaveMannualMonster(xmlDoc, nodeMannualMonster);


        XmlNode nodeFakeRandomThornPos = xmlDoc.CreateElement("FakeRandomThornPos");
		root.AppendChild(nodeFakeRandomThornPos);
		CClassEditor.s_Instance.SaveFakeRandomThornPos ( xmlDoc, nodeFakeRandomThornPos);

        /*
		XmlNode nodeBuff = xmlDoc.CreateElement("Buff");
		root.AppendChild(nodeBuff);
		CBuffEditor.s_Instance.SaveBuff ( xmlDoc, nodeBuff);
        */

		XmlNode nodeGrow = xmlDoc.CreateElement("Grow");
		root.AppendChild(nodeGrow);
		CGrowSystem.s_Instance.SaveGrow ( xmlDoc, nodeGrow);

        /*
		XmlNode nodePve = xmlDoc.CreateElement("Pve");
		root.AppendChild(nodePve);
		CPveEditor.s_Instance.SavePve ( xmlDoc, nodePve);
        */

        XmlNode nodeItem = xmlDoc.CreateElement("ItemSystem");
        root.AppendChild(nodeItem);
       CItemSystem.s_Instance.SaveItem (xmlDoc, nodeItem);

        XmlNode nodeSkill = xmlDoc.CreateElement("Skill");
        root.AppendChild(nodeSkill);
        CSkillSystem.s_Instance.SaveSkillParamConfig(xmlDoc, nodeSkill);

        XmlNode nodeSkillDesc = xmlDoc.CreateElement("SkillDesc"); // 按理说SkillDesc就应该放在上面那个Skill节点里。但是CheckPoint版本在即，实在不敢去改已经稳定的模块。暂时这样加
        root.AppendChild(nodeSkillDesc);
        CSkillSystem.s_Instance.SaveSkillDesc(xmlDoc, nodeSkillDesc);

        XmlNode nodeGesture = xmlDoc.CreateElement("Gesture");
        root.AppendChild(nodeGesture);
        CGestureManager.s_Instance.Save(xmlDoc, nodeGesture);


        //// !---- 保存EatMode信息
        XmlNode nodeEatMode = xmlDoc.CreateElement("eatmode");
        root.AppendChild(nodeEatMode);
        SaveEatMode( xmlDoc, nodeEatMode);

		//// !---- 保存多边形的信息
		XmlNode nodePolygon = xmlDoc.CreateElement("polygon");
		root.AppendChild(nodePolygon);
		SavePolygons(xmlDoc, nodePolygon);

		//// !---- 保存草丛的信息
		XmlNode nodeGrass = xmlDoc.CreateElement("grass");
		root.AppendChild(nodeGrass);
        //SaveGrass( xmlDoc, nodeGrass );


        //// !---- 保存星尘信息
        XmlNode nodeDust = xmlDoc.CreateElement("StarDust");
        root.AppendChild(nodeDust);
        CStarDust.s_Instance.SaveDust(xmlDoc, nodeDust, CClassEditor.s_Instance.GetClassCircleRadius());


        //// !---- 保存死亡重生信息
        XmlNode nodeDeadReborn = xmlDoc.CreateElement("DeadReborn");
        root.AppendChild(nodeDeadReborn);
        CCameraManager.s_Instance.SaveDeadReborn(xmlDoc, nodeDeadReborn);

        //// !---- 保存Control信息
        XmlNode nodeControl = xmlDoc.CreateElement("Control");
        root.AppendChild(nodeControl);
        CControl.s_Instance.SaveControl(xmlDoc, nodeControl);


        //// !--- 保存彩色刺的信息
        /*
		XmlNode nodeThorn = xmlDoc.CreateElement("colorthorn");
		root.AppendChild(nodeThorn);
		SaveColorThorn(xmlDoc, nodeThorn);

		*/

        //// !--- 保存彩色刺的信息
        ///
        /*
		XmlNode nodeBeanSpray = xmlDoc.CreateElement("beanspray");
		root.AppendChild(nodeBeanSpray);
		SaveBeanSpray(xmlDoc, nodeBeanSpray);
		*/

        //// !--- 保存粑粑的信息
        XmlNode nodeLababa = xmlDoc.CreateElement("Lababa");
        root.AppendChild(nodeLababa);
        SaveLababa(xmlDoc, nodeLababa);


        // poppin test
        string szRoomName = "hgr";
        if (CSelectRoomManager.s_bEnterTestRoom)
        {
            szRoomName = "test3";
        }


        //xmlDoc.Save(Application.dataPath + "/StreamingAssets/MapData/map001.xml");
        //Upload ( "e:\\1.txt", "\\\\192.168.31.1\\tddownload" );    
        //AccountManager.SaveAndUploadMapFile( xmlDoc, m_szCurRoom );
        StartCoroutine( DoSaveMapXmlFile (szRoomName/*m_szCurRoom*/, xmlDoc.InnerXml ) );
		xmlDoc.Save(Application.dataPath + "/StreamingAssets/" + szRoomName + ".xml");
    }


	IEnumerator DoSaveMapXmlFile( string szRoomName, string szContent )
	{
		WWWForm wwwForm = new WWWForm ();
		wwwForm.AddField (  "name",  AccountManager.GetPrefix(1) + szRoomName + ".xml"  );  
		wwwForm.AddField (  "content",  szContent );
		wwwForm.AddBinaryData( "content", System.Text.Encoding.Default.GetBytes (szContent ), "Polygon_" + szRoomName, "xml" );
		WWW www = new WWW(AccountManager.url, wwwForm);
		yield return www;
		Debug.Log ( www.text );  
	}


	// \\192.168.31.1\共享\liruirui
	void Upload (string fileNamePath, string uriString)
	{
		string fileName = fileNamePath.Substring(fileNamePath.LastIndexOf("\\") + 1);
		string NewFileName = fileName;
		string fileNameExt = fileName.Substring(fileName.LastIndexOf(".") + 1);
		if (uriString.EndsWith("/") == false) uriString = uriString + "/";

		uriString = uriString + NewFileName;
      

		///  创建WebClient实例 
		WebClient myWebClient = new WebClient();
		myWebClient.Credentials = CredentialCache.DefaultCredentials;

	
			// 要上传的文件 
			FileStream fs = new FileStream(fileNamePath, FileMode.Open, FileAccess.Read);
			BinaryReader r = new BinaryReader(fs);
			byte[] postArray = r.ReadBytes((int)fs.Length);
			Stream postStream = myWebClient.OpenWrite(uriString, "PUT");

		
				// 使用UploadFile方法可以用下面的格式
				// myWebClient.UploadFile(uriString,"PUT",fileNamePath);

				if (postStream.CanWrite)
				{
					postStream.Write(postArray, 0, postArray.Length);
					postStream.Close();
					fs.Dispose();
				}
				else
				{
					postStream.Close();
					fs.Dispose();
				}

	}

	void DownLoad(string url, string path)
	{
        WebClient client = new WebClient();

        string URLAddress = url;

        string receivePath= path;

        client.DownloadFile(URLAddress, receivePath );
	}

	public void BtnClick_Load()
	{
        //DownLoad ("\\\\192.168.31.1\\tddownload/1.txt", "f:\\1.txt" );
	}


    public void LeaveGrass( Polygon grass )
    {
        grass.SetStatus( 0 );
    }

    public void EnterGrass(Polygon grass)
    {
        grass.SetStatus( 1 );
    }

    public Polygon GetGrassByGUID( int nGrassGUID )
    {
        Polygon grass = null;
        m_dicGrass.TryGetValue(nGrassGUID, out grass);
        return grass;
    }

    public void ProcessGrassSeed( int nOwnerId, int nGrassGUID, double dCurTime)
    {
        Polygon grass = GetGrassByGUID(nGrassGUID);
        if ( grass == null )
        {
            Debug.LogError( "通过GUID没有找到Grass" );
            return;
        }
        
        grass.SeedBecomeGrass(nOwnerId, dCurTime);
    }

    public void ExitMapEditor()
    {
        //SceneManager.LoadScene( "SelectCaster" );
        StartCoroutine("LoadScene", "SelectCaster");

    }

    //异步加载场景  
    IEnumerator LoadScene(string scene_name)
    {
        AccountData.s_Instance.asyncLoad = SceneManager.LoadSceneAsync(scene_name);

        while (!AccountData.s_Instance.asyncLoad.isDone)
        {
            yield return null;
        }


    }
    // 进入多边形编辑器
    public void EnterPolygonEditor()
    {
        SceneManager.LoadScene( "PolygonEditor" );
    }

    public void EnterGrassMode()
    {
       
    }

    void SetOp( eMapEditorOp op )
    {
        m_eOp = op;
    }

	public void OnSelected( int nIndex )
	{

	}

	List<string> m_lstPolygons = new List<string>();
	IEnumerator LoadPolygonList()
	{
		string szFileName =  AccountManager.url + "PolygonList.xml";
		XmlNode root = null;
		WWW www = new WWW ( szFileName );
		yield return www;
		XmlDocument XmlDoc = StringManager.CreateXmlByText( www.text, ref root );
		m_lstPolygons.Clear ();
		for (int i = 0; i < root.ChildNodes.Count; i++) {
			m_lstPolygons.Add ( root.ChildNodes[i].InnerText );
		}
		RefreshPolygonList ();
	}

	void RefreshPolygonList()
	{
		_PolygonList.ClearAll ();
		for (int i = 0; i < m_lstPolygons.Count; i++) {
			_PolygonList.AddItem ( m_lstPolygons[i] );
		}
		if (m_lstPolygons.Count > 0) {
			_PolygonList.SelectByIndex (0);
		}
	}

    public void PickPolygon( Polygon polygon )
    {
        /*
        m_CurSelectedPolygon = polygon;
        m_CurMovingPolygon = polygon;
        if (m_CurSelectedPolygon == null)
        {
            _txtPolygonTitle.text = "当前未选中多边形";
            return;
        }
        _txtPolygonTitle.text = "当前选中的多边形参数";
        _sliderScaleX.value = polygon.transform.localScale.x;
        _sliderScaleY.value = polygon.transform.localScale.y;
        _sliderRotation.value = polygon.m_fRotatoinZ ;
        */
    }

    public void PickObj( MapObj obj )
    {
        if (IsPointerOverUI())
        {
            return;
        }

        m_CurSelectedObj = obj;
        m_CurMovingObj = obj;
        switch( obj.m_eObjType )
        {
            case MapObj.eMapObjType.polygon:
                {
                //    _txtPolygonTitle.text = "当前选中的多边形参数";
                //    _sliderScaleX.value = obj.transform.localScale.x;
                //    _sliderScaleY.value = obj.transform.localScale.y;
                //    _sliderRotation.value = obj.m_fRotatoinZ;
				_inputPolygonScaleX.text = obj.transform.localScale.x.ToString();
				_inputPolygonScaleY.text = obj.transform.localScale.y.ToString();
				_inputPolygonRotation.text = obj.m_fRotatoinZ.ToString();
                    _toogleIsGrass.isOn = ((Polygon)obj).GetIsGrass();
                    _toogleIsGrassSeed.isOn = ((Polygon)obj).GetIsGrassSeed();
				_toogleIsHardObstacle.isOn = ((Polygon)obj).IsHardObstacle();
                }
                break;
            case MapObj.eMapObjType.bean_spray:
                {
                    Spray spray = (Spray)obj;
				if (spray == null) {
					return;
				}
                    _sliderBeanLifeTime.value = spray.GetBeanLifeTime();
                    _sliderDensity.value = spray.GetDensity();
                    _sliderMaxDis.value = spray.GetMaxDis();
                    _txtDensity.text = _sliderDensity.value.ToString("f5") + " 个/秒";
                    _txtMaxDis.text = _sliderMaxDis.value.ToString("f5") + " (世界坐标单位)";
                    _txtBeanLifeTime.text = _sliderBeanLifeTime.value.ToString("f5") + " 秒";

				_inputfieldMaxDis.text = spray.GetMaxDis ().ToString ( "f5" );
				_inputfieldMinDis.text = spray.GetMinDis().ToString ( "f5" );
				_inputfieldBeanLifeTime.text = spray.GetBeanLifeTime ().ToString ("f5");
				_inputfieldDensity.text = spray.GetDensity().ToString ("f5");
				_inputfieldMeatDensity.text = spray.GetMeatDensit().ToString ("f5");
				_inputfieldThornChance.text = spray.GetThornChance().ToString ("f5");
				_inputfieldMeat2BeanSprayTime.text = spray.GetMeat2BeanSprayTime().ToString ("f5");
				_inputfieldSpraySize.text = spray.GetSpraySize().ToString ("f5");
				_inputfieldDrawSpeed.text = spray.GetDrawSpeed().ToString ("f5");
				_inputfieldDrawArea.text = spray.GetDrawArea().ToString ("f5");
				_inputfieldDrawTime.text = spray.GetDrawTime().ToString ("f5");
				_inputfieldDrawInterval.text = spray.GetDrawInterval().ToString ("f5");
				_inputfieldSprayInterval.text = spray.GetSprayInterval().ToString ("f5");
					
                }
                break;
            case MapObj.eMapObjType.thorn:
                {
                    if ( m_toggleRemoveThorn.isOn )
                    {
                        RemoveCurSelectedThorn();
                    }
                }
                break;
        }
    }


    public void AddPolygon()
    {
		string szPolygonName = _PolygonList.GetCurSelectedItem ().GetContent ();
		string selectName = PolygonEditor.GetPolygonFileName( szPolygonName );
		//string[] aryPointPos = PolygonEditor.LoadXML(selectName);// right here 3
            Polygon polygon = PolygonEditor.NewPolygon();
        vecTempPos.x = Screen.width / 2.0f;
        vecTempPos.y = Screen.height / 2.0f;
        vecTempPos = Camera.main.ScreenToWorldPoint(vecTempPos);
        vecTempPos.z = 0.0f;
        StartCoroutine ( LoadPolygonByXml( 
                                                                true,
                                                                szPolygonName,
                                                                polygon,
                                                                vecTempPos.x,
                                                                vecTempPos.y,
                                                                1f,
                                                                1f,
                                                                0f,
                                                                0,
                                                                false,
                                                                false
                                                                )
                               );
      
            PickPolygon(polygon);
     }
    
    public static Vector3 GetScreenCenter()
    {
        s_vecTempPos.x = Screen.width / 2.0f;
        s_vecTempPos.y = Screen.height / 2.0f;
        s_vecTempPos = Camera.main.ScreenToWorldPoint(s_vecTempPos);
        s_vecTempPos.z = -10.0f;
        return s_vecTempPos;
    }

    public void OnSliderScaleXChanged()
    {
        if (m_CurSelectedObj == null)
        {
            return;
        }

        if (m_CurSelectedObj.m_eObjType == MapObj.eMapObjType.polygon)
        {
            vecTemp = m_CurSelectedObj.transform.localScale;
            vecTemp.x = _sliderScaleX.value;
            //m_CurSelectedObj.transform.localScale = vecTemp;
            ((Polygon)m_CurSelectedObj).SetScale(vecTemp);
        }
    }

	public void OnInputValueChanged_ScaleX()
	{
		if (m_CurSelectedObj == null)
		{
			return;
		}
		if (m_CurSelectedObj.m_eObjType == MapObj.eMapObjType.polygon)
		{
			float val = 1f;
			if (!float.TryParse (_inputPolygonScaleX.text, out val)) {
				val = 1f;
			}
			if (val <= 0) {
				val = 1f;
			}

			vecTemp = m_CurSelectedObj.transform.localScale;
			vecTemp.x = val;
			((Polygon)m_CurSelectedObj).SetScale(vecTemp);
		}
	}

	public void OnInputValueChanged_ScaleY()
	{
		if (m_CurSelectedObj == null)
		{
			return;
		}
		if (m_CurSelectedObj.m_eObjType == MapObj.eMapObjType.polygon)
		{
			float val = 1f;
			if (!float.TryParse (_inputPolygonScaleY.text, out val)) {
				val = 1f;
			}
			if (val <= 0) {
				val = 1f;
			}

			vecTemp = m_CurSelectedObj.transform.localScale;
			vecTemp.y = val;
			((Polygon)m_CurSelectedObj).SetScale(vecTemp);
		}
	}

    public void OnSliderScaleYChanged()
    {
        if (m_CurSelectedObj == null)
        {
            return;
        }

        if (m_CurSelectedObj.m_eObjType == MapObj.eMapObjType.polygon)
        {
            vecTemp = m_CurSelectedObj.transform.localScale;
            vecTemp.y = _sliderScaleY.value;
            //m_CurSelectedObj.transform.localScale = vecTemp;
            ((Polygon)m_CurSelectedObj).SetScale(vecTemp);
        }
    }

	public void OnInputValueChanged_Rotation()
	{
		if (m_CurSelectedObj == null)
		{
			return;
		}

		if (m_CurSelectedObj.m_eObjType == MapObj.eMapObjType.polygon)
		{
			float val = 0f;
			if (!float.TryParse (_inputPolygonRotation.text, out val)) {
				val = 0f;
			}
			if (val < 0) {
				val = 0f;
			}

			m_CurSelectedObj.transform.localRotation = Quaternion.identity;
			m_CurSelectedObj.transform.Rotate(0.0f, 0.0f, val);
			m_CurSelectedObj.m_fRotatoinZ = val;
		}
	}

    public void OnSliderRotationChanged()
    {
        if (m_CurSelectedObj == null)
        {
            return;
        }

        if (m_CurSelectedObj.m_eObjType == MapObj.eMapObjType.polygon)
        {
            m_CurSelectedObj.transform.localRotation = Quaternion.identity;
            m_CurSelectedObj.transform.Rotate(0.0f, 0.0f, _sliderRotation.value);
            m_CurSelectedObj.m_fRotatoinZ = _sliderRotation.value;
        }
    }


    public void RemoveCurSelectedPolygon()
    {
        if (m_CurSelectedObj == null)
        {
            return;
        }

        RemovePolygon((Polygon)m_CurSelectedObj);
        m_CurSelectedObj = null;
        m_CurMovingObj = null; ;
    }

    public static void RemovePolygon( Polygon polygon ) 
    {
        GameObject.Destroy( polygon.gameObject );
    }

    public void AddBeanSpray()
    {
        AddOneBeanSpray( );
        //_txtDensity.text = _sliderDensity.value.ToString("f5") + " 个/秒";
        //_txtMaxDis.text = _sliderMaxDis.value.ToString("f5") + " (世界坐标单位)";
        //_txtBeanLifeTime.text = _sliderBeanLifeTime.value.ToString("f5") + " 秒";
        //_inputfieldThornChance.text = m_nThornChance.ToString();

    }

	List<Spray> m_lstSprays = new List<Spray>();
    public Spray AddOneBeanSpray( /*Vector3 pos, float fDensity, float fMaxDis, float fBeanLifeTime, int nThornChance*/ )
    {
        GameObject goSpray = GameObject.Instantiate((GameObject)Resources.Load("MapEditor/Spray/preBeanSpray_001"));
        goSpray.transform.parent = m_goBeanSprayContainer.transform;
		goSpray.transform.localPosition = GetScreenCenter();
        Spray spray = goSpray.GetComponent<Spray>();
		spray.SetSpraySize (10f);
		m_lstSprays.Add (spray );
		/*
        spray.SetBeanLifeTime(fBeanLifeTime);
        spray.SetDensity(fDensity);
        spray.SetMaxDis(fMaxDis);
        spray.SetThornChance(nThornChance);
		*/
        return spray;
    }

    public void OnSliderValueChanged_BeanSprayDensity()
    {
        if ( m_CurSelectedObj == null )
        {
            return;
        }

        Spray spray = (Spray)m_CurSelectedObj;
        if (spray == null)
        {
            return;
        }

        spray.SetDensity( _sliderDensity.value );
        _txtDensity.text = _sliderDensity.value.ToString("f5") + " 个/秒";
    }

    public void OnSliderValueChanged_BeanSprayMaxDis()
    {
        if (m_CurSelectedObj == null)
        {
            return;
        }

        Spray spray = (Spray)m_CurSelectedObj;
        if (spray == null)
        {
            return;
        }

        spray.SetMaxDis( _sliderMaxDis.value );
        _txtMaxDis.text = _sliderMaxDis.value.ToString("f5") + " (世界坐标单位)";
    }

    public void OnSliderValueChanged_BeanSprayBeanLifeTime()
    {
        if (m_CurSelectedObj == null)
        {
            return;
        }

        Spray spray = (Spray)m_CurSelectedObj;
        if (spray == null)
        {
            return;
        }

        spray.SetBeanLifeTime( _sliderBeanLifeTime.value );
        _txtBeanLifeTime.text = _sliderBeanLifeTime.value.ToString("f5") + " 秒";
    }

    public bool m_bShowRealtimeBeanSpray = true;
    public void ToogleValueChanged_ShowRealtime()
    {
        m_bShowRealtimeBeanSpray = !m_bShowRealtimeBeanSpray;
        if (m_bShowRealtimeBeanSpray == false)
        {

        }
    }

    public void RemoveSelectedSpray()
    {
        if ( m_CurSelectedObj == null )
        {
            return;
        }

        Spray spray = (Spray)m_CurSelectedObj;
        if ( spray == null )
        {
            return;
        }

		m_lstSprays.Remove ( spray );
        GameObject.Destroy( spray.gameObject );
    }

    public void SetPolygonAsGrass()
    {
        if ( m_CurSelectedObj == null )
        {
            return;
        }

        Polygon polygon = (Polygon)m_CurSelectedObj;
        if ( polygon == null )
        {
            return;
        }

        polygon.SetIsGrass( _toogleIsGrass.isOn );
        if (_toogleIsGrass.isOn == false)
        {
            _toogleIsGrassSeed.isOn = false;
        }
    }

	public void OnToggleValueChanged_HardObstacle()
	{
		if ( m_CurSelectedObj == null )
		{
			return;
		}	
		Polygon polygon = (Polygon)m_CurSelectedObj;
		if (polygon == null)
		{
			return;
		}
		polygon.SetHardObstacle ( _toogleIsHardObstacle.isOn?1:0 ); 
	}

    public void OnToggleValueChanged_GrassSeed()
    {
        if ( AccountManager.m_eSceneMode == AccountManager.eSceneMode.Game )
        {
            return;
        }

        if ( m_CurSelectedObj == null )
        {
            return;
        }
        Polygon polygon = (Polygon)m_CurSelectedObj;
        if (polygon == null)
        {
            return;
        }
        if ( !polygon.GetIsGrass() )
        {
            _toogleIsGrassSeed.isOn = false; // 如果根本不是草丛，则无法选择种子模式
            return;
        }

        polygon.SetIsGrassSeed(_toogleIsGrassSeed.isOn);    
    }


    public float GetWorldLeft()
    {
        return m_fWorldLeft;
    }

    public float GetWorldRight()
    {
        return m_fWorldRight;
    }

    void UpdateWorldBorder()
    {
        m_fHalfWorldWidth = m_fWorldSizeX / 2f;
        m_fHalfWorldHeight = m_fWorldSizeY / 2f;
		m_goWorldBorderTop.SetSize(m_fWorldSizeX, m_fWorldSizeY, 0);
		m_goWorldBorderBottom.SetSize(m_fWorldSizeX, m_fWorldSizeY, 1);
		m_goWorldBorderLeft.SetSize(m_fWorldSizeX, m_fWorldSizeY, 2);
		m_goWorldBorderRight.SetSize(m_fWorldSizeX, m_fWorldSizeY, 3);
		Main.s_Instance.UpdateWorldSize( m_fWorldSizeX, m_fWorldSizeY );
        // UpdateDaTaoShaCircle ( m_fWorldSize );

        CClassEditor.s_Instance.RefreshMonsterNum();
    }

	float m_fDaTaoShaSize = 1.0f;
   

    // 添加重生点
    public void AddRebornSpot()
    {
        AddRebornSpot( Vector3.zero );
    }

    void AddRebornSpot( Vector3 pos )
    {
        /*
        pos.z = -300f;
        GameObject goSpot = GameObject.Instantiate((GameObject)Resources.Load("MapEditor/preRebornSpot"));
        goSpot.transform.parent = m_goRebornSpotContainer.transform;
        goSpot.transform.position = pos;

        */
    }

    // 删除当前选中的重生点
    public void RemoveCurSelectedRebornSpot()
    {
        if ( m_CurSelectedObj && m_CurSelectedObj.m_eObjType == MapObj.eMapObjType.reborn_spot)
        {
            GameObject.Destroy(m_CurSelectedObj.gameObject);
        }
    }

    public Vector3 RandomGetRebornPos()
    {
        if ( m_lstRebornPos.Count < 1 )
        {
            return Vector3.zero;
        }
        int nIndex =(int)UnityEngine.Random.Range(0.0f, m_lstRebornPos.Count);
        if (Main.s_Instance.m_nRebornPosIdx != -1 )
        {
            nIndex = Main.s_Instance.m_nRebornPosIdx;
        }
        return m_lstRebornPos[nIndex];

    }

    float m_fThornAttenuateSpeed = 0.01f;
    public float GetThornAttenuateSpeed()
    {
        return m_fThornAttenuateSpeed;
    }

	/// <summary>
	/// / Bean Spray 
	/// </summary>
    public void OnInputFieldContentChanged_ThornChance()
    {
        if (!int.TryParse(_inputfieldThornChance.text, out m_nThornChance))
        {
            m_nThornChance = 0;
        }

        if ( m_CurSelectedObj )
        {
            Spray spray = (Spray)m_CurSelectedObj;
            if ( spray )
            {
                spray.SetThornChance(m_nThornChance);
            }
        }
    }

	public void OnInputFieldContentChanged_SprayInterval()
	{
		float val = 0f;
		if (!float.TryParse(_inputfieldSprayInterval.text, out val))
		{
			val = 0;
		}

		if ( m_CurSelectedObj )
		{
			Spray spray = (Spray)m_CurSelectedObj;
			if ( spray )
			{
				spray.SetSprayIntrerval(val);
			}
		}
	}

	public void OnInputFieldContentChanged_Density()
	{
		float val = 0f;
		if (!float.TryParse(_inputfieldDensity.text, out val))
		{
			val = 0;
		}

		if ( m_CurSelectedObj )
		{
			Spray spray = (Spray)m_CurSelectedObj;
			if ( spray )
			{
				spray.SetDensity (val);
			}
		}
	}

	public void OnInputFieldContentChanged_MeatDensity()
	{
		float val = 0f;
		if (!float.TryParse(_inputfieldMeatDensity.text, out val))
		{
			val = 0;
		}

		if ( m_CurSelectedObj )
		{
			Spray spray = (Spray)m_CurSelectedObj;
			if ( spray )
			{
				spray.SetMeatDensit(val);
			}
		}
	}

	public void OnInputFieldContentChanged_MaxDis()
	{
		float val = 0f;
		if (!float.TryParse(_inputfieldMaxDis.text, out val))
		{
			val = 0;
		}

		if ( m_CurSelectedObj )
		{
			Spray spray = (Spray)m_CurSelectedObj;
			if ( spray )
			{
				spray.SetMaxDis (val);
			}
		}
	}

	public void OnInputFieldContentChanged_MinDis()
	{
		float val = 0f;
		if (!float.TryParse(_inputfieldMinDis.text, out val))
		{
			val = 0;
		}

		if ( m_CurSelectedObj )
		{
			Spray spray = (Spray)m_CurSelectedObj;
			if ( spray )
			{
				spray.SetMinDis (val);
			}
		}
	}

	public void OnInputFieldContentChanged_BeanLifeTime()
	{
		float val = 0f;
		if (!float.TryParse(_inputfieldBeanLifeTime.text, out val))
		{
			val = 0;
		}

		if ( m_CurSelectedObj )
		{
			Spray spray = (Spray)m_CurSelectedObj;
			if ( spray )
			{
				spray.SetBeanLifeTime (val);
			}
		}
	}

	public void OnInputFieldContentChanged_Meat2BeanSprayTime()
	{
		float val = 0f;
		if (!float.TryParse(_inputfieldMeat2BeanSprayTime.text, out val))
		{
			val = 0;
		}

		if ( m_CurSelectedObj )
		{
			Spray spray = (Spray)m_CurSelectedObj;
			if ( spray )
			{
				spray.SetMeat2BeanSprayTime (val);
			}
		}
	}

	public void OnInputFieldContentChanged_SpraySize()
	{
		float val = 0f;
		if (!float.TryParse(_inputfieldSpraySize.text, out val))
		{
			val = 0;
		}

		if ( m_CurSelectedObj )
		{
			Spray spray = (Spray)m_CurSelectedObj;
			if ( spray )
			{
				spray.SetSpraySize (val);
			}
		}
	}

	public void OnInputFieldContentChanged_DrawSpeed()
	{
		float val = 0f;
		if (!float.TryParse(_inputfieldDrawSpeed.text, out val))
		{
			val = 0;
		}

		if ( m_CurSelectedObj )
		{
			Spray spray = (Spray)m_CurSelectedObj;
			if ( spray )
			{
				spray.SetDrawSpeed (val);
			}
		}
	}

	public void OnInputFieldContentChanged_DrawArea()
	{
		float val = 0f;
		if (!float.TryParse(_inputfieldDrawArea.text, out val))
		{
			val = 0;
		}

		if ( m_CurSelectedObj )
		{
			Spray spray = (Spray)m_CurSelectedObj;
			if ( spray )
			{
				spray.SetDrawArea (val);
			}
		}
	}

	public void OnInputFieldContentChanged_PreDrawTime()
	{
		float val = 0f;
		if (!float.TryParse(_inputfieldPreDrawTime.text, out val))
		{
			val = 0;
		}

		if ( m_CurSelectedObj )
		{
			Spray spray = (Spray)m_CurSelectedObj;
			if ( spray )
			{
				spray.SetPreDrawTime (val);
			}
		}
	}

	public void OnInputFieldContentChanged_DrawInterval()
	{
		float val = 0f;
		if (!float.TryParse(_inputfieldDrawInterval.text, out val))
		{
			val = 0;
		}

		if ( m_CurSelectedObj )
		{
			Spray spray = (Spray)m_CurSelectedObj;
			if ( spray )
			{
				spray.SetDrawInterval (val);
			}
		}
	}


	public void OnInputFieldContentChanged_DrawTime()
	{
		float val = 0f;
		if (!float.TryParse(_inputfieldDrawTime.text, out val))
		{
			val = 0;
		}

		if ( m_CurSelectedObj )
		{
			Spray spray = (Spray)m_CurSelectedObj;
			if ( spray )
			{
				spray.SetDrawTime (val);
			}
		}
	}



	/// <summary>
	/// / end Bean Spray
	/// </summary>
	/// <returns>The max camera size.</returns>

    public float GetMaxCameraSize()
    {
        return m_fMaxCameraSize;
    }

    public float GetMinCameraSize()
    {
        return m_fMinCameraSize;
    }

    public void OnSliderValueChanged_ReferBall()
    {
        /*
        float fSize = _sliderReferBall.value;
        if ( fSize < 0.0f )
        {
            fSize = 0.0f;
        }
        m_ReferBall.Local_SetSize( fSize );
        */
    }

    public void OnInputFieldValueChanged_SeedGrassLifeTime()
    {
		if (m_CurSelectedObj == null || m_CurSelectedObj.m_eObjType != MapObj.eMapObjType.polygon)
		{
			return;
		}

        float val = 0.0f;
        if ( !float.TryParse( _inputfieldSeedGrassLifeTime.text, out val  ) )
        {
            return;
        }

        if ( val < 0.0f )
        {
            return;
        }

		((Polygon)m_CurSelectedObj).SetSeedGrassLifeTime( val );
    }

	public float GetWorldArea()
	{
		return m_fWorldSizeX * m_fWorldSizeY;
	}

	public float GetWorldSizeX()
	{
		return m_fWorldSizeX;
	}

    public float GetWorldSize()
    {
        return m_fWorldSizeX > m_fWorldSizeY ? m_fWorldSizeX: m_fWorldSizeY;
    }



    public Vector3 GetArenaLeftBottom_Screen()
    {
        vecTempPos.x = -m_fHalfWorldWidth;
        vecTempPos.y = -m_fHalfWorldHeight;
        vecTempPos.z = 0f;

        return Camera.main.WorldToScreenPoint(vecTempPos);
    }

    public Vector3 GetArenaRightTop_Screen()
    {
        vecTempPos.x = m_fHalfWorldWidth;
        vecTempPos.y = m_fHalfWorldHeight;
        vecTempPos.z = 0f;

        return Camera.main.WorldToScreenPoint(vecTempPos);
    }

    public struct sRebornRanPos
    {
        public Vector3 vecMinPos;
        public Vector3 vecMaxPos;
    };
    List<sRebornRanPos> m_lstRebornRandomPos = new List<sRebornRanPos>();
    Vector3 m_vecRebornSpotRandomPos = new Vector3();
    public void GenerateRebornSporPos()
    {
        int nIndex = UnityEngine.Random.Range(0, 4);
        sRebornRanPos pos = m_lstRebornRandomPos[nIndex];
        m_vecRebornSpotRandomPos.x = (float)UnityEngine.Random.Range( pos.vecMinPos.x, pos.vecMaxPos.x);
        m_vecRebornSpotRandomPos.y = (float)UnityEngine.Random.Range(pos.vecMinPos.y, pos.vecMaxPos.y);
        m_vecRebornSpotRandomPos.z = -11f;
    }

    public Vector3 GetRebornSpotRandomPos()
    {
        if (CCheat.s_Instance._toggleFixedRebornPos.isOn)
        {
            return new Vector3(0, 400, 0f);
        }
        else
        {
            return m_vecRebornSpotRandomPos;
        }
    }

    public float GetWorldSizeY()
	{
		return m_fWorldSizeY;
	}

    public float GetWorldWidth()
    {
        return m_fWorldWidth;
    }

    public float GetWorldHeight()
    {
        return m_fWorldHeight;
    }


  
	public void OnDropdownValuedChanged_BeiShuDouZiType()
	{
		SwitchBeiShuDouZiType ( _dropdownFoodType.value );
	}

	public void SwitchBeiShuDouZiType( int nType )
	{
		if (nType >= m_lstBeiShuDouZi.Count) {
			return;
		}

		m_CurEditingBeiShuDouZiConfig = m_lstBeiShuDouZi [nType];
		_inputNum.text = m_CurEditingBeiShuDouZiConfig.num.ToString();
		_inputValue.text = m_CurEditingBeiShuDouZiConfig.value.ToString ( "f5" );
	}

	public void OnInputFieldValueChanged_BeiShuDouZi_Value()
	{
		m_CurEditingBeiShuDouZiConfig.value = float.Parse( _inputValue.text );
	}


	public void OnInputFieldValueChanged_BeiShuDouZi_Num()
	{
		m_CurEditingBeiShuDouZiConfig.num = int.Parse( _inputNum.text );
	}

	public void OnInputFieldValueChanged_BeiShuDouZi_Desc()
	{
		m_CurEditingBeiShuDouZiConfig.desc = _inputDesc.text;
	}






	public List<sBeiShuDouZiConfig> GetBeiShuDouZiConfig()
	{
		return m_lstBeiShuDouZi;
	}

	public sBeiShuDouZiConfig GetFoodConfig( int nIndex, ref bool ret )
	{
		sBeiShuDouZiConfig config;
		if (nIndex < 0 || nIndex >= m_lstBeiShuDouZi.Count) {
			ret = false;
			config = new sBeiShuDouZiConfig();
			return config;
		}
		config = m_lstBeiShuDouZi[nIndex];
		ret = true;
		return config;
	}

    public void CheckIfCanCrossClassCircle(Ball ball, float fDeltaX, float fDeltaY, ref bool bCanMoveX, ref bool bCanMoveY)
    {
        vecTempPos = ball.GetPos();
        float fCurDisX = Mathf.Abs ( vecTempPos.x -  Main.s_Instance.m_vecClassCircleCenterPos.x );
        float fCurDisY = Mathf.Abs( vecTempPos.y - Main.s_Instance.m_vecClassCircleCenterPos.y   );
        vecTempPos.x += fDeltaX;
        vecTempPos.y += fDeltaY;
        float fNextDisX = Mathf.Abs( vecTempPos.x - Main.s_Instance.m_vecClassCircleCenterPos.x);
        float fNextDisY = Mathf.Abs(vecTempPos.y - Main.s_Instance.m_vecClassCircleCenterPos.y);
        if (fNextDisX > fCurDisX)
        {
            bCanMoveX = false;
        }
        if (fNextDisY > fCurDisY)
        {
            bCanMoveY = false;
        }

    }

    public bool IsMaxCameraSize()
    {
        return Camera.main.orthographicSize >= GetMaxCameraSize();
    }

    Vector3 vecScreenLeftBottom;
    Vector3 vecScreenRightTop;
    public void CheckIfWillExceedWorldBorder(Ball ball, float fDeltaX, float fDeltaY, ref bool bCanMoveX, ref bool bCanMoveY)
    {
        float fRadius = ball.GetRadius();

       // CCameraManager.s_Instance.GetScreenBorder_WorldCoordinate(ref vecScreenLeftBottom, ref vecScreenRightTop);

        //// 是否超过了场景的边界
        vecTempPos = ball.GetPos();
        vecTempPos.x += fDeltaX;
        vecTempPos.y += fDeltaY;
        if (fDeltaX > 0)
        {

            if (vecTempPos.x >= m_fHalfWorldWidth - fRadius)
            {
                bCanMoveX = false;
            }
        }
        else if (fDeltaX < 0)
        {

            if (vecTempPos.x <= -m_fHalfWorldWidth + fRadius)
            {
                bCanMoveX = false;
            }
        }


        if (fDeltaY > 0)
        {

            if (vecTempPos.y >= m_fHalfWorldHeight - fRadius)
            {
                bCanMoveY = false;
            }
        }
        else if (fDeltaY < 0)
        {

            if (vecTempPos.y <= -m_fHalfWorldHeight + fRadius)
            {
                bCanMoveY = false;
            }
        }
        //// end 是否超过了场景的边界 判断

   
    }

    public void ClampMoveToWorldBorder( float fRadius, float fCurPosX, float fCurPosY, float fDeltaX, float fDeltaY, ref float fRealNextX, ref float fRealNextY )
	 {
		float fExpectNextX = fCurPosX + fDeltaX;
		float fExpectNextY = fCurPosY + fDeltaY;
		fRealNextX = fCurPosX;
		fRealNextY = fCurPosY;
		if ( fDeltaX > 0 )
		{
			if ( fExpectNextX < m_fHalfWorldWidth - fRadius  )
			{
				fRealNextX = fExpectNextX;
			}
		}
		else if ( fDeltaX < 0 )
		{
			if ( fExpectNextX > -m_fHalfWorldWidth + fRadius  )
			{
				fRealNextX = fExpectNextX;
			}
		}

		if ( fDeltaY > 0 )
		{
			if ( fExpectNextY < m_fHalfWorldHeight - fRadius  )
			{
				fRealNextY = fExpectNextY;
			}
		}
		else if ( fDeltaY < 0 )
		{
			if ( fExpectNextY > -m_fHalfWorldHeight + fRadius  )
			{
				fRealNextY = fExpectNextY;
			}
		}
	}

	public bool CheckIfExceedWorldBorder( float val, int op )
	{
		switch (op) {
		case 0: // right
			{
				if (val > m_fHalfWorldWidth) {
					return true;
				}
			}
			break;
		case 1: // left
			{
				if (val < -m_fHalfWorldWidth) {
					return true;
				}
			}
			break;
		case 2: // top
			{
				if (val > m_fHalfWorldHeight) {
					return true;
				}
			}
			break;
		case 3: // bottom
			{
				if (val < -m_fHalfWorldHeight) {
					return true;
				}
			}
			break;

		} // end switch

		return false;
	}

    static string m_szCurRoom = "";
    public static void SetCurRoom( string szRoomName )
    {
        m_szCurRoom = szRoomName;
    }

	static string m_szMainPlayerName = "";
	public static void SetMainPlayerName( string szMainPlayerName )
	{
		m_szMainPlayerName = szMainPlayerName;
	}

	public static string GetMainPlayerName()
	{
		return m_szMainPlayerName;
	}

    public void Play()
    {
        AccountManager.s_Instance.PlayRoom(  );
    }

    //// !---- Common
    public void OnInputValueChanged_WorlsSizeX()
    {
        float val = 0;
        if (!float.TryParse(_inputWorldSizeX.text, out val))
        {
            return;
        }

        if (val <= 0)
        {
            return;
        }

        //m_fWorldSizeX = val;
        SetWorldSizeX( val );
        UpdateWorldBorder();
    }

	//// !---- Common
	public void OnInputValueChanged_WorlsSizeY()
	{
		float val = 0;
		if (!float.TryParse(_inputWorldSizeY.text, out val))
		{
			return;
		}

		if (val <= 0)
		{
			return;
		}

        //m_fWorldSizeY = val;
        SetWorldSizeY( val );

        UpdateWorldBorder();
	}

    float m_fWorldLeft = 0;
    float m_fWorldRight = 0;
    float m_fWorldTop = 0;
    float m_fWorldBottom = 0;
    void SetWorldSizeX( float val )
    {
        m_fWorldSizeX = val;
        m_fWorldLeft = -m_fWorldSizeX / 2f;
        m_fWorldRight = m_fWorldSizeX / 2f;
    }

    void SetWorldSizeY(float val)
    {
        m_fWorldSizeY = val;
        m_fWorldTop = m_fWorldSizeY / 2f;
        m_fWorldBottom = -m_fWorldSizeY / 2f;
    }


    public float GetWorldTop()
    {
        return m_fWorldTop;
    }

    public float GetWorldBottom()
    {
        return m_fWorldBottom;
    }

    public void OnInputValueChanged_CamSizeMax()
    {
        float val = 0;
        if (!float.TryParse(_inputfieldCamSizeMax.text, out val))
        {
            return;
        }

        if (val <= 0)
        {
            return;
        }

        m_fMaxCameraSize = val;
    }

    public void OnInputValueChanged_CamSizeMin()
    {
        float val = 30f;
        if (!float.TryParse(_inputfieldCamSizeMin.text, out val))
        {
            return;
        }

        if (val <= 0f)
        {
            return;
        }

        m_fMinCameraSize = val;
    }

    public void OnInputValueChanged_CamShangFuXiShu()
    {
        float val = 100f;
        if (!float.TryParse(_inputCamShangFuXiShu.text, out val))
        {
            return;
        }

        if (val <= 0f)
        {
            return;
        }

        Main.s_Instance.m_fShangFuXiShu = val;
    }

    /// <summary>
    ///  镜头抬升相关参数（其余的参数暂时废弃了）
    /// </summary>
    public void OnInputValueChanged_CamRaiseSpeed()
    {
        float val = 300f;
        if (!float.TryParse(_inputCamRaiseSpeed.text, out val))
        {
            return;
        }

        if (val <= 0f)
        {
            return;
        }

        Main.s_Instance.m_fRaiseSpeed = val;
    }


    public void OnInputValueChanged_CamRespondSpeed()
    {
        float val = 50f;
        if (!float.TryParse(_inputfieldCamRespondSpeed.text, out val))
        {
            return;
        }

        if (val <= 0f)
        {
            return;
        }

        Main.s_Instance.m_fCamRespondSpeed = val;
    }

    public void OnInputValueChanged_CamTimeBeforeDown()
    {
        float val = 0.1f;
        if (!float.TryParse(_inputCamTimeBeforeDown.text, out val))
        {
            return;
        }

        Main.s_Instance.m_fTimeBeforeCamDown = val;
    }
    

    public void OnInputValueChanged_CamSizeChangeDelaySpit()
    {
        float val = 0.5f;
        if (!float.TryParse( _inputCamChangeDelaySpit.text, out val))
        {
            return;
        }

        Main.s_Instance.m_fCamChangeDelaySpit = val;
    }

    public void OnInputValueChanged_CamSizeChangeDelayExplode()
    {
        float val = 0.5f;
        if (!float.TryParse(_inputCamChangeDelayExplode.text, out val))
        {
            return;
        }

        Main.s_Instance.m_fCamChangeDelayExplode = val;
    }

    public void OnInputValueChanged_CamShangFuAccelerate()
    {
        float val = 0.5f;
        if (!float.TryParse(_inputCamShangFuAccelerate.text, out val))
        {
            return;
        }

        Main.s_Instance.m_fCamRaiseAccelerate = val;
    }


    //// end 镜头抬升相关参数




    public void OnInputValueChanged_CamDownSpeed()
    {
        float val = 300f;
        if (!float.TryParse(_inputCamDownSpeed.text, out val))
        {
            return;
        }

        if (val <= 0f)
        {
            return;
        }

        Main.s_Instance.m_fDownSpeed = val;
    }



    public void OnInputValueChanged_CamSizeXiShu()
    {
        float val = 100f;
        if (!float.TryParse(_inputfieldCamSizeXiShu.text, out val))
        {
            return;
        }

        if (val <= 0f)
        {
            return;
        }

        Main.s_Instance.m_fCamSizeXiShu = val;
    }

    public void OnInputValueChanged_BornProtectTime()
    {
        float val = 0;
        if (!float.TryParse(_inputBornProtectTime.text, out val))
        {
            return;
        }

        Main.s_Instance.m_fBornProtectTime = val;
    }

    public void OnInputValueChanged_CamZoomSpeed()
    {
        float val = 0;
        if (!float.TryParse(_inputfieldCamZoomSpeed.text, out val))
        {
            return;
        }

        if (val <= 0)
        {
            return;
        }

        m_fCameraZoomSpeed = val;
		SetCamZoomTime ( m_fCameraZoomSpeed );
    }

	public void OnInputValueChanged_BallSizeAffectCamSize()
	{
		float val = 0;
		if (!float.TryParse(_inputfieldBallSizeAffectCamSize.text, out val))
		{
			return;
		}

		if (val <= 0)
		{
			return;
		}

		m_fBallSizeAffectCamSize = val; 
		SetBallSizeAffectCamSize ( m_fBallSizeAffectCamSize );
	}

    public void OnInputValueChanged_BeanNumPerRange()
    {
        float val = 0;
        if (!float.TryParse(_inputfieldBeanNumPerRange.text, out val))
        {
            return;
        }

        if (val < 0)
        {
            return;
        }

        Main.s_Instance.m_fBeanNumPerRange = val;
    }

    public void OnInputValueChanged_SpeedRadiusKaiFangFenMu()
    {
        float val = 0;
        if (!float.TryParse(_inputfieldSpeedRadiusKaiFangFenMu.text, out val))
        {
            return;
        }

        if (val <= 0)
        {
            return;
        }

        Main.s_Instance.m_fBallSpeedRadiusKaiFangFenMu = val;
    }

    public void OnInputValueChanged_MaxSpeed()
    {
        float val = 24;
        if (!float.TryParse(_inputfieldMaxSpeed.text, out val))
        {
            return;
        }

        if (val <= 0)
        {
            return;
        }

        Main.s_Instance.m_fMaxPlayerSpeed = val;
    }

    public void OnInputValueChanged_BallBaseSpeed()
    {
        float val = 0;
        if (!float.TryParse(_inputfieldBallBaseSpeed.text, out val))
        {
            return;
        }

        if (val <= 0)
        {
            return;
        }

        Main.s_Instance.m_fBallBaseSpeed = val;
    }

    public void OnInputValueChanged_BornMinDiameter()
    {
        float val = 0;
        if (!float.TryParse(_inputfieldBornMinDiameter.text, out val))
        {
            return;
        }

        if (val <= 0)
        {
            return;
        }

        Main.s_Instance.m_fBornMinTiJi = val;
    }

    // 我日你个龟
    public void OnInputValueChanged_ThornNumPerRange()
    {
        float val = 0;
        if (!float.TryParse(_inputfieldThornNumPerRange.text, out val))
        {
            return;
        }

        if (val < 0)
        {
            return;
        }

        Main.s_Instance.m_fThornNumPerRange = val;
    }

    public void OnInputValueChanged_GenerateNewBeanTimeInterval()
    {
        float val = 0;
        if (!float.TryParse(_inputfieldGenerateNewBeanTimeInterval.text, out val))
        {
            return;
        }

        if (val <= 0)
        {
            return;
        }

        Main.s_Instance.m_fGenerateNewBeanTimeInterval = val;
    }


    public void OnInputValueChanged_GenerateNewThornTimeInterval()
    {
        float val = 0;
        if (!float.TryParse(_inputfieldGenerateNewThornTimeInterval.text, out val))
        {
            return;
        }

        if (val <= 0)
        {
            return;
        }

        Main.s_Instance.m_fGenerateNewThornTimeInterval= val;
    }

    public void OnInputValueChanged_BeanSize()
    {
        float val = 0;
        if (!float.TryParse(_inputfieldBeanSize.text, out val))
        {
            return;
        }

        if (val < 0)
        {
            return;
        }

        Main.s_Instance.m_fBeanSize = val;
    }

    public void OnInputValueChanged_ThornSize()
    {
        float val = 0;
        if (!float.TryParse(_inputfieldThornSize.text, out val))
        {
            return;
        }

        if (val < 0)
        {
            return;
        }

        Main.s_Instance.m_fThornSize = val;
    }

    public void OnInputValueChanged_ThornContributeToBallSize()
    {
        float val = 0;
        if (!float.TryParse(_inputfieldThornContributeToBallSize.text, out val))
        {
            return;
        }

        if (val < 0)
        {
            return;
        }

        Main.s_Instance.m_fThornContributeToBallSize = val;
    }
		

    public void OnInputValueChanged_SpitRunTime()
    {
        float val = 0;
        if (!float.TryParse(_inputfieldSpitRunTime.text, out val))
        {
            return;
        }

        if (val <= 0)
        {
            return;
        }

        Main.s_Instance.m_fSpitRunTime = val;
    }

	public void OnInputValueChanged_SpitSporeRunDistance()
	{
		float val = 0;
		if (!float.TryParse(_inputfieldSpitSporeRunDistance.text, out val))
		{
			return;
		}

		if (val < 0)
		{
			return;
		}

		Main.s_Instance.m_fSpitSporeRunDistance= val;
	}

    public void OnInputValueChanged_SpitSporeBeanId()
    {
        Main.s_Instance.m_szSpitSporeBeanType = _inputfieldSpitSporeBeanType.text;
    }

    public void OnInputValueChanged_SpitSporeCostMP()
	{
		float val = 0;
		if (!float.TryParse(_inputfieldSpitSporeCostMP.text, out val))
		{
			return;
		}
			

		Main.s_Instance.m_fSpitSporeCostMP= val;
	}
	public void OnInputValueChanged_SpitBallRunTime()
	{
		float val = 0;
		if (!float.TryParse(_inputfieldSpitBallRunTime.text, out val))
		{
			return;
		}

		if (val <= 0)
		{
			return;
		}

		Main.s_Instance.m_fSpitBallRunTime = val;
	}

    public void OnInputValueChanged_SpitBallRunDistanceMultiple()
    {
        float val = 0;
        if (!float.TryParse(_inputfieldSpitBallRunDistanceMultiple.text, out val))
        {
            return;
        }

        if (val <= 0)
        {
            return;
        }

        Main.s_Instance.m_fSpitBallRunDistanceMultiple = val;
    }

	public void OnInputValueChanged_SpitBallCostMP()
	{
		float val = 0;
		if (!float.TryParse(_inputfieldSpitBallCostMP.text, out val))
		{
			return;
		}

		Main.s_Instance.m_fSpitBallCostMP = val;
	}

    public void OnInputValueChanged_SpitBallSpeed()
    {
        float val = 0;
        if (!float.TryParse(_inputfieldSpitBallSpeed.text, out val))
        {
            return;
        }

        Main.s_Instance.m_fSpitBallRunSpeed = val;
    }
    

    public void OnInputValueChanged_WSpitBallStayTime()
	{
		Main.s_Instance.m_fSpitBallStayTime = float.Parse ( _inputfieldSpitBallStayTime.text );
	}

    public void OnInputValueChanged_AutoAttenuate()
    {
        float val = 0;
        if (!float.TryParse(_inputfieldAutoAttenuate.text, out val))
        {
            return;
        }

        if (val < 0)
        {
            return;
        }

        Main.s_Instance.m_fAutoAttenuate = val;
    }

    public void OnInputValueChanged_ThornAttenuate()
    {
        float val = 0;
        if (!float.TryParse(_inputfieldThornAttenuate.text, out val))
        {
            return;
        }

        if (val < 0)
        {
            return;
        }

        Main.s_Instance.m_fThornAttenuate = val;
    }

    public void OnInputValueChanged_ForceSpitTotalTime()
    {
        float val = 0;
        if (!float.TryParse(_inputfieldForceSpitTotalTime.text, out val))
        {
            return;
        }

        if (val < 0)
        {
            return;
        }

        Main.s_Instance.m_fForceSpitTotalTime= val;
    }

    public void OnInputValueChanged_ShellShrinkTotalTime()
    {
        float val = 0;
        if (!float.TryParse(_inputfieldShellShrinkTotalTime.text, out val))
        {
            return;
        }

        if (val < 0)
        {
            return;
        }

        Main.s_Instance.m_fShellShrinkTotalTime = val;
    }

	public void OnInputValueChanged_SplitShellShrinkTotalTime()
	{
		float val = 0;
		if (!float.TryParse(_inputfieldSplitShellTotalTime.text, out val))
		{
			return;
		}

		if (val < 0)
		{
			return;
		}

		Main.s_Instance.m_fSplitShellShrinkTotalTime = val;
	}

	public void OnInputValueChanged_SplitCostMP()
	{
		float val = 0;
		if (!float.TryParse(_inputfieldSplitCostMP.text, out val))
		{
			return;
		}

		Main.s_Instance.m_fSplitCostMP = val;
	}

	public void OnInputValueChanged_SplitStayTime()
	{
		Main.s_Instance.m_fSplitStayTime = float.Parse( _inputfieldSplitStayTime.text );
	}


	public void OnInputValueChanged_ExplodeShellShrinkTotalTime()
	{
		float val = 0;
		if (!float.TryParse(_inputfieldExplodeShellTotalTime.text, out val))
		{
			return;
		}

		if (val < 0)
		{
			return;
		}

		Main.s_Instance.m_fExplodeShellShrinkTotalTime = val;
	}


    public void OnInputValueChanged_SpitBallTimeInterval()
    {
        float val = 0;
        if (!float.TryParse(_inputfieldSpitBallTimeInterval.text, out val))
        {
            return;
        }

        if (val < 0)
        {
            return;
        }

        Main.s_Instance.m_fSpitBallTimeInterval = val;
    }

    public void OnInputValueChanged_SpitSporeTimeInterval()
    {
        float val = 0;
        if (!float.TryParse(_inputfieldSpitSporeTimeInterval.text, out val))
        {
            return;
        }

        if (val < 0)
        {
            return;
        }

        Main.s_Instance.m_fSpitSporeTimeInterval = val;
    }

    public void OnInputValueChanged_MinDiameterToEatThorn()
    {
        float val = 0;
        if (!float.TryParse(_inputfieldMinDiameterToEatThorn.text, out val))
        {
            return;
        }

        if (val <= 0)
        {
            return;
        }

        Main.s_Instance.m_fMinDiameterToEatThorn = val;
    }

    public void OnInputValueChanged_ExplodeRunDistanceMultiple()
    {
        float val = 0;
        if (!float.TryParse(_inputfieldExplodeRunDistanceMultiple.text, out val))
        {
            return;
        }

        if (val < 0)
        {
            return;
        }

        Main.s_Instance.m_fExplodeRunDistanceMultiple = val;
    }


	public void OnInputValueChanged_ExplodeStayTime()
	{
		float val = 0f;
		if (!float.TryParse(_inputfieldExplodeStayTime.text, out val))
		{
			return;
		}

		if (val < 0)
		{
			return;
		}

		Main.s_Instance.m_fExplodeStayTime = val;
	}


	public void OnInputValueChanged_ExplodeRunTime()
	{
		float val = 0.5f;
		if (!float.TryParse(_inputfieldExplodeRunTime.text, out val))
		{
			return;
		}

		if (val < 0)
		{
			return;
		}

		Main.s_Instance.m_fExplodeRunTime = val;
	}

	public void OnInputValueChanged_ExplodePercent()
	{
		float val = 0.7f;
		if (!float.TryParse(_inputfieldExplodePercent.text, out val))
		{
			return;
		}

		if (val < 0 || val > 1)
		{
			return;
		}

		Main.s_Instance.m_fExplodePercent = val;
	}

	public void OnInputValueChanged_ExplodeExpectNum()
	{
		float val = 8f;
		if (!float.TryParse(_inputfieldExplodeExpectNum.text, out val))
		{
			return;
		}

		if (val <= 0 )
		{
			return;
		}

		Main.s_Instance.m_fExpectExplodeNum = val;
	}





















    public void OnInputValueChanged_MaxBallNumPerPlayer()
    {
        float val = 0;
        if (!float.TryParse(_inputfieldMaxBallNumPerPlayer.text, out val))
        {
            return;
        }

        if (val <= 0)
        {
            return;
        }

        Main.s_Instance.m_fMaxBallNumPerPlayer = val;
    }

    public void OnInputValueChanged_SplitNum()
    {
        float val = 0;
        if (!float.TryParse(_inputfieldSplitNum.text, out val))
        {
            return;
        }

        if (val < 1 )
        {
            return;
        }

        Main.s_Instance.m_nSplitNum = (int)val;
    }

    public void OnInputValueChanged_SplitRunTime()
    {
        float val = 0;
        if (!float.TryParse(_inputfieldSplitRunTime.text, out val))
        {
            return;
        }

        if (val < 0)
        {
            return;
        }

        Main.s_Instance.m_fSplitRunTime = val;
    }

    public void OnInputValueChanged_SplitMaxDistance()
    {
        float val = 0;
        if (!float.TryParse(_inputfieldSplitMaxDistance.text, out val))
        {
            return;
        }

        if (val < 0)
        {
            return;
        }

        Main.s_Instance.m_fSplitMaxDistance = val;
    }

    public void OnInputValueChanged_SplitTimeInterval()
    {
        float val = 0;
        if (!float.TryParse(_inputfieldSplitTimeInterval.text, out val))
        {
            return;
        }

        if (val < 0)
        {
            return;
        }

        Main.s_Instance.m_fSplitTimeInterval = val;
    }


    //// !---- end Common

    //// !-- Eat Style

    public void OnDropDownValueChanged_EatMode()
    {
        m_eEatMode = (eEatMode)_dropdownEatMode.value;
    }

    public void OnInputFieldValueChanged_YaQiuTiaoJian()
    {
        float val = 1.0f;
        if (!float.TryParse (_inputYaQiuTiaoJian.text, out val)) {
            val = 1.0f;
        }

        if (val < 1.0f) {
            val = 1.0f;
        }

        m_fYaQiuTiaoJian = val;
    }

    public void OnInputFieldValueChanged_YaQiuNaiFenBi()
    {
        float val = 0.5f;
        if (!float.TryParse (_inputYaQiuBaiFenBi.text, out val)) {
            val = 0.5f;
        }

        if (val < 0.0f || val > 1.0f) {
            val = 0.5f;
        }

        m_fYaQiuBaiFenBi = val;
    }

    public void OnInputFieldValueChanged_YaQiuNaiFenBi_Self()
    {
        float val = 0.5f;
        if (!float.TryParse(_inputYaQiuBaiFenBi_Self.text, out val))
        {
            val = 0.5f;
        }

        if (val < 0.0f || val > 1.0f)
        {
            val = 0.5f;
        }

        m_fYaQiuBaiFenBi_Self = val;
    }

    public void OnInputFieldValueChanged_XiQiuSuDu()
    {
        float val = 1.0f;
        if (!float.TryParse (_inputXiQiuSuDu.text, out val)) {
            val = 1.0f;
        }

        if (val <= 0.0f) {
            val = 1.0f;
        }

        m_fXiQiuSuDu = val;
    }

    public void OnInputFieldValueChanged_MainTriggerPreUnfoldTime()
    {
        float val = 1.0f;
        if (!float.TryParse (_inputMainTriggerPreUnfoldTime.text, out val)) {
            val = 1.0f;
        }

        if (val <= 0.0f) {
            val = 1.0f;
        }

        Main.s_Instance.m_fMainTriggerPreUnfoldTime = val;
    }

    public void OnInputFieldValueChanged_MainTriggerUnfoldTime()
    {
        float val = 1.0f;
        if (!float.TryParse (_inputMainTriggerUnfoldTime.text, out val)) {
            val = 1.0f;
        }

        if (val <= 0.0f) {
            val = 1.0f;
        }

        Main.s_Instance.m_fMainTriggerUnfoldTime = val;
    }

    public void OnInputFieldValueChanged_UnfoldSale()
    {
        float val = 1.5f;
        if (!float.TryParse (_inputUnfoldSale.text, out val)) {
			return;
        }


        Main.s_Instance.m_fUnfoldSale = val;
    }

	public void OnInputFieldValueChanged_UnfoldCostMP()
	{
		float val = 1.5f;
		if (!float.TryParse (_inputUnfoldCostMP.text, out val)) {
			return;
		}
			

		Main.s_Instance.m_fUnfoldCostMP = val;
	}

    public void OnInputFieldValueChanged_UnfoldTimeInterval()
    {
        float val = 1.5f;
        if (!float.TryParse (_inputUnfoldTimeInterval.text, out val)) {
            val = 1.5f;
        }

        if (val <= 0.0f) {
            val = 1.5f;
        }

        Main.s_Instance.m_fUnfoldTimeInterval = val;
    }
    //// !-- end Eat Style


	public void ProcessGrassRelatedToMe( Ball ball )
	{
//		if (!ball.photonView.isMine) {
//			return;
//		}

		for (int i = 0; i < m_lstGrass.Count; i++) {
			Polygon grass = m_lstGrass [i];
			if (grass == null) {
				continue;
			}
			grass.SetStatus (0);
		}
	}


	public void BeginSprayPreDraw( float BeginSprayPreDrawTime, bool bAll, int nSprayId = 0 )
	{
		if (bAll) {
			for (int i = 0; i < m_lstSprays.Count; i++) {
				Spray spray = m_lstSprays [i];
				if (spray == null) {
					Debug.LogError ("spray null");
					continue;
				}
				spray.BeginPreDraw (BeginSprayPreDrawTime);
			}
		} else {
			Spray spray = FindSprayById ( nSprayId );
			if (spray == null) {
				Debug.LogError ("spray null");
				return;
			}
			spray.BeginPreDraw (BeginSprayPreDrawTime);
		}
	}

	public void SprayEatBall ( int nSprayGUID, float fEatSize )
	{
		if (nSprayGUID >= m_lstSprays.Count) {
			Debug.LogError ( "spray null" );
			return;
		}

		Spray spray = m_lstSprays [nSprayGUID];
		if (spray == null) {
			Debug.LogError ( "spray null" );
			return;
		}

		spray.DoEat ( fEatSize );
	}

	public Spray FindSprayById( int nGUID )
	{
		for (int i = 0; i < m_lstSprays.Count; i++) {
			if (m_lstSprays [i].GetGUID () == nGUID) {
				return m_lstSprays [i];
			}
		}
		return null;
	}

	public void LogTestInfo( int nViewId, string szNewContent )
	{
		string szContent = "";
		if (_dicTest.TryGetValue (nViewId, out szContent)) {
			_dicTest [nViewId] = szContent;
		}
		szContent += szNewContent;
		szContent += "\n";
		_dicTest [nViewId] = szContent;
	}

	public void OnClickShowTestLog()
	{
		int nViewId = 0;
		nViewId = int.Parse ( _inputTest.text );
		string szContent = "";
		if (_dicTest.TryGetValue (nViewId, out szContent)) {
			Debug.Log ( szContent );
		}
	}

	public void UpdateDebugInfo( string szInfo )
	{
		_txtDebugInfo.text = szInfo;
	}

    public void OnInputFieldValueChanged_TimeBeforeShowDeadUI()
    {
        float val = 3.0f;
        if (!float.TryParse(_inputTimeBeforeShowDeadUI.text, out val))
        {
            
        }

        if (val <= 0.0f)
        {
            val = 1.0f;
        }

        Main.s_Instance.m_fTimeBeforeUiDead = val;
    }

    public void OnInputFieldValueChanged_BaseVolumeDecreasedPercentWhenDead()
    {
        float val = -0.1f;
        if (!float.TryParse(_inputBaseVolumeDecreasedPercentWhenDead.text, out val))
        {

        }

        Main.s_Instance.m_fBaseVolumeyByItemDecreasedPercentWhenDead = val;
    }

    public void OnInputFieldValueChanged_TimeOfOneRound()
    {
        float val = 300.0f;
        if (!float.TryParse(_inputTimeTimeOfOneRound.text, out val))
        {

        }

        if (val <= 0.0f)
        {
            val = 300.0f;
        }

        Main.s_Instance.m_fTimeOfOneGame = val;
    }


    public void OnInputFieldValueChanged_ClassCircleRadius()
    {
        float val = 100.0f;
        if (!float.TryParse(_inputClassCircleRadius.text, out val))
        {

        }

        if (val <= 0.0f)
        {
            val = 100;
        }

       // Main.s_Instance.m_fClassCircleLength = val;
    }

    public void OnInputFieldValueChanged_ClassThreshold()       
    {
        float val = 8.0f;
        if (!float.TryParse(_inputClassThreshold.text, out val))
        {

        }

        if (val <= 0.0f)
        {
            val = 8f;
        }

     //   Main.s_Instance.m_fClassCircleThreshold = val;
    }

    public InputField _inputBallMinArea;
    public void OnInputFieldValueChanged_BallMinArea()
    {
        float val = 1f;
        if (!float.TryParse(_inputBallMinArea.text, out val))
        {
            val = 1f;
        }
        if ( val < 1f )
        {
            val = 1f;
        }
       Main.m_fBallMinVolume = val;
    }

    //// 排队相关
    public InputField _inputPlayerNumToStartGame;
    static int m_nPlayerNumToStartGame = 1;
    public void OnInputValueChanged_PlayerNumToStartGame()
    {
        if ( !int.TryParse(_inputPlayerNumToStartGame.text, out m_nPlayerNumToStartGame) )
        {
            m_nPlayerNumToStartGame = 1;
        }
    }

    public static int GetPlayerNumToStartGame()
    {
        return m_nPlayerNumToStartGame;
    }

    public static void SetPlayerNumToStartGame( int nNum )
    {
        m_nPlayerNumToStartGame = nNum;
    }

    //// 射球参数相关
    public float m_fEjectParamA = 2f;
    public float m_fEjectParamB = 0f;
    public float m_fEjectParamX = 0.5f;
    public void OnInputValueChanged_EjectParamA()
    {
        if ( !float.TryParse( _inputA.text, out m_fEjectParamA) )
        {
            m_fEjectParamA = 2f;
        }
    }

    public void OnInputValueChanged_EjectParamB()
    {
        if (!float.TryParse(_inputB.text, out m_fEjectParamB))
        {
            m_fEjectParamB = 0f;
        }
    }

    public void OnInputValueChanged_EjectParamX()
    {
        if (!float.TryParse(_inputX.text, out m_fEjectParamX))
        {
            m_fEjectParamX = 0.5f;
        }
    }

}
